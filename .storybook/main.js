const path = require('path');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');

module.exports = {
  "stories": [
    "../src/**/*.stories.mdx",
    "../src/**/*.stories.@(js|jsx|ts|tsx)"
  ],
  "addons": [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    {
      name: '@storybook/preset-scss',
      options: {
        cssLoaderOptions: {
          modules: { localIdentName: '[name]__[local]--[hash:base64:5]' },
        },
      },
    },
  ],
  staticDirs: ['../src/assets'],
  "core": {
    "builder": "webpack5",
    disableTelemetry: true,
  },
  webpackFinal: async (config, { configType }) => {
    const rules = config.module.rules;
    config.plugins.push(new ForkTsCheckerWebpackPlugin());

    // modify storybook's file-loader rule to avoid conflicts with svgr
    const fileLoaderRule = rules.find(rule => rule.test?.test('.svg'));
    fileLoaderRule.exclude = path.resolve(__dirname, '../src/assets/svg');

    config.module.rules.push(
      {
        test: /\.svg$/i,
        issuer: /\.[jt]sx?$/,
        use: ['@svgr/webpack', 'url-loader'],
      },
    );

    config.resolve.alias = {
      ...config.resolve?.alias,
      Components: path.resolve(__dirname, '../src/components/'),
      Pages: path.resolve(__dirname, '../src/pages/'),
      Styles: path.resolve(__dirname, '../src/assets/styles/'),
      Svg: path.resolve(__dirname, '../src/assets/svg/'),
      Img: path.resolve(__dirname, '../src/assets/img/'),
      Utils: path.resolve(__dirname, '../src/utils/'),
      Mocks: path.resolve(__dirname, '../src/mocks/'),
    };
    config.resolve.extensions.push(".ts", ".tsx");

    return config;
  },
}
