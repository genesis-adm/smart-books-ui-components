module.exports = {
  // Add your installed PostCSS plugins here:
  plugins: [
    // require('postcss-flexbugs-fixes'),
    require('autoprefixer')({
      flexbox: 'no-2009',
    }),
  ],
};
