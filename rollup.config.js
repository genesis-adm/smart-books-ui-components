import path from 'path';
import peerDepsExternal from 'rollup-plugin-peer-deps-external';
import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import typescript from '@rollup/plugin-typescript';
import json from '@rollup/plugin-json';
import postcss from 'rollup-plugin-postcss';
import url from '@rollup/plugin-url';
import svgr from '@svgr/rollup';
import alias from '@rollup/plugin-alias';
import autoprefixer from 'autoprefixer';
import easyImport from 'postcss-easy-import';
import postcssFlexbugsFixes from 'postcss-flexbugs-fixes';
import copy from 'rollup-plugin-copy';

const postCssUrl = require('postcss-url');
const packageJson = require('./package.json');

const customResolver = resolve({
  extensions: ['.tsx', 'ts', '.js', '.jsx', '.json', '.sass', '.scss'],
});

const plugins = [
  peerDepsExternal(),
  resolve(),
  commonjs(),
  json(),
  typescript({ tsconfig: './tsconfig.json' }),
  url(),
  svgr(),
  postcss({
    modules: true,
    autoModules: true,
    plugins: [
      easyImport,
      postcssFlexbugsFixes,
      autoprefixer(),
      postCssUrl({
        url: 'inline', // enable inline assets using base64 encoding
        maxSize: 10, // maximum file size to inline (in kilobytes)
        fallback: 'copy', // fallback method to use if max size is exceeded
      }),
    ],
  }),
  copy({
    targets: [
      {
        src: 'src/assets/',
        dest: 'build',
      },
    ],
  }),
  alias({
    entries: [
      { find: 'Components', replacement: path.resolve(__dirname, './src/components') },
      { find: 'Pages', replacement: path.resolve(__dirname, './src/pages') },
      { find: 'Styles', replacement: path.resolve(__dirname, '.src/assets/styles') },
      { find: 'Img', replacement: path.resolve(__dirname, '.src/assets/img') },
      { find: 'Svg', replacement: path.resolve(__dirname, './src/assets/svg') },
      { find: 'Utils', replacement: path.resolve(__dirname, './src/utils') },
      { find: 'Mocks', replacement: path.resolve(__dirname, './src/mocks') },
    ],
    customResolver,
  }),
];

export default [
  {
    input: 'src/index.ts',
    output: [
      {
        file: packageJson.main,
        format: 'cjs',
        sourcemap: true,
      },
      {
        file: packageJson.module,
        format: 'esm',
        sourcemap: true,
      },
    ],
    plugins,
  },
];
