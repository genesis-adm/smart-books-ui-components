import { Meta, Story } from '@storybook/react';
import { ErrorImage as Component, ErrorImageProps as Props } from 'Components/graphics/ErrorImage';
import React from 'react';

export default {
  title: 'Graphics/Error Image',
  component: Component,
  argTypes: {
    className: {
      table: {
        disable: true,
      },
    },
  },
} as Meta;

const ErrorImageComponent: Story<Props> = (args) => <Component {...args} />;

export const ErrorImage = ErrorImageComponent.bind({});
ErrorImage.args = {};
