import classNames from 'classnames';
import React from 'react';

// import ErrorImg from 'Img/error.png';
import style from './ErrorImage.module.scss';

export interface ErrorImageProps {
  error: string;
  className?: string;
}

const ErrorImage: React.FC<ErrorImageProps> = ({ error, className }) => (
  <img
    className={classNames(style.component, className)}
    // src={ErrorImg}
    src=""
    alt={error}
  />
);

export default ErrorImage;
