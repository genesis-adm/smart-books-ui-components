import { Meta, Story } from '@storybook/react';
import {
  CircledIcon as Component,
  CircledIconProps as Props,
} from 'Components/graphics/CircledIcon';
import { ReactComponent as DownloadIcon } from 'Svg/v2/16/download.svg';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Graphics/Circled Icon',
  component: Component,
  argTypes: {
    icon: hideProperty,
    className: hideProperty,
  },
} as Meta;

const CircledIconComponent: Story<Props> = (args) => <Component {...args} />;

export const CircledIcon = CircledIconComponent.bind({});
CircledIcon.args = {
  icon: <DownloadIcon />,
};
