import { Container } from 'Components/base/grid/Container';
import { BackgroundColorStaticNewTypes } from 'Components/types/colorTypes';
import classNames from 'classnames';
import React from 'react';

import style from './CircledIcon.module.scss';

export interface CircledIconProps {
  icon: React.ReactNode;
  background?: BackgroundColorStaticNewTypes;
  size?: '24' | '80';
  className?: string;
}

const CircledIcon: React.FC<CircledIconProps> = ({
  icon,
  background = 'grey-10',
  size = '80',
  className,
}) => (
  <Container
    background={background}
    justifycontent="center"
    alignitems="center"
    className={classNames(style.component, style[`size_${size}`], className)}
  >
    {icon}
  </Container>
);

export default CircledIcon;
