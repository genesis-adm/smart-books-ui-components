import { Meta, Story } from '@storybook/react';
import { FolderIcon as Component, FolderIconProps as Props } from 'Components/graphics/FolderIcon';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Graphics/Folder Icon',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const FolderIconComponent: Story<Props> = (args) => <Component {...args} />;

export const FolderIcon = FolderIconComponent.bind({});
FolderIcon.args = {};
