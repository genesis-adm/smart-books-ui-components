import { Container } from 'Components/base/grid/Container';
import { ReactComponent as MainIcon } from 'Svg/24/folder.svg';
import classNames from 'classnames';
import React from 'react';

import style from './FolderIcon.module.scss';

export interface FolderIconProps {
  className?: string;
}

const FolderIcon: React.FC<FolderIconProps> = ({ className }) => (
  <Container
    background="grey-10"
    justifycontent="center"
    alignitems="center"
    className={classNames(style.component, className)}
  >
    <MainIcon />
  </Container>
);

export default FolderIcon;
