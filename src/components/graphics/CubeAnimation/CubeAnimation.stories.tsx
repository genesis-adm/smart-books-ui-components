import { Meta, Story } from '@storybook/react';
import {
  CubeAnimation as Component,
  CubeAnimationProps as Props,
} from 'Components/graphics/CubeAnimation';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Graphics/Cube Animation',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const CubeAnimationComponent: Story<Props> = (args) => <Component {...args} />;

export const CubeAnimation = CubeAnimationComponent.bind({});
CubeAnimation.args = {
  size: 200,
};
