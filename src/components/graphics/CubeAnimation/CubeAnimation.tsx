import classNames from 'classnames';
import { useLottie } from 'lottie-react';
import React, { CSSProperties } from 'react';

import Cube from '../../../assets/animations/cube.json';
import style from './CubeAnimation.module.scss';

export interface CubeAnimationProps {
  size: 200 | 300;
  className?: string;
}

const CubeAnimation: React.FC<CubeAnimationProps> = ({ size, className }) => {
  const options = {
    animationData: Cube,
  };
  const styles: CSSProperties = {
    width: size * 2,
    height: size * 2,
    position: 'absolute',
    top: -size / 2,
    left: -size / 2,
  };
  const { View } = useLottie(options, styles);

  return (
    <div className={classNames(style.component, style[`size_${size}`], className)}>{View}</div>
  );
};

export default CubeAnimation;
