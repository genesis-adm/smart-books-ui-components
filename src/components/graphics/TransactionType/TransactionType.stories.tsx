import { Meta, Story } from '@storybook/react';
import {
  TransactionType as Component,
  TransactionTypeProps as Props,
} from 'Components/graphics/TransactionType';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Graphics/Transaction Type',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const TransactionTypeComponent: Story<Props> = (args) => <Component {...args} />;

export const TransactionType = TransactionTypeComponent.bind({});
TransactionType.args = {
  payment: 'in',
};
