import { ReactComponent as InIcon } from 'Svg/20/transaction-in.svg';
import { ReactComponent as OutIcon } from 'Svg/20/transaction-out.svg';
import React from 'react';

export interface TransactionTypeProps {
  payment: string;
}

const TransactionType: React.FC<TransactionTypeProps> = ({ payment }) => (
  <>
    {payment === 'in' && <InIcon />}
    {payment === 'out' && <OutIcon />}
  </>
);

export default TransactionType;
