import { ReactComponent as DescriptionIcon } from 'Svg/20/papers.svg';
import classNames from 'classnames';
import React from 'react';

import style from './DescriptionTooltip.module.scss';

export interface DescriptionTooltipProps {
  className?: string;
}

const DescriptionTooltip: React.FC<DescriptionTooltipProps> = ({ className }) => (
  <div className={classNames(style.container, className)}>
    <DescriptionIcon className={style.icon} />
  </div>
);

export default DescriptionTooltip;
