import { Meta, Story } from '@storybook/react';
import {
  DescriptionTooltip as Component,
  DescriptionTooltipProps as Props,
} from 'Components/graphics/DescriptionTooltip';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Graphics/Description Tooltip',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const DescriptionTooltipComponent: Story<Props> = (args) => <Component {...args} />;

export const DescriptionTooltip = DescriptionTooltipComponent.bind({});
DescriptionTooltip.args = {};
