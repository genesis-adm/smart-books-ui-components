import { SelectAllHeight, SelectType } from 'Components/inputs/Select/Select.types';
import MultiSelectItemList from 'Components/inputs/Select/components/MultiSelectItemList/MultiSelectItemList';
import SelectIcon from 'Components/inputs/Select/components/SelectIcon/SelectIcon';
import SelectLabel from 'Components/inputs/Select/components/SelectLabel/SelectLabel';
import SelectPlaceholder from 'Components/inputs/Select/components/SelectPlaceholder/SelectPlaceholder';
import { useMultiSelectValue } from 'Components/inputs/Select/hooks/useMultiSelectValue';
import { SelectValue } from 'Components/inputs/Select/hooks/useSelectValue';
import classNames from 'classnames';
import React, { ReactElement, ReactNode, useEffect, useState } from 'react';

import styles from './MultiSelectBody.module.scss';

interface MultiSelectBodyProps<IsMulti extends boolean = false> {
  type?: SelectType;
  isSelectFocused: boolean;
  isSelectFilled: boolean;
  isSelectError: boolean;
  isSelectHovered: boolean;
  isSelectDisabled: boolean;
  isSelectReadOnly: boolean;

  height: '36' | '48';

  value?: SelectValue<IsMulti>;
  isSelectRequired: boolean;

  placeholder?: string;
  label?: string;
  icon?: ReactNode;
  iconStaticColor?: boolean;

  onDeleteItem: (index: number) => void;
  onChangeHeight?: (height: SelectAllHeight) => void;
}

const MultiSelectBody = <IsMulti extends boolean = false>(
  props: MultiSelectBodyProps<IsMulti>,
): ReactElement => {
  const {
    type = 'base',
    isSelectFocused,
    isSelectFilled,
    height,
    isSelectError,
    isSelectRequired,
    isSelectHovered,
    isSelectDisabled,
    isSelectReadOnly,
    placeholder,
    label,
    icon,
    iconStaticColor,
    value,
    onDeleteItem,
    onChangeHeight,
  } = props as MultiSelectBodyProps<true>;
  const [itemsWidth, setItemsWidth] = useState<Record<string, number>[]>([]);
  const [itemListWrapperWidth, setItemListWrapperWidth] = useState<number>();

  const { normalizedValue, hiddenItemsLength, itemsContainerHeight } = useMultiSelectValue({
    value,
    height,
    containerWidth: itemListWrapperWidth,
    itemsWidth,
  });

  useEffect(() => {
    if (!value) {
      setItemsWidth([]);
      return;
    }

    setItemsWidth(
      value?.map(({ value, label }) => {
        const item = itemsWidth?.find(
          (item) => Object.keys(item)[0] === value + label && Object.values(item)[0] > 0,
        );

        return item || { [value + label]: 0 };
      }),
    );
  }, [value]);

  useEffect(() => {
    onChangeHeight?.(itemsContainerHeight);
  }, [onChangeHeight, itemsContainerHeight]);

  return (
    <div
      className={classNames(styles.multiSelectMain, {
        [styles.focused]: isSelectFocused || isSelectFilled,
      })}
    >
      <div
        className={classNames(styles.multiSelectItemWrapper, {
          [styles.filled]: isSelectFilled,
          [styles.gap]: !!icon,
        })}
      >
        {!(height === '48' && isSelectFocused && !isSelectFilled) &&
          !(height === '36' && (isSelectFocused || isSelectFilled)) && (
            <SelectLabel
              label={label}
              height={height}
              isSelectFocused={isSelectFocused}
              isSelectDisabled={isSelectDisabled}
              isSelectView={isSelectReadOnly}
              isSelectError={isSelectError}
              isSelectRequired={isSelectRequired}
              isSelectFilled={isSelectFilled}
            />
          )}

        <div className={styles.multiSelectValueWrapper}>
          {!!icon && (
            <SelectIcon
              icon={icon}
              iconStaticColor={iconStaticColor}
              isSelectError={isSelectError}
              isSelectFocused={isSelectFocused}
              isSelectDisabled={isSelectDisabled}
              isSelectView={isSelectReadOnly}
            />
          )}

          {!isSelectFilled ? (
            isSelectFocused && (
              <SelectPlaceholder
                placeholder={placeholder}
                isSelectError={isSelectError}
                height={height}
              />
            )
          ) : (
            <MultiSelectItemList
              type={type}
              value={normalizedValue}
              isSelectDisabled={isSelectDisabled}
              isSelectReadOnly={isSelectReadOnly}
              isSelectHovered={isSelectHovered}
              isSelectError={isSelectError}
              hiddenItemsLength={hiddenItemsLength}
              onChangeWidth={setItemListWrapperWidth}
              onDeleteItem={onDeleteItem}
              onChangeItemsWidth={setItemsWidth}
            />
          )}
        </div>
      </div>
    </div>
  );
};

export default MultiSelectBody;
