import classNames from 'classnames';
import React, { ReactElement } from 'react';

import styles from './SelectPlaceholder.module.scss';

interface SelectPlaceholderProps {
  placeholder?: string;
  height: '36' | '48';
  isSelectError: boolean;
}

const SelectPlaceholder = ({
  placeholder = '',
  height,
  isSelectError,
}: SelectPlaceholderProps): ReactElement => {
  return (
    <span
      className={classNames(styles.selectPlaceholder, styles[`height_${height}`], {
        [styles.error]: isSelectError,
      })}
    >
      {placeholder}
    </span>
  );
};

export default SelectPlaceholder;
