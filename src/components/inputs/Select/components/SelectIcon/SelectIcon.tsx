import { Icon } from 'Components/base/Icon';
import classNames from 'classnames';
import React, { ReactElement, ReactNode } from 'react';

import styles from './SelectIcon.module.scss';

interface SelectIconProps {
  icon: ReactNode;
  iconStaticColor?: boolean;
  isSelectFocused: boolean;
  isSelectDisabled: boolean;
  isSelectView: boolean;
  isSelectError: boolean;
}

const SelectIcon = ({
  icon,
  iconStaticColor,
  isSelectFocused,
  isSelectDisabled,
  isSelectView,
  isSelectError,
}: SelectIconProps): ReactElement => {
  const iconElement = icon as unknown as ReactElement;
  const isDialogTooltip = typeof icon !== 'string' && iconElement.props.control;

  return (
    <>
      {isDialogTooltip ? (
        icon
      ) : (
        <Icon
          icon={icon}
          path="inherit"
          className={classNames(styles.selectIcon, {
            [styles.focused]: isSelectFocused && !isSelectDisabled && !isSelectView,
            [styles.disabled]: isSelectDisabled,
            [styles.error]: isSelectError,
          })}
          clickThrough
          staticColor={iconStaticColor}
        />
      )}
    </>
  );
};

export default SelectIcon;
