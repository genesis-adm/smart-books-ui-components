import { TagSegmented } from 'Components/base/TagSegmented';
import { InputTag } from 'Components/base/inputs/InputElements/InputTag';
import { SelectType } from 'Components/inputs/Select/Select.types';
import styles from 'Components/inputs/Select/components/MultiSelectBody/MultiSelectBody.module.scss';
import handleAccountLength from 'Utils/handleAccountLength';
import classNames from 'classnames';
import React, {
  CSSProperties,
  Dispatch,
  Fragment,
  ReactElement,
  SetStateAction,
  useCallback,
  useEffect,
  useState,
} from 'react';

import { useBounding } from '../../../../../hooks/useBounding';
import { Nullable } from '../../../../../types/general';
import { SelectValue } from '../../hooks/useSelectValue';

interface MultiSelectItemListProps {
  type?: SelectType;
  value?: SelectValue<true>;

  isSelectHovered: boolean;
  isSelectError: boolean;
  isSelectDisabled: boolean;
  isSelectReadOnly: boolean;

  hiddenItemsLength: Nullable<number>;
  onChangeWidth?: (width: number) => void;
  onDeleteItem?: (index: number) => void;
  onChangeItemsWidth: Dispatch<SetStateAction<Record<string, number>[]>>;
}

const MultiSelectItemList = ({
  type,
  value,
  isSelectHovered,
  isSelectError,
  hiddenItemsLength,
  onChangeWidth,
  onDeleteItem,
  onChangeItemsWidth,
  isSelectDisabled,
  isSelectReadOnly,
}: MultiSelectItemListProps): ReactElement => {
  const { ref, domRect: wrapperDomRect } = useBounding<HTMLDivElement>();

  const [itemsWidth, setItemsWidth] = useState<Record<string, number>[]>([]);

  const itemStyle: CSSProperties = hiddenItemsLength === null ? { position: 'absolute' } : {};

  const changeItemWidth = useCallback((domRect: DOMRect, id: string) => {
    setItemsWidth((prev) => {
      if (prev?.find((obj) => Object.keys(obj)[0] === id)) {
        return prev.map((item) => (Object.keys(item)[0] === id ? { [id]: domRect.width } : item));
      }

      return [...prev, { [id]: domRect.width }];
    });
  }, []);

  const deleteItem = useCallback(
    (index: number) =>
      (id: string): void => {
        onDeleteItem?.(index);

        setItemsWidth((prev) => {
          return prev?.filter((obj) => Object.keys(obj)[0] !== id);
        });
      },
    [onDeleteItem],
  );

  useEffect(() => {
    if (!wrapperDomRect?.width) return;

    onChangeWidth?.(wrapperDomRect?.width);
  }, [wrapperDomRect?.width, onChangeWidth]);

  useEffect(() => {
    onChangeItemsWidth(itemsWidth);
  }, [itemsWidth, onChangeItemsWidth]);

  return (
    <div
      ref={ref}
      className={classNames(styles.multiSelectItemList, {
        [styles.hidden]: hiddenItemsLength === null,
      })}
    >
      {value?.map(
        ({ label, value, subLabel, tertiaryLabel, quadroLabel, bankAccountType }, index) => {
          const dividedTertiaryLabel = tertiaryLabel?.split('|');
          const secondLabelValue =
            bankAccountType !== 'cash'
              ? handleAccountLength(dividedTertiaryLabel?.[0] ? dividedTertiaryLabel[0] : '')
              : dividedTertiaryLabel?.[0];
          return (
            <Fragment key={label + value}>
              {type === 'bankAccount' ? (
                <TagSegmented
                  id={value + label}
                  mainLabel={label}
                  secondLabel={secondLabelValue}
                  noWrapSecondLabel={bankAccountType === 'cash'}
                  thirdLabel={dividedTertiaryLabel?.[1]}
                  deleteIcon
                  onChangeBounding={changeItemWidth}
                  complexTooltipMessage={{
                    message: label,
                    subMessage: subLabel,
                    secondaryMessage: tertiaryLabel,
                    thirdMessage: quadroLabel,
                    tooltipWidth: '560',
                  }}
                  background={isSelectHovered ? 'grey-30' : 'grey-20'}
                  className={styles.bankAccountTag}
                  style={itemStyle}
                  onDelete={deleteItem(index)}
                />
              ) : (
                <InputTag
                  key={value + label}
                  id={value + label}
                  label={label}
                  onDelete={deleteItem(index)}
                  background={isSelectHovered ? 'grey_30' : 'grey'}
                  disabled={isSelectDisabled}
                  readOnly={isSelectReadOnly}
                  isError={isSelectError}
                  onChangeBounding={changeItemWidth}
                  style={itemStyle}
                />
              )}
            </Fragment>
          );
        },
      )}

      {!!hiddenItemsLength && (
        <InputTag
          id={`+${hiddenItemsLength}`}
          label={`+${hiddenItemsLength}`}
          onDelete={() => {}}
          background={isSelectHovered ? 'grey_30' : 'grey'}
          disabled={isSelectDisabled}
          readOnly
          tooltipMessage="Show all"
          isError={isSelectError}
        />
      )}
    </div>
  );
};

export default MultiSelectItemList;
