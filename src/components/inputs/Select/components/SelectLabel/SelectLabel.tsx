import classNames from 'classnames';
import React, { ReactElement } from 'react';

import styles from './SelectLabel.module.scss';

interface SelectLabelProps {
  label?: string;
  height: '36' | '48';
  isSelectFocused: boolean;
  isSelectDisabled: boolean;
  isSelectView: boolean;
  isSelectError: boolean;
  isSelectRequired: boolean;
  isSelectFilled: boolean;
}

const SelectLabel = ({
  label = '',
  height,
  isSelectView,
  isSelectError,
  isSelectDisabled,
  isSelectRequired,
  isSelectFocused,
  isSelectFilled,
}: SelectLabelProps): ReactElement => {
  return (
    <span
      className={classNames(styles.selectLabel, styles[`height_${height}`], {
        [styles.collapsed]: isSelectFocused || isSelectFilled,
        [styles.focused]: isSelectFocused && !isSelectDisabled && !isSelectView,
        [styles.required]: isSelectRequired,
        [styles.disabled]: isSelectDisabled,
        [styles.error]: isSelectError,
      })}
    >
      {label}
    </span>
  );
};

export default SelectLabel;
