import { Tooltip } from 'Components/base/Tooltip';
import SelectLabel from 'Components/inputs/Select/components/SelectLabel/SelectLabel';
import { useSingleSelectValueComponentsByType } from 'Components/inputs/Select/hooks/useSingleSelectValueComponentsByType';
import classNames from 'classnames';
import React, { ReactElement, ReactNode, useRef } from 'react';

import { useTooltipValue } from '../../../../../hooks/useTooltipValue';
import { SelectType } from '../../Select.types';
import { SelectValue } from '../../hooks/useSelectValue';
import styles from './SingleSelectBody.module.scss';

export interface SingleSelectBodyProps<IsMulti extends boolean = false> {
  type: SelectType;
  isSelectFocused: boolean;
  isSelectFilled: boolean;
  isSelectView: boolean;
  isSelectError: boolean;

  height: '36' | '48';

  isSelectDisabled: boolean;

  value?: SelectValue<IsMulti>;
  isSelectRequired: boolean;

  placeholder?: string;
  label?: string;
  icon?: ReactNode;
  iconStaticColor?: boolean;
}

const SingleSelectBody = <IsMulti extends boolean = false>(
  props: SingleSelectBodyProps<IsMulti>,
): ReactElement => {
  const {
    isSelectFocused,
    isSelectDisabled,
    isSelectView,
    isSelectError,
    isSelectFilled,
    isSelectRequired,
    height,
    value,
    label,
    type,
    icon,
  } = props as SingleSelectBodyProps;

  const singleSelectValueComponents = useSingleSelectValueComponentsByType(
    props as SingleSelectBodyProps,
  );
  const subLabelTextRef = useRef<HTMLDivElement>(null);
  const tooltipMessage = useTooltipValue(subLabelTextRef, value?.subLabel);

  const isValue = !!value?.label && !!value?.label;

  return (
    <div
      className={classNames(styles.singleSelectMain, styles[`height_${height}`], {
        [styles.focused]: isSelectFocused || isSelectFilled,
      })}
    >
      <div
        className={classNames(styles.singleSelectItemWrapper, {
          [styles.filled]: isSelectFilled || isSelectFocused,
          [styles.gap]: isValue || !!icon,
        })}
      >
        {!(height === '36' && (isSelectFocused || isSelectFilled)) && (
          <SelectLabel
            label={label}
            height={height}
            isSelectFocused={isSelectFocused}
            isSelectDisabled={isSelectDisabled}
            isSelectView={isSelectView}
            isSelectError={isSelectError}
            isSelectRequired={isSelectRequired}
            isSelectFilled={isSelectFilled}
          />
        )}

        <div className={classNames(styles.singleSelectValueWrapper, { [styles.gap]: isValue })}>
          {singleSelectValueComponents[type]}
        </div>
      </div>

      {type === 'baseWithCategory' && !!value?.subLabel && (
        <Tooltip
          tooltipWidth="max-content"
          message={tooltipMessage && value?.label}
          secondaryMessage={tooltipMessage && value?.tertiaryLabel}
          subMessage={tooltipMessage}
          thirdMessage={tooltipMessage && value?.quadroLabel}
          wrapperClassName={styles.singleSelectItemSubLabelWrapper}
        >
          <span
            className={classNames(styles.singleSelectItemSubLabel, {
              [styles.error]: isSelectError,
            })}
            ref={subLabelTextRef}
          >
            {value?.subLabel || ''}
          </span>
        </Tooltip>
      )}
    </div>
  );
};

export default SingleSelectBody;
