import React, { ReactNode } from 'react';

import { TestableProps } from '../../../types/general';
import { InputError, InputWidthType } from '../../../types/inputs';
import { SelectValue } from './hooks/useSelectValue';

export interface SelectOption {
  icon?: ReactNode;
  label: string;
  subLabel?: string;
  tertiaryLabel?: string;
  quadroLabel?: string;
  bankAccountType?: 'card' | 'bank' | 'cash' | 'wallet';

  value: string;
}

export type SelectType = 'base' | 'baseWithCategory' | 'bankAccount' | 'column';
export type SelectHeight = '36' | '48';

export type SelectAllHeight = SelectHeight | '72' | '82';

export interface SelectProps<IsMulti extends boolean> extends TestableProps {
  width?: InputWidthType;
  height?: SelectHeight;
  className?: string;
  type?: SelectType;
  background?: 'grey-10' | 'transparent' | 'white-100';

  isMulti?: IsMulti;
  required?: boolean;
  isFocused?: boolean;
  isLoading?: boolean;
  disabled?: boolean;
  readOnly?: boolean;

  value?: SelectValue<IsMulti>;
  defaultValue?: SelectValue<IsMulti>;
  onChange?: (value: SelectValue<IsMulti>) => void;
  label?: string;
  placeholder?: string;
  icon?: ReactNode;
  iconStaticColor?: boolean;

  error?: InputError;

  children: (args: {
    isOpen: boolean;
    selectValue?: SelectValue<IsMulti>;
    onOptionClick: (value: SelectValue<IsMulti>) => void;
    closeDropdown: () => void;
    selectElement: Element;
  }) => React.ReactElement;
}
