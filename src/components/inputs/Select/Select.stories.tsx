import { Meta, Story } from '@storybook/react';
import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownFooter } from 'Components/SelectDropdown/components/SelectDropdownFooter';
import { OptionNoResults } from 'Components/base/inputs/SelectNew/options/OptionNoResults';
import { Search } from 'Components/inputs/Search';
import { Select as Component } from 'Components/inputs/Select';
import { SelectOption } from 'Components/inputs/Select/Select.types';
import { OptionWithFourLabels } from 'Components/inputs/Select/options/OptionWithFourLabels';
import { OptionWithSingleLabel } from 'Components/inputs/Select/options/OptionWithSingleLabel';
import { OptionWithTwoLabels } from 'Components/inputs/Select/options/OptionWithTwoLabels';
import { OptionsFooter } from 'Components/inputs/Select/options/OptionsFooter';
import { bankAccountMultiSelectValues, currencyOptions, options } from 'Mocks/fakeOptions';
import { ReactComponent as MastercardIcon } from 'Svg/v2/16/logo-mastercard.svg';
import { deleteArrayItemByIndex } from 'Utils/arrays';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';
import SelectDropdownBody from '../../SelectDropdown/components/SelectDropdownBody/SelectDropdownBody';
import SelectDropdownItemList from '../../SelectDropdown/components/SelectDropdownItemList/SelectDropdownItemList';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Inputs/selects',
  component: Component,
  argTypes: {
    className: hideProperty,
    dropdownEmptyState: { control: { type: 'boolean' } },
  },
  args: { dropdownEmptyState: false },
} as Meta;

const SelectComponent: Story = (args) => {
  const [searchValue, setSearchValue] = useState('');

  return (
    <div
      style={{
        display: 'flex',
        gap: '10px',
        flexWrap: 'wrap',
      }}
    >
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          gap: 32,
        }}
      >
        <Component {...args}>
          {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
            <SelectDropdown selectElement={selectElement}>
              <SelectDropdownBody>
                <Search
                  className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                  width="full"
                  height="40"
                  name="search-LE"
                  placeholder="Search option"
                  value={searchValue}
                  onChange={(e) => setSearchValue(e.target.value)}
                  searchParams={[]}
                  onSearch={() => {}}
                  onClear={() => {}}
                />
                {args.dropdownEmptyState ? (
                  <OptionNoResults />
                ) : (
                  <SelectDropdownItemList>
                    {options.map((value, index) => (
                      <OptionWithSingleLabel
                        id={value.label}
                        key={index}
                        label={value?.label}
                        selected={!Array.isArray(selectValue) && selectValue?.value === value.value}
                        onClick={() => {
                          onOptionClick({
                            value: value?.value,
                            label: value?.label,
                          });
                          closeDropdown();
                        }}
                      />
                    ))}
                  </SelectDropdownItemList>
                )}
              </SelectDropdownBody>
            </SelectDropdown>
          )}
        </Component>
      </div>

      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          gap: 32,
        }}
      >
        <Component {...args}>
          {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
            <SelectDropdown selectElement={selectElement}>
              <SelectDropdownBody>
                <Search
                  className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                  width="full"
                  height="40"
                  name="search-LE"
                  placeholder="Search option"
                  value={searchValue}
                  onChange={(e) => setSearchValue(e.target.value)}
                  searchParams={[]}
                  onSearch={() => {}}
                  onClear={() => {}}
                />
                {args.dropdownEmptyState ? (
                  <OptionNoResults />
                ) : (
                  <SelectDropdownItemList>
                    {options.map((value, index) => (
                      <OptionWithTwoLabels
                        id={value.label}
                        key={index}
                        tooltip
                        label={value?.label}
                        secondaryLabel={value.secondaryLabel}
                        disabled={!!(index % 2)}
                        selected={!Array.isArray(selectValue) && selectValue?.value === value.value}
                        onClick={() => {
                          onOptionClick({
                            value: value?.value,
                            label: value?.label,
                            tertiaryLabel: value?.secondaryLabel,
                          });
                          closeDropdown();
                        }}
                      />
                    ))}
                  </SelectDropdownItemList>
                )}
              </SelectDropdownBody>
            </SelectDropdown>
          )}
        </Component>
      </div>

      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          gap: 32,
        }}
      >
        <Component {...args} type="baseWithCategory">
          {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
            <SelectDropdown selectElement={selectElement}>
              <SelectDropdownBody>
                <Search
                  className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                  width="full"
                  height="40"
                  name="search-LE"
                  placeholder="Search option"
                  value={searchValue}
                  onChange={(e) => setSearchValue(e.target.value)}
                  searchParams={[]}
                  onSearch={() => {}}
                  onClear={() => {}}
                />
                {args.dropdownEmptyState ? (
                  <OptionNoResults />
                ) : (
                  <SelectDropdownItemList>
                    {options.map((value, index) => (
                      <OptionWithTwoLabels
                        id={value.label}
                        key={index}
                        icon={<MastercardIcon />}
                        iconStaticColor
                        tooltip
                        labelDirection="horizontal"
                        label={value?.label}
                        secondaryLabel={value.subLabel}
                        selected={!Array.isArray(selectValue) && selectValue?.value === value.value}
                        onClick={() => {
                          onOptionClick({
                            value: value?.value,
                            label: value?.label,
                            subLabel: value?.subLabel,
                          });
                          closeDropdown();
                        }}
                      />
                    ))}
                  </SelectDropdownItemList>
                )}
              </SelectDropdownBody>
            </SelectDropdown>
          )}
        </Component>
      </div>
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          gap: 32,
        }}
      >
        <Component {...args} type="baseWithCategory">
          {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
            <SelectDropdown selectElement={selectElement}>
              <SelectDropdownBody>
                <Search
                  className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                  width="full"
                  height="40"
                  name="search-LE"
                  placeholder="Search option"
                  value={searchValue}
                  onChange={(e) => setSearchValue(e.target.value)}
                  searchParams={[]}
                  onSearch={() => {}}
                  onClear={() => {}}
                />
                {args.dropdownEmptyState ? (
                  <OptionNoResults />
                ) : (
                  <SelectDropdownItemList>
                    {options.map((value, index) => (
                      <OptionWithFourLabels
                        key={index}
                        label={value?.label}
                        secondaryLabel={value.subLabel}
                        tertiaryLabel={value.tertiaryLabel}
                        quadroLabel={value.quadroLabel}
                        selected={selectValue === value}
                        onClick={() => {
                          onOptionClick(value);
                          closeDropdown();
                        }}
                      />
                    ))}
                  </SelectDropdownItemList>
                )}
              </SelectDropdownBody>
            </SelectDropdown>
          )}
        </Component>
      </div>
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          gap: 32,
        }}
      >
        <Component {...args} type="column" label="Column select">
          {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
            <SelectDropdown selectElement={selectElement}>
              <SelectDropdownBody>
                <Search
                  className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                  width="full"
                  height="40"
                  name="search-LE"
                  placeholder="Search option"
                  value={searchValue}
                  onChange={(e) => setSearchValue(e.target.value)}
                  searchParams={[]}
                  onSearch={() => {}}
                  onClear={() => {}}
                />
                {args.dropdownEmptyState ? (
                  <OptionNoResults />
                ) : (
                  <SelectDropdownItemList>
                    {options.map((value, index) => (
                      <OptionWithTwoLabels
                        id={value.label}
                        key={index}
                        label={value?.label}
                        secondaryLabel={value.secondaryLabel}
                        selected={selectValue === value}
                        onClick={() => {
                          onOptionClick({
                            value: value?.value,
                            label: value?.label,
                            subLabel: value?.subLabel,
                          });
                          closeDropdown();
                        }}
                      />
                    ))}
                  </SelectDropdownItemList>
                )}
              </SelectDropdownBody>
            </SelectDropdown>
          )}
        </Component>
      </div>
    </div>
  );
};

export const Select = SelectComponent.bind({});
Select.args = {
  width: '416',
  height: '36',
  isMulti: false,
  placeholder: 'Choose',
  label: 'Currency',
  required: true,
  isLoading: false,
  // defaultValue: options[2],
  icon: <MastercardIcon />,
  iconStaticColor: true,
  readOnly: false,
  disabled: false,
  // error: { messages: ['ErrOr 1', 'Erkvodk'] },
};

const MultiSelectComponent: Story = (args) => {
  const [appliedOptions, setAppliedOptions] = useState<SelectOption[] | undefined | null>([]);
  const [searchValue, setSearchValue] = useState('');
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        gap: 32,
      }}
    >
      <Component
        {...args}
        isMulti
        value={appliedOptions}
        onChange={(value) => {
          setAppliedOptions(value);
        }}
      >
        {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
          <SelectDropdown
            selectElement={selectElement}
            width="480"
            selectValue={selectValue}
            isMulti
          >
            {({ selectedOptions, setSelectedOptions }) => (
              <>
                <SelectDropdownBody>
                  <Search
                    className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                    width="full"
                    height="40"
                    name="search-LE"
                    placeholder="Search option"
                    value={searchValue}
                    onChange={(e) => setSearchValue(e.target.value)}
                    searchParams={[]}
                    onSearch={(value) => setSearchValue(value)}
                    onClear={() => {}}
                  />
                  {args.dropdownEmptyState ? (
                    <OptionNoResults />
                  ) : (
                    <SelectDropdownItemList
                      type="multi"
                      isSelectFilled={!!appliedOptions?.length}
                      selectedItems={appliedOptions?.map((appliedOption, index) => (
                        <OptionWithSingleLabel
                          id={appliedOption?.label}
                          key={index}
                          label={appliedOption?.label}
                          type="checkbox"
                          selected={
                            !!selectedOptions?.find(
                              ({ value: selectedValue }) => selectedValue === appliedOption?.value,
                            )
                          }
                          onClick={() => {
                            if (!selectedOptions) return;

                            const index = selectedOptions?.findIndex(
                              ({ value: selectedValue }) => selectedValue === appliedOption?.value,
                            );

                            if (index !== -1) {
                              const arr: SelectOption[] = deleteArrayItemByIndex(
                                selectedOptions,
                                index,
                              );
                              setSelectedOptions(arr);
                              return;
                            }
                            setSelectedOptions([...selectedOptions, appliedOption]);
                          }}
                        />
                      ))}
                      otherItems={currencyOptions
                        ?.filter(
                          ({ value }) =>
                            !appliedOptions?.find(
                              ({ value: appliedValue }) => appliedValue === value,
                            ),
                        )
                        .map((value, index) => (
                          <OptionWithSingleLabel
                            id={value?.label}
                            key={index}
                            label={value?.label}
                            type="checkbox"
                            selected={
                              !!selectedOptions?.find(
                                ({ value: selectedValue }) => selectedValue === value?.value,
                              )
                            }
                            onClick={() => {
                              if (!selectedOptions) return;

                              const index = selectedOptions?.findIndex(
                                ({ value: selectedValue }) => selectedValue === value?.value,
                              );

                              if (index !== -1) {
                                const arr: SelectOption[] = deleteArrayItemByIndex(
                                  selectedOptions,
                                  index,
                                );
                                setSelectedOptions(arr);
                                return;
                              }
                              setSelectedOptions([...selectedOptions, value]);
                            }}
                          />
                        ))}
                    />
                  )}
                </SelectDropdownBody>

                {!args.dropdownEmptyState && (
                  <SelectDropdownFooter>
                    <OptionsFooter
                      optionsLength={currencyOptions.length}
                      selectedOptionsNumber={selectedOptions?.length || 0}
                      checked={selectedOptions?.length === currencyOptions.length}
                      onSelectChange={() => {
                        selectedOptions?.length !== currencyOptions.length
                          ? setSelectedOptions(currencyOptions)
                          : setSelectedOptions([]);
                      }}
                      onClear={() => {
                        setSelectedOptions([]);
                      }}
                      onApply={() => {
                        onOptionClick(selectedOptions);
                        closeDropdown();
                      }}
                    />
                  </SelectDropdownFooter>
                )}
              </>
            )}
          </SelectDropdown>
        )}
      </Component>
      <Component
        {...args}
        type="bankAccount"
        isMulti
        // width="416"
        width="672"
        value={appliedOptions}
        onChange={(value) => {
          setAppliedOptions(value);
        }}
      >
        {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
          <SelectDropdown
            selectElement={selectElement}
            width="480"
            selectValue={selectValue}
            isMulti
          >
            {({ selectedOptions, setSelectedOptions }) => (
              <>
                <SelectDropdownBody>
                  <Search
                    className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                    width="full"
                    height="40"
                    name="search-LE"
                    placeholder="Search option"
                    value={searchValue}
                    onChange={(e) => setSearchValue(e.target.value)}
                    searchParams={[]}
                    onSearch={(value) => setSearchValue(value)}
                    onClear={() => {}}
                  />
                  {args.dropdownEmptyState ? (
                    <OptionNoResults />
                  ) : (
                    <SelectDropdownItemList
                      type="multi"
                      isSelectFilled={!!appliedOptions?.length}
                      selectedItems={appliedOptions?.map((appliedOption, index) => (
                        <OptionWithFourLabels
                          id={appliedOption?.label}
                          key={index}
                          label={appliedOption?.label}
                          type="checkbox"
                          selected={
                            !!selectedOptions?.find(
                              ({ value: selectedValue }) => selectedValue === appliedOption?.value,
                            )
                          }
                          onClick={() => {
                            if (!selectedOptions) return;

                            const index = selectedOptions?.findIndex(
                              ({ value: selectedValue }) => selectedValue === appliedOption?.value,
                            );

                            if (index !== -1) {
                              const arr: SelectOption[] = deleteArrayItemByIndex(
                                selectedOptions,
                                index,
                              );
                              setSelectedOptions(arr);
                              return;
                            }
                            setSelectedOptions([...selectedOptions, appliedOption]);
                          }}
                        />
                      ))}
                      otherItems={bankAccountMultiSelectValues
                        ?.filter(
                          ({ value }) =>
                            !appliedOptions?.find(
                              ({ value: appliedValue }) => appliedValue === value,
                            ),
                        )
                        .map((value, index) => (
                          <OptionWithFourLabels
                            id={value?.value}
                            key={index}
                            label={value?.bank}
                            secondaryLabel={value.type}
                            tertiaryLabel={`${value?.accountNumber} | ${value?.currency}`}
                            quadroLabel={value?.legalEntity}
                            type="checkbox"
                            selected={
                              !!selectedOptions?.find(
                                ({ value: selectedValue }) => selectedValue === value?.value,
                              )
                            }
                            onClick={() => {
                              if (!selectedOptions) return;

                              const index = selectedOptions?.findIndex(
                                ({ value: selectedValue }) => selectedValue === value?.value,
                              );

                              if (index !== -1) {
                                const arr: SelectOption[] = deleteArrayItemByIndex(
                                  selectedOptions,
                                  index,
                                );
                                setSelectedOptions(arr);
                                return;
                              }
                              setSelectedOptions([
                                ...selectedOptions,
                                {
                                  value: value?.value,
                                  label: value?.bank,
                                  subLabel: value?.type,
                                  tertiaryLabel: `${value.accountNumber} | ${value?.currency}`,
                                  quadroLabel: value?.legalEntity,
                                },
                              ]);
                            }}
                          />
                        ))}
                    />
                  )}
                </SelectDropdownBody>

                {!args.dropdownEmptyState && (
                  <SelectDropdownFooter>
                    <OptionsFooter
                      optionsLength={currencyOptions.length}
                      selectedOptionsNumber={selectedOptions?.length || 0}
                      checked={selectedOptions?.length === currencyOptions.length}
                      onSelectChange={() => {
                        selectedOptions?.length !== currencyOptions.length
                          ? setSelectedOptions(currencyOptions)
                          : setSelectedOptions([]);
                      }}
                      onClear={() => {
                        setSelectedOptions([]);
                      }}
                      onApply={() => {
                        onOptionClick(selectedOptions);
                        closeDropdown();
                      }}
                    />
                  </SelectDropdownFooter>
                )}
              </>
            )}
          </SelectDropdown>
        )}
      </Component>
    </div>
  );
};

export const MultiSelect = MultiSelectComponent.bind({});
MultiSelect.args = {
  isMulti: true,
  width: '300',
  height: '48',
  placeholder: 'Select currency',
  label: 'Currency',
  required: true,
  icon: <MastercardIcon />,
  iconStaticColor: true,
  readOnly: false,
  disabled: false,
  // error: { messages: ['ErrOr 1', 'Erkvodk'] },
  // defaultValue: options,
};
