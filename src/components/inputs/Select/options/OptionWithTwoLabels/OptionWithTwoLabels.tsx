import { Checkbox } from 'Components/base/Checkbox';
import { Icon } from 'Components/base/Icon';
import { Radio } from 'Components/base/Radio';
import { Tooltip } from 'Components/base/Tooltip';
import { ReactComponent as CheckIcon } from 'Svg/16/check.svg';
import classNames from 'classnames';
import React, { ReactElement, useRef } from 'react';

import { useTooltipValue } from '../../../../../hooks/useTooltipValue';
import style from './OptionWithTwoLabels.module.scss';
import { OptionWithTwoLabelsProps } from './OptionWithTwoLabels.types';

// Reminder: do not forget to add the following code to root index.ts
// export { OptionWithTwoLabels } from './components/base/inputs/options/OptionWithTwoLabels';

const OptionWithTwoLabels = ({
  id,
  label,
  secondaryLabel,
  selected,
  icon,
  iconStaticColor,
  type,
  className,
  tooltip,
  labelDirection = 'vertical',
  onClick,
  disabled,
}: OptionWithTwoLabelsProps): ReactElement => {
  const textPrimaryRef = useRef<HTMLDivElement>(null);
  const textSecondaryRef = useRef<HTMLDivElement>(null);

  const tooltipValue = useTooltipValue(textPrimaryRef, label);
  const secondaryTooltipValue = useTooltipValue(textSecondaryRef, secondaryLabel);

  const click = (
    e: React.MouseEvent<HTMLLIElement> | React.FormEvent<HTMLInputElement> | boolean,
  ) => {
    if (!disabled) onClick(e);
  };

  return (
    <li
      className={classNames(
        style.item,
        {
          [style.selected]: selected,
          [style.disabled]: !!disabled,
        },
        className,
      )}
      role="presentation"
      onClick={click}
    >
      <div className={classNames(style.container, { [style.disabled]: !!disabled })}>
        {!type && selected && <Icon icon={<CheckIcon />} path="inherit" />}

        {icon && (
          <Icon
            className={style.icon}
            icon={icon}
            path="inherit"
            staticColor={iconStaticColor}
            transition="inherit"
          />
        )}
        {type === 'radio' && (
          <Radio id={id} name={id} checked={selected} disabled={disabled} onChange={click} />
        )}
        {type === 'checkbox' && (
          <Checkbox name={id} checked={selected} disabled={disabled} onChange={click} />
        )}

        {tooltip ? (
          <div
            className={classNames(style.text, {
              [style.row]: labelDirection === 'horizontal',
              [style.column]: labelDirection === 'vertical',
            })}
          >
            <Tooltip
              tooltipWidth="max-content"
              message={tooltipValue}
              subMessage={tooltipValue && labelDirection === 'horizontal' && secondaryLabel}
              secondaryMessage={tooltipValue && labelDirection === 'vertical' && secondaryLabel}
            >
              <span ref={textPrimaryRef} className={style.label}>
                {label}
              </span>
            </Tooltip>
            <Tooltip
              tooltipWidth="max-content"
              message={secondaryTooltipValue && label}
              subMessage={labelDirection === 'horizontal' && secondaryTooltipValue}
              secondaryMessage={labelDirection === 'vertical' && secondaryTooltipValue}
            >
              <span ref={textSecondaryRef} className={style.secondaryLabel}>
                {secondaryLabel}
              </span>
            </Tooltip>
          </div>
        ) : (
          <div
            className={classNames(style.text, {
              [style.row]: labelDirection === 'horizontal',
              [style.label_width]: labelDirection === 'horizontal',
              [style.column]: labelDirection === 'vertical',
            })}
          >
            <span className={style.label}>{label}</span>
            <span className={style.secondaryLabel}>{secondaryLabel}</span>
          </div>
        )}
      </div>
    </li>
  );
};

export default OptionWithTwoLabels;
