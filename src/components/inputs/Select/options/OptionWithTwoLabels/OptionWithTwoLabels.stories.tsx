import { Meta, Story } from '@storybook/react';
import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { SelectNew } from 'Components/base/inputs/SelectNew';
import {
  OptionWithTwoLabels as Component,
  OptionWithTwoLabelsProps as Props,
} from 'Components/inputs/Select/options/OptionWithTwoLabels/index';
import { options } from 'Mocks/fakeOptions';
import { ReactComponent as MastercardIcon } from 'Svg/v2/24/logo-mastercard.svg';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Draft Components/OptionWithTwoLabels',
  component: Component,
  argTypes: {
    className: hideProperty,
    label: hideProperty,
    secondaryLabel: hideProperty,
    selected: hideProperty,
    icon: hideProperty,
    onClick: hideProperty,
  },
} as Meta;

const OptionWithTwoLabelsComponent: Story<Props> = (args) => (
  <SelectNew onChange={() => {}} label="Choose option" placeholder="Choose option" name="test">
    {({ onClick, state, searchValue, selectElement }) => (
      <SelectDropdown selectElement={selectElement}>
        <SelectDropdownBody>
          <SelectDropdownItemList>
            {options
              .filter(({ label }) => label.toLowerCase().includes(searchValue.toLowerCase()))
              .map(({ label, secondaryLabel, value }) => (
                <Component
                  {...args}
                  key={value}
                  label={label}
                  icon={<MastercardIcon />}
                  iconStaticColor
                  secondaryLabel={secondaryLabel}
                  selected={state?.value === value}
                  tooltip
                  onClick={() =>
                    onClick({
                      label,
                      value,
                    })
                  }
                />
              ))}
          </SelectDropdownItemList>
        </SelectDropdownBody>
      </SelectDropdown>
    )}
  </SelectNew>
);

export const OptionWithTwoLabels = OptionWithTwoLabelsComponent.bind({});
OptionWithTwoLabels.args = { labelDirection: 'vertical' };
