import React, { ReactNode } from 'react';

export interface OptionWithTwoLabelsProps {
  id: string;
  label: string | ReactNode;
  secondaryLabel?: string;
  selected: boolean;
  icon?: ReactNode;
  iconStaticColor?: boolean;
  type?: 'radio' | 'checkbox';
  className?: string;
  tooltip?: boolean;
  labelDirection?: 'horizontal' | 'vertical';

  onClick(e: React.MouseEvent<HTMLLIElement> | React.FormEvent<HTMLInputElement> | boolean): void;

  disabled?: boolean;
}
