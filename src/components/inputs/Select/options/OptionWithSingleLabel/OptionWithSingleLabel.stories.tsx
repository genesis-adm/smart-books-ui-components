import { Meta, Story } from '@storybook/react';
import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { SelectNew } from 'Components/base/inputs/SelectNew';
import {
  OptionWithSingleLabel as Component,
  OptionWithSingleLabelProps as Props,
} from 'Components/inputs/Select/options/OptionWithSingleLabel/index';
import { options } from 'Mocks/fakeOptions';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Draft Components/OptionWithSingleLabel',
  component: Component,
  argTypes: {
    label: hideProperty,
    selected: hideProperty,
    icon: hideProperty,
    onClick: hideProperty,
    className: hideProperty,
    input: hideProperty,
  },
} as Meta;

const OptionWithSingleLabelComponent: Story<Props> = (args) => (
  <SelectNew onChange={() => {}} placeholder="Choose option" label="Choose option" name="test">
    {({ onClick, state, selectElement }) => (
      <SelectDropdown selectElement={selectElement}>
        <SelectDropdownBody>
          <SelectDropdownItemList>
            {options.map(({ label, value }, index) => (
              <Component
                {...args}
                key={value}
                label={label}
                selected={state?.value === value}
                disabled={index === 2}
                onClick={() =>
                  onClick({
                    label,
                    value,
                  })
                }
              />
            ))}
          </SelectDropdownItemList>
        </SelectDropdownBody>
      </SelectDropdown>
    )}
  </SelectNew>
);

export const OptionWithSingleLabel = OptionWithSingleLabelComponent.bind({});
OptionWithSingleLabel.args = { type: 'default', disabled: false, height: '40' };
