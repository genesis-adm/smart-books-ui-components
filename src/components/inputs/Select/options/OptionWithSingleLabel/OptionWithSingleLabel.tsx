import { Checkbox } from 'Components/base/Checkbox';
import { Icon } from 'Components/base/Icon';
import { Radio } from 'Components/base/Radio';
import { Tooltip } from 'Components/base/Tooltip';
import { ReactComponent as CheckIcon } from 'Svg/16/check.svg';
import classNames from 'classnames';
import React, { ReactElement, useRef } from 'react';

import { useTooltipValue } from '../../../../../hooks/useTooltipValue';
import style from './OptionWithSingleLabel.module.scss';
import { OptionWithSingleLabelProps } from './OptionWithSingleLabel.types';

const OptionWithSingleLabel = ({
  id,
  label,
  selected,
  icon,
  iconStaticColor,
  input,
  type = 'default',
  disabled,
  height = '40',
  className,
  onClick,
}: OptionWithSingleLabelProps): ReactElement => {
  const textRef = useRef<HTMLDivElement>(null);
  const tooltipMessage = useTooltipValue(textRef, label);
  const handleClick = () => {
    if (!disabled) onClick();
  };

  return (
    <li
      className={classNames(
        style.item,
        style[`height_${height}`],
        {
          [style.selected]: selected,
          [style.disabled]: disabled,
        },
        className,
      )}
      role="presentation"
      onClick={handleClick}
    >
      <div className={style.labelWrapper}>
        {type === 'radio' && (
          <Radio id={id} name={id} disabled={disabled} checked={selected} onChange={onClick} />
        )}
        {type === 'checkbox' && (
          <Checkbox name={id} disabled={disabled} checked={selected} onChange={onClick} />
        )}
        {type === 'default' && selected && <Icon path="inherit" icon={<CheckIcon />} />}
        {!!icon && (
          <div
            className={classNames(style.iconWrapper, { [style.currentColor]: !iconStaticColor })}
          >
            {icon}
          </div>
        )}
        {!!label && (
          <Tooltip message={tooltipMessage} tooltipWidth="max-content">
            <span className={style.label} ref={textRef}>
              {label}
            </span>
          </Tooltip>
        )}
      </div>

      {!!input && <div className={style.input}>{input}</div>}
    </li>
  );
};

export default OptionWithSingleLabel;
