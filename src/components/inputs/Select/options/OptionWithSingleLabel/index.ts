export { default as OptionWithSingleLabel } from './OptionWithSingleLabel';
export type { OptionWithSingleLabelProps } from './OptionWithSingleLabel.types';
