import React, { ReactNode } from 'react';

export interface OptionWithSingleLabelProps {
  id: string;
  label: string | ReactNode;
  selected: boolean;
  icon?: React.ReactNode;
  iconStaticColor?: boolean;
  input?: React.ReactNode;
  height?: '32' | '40';
  type?: 'default' | 'radio' | 'checkbox';
  disabled?: boolean;
  className?: string;

  onClick(): void;
}
