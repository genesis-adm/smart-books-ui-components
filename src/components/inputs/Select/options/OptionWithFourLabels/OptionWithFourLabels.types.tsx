import React, { ReactNode } from 'react';

export interface OptionWithFourLabelsProps {
  id?: string;
  type?: 'default' | 'checkbox';
  label: string | ReactNode;
  secondaryLabel?: string;
  tertiaryLabel?: string;
  quadroLabel?: string;
  selected: boolean;
  icon?: React.ReactNode;
  className?: string;
  onClick(): void;
}
