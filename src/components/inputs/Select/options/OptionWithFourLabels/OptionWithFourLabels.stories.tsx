import { Meta, Story } from '@storybook/react';
import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { SelectNew } from 'Components/base/inputs/SelectNew';
import {
  OptionWithFourLabels as Component,
  OptionWithFourLabelsProps as Props,
} from 'Components/inputs/Select/options/OptionWithFourLabels/index';
import { options } from 'Mocks/fakeOptions';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Draft Components/OptionWithFourLabels',
  component: Component,
  argTypes: {
    label: hideProperty,
    secondaryLabel: hideProperty,
    tertiaryLabel: hideProperty,
    quadroLabel: hideProperty,
    selected: hideProperty,
    icon: hideProperty,
    onClick: hideProperty,
    className: hideProperty,
  },
} as Meta;

const OptionWithFourLabelsComponent: Story<Props> = (args) => (
  <SelectNew onChange={() => {}} label="Choose option" placeholder="Choose option" name="test">
    {({ onClick, state, selectElement }) => (
      <SelectDropdown selectElement={selectElement}>
        <SelectDropdownBody>
          <SelectDropdownItemList>
            {options.map(({ label, secondaryLabel, tertiaryLabel, quadroLabel, value }) => (
              <Component
                {...args}
                key={value}
                label={label}
                secondaryLabel={secondaryLabel}
                tertiaryLabel={tertiaryLabel}
                quadroLabel={quadroLabel}
                selected={state?.value === value}
                onClick={() =>
                  onClick({
                    label,
                    value,
                  })
                }
              />
            ))}
          </SelectDropdownItemList>
        </SelectDropdownBody>
      </SelectDropdown>
    )}
  </SelectNew>
);

export const OptionWithFourLabels = OptionWithFourLabelsComponent.bind({});
OptionWithFourLabels.args = {};
