import { Checkbox } from 'Components/base/Checkbox';
import { Icon } from 'Components/base/Icon';
import { Tooltip } from 'Components/base/Tooltip';
import { ReactComponent as CheckIcon } from 'Svg/16/check.svg';
import classNames from 'classnames';
import React, { ReactElement, useRef } from 'react';

import { useTooltipValue } from '../../../../../hooks/useTooltipValue';
import style from './OptionWithFourLabels.module.scss';
import { OptionWithFourLabelsProps } from './OptionWithFourLabels.types';

// Reminder: do not forget to add the following code to root index.ts
// export { OptionWithFourLabels } from './components/base/inputs/options/OptionWithFourLabels';

const OptionWithFourLabels = ({
  id,
  type = 'default',
  label,
  secondaryLabel,
  tertiaryLabel,
  quadroLabel,
  selected,
  icon,
  className,
  onClick,
}: OptionWithFourLabelsProps): ReactElement => {
  const labelRef = useRef<HTMLDivElement>(null);
  const secondaryLabelRef = useRef<HTMLDivElement>(null);
  const tertiaryLabelRef = useRef<HTMLDivElement>(null);
  const quadroLabelRef = useRef<HTMLDivElement>(null);

  const tooltipLabelMessage = useTooltipValue(labelRef, label);
  const secondaryTooltipLabelMessage = useTooltipValue(secondaryLabelRef, secondaryLabel);
  const tertiaryTooltipLabelMessage = useTooltipValue(tertiaryLabelRef, tertiaryLabel);
  const quadroTooltipLabelMessage = useTooltipValue(quadroLabelRef, quadroLabel);

  return (
    <li
      className={classNames(style.item, { [style.selected]: selected }, className)}
      role="presentation"
      onClick={onClick}
    >
      <div className={classNames(style.checkIconBlock, { [style.gap]: type === 'checkbox' })}>
        {type === 'checkbox' && (
          <Checkbox name={id ? id : ''} checked={selected} onChange={onClick} />
        )}
        {type === 'default' && selected && <Icon icon={<CheckIcon />} path="inherit" />}

        <div className={style.left}>
          <Tooltip
            tooltipWidth="max-content"
            message={tooltipLabelMessage}
            secondaryMessage={tertiaryLabel}
            thirdMessage={quadroLabel}
            subMessage={secondaryLabel}
          >
            <span className={style.label} ref={labelRef}>
              {icon}
              {label}
            </span>
          </Tooltip>
          <Tooltip
            tooltipWidth="max-content"
            message={tertiaryTooltipLabelMessage && label}
            secondaryMessage={tertiaryTooltipLabelMessage}
            thirdMessage={tertiaryTooltipLabelMessage && quadroLabel}
            subMessage={tertiaryTooltipLabelMessage && secondaryLabel}
          >
            <span className={style.tertiaryLabel} ref={tertiaryLabelRef}>
              {tertiaryLabel}
            </span>
          </Tooltip>
          <Tooltip
            tooltipWidth="max-content"
            message={quadroTooltipLabelMessage && label}
            secondaryMessage={quadroTooltipLabelMessage && tertiaryLabel}
            thirdMessage={quadroTooltipLabelMessage}
            subMessage={quadroTooltipLabelMessage && secondaryLabel}
          >
            <span className={style.quadroLabel} ref={quadroLabelRef}>
              {quadroLabel}
            </span>
          </Tooltip>
        </div>
      </div>
      <div className={style.right}>
        <Tooltip
          tooltipWidth="max-content"
          message={secondaryTooltipLabelMessage && label}
          secondaryMessage={secondaryTooltipLabelMessage && tertiaryLabel}
          thirdMessage={secondaryTooltipLabelMessage && quadroLabel}
          subMessage={secondaryTooltipLabelMessage}
        >
          <span className={style.secondaryLabel} ref={secondaryLabelRef}>
            {secondaryLabel}
          </span>
        </Tooltip>
      </div>
    </li>
  );
};
export default OptionWithFourLabels;
