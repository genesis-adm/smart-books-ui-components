export interface OptionsFooterProps {
  className?: string;
  checked: boolean;
  optionsLength: number;
  selectedOptionsNumber: number;
  onSelectChange?: () => void;
  onClear?: () => void;
  onApply?: () => void;
}
