import { Checkbox } from 'Components/base/Checkbox';
import { Button } from 'Components/base/buttons/Button';
import classNames from 'classnames';
import React, { ReactElement } from 'react';

import style from './OptionsFooter.module.scss';
import { OptionsFooterProps } from './OptionsFooter.types';

const OptionsFooter = ({
  className,
  checked,
  optionsLength,
  selectedOptionsNumber,
  onSelectChange,
  onClear,
  onApply,
}: OptionsFooterProps): ReactElement => (
  <div className={classNames(style.component, className)}>
    <div className={style.select_group}>
      {!!onSelectChange && (
        <>
          <Checkbox
            name="tickAll"
            checked={checked}
            onChange={onSelectChange}
            textcolor={checked ? 'violet-90' : 'grey-100'}
            font="caption-regular"
            className={style.checkbox}
          >
            Select all
          </Checkbox>
          <span className={style.label}>
            {selectedOptionsNumber} of {optionsLength}
          </span>
        </>
      )}
    </div>

    <div className={style.button_group}>
      {!!onClear && (
        <Button
          color="violet-100"
          background={'transparent'}
          onClick={onClear}
          className={style.clear_button}
        >
          Clear
        </Button>
      )}

      {!!onApply && (
        <Button width="108" height="32" onClick={onApply}>
          Apply
        </Button>
      )}
    </div>
  </div>
);

export default OptionsFooter;
