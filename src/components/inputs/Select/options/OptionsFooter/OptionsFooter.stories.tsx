import { Meta, Story } from '@storybook/react';
import {
  OptionsFooter as Component,
  OptionsFooterProps as Props,
} from 'Components/inputs/Select/options/OptionsFooter/index';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Draft Components/OptionsFooter',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const OptionsFooterComponent: Story<Props> = (args) => <Component {...args} />;

export const OptionsFooter = OptionsFooterComponent.bind({});
OptionsFooter.args = {};
