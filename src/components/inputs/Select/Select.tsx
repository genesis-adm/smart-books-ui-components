import { Icon } from 'Components/base/Icon';
import InputErrorPopup from 'Components/base/inputs/InputErrorPopup/InputErrorPopup';
import { InputWrapper } from 'Components/base/inputs/InputWrapper';
import MultiSelectBody from 'Components/inputs/Select/components/MultiSelectBody/MultiSelectBody';
import SingleSelectBody from 'Components/inputs/Select/components/SingleSelectBody/SingleSelectBody';
import { useFocusNode } from 'Components/inputs/Select/hooks/useFocusNode';
import { ReactComponent as CaretDownIcon } from 'Svg/v2/16/caret-down.svg';
import { ReactComponent as CrossInCircleIcon } from 'Svg/v2/16/cross-in-circle.svg';
import { deleteArrayItemByIndex } from 'Utils/arrays';
import classNames from 'classnames';
import React, { ReactElement, useCallback, useMemo, useRef, useState } from 'react';

import useClickOutside from '../../../hooks/useClickOutside';
import useScrollOutside from '../../../hooks/useScrollOutside';
import styles from './Select.module.scss';
import { SelectAllHeight, SelectProps } from './Select.types';
import { SelectValue, useSelectValue } from './hooks/useSelectValue';

const Select = <IsMulti extends boolean = false>({
  className,
  height = '48',
  width,
  type = 'base',
  isFocused,
  isLoading,
  defaultValue,
  value,
  label = '',
  placeholder = '',
  icon,
  iconStaticColor,
  required = false,
  error,
  children,
  onChange,
  background = 'grey-10',
  isMulti = false as IsMulti,
  disabled = false,
  readOnly = false,
  dataTestId,
}: SelectProps<IsMulti>): ReactElement => {
  const { current: memoizedDefaultValue } = useRef(defaultValue);
  const wrapperRef = useRef<HTMLDivElement>(null);
  const wrapperElement = wrapperRef?.current;

  const [currentHeight, setCurrentHeight] = useState<SelectAllHeight>(height);
  const [isSelectHovered, setIsSelectHovered] = useState(false);

  const isError = !!error?.messages?.filter((message) => !!message)?.length;

  const { selectValue, setSelectValue } = useSelectValue<IsMulti>({
    value,
    defaultValue: memoizedDefaultValue,
  });

  const isSelectFilled = Array.isArray(selectValue)
    ? !!selectValue?.length
    : !!(selectValue?.value && selectValue?.label);

  const { isFocused: isSelectFocused, focus, blur } = useFocusNode(isFocused);

  useClickOutside({
    ref: wrapperRef.current,
    cb: blur,
  });
  useScrollOutside({
    isListOpen: isSelectFocused,
    cb: focus,
  });

  const click = (): void => {
    focus();
  };

  const handleOptionClick = useCallback(
    (value: SelectValue<IsMulti>) => {
      setSelectValue(value);
      onChange?.(value);
    },
    [setSelectValue, onChange],
  );

  const changeHeight = useCallback((height: SelectAllHeight) => {
    setCurrentHeight(height);
  }, []);

  const deleteMultiSelectItem = useCallback(
    (index: number): void => {
      if (!Array.isArray(selectValue)) return;

      setSelectValue(deleteArrayItemByIndex(selectValue, index));
      onChange?.(deleteArrayItemByIndex(selectValue, index));
    },
    [selectValue, setSelectValue, onChange],
  );

  const clearSelect = (e: React.MouseEvent<HTMLDivElement>) => {
    e?.stopPropagation();

    setSelectValue(null);
    onChange?.(null);
  };

  const controls = useMemo(() => {
    if (!wrapperElement) return;

    return {
      isOpen: isSelectFocused,
      selectValue,
      onOptionClick: handleOptionClick,
      closeDropdown: blur,
      selectElement: wrapperElement,
    };
  }, [wrapperElement, selectValue, handleOptionClick, blur, isSelectFocused]);

  return (
    <div
      ref={wrapperRef}
      style={{
        height: `${currentHeight}px`,
        width: width ? `${width}px` : '100%',
      }}
      data-testid={dataTestId}
    >
      <InputWrapper
        isFocused={isSelectFocused}
        isLoading={isLoading}
        isError={isError}
        width={width}
        height={height}
        onClick={click}
        onHover={setIsSelectHovered}
        disabled={disabled}
        readOnly={readOnly}
        style={{ height: `${currentHeight}px` }}
        background={background}
      >
        {({ isHovered }) => (
          <div className={classNames(styles.select, className)}>
            {isMulti ? (
              <MultiSelectBody
                type={type}
                isSelectDisabled={disabled}
                isSelectReadOnly={readOnly}
                isSelectFocused={isSelectFocused}
                isSelectFilled={isSelectFilled}
                isSelectError={isError}
                isSelectHovered={isSelectHovered}
                height={height}
                isSelectRequired={required}
                label={label}
                placeholder={placeholder}
                value={selectValue}
                onDeleteItem={deleteMultiSelectItem}
                onChangeHeight={changeHeight}
                icon={icon}
                iconStaticColor={iconStaticColor}
              />
            ) : (
              <SingleSelectBody
                isSelectFocused={isSelectFocused}
                isSelectFilled={isSelectFilled}
                isSelectView={readOnly}
                isSelectError={isError}
                height={height}
                type={type}
                isSelectDisabled={disabled}
                isSelectRequired={required}
                label={label}
                placeholder={placeholder}
                value={selectValue}
                icon={icon}
                iconStaticColor={iconStaticColor}
              />
            )}

            <div
              className={classNames(styles.iconsWrapper, {
                [styles.h40fixed]: height === '48',
              })}
            >
              {isSelectFilled && !disabled && !readOnly && (
                <Icon
                  icon={<CrossInCircleIcon />}
                  path="inherit"
                  className={classNames(styles.selectClearCross, { [styles.error]: isError })}
                  onClick={clearSelect}
                />
              )}

              {!readOnly && (
                <Icon
                  icon={<CaretDownIcon />}
                  path="inherit"
                  className={classNames(styles.selectArrow, {
                    [styles.focused]: isSelectFocused,
                    [styles.disabled]: disabled,
                    [styles.error]: isError,
                  })}
                  clickThrough
                />
              )}
            </div>

            {isError && isHovered && !isSelectFocused && wrapperElement && (
              <InputErrorPopup
                inputWrapperElement={wrapperElement}
                errorMessage={error?.messages}
              />
            )}
          </div>
        )}
      </InputWrapper>

      {isSelectFocused && controls && children(controls)}
    </div>
  );
};

Select.displayName = 'Select';

export default Select;
