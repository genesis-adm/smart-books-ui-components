import { useEffect, useMemo, useState } from 'react';

export const useFocusNode = (isFocused = false) => {
  const [isNodeFocused, setIsNodeFocused] = useState(false);

  useEffect(() => {
    if (!isFocused) {
      setIsNodeFocused(false);
      return;
    }

    setIsNodeFocused(true);
  }, [isFocused]);

  return useMemo(
    () => ({
      isFocused: isNodeFocused,
      focus: () => setIsNodeFocused(true),
      blur: () => setIsNodeFocused(false),
    }),
    [isNodeFocused],
  );
};
