import { useEffect, useMemo, useState } from 'react';

import { Nullable } from '../../../../types/general';
import { SelectOption } from '../Select.types';

export type SelectValue<IsMulti extends boolean> = Nullable<
  IsMulti extends false ? SelectOption : SelectOption[]
>;

export const useSelectValue = <IsMulti extends boolean = false>({
  value,
  defaultValue,
}: {
  value?: SelectValue<IsMulti>;
  defaultValue?: SelectValue<IsMulti>;
}) => {
  const [selectValue, setSelectValue] = useState<SelectValue<IsMulti> | undefined>(defaultValue);

  useEffect(() => {
    if (value) {
      setSelectValue(value);
    }
  }, [value]);

  return useMemo(
    () => ({
      selectValue,
      setSelectValue,
    }),
    [selectValue],
  );
};
