import { SelectAllHeight, SelectHeight } from 'Components/inputs/Select/Select.types';
import { useEffect, useMemo, useState } from 'react';

import { SelectValue } from '../hooks/useSelectValue';

const ITEMS_GAP = 8;
const ADDITIONAL_ITEMS_TAG_WIDTH = 42;

type RowsTypeValue = {
  width: number;
  itemsLength: number;
};

type RowsType = Record<'1' | '2', RowsTypeValue>;

export const useMultiSelectValue = ({
  value,
  height,
  containerWidth,
  itemsWidth,
}: {
  value?: SelectValue<true>;
  height: SelectHeight;
  containerWidth?: number;
  itemsWidth: Record<string, number>[];
}): {
  normalizedValue: SelectValue<true> | undefined;
  hiddenItemsLength: number | null;
  itemsContainerHeight: SelectAllHeight;
} => {
  const [normalizedValue, setNormalizedValue] = useState<SelectValue<true>>();
  const [itemsContainerHeight, setItemsContainerHeight] = useState<SelectAllHeight>(height);

  const [items, setItems] = useState<RowsType | null>(null);
  const [hiddenItemsLength, setHiddenItemsLength] = useState<number | null>(null);

  const itemsLength = value?.length || 0;
  const firstRowItemsLength = items ? items['1']?.itemsLength : 0;

  useEffect(() => {
    setNormalizedValue(value);
    setHiddenItemsLength(null);
  }, [value]);

  useEffect(() => {
    if (!containerWidth) return;

    if (!itemsWidth?.length && !!items) {
      setItems(null);
      return;
    }

    if (!itemsWidth.some((item) => Object.values(item).includes(0))) {
      const items = itemsWidth?.reduce((acc, cur, index) => {
        const curWidth = Object.values(cur)[0] || 0;
        const firstRowAccWidth = acc['1']?.width || 0;
        const secondRowAccWidth = acc['2']?.width || 0;
        const curItemWidth = curWidth + ITEMS_GAP;

        const isFirstRowFull =
          containerWidth < firstRowAccWidth + curItemWidth - ITEMS_GAP || !!secondRowAccWidth;

        if (!isFirstRowFull) {
          return {
            ...acc,
            ['1']: {
              width: firstRowAccWidth + curItemWidth,
              itemsLength: index + 1,
            },
          };
        } else if (containerWidth - ADDITIONAL_ITEMS_TAG_WIDTH > secondRowAccWidth + curItemWidth) {
          const accItemsLength = acc['1']?.itemsLength || 0;

          return {
            ...acc,
            ['2']: {
              width: secondRowAccWidth + curItemWidth,
              itemsLength: index - accItemsLength + 1,
            },
          };
        }

        return acc;
      }, {} as Record<string, RowsTypeValue>) as RowsType;

      setItems(items);
    }
  }, [containerWidth, itemsWidth]);

  useEffect(() => {
    if (!normalizedValue || !items) return;

    const visibleItemsLength = Object.values(items)?.reduce(
      (acc, { itemsLength }) => acc + itemsLength,
      0,
    );

    if (normalizedValue?.length > visibleItemsLength) {
      setNormalizedValue(normalizedValue.slice(0, visibleItemsLength));
    }

    const hiddenItemListLength =
      itemsLength - visibleItemsLength > 0 ? itemsLength - visibleItemsLength : 0;

    setHiddenItemsLength(hiddenItemListLength);
  }, [items]);

  useEffect(() => {
    if (!firstRowItemsLength) {
      setItemsContainerHeight(height);
      return;
    }

    if (itemsLength > firstRowItemsLength) {
      setItemsContainerHeight(multiSelectExtendedHeight[height]);
    } else {
      setItemsContainerHeight(height);
    }
  }, [firstRowItemsLength, itemsLength, height]);

  return useMemo(
    () => ({
      normalizedValue,
      hiddenItemsLength,
      itemsContainerHeight,
    }),
    [normalizedValue, hiddenItemsLength, itemsContainerHeight],
  );
};

const multiSelectExtendedHeight: Record<SelectHeight, '72' | '82'> = {
  ['36']: '72',
  ['48']: '82',
};
