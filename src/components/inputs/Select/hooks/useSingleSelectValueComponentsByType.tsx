import { TagSegmented } from 'Components/base/TagSegmented';
import { Tooltip } from 'Components/base/Tooltip';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import SelectIcon from 'Components/inputs/Select/components/SelectIcon/SelectIcon';
import SelectPlaceholder from 'Components/inputs/Select/components/SelectPlaceholder/SelectPlaceholder';
import styles from 'Components/inputs/Select/components/SingleSelectBody/SingleSelectBody.module.scss';
import handleAccountLength from 'Utils/handleAccountLength';
import classNames from 'classnames';
import React, { ReactNode, useMemo, useRef } from 'react';

import { useTooltipValue } from '../../../../hooks/useTooltipValue';
import { SelectType } from '../Select.types';
import { SingleSelectBodyProps } from '../components/SingleSelectBody/SingleSelectBody';

export const useSingleSelectValueComponentsByType = ({
  isSelectFocused,
  isSelectDisabled,
  isSelectView,
  isSelectError,
  placeholder,
  height,
  value,
  icon,
  iconStaticColor,
}: SingleSelectBodyProps): Record<SelectType, ReactNode> => {
  const baseTextRef = useRef<HTMLDivElement>(null);
  const baseWithCategoryTextRef = useRef<HTMLDivElement>(null);
  const columnTypeMainLabelRef = useRef<HTMLDivElement>(null);
  const columnTypeSubLabelRef = useRef<HTMLDivElement>(null);

  const baseTooltipMessage = useTooltipValue(baseTextRef, value?.value ? value?.label : '');
  const baseWithCategoryTooltipMessage = useTooltipValue(
    baseWithCategoryTextRef,
    value?.value ? value?.label : '',
  );
  const columnTypeMainLabelTooltipMessage = useTooltipValue(
    columnTypeMainLabelRef,
    value?.value ? value?.label : '',
  );
  const columnTypeSubLabelTooltipMessage = useTooltipValue(
    columnTypeSubLabelRef,
    value?.value ? value?.subLabel : '',
  );

  const isValue = !!value?.value && !!value?.label;

  return useMemo(
    () => ({
      base: (
        <>
          {!!icon && (
            <SelectIcon
              icon={icon}
              iconStaticColor={iconStaticColor}
              isSelectError={isSelectError}
              isSelectFocused={isSelectFocused}
              isSelectDisabled={isSelectDisabled}
              isSelectView={isSelectView}
            />
          )}

          {!isValue ? (
            isSelectFocused && (
              <SelectPlaceholder
                placeholder={placeholder}
                isSelectError={isSelectError}
                height={height}
              />
            )
          ) : (
            <Tooltip
              tooltipWidth="max-content"
              message={baseTooltipMessage}
              subMessage={value?.subLabel}
              secondaryMessage={value?.tertiaryLabel}
              thirdMessage={value?.quadroLabel}
            >
              <span
                className={classNames(styles.singleSelectItemLabel, {
                  [styles.error]: isSelectError,
                })}
                ref={baseTextRef}
              >
                {value?.label || ''}
              </span>
            </Tooltip>
          )}
        </>
      ),
      baseWithCategory: (
        <>
          {!!icon && (
            <SelectIcon
              icon={icon}
              iconStaticColor={iconStaticColor}
              isSelectError={isSelectError}
              isSelectFocused={isSelectFocused}
              isSelectDisabled={isSelectDisabled}
              isSelectView={isSelectView}
            />
          )}

          {!isValue ? (
            isSelectFocused && (
              <SelectPlaceholder
                placeholder={placeholder}
                isSelectError={isSelectError}
                height={height}
              />
            )
          ) : (
            <Tooltip
              tooltipWidth="max-content"
              message={baseWithCategoryTooltipMessage}
              secondaryMessage={value?.tertiaryLabel}
              subMessage={value?.subLabel}
              thirdMessage={value?.quadroLabel}
            >
              <span
                className={classNames(styles.singleSelectItemLabel, {
                  [styles.error]: isSelectError,
                })}
                ref={baseWithCategoryTextRef}
              >
                {value?.label || ''}
              </span>
            </Tooltip>
          )}
        </>
      ),
      bankAccount:
        !(!isSelectFocused && !isValue) &&
        (isSelectFocused && !isValue ? (
          <SelectPlaceholder
            placeholder={placeholder}
            isSelectError={isSelectError}
            height={height}
          />
        ) : (
          <TagSegmented
            className={styles.bankAccountTypeTag}
            mainLabel={value?.label || ''}
            secondLabel={
              value?.bankAccountType !== 'cash'
                ? handleAccountLength(value?.subLabel || '')
                : value?.subLabel || ''
            }
            noWrapSecondLabel={value?.bankAccountType === 'cash'}
            thirdLabel={value?.tertiaryLabel || ''}
            icon={value?.icon}
            tooltipMessage={
              <Container flexdirection="column">
                <Text color="white-100" style={{ wordBreak: 'break-all' }}>
                  {value?.label}
                </Text>
                <Text color="white-100" style={{ wordBreak: 'break-all' }}>
                  {value?.subLabel} | {value?.tertiaryLabel}
                </Text>
              </Container>
            }
          />
        )),
      column: (
        <>
          {!!icon && (
            <SelectIcon
              icon={icon}
              iconStaticColor={iconStaticColor}
              isSelectError={isSelectError}
              isSelectFocused={isSelectFocused}
              isSelectDisabled={isSelectDisabled}
              isSelectView={isSelectView}
            />
          )}

          {!isValue ? (
            isSelectFocused && (
              <SelectPlaceholder
                placeholder={placeholder}
                isSelectError={isSelectError}
                height={height}
              />
            )
          ) : (
            <div className={styles.singleSelectColumnValueWrapper}>
              {isValue &&
                (height === '36' ? (
                  <div className={styles.singleSelectColumnLabelWrapper}>
                    <Tooltip
                      tooltipWidth="max-content"
                      message={columnTypeMainLabelTooltipMessage}
                      innerLabelDirectionType="column"
                      subMessage={value?.subLabel}
                      secondaryMessage={value?.tertiaryLabel}
                      thirdMessage={value?.quadroLabel}
                    >
                      <span
                        ref={columnTypeMainLabelRef}
                        className={classNames(styles.singleSelectColumnLabel, {
                          [styles.error]: isSelectError,
                        })}
                      >
                        {value?.label || ''}
                      </span>
                    </Tooltip>
                    <Tooltip
                      tooltipWidth="max-content"
                      message={columnTypeSubLabelTooltipMessage && value?.label}
                      subMessage={columnTypeSubLabelTooltipMessage}
                      innerLabelDirectionType="column"
                      secondaryMessage={columnTypeSubLabelTooltipMessage && value?.tertiaryLabel}
                      thirdMessage={columnTypeSubLabelTooltipMessage && value?.quadroLabel}
                    >
                      <span
                        ref={columnTypeSubLabelRef}
                        className={classNames(styles.singleSelectColumnSubLabel, {
                          [styles.error]: isSelectError,
                        })}
                      >
                        {value?.subLabel || ''}
                      </span>
                    </Tooltip>
                  </div>
                ) : (
                  <Tooltip
                    tooltipWidth="max-content"
                    message={columnTypeMainLabelTooltipMessage}
                    innerLabelDirectionType="column"
                    subMessage={value?.subLabel}
                    secondaryMessage={value?.tertiaryLabel}
                    thirdMessage={value?.quadroLabel}
                  >
                    <span
                      ref={columnTypeMainLabelRef}
                      className={classNames(styles.singleSelectColumnLabel, {
                        [styles.error]: isSelectError,
                      })}
                    >
                      {value?.label || ''}
                    </span>
                  </Tooltip>
                ))}
            </div>
          )}
        </>
      ),
    }),
    [
      isValue,
      isSelectDisabled,
      isSelectView,
      isSelectError,
      isSelectFocused,
      placeholder,
      value,
      icon,
      baseTooltipMessage,
      baseWithCategoryTooltipMessage,
      columnTypeSubLabelTooltipMessage,
      columnTypeMainLabelTooltipMessage,
      height,
    ],
  );
};
