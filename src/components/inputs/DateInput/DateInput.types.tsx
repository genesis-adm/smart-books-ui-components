import { ReactDatePickerProps } from 'react-datepicker';

import { InputBaseProps, InputHeightType, InputWidthType } from '../../../types/inputs';

export type DateInputProps = ReactDatePickerProps & {
  height?: Extract<InputHeightType, '36' | '48'>;
  width: InputWidthType;
} & InputBaseProps;
