import { Meta, Story } from '@storybook/react';
import { DateInput as Component, DateInputProps as Props } from 'Components/inputs/DateInput/index';
import React, { useState } from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Inputs/DateInput',
  component: Component,
  argTypes: {
    className: hideProperty,
    error: hideProperty,
    tooltip: hideProperty,
    tooltipWidth: hideProperty,
  },
} as Meta;

const DateInputComponent: Story<
  Props & {
    isError: boolean;
    isPrefix: boolean;
  }
> = (args) => {
  const [startDate, setStartDate] = useState<Date | null>(null);
  const error = args?.isError ? { messages: ['Test error!!!'] } : undefined;

  return (
    <Component
      {...args}
      selected={startDate}
      onChange={(date: Date) => setStartDate(date)}
      error={error}
    />
  );
};

export const DateInput = DateInputComponent.bind({});
DateInput.args = {
  disabled: false,
  readOnly: false,
  isError: false,
  isLoading: false,
  required: true,
  height: '48',
  width: '260',
  label: 'Label of a datepicker',
  placeholderText: 'Choose date',
  background: 'grey-10',
  tooltipWidth: '350',
};
