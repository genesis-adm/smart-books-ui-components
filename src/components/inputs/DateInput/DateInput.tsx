import { Icon } from 'Components/base/Icon';
import InputBase from 'Components/inputs/Input/InputBase';
import { ReactComponent as CalendarIcon } from 'Svg/v2/16/calendar.svg';
import classNames from 'classnames';
import React, { ReactElement, useImperativeHandle, useRef } from 'react';
import DatePicker, { ReactDatePicker } from 'react-datepicker';

import styles from './DateInput.module.scss';
import { DateInputProps } from './DateInput.types';

const DateInput = React.forwardRef<ReactDatePicker, DateInputProps>(
  (
    {
      height = '48',
      width,
      className,
      error,
      tooltip,
      tooltipWidth,
      label,
      background,
      required,
      isLoading,
      ...datePickerProps
    },
    ref,
  ): ReactElement => {
    const {
      selected,
      disabled,
      readOnly,
      popperPlacement = 'auto',
      calendarStartDay = 1,
      dateFormat = 'yyyy-MM-dd',
      onChange,
    } = datePickerProps;

    const datePickerRef = useRef<ReactDatePicker>(null);
    useImperativeHandle(ref, () => datePickerRef?.current as ReactDatePicker);

    const isError = !!error?.messages?.filter((message) => !!message)?.length;

    const focus = (): void => {
      datePickerRef?.current?.setFocus();
    };

    const change = (
      date: Date | null,
      event: React.SyntheticEvent<any, Event> | undefined,
    ): void => {
      event?.stopPropagation();

      onChange?.(date, event);
    };

    return (
      <InputBase
        tag="DatePicker"
        height={height}
        width={width}
        label={label}
        background={background}
        required={required}
        error={error}
        icon={<Icon icon={<CalendarIcon />} path="inherit" clickThrough />}
        isLoading={isLoading}
        isFilled={!!selected}
        isError={isError}
        tooltip={tooltip}
        tooltipWidth={tooltipWidth}
        disabled={disabled}
        readOnly={readOnly}
        className={className}
        onFocus={focus}
      >
        {() => (
          <>
            <DatePicker
              ref={datePickerRef}
              {...datePickerProps}
              wrapperClassName={classNames(styles.pickerWrapper, styles[`height_${height}`], {
                [styles.disabled]: disabled,
                [styles.readOnly]: readOnly,
                [styles.error]: isError,
              })}
              className={classNames('react-datepicker__input', className)}
              portalId="tooltip"
              calendarClassName="react-datepicker_dropdown"
              popperClassName={classNames(styles.popperClassName, styles[`height_${height}`])}
              popperPlacement={popperPlacement}
              calendarStartDay={calendarStartDay}
              dateFormat={dateFormat}
              showMonthDropdown
              showYearDropdown
              useShortMonthInDropdown
              yearDropdownItemNumber={4}
              disabledKeyboardNavigation
              showPopperArrow={false}
              onChange={change}
            />
          </>
        )}
      </InputBase>
    );
  },
);

DateInput.displayName = 'DateInput';

export default DateInput;
