import { TagType } from 'Components/base/inputs/InputElements/InputTag';
import { useCallback, useMemo, useState } from 'react';

export default function useSearchExample() {
  const [searchValue, setSearchValue] = useState('');
  const [searchValuesList, setSearchValuesList] = useState<TagType[]>([]);

  const handleSearchValueDelete = useCallback(
    (value: string) =>
      setSearchValuesList((prevState) => {
        return prevState.filter((item: TagType) => item.id !== value);
      }),
    [],
  );

  const handleSearchValueAdd = useCallback(() => {
    if (!searchValuesList.find((item: TagType) => item.id === searchValue)) {
      setSearchValuesList((prevState) => [
        ...prevState,
        {
          id: searchValue,
          label: searchValue,
          onDelete: () => handleSearchValueDelete(searchValue),
        },
      ]);
      setSearchValue('');
    }
  }, [searchValue, searchValuesList, handleSearchValueDelete]);

  const handleClearAll = useCallback(() => {
    setSearchValuesList([]);
    setSearchValue('');
  }, []);

  return useMemo(
    () => ({ searchValue, setSearchValue, searchValuesList, handleSearchValueAdd, handleClearAll }),
    [searchValue, setSearchValue, searchValuesList, handleSearchValueAdd, handleClearAll],
  );
}
