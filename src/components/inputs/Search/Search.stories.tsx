import { Meta, Story } from '@storybook/react';
import { Search as Component, SearchProps as Props } from 'Components/inputs/Search/index';
import React from 'react';

import useSearchExample from './hooks/useSearchExample';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Inputs/Search',
  component: Component,
  argTypes: {
    // width: hideProperty,
    // height: hideProperty,
    name: hideProperty,
    className: hideProperty,
    value: hideProperty,
    searchParams: hideProperty,
    // tags: hideProperty,
    onChange: hideProperty,
    onSearch: hideProperty,
    onClear: hideProperty,
    onFocus: hideProperty,
    onBlur: hideProperty,
  },
} as Meta;

const SearchComponent: Story<Props> = (args) => {
  const { searchValue, setSearchValue, searchValuesList, handleSearchValueAdd, handleClearAll } =
    useSearchExample();

  return (
    <Component
      {...args}
      value={searchValue}
      onChange={(e) => setSearchValue(e.target.value)}
      searchParams={searchValuesList}
      onSearch={handleSearchValueAdd}
      onClear={handleClearAll}
    />
  );
};

export const Search = SearchComponent.bind({});
Search.args = {
  disabled: false,
  isLoading: false,
  placeholder: 'Search by ID and description',
  width: '320',
  height: '40',
  name: 'search-new',
};
