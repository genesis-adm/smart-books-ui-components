import { ContentLoader } from 'Components/base/ContentLoader';
import { Icon } from 'Components/base/Icon';
import { Tooltip } from 'Components/base/Tooltip';
import { InputTagWrapper } from 'Components/base/inputs/InputElements/InputTag';
import type { TagType } from 'Components/base/inputs/InputElements/InputTag';
import { ReactComponent as ClearIcon } from 'Svg/v2/16/cross-in-circle.svg';
import { ReactComponent as SearchIcon } from 'Svg/v2/16/search.svg';
import classNames from 'classnames';
import React, { useEffect, useState } from 'react';

import style from './Search.module.scss';

export interface SearchProps {
  name: string;
  width?: '320' | '350' | '360' | '440' | 'full';
  height?: '40' | '50';
  value?: string;
  placeholder?: string;
  disabled?: boolean;
  maxLength?: number;
  searchParams?: TagType[];
  tooltip?: React.ReactNode;
  className?: string;
  isLoading?: boolean;
  isFocused?: boolean;

  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  onSearch?: (value: string) => void;
  onFocus?: (event: React.FocusEvent<HTMLInputElement>) => void;
  onBlur?: (event: React.FocusEvent<HTMLInputElement>) => void;
  onClick?(): void;
  onClear?: () => void;
}

const Search: React.FC<SearchProps> = ({
  width = 'full',
  height = '40',
  name,
  value,
  placeholder = 'Search',
  disabled,
  searchParams = [],
  maxLength = 40,
  tooltip,
  className,
  onChange,
  onSearch,
  onFocus,
  onBlur,
  onClear,
  onClick,
  isLoading,
  isFocused = false,
}) => {
  const [focused, setFocused] = useState(false);
  const [hovered, setHovered] = useState(false);
  const notEmpty = (searchParams && searchParams.length > 0) || !!value;
  const searchActive = searchParams && searchParams?.length < 5;
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (searchActive) {
      onChange?.(e);
    }
  };
  const handleFocus = (e: React.FocusEvent<HTMLInputElement>) => {
    onFocus?.(e);
    setFocused(true);
  };
  const handleBlur = (e: React.FocusEvent<HTMLInputElement>) => {
    onBlur?.(e);
    setFocused(false);
  };
  const handleKeyDown = ({ key }: React.KeyboardEvent<HTMLInputElement>) => {
    if (key === 'Enter' && value && searchActive) {
      onSearch?.(value);
    }
  };

  const tooltipMessage = disabled ? tooltip && 'Disabled' : tooltip;

  useEffect(() => {
    setFocused(isFocused);
  }, [isFocused]);

  return (
    <Tooltip
      message={tooltipMessage}
      wrapperClassName={classNames(
        style.wrapper,
        style[`width_${width}`],
        style[`height_${height}`],
        {
          [style.hovered]: hovered,
          [style.focused]: focused,
          [style.withValues]: searchParams && searchParams.length > 0,
        },
        className,
      )}
      onMouseEnter={() => setHovered(true)}
      onMouseLeave={() => setHovered(false)}
      onClick={onClick}
    >
      <Icon
        className={style.icon}
        icon={<SearchIcon />}
        path="inherit"
        transition="inherit"
        clickThrough
      />
      {searchParams && <InputTagWrapper paddingBottom="0" tags={searchParams} />}
      <input
        type="text"
        id={name}
        autoComplete="off"
        maxLength={maxLength}
        placeholder={placeholder}
        className={classNames(style.input, { [style.full]: !searchActive })}
        disabled={disabled}
        readOnly={!searchActive}
        value={value}
        onChange={handleChange}
        onKeyDown={handleKeyDown}
        onFocus={handleFocus}
        onBlur={handleBlur}
      />
      <Icon
        className={classNames(style.reset, {
          [style.active]: notEmpty,
        })}
        icon={<ClearIcon />}
        cursor={notEmpty ? 'pointer' : 'default'}
        path="inherit"
        transition="inherit"
        onClick={onClear}
      />
      {isLoading && (
        <ContentLoader height="100%" isLoading={isLoading ?? false} isInsideComponent />
      )}
    </Tooltip>
  );
};

export default Search;
