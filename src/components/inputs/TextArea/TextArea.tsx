import { InputTagWrapper } from 'Components/base/inputs/InputElements/InputTag';
import InputBase from 'Components/inputs/Input/InputBase';
import classNames from 'classnames';
import React, { ReactElement, RefObject } from 'react';

import styles from './TextArea.module.scss';
import { TextAreaProps } from './TextArea.types';

const TextArea = React.forwardRef<HTMLTextAreaElement, TextAreaProps>(
  (
    {
      name,
      label,
      background = 'grey-10',
      align,
      error,
      minHeight = '88',
      tooltip,
      className,
      isLoading,
      counter = true,
      tooltipWidth,
      tags = [],
      ...textAreaProps
    },
    ref,
  ): ReactElement => {
    const { required, disabled, readOnly, value, maxLength = 200, icon } = textAreaProps;

    const isError = !!error?.messages?.filter((message) => !!message)?.length;

    return (
      <InputBase
        ref={ref}
        tag="textarea"
        minHeight={minHeight}
        label={label}
        background={background}
        required={required}
        error={error}
        isLoading={isLoading}
        isFilled={!!value}
        isError={isError}
        tooltip={tooltip}
        tooltipWidth={tooltipWidth}
        disabled={disabled}
        counter={counter}
        value={value}
        maxLength={maxLength !== 'none' ? maxLength : undefined}
        readOnly={readOnly}
        className={className}
        icon={icon}
      >
        {({ inputRef: textAreaRef }) => (
          <>
            {tags?.length && <InputTagWrapper paddingBottom="0" tags={tags} />}
            <textarea
              ref={textAreaRef as RefObject<HTMLTextAreaElement>}
              {...textAreaProps}
              id={name}
              autoComplete="off"
              required={false}
              maxLength={maxLength !== 'none' ? maxLength : undefined}
              className={classNames(styles.textarea, {
                [styles[`text-align_${align}`]]: align,
                [styles.disabled]: disabled,
                [styles.readOnly]: readOnly,
                [styles.error]: isError,
              })}
            />
          </>
        )}
      </InputBase>
    );
  },
);

TextArea.displayName = 'TextArea';

export default TextArea;
