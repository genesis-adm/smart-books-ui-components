import { Meta, Story } from '@storybook/react';
import { TextArea as Component, TextAreaProps as Props } from 'Components/inputs/TextArea/index';
import React, { useState } from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Inputs/TextArea',
  component: Component,
  argTypes: {
    className: hideProperty,
    name: hideProperty,
    error: hideProperty,
    onChange: hideProperty,
    tooltip: hideProperty,
    tooltipWidth: hideProperty,
  },
} as Meta;

const TextAreaComponent: Story<Props & { isError: boolean }> = (args) => {
  const [val, setVal] = useState('');

  const error = args?.isError ? { messages: ['Test error!!!'] } : undefined;

  return (
    <Component
      {...args}
      value={val}
      onChange={(e) => {
        setVal(e.target.value);
      }}
      error={error}
    />
  );
};

export const TextArea = TextAreaComponent.bind({});
TextArea.args = {
  disabled: false,
  readOnly: false,
  isError: false,
  isLoading: false,
  required: true,
  minHeight: '88',
  counter: true,
  name: 'textarea-storybook',
  label: 'Label',
  placeholder: 'Enter your note...',
  background: 'grey-10',
  tooltipWidth: '350',
  maxLength: 200,
  tags: [{ id: 'test', label: 'test', onDelete: () => {} }, { id: 'test1', label: 'test1', onDelete: () => {} }],
};
