import type { TagType } from 'Components/base/inputs/InputElements/InputTag';
import React, { ReactElement } from 'react';

import { InputBaseProps } from '../../../types/inputs';
import { TextAlign } from '../../types/fontTypes';

export type TextAreaMinHeight = '48' | '88' | '128' | '134' | '155';

export type TextAreaGeneralProps = {
  align?: TextAlign;
  minHeight?: TextAreaMinHeight;
  maxLength?: number | 'none';
  icon?: ReactElement;
  tags?: TagType[];
  onChange(e: React.ChangeEvent<HTMLTextAreaElement>): void;

  counter?: boolean;
};

export type TextAreaProps = InputBaseProps &
  TextAreaGeneralProps &
  Omit<React.InputHTMLAttributes<HTMLTextAreaElement>, 'maxLength'>;
