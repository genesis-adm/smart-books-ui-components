import InputBase from 'Components/inputs/Input/InputBase';
import classNames from 'classnames';
import React, { ReactElement, RefObject } from 'react';

import styles from './TextInput.module.scss';
import { TextInputProps } from './TextInput.types';

const TextInput = React.forwardRef<HTMLInputElement, TextInputProps>(
  (
    {
      background = 'grey-10',
      icon,
      iconFill,
      isIconColorStatic,
      label,
      height = '48',
      width = '280',
      error,
      isLoading,
      align,
      replaceContentComponent,
      suffix,
      tooltip,
      tooltipWidth,
      counter,
      autoResizable,
      className,
      ...inputProps
    },
    ref,
  ): ReactElement => {
    const { required, disabled, readOnly, value, onFocus, onBlur, maxLength = 200 } = inputProps;

    const isError = !!error?.messages?.filter((message) => !!message)?.length;
    const isFilled = !!value || !!replaceContentComponent;

    const inputFocus = (e: React.FocusEvent<HTMLInputElement>) => {
      onFocus?.(e);
    };

    const inputBlur = (e: React.FocusEvent<HTMLInputElement>) => {
      onBlur?.(e);
    };

    return (
      <InputBase
        ref={ref}
        tag="input"
        label={label}
        width={width}
        height={height}
        icon={icon}
        iconFill={iconFill}
        isIconColorStatic={isIconColorStatic}
        suffix={suffix}
        background={background}
        required={required}
        error={error}
        isLoading={isLoading}
        isFilled={isFilled}
        isError={isError}
        tooltip={tooltip}
        tooltipWidth={tooltipWidth}
        autoResizable={autoResizable}
        disabled={disabled}
        counter={counter && !replaceContentComponent}
        value={value}
        maxLength={maxLength}
        readOnly={readOnly || !!replaceContentComponent}
        className={className}
      >
        {({ inputRef }) => {
          return replaceContentComponent ? (
            replaceContentComponent
          ) : (
            <input
              ref={inputRef as RefObject<HTMLInputElement>}
              {...inputProps}
              autoComplete="off"
              onFocus={inputFocus}
              onBlur={inputBlur}
              required={false}
              maxLength={maxLength}
              className={classNames(styles.input, styles[`height_${height}`], {
                [styles[`text-align_${align}`]]: align,
                [styles.disabled]: disabled,
                [styles.readOnly]: readOnly,
                [styles.error]: isError,
              })}
            />
          );
        }}
      </InputBase>
    );
  },
);

TextInput.displayName = 'TextInput';

export default TextInput;
