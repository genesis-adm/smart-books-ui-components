import { Meta, Story } from '@storybook/react';
import { Icon } from 'Components/base/Icon';
import { TextInput as Component, TextInputProps as Props } from 'Components/inputs/TextInput/index';
import { ReactComponent as SearchIcon } from 'Svg/v2/16/search.svg';
import { ReactComponent as MastercardIcon } from 'Svg/v2/24/logo-mastercard.svg';
import React, { useState } from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Inputs/TextInput',
  component: Component,
  argTypes: {
    name: hideProperty,
    className: hideProperty,
    icon: hideProperty,
    suffix: hideProperty,
    isIconColorStatic: hideProperty,
  },
} as Meta;

const TextInputComponent: Story<
  Props & {
    isError: boolean;
    isPrefix: boolean;
    isSuffix: boolean;
  }
> = (args) => {
  const [val, setVal] = useState('');
  const error = args?.isError ? { messages: ['Test error!!!'] } : undefined;
  const icon = args?.isPrefix ? <SearchIcon /> : undefined;
  const suffix = args?.isSuffix ? (
    <Icon icon={<MastercardIcon />} clickThrough staticColor />
  ) : undefined;

  return (
    <Component
      {...args}
      value={val}
      onChange={(e) => {
        setVal(e.target.value);
      }}
      error={error}
      icon={icon}
      suffix={suffix}
    />
  );
};

export const TextInput = TextInputComponent.bind({});
TextInput.args = {
  disabled: false,
  readOnly: false,
  isError: false,
  isLoading: false,
  required: true,
  isPrefix: false,
  isSuffix: false,
  counter: true,
  autoResizable: false,
  name: 'text-input-storybook',
  width: '280',
  height: '48',
  label: 'Label',
  placeholder: 'Type...',
  background: 'grey-10',
  isIconColorStatic: false,
  tooltipWidth: '350',
  maxLength: 40,
};
