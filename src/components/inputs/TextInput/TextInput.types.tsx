import React, { ReactElement, ReactNode } from 'react';

import { InputBaseProps, InputHeightType, InputWidthType } from '../../../types/inputs';
import { BackgroundColorStaticNewTypes } from '../../types/colorTypes';
import { TextAlign } from '../../types/fontTypes';

export type TextInputOwnProps = {
  height?: InputHeightType;
  width?: InputWidthType;

  icon?: ReactElement | string;
  iconFill?: BackgroundColorStaticNewTypes;
  isIconColorStatic?: boolean;
  suffix?: ReactNode;

  counter?: boolean;
  autoResizable?: boolean;
};

export type TextInputProps = InputBaseProps &
  TextInputOwnProps & {
    align?: TextAlign;
    replaceContentComponent?: ReactElement;
  } & React.InputHTMLAttributes<HTMLInputElement>;
