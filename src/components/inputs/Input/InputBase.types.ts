import { InputHTMLAttributes, ReactElement, RefObject } from 'react';

import { InputBaseProps } from '../../../types/inputs';
import { TextAreaMinHeight } from '../../inputs/TextArea/TextArea.types';
import { TextInputOwnProps } from '../../inputs/TextInput/TextInput.types';

export type InputBaseOwnProps = Omit<InputBaseProps, 'name'> &
  TextInputOwnProps &
  Pick<InputHTMLAttributes<HTMLInputElement>, 'value' | 'disabled' | 'maxLength'> & {
    // general
    tag: 'input' | 'textarea' | 'DatePicker';
    required?: boolean;
    isFilled: boolean;
    isError: boolean;

    onFocus?: () => void;

    children: (args: {
      inputRef: RefObject<HTMLInputElement | HTMLTextAreaElement>;
    }) => ReactElement;

    // input
    autoResizable?: boolean;

    // textarea
    minHeight?: TextAreaMinHeight;
  };
