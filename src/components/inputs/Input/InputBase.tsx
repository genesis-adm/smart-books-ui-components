import { ContentLoader } from 'Components/base/ContentLoader';
import { Icon } from 'Components/base/Icon';
import InputErrorPopup from 'Components/base/inputs/InputErrorPopup/InputErrorPopup';
import { InputWrapper } from 'Components/base/inputs/InputWrapper';
import { Text } from 'Components/base/typography/Text';
import { InputBaseOwnProps } from 'Components/inputs/Input/InputBase.types';
import { useFocusNode } from 'Components/inputs/Select/hooks/useFocusNode';
import classNames from 'classnames';
import React, {
  ReactElement,
  useEffect,
  useImperativeHandle,
  useLayoutEffect,
  useRef,
  useState,
} from 'react';

import useResizeObserver from '../../../hooks/useResizeObserver';
import styles from './InputBase.module.scss';

const InputBase = React.forwardRef<HTMLInputElement | HTMLTextAreaElement, InputBaseOwnProps>(
  (
    {
      tag,
      width,
      height,
      minHeight,
      background,
      required,
      error,
      isFilled,
      isError,
      isLoading,
      disabled,
      label,
      icon,
      iconFill = 'inherit',
      isIconColorStatic,
      suffix,
      tooltip,
      tooltipWidth,
      counter,
      value,
      maxLength,
      autoResizable,
      children,
      readOnly,
      className,
      onFocus,
    },
    ref,
  ): ReactElement => {
    const inputRef = useRef<HTMLInputElement>(null);
    useImperativeHandle(ref, () => inputRef.current as HTMLInputElement);

    const [setInputObserverEntry, inputElement] = useResizeObserver();
    const inputRefElement = inputRef.current;

    const valueLength = value ? String(value)?.length : 0;
    const [tooltipValue, setTooltipValue] = useState<React.ReactNode>(tooltip);

    const { isFocused: focused, focus, blur } = useFocusNode();

    const focusElement = (): void => {
      focus();
      inputRef?.current?.focus();
      onFocus?.();
    };

    const click = (): void => {
      focusElement();
    };

    useEffect(() => {
      if (!inputRefElement || !!inputElement?.target) return;

      setInputObserverEntry(inputRefElement);
    }, [inputRefElement, setInputObserverEntry, inputElement]);

    useEffect(() => {
      if (tooltip) return;

      if (inputElement?.target?.clientWidth < inputElement?.target?.scrollWidth) {
        setTooltipValue(value);
      } else {
        setTooltipValue('');
      }
    }, [tooltip, value, inputElement?.target?.clientWidth, inputElement?.target?.scrollWidth]);

    useLayoutEffect(() => {
      if (tag === 'textarea' && inputRef.current) {
        inputRef.current.style.height = '100%';

        if (inputRef.current.scrollHeight > inputRef.current.clientHeight) {
          inputRef.current.style.height = inputRef.current.scrollHeight + 'px';
        }
      }
    }, [value, tag]);

    const isDialogTooltip = typeof icon !== 'string' && icon?.props.control;

    return (
      <InputWrapper
        isFocused={focused}
        isLoading={isLoading}
        isError={isError}
        width={width}
        height={height}
        minHeight={minHeight}
        onClick={click}
        onBlur={blur}
        disabled={disabled}
        readOnly={readOnly}
        background={background}
        style={{ cursor: 'auto' }}
        tooltip={tooltipValue}
        tooltipWidth={tooltipWidth}
        autoResizable={autoResizable}
        className={className}
      >
        {({ inputWrapperElement, isHovered: hovered }) => (
          <div
            className={classNames(
              styles.inputBase,
              styles[`tag_${tag}`],
              styles[`height_${height}`],
              styles[`minHeight_${minHeight}`],
              {
                [styles.filled]: isFilled,
                [styles.focused]: focused,
                [styles.hovered]: hovered,
                [styles.disabled]: disabled,
                [styles.view]: readOnly,
                [styles.error]: isError,
              },
            )}
          >
            {!!icon &&
              !isDialogTooltip &&
              (typeof icon === 'string' ? (
                <Text type="body-regular-14" color={iconFill} className={styles.symbol} noWrap>
                  {icon}
                </Text>
              ) : (
                <Icon
                  className={styles.icon}
                  icon={icon}
                  staticColor={isIconColorStatic}
                  path={iconFill}
                  cursor="text"
                  clickThrough
                />
              ))}

            {!!icon && isDialogTooltip && icon}

            <div className={styles.inputWithLabelWrapper}>
              <span className={styles.label}>
                {label}
                {required && <span className={styles.required}>*</span>}
              </span>

              <div className={classNames(styles.inputWrapper)}>{children({ inputRef })}</div>

              {!!counter && typeof maxLength === 'number' && !!valueLength && (
                <InputCounter valueLength={valueLength} maxLength={maxLength} />
              )}
            </div>

            {!!suffix && <div className={styles.suffix}>{suffix}</div>}

            {isLoading && (
              <ContentLoader height="100%" isLoading={isLoading ?? false} isInsideComponent />
            )}

            {isError && hovered && !focused && inputWrapperElement && (
              <InputErrorPopup
                inputWrapperElement={inputWrapperElement}
                errorMessage={error?.messages}
              />
            )}
          </div>
        )}
      </InputWrapper>
    );
  },
);

InputBase.displayName = 'InputBase';

export default InputBase;

type InputCounterProps = {
  valueLength: number;
  maxLength: number;
};

const InputCounter = ({ valueLength, maxLength }: InputCounterProps): ReactElement => {
  return (
    <span className={styles.counter}>
      <span className={styles.valueLength}>{valueLength}</span>/{maxLength}
    </span>
  );
};
