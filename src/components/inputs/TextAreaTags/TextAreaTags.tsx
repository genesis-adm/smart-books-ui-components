import { Divider } from 'Components/base/Divider';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { TagsEditable } from 'Components/custom/Users/EmailNotesInput/TagsEditable';
import { areLabelsDifferent } from 'Components/inputs/TextAreaTags/utils';
import classNames from 'classnames';
import React, { useCallback, useMemo, useRef, useState } from 'react';

import { TagType } from '../../custom/Users/EmailNotesInput/TagsEditable/TagsEditable.types';
import styles from './TextAreaTags.module.scss';
import { TextAreaTagsProps } from './TextAreaTags.types';

const TextAreaTags = ({
  value = [],
  search,
  placeholder,
  allowSpaces,
  onSave,
  onCancel,
  onChange,
  onFocus,
  validateValues,
}: TextAreaTagsProps): React.ReactElement => {
  const inputRef = useRef<HTMLInputElement>(null);
  const [isFocus, setIsFocus] = useState<boolean>(false);
  const [isHovered, setIsHovered] = useState(false);
  const [tags, setTags] = useState<TagType[]>(value);
  const [defaultValue, setDefaultValue] = useState<TagType[]>(value);

  const isError = tags?.some(({ error }) => error?.isError);
  const isDirty = useMemo(() => areLabelsDifferent(tags, defaultValue), [defaultValue, tags]);

  const handleClick = () => {
    setIsFocus(true);
    setTimeout(() => inputRef.current?.focus(), 100);
    onFocus?.();
  };

  const handleCancel = (e: React.MouseEvent<HTMLButtonElement>) => {
    e.stopPropagation();
    setIsFocus(false);
    inputRef.current?.blur();

    setDefaultValue([...defaultValue]);
    setTags([...defaultValue]);
    onCancel?.();
  };

  const handleSave = (e: React.MouseEvent<HTMLButtonElement>) => {
    e.stopPropagation();
    setDefaultValue([...tags]);
    onSave(tags);
    setIsFocus(false);
  };

  const handleChange = useCallback(
    (tags: TagType[]) => {
      setTags(tags);
      inputRef.current?.focus();
      onChange?.(tags);
    },
    [onChange],
  );

  const hover = () => {
    setIsHovered(true);
  };

  const leave = () => {
    setIsHovered(false);
  };

  return (
    <Container
      background={'grey-10'}
      className={classNames(styles.container, { [styles.focus]: isFocus })}
      onClick={handleClick}
      onMouseEnter={hover}
      onMouseLeave={leave}
    >
      <Container className={styles.textarea}>
        <TagsEditable
          validateValues={validateValues}
          ref={inputRef}
          value={defaultValue}
          search={search}
          onChange={handleChange}
          paddingBottom="0"
          placeholder={placeholder}
          tagColor={isHovered && !isFocus ? 'grey_30' : 'grey'}
          tagWidth="auto"
          allowSpaces={allowSpaces}
          readOnly={!isFocus}
          autoFocus={isFocus}
        />
      </Container>
      <Container className={classNames(styles.footer, { [styles.visible]: isFocus })}>
        <Divider fullHorizontalWidth />
        {isFocus && (
          <Container className={classNames(styles.actions)}>
            <Button
              background={'white-100'}
              height="32"
              width="64"
              color="black-90"
              font="text-regular"
              border="grey-40"
              onClick={handleCancel}
            >
              Cancel
            </Button>
            <Button
              color="white-100"
              height="32"
              width="64"
              font="text-regular"
              disabled={isError || !isDirty}
              onClick={handleSave}
            >
              Save
            </Button>
          </Container>
        )}
      </Container>
    </Container>
  );
};

export default TextAreaTags;
