import { TagType } from 'Components/custom/Users/EmailNotesInput/TagsEditable/TagsEditable.types';

export const areLabelsDifferent = (array1: TagType[], array2: TagType[]) => {
  const labels1 = new Set(array1.map((tag) => tag.label));
  const labels2 = new Set(array2.map((tag) => tag.label));

  if (labels1.size !== labels2.size) return true;

  for (const label of labels1) {
    if (!labels2.has(label)) return true;
  }

  return false;
};
