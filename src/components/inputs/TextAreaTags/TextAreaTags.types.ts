import { TagType } from '../../custom/Users/EmailNotesInput/TagsEditable/TagsEditable.types';

export type TextAreaTagsProps = {
  value: TagType[];
  search?: string;
  placeholder?: string;
  allowSpaces?: boolean;
  onCancel?: () => void;
  onSave: (tags: TagType[]) => void;
  onChange?: (tags: TagType[]) => void;
  onFocus?: () => void;
  validateValues?: (tags: TagType[]) => TagType[];
};
