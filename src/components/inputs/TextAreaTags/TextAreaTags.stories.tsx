import { Meta, Story } from '@storybook/react';
import { TagType } from 'Components/custom/Users/EmailNotesInput/TagsEditable/TagsEditable.types';
import {
  TextAreaTags as Component,
  TextAreaTagsProps as Props,
} from 'Components/inputs/TextAreaTags/index';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Inputs/TextAreaTags',
  component: Component,
  argTypes: {
    onSave: hideProperty,
    onCancel: hideProperty,
    validateValues: hideProperty,
  },
} as Meta;

const TextAreaTagsComponent: Story<Props> = (args) => {
  const [value, setValue] = React.useState<TagType[]>([{ id: 1, label: 'test' }]);
  return (
    <>
      <Component {...args} validateValues={validateTags} value={value} />
    </>
  );
};

export const TextAreaTags = TextAreaTagsComponent.bind({});
TextAreaTags.args = {};

const validateTags = (tags: TagType[]): TagType[] => {
  const duplicate = new Map<string, number>();

  tags.forEach((tag) => {
    const normalizedLabel = tag.label.trim().toLowerCase();
    duplicate.set(normalizedLabel, (duplicate.get(normalizedLabel) ?? 0) + 1);
  });

  return tags.map((tag) => {
    const normalizedLabel = tag.label.trim().toLowerCase();

    const isError = Number(duplicate.get(normalizedLabel)) > 1;

    return {
      ...tag,
      error: {
        isError,
        message: isError
          ? 'This keyword already exists in this category. Please enter a unique keyword.'
          : undefined,
      },
    };
  });
};
