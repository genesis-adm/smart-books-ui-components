import React from 'react';

import ScrollbarWrapper from './ScrollbarWrapper';

export interface ScrollbarProps {
  type: 'modal' | 'table';
  tableHead?: React.ReactNode;
  disabled?: boolean;
  className?: string;
  children?: React.ReactNode;
}

const Scrollbar: React.FC<ScrollbarProps> = ({
  type,
  tableHead,
  disabled = false,
  className,
  children,
}) => (
  <>
    {!disabled && (
      <ScrollbarWrapper
        className={className}
        contentInViewPort={tableHead}
        options={{
          sizeAutoCapable: false,
          scrollbars: {
            autoHide: type === 'table' ? 'never' : 'scroll',
          },
        }}
      >
        {children}
      </ScrollbarWrapper>
    )}
    {disabled && children}
  </>
);

export default Scrollbar;
