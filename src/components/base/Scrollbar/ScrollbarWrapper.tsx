import classNames from 'classnames';
import OverlayScrollbars from 'overlayscrollbars';
import React, { useEffect, useRef } from 'react';

import './Scrollbar.css';

export interface ScrollbarWrapperProps extends React.HTMLAttributes<HTMLDivElement> {
  className?: string;
  contentInViewPort?: React.ReactNode;
  children?: React.ReactNode;
  options?: OverlayScrollbars.Options;
  extensions?: OverlayScrollbars.Extensions;
}

const ScrollbarWrapper: React.FC<ScrollbarWrapperProps> = ({
  className,
  contentInViewPort,
  children,
  options,
  extensions,
}) => {
  const osTargetRef = useRef(null);
  const osInstance = useRef<OverlayScrollbars | null>(null);
  const osTarget = (): HTMLDivElement | null => osTargetRef.current || null;
  useEffect(() => {
    osInstance.current = OverlayScrollbars(osTarget() as HTMLElement, options || {}, extensions);
    if (OverlayScrollbars.valid(osInstance)) {
      osInstance?.current?.options(options as OverlayScrollbars.Options);
    }
    return () => {
      if (OverlayScrollbars.valid(osInstance)) {
        osInstance?.current?.destroy();
        osInstance.current = null;
      }
    };
  }, [options, extensions]);
  return (
    <div
      className={classNames('os-host os-host-flexbox os-theme-dark', className)}
      ref={osTargetRef}
    >
      <div className="os-resize-observer-host" />
      <div className="os-padding">
        <div className="os-viewport">
          {contentInViewPort}
          <div className="os-content">{children}</div>
        </div>
      </div>
      <div className="os-scrollbar os-scrollbar-horizontal ">
        <div className="os-scrollbar-track">
          <div className="os-scrollbar-handle" />
        </div>
      </div>
      <div className="os-scrollbar os-scrollbar-vertical">
        <div className="os-scrollbar-track">
          <div className="os-scrollbar-handle" />
        </div>
      </div>
      <div className="os-scrollbar-corner" />
    </div>
  );
};

export default ScrollbarWrapper;
