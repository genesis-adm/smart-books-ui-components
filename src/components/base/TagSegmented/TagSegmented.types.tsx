import React, { CSSProperties, ReactNode } from 'react';

import { TestableProps } from '../../../types/general';
import { TooltipWidth } from '../../../types/tooltips';

type InnerTooltipMessage = {
  message: string;
  secondaryMessage?: string;
  thirdMessage?: string;
  subMessage?: string;
  tooltipWidth?: TooltipWidth;
};

export type TagSegmentedBackground = 'grey-20' | 'grey-30' | 'blue-10';
export interface TagSegmentedProps extends TestableProps {
  id?: string;
  mainLabel: string;
  secondLabel?: string;
  thirdLabel?: string;
  icon?: ReactNode;
  iconStaticColor?: boolean;
  tooltipMessage?: ReactNode;
  complexTooltipMessage?: InnerTooltipMessage;
  tooltipInnerClassName?: string;
  deleteIcon?: boolean;
  showCounter?: boolean;
  counter?: ReactNode;
  background?: TagSegmentedBackground;
  className?: string;
  noWrapSecondLabel?: boolean;
  style?: CSSProperties;
  onClick?: (e: React.MouseEvent<HTMLElement>) => void;
  onDelete?: (id: string) => void;
  onChangeBounding?: (domRect: DOMRect, id: string) => void;
}
