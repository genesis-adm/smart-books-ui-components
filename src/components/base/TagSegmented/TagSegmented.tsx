import { Tooltip } from 'Components/base/Tooltip';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as CloseIcon } from 'Svg/v2/16/cross.svg';
import classNames from 'classnames';
import React, { ReactElement, createContext, useContext, useEffect, useRef } from 'react';

import { useBounding } from '../../../hooks/useBounding';
import { useTooltipValue } from '../../../hooks/useTooltipValue';
import Icon from '../Icon/Icon';
import styles from './TagSegmented.module.scss';
import { TagSegmentedBackground, TagSegmentedProps } from './TagSegmented.types';

const TagSegmentedContext = createContext<{ background: TagSegmentedBackground }>({
  background: 'grey-20',
});

const TagSegmented = ({
  id,
  mainLabel,
  secondLabel,
  thirdLabel,
  tooltipMessage,
  complexTooltipMessage,
  tooltipInnerClassName,
  counter,
  showCounter,
  iconStaticColor,
  icon,
  deleteIcon,
  background = 'grey-20',
  className,
  noWrapSecondLabel,
  style,
  onClick,
  onDelete,
  onChangeBounding,
  dataTestId,
}: TagSegmentedProps): ReactElement => {
  const { ref, domRect } = useBounding<HTMLDivElement>();

  const mainTextRef = useRef<HTMLDivElement>(null);
  const mainText = useTooltipValue(mainTextRef, mainLabel);
  const tooltipValue = tooltipMessage ? tooltipMessage : mainText;
  const handleClick = (e: React.MouseEvent<HTMLElement>) => {
    onClick && onClick(e);
  };

  const handleDelete = () => {
    onDelete && onDelete(id ? id : '');
  };

  useEffect(() => {
    if (!domRect) return;

    onChangeBounding?.(domRect, id ? id : '');
  }, [onChangeBounding, domRect, id]);

  return (
    <TagSegmentedContext.Provider value={{ background: background }}>
      <div className={styles.container} data-testid={dataTestId}>
        <Tooltip
          {...complexTooltipMessage}
          ref={ref}
          message={tooltipValue || complexTooltipMessage?.message}
          innerClassName={tooltipInnerClassName}
          cursor="pointer"
          wrapperClassName={classNames(
            styles.wrapper,
            styles[`background_${background}`],
            {
              [styles.multiple]: showCounter,
            },
            className,
          )}
          style={style}
          onClick={handleClick}
        >
          {icon && <Icon icon={icon} path="inherit" staticColor={iconStaticColor} />}
          <Text color="inherit" ref={mainTextRef} noWrap className={styles.main_label}>
            {mainLabel}
          </Text>
          {secondLabel && (
            <Text
              color="inherit"
              noWrap={noWrapSecondLabel}
              className={noWrapSecondLabel ? styles.main_label : undefined}
            >
              | {secondLabel}
            </Text>
          )}
          {thirdLabel && <Text color="inherit">| {thirdLabel}</Text>}
          {deleteIcon && (
            <Icon
              icon={<CloseIcon />}
              path="inherit"
              staticColor={iconStaticColor}
              cursor="pointer"
              onClick={handleDelete}
            />
          )}
        </Tooltip>
        {showCounter && !!counter && counter}
      </div>
    </TagSegmentedContext.Provider>
  );
};

type CounterProps = { className?: string; counterValue: number; onClick?(): void };
const Counter = ({ className, counterValue, onClick }: CounterProps) => {
  const { background } = useContext(TagSegmentedContext);
  return (
    <div
      className={classNames(styles.counter, styles[`background_${background}`], className)}
      onClick={onClick}
    >
      <span className={styles.divider} />
      <Text color="inherit">+{counterValue}</Text>
    </div>
  );
};

export default TagSegmented;

TagSegmented.Counter = Counter;
