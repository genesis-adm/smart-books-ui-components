import { Meta, Story } from '@storybook/react';
import { DropDown } from 'Components/DropDown';
import Icon from 'Components/base/Icon/Icon';
import {
  TagSegmented as Component,
  TagSegmentedProps as Props,
} from 'Components/base/TagSegmented';
import { Tooltip } from 'Components/base/Tooltip';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as SuitcaseIcon } from 'Svg/16/suitcase-dollar.svg';
import handleAccountLength from 'Utils/handleAccountLength';
import React from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/TagSegmented',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const TagSegmentedMultipleTemplate: Story<Props> = (args) => (
  <Component
    {...args}
    counter={
      <DropDown
        padding={'16'}
        control={({ handleOpen }) => (
          <Tooltip message="Show all">
            <Component.Counter counterValue={233} onClick={handleOpen} />
          </Tooltip>
        )}
      >
        <Container flexdirection="column" gap="12" className={spacing.h300max} customScroll>
          <Text type="text-regular" color="grey-100">
            Cards
          </Text>
          {Array.from(Array(3)).map((_) => (
            <Container flexdirection="row" gap="8" alignitems="center">
              <Icon icon={<SuitcaseIcon />} />
              <Container flexdirection="column">
                <Text type="text-regular">{args.mainLabel}</Text>
                <Text type="text-regular" color="grey-100">
                  {`${handleAccountLength(args?.secondLabel ? args.secondLabel : '', 8)} | `}
                  {args?.thirdLabel?.toUpperCase()}
                </Text>
              </Container>
            </Container>
          ))}
        </Container>
      </DropDown>
    }
  />
);

const TagSegmentedTemplate: Story<Props> = (args) => (
  <Component
    {...args}
    tooltipMessage={
      <Container flexdirection="column">
        <Text color="white-100" style={{ wordBreak: 'break-all' }}>
          {args.mainLabel}
        </Text>
        <Text color="white-100" style={{ wordBreak: 'break-all' }}>
          {args.secondLabel} | {args.thirdLabel}
        </Text>
      </Container>
    }
  />
);

const TagSegmentedWithDeleteTemplate: Story<Props> = (args) => (
  <Component
    {...args}
    tooltipMessage={
      <Container flexdirection="column">
        <Text color="white-100" style={{ wordBreak: 'break-all' }}>
          {args.mainLabel}
        </Text>
        <Text color="white-100" style={{ wordBreak: 'break-all' }}>
          {args.secondLabel} | {args.thirdLabel}
        </Text>
      </Container>
    }
  />
);
export const TagSegmented = TagSegmentedTemplate.bind({});
TagSegmented.args = {
  mainLabel: 'BOSNA BANK INTERNATIONAL DD',
  secondLabel: handleAccountLength('AT483200000012345864', 8),
  thirdLabel: 'UAH',
  icon: <SuitcaseIcon />,
};

export const TagSegmentedWithDelete = TagSegmentedWithDeleteTemplate.bind({});
TagSegmentedWithDelete.args = {
  mainLabel: 'BOSNA BANK INTERNATIONAL DD',
  secondLabel: handleAccountLength('AT483200000012345864', 8),
  thirdLabel: 'UAH',
  icon: <SuitcaseIcon />,
  deleteIcon: true,
};

export const TagSegmentedMultiple = TagSegmentedMultipleTemplate.bind({});
TagSegmentedMultiple.args = {
  mainLabel: 'BOSNA BANK INTERNATIONAL DD',
  secondLabel: handleAccountLength('AT483200000012345864', 8),
  thirdLabel: 'UAH',
  showCounter: true,

  tooltipMessage: 'Show all',
  icon: <SuitcaseIcon />,
};
