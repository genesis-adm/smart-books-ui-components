import { Meta, Story } from '@storybook/react';
import { Switch as Component, SwitchProps as Props } from 'Components/base/Switch';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Switch',
  component: Component,
  argTypes: {
    id: hideProperty,
    onChange: hideProperty,
    className: hideProperty,
  },
} as Meta;

const SwitchComponent: Story<Props> = (args) => <Component {...args} />;

export const Switch = SwitchComponent.bind({});
Switch.args = {
  checked: true,
  disabled: false,
  id: 'switch',
};
