import classNames from 'classnames';
import React, { ReactElement } from 'react';

import { TestableProps } from '../../../types/general';
import style from './Switch.module.scss';

export interface SwitchProps extends TestableProps {
  id: string;
  checked?: boolean;
  disabled?: boolean;
  className?: string;
  onChange?(checked: boolean): void;
}

const Switch = ({
  className,
  checked = false,
  disabled,
  onChange,
  id,
  dataTestId,
}: SwitchProps): ReactElement => {
  const handleChange = ({ target }: React.ChangeEvent<HTMLInputElement>) => {
    if (disabled) return;
    onChange?.(target.checked);
  };

  return (
    <input
      id={id}
      data-testid={dataTestId}
      className={classNames(style.switch, className, {
        [style.disabled]: disabled,
      })}
      type="checkbox"
      checked={checked}
      onChange={handleChange}
    />
  );
};

export default Switch;
