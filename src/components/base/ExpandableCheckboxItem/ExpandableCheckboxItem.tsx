import { Checkbox } from 'Components/base/Checkbox';
import { Icon } from 'Components/base/Icon';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as ChevronDown } from 'Svg/v2/16/chevron-down.svg';
import { ReactComponent as ChevronUp } from 'Svg/v2/16/chevron-up.svg';
import classNames from 'classnames';
import React, { ReactElement, ReactNode } from 'react';

import { useToggle } from '../../../hooks/useToggle';
import style from './ExpandableCheckboxItem.module.scss';
import { ExpandableCheckboxItemProps } from './ExpandableCheckboxItem.types';

// Reminder: do not forget to add the following code to root index.ts
// export { ExpandableCheckboxItem } from './components/base/ExpandableCheckboxItem';

const ExpandableCheckboxItem = ({
  id,
  checkboxName,
  onCheckboxChange,
  checked,
  className,
  children,
  showCollapseBtn,
  collapseBtnTitle,
  showBadge,
  itemType,
  onToggle,
}: ExpandableCheckboxItemProps): ReactElement => {
  const { isOpen: isShownCollapsedItems, onToggle: onToggleCollapseBtn } = useToggle();

  const collapsedBtnTooltip = isShownCollapsedItems
    ? `Hide ${itemType ? itemType : ''}`
    : `Show ${itemType ? itemType : ''}`;

  const handleToggle = () => {
    onToggleCollapseBtn();
    onToggle?.();
  };

  return (
    <div className={style.container}>
      <div className={classNames(style.component, className)}>
        <Checkbox
          name={id.toString()}
          checked={checked}
          onChange={onCheckboxChange}
          font={'text-regular'}
          textcolor={checked ? 'violet-90' : 'grey-100'}
          noWrap
        >
          <div className={style.itemNameContainer}>{checkboxName}</div>
        </Checkbox>
        {showCollapseBtn && (
          <div className={style.collapseBtnContainer}>
            <IconButton
              tooltip={collapsedBtnTooltip}
              icon={isShownCollapsedItems ? <ChevronUp /> : <ChevronDown />}
              size="24"
              color="grey-100"
              background={'transparent-grey-30'}
              onClick={handleToggle}
            />
            <Text color={'grey-90'} type="subtext-regular">
              {collapseBtnTitle}
            </Text>
            {showBadge && !isShownCollapsedItems && <div className={style.badge} />}
          </div>
        )}
      </div>
      {children && isShownCollapsedItems && children}
    </div>
  );
};

export type ExpandableCheckboxGroupProps = {
  children: ReactNode;
  groupName?: string;
  groupIcon?: ReactNode;
  firstChild: boolean;

  className?: string;
};
const ExpandableCheckboxGroup = ({
  groupName,
  groupIcon,
  children,
  firstChild,
  className,
}: ExpandableCheckboxGroupProps) => {
  return (
    <div className={classNames(style.group, className)}>
      {groupName && firstChild && (
        <div className={style.groupContainer}>
          <span className={style.connector} />
          <div className={style.groupTitle}>
            <Icon icon={groupIcon} />
            <Text type="text-regular" color="grey-90">
              {groupName}
            </Text>
          </div>
        </div>
      )}
      {children}
    </div>
  );
};

export { ExpandableCheckboxItem, ExpandableCheckboxGroup };
