import { Meta, Story } from '@storybook/react';
import {
  ExpandableCheckboxItem as Component,
  ExpandableCheckboxItemProps as Props,
} from 'Components/base/ExpandableCheckboxItem';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/base/Expandable Checkbox/ExpandableCheckboxItem',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const ExpandableCheckboxItemComponent: Story<Props> = (args) => <Component {...args} />;

export const ExpandableCheckboxItem = ExpandableCheckboxItemComponent.bind({});
ExpandableCheckboxItem.args = { checkboxName: 'Checkbox name' };
