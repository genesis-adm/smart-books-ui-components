import { ReactNode } from 'react';

export interface ExpandableCheckboxItemProps {
  id: string | number;
  checkboxName: string | ReactNode;
  checked: boolean;
  onCheckboxChange(): void;

  itemType?: string;

  showCollapseBtn: boolean;
  collapseBtnTitle?: string;
  showBadge?: boolean;
  setShowBadge?(arg: boolean): void;

  children?: ReactNode;
  className?: string;
  onToggle?(): void;
}
