import React, { useEffect } from 'react';
import { createPortal } from 'react-dom';

export interface PortalProps {
  rootElemId?: string;
  tag?: string;
  position?: 'relative' | 'absolute';
  children?: React.ReactNode;
}

const Portal: React.FC<PortalProps> = ({
  children,
  tag = 'div',
  position = 'relative',
  rootElemId,
}) => {
  const element: HTMLElement = document.createElement(tag);
  element.setAttribute('style', `height: 100%; width: 100%; top: 0; position: ${position};`);
  const bodyElem = document.body as HTMLElement;

  const rootElem = (rootElemId && document.getElementById(rootElemId)) || bodyElem;

  useEffect(() => {
    rootElem.appendChild(element);

    return () => {
      rootElem.removeChild(element);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return createPortal(children, rootElem);
};

export default Portal;
