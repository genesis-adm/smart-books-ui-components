import classNames from 'classnames';
import React, { CSSProperties, InputHTMLAttributes, useState } from 'react';

import { TestableProps } from '../../../types/general';
import { TooltipWidth } from '../../../types/tooltips';
import styles from './Tooltip.module.scss';
import Portal from './TooltipPortal';

export interface TooltipProps extends TestableProps, InputHTMLAttributes<HTMLDivElement> {
  message: string | React.ReactNode;
  secondaryMessage?: string | React.ReactNode;
  thirdMessage?: string | React.ReactNode;
  subMessage?: string | React.ReactNode;
  innerLabelDirectionType?: 'column';
  tooltipWidth?: TooltipWidth;
  cursor?: 'default' | 'pointer' | 'move' | 'help' | 'text' | 'inherit';
  innerClassName?: string;
  wrapperClassName?: string;
  children?: React.ReactNode;
  style?: CSSProperties;
  onClick?: (e: React.MouseEvent<HTMLDivElement>) => void;
  onMouseEnter?: () => void;
  onMouseLeave?: () => void;
}

const Tooltip = React.forwardRef<HTMLDivElement, TooltipProps>(
  (
    {
      message,
      tooltipWidth = '350',
      cursor,
      innerClassName,
      wrapperClassName,
      onClick,
      style,
      onMouseEnter,
      onMouseLeave,
      children,
      secondaryMessage,
      thirdMessage,
      subMessage,
      innerLabelDirectionType,
      dataTestId,
    },
    ref,
  ) => {
    const [isActive, setActive] = useState(false);
    const [positionTop, setPositionTop] = useState('unset');
    const [positionBottom, setPositionBottom] = useState('unset');
    const [positionLeft, setPositionLeft] = useState('unset');
    const [positionRight, setPositionRight] = useState('unset');
    const mouseEnterHandler = (e: React.MouseEvent<HTMLElement>) => {
      if (
        e.clientX * 2 >
        (window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth)
      ) {
        setPositionLeft('unset');
        setPositionRight(
          `${
            (window.innerWidth ||
              document.documentElement.clientWidth ||
              document.body.clientWidth) -
            e.clientX +
            10
          }px`,
        );
      } else {
        setPositionRight('unset');
        setPositionLeft(`${e.clientX + 10}px`);
      }
      if (
        e.clientY * 2 >
        (window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight)
      ) {
        setPositionTop('unset');
        setPositionBottom(
          `${
            (window.innerHeight ||
              document.documentElement.clientHeight ||
              document.body.clientHeight) -
            e.clientY +
            10
          }px`,
        );
      } else {
        setPositionBottom('unset');
        setPositionTop(`${e.clientY + 10}px`);
      }
      setActive(true);
      onMouseEnter?.();
    };
    const mouseLeaveHandler = () => {
      setPositionTop('unset');
      setPositionBottom('unset');
      setPositionLeft('unset');
      setPositionRight('unset');
      setActive(false);
      onMouseLeave?.();
    };
    return (
      <>
        <div
          role="presentation"
          className={classNames(
            styles.wrapper,
            { [styles[`cursor_${cursor}`]]: cursor },
            wrapperClassName,
          )}
          style={style}
          onMouseEnter={mouseEnterHandler}
          onMouseMove={mouseEnterHandler}
          onMouseLeave={mouseLeaveHandler}
          onClick={onClick}
          ref={ref}
        >
          {children}
        </div>
        {isActive && message && (
          <Portal>
            <div
              className={classNames(
                styles.tooltip,
                styles[`width_${tooltipWidth}`],
                innerClassName,
              )}
              style={{
                top: `${positionTop}`,
                bottom: `${positionBottom}`,
                left: `${positionLeft}`,
                right: `${positionRight}`,
              }}
              data-testid={dataTestId}
            >
              <div className={classNames(styles.content_container)}>
                <div
                  className={classNames(styles.text_content, {
                    [styles.text_width]: subMessage && innerLabelDirectionType !== 'column',
                  })}
                >
                  <div className={styles.primary_text_color}>{message}</div>
                  {subMessage && innerLabelDirectionType === 'column' && (
                    <div className={styles.secondary_text_color}>{subMessage}</div>
                  )}
                  {secondaryMessage && (
                    <div className={styles.secondary_text_color}>{secondaryMessage}</div>
                  )}
                  {thirdMessage && (
                    <div className={styles.secondary_text_color}>{thirdMessage}</div>
                  )}
                </div>
                {subMessage && innerLabelDirectionType !== 'column' && (
                  <div className={styles.secondary_text_color}>{subMessage}</div>
                )}
              </div>
            </div>
          </Portal>
        )}
      </>
    );
  },
);

Tooltip.displayName = 'Tooltip';

export default Tooltip;
