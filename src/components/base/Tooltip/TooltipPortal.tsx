import React from 'react';
import { createPortal } from 'react-dom';

const tooltipRoot = document.getElementById('tooltip') as HTMLElement;

interface TooltipPortalProps {
  children?: React.ReactNode;
}

const TooltipPortal: React.FC<TooltipPortalProps> = ({ children }) =>
  createPortal(children, tooltipRoot);

export default TooltipPortal;
