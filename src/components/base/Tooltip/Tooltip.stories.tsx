import { Tooltip as Component } from 'Components/base/Tooltip';
import { Container } from 'Components/base/grid/Container';
import React from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

export default {
  title: 'Components/Base/Tooltip',
};

export const Tooltip: React.FC = () => (
  <Container className={spacing.mt80} justifycontent="space-evenly" alignitems="center">
    <Component message="This is a tooltip message with a very very very very very very very very very very very very very very very loooooooooong text that can be wrapped automatically">
      Hover me! (loooong)
    </Component>
    <Component message="This is a tooltip message">Hover me! (short)</Component>
  </Container>
);
