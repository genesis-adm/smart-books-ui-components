import { Meta, Story } from '@storybook/react';
import { DatePicker as Component } from 'Components/base/DatePicker';
import {
  DatePickerDateRange,
  DatePickerSingleDate,
  DatePickerProps as Props,
} from 'Components/base/DatePicker/DatePicker.types';
import React, { useState } from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/DatePicker',
  component: Component,
  argTypes: {
    value: hideProperty,
    className: hideProperty,
    onChange: hideProperty,
    initialMode: hideProperty,
    withRange: {
      type: 'boolean',
    },
    withPresets: {
      type: 'boolean',
    },
    singleDateProps: hideProperty,
    name: hideProperty,
    calendarStartDay: hideProperty,
  },
} as Meta;

const DatePickerComponent: Story<Props> = ({ withRange = false as boolean, ...args }) => {
  const [singleDate, setSingleDate] = useState<DatePickerSingleDate>(null);
  const [dateRange, setDateRange] = useState<DatePickerDateRange>([null, null]);

  const [startDate, endDate] = dateRange;

  return (
    <div
      style={{
        display: 'inline-flex',
        borderRadius: '10px',
        border: '1px solid #EDEFF1',
        background: '#FFF',
        boxShadow: '0px 10px 30px 0px rgba(192, 196, 205, 0.30)',
        overflow: 'hidden',
      }}
    >
      {!withRange ? (
        <Component {...args} selected={singleDate} onChange={setSingleDate} />
      ) : (
        <Component
          {...args}
          withRange
          startDate={startDate}
          endDate={endDate}
          onChange={setDateRange}
        />
      )}
    </div>
  );
};

export const DatePicker: Story<Props> = DatePickerComponent.bind({});
DatePicker.args = {
  withRange: false,
  withPresets: false,
  maxDate: 0,
};
