import { CalendarProps } from '../calendars/Calendar/Calendar.types';
import { RangeDatePickerPropsExtension } from './components/RangeDatePicker/RangeDatePicker.types';

export type DatePickerSingleDate = Date | null;
export type DatePickerDateRange = [DatePickerSingleDate, DatePickerSingleDate];

export type DatePickerProps<WithRange extends boolean = false> = CalendarProps<WithRange> &
  RangeDatePickerPropsExtension;
