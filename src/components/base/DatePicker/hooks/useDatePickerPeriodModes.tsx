import { RangeDatePickerMode } from 'Components/base/DatePicker/components/RangeDatePicker/RangeDatePicker.types';
import RangeDatePickerCalendar from 'Components/base/DatePicker/components/RangeDatePickerCalendar/RangeDatePickerCalendar';
import RangeDatePickerPresets from 'Components/base/DatePicker/components/RangeDatePickerPresets/RangeDatePickerPresets';
import { CalendarProps } from 'Components/base/calendars/Calendar';
import React, { ReactNode } from 'react';

export type UseDatepickerModesValue = {
  label: string;
  render: (props: CalendarProps<true>) => ReactNode;
};

export const useDatePickerPeriodModes = (): Record<
  RangeDatePickerMode,
  UseDatepickerModesValue
> => {
  return {
    presets: {
      label: 'Presets',
      render: (props) => <RangeDatePickerPresets {...props} />,
    },
    calendar: {
      label: 'Custom range',
      render: (props) => <RangeDatePickerCalendar {...props} />,
    },
  };
};

export default useDatePickerPeriodModes;
