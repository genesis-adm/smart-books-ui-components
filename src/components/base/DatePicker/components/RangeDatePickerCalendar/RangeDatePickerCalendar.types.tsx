import { CalendarProps } from 'Components/base/calendars/Calendar';

export type DatepickerPeriodCalendarProps = CalendarProps<true>;
