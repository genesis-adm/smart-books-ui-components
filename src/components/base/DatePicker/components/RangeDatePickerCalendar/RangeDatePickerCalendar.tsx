import {
  DatePickerDateRange,
  DatePickerSingleDate,
} from 'Components/base/DatePicker/DatePicker.types';
import { DatepickerPeriodCalendarProps } from 'Components/base/DatePicker/components/RangeDatePickerCalendar/RangeDatePickerCalendar.types';
import {
  date2StringDate,
  parseStringifyDate,
} from 'Components/base/DatePicker/utils/dateValidation';
import { Calendar } from 'Components/base/calendars/Calendar';
import { InputNew, InputNewProps } from 'Components/base/inputs/InputNew';
import { endOfDay, startOfDay } from 'date-fns';
import React, { FC, ReactElement } from 'react';
import { NumberFormatBase, usePatternFormat } from 'react-number-format';
import { FormatInputValueFunction, OnValueChange } from 'react-number-format/types/types';

import styles from './RangeDatePickerCalendar.module.scss';

const RangeDatePickerCalendar: FC<DatepickerPeriodCalendarProps> = ({
  startDate = null,
  endDate = null,
  onChange,
  ...restProps
}: DatepickerPeriodCalendarProps): ReactElement => {
  const handleCalendarChange = (dates: DatePickerDateRange) => {
    onChange?.(dates);
  };

  return (
    <div className={styles.datepickerPeriodCalendarWrapper}>
      <Calendar
        {...restProps}
        withRange
        selected={startDate ? startOfDay(startDate) : startDate}
        startDate={startDate ? startOfDay(startDate) : startDate}
        endDate={endDate ? endOfDay(endDate) : endDate}
        onChange={handleCalendarChange}
      />

      <div className={styles.datepickerPeriodCalendarDateInputsContainer}>
        <DateInput
          name="startDate"
          width="110"
          value={startDate}
          onChange={(date) => onChange([date, endDate])}
          label="Start date"
          error={!!startDate && !!endDate && startDate > endDate}
        />
        <DateInput
          name="endDate"
          width="110"
          value={endDate}
          onChange={(date) => onChange([startDate, date])}
          label="End date"
          error={!!startDate && !!endDate && startDate > endDate}
        />
      </div>
    </div>
  );
};

export default RangeDatePickerCalendar;

interface DateInputProps extends Omit<InputNewProps, 'value' | 'onChange'> {
  value?: DatePickerSingleDate;

  onChange?: (value: DatePickerSingleDate) => void;
}

const DateInput = ({ value, onChange, label }: DateInputProps): ReactElement => {
  const {
    value: formattedValue,
    format: patternFormat,
    ...rest
  } = usePatternFormat({
    value: date2StringDate(value),
    format: '##/##/####',
    mask: '_',
    inputMode: 'text',
  });

  const _format: FormatInputValueFunction = (inputValue: string) => {
    const { day, month, year } = parseStringifyDate(inputValue);

    return patternFormat?.(day + month + year) || '';
  };

  const changeValue: OnValueChange = ({ value }) => {
    if (!value) {
      onChange?.(null);
    }

    const { day, month, year } = parseStringifyDate(value);

    const dateValue = new Date(`${year}-${month}-${day}`);

    const isDay = +day > 0 && +day < 32 && day?.length < 3;
    const isMonth = +month > 0 && +month < 13 && month?.length < 3;
    const isYear = +year > 1969;

    const isDate = !isNaN(+dateValue) && isDay && isMonth && isYear;

    if (isDate) {
      onChange?.(dateValue);
    }
  };

  return (
    <div className={styles.datepickerCalendar_dateInput_wrapper}>
      <span className={styles.datepickerCalendar_dateInput_label}>{label}</span>
      <NumberFormatBase
        {...rest}
        format={_format}
        customInput={CustomInput}
        value={formattedValue}
        onValueChange={changeValue}
      />
    </div>
  );
};

const CustomInput = (props: {
  value: string;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
}): ReactElement => {
  return (
    <InputNew
      {...props}
      name="dateInput"
      label=""
      width="110"
      height="36"
      background="grey-10"
      placeholder="dd/mm/yyyy"
      className={styles.datepickerCalendar_dateInput}
    />
  );
};
