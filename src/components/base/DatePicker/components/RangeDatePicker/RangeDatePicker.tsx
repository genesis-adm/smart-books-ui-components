import RangeDatePickerCalendar from 'Components/base/DatePicker/components/RangeDatePickerCalendar/RangeDatePickerCalendar';
import useDatePickerPeriodModes, {
  UseDatepickerModesValue,
} from 'Components/base/DatePicker/hooks/useDatePickerPeriodModes';
import classNames from 'classnames';
import React, { ReactElement, useState } from 'react';

import {
  RangeDatePickerMode,
  RangeDatePickerProps,
} from '../RangeDatePicker/RangeDatePicker.types';
import styles from './RangeDatePicker.module.scss';

const RangeDatePicker = ({
  withPresets,
  initialMode = 'presets',
  ...restProps
}: RangeDatePickerProps): ReactElement => {
  const [currentMode, setCurrentMode] = useState<RangeDatePickerMode>(initialMode);

  const modes = useDatePickerPeriodModes();

  const handleSetCurrentMode = (id: RangeDatePickerMode) => () => setCurrentMode(id);

  return (
    <div className={styles.rangeDatepickerWrapper}>
      {!withPresets ? (
        <RangeDatePickerCalendar {...restProps} />
      ) : (
        <div className={styles.presetsWrapper}>
          <div className={styles.tabsContainer}>
            <div className={styles.tabsWrapper}>
              {(Object.entries(modes) as [RangeDatePickerMode, UseDatepickerModesValue][])?.map(
                ([key, { label }]) => (
                  <div
                    key={key}
                    onClick={handleSetCurrentMode(key)}
                    className={classNames(styles.tabItem, { [styles.active]: currentMode === key })}
                  >
                    {label}
                  </div>
                ),
              )}
            </div>
          </div>

          {modes[currentMode].render(restProps)}
        </div>
      )}
    </div>
  );
};

export default RangeDatePicker;
