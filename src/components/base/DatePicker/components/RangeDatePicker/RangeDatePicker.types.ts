import { CalendarProps } from '../../../calendars/Calendar/Calendar.types';

export type RangeDatePickerMode = 'presets' | 'calendar';

export type RangeDatePickerPropsExtension = {
  withPresets?: boolean;
  initialMode?: RangeDatePickerMode;
};

export type RangeDatePickerProps = CalendarProps<true> & RangeDatePickerPropsExtension;
