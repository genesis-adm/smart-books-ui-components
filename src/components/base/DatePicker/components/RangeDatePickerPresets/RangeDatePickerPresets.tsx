import { DatePickerDateRange } from 'Components/base/DatePicker/DatePicker.types';
import {
  DatepickerPeriodPresetsProps,
  DatepickerPresetsKey,
} from 'Components/base/DatePicker/components/RangeDatePickerPresets/RangeDatePickerPresets.types';
import PresetsItem from 'Components/base/DatePicker/components/RangeDatePickerPresets/components/PresetsItem/PresetsItem';
import useRangeDatePickerPresets from 'Components/base/DatePicker/components/RangeDatePickerPresets/hooks/useRangeDatePickerPresets';
import { dateFormatDefault } from 'Components/base/DatePicker/constants';
import { getGetPeriodKeyByDateRange } from 'Components/base/DatePicker/utils/transformData';
import { Divider } from 'Components/base/Divider';
import React, { FC, ReactElement, useEffect, useState } from 'react';

import styles from './RangeDatePickerPresets.module.scss';

const RangeDatePickerPresets: FC<DatepickerPeriodPresetsProps> = ({
  startDate = null,
  endDate = null,
  onChange,
  dateFormat = dateFormatDefault,
}): ReactElement => {
  const [period, setPeriod] = useState<DatepickerPresetsKey>();

  const presets = useRangeDatePickerPresets();
  const presetList = Object.entries(presets) as [
    DatepickerPresetsKey,
    {
      label: string;
      startDate: Date;
      endDate: Date;
    },
  ][];

  const changePeriod = ({
    periodKey,
    value,
  }: {
    periodKey: DatepickerPresetsKey;
    value: DatePickerDateRange;
  }): void => {
    setPeriod(periodKey);

    onChange?.(value);
  };

  useEffect(() => {
    const initialPeriodKey = getGetPeriodKeyByDateRange(presets, [startDate, endDate]);

    setPeriod(initialPeriodKey);
  }, []);

  return (
    <div className={styles.datepickerPresetsWrapper}>
      <div>
        {presetList.slice(0, 4)?.map(([key, preset]) => (
          <PresetsItem
            key={key}
            checked={key === period}
            dateFormat={dateFormat}
            period={{ period: key, ...preset }}
            onChange={changePeriod}
          />
        ))}
      </div>

      <Divider fullHorizontalWidth />

      <div>
        {presetList.slice(4)?.map(([key, preset]) => (
          <PresetsItem
            key={key}
            checked={key === period}
            dateFormat={dateFormat}
            period={{ period: key, ...preset }}
            onChange={changePeriod}
          />
        ))}
      </div>
    </div>
  );
};

export default RangeDatePickerPresets;
