import { DatepickerPresetsKey } from 'Components/base/DatePicker/components/RangeDatePickerPresets/RangeDatePickerPresets.types';
import {
  addDays,
  addMonths,
  addQuarters,
  addWeeks,
  addYears,
  endOfDay,
  endOfMonth,
  endOfQuarter,
  endOfWeek,
  endOfYear,
  startOfMonth,
  startOfQuarter,
  startOfToday,
  startOfWeek,
  startOfYear,
} from 'date-fns';

export type RangeDatePickerPresetsItems = Record<
  DatepickerPresetsKey,
  { label: string; startDate: Date; endDate: Date }
>;

const useRangeDatePickerPresets = (): RangeDatePickerPresetsItems => {
  const today = startOfToday();

  return {
    thisWeek: {
      label: 'This week',
      startDate: addDays(startOfWeek(today), 1),
      endDate: endOfDay(today),
    },
    thisMonth: {
      label: 'This month',
      startDate: startOfMonth(today),
      endDate: endOfDay(today),
    },
    thisQuarter: {
      label: 'This quarter',
      startDate: startOfQuarter(today),
      endDate: endOfDay(today),
    },
    thisYear: {
      label: 'This year',
      startDate: startOfYear(today),
      endDate: endOfDay(today),
    },

    lastWeek: {
      label: 'Last week',
      startDate: addWeeks(addDays(startOfWeek(today), 1), -1),
      endDate: addDays(endOfWeek(addWeeks(startOfWeek(today), -1)), 1),
    },
    lastMonth: {
      label: 'Last month',
      startDate: addMonths(startOfMonth(today), -1),
      endDate: endOfMonth(addMonths(startOfMonth(today), -1)),
    },
    lastQuarter: {
      label: 'Last quarter',
      startDate: addQuarters(startOfQuarter(today), -1),
      endDate: endOfQuarter(addQuarters(startOfQuarter(today), -1)),
    },
    lastYear: {
      label: 'Last year',
      startDate: addYears(startOfYear(today), -1),
      endDate: endOfYear(addYears(startOfYear(today), -1)),
    },
  };
};

export default useRangeDatePickerPresets;
