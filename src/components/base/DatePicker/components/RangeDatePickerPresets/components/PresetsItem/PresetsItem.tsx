import { DatePickerDateRange } from 'Components/base/DatePicker/DatePicker.types';
import styles from 'Components/base/DatePicker/components/RangeDatePickerPresets/RangeDatePickerPresets.module.scss';
import {
  DatepickerPresetsKey,
  PresetsDateFormats,
} from 'Components/base/DatePicker/components/RangeDatePickerPresets/RangeDatePickerPresets.types';
import { Radio } from 'Components/base/Radio';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { format } from 'date-fns';
import React, { FC, ReactElement, useState } from 'react';

type PresetsItemProps = {
  dateFormat: PresetsDateFormats;
  period: { period: DatepickerPresetsKey; label: string; startDate: Date; endDate: Date };

  checked: boolean;
  onChange?: ({
    periodKey,
    value,
  }: {
    periodKey: DatepickerPresetsKey;
    value: DatePickerDateRange;
  }) => void;
};

const PresetsItem: FC<PresetsItemProps> = ({
  dateFormat,
  period: { period, startDate, endDate, label },
  checked,
  onChange,
}): ReactElement => {
  const [isHovered, setIsHovered] = useState(false);
  const presetPeriod = `${format(startDate, dateFormat.dateMonthYear)} - ${format(
    endDate,
    dateFormat.dateMonthYear,
  )}`;

  const hover = () => {
    setIsHovered(true);
  };

  const leave = () => {
    setIsHovered(false);
  };

  const changeHandler = (e: React.FormEvent<HTMLInputElement>) => {
    const datePeriodKey = e.currentTarget.id as DatepickerPresetsKey;

    onChange?.({
      periodKey: datePeriodKey,
      value: [startDate, endDate],
    });
  };

  return (
    <Container
      onMouseEnter={hover}
      onMouseLeave={leave}
      background={isHovered ? 'grey-10' : undefined}
      borderRadius="10"
      className={styles.presetsRadioWrapper}
    >
      <Radio
        className={styles.presetsRadio}
        id={period}
        name="period"
        onChange={changeHandler}
        color="grey-100"
        checked={checked}
        hovered={isHovered}
      >
        <div className={styles.presetsLabelWrapper}>
          <Text type="caption-regular" color={checked ? 'violet-90' : 'black-100'}>
            {label}
          </Text>

          <Text type="caption-regular" color="inherit">
            {presetPeriod}
          </Text>
        </div>
      </Radio>
    </Container>
  );
};

export default PresetsItem;
