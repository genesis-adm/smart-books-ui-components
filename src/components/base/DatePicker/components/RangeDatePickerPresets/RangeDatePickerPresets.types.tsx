import { CalendarProps } from 'Components/base/calendars/Calendar';

export type PresetsDateFormats = {
  dateMonthYear: 'dd.MM.yyyy';
  dateMonth: 'dd MMM';
};

export interface DatepickerPeriodPresetsProps
  extends Pick<CalendarProps<true>, 'startDate' | 'endDate' | 'onChange'> {
  dateFormat?: PresetsDateFormats;
}

export type DatepickerPresetsKey =
  | 'thisWeek'
  | 'thisMonth'
  | 'thisQuarter'
  | 'thisYear'
  | 'lastWeek'
  | 'lastMonth'
  | 'lastQuarter'
  | 'lastYear';
