import { PresetsDateFormats } from 'Components/base/DatePicker/components/RangeDatePickerPresets/RangeDatePickerPresets.types';

export const dateFormatDefault: PresetsDateFormats = {
  dateMonthYear: 'dd.MM.yyyy',
  dateMonth: 'dd MMM',
};
