import RangeDatePicker from 'Components/base/DatePicker/components/RangeDatePicker/RangeDatePicker';
import { Calendar } from 'Components/base/calendars/Calendar';
import classNames from 'classnames';
import React, { ReactElement } from 'react';

import { CalendarProps } from '../calendars/Calendar/Calendar.types';
import styles from './DatePicker.module.scss';
import { DatePickerProps } from './DatePicker.types';

const DatePicker = <WithRange extends boolean = false>({
  withRange = false as WithRange,
  ...restProps
}: DatePickerProps<WithRange>): ReactElement => {
  const rangeDatePickerProps = restProps as CalendarProps<true>;

  return (
    <div className={classNames(styles.datepickerWrapper, restProps?.className)}>
      {!withRange ? (
        <Calendar {...restProps} withRange={withRange} />
      ) : (
        <RangeDatePicker {...rangeDatePickerProps} withRange={withRange} />
      )}
    </div>
  );
};

export default DatePicker;
