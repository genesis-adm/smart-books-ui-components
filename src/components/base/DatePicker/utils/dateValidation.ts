import { DatePickerSingleDate } from 'Components/base/DatePicker/DatePicker.types';
import { format } from 'date-fns';

export class DateValidator {
  day(numStr: string): string {
    if (!numStr?.length) return '';

    if (numStr?.length === 1 && +numStr > 3) {
      return '';
    }

    if (numStr?.length === 2 && +numStr > 31) {
      return numStr[0];
    }

    return numStr;
  }

  month(numStr: string): string {
    if (!numStr?.length) return '';

    if (numStr?.length === 1 && +numStr > 1) {
      return '';
    }

    if (numStr?.length === 2 && +numStr > 12) {
      return numStr[0];
    }

    return numStr;
  }

  year(numStr: string): string {
    if (!numStr?.length) return '';

    if (numStr?.length > 4) {
      return numStr.slice(0, 3);
    }

    if (numStr?.length === 4 && +numStr < 1970) {
      return numStr.slice(0, 3);
    }

    return numStr;
  }
}

export const dateString2Date = (dateString: string) => {
  const dt = dateString.split(/\/|\s/);

  return new Date(dt.slice(0, 3).reverse().join('-'));
};

export const date2StringDate = (date?: DatePickerSingleDate): string => {
  return date ? format(date, 'ddMMyyyy') : '';
};

export const parseStringifyDate = (date: string) => {
  const dateValidator = new DateValidator();

  return {
    day: dateValidator.day(date.substring(0, 2)),
    month: dateValidator.month(date.substring(2, 4)),
    year: dateValidator.year(date.substring(4, 8)),
  };
};
