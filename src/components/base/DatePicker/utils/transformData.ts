import { RangeDatePickerPresetsItems } from 'Components/base/DatePicker/components/RangeDatePickerPresets/hooks/useRangeDatePickerPresets';
import { isSameDay } from 'date-fns';

import { DatePickerDateRange } from '../DatePicker.types';
import { DatepickerPresetsKey } from '../components/RangeDatePickerPresets/RangeDatePickerPresets.types';

export const getGetPeriodKeyByDateRange = (
  presets: RangeDatePickerPresetsItems,
  dateRange: DatePickerDateRange,
): DatepickerPresetsKey | undefined => {
  const [startDate, endDate] = dateRange;

  return (Object.keys(presets || {}) as DatepickerPresetsKey[])?.find(
    (key) =>
      startDate &&
      endDate &&
      isSameDay(presets[key].startDate, startDate) &&
      isSameDay(presets[key].endDate, endDate),
  );
};
