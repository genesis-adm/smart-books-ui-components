import { CalendarProps } from 'Components/base/calendars/Calendar/Calendar.types';
import classNames from 'classnames';
import { addDays, subDays } from 'date-fns';
import React from 'react';
import DatePicker from 'react-datepicker';

import style from './Calendar.module.scss';

const Calendar = <WithRange extends boolean = false>({
  name,
  minDate,
  maxDate,
  calendarStartDay = 1,
  className,
  onChange,
  withRange = false as WithRange,
  ...restProps
}: CalendarProps<WithRange>) => (
  <div className={classNames(style.wrapper, className)}>
    <DatePicker
      {...restProps}
      id={name}
      name={name}
      selectsRange={withRange}
      onChange={onChange}
      inline
      calendarClassName="react-datepicker_inline"
      showMonthDropdown
      showYearDropdown
      useShortMonthInDropdown
      yearDropdownItemNumber={4}
      calendarStartDay={calendarStartDay}
      disabledKeyboardNavigation
      minDate={minDate !== undefined ? subDays(new Date(), minDate) : undefined}
      maxDate={maxDate !== undefined ? addDays(new Date(), maxDate) : undefined}
    />
  </div>
);
export default Calendar;
