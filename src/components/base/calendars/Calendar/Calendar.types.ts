import { DatePickerDateRange, DatePickerSingleDate } from '../../DatePicker/DatePicker.types';

interface SingleDate {
  selected?: DatePickerSingleDate;
}

interface DateRange {
  selected?: DatePickerSingleDate;
  startDate?: DatePickerSingleDate;
  endDate?: DatePickerSingleDate;
}

type OwnCalendarProps<WithRange extends boolean = false> = WithRange extends false
  ? SingleDate
  : DateRange;

export type CalendarProps<WithRange extends boolean = false> = OwnCalendarProps<WithRange> & {
  withRange?: WithRange | boolean;
  name: string;
  minDate?: number;
  maxDate?: number;
  calendarStartDay?: 0 | 1 | 2 | 3 | 4 | 5 | 6;
  className?: string;

  onChange: (
    dates: WithRange extends false | undefined ? DatePickerSingleDate : DatePickerDateRange,
  ) => void;
};
