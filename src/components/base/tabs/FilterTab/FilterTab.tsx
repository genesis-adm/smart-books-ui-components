import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React from 'react';

import style from './FilterTab.module.scss';

export interface FilterTabProps {
  id: string;
  active: boolean;
  className?: string;
  children?: React.ReactNode;
  onClick(id: string): void;
}

export interface FilterTabWrapperProps {
  className?: string;
  children?: React.ReactNode;
}

const FilterTab: React.FC<FilterTabProps> = ({ id, active, className, onClick, children }) => {
  const handleClick = () => onClick(id);
  return (
    <div
      role="presentation"
      id={id}
      className={classNames(style.item, { [style.active]: active }, className)}
      onClick={handleClick}
    >
      <Text className={style.text} type="caption-semibold" color="inherit">
        {children}
      </Text>
    </div>
  );
};

const FilterTabWrapper: React.FC<FilterTabWrapperProps> = ({ className, children }) => (
  <div className={classNames(style.wrapper, className)}>{children}</div>
);

export { FilterTab, FilterTabWrapper };
