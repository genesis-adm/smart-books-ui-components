import { FilterTab, FilterTabWrapper } from 'Components/base/tabs/FilterTab';
import React, { useState } from 'react';

export default {
  title: 'Components/Base/Tabs/Filter Tabs',
};

export const FilterTabs: React.FC = () => {
  const [active, setActive] = useState<string>('presets');
  return (
    <FilterTabWrapper>
      <FilterTab id="presets" onClick={setActive} active={active === 'presets'}>
        Presets
      </FilterTab>
      <FilterTab id="custom" onClick={setActive} active={active === 'custom'}>
        Custom range
      </FilterTab>
    </FilterTabWrapper>
  );
};
