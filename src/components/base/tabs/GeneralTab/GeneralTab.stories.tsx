import { GeneralTab, GeneralTabWrapper } from 'Components/base/tabs/GeneralTab';
import { ReactComponent as QuestionAltIcon } from 'Svg/v2/16/question-transparent.svg';
import React, { useState } from 'react';

export default {
  title: 'Components/Base/Tabs/General Tabs',
};

export const GeneralTabs: React.FC = () => {
  const [active, setActive] = useState<string>('general');
  return (
    <GeneralTabWrapper>
      <GeneralTab
        id="general"
        icon={<QuestionAltIcon />}
        onClick={setActive}
        active={active === 'general'}
        counter={345}
      >
        General
      </GeneralTab>
      <GeneralTab id="accounting" onClick={setActive} active={active === 'accounting'}>
        Accounting
      </GeneralTab>
    </GeneralTabWrapper>
  );
};
