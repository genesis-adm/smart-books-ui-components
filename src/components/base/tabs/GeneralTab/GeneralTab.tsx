import { Icon } from 'Components/base/Icon';
import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React from 'react';

import { TestableProps } from '../../../../types/general';
import { FontTypes } from '../../../types/fontTypes';
import style from './GeneralTab.module.scss';

export interface GeneralTabProps extends TestableProps {
  id: string;
  active: boolean;
  disabled?: boolean;
  icon?: React.ReactNode;
  font?: FontTypes;
  counter?: number;
  children?: React.ReactNode;
  onClick(id: string): void;
}

export interface GeneralTabWrapperProps {
  className?: string;
  children?: React.ReactNode;
}

const GeneralTab: React.FC<GeneralTabProps> = ({
  id,
  active,
  disabled,
  icon,
  font = 'text-regular',
  counter,
  onClick,
  children,
  dataTestId,
}) => {
  const handleClick = () => onClick(id);
  return (
    <div
      data-testid={dataTestId}
      role="presentation"
      id={id}
      className={classNames(style.item, { [style.active]: active, [style.disabled]: disabled })}
      onClick={handleClick}
    >
      {icon && (
        <Icon className={style.icon} icon={icon} path="inherit" transition="unset" clickThrough />
      )}
      <Text className={style.text} type={font} color="inherit" transition="unset">
        {children}
      </Text>
      {counter && (
        <div className={style.counter}>
          <Text color="white-100">{counter}</Text>
        </div>
      )}
    </div>
  );
};

const GeneralTabWrapper: React.FC<GeneralTabWrapperProps> = ({ className, children }) => (
  <div className={classNames(style.wrapper, className)}>{children}</div>
);

export { GeneralTab, GeneralTabWrapper };
