import { Icon } from 'Components/base/Icon';
import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React, { ReactNode } from 'react';

import { TestableProps } from '../../../../types/general';
import { FontTypes } from '../../../types/fontTypes';
import style from './MenuTab.module.scss';

export interface MenuTabProps extends TestableProps {
  active?: boolean;
  disabled?: boolean;
  icon?: ReactNode;
  id: string;
  font?: FontTypes;
  children?: React.ReactNode;
  onClick?(id: string): void;
}

export interface MenuTabWrapperProps {
  className?: string;
  children?: React.ReactNode;
}

const MenuTab: React.FC<MenuTabProps> = ({
  id,
  active,
  font = 'body-medium',
  icon,
  disabled,
  onClick,
  children,
  dataTestId,
}) => {
  const handleClick = () => onClick?.(id);
  return (
    <div
      data-testid={dataTestId}
      role="presentation"
      id={id}
      className={classNames(style.item, { [style.active]: active, [style.disabled]: disabled })}
      onClick={handleClick}
    >
      {icon && <Icon icon={icon} path={active ? 'black-100' : 'grey-90'} />}
      <Text className={style.text} type={font} color={active ? 'black-100' : 'grey-90'}>
        {children}
      </Text>
    </div>
  );
};

const MenuTabWrapper: React.FC<MenuTabWrapperProps> = ({ className, children }) => (
  <div className={classNames(style.wrapper, className)}>{children}</div>
);

export { MenuTab, MenuTabWrapper };
