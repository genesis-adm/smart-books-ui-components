import { MenuTab, MenuTabWrapper } from 'Components/base/tabs/MenuTab';
import React, { useState } from 'react';

export default {
  title: 'Components/Base/Tabs/Menu Tabs',
};

export const MenuTabs: React.FC = () => {
  const [active, setActive] = useState<string>('general');
  return (
    <MenuTabWrapper>
      <MenuTab id="general" onClick={setActive} active={active === 'general'}>
        General
      </MenuTab>
      <MenuTab id="accounting" onClick={setActive} active={active === 'accounting'}>
        Accounting
      </MenuTab>
    </MenuTabWrapper>
  );
};
