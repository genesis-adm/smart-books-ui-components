import { Text } from 'Components/base/typography/Text';
import { BackgroundColorStaticNewTypes } from 'Components/types/colorTypes';
import classNames from 'classnames';
import React from 'react';

import { TestableProps } from '../../../../types/general';
import style from './CurrencyTab.module.scss';

export interface CurrencyTabProps extends TestableProps {
  id: string;
  active: boolean;
  className?: string;
  children?: React.ReactNode;
  onClick(id: string): void;
}

export interface CurrencyTabWrapperProps {
  background?: BackgroundColorStaticNewTypes;
  className?: string;
  children?: React.ReactNode;
}

const CurrencyTab: React.FC<CurrencyTabProps> = ({
  id,
  active,
  className,
  onClick,
  children,
  dataTestId,
}) => {
  const handleClick = () => onClick(id);
  return (
    <div
      data-testid={dataTestId}
      role="presentation"
      id={id}
      className={classNames(style.item, { [style.active]: active }, className)}
      onClick={handleClick}
    >
      <Text className={style.text} type="body-regular-14" color="inherit">
        {children}
      </Text>
    </div>
  );
};

const CurrencyTabWrapper: React.FC<CurrencyTabWrapperProps> = ({
  background = 'violet-10',
  className,
  children,
}) => (
  <div className={classNames(style.wrapper, style[`background_${background}`], className)}>
    {children}
  </div>
);

export { CurrencyTab, CurrencyTabWrapper };
