import { CurrencyTab, CurrencyTabWrapper } from 'Components/base/tabs/CurrencyTab';
import React, { useState } from 'react';

export default {
  title: 'Components/Base/Tabs/Currency Tabs',
};

export const CurrencyTabs: React.FC = () => {
  const [active, setActive] = useState<string>('usd');
  return (
    <CurrencyTabWrapper>
      <CurrencyTab id="usd" onClick={setActive} active={active === 'usd'}>
        USD
      </CurrencyTab>
      <CurrencyTab id="eur" onClick={setActive} active={active === 'eur'}>
        EUR
      </CurrencyTab>
    </CurrencyTabWrapper>
  );
};
