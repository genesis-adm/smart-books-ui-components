import { Meta, Story } from '@storybook/react';
import {
  LazyLoader as Component,
  LazyLoaderProps as Props,
} from 'Components/base/loaders/LazyLoader';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Loaders/LazyLoader',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const LazyLoaderComponent: Story<Props> = (args) => <Component {...args} />;

export const LazyLoader = LazyLoaderComponent.bind({});
LazyLoader.args = {};
