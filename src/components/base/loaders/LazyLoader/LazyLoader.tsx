import LoaderSpinner from 'Components/base/loaders/LoaderSpinner/LoaderSpinner';
import classNames from 'classnames';
import React, { ReactElement } from 'react';

import style from './LazyLoader.module.scss';
import { LazyLoaderProps } from './LazyLoader.types';

const LazyLoader = ({ className }: LazyLoaderProps): ReactElement => (
  <div className={classNames(style.component, className)}>
    <LoaderSpinner size="14" />
    <span className={style.text}>Loading...</span>
  </div>
);

export default LazyLoader;
