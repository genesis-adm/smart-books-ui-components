import { Icon } from 'Components/base/Icon';
import { Portal } from 'Components/base/Portal';
import { Button } from 'Components/base/buttons/Button';
import { LoaderSpinner } from 'Components/base/loaders/LoaderSpinner';
import { ReactComponent as AlertIcon } from 'Svg/v2/32/alert-filled.svg';
import { ReactComponent as CheckIcon } from 'Svg/v2/32/check-circle.svg';
import classNames from 'classnames';
import React, { ReactElement, useEffect, useRef, useState } from 'react';
import { Timeout } from 'react-number-format/types/types';

import style from './ActionLoader.module.scss';
import { Action, ActionLoaderProps } from './ActionLoader.types';

const actionData: Record<Action, { inProgressTitle: string; completedTitle: string }> = {
  update: {
    inProgressTitle: 'Updating',
    completedTitle: 'Updated',
  },
  apply: { inProgressTitle: 'Applying', completedTitle: 'Applied' },
  save: { inProgressTitle: 'Saving', completedTitle: 'Saved' },
  delete: { inProgressTitle: 'Deleting', completedTitle: 'Deleted' },
};

const ActionLoader = ({
  type,
  overlay = true,
  isLoading,
  className,
  onLoadComplete,
  rootElemId,
  errorMessage,
  dataTestId,
}: ActionLoaderProps): ReactElement => {
  const [showLoadedIcon, setShowLoadedIcon] = useState<boolean>(false);
  const [showSpinner, setShowSpinner] = useState<boolean>(false);
  const [showError, setShowError] = useState<boolean>(false);

  const prevLoadingValue = useRef(isLoading);

  const showLoader = showSpinner || showLoadedIcon || showError;

  const handleCloseOnError = () => setShowError(false);

  useEffect(() => {
    let timeout: Timeout | null = null;
    if (isLoading) {
      setShowSpinner(true);
    }

    if (prevLoadingValue.current && !isLoading) {
      setShowSpinner(false);
      setShowLoadedIcon(!errorMessage);
      setShowError(!!errorMessage);

      timeout = setTimeout(() => {
        setShowLoadedIcon(false);
        !errorMessage && onLoadComplete?.();
      }, 2000);
    }

    prevLoadingValue.current = isLoading;
    return () => {
      if (timeout) {
        clearTimeout(timeout);
      }
      return;
    };
  }, [isLoading]);

  const LoaderContent = () => (
    <div className={classNames(style.wrapper, className)} data-testid={dataTestId}>
      {overlay && <div className={style.overlay} />}
      <div className={style.container}>
        {showSpinner && (
          <>
            <LoaderSpinner />
            <span className={style.text}>{actionData[type].inProgressTitle}...</span>
          </>
        )}
        {showLoadedIcon && (
          <div className={style.icon}>
            <Icon icon={<CheckIcon />} />
            <span className={style.text}>{actionData[type].completedTitle}</span>
          </div>
        )}
        {showError && !isLoading && (
          <div className={style.error_wrapper}>
            <div className={style.error_message_container}>
              <Icon icon={<AlertIcon />} />
              <span className={style.text}>{errorMessage}</span>
            </div>
            <Button width="xs" height="40" onClick={handleCloseOnError}>
              Okay
            </Button>
          </div>
        )}
      </div>
    </div>
  );

  if (!showLoader) return <></>;

  return (
    <Portal position="absolute" rootElemId={rootElemId}>
      <LoaderContent />
    </Portal>
  );
};

export default ActionLoader;
