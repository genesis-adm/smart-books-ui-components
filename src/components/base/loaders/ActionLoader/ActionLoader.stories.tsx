import { Meta, Story } from '@storybook/react';
import { Container } from 'Components/base/grid/Container';
import { ContainerMain } from 'Components/base/grid/ContainerMain';
import {
  ActionLoader as Component,
  ActionLoaderProps as Props,
} from 'Components/base/loaders/ActionLoader';
import { NavigationSB } from 'Components/custom/Stories/NavigationSB';
import React, { useState } from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/base/loaders/ActionLoader',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const ActionLoaderComponent: Story<Props> = (args) => {
  const [toggle, toggleMenu] = useState(true);
  const toggleOpen = () => toggleMenu(!toggle);
  return (
    <Container fullscreen flexwrap="nowrap" background={'white-100'} position="relative">
      <NavigationSB opened={toggle} onToggleOpen={toggleOpen} />
      <ContainerMain navOpened={toggle}>
        <Component {...args} />
      </ContainerMain>
    </Container>
  );
};

export const ActionLoader = ActionLoaderComponent.bind({});
ActionLoader.args = {
  type: 'update',
  errorMessage: 'Something went wrong. Please, try again',
};
