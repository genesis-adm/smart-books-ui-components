import { TestableProps } from '../../../../types/general';

export type Action = 'update' | 'save' | 'delete' | 'apply';

export interface ActionLoaderProps extends TestableProps {
  type: Action;
  overlay?: boolean;
  rootElemId?: string;
  isLoading?: boolean;
  className?: string;

  errorMessage?: string;

  onLoadComplete?(): void;
}
