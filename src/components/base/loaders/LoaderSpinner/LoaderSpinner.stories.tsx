import { Meta, Story } from '@storybook/react';
import {
  LoaderSpinner as Component,
  LoaderSpinnerProps as Props,
} from 'Components/base/loaders/LoaderSpinner';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Loaders/LoaderSpinner',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const LoaderSpinnerComponent: Story<Props> = (args) => <Component {...args} />;

export const LoaderSpinner = LoaderSpinnerComponent.bind({});
LoaderSpinner.args = {};
