import classNames from 'classnames';
import React, { ReactElement } from 'react';

import style from './LoaderSpinner.module.scss';
import { LoaderSpinnerProps } from './LoaderSpinner.types';

const LoaderSpinner = ({ size = '32', className }: LoaderSpinnerProps): ReactElement => (
  <div className={classNames(style.component, style[`size_${size}`], className)} />
);

export default LoaderSpinner;
