export interface LoaderSpinnerProps {
  size?: '14' | '32';
  className?: string;
}
