import classNames from 'classnames';
import React from 'react';

import { TestableProps } from '../../../types/general';
import type { BackgroundColorStaticNewTypes } from '../../types/colorTypes';
import type { Display } from '../../types/gridTypes';
import style from './Icon.module.scss';

export interface IconProps extends TestableProps {
  icon: React.ReactNode;
  staticColor?: boolean;
  path?: BackgroundColorStaticNewTypes;
  stroke?: BackgroundColorStaticNewTypes;
  ellipse?: BackgroundColorStaticNewTypes;
  display?: Display;
  cursor?: 'default' | 'pointer' | 'move' | 'text' | 'inherit';
  transition?: 'inherit' | 'unset';
  clickThrough?: boolean;
  className?: string;

  onClick?(e: React.MouseEvent<HTMLDivElement>): void;
}

const Icon: React.FC<IconProps> = ({
  icon,
  staticColor,
  path,
  stroke,
  ellipse,
  display = 'inline-block',
  cursor,
  transition,
  clickThrough,
  className,
  dataTestId,
  onClick,
}) => (
  <div
    data-testid={dataTestId}
    role="presentation"
    className={classNames(
      style.icon,
      style[`display_${display}`],
      {
        [style[`path_${path}`]]: path && !staticColor,
        [style[`stroke_${stroke}`]]: stroke && !staticColor,
        [style[`ellipse_${ellipse}`]]: ellipse && !staticColor,
        [style[`cursor_${cursor}`]]: cursor,
        [style[`transition_${transition}`]]: transition,
        [style.clickThrough]: clickThrough,
      },
      className,
    )}
    onClick={onClick}
  >
    {icon}
  </div>
);

export default Icon;
