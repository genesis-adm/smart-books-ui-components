import { Meta, Story } from '@storybook/react';
import { Icon as Component, IconProps as Props } from 'Components/base/Icon';
import { ReactComponent as LinkIcon } from 'Svg/v2/16/link.svg';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Icon',
  component: Component,
  argTypes: {
    className: hideProperty,
    icon: hideProperty,
    onClick: hideProperty,
  },
} as Meta;

const IconTemplate: Story<Props> = (args) => <Component {...args} icon={<LinkIcon />} />;

export const Icon = IconTemplate.bind({});
Icon.args = {
  display: 'inline-block',
  path: 'violet-90',
  cursor: 'pointer',
  clickThrough: false,
};
