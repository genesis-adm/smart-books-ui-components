import { Meta, Story } from '@storybook/react';
import { Loader as Component, LoaderProps as Props } from 'Components/base/Loader';
import { Container } from 'Components/base/grid/Container';
import { ContainerMain } from 'Components/base/grid/ContainerMain';
import { NavigationSB } from 'Components/custom/Stories/NavigationSB';
import React, { useState } from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Loader',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const LoaderComponent: Story<Props> = (args) => {
  const [toggle, toggleMenu] = useState(true);
  const toggleOpen = () => toggleMenu(!toggle);
  return (
    <Container fullscreen flexwrap="nowrap" background="white-100">
      <NavigationSB opened={toggle} onToggleOpen={toggleOpen} />
      <ContainerMain navOpened={toggle}>
        <Component {...args} />
      </ContainerMain>
    </Container>
  );
};

export const Loader = LoaderComponent.bind({});
Loader.args = {
  position: 'relative',
  overlay: false,
};
