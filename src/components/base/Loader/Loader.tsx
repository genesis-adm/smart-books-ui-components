import { Portal } from 'Components/base/Portal';
import React from 'react';

import { TestableProps } from '../../../types/general';
import style from './Loader.module.scss';

export interface LoaderProps extends TestableProps {
  position?: 'absolute' | 'relative';
  overlay?: boolean;
}

const Loader: React.FC<LoaderProps> = ({ position = 'absolute', overlay = false, dataTestId }) => (
  <>
    {position === 'absolute' && (
      <Portal position="absolute">
        <div className={style.wrapper} data-testid={dataTestId}>
          {overlay && <div className={style.overlay} />}
          <div className={style.loader}>
            <span className={style.content} />
          </div>
        </div>
      </Portal>
    )}
    {position === 'relative' && (
      <div className={style.wrapper} data-testid={dataTestId}>
        {overlay && <div className={style.overlay} />}
        <div className={style.loader}>
          <span className={style.content} />
        </div>
      </div>
    )}
  </>
);

export default Loader;
