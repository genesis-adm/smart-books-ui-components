import { Meta, Story } from '@storybook/react';
import { LoadingLine as Component, LoadingLineProps as Props } from 'Components/base/LoadingLine';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Loading Line',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const LoadingLineComponent: Story<Props> = (args) => <Component {...args} />;

export const LoadingLine = LoadingLineComponent.bind({});
LoadingLine.args = {
  loadingCompleted: false,
};
