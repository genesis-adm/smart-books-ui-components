import classNames from 'classnames';
import React from 'react';

import style from './LoadingLine.module.scss';

export interface LoadingLineProps {
  loadingCompleted: boolean;
  className?: string;
}

const LoadingLine: React.FC<LoadingLineProps> = ({ loadingCompleted, className }) => (
  <div className={classNames(style.wrapper, className)}>
    <div className={classNames(style.line, { [style.loading]: !loadingCompleted })} />
  </div>
);

export default LoadingLine;
