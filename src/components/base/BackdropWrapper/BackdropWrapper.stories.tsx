import { Meta, Story } from '@storybook/react';
import {
  BackdropWrapper as Component,
  BackdropWrapperProps as Props,
} from 'Components/base/BackdropWrapper';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Draft Components/BackdropWrapper',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const BackdropWrapperComponent: Story<Props> = (args) => <Component {...args} />;

export const BackdropWrapper = BackdropWrapperComponent.bind({});
BackdropWrapper.args = {};
