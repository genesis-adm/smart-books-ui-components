import { ReactElement } from 'react';

export interface BackdropWrapperProps {
  className?: string;
  children: ReactElement;
  isVisible: boolean;
}
