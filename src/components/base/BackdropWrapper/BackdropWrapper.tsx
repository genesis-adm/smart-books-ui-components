import React, { ReactElement } from 'react';

import style from './BackdropWrapper.module.scss';
import { BackdropWrapperProps } from './BackdropWrapper.types';

// Reminder: do not forget to add the following code to root index.ts
// export { BackdropWrapper } from './components/Base/BackdropWrapper';

const BackdropWrapper = ({ isVisible, children }: BackdropWrapperProps): ReactElement => (
  <>
    {isVisible && <div className={style.blurBackground}></div>}
    {children}
  </>
);

export default BackdropWrapper;
