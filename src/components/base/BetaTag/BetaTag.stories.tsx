import { Meta, Story } from '@storybook/react';
import { BetaTag as Component, BetaTagProps as Props } from 'Components/base/BetaTag';
import { IconButton } from 'Components/base/buttons/IconButton';
import { ReactComponent as HistoryIcon } from 'Svg/v2/16/clock-filled.svg';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/BetaTag',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const BetaTagComponent: Story<Props> = (args) => (
  <Component {...args}>
    <IconButton
      size="32"
      background={'transparent-blue-10'}
      color="grey-100-violet-90"
      tooltip="Audit log"
      icon={<HistoryIcon transform="scale(1.3)" />}
      onClick={() => {}}
    />
  </Component>
);

export const BetaTag = BetaTagComponent.bind({});
BetaTag.args = {};
