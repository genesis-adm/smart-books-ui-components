import { ReactNode } from 'react';

export interface BetaTagProps {
  className?: string;
  children: ReactNode;
  isShown?: boolean;
}
