import classNames from 'classnames';
import React, { ReactElement } from 'react';

import style from './BetaTag.module.scss';
import { BetaTagProps } from './BetaTag.types';

const BetaTag = ({ className, children, isShown = true }: BetaTagProps): ReactElement => {
  return (
    <>
      {isShown ? (
        <div className={classNames(style.component, className)}>
          {isShown && (
            <div className={style.beta}>
              <span className={style.text}>Beta</span>
            </div>
          )}
          {children}
        </div>
      ) : (
        children
      )}
    </>
  );
};

export default BetaTag;
