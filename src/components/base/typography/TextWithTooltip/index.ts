export { default as TextWithTooltip } from './TextWithTooltip';
export type { TextWithTooltipProps } from './TextWithTooltip.types';
