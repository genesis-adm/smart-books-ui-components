import { TextColorStaticNewTypes } from 'Components/types/colorTypes';
import { FontTypes, TextAlign } from 'Components/types/fontTypes';
import React from 'react';

export interface TextWithTooltipProps {
  className?: string;
  text: string;
  tooltip?: React.ReactNode;
  type?: FontTypes;
  align?: TextAlign;
  color?: TextColorStaticNewTypes;
}
