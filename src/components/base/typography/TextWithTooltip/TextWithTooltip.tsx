import { Tooltip } from 'Components/base/Tooltip';
import { Text } from 'Components/base/typography/Text';
import React, { ReactElement, useEffect, useRef, useState } from 'react';

import { TextWithTooltipProps } from './TextWithTooltip.types';

// Reminder: do not forget to add the following code to root index.ts
// export { TextWithTooltip } from './components/base/typography/TextWithTooltip';

const TextWithTooltip = ({
  className,
  tooltip,
  text,
  type,
  align,
  color,
}: TextWithTooltipProps): ReactElement => {
  const textPrimaryRef = useRef<HTMLDivElement>(null);
  const [tooltipValue, setTooltipValue] = useState<React.ReactNode>(tooltip);

  useEffect(() => {
    if (
      !tooltip &&
      textPrimaryRef.current &&
      (textPrimaryRef.current.scrollHeight > textPrimaryRef.current.clientHeight ||
        textPrimaryRef.current.scrollWidth > textPrimaryRef.current.clientWidth)
    ) {
      setTooltipValue(text);
    }
  }, [tooltip, text]);
  return (
    <Tooltip message={tooltipValue} tooltipWidth="auto">
      <Text
        ref={textPrimaryRef}
        className={className}
        type={type}
        color={color}
        align={align}
        noWrap
      >
        {text}
      </Text>
    </Tooltip>
  );
};

export default TextWithTooltip;
