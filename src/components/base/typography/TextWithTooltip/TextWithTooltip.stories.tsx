import { Meta, Story } from '@storybook/react';
import {
  TextWithTooltip as Component,
  TextWithTooltipProps as Props,
} from 'Components/base/typography/TextWithTooltip';
import React from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Typography/TextWithTooltip',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const TextWithTooltipComponent: Story<Props> = (args) => (
  <Component {...args} className={spacing.w230} />
);

export const TextWithTooltip = TextWithTooltipComponent.bind({});
TextWithTooltip.args = {
  text: 'Veryyyyy veryyyyyyyyyyyyyyyy looooooong teeeeext',
};
