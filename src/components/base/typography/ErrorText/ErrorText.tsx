import classNames from 'classnames';
import React from 'react';

import style from './ErrorText.module.scss';

export interface ErrorTextProps {
  error: '403' | '404' | '500';
  className?: string;
}

const ErrorText: React.FC<ErrorTextProps> = ({ error, className }) => (
  <p className={classNames(style.text, className)}>{error}</p>
);

export default ErrorText;
