import { Meta, Story } from '@storybook/react';
import {
  ErrorText as Component,
  ErrorTextProps as Props,
} from 'Components/base/typography/ErrorText';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Typography/Error Text',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const ErrorTextComponent: Story<Props> = (args) => <Component {...args} />;

export const ErrorText = ErrorTextComponent.bind({});
ErrorText.args = {
  error: '404',
};
