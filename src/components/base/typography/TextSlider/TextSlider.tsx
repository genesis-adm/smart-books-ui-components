import { TextTransition } from 'Components/base/typography/TextTransition';
import React, { useEffect, useState } from 'react';

export interface TextSliderProps {
  content: string[];
}

const TextSlider: React.FC<TextSliderProps> = ({ content }) => {
  const [textIndex, setTextIndex] = useState<number>(0);
  useEffect(() => {
    const interval = setInterval(() => {
      setTextIndex((prevState) => prevState + 1);
    }, 5000);
    return () => clearTimeout(interval);
  }, [textIndex]);
  return (
    <TextTransition springConfig={{ tension: 120, friction: 14 }}>
      {content[textIndex % content.length]}
    </TextTransition>
  );
};

export default TextSlider;
