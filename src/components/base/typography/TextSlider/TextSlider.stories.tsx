import { Meta, Story } from '@storybook/react';
import {
  TextSlider as Component,
  TextSliderProps as Props,
} from 'Components/base/typography/TextSlider';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Typography/Text Slider',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const TextSliderComponent: Story<Props> = (args) => <Component {...args} />;

export const TextSlider = TextSliderComponent.bind({});
TextSlider.args = {
  content: ['First', 'Second', 'Third', 'Fourth', 'Fifth', 'Sixth'],
};
