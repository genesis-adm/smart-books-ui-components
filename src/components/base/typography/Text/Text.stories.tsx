import { Meta, Story } from '@storybook/react';
import { Text as Component, TextProps as Props } from 'Components/base/typography/Text';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Typography/Text',
  component: Component,
  argTypes: {
    className: hideProperty,
    style: hideProperty,
  },
} as Meta;

const TextComponent: Story<Props> = (args) => (
  <Component {...args}>text example with BIG letters</Component>
);

export const Text = TextComponent.bind({});
Text.args = {
  type: 'body-regular-16',
  color: 'black-100',
  align: 'center',
  transform: 'none',
  display: 'block',
  noWrap: false,
  transition: 'default',
  selectText: false,
  lineClamp: '1',
};
