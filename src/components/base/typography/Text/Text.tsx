import classNames from 'classnames';
import React from 'react';

import { TestableProps } from '../../../../types/general';
import type { TextColorNewTypes } from '../../../types/colorTypes';
import type { FontTypes, LineClamp, TextAlign, TextTransform } from '../../../types/fontTypes';
import type { Display } from '../../../types/gridTypes';
import styles from './Text.module.scss';

export interface TextProps extends TestableProps {
  type?: FontTypes | 'avatar-small' | 'avatar-big' | 'filename';
  color?: TextColorNewTypes;
  align?: TextAlign;
  display?: Display;
  transform?: TextTransform;
  transition?: 'default' | 'inherit' | 'unset';
  noWrap?: boolean;
  lineClamp?: LineClamp;
  selectText?: boolean;
  className?: string;
  style?: React.CSSProperties;
  children?: React.ReactNode;
}

const variantMapping = {
  inherit: 'span',
  'h1-bold': 'h1',
  'h1-semibold': 'h1',
  'h2-bold': 'h2',
  'h2-semibold': 'h2',
  'title-bold': 'h3',
  'title-semibold': 'h3',
  'title-regular': 'h3',
  'body-medium': 'p',
  'body-regular-16': 'p',
  'body-bold-14': 'p',
  'body-regular-14': 'p',
  button: 'span',
  'caption-uppercase': 'span',
  'caption-bold': 'span',
  'caption-semibold': 'span',
  'caption-regular': 'span',
  'caption-regular-height-20': 'span',
  'caption-regular-height-24': 'span',
  'text-medium': 'span',
  'text-regular': 'span',
  'subtext-bold': 'span',
  'subtext-semibold': 'span',
  'subtext-medium': 'span',
  'subtext-regular': 'span',
  'avatar-small': 'span',
  'avatar-big': 'span',
  filename: 'span',
};

const Text = React.forwardRef<HTMLElement, TextProps>(
  (
    {
      type = 'text-regular',
      color = 'black-100',
      align = 'left',
      display,
      transform,
      transition = 'default',
      noWrap,
      lineClamp,
      selectText,
      className,
      style,
      children,
      dataTestId,
    },
    ref,
  ) => {
    const tag = variantMapping[type];

    return React.createElement(
      tag,
      {
        ref,
        'data-testid': dataTestId,
        style: style,
        className: classNames(
          styles.text,
          styles[type],
          styles[align],
          {
            [styles[`display_${display}`]]: display,
            [styles[`transition_${transition}`]]: transition,
            [styles[`line-clamp_${lineClamp}`]]: lineClamp,
            [styles.noWrap]: noWrap,
            [styles.noSelect]: !selectText,
          },
          styles[`color_${color}`],
          styles[`text-align_${align}`],
          styles[`text-transform_${transform}`],
          className,
        ),
      },
      children,
    );
  },
);

Text.displayName = 'Text';

export default Text;
