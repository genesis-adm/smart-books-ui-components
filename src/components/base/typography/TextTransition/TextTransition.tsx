import { SpringConfig, animated, config, useSpring, useTransition } from '@react-spring/web';
import React, { CSSProperties, useEffect, useRef, useState } from 'react';

export interface TextTransitionProps {
  direction?: 'up' | 'down';
  inline?: boolean;
  springConfig?: SpringConfig;
  delay?: number;
  style?: CSSProperties;
  className?: string;
  children?: React.ReactNode;
}

const TextTransition: React.FC<TextTransitionProps> = ({
  direction = 'up',
  inline,
  springConfig = config.default,
  delay,
  style,
  className,
  children,
}) => {
  const initialRun = useRef<boolean>(true);
  const transitions = useTransition([children], {
    from: { opacity: 0, transform: `translateY(${direction === 'down' ? '-100%' : '100%'})` },
    enter: { opacity: 1, transform: 'translateY(0%)' },
    leave: {
      opacity: 0,
      transform: `translateY(${direction === 'down' ? '100%' : '-100%'})`,
      position: 'absolute',
    },
    config: springConfig,
    immediate: initialRun.current,
    delay: !initialRun.current ? delay : undefined,
  });

  const [width, setWidth] = useState<number>(0);
  const textElementRef = useRef<HTMLDivElement>(null);
  const heightRef = useRef<number | string>('auto');

  useEffect(() => {
    initialRun.current = false;
    const elem = textElementRef.current;
    if (!elem) {
      return;
    }
    const { width, height } = elem.getBoundingClientRect();
    setWidth(width);
    heightRef.current = height;
  }, [children, setWidth, textElementRef]);

  const widthTransition = useSpring({
    to: { width },
    config: springConfig,
    immediate: initialRun.current,
    delay: !initialRun.current ? delay : undefined,
  });
  return (
    <animated.div
      className={className}
      style={{
        ...(inline && !initialRun.current ? widthTransition : undefined),
        ...style,
        whiteSpace: inline ? 'nowrap' : 'normal',
        display: inline ? 'inline-flex' : 'flex',
        height: heightRef.current,
      }}
    >
      {transitions((styles, item) => (
        <animated.div style={{ ...styles }} ref={item === children ? textElementRef : undefined}>
          {item}
        </animated.div>
      ))}
    </animated.div>
  );
};

export default TextTransition;
