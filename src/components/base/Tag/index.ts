export { default as Tag } from './Tag';
export type { TagProps, TagColorType } from './Tag';
