import { Icon } from 'Components/base/Icon';
import { Tooltip } from 'Components/base/Tooltip';
import React, { ReactNode } from 'react';

import { Nullable } from '../../../../types/general';

type AdditionalIconProps = {
  icon: React.ReactNode;
  tooltip?: Nullable<string | ReactNode>;
  onClick?(): void;
  className?: string;
};
const AdditionalIcon: React.FC<AdditionalIconProps> = ({ icon, tooltip, onClick, className }) => (
  <Tooltip message={tooltip} wrapperClassName={className} onClick={onClick}>
    <Icon icon={icon} path="inherit" />
  </Tooltip>
);

export default AdditionalIcon;
