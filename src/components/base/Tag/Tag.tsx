import { DropDown } from 'Components/DropDown';
import { Icon } from 'Components/base/Icon';
import AdditionalIcon from 'Components/base/Tag/components/AdditionalIcon';
import { Tooltip } from 'Components/base/Tooltip';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as CrossIcon } from 'Svg/v2/16/cross.svg';
import classNames from 'classnames';
import React, { ReactNode, useEffect, useRef, useState } from 'react';

import useHover from '../../../hooks/useHover';
import { TestableProps } from '../../../types/general';
import type { FontTypes, LineClamp, TextAlign } from '../../types/fontTypes';
import style from './Tag.module.scss';

export type TagColorType =
  | 'grey'
  | 'grey-black'
  | 'grey-10'
  | 'grey-10-grey-90'
  | 'green'
  | 'green-10-green-20'
  | 'yellow'
  | 'yellow-10-yellow-20'
  | 'red'
  | 'violet'
  | 'purple'
  | 'violetFilled'
  | 'white'
  | 'black';

export type AdditionalBlock = {
  icon: React.ReactNode;
  tooltip?: React.ReactNode;
  customScroll?: boolean;
  padding: 'none' | '8' | '10' | '16' | '20' | '24' | '32';
  dropdown?: React.ReactNode;
};
export interface TagProps extends TestableProps {
  truncateText?: boolean;
  deleteFlow?: boolean;
  isNotClickable?: boolean;
  size?: '16' | '24' | '32' | '48';
  padding?: '3' | '4' | '0-4' | '2-8' | '4-8' | '8';
  shape?: 'square' | 'circle';
  color?: TagColorType;
  icon?: React.ReactNode;
  staticIconColor?: boolean;
  iconPosition?: 'left' | 'right';
  text?: string;
  additionalCounter?: number;
  font?: FontTypes;
  lineClamp?: LineClamp;
  align?: TextAlign;
  noWrap?: boolean;
  flexShrink?: '1';
  cursor?: 'default' | 'pointer' | 'move' | 'help' | 'text' | 'inherit';
  tooltip?: React.ReactNode;
  iconTooltip?: React.ReactNode;
  additionalDropdownBlock?: AdditionalBlock;
  additionalIconRight?: React.ReactNode;
  onClickAdditionalIconRight?(): void;
  additionalIconTooltip?: React.ReactNode;
  className?: string;
  onClick?(): void;
  onDelete?(): void;
}

const Tag: React.FC<TagProps> = ({
  truncateText,
  deleteFlow,
  isNotClickable,
  size = '24',
  padding = '4',
  shape = 'square',
  color = 'grey',
  icon,
  iconPosition = 'left',
  staticIconColor,
  text,
  additionalCounter,
  font,
  lineClamp,
  align = 'center',
  flexShrink,
  noWrap,
  cursor,
  tooltip,
  iconTooltip,
  additionalDropdownBlock,
  additionalIconRight,
  onClickAdditionalIconRight,
  additionalIconTooltip,
  className,
  onClick,
  onDelete,
  dataTestId,
}) => {
  const textRef = useRef<HTMLDivElement>(null);
  const wrapperRef = useRef<HTMLDivElement>(null);
  const isWrapperHover = useHover(wrapperRef);
  const [tooltipValue, setTooltipValue] = useState<ReactNode>(tooltip);

  const deleteIconRef = useRef<HTMLDivElement>(null);
  const isDeleteIconHovered = useHover(deleteIconRef);
  const cursorMode = cursor || (isNotClickable && 'default') || (tooltipValue ? 'help' : 'pointer');

  useEffect(() => {
    const refCurrent = textRef.current;
    if (
      !tooltip &&
      refCurrent &&
      (refCurrent.scrollHeight > refCurrent.clientHeight ||
        refCurrent.scrollWidth > refCurrent.clientWidth) &&
      !isWrapperHover
    ) {
      setTooltipValue(text);
    } else {
      setTooltipValue(tooltip);
    }
  }, [tooltip, textRef, text, isWrapperHover]);

  return (
    <div
      className={classNames(style.container, { [style.width]: truncateText })}
      data-testid={dataTestId}
    >
      <div
        className={classNames(
          style.component,
          style[`size_${size}`],
          style[`color_${color}`],
          style[`padding_${padding}`],
          style[`shape_${shape}`],
          {
            [style.isNotClickable]: isNotClickable,
            [style[`color_red`]]: isDeleteIconHovered,
            [style[`flexShrink_${flexShrink}`]]: flexShrink,
            [style.width]: truncateText && !className?.includes('w'),
          },
          className,
        )}
        onClick={onClick}
      >
        <div
          className={classNames(style.body, {
            [style[`icon_${iconPosition}`]]: icon && iconPosition,
          })}
        >
          <Tooltip
            ref={iconTooltip ? wrapperRef : undefined}
            message={iconTooltip}
            wrapperClassName={classNames({ [style.tooltip_wrapper]: icon && truncateText })}
            style={iconTooltip ? { minWidth: 'auto' } : undefined}
          >
            {icon && <Icon icon={icon} staticColor={staticIconColor} path="inherit" />}
          </Tooltip>
          {text && (
            <Tooltip
              message={tooltipValue}
              innerClassName={style.tooltip_message}
              cursor={cursorMode}
            >
              <Text
                ref={textRef}
                type={font}
                lineClamp={lineClamp}
                noWrap={noWrap || truncateText}
                align={align}
                color="inherit"
                transition="unset"
              >
                {text}
              </Text>
            </Tooltip>
          )}
          {deleteFlow && (
            <Tooltip
              ref={deleteIconRef}
              message={'Delete'}
              wrapperClassName={classNames({ [style.tooltip_wrapper]: truncateText })}
              style={{ minWidth: 'auto' }}
              cursor="pointer"
              onClick={onDelete}
            >
              <Icon icon={<CrossIcon />} path="inherit" />
            </Tooltip>
          )}
        </div>
        {additionalCounter && (
          <>
            <span className={style.divider} />
            <Text type={font} color="inherit">
              +{additionalCounter}
            </Text>
          </>
        )}
      </div>
      {additionalIconRight && (
        <AdditionalIcon
          icon={additionalIconRight}
          tooltip={additionalIconTooltip}
          onClick={onClickAdditionalIconRight}
          className={classNames(
            style.component,
            style.icon_tooltip,
            style[`size_${size}`],
            style[`color_${color}`],
            style[`padding_${padding}`],
            style[`shape_${shape}`],
            {
              [style[`flexShrink_${flexShrink}`]]: flexShrink,
            },
          )}
        />
      )}
      {additionalDropdownBlock && (
        <DropDown
          flexdirection="column"
          padding={additionalDropdownBlock.padding}
          customScroll={additionalDropdownBlock.customScroll}
          gap="8"
          control={({ handleOpen }) => (
            <AdditionalIcon
              icon={additionalDropdownBlock?.icon}
              onClick={handleOpen}
              tooltip={additionalDropdownBlock?.tooltip}
              className={classNames(
                style.component,
                style.icon_tooltip,
                style[`size_${size}`],
                style[`color_${color}`],
                style[`padding_${padding}`],
                style[`shape_${shape}`],
                {
                  [style[`flexShrink_${flexShrink}`]]: flexShrink,
                },
              )}
            />
          )}
        >
          {additionalDropdownBlock.dropdown}
        </DropDown>
      )}
    </div>
  );
};

export default Tag;
