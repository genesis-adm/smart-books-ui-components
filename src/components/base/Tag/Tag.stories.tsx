import { Meta, Story } from '@storybook/react';
import { Tag as Component, TagProps as Props } from 'Components/base/Tag';
import { Container } from 'Components/base/grid/Container';
import { ReactComponent as WarningIcon } from 'Svg/v2/16/warning.svg';
import React from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Tag',
  component: Component,
  argTypes: {
    icon: hideProperty,
    className: hideProperty,
  },
} as Meta;

const TagComponent: Story<Props> = (args) => (
  <Container className={spacing.w100}>
    <Component {...args} />
  </Container>
);

export const Tag = TagComponent.bind({});
Tag.args = {
  text: '+3wehufqwu[hfu9qhfuwehfwurhgwyurhgyruwhgw',
  tooltip: 'Info tooltip',
  color: 'yellow',
  size: '24',
  padding: '8',
  font: 'caption-regular',
  icon: <WarningIcon />,
  cursor: 'default',
  shape: 'square',
  additionalDropdownBlock: {
    icon: <WarningIcon />,
    padding: '8',
    tooltip: 'Rule: Rule name',
  },
  truncateText: true,
};
