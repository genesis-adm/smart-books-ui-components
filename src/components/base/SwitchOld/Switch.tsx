import classNames from 'classnames';
import React from 'react';

import style from './Switch.module.scss';

interface SwitchProps {
  id: string;
  checked?: boolean;
  disabled?: boolean;
  className?: string;
  onChange?: (checked: boolean) => void;
}

const Switch: React.FC<SwitchProps> = ({ className, checked = false, disabled, onChange, id }) => {
  const handleChange = ({ target }: React.ChangeEvent<HTMLInputElement>) => {
    if (disabled) return;
    onChange?.(target.checked);
  };

  return (
    <input
      id={id}
      className={classNames(style.switch, className, {
        [style.disabled]: disabled,
      })}
      type="checkbox"
      checked={checked}
      onChange={handleChange}
    />
  );
};

Switch.defaultProps = {
  checked: false,
  disabled: false,
  className: '',
  onChange: () => {},
};

export default Switch;
