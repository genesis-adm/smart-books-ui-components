import { ReactNode } from 'react';

export interface DropdownControl {
  isOpen: boolean;
  handleOpen(): void;
}

export interface DialogTooltipProps {
  control: (arg: DropdownControl) => ReactNode;
  title?: string;
  children?: ReactNode;
  className?: string;
  classNameContent?: string;
}
