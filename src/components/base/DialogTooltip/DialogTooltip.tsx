import { IconButton } from 'Components/base/buttons/IconButton';
import { ReactComponent as CloseIcon } from 'Svg/v2/16/cross.svg';
import classNames from 'classnames';
import React, { useRef, useState } from 'react';

import useClickOutside from '../../../hooks/useClickOutside';
import usePositioning from '../../../hooks/usePositioning';
import useScrollOutside from '../../../hooks/useScrollOutside';
import style from './DialogTooltip.module.scss';
import { DialogTooltipProps } from './DialogTooltip.types';

const DialogTooltip: React.FC<DialogTooltipProps> = ({
  title,
  children,
  control,
  className,
  classNameContent,
}: DialogTooltipProps) => {
  const [isOpenedDialog, setIsOpenedDialog] = useState<boolean>(false);

  const wrapperRef = useRef<HTMLDivElement>(null);

  const handleOpen = () => {
    setIsOpenedDialog((prevState) => !prevState);
  };

  useClickOutside({
    ref: wrapperRef.current,
    cb: setIsOpenedDialog,
  });

  useScrollOutside({
    isListOpen: isOpenedDialog,
    cb: setIsOpenedDialog,
  });

  const { styles: positionStyles, mode } = usePositioning(wrapperRef.current, 'auto');

  return (
    <div ref={wrapperRef} className={classNames(style.component, className)}>
      {control({
        handleOpen,
        isOpen: isOpenedDialog,
      })}
      {isOpenedDialog && (
        <div style={positionStyles} className={classNames(style.dialogContainer, classNameContent)}>
          <div className={classNames(style.dialogHeader)}>
            <span className={classNames(style.headerTitle)}>{title}</span>
            <IconButton
              icon={<CloseIcon />}
              size="24"
              background={'transparent-grey-20'}
              tooltip="Close"
              color="grey-100"
              onClick={handleOpen}
            />
          </div>
          <div
            aria-label="dialogContainer"
            className={classNames(style.dialogContent, style[`direction_${mode}`])}
          >
            {children}
          </div>
        </div>
      )}
    </div>
  );
};

export default DialogTooltip;
