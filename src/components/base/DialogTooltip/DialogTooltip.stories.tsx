import { Meta, Story } from '@storybook/react';
import {
  DialogTooltip as Component,
  DialogTooltipProps as Props,
} from 'Components/base/DialogTooltip';
import { Icon } from 'Components/base/Icon';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as InfoIcon } from 'Svg/v2/16/info.svg';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/base/DialogTooltip',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const DialogTooltipComponent: Story<Props> = (args) => (
  <Component
    {...args}
    control={({ handleOpen }) => <Icon icon={<InfoIcon />} onClick={handleOpen} />}
  >
    <Text>Hello, I am dialog content</Text>
  </Component>
);

export const DialogTooltip = DialogTooltipComponent.bind({});
DialogTooltip.args = {
  title: 'How to find?',
};
