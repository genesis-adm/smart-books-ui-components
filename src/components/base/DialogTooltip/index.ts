export { default as DialogTooltip } from './DialogTooltip';
export type { DialogTooltipProps } from './DialogTooltip.types';
