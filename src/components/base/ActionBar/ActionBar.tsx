import classNames from 'classnames';
import React from 'react';

import { TestableProps } from '../../../types/general';
import style from './ActionBar.module.scss';

export interface ActionBarProps extends TestableProps {
  className?: string;
  children?: React.ReactNode;
}

const ActionBar: React.FC<ActionBarProps> = ({ className, children, dataTestId }) => (
  <div className={classNames(style.wrapper, className)} data-testid={dataTestId}>
    <div className={classNames(style.bar)}>{children}</div>
  </div>
);

export default ActionBar;
