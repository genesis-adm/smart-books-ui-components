import { Meta, Story } from '@storybook/react';
import { ActionBar as Component, ActionBarProps as Props } from 'Components/base/ActionBar/index';
import { Divider } from 'Components/base/Divider';
import { Icon } from 'Components/base/Icon';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as MinusIcon } from 'Svg/16/minus-light.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

export default {
  title: 'Components/base/Action Bar',
  component: Component,
  argTypes: {
    className: {
      table: {
        disable: true,
      },
    },
  },
} as Meta;

const ActionBarComponent: Story<Props> = (args) => {
  const counter = 5;
  return (
    <Component {...args}>
      <Container gap="40">
        <Container alignitems="center" gap="12">
          <Container
            className={classNames(spacing.w16, spacing.h16)}
            background={'grey-100'}
            border="grey-90"
            alignitems="center"
            justifycontent="center"
            borderRadius="2"
          >
            <Icon icon={<MinusIcon />} />
          </Container>
          <Text type="body-regular-14" color={'grey-90'}>
            Selected lines:&nbsp;
            {counter}
          </Text>
        </Container>

        <Container gap="8" alignitems="center">
          <Button
            background={'black-90-grey-20'}
            color="grey-20-black-90"
            width="88"
            borderRadius="10"
            height="32"
            iconLeft={<TrashIcon />}
            onClick={() => {}}
          >
            Exclude
          </Button>
          <Divider type="vertical" height="20" />
          <Button
            background={'black-90-grey-20'}
            color="grey-20-black-90"
            borderRadius="10"
            width="auto"
            height="32"
            iconLeft={<TrashIcon />}
            onClick={() => {}}
          >
            Delete split
          </Button>
          <Divider type="vertical" height="20" />
          <Button
            background={'black-90-grey-20'}
            color="grey-20-black-90"
            borderRadius="10"
            width="auto"
            height="32"
            iconLeft={<TrashIcon />}
            onClick={() => {}}
          >
            Delete transaction
          </Button>
        </Container>
      </Container>
    </Component>
  );
};

export const ActionBar = ActionBarComponent.bind({});
ActionBarComponent.args = {};
