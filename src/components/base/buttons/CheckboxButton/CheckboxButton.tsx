import { Checkbox } from 'Components/base/Checkbox';
import classNames from 'classnames';
import React from 'react';

import type { FontTypes } from '../../../types/fontTypes';
import style from './CheckboxButton.module.scss';

export interface CheckboxButtonProps {
  name: string;
  label: string;
  checked: boolean;
  font?: FontTypes;
  className?: string;
  onChange(): void;
}

const CheckboxButton: React.FC<CheckboxButtonProps> = ({
  name,
  label,
  checked,
  font = 'text-regular',
  className,
  onChange,
}) => {
  const handleChange = () => {
    onChange?.();
  };
  return (
    <div
      role="presentation"
      className={classNames(
        style.wrapper,
        {
          [style.checked]: checked,
        },
        className,
      )}
      onClick={handleChange}
    >
      <Checkbox
        name={name}
        font={font}
        textcolor="inherit"
        checked={checked}
        onChange={handleChange}
      >
        {label}
      </Checkbox>
    </div>
  );
};

export default CheckboxButton;
