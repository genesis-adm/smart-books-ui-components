import { Meta, Story } from '@storybook/react';
import {
  CheckboxButton as Component,
  CheckboxButtonProps as Props,
} from 'Components/base/buttons/CheckboxButton';
import React, { useState } from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Buttons/CheckboxButton',
  component: Component,
  argTypes: {
    name: hideProperty,
    checked: hideProperty,
    className: hideProperty,
    onChange: hideProperty,
  },
} as Meta;

const CheckboxButtonComponent: Story<Props> = (args) => {
  const [active, setActive] = useState(false);
  return (
    <Component
      {...args}
      checked={active}
      onChange={() => {
        setActive((prevState) => !prevState);
      }}
    />
  );
};

export const CheckboxButton = CheckboxButtonComponent.bind({});
CheckboxButton.args = {
  name: 'checkbox-button',
  label: 'Test label',
  font: 'text-regular',
};
