import { Meta, Story } from '@storybook/react';
import {
  RadioButtonStoplight as Component,
  RadioButtonStoplightProps as Props,
} from 'Components/base/buttons/RadioButtonStoplight';
import { Container } from 'Components/base/grid/Container';
import React, { useState } from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Buttons/RadioButtonStoplight',
  component: Component,
  argTypes: {
    id: hideProperty,
    name: hideProperty,
    checked: hideProperty,
    color: hideProperty,
    className: hideProperty,
    onChange: hideProperty,
  },
} as Meta;

const RadioButtonStoplightComponent: Story<Props> = (args) => {
  const [active, setActive] = useState<number | null>(null);

  return (
    <Container flexdirection="column" gap="4">
      {Array.from(Array(3)).map((_, index) => (
        <Component
          key={index}
          {...args}
          id={`${index}`}
          color={index === 0 ? 'error' : index === 1 ? 'warning' : 'success'}
          checked={active === index}
          onChange={() => setActive(index)}
        />
      ))}
    </Container>
  );
};

export const RadioButtonStoplight = RadioButtonStoplightComponent.bind({});
RadioButtonStoplight.args = {
  type: 'box',
  name: 'storybook',
  disabled: false,
};
