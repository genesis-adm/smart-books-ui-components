import classNames from 'classnames';
import React, { ReactElement } from 'react';

import style from './RadioButtonStoplight.module.scss';

export interface RadioButtonStoplightProps {
  id: string;
  name: string;
  checked?: boolean;
  disabled?: boolean;
  color: 'success' | 'warning' | 'error';
  type?: 'radio' | 'box';
  className?: string;
  onChange(e: React.FormEvent<HTMLInputElement>): void;
}

const RadioButtonStoplight = ({
  id,
  name,
  checked,
  disabled,
  type = 'box',
  color,
  className,
  onChange,
}: RadioButtonStoplightProps): ReactElement => (
  <label
    htmlFor={id}
    className={classNames(
      style.wrapper,
      style[`type_${type}`],
      style[`color_${color}`],
      { [style.checked]: checked, [style.disabled]: disabled },
      className,
    )}
  >
    <input
      id={id}
      className={classNames(style.radio, { [style.disabled]: disabled })}
      type="radio"
      name={name}
      checked={checked}
      onChange={onChange}
      disabled={disabled}
    />
  </label>
);

export default RadioButtonStoplight;
