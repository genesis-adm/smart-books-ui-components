import { Meta, Story } from '@storybook/react';
import {
  LinkButton as Component,
  LinkButtonProps as Props,
} from 'Components/base/buttons/LinkButton';
import { ReactComponent as DownloadIcon } from 'Svg/v2/16/download.svg';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Buttons/Link Button',
  component: Component,
  argTypes: {
    icon: hideProperty,
    className: hideProperty,
    onClick: hideProperty,
  },
} as Meta;

const LinkButtonComponent: Story<Props> = (args) => (
  <Component icon={<DownloadIcon />} {...args}>
    Link text
  </Component>
);

export const LinkButton = LinkButtonComponent.bind({});
LinkButton.args = {
  type: 'button',
  color: 'violet-90-violet-100',
  iconRight: false,
  disabled: false,
};
