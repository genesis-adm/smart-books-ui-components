import { Tooltip } from 'Components/base/Tooltip';
import classNames from 'classnames';
import React, { ReactNode } from 'react';

import { TestableProps } from '../../../../types/general';
import type { TextColorNewTypes } from '../../../types/colorTypes';
import type { FontTypes } from '../../../types/fontTypes';
import style from './LinkButton.module.scss';

export interface LinkButtonProps extends TestableProps {
  secondLabel?: string;
  icon?: React.ReactNode;
  iconRight?: boolean;
  color?: TextColorNewTypes;
  secondLabelColor?: TextColorNewTypes;
  type?: FontTypes;
  noWrap?: boolean;
  disabled?: boolean;
  tooltip?: ReactNode;
  className?: string;
  tooltipWrapperClassName?: string;
  children?: React.ReactNode;
  onClick(e: React.MouseEvent<HTMLButtonElement>): void;
}

const LinkButton: React.FC<LinkButtonProps> = ({
  secondLabel,
  icon,
  iconRight,
  type = 'button',
  color = 'violet-100',
  secondLabelColor = color,
  noWrap,
  disabled,
  tooltip,
  className,
  tooltipWrapperClassName,
  children,
  dataTestId,
  onClick,
}) => (
  <Tooltip message={tooltip} wrapperClassName={tooltipWrapperClassName}>
    <button
      type="button"
      onClick={onClick}
      disabled={disabled}
      className={classNames(
        style.link,
        style[type],
        {
          [style.disabled]: disabled,
        },
        className,
      )}
      data-testid={dataTestId}
    >
      <div className={classNames(style.main_text_container, style[`color_${color}`])}>
        {icon && !iconRight && <span className={style.icon}>{icon}</span>}
        <span className={classNames(style.text, { [style.noWrap]: noWrap })}>{children}</span>
        {icon && iconRight && (
          <span className={classNames(style.icon, [style.icon_right])}>{icon}</span>
        )}
      </div>
      {secondLabel && (
        <span className={classNames(style.second_text, style[`color_${secondLabelColor}`])}>
          {secondLabel}
        </span>
      )}
    </button>
  </Tooltip>
);

export default LinkButton;
