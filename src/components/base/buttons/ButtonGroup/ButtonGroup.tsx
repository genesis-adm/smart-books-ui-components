import classNames from 'classnames';
import React from 'react';

import style from './ButtonGroup.module.scss';

export interface ButtonGroupProps {
  align?: 'left' | 'center' | 'right';
  size?: 'auto' | 'full';
  margin?: 'small' | 'big';
  nospace?: boolean;
  className?: string;
  children?: React.ReactNode;
}

const ButtonGroup: React.FC<ButtonGroupProps> = ({
  align = 'left',
  nospace = false,
  size = 'auto',
  margin = 'big',
  className,
  children,
}) => (
  <div
    className={classNames(
      style.group,
      style[align],
      style[size],
      style[`margin_${margin}`],
      className,
      {
        [style.nospace]: nospace,
      },
    )}
  >
    {children}
  </div>
);

export default ButtonGroup;
