import { DropDown, DropdownPositionType } from 'Components/DropDown';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as CaretIcon } from 'Svg/v2/16/caret-down.svg';
import classNames from 'classnames';
import React from 'react';

import { TestableProps } from '../../../../types/general';
import type {
  BackgroundColorNewTypes,
  BorderColorNewTypes,
  TextColorNewTypes,
} from '../../../types/colorTypes';
import type { FontTypes } from '../../../types/fontTypes';
import style from './Button.module.scss';

export interface ButtonProps extends TestableProps {
  background?: BackgroundColorNewTypes;
  color?: TextColorNewTypes;
  border?: BorderColorNewTypes;
  borderRadius?: 'default' | '10' | '16';
  dropdownBorder?: 'white-100' | 'grey-20-grey-90';
  dashed?: 'inherit' | 'none' | 'static' | 'hover' | 'always';
  width?:
    | 'xs-auto'
    | 'xs'
    | 'sm'
    | 'md'
    | 'lg'
    | 'xl'
    | 'xxl'
    | 'full'
    | 'auto'
    | '40'
    | '50'
    | '64'
    | '80'
    | '88'
    | '100'
    | '108'
    | '120'
    | '128'
    | '134'
    | '140'
    | '150'
    | '156'
    | '180'
    | '196'
    | '215'
    | '220'
    | '230';
  height?:
    | '24'
    | '28'
    | '30'
    | '32'
    | '40'
    | '44'
    | '48'
    | '50'
    | 'tiny'
    | 'small'
    | 'medium'
    | 'large';
  padding?: 'default' | 'none' | 'minimal' | '8';
  textTransition?: 'unset' | 'inherit';
  font?: FontTypes;
  align?: 'left' | 'center' | 'right';
  shadow?: 'default';
  iconLeft?: React.ReactNode;
  iconRight?: React.ReactNode;
  sourceIconColorLeft?: boolean;
  sourceIconColorRight?: boolean;
  navigation?: boolean;
  disabled?: boolean;
  disabledDropdown?: boolean;
  enablePropagation?: boolean;
  dropdown?: React.ReactNode;
  dropdownPosition?: DropdownPositionType;
  className?: string;
  submit?: boolean;
  onlyIcon?: boolean;
  children?: React.ReactNode;
  onClick?(e: React.MouseEvent<HTMLButtonElement>): void;
}

const Button: React.FC<ButtonProps> = ({
  background = 'violet-90-violet-100',
  color = 'white-100',
  border = 'none',
  borderRadius = 'default',
  dropdownBorder = 'white-100',
  dashed,
  width = 'auto',
  height = 'large',
  padding = 'default',
  textTransition = 'unset',
  font = 'button',
  align = 'center',
  shadow,
  iconLeft,
  iconRight,
  sourceIconColorLeft,
  sourceIconColorRight,
  navigation,
  disabled,
  disabledDropdown,
  dropdown,
  dropdownPosition,
  className,
  submit,
  onlyIcon,
  onClick,
  children,
  enablePropagation,
  dataTestId,
}) => (
  <div className={style.buttonWrapper} data-testid={dataTestId}>
    {!!disabled && enablePropagation && <div className={style.disabledClickable} />}
    <button
      type={submit ? 'submit' : 'button'}
      className={classNames(
        style.button,
        style[`width_${width}`],
        style[`height_${height}`],
        style[`background_${background}`],
        style[`color_${color}`],
        style[`border_${border}`],
        style[`borderRadius_${borderRadius}`],
        style[`dashed_${dashed}`],
        style[`padding_${padding}`],
        style[`align_${align}`],
        {
          [style[`shadow_${shadow}`]]: shadow,
          [style.disabled]: disabled && !dropdown,
          [style.disabled_button]: disabled && dropdown,
          [style.navigation]: navigation,
          [style.onlyIcon]: onlyIcon,
          [style.dropdown]: dropdown,
        },
        className,
      )}
      disabled={disabled}
      onClick={onClick}
    >
      {iconLeft && (
        <span
          className={classNames(style.icon, style.icon_left, {
            [style.icon_color]: !sourceIconColorLeft,
          })}
        >
          {iconLeft}
        </span>
      )}
      {children && (
        <Text
          className={classNames(style.text, style[`text-transition_${textTransition}`])}
          type={font}
          align="center"
          color="inherit"
        >
          {children}
        </Text>
      )}
      {iconRight && (
        <span
          className={classNames(style.icon, style.icon_right, {
            [style.icon_color]: !sourceIconColorRight,
          })}
        >
          {iconRight}
        </span>
      )}
      {dropdown && (
        <DropDown
          classNameOuter={style.dropdown_wrapper}
          flexdirection="column"
          padding="8"
          position={dropdownPosition}
          control={({ handleOpen }) => (
            <div
              className={classNames(style.trigger, style[`border-left_${dropdownBorder}`], {
                [style.disable_dropdown]: disabledDropdown,
                [style.enable]: disabled && !disabledDropdown,
              })}
              role="presentation"
              onClick={(e: React.MouseEvent<HTMLDivElement>) => {
                e.stopPropagation();
                handleOpen();
              }}
            >
              <CaretIcon />
            </div>
          )}
        >
          {!disabledDropdown && dropdown}
        </DropDown>
      )}
    </button>
  </div>
);

export default Button;
