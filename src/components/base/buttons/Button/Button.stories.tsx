import { Meta, Story } from '@storybook/react';
import { DropDownItem } from 'Components/DropDownItem';
import { Button as Component, ButtonProps as Props } from 'Components/base/buttons/Button';
import { ReactComponent as DownloadIcon } from 'Svg/v2/16/download.svg';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Buttons/Button',
  component: Component,
  argTypes: {
    iconLeft: hideProperty,
    iconRight: hideProperty,
    onlyIcon: hideProperty,
    textTransition: hideProperty,
    dropdown: hideProperty,
    className: hideProperty,
    onClick: hideProperty,
  },
} as Meta;

const ButtonTemplate: Story<Props> = (args) => (
  <Component iconLeft={<DownloadIcon />} {...args}>
    Download file
  </Component>
);

const ButtonDropdownTemplate: Story<Props> = (args) => (
  <Component
    iconLeft={<DownloadIcon />}
    dropdown={
      <>
        <DropDownItem type="option" width="240" icon={<DownloadIcon />} onClick={() => {}}>
          Download
        </DropDownItem>
        <DropDownItem type="option" width="240" icon={<DownloadIcon />} onClick={() => {}}>
          Download
        </DropDownItem>
      </>
    }
    {...args}
  >
    Download file
  </Component>
);

export const Button = ButtonTemplate.bind({});
Button.args = {
  background: 'violet-90-violet-100',
  color: 'white-100',
  border: 'none',
  dashed: 'inherit',
  width: 'xl',
  height: 'large',
  submit: false,
  disabled: false,
  navigation: false,
  textTransition: 'unset',
  padding: 'default',
  sourceIconColorLeft: false,
  sourceIconColorRight: false,
};

export const ButtonWithDropdown = ButtonDropdownTemplate.bind({});
ButtonWithDropdown.args = {
  background: 'violet-90-violet-100',
  color: 'white-100',
  dropdownBorder: 'white-100',
  border: 'none',
  dashed: 'inherit',
  width: 'xl',
  height: 'large',
  submit: false,
  disabled: false,
  navigation: false,
  textTransition: 'unset',
  padding: 'default',
  sourceIconColorLeft: false,
  sourceIconColorRight: false,
};
