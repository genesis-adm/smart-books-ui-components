import { DropDownContext } from 'Components/DropDown/DropDown';
import { Tooltip } from 'Components/base/Tooltip';
import { Text } from 'Components/base/typography/Text';
import { FontTypes } from 'Components/types/fontTypes';
import { ReactComponent as ChevronIcon16 } from 'Svg/v2/16/chevron-right.svg';
import classNames from 'classnames';
import React, { useContext } from 'react';

import { TestableProps } from '../../../../types/general';
import style from './DropDownButton.module.scss';

export interface DropDownButtonProps extends TestableProps {
  type?: 'default' | 'danger';
  size?: 'auto' | '160' | '204' | '224';
  fontType?: FontTypes;
  icon?: React.ReactNode;
  tooltip?: string;
  className?: string;
  children?: React.ReactNode;
  disabled?: boolean;
  onClick?(event: React.MouseEvent<HTMLDivElement>): void;
  drop?: boolean;
}

const DropDownButton: React.FC<DropDownButtonProps> = ({
  type = 'default',
  size,
  fontType = 'caption-regular',
  icon,
  tooltip,
  onClick,
  className,
  children,
  disabled,
  dataTestId,
  drop,
}) => {
  const { handleOpen } = useContext(DropDownContext);

  const handleClick = (event: React.MouseEvent<HTMLDivElement>) => {
    event.stopPropagation();
    !disabled && onClick?.(event);

    if (drop) return;

    if (handleOpen) handleOpen();
  };

  return (
    <Tooltip message={tooltip}>
      <div
        data-testid={dataTestId}
        role="presentation"
        onClick={handleClick}
        className={classNames(
          style.button,
          style[type],
          {
            [style[`size_${size}`]]: size,
            [style.disabled]: disabled,
          },
          className,
        )}
      >
        {!!icon && <span className={style.icon}>{icon}</span>}

        <Text type={fontType} color="grey-100" noWrap className={style.text}>
          {children}
        </Text>

        {!!drop && (
          <div className={style.dropIcon}>
            <ChevronIcon16 />
          </div>
        )}
      </div>
    </Tooltip>
  );
};

export default DropDownButton;
