import { Meta, Story } from '@storybook/react';
import {
  DropDownButton as Component,
  DropDownButtonProps as Props,
} from 'Components//base/buttons/DropDownButton';
import { ReactComponent as DownloadIcon } from 'Svg/v2/16/download.svg';
import React from 'react';

export default {
  title: 'Components/Base/Buttons/DropDownButton',
  component: Component,
  argTypes: {},
} as Meta;

const DropDownButtonComponent: Story<Props> = (args) => <Component {...args} />;

export const DropDownButton = DropDownButtonComponent.bind({});
DropDownButton.args = {
  type: 'default',
  size: 'auto',
  icon: <DownloadIcon />,
  onClick: () => {},
  disabled: false,
};
