import { Meta, Story } from '@storybook/react';
import { DropDownItem } from 'Components/DropDownItem';
import {
  ButtonWithDropdown as Component,
  ButtonWithDropdownProps as Props,
} from 'Components/base/buttons/ButtonWithDropdown';
import { ReactComponent as DownloadIcon } from 'Svg/v2/16/download.svg';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/base/buttons/ButtonWithDropdown',
  component: Component,
  argTypes: {
    iconLeft: hideProperty,
    iconRight: hideProperty,
    onlyIcon: hideProperty,
    textTransition: hideProperty,
    dropdown: hideProperty,
    className: hideProperty,
    onClick: hideProperty,
  },
} as Meta;

const ButtonWithDropdownComponent: Story<Props> = (args) => <Component {...args}>Import</Component>;

export const ButtonWithDropdown = ButtonWithDropdownComponent.bind({});
ButtonWithDropdown.args = {
  background: 'violet-90-violet-100',
  color: 'white-100',
  dropdownBorder: 'white-100',
  border: 'none',
  dashed: 'inherit',
  mainActionButtonWidth: '80',
  height: '24',
  submit: false,
  disabled: false,
  navigation: false,
  textTransition: 'unset',
  padding: 'default',
  sourceIconColorLeft: false,
  sourceIconColorRight: false,
  dropdown: (
    <>
      <DropDownItem type="option" width="240" icon={<DownloadIcon />} onClick={() => {}}>
        Download
      </DropDownItem>
      <DropDownItem type="option" width="240" icon={<DownloadIcon />} onClick={() => {}}>
        Download
      </DropDownItem>
    </>
  ),
};
