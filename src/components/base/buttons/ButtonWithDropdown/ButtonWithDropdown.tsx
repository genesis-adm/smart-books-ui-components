import { DropDown } from 'Components/DropDown';
import { Tooltip } from 'Components/base/Tooltip';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as CaretIcon } from 'Svg/v2/16/caret-down.svg';
import classNames from 'classnames';
import React, { ReactElement } from 'react';

import style from './ButtonWithDropdown.module.scss';
import { ButtonWithDropdownProps } from './ButtonWithDropdown.types';

// Reminder: do not forget to add the following code to root index.ts
// export { ButtonWithDropdown } from './components/base/buttons/ButtonWithDropdown';

const ButtonWithDropdown = ({
  background = 'violet-90-violet-100',
  color = 'white-100',
  border = 'none',
  dropdownBorder = border,
  dashed,
  mainActionButtonWidth,
  height = '50',
  padding = 'default',
  textTransition = 'unset',
  font = 'button',
  align = 'center',
  shadow,
  iconLeft,
  iconRight,
  sourceIconColorLeft,
  sourceIconColorRight,
  navigation,
  disabled,
  disabledDropdown,
  dropdown,
  dropdownPosition,
  className,
  submit,
  onlyIcon,
  onClick,
  children,
  mainActionTooltipMessage,
  dropdownTooltipMessage,
}: ButtonWithDropdownProps): ReactElement => {
  const handleClick = (e: React.MouseEvent<HTMLButtonElement>) => {
    if (!disabled && onClick) {
      onClick(e);
    }
  };

  return (
    <div
      className={classNames(
        style.dropdown,
        style[`height_${height}`],
        style[`width_${mainActionButtonWidth}`],
      )}
    >
      <Tooltip message={mainActionTooltipMessage} wrapperClassName={style.tooltip}>
        <button
          type={submit ? 'submit' : 'button'}
          className={classNames(
            style.button,
            style[`background_${background}`],
            style[`color_${color}`],
            style[`border_${border}`],
            style[`dashed_${dashed}`],
            style[`padding_${padding}`],
            style[`align_${align}`],
            {
              [style[`shadow_${shadow}`]]: shadow,
              [style.disabled_button]: disabled && dropdown,
              [style.navigation]: navigation,
              [style.onlyIcon]: onlyIcon,
            },
            className,
          )}
          onClick={handleClick}
        >
          {iconLeft && (
            <span
              className={classNames(style.icon, style.icon_left, {
                [style.icon_color]: !sourceIconColorLeft,
              })}
            >
              {iconLeft}
            </span>
          )}
          {children && (
            <Text
              className={classNames(style.text, style[`text-transition_${textTransition}`])}
              type={font}
              align="center"
              color="inherit"
            >
              {children}
            </Text>
          )}
          {iconRight && (
            <span
              className={classNames(style.icon, style.icon_right, {
                [style.icon_color]: !sourceIconColorRight,
              })}
            >
              {iconRight}
            </span>
          )}
        </button>
      </Tooltip>

      <DropDown
        classNameOuter={classNames(style.dropdown_wrapper, style[`background_${background}`])}
        flexdirection="column"
        padding="8"
        position={dropdownPosition}
        control={({ handleOpen }) => (
          <Tooltip
            message={dropdownTooltipMessage}
            wrapperClassName={classNames(
              style.dropdown_wrapper,
              style[`background_${background}`],
              style[`color_${color}`],
            )}
          >
            <div
              className={classNames(style.trigger, style[`border-left_${dropdownBorder}`], {
                [style.disable_dropdown]: disabledDropdown,
                [style.enable]: disabled && !disabledDropdown,
              })}
              role="presentation"
              onClick={(e: React.MouseEvent<HTMLDivElement>) => {
                e.stopPropagation();
                if (!disabledDropdown) handleOpen();
              }}
            >
              <CaretIcon />
            </div>
          </Tooltip>
        )}
      >
        {dropdown}
      </DropDown>
    </div>
  );
};

export default ButtonWithDropdown;
