import { DropdownPositionType } from 'Components/DropDown';
import {
  BackgroundColorNewTypes,
  BorderColorNewTypes,
  TextColorNewTypes,
} from 'Components/types/colorTypes';
import { FontTypes } from 'Components/types/fontTypes';
import React from 'react';

export interface ButtonWithDropdownProps {
  className?: string;
  background?: BackgroundColorNewTypes;
  color?: TextColorNewTypes;
  border?: BorderColorNewTypes;
  dropdownBorder?: BorderColorNewTypes;
  dashed?: 'inherit' | 'none' | 'static' | 'hover' | 'always';
  mainActionButtonWidth?:
    | 'xs-auto'
    | 'xs'
    | 'sm'
    | 'md'
    | 'lg'
    | 'xl'
    | 'xxl'
    | 'full'
    | 'auto'
    | '40'
    | '50'
    | '80'
    | '88'
    | '100'
    | '108'
    | '120'
    | '134'
    | '140'
    | '156'
    | '180'
    | '220'
    | '230';
  height?: '24' | '28' | '30' | '32' | '40' | '44' | '48' | '50';
  padding?: 'default' | 'none' | 'minimal' | '8';
  textTransition?: 'unset' | 'inherit';
  font?: FontTypes;
  align?: 'left' | 'center' | 'right';
  shadow?: 'default';
  iconLeft?: React.ReactNode;
  iconRight?: React.ReactNode;
  sourceIconColorLeft?: boolean;
  sourceIconColorRight?: boolean;
  navigation?: boolean;
  disabled?: boolean;
  disabledDropdown?: boolean;
  enablePropagation?: boolean;
  dropdown: React.ReactNode;
  dropdownPosition?: DropdownPositionType;
  submit?: boolean;
  onlyIcon?: boolean;
  children?: React.ReactNode;
  mainActionTooltipMessage?: string;
  dropdownTooltipMessage?: string;
  onClick?(e: React.MouseEvent<HTMLButtonElement>): void;
}
