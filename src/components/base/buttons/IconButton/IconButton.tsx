import { Counter } from 'Components/base/Counter';
import { Divider } from 'Components/base/Divider';
import { Icon } from 'Components/base/Icon';
import { Tooltip } from 'Components/base/Tooltip';
import { ReactComponent as CaretDownIcon } from 'Svg/v2/16/caret-down.svg';
import classNames from 'classnames';
import React from 'react';

import { TestableProps } from '../../../../types/general';
import type {
  BackgroundColorNewTypes,
  BorderColorNewTypes,
  TextColorNewTypes,
} from '../../../types/colorTypes';
import style from './IconButton.module.scss';

export interface IconButtonProps extends TestableProps {
  mode?: 'dropdown';
  size?:
    | 'minimal'
    | '16'
    | '24'
    | '28'
    | '30'
    | '32'
    | '40'
    | '44'
    | '48'
    | '50'
    | '64'
    | 'dropdown'
    | 'xs'
    | 'sm'
    | 'md'
    | 'lg';
  shape?: 'square' | 'circle';
  background?: BackgroundColorNewTypes;
  color?: TextColorNewTypes;
  border?: BorderColorNewTypes;
  borderRadius?: '20' | 'default';
  icon: React.ReactNode;
  additionalIcon?: React.ReactNode;
  tooltip?: React.ReactNode;
  counter?: string;
  sourceIconColor?: boolean;
  disabled?: boolean;
  enablePropagation?: boolean;
  submit?: boolean;
  className?: string;
  onClick?: (e: React.MouseEvent<HTMLDivElement>) => void;

  onFocus?(): void;

  onBlur?(): void;

  onMouseOver?(): void;

  onMouseOut?(): void;
}

const IconButton: React.FC<IconButtonProps> = ({
  mode,
  size = 'sm',
  shape = 'square',
  background,
  color,
  border = 'none',
  borderRadius,
  icon,
  additionalIcon,
  tooltip,
  counter,
  sourceIconColor,
  disabled,
  enablePropagation,
  submit,
  className,
  dataTestId,
  onClick,
  onFocus,
  onBlur,
  onMouseOver,
  onMouseOut,
}) => {
  const handleClick = (e: React.MouseEvent<HTMLDivElement>) => {
    e.stopPropagation();
    !disabled && onClick?.(e);
  };

  return (
    <Tooltip
      wrapperClassName={classNames(style.wrapper, style[`size_${size}`], className)}
      message={disabled ? '' : tooltip}
      onClick={handleClick}
    >
      <div
        className={classNames(style.buttonWrapper, style.wrapper, style[`size_${size}`])}
        data-testid={dataTestId}
      >
        {!!disabled && enablePropagation && <div className={style.disabledClickable} />}
        <button
          type={submit ? 'submit' : 'button'}
          className={classNames(
            style.button,
            style[`borderRadius_${borderRadius}`],
            style[shape],
            style[`background_${background}`],
            style[`color_${color}`],
            style[`border_${border}`],
            {
              [style.disabled]: disabled,
            },
          )}
          onFocus={onFocus}
          onBlur={onBlur}
          onMouseOver={onMouseOver}
          onMouseOut={onMouseOut}
          disabled={disabled}
        >
          <span
            className={classNames(style.icon, {
              [style.icon_color]: !sourceIconColor,
            })}
          >
            {icon}
          </span>
          {additionalIcon && (
            <div className={style.additionalIcon}>
              <Icon path={'grey-90'} icon={additionalIcon} />
            </div>
          )}
          {counter && <Counter className={style.counter} value={counter} />}
          {mode === 'dropdown' && (
            <>
              <div className={style.dropdown_block}>
                <Divider type="vertical" height="auto" background={'grey-90'} />
                <Icon icon={<CaretDownIcon />} path="inherit" />
              </div>
            </>
          )}
        </button>
      </div>
    </Tooltip>
  );
};

export default IconButton;
