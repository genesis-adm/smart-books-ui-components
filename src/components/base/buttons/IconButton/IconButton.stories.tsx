import { Meta, Story } from '@storybook/react';
import { IconButton, IconButtonProps } from 'Components/base/buttons/IconButton';
import { ReactComponent as CSVIcon } from 'Svg/16/csv.svg';
import { ReactComponent as DownloadIcon } from 'Svg/16/download.svg';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Buttons/Icon Button',
  component: IconButton,
  argTypes: {
    icon: hideProperty,
    tooltip: hideProperty,
    className: hideProperty,
    onClick: hideProperty,
  },
} as Meta;

const IconButtonInheritsColorTemplate: Story<IconButtonProps> = ({ icon, tooltip, ...args }) => (
  <IconButton icon={<DownloadIcon />} tooltip="Tooltip example" {...args} />
);
const IconButtonOriginalColorTemplate: Story<IconButtonProps> = ({ icon, tooltip, ...args }) => (
  <IconButton icon={<CSVIcon />} tooltip="Tooltip example" {...args} />
);

export const IconButtonInheritsColor = IconButtonInheritsColorTemplate.bind({});
IconButtonInheritsColor.args = {
  size: 'sm',
  shape: 'square',
  background: 'violet-90-violet-100',
  color: 'white-100',
  border: 'none',
  sourceIconColor: false,
  disabled: false,
  submit: false,
};

export const IconButtonOriginalColor = IconButtonOriginalColorTemplate.bind({});
IconButtonOriginalColor.args = {
  size: 'sm',
  shape: 'circle',
  background: 'violet-90-violet-100',
  color: 'white-100',
  border: 'none',
  sourceIconColor: true,
  disabled: false,
  submit: false,
};
