import classNames from 'classnames';
import React from 'react';

import style from './Counter.module.scss';

export interface CounterProps {
  value: string;
  className?: string;
}

const Counter: React.FC<CounterProps> = ({ value, className }) => (
  <span className={classNames(style.counter, className)}>{value}</span>
);

export default Counter;
