import { Meta, Story } from '@storybook/react';
import { Counter as Component, CounterProps as Props } from 'Components/base/Counter';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Counter',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const CounterComponent: Story<Props> = (args) => <Component {...args} />;

export const Counter = CounterComponent.bind({});
Counter.args = {
  value: '+2',
};
