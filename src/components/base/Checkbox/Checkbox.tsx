import { Tooltip } from 'Components/base/Tooltip';
import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React from 'react';

import { TestableProps } from '../../../types/general';
import type { TextColorNewTypes } from '../../types/colorTypes';
import type { FontTypes } from '../../types/fontTypes';
import style from './Checkbox.module.scss';

export interface CheckboxProps extends TestableProps {
  name: string;
  className?: string;
  size?: 'medium' | 'large';
  tick?: 'default' | 'unselect';
  textcolor?: TextColorNewTypes;
  font?: FontTypes;
  checked: boolean;
  noWrap?: boolean;
  option?: boolean;
  disabled?: boolean;
  children?: React.ReactNode;

  tooltipMessage?: string;

  onChange(checked: boolean): void;
}

const Checkbox: React.FC<CheckboxProps> = ({
  name,
  className,
  size = 'medium',
  checked = false,
  tick = 'default',
  textcolor = 'black-100',
  font = 'text-regular',
  noWrap,
  disabled,
  option,
  onChange,
  children,
  tooltipMessage,
  dataTestId,
}) => {
  const handleStopLabelPropagation = (event: React.MouseEvent<HTMLLabelElement>) =>
    event.stopPropagation();

  const handleStopInputPropagation = (event: React.MouseEvent<HTMLInputElement>) =>
    event.stopPropagation();

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (disabled) return;
    onChange?.(event.target.checked);
  };

  return (
    <div
      className={classNames(style.checkbox_wrapper, style[`size_${size}`], className, {
        [style.option]: option,
      })}
      data-testid={dataTestId}
    >
      <Tooltip message={tooltipMessage}>
        <input
          onClick={handleStopInputPropagation}
          onChange={handleChange}
          className={classNames(style.checkbox, style[tick], style[`size_${size}`], {
            [style.disabled]: disabled,
          })}
          type="checkbox"
          checked={checked}
          id={name}
        />
      </Tooltip>
      {children && (
        <label role="presentation" onClick={handleStopLabelPropagation} htmlFor={name}>
          <Text className={style.content} type={font} color={textcolor} noWrap={noWrap}>
            {children}
          </Text>
        </label>
      )}
    </div>
  );
};

export default Checkbox;
