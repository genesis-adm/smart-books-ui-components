import { Meta, Story } from '@storybook/react';
import { Checkbox as Component, CheckboxProps as Props } from 'Components/base/Checkbox';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Checkbox',
  component: Component,
  argTypes: {
    className: hideProperty,
    onChange: hideProperty,
  },
} as Meta;

const CheckboxTemplate: Story<Props> = (args) => (
  <Component {...args}>Label text placed in children</Component>
);

export const Checkbox = CheckboxTemplate.bind({});
Checkbox.args = {
  checked: true,
  disabled: false,
  size: 'medium',
  textcolor: 'black-100',
  tick: 'default',
  name: 'test',
  noWrap: false,
};
