import classNames from 'classnames';
import React from 'react';

import { AlignItems, JustifyContent } from '../../../types/gridTypes';
import style from './FormContainerNew.module.scss';

export interface FormContainerNewProps {
  action?: string;
  justifycontent?: JustifyContent;
  alignitems?: AlignItems;
  gap?: '4' | '8' | '12' | '16' | '20' | '24';
  className?: string;
  onSubmit(): void;
  children?: React.ReactNode;
}

const FormContainerNew: React.FC<FormContainerNewProps> = ({
  action,
  justifycontent,
  alignitems,
  gap,
  className,
  onSubmit,
  children,
}) => (
  <form
    className={classNames(
      style.container,
      {
        [style[`justify-content_${justifycontent}`]]: justifycontent,
        [style[`align-items_${alignitems}`]]: alignitems,
        [style[`gap_${gap}`]]: gap,
      },
      className,
    )}
    action={action}
    autoComplete="off"
    onSubmit={onSubmit}
  >
    {children}
  </form>
);

export default FormContainerNew;
