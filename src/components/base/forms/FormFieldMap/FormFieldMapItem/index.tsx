import { ReactComponent as ArrowLeftIcon } from 'Svg/v2/16/arrow-left.svg';
import React from 'react';

import style from './FormFieldMapItem.module.scss';

interface FormFieldMapItemProps {
  label: React.ReactNode;
  children?: React.ReactNode;
}

const FormFieldMapItem: React.FC<FormFieldMapItemProps> = ({ label, children }) => (
  <div className={style.item}>
    <div className={style.label}>
      <div>{label}</div>
      <span>
        <ArrowLeftIcon />
      </span>
    </div>
    <div>{children}</div>
  </div>
);

export default FormFieldMapItem;
