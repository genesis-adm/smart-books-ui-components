import React from 'react';

import style from './FormFieldMap.module.scss';

interface FormFieldMapProps {
  children?: React.ReactNode;
}

const FormFieldMap: React.FC<FormFieldMapProps> = ({ children }) => (
  <div className={style.form}>{children}</div>
);

export default FormFieldMap;
