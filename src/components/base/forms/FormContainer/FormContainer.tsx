import classNames from 'classnames';
import React from 'react';

import style from './FormContainer.module.scss';

interface FormContainerProps {
  onSubmit(): void;
  action?: string;
  justifycontent?:
    | 'center'
    | 'flex-start'
    | 'flex-end'
    | 'space-around'
    | 'space-between'
    | 'space-evenly';
  alignitems?: 'center' | 'flex-start' | 'flex-end' | 'stretch' | 'baseline';
  children?: React.ReactNode;
}

const FormContainer: React.FC<FormContainerProps> = ({
  action,
  onSubmit,
  children,
  justifycontent = 'flex-start',
  alignitems,
}) => (
  <form
    action={action}
    autoComplete="off"
    onSubmit={onSubmit}
    className={classNames(
      style.form,
      style[`justify-content_${justifycontent}`],
      style[`align-items_${alignitems}`],
    )}
    style={{
      display: 'flex',
      flexDirection: 'inherit',
      flexWrap: 'wrap',
    }}
  >
    {children}
  </form>
);

FormContainer.defaultProps = {
  action: '',
  justifycontent: 'flex-start',
  alignitems: 'stretch',
};

export default FormContainer;
