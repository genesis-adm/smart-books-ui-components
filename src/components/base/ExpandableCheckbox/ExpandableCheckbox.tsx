import { Checkbox } from 'Components/base/Checkbox';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as ChevronDown } from 'Svg/v2/16/chevron-down.svg';
import { ReactComponent as ChevronUp } from 'Svg/v2/16/chevron-up.svg';
import classNames from 'classnames';
import React, { ReactElement, useEffect, useState } from 'react';

import { useToggle } from '../../../hooks/useToggle';
import style from './ExpandableCheckbox.module.scss';
import { ExpandableCheckboxProps } from './ExpandableCheckbox.types';

// Reminder: do not forget to add the following code to root index.ts
// export { ExpandableCheckbox } from './components/base/ExpandableCheckbox';

const ExpandableCheckbox = ({
  id,
  checkboxName,
  onCheckboxChange,
  checked,
  className,
  children,
  collapseBtnTitle,
  showCollapseBtn,
  showBadge,
  checkboxTooltip,
  onToggle,
}: ExpandableCheckboxProps): ReactElement => {
  const { isOpen: isShownCollapsedItems, onToggle: onToggleCollapseBtn } = useToggle();

  const [isBadgeShown, setIsBadgeShown] = useState<boolean>();

  const handleToggle = () => {
    onToggleCollapseBtn();
    onToggle?.();
    setIsBadgeShown(false);
  };

  useEffect(() => {
    setIsBadgeShown(!!showBadge);
  }, [showBadge]);

  return (
    <div className={classNames(style.wrapper, style.badge, className)}>
      {!isShownCollapsedItems && isBadgeShown && <div className={style.badge} />}
      <div className={style.checkboxContainer}>
        <Checkbox
          name={id.toString()}
          checked={checked}
          onChange={onCheckboxChange}
          font={'body-regular-16'}
          textcolor={checked ? 'violet-90' : 'grey-100'}
          noWrap
          tooltipMessage={checkboxTooltip}
        >
          {checkboxName}
        </Checkbox>
        {showCollapseBtn && (
          <div className={style.collapseBtnContainer}>
            <Text color={'grey-90'} type="subtext-regular">
              {collapseBtnTitle}
            </Text>
            <IconButton
              tooltip={isShownCollapsedItems ? 'Hide Group' : 'Show Group'}
              icon={isShownCollapsedItems ? <ChevronUp /> : <ChevronDown />}
              size="24"
              color="grey-100"
              background={'transparent-grey-30'}
              onClick={handleToggle}
            />
          </div>
        )}
      </div>
      {isShownCollapsedItems && children}
    </div>
  );
};

export default ExpandableCheckbox;
