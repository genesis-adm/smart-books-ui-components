import { ReactNode } from 'react';

export interface ExpandableCheckboxProps {
  id: string | number;
  checkboxName: string | ReactNode;
  checked: boolean;
  onCheckboxChange(): void;

  collapseBtnTitle?: string;
  showCollapseBtn?: boolean;
  children?: ReactNode;

  onToggle?(): void;

  checkboxTooltip?: string;

  showBadge?: boolean;

  className?: string;
}
