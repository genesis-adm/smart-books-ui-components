import { Meta, Story } from '@storybook/react';
import {
  ExpandableCheckbox as Component,
  ExpandableCheckboxProps as Props,
} from 'Components/base/ExpandableCheckbox';
import {
  ExpandableCheckboxGroup,
  ExpandableCheckboxItem,
} from 'Components/base/ExpandableCheckboxItem';
import { Container } from 'Components/base/grid/Container';
import { businessUnits } from 'Pages/Reports/constants';
import { useExpandableCheckboxData } from 'Pages/Reports/modals/combinateReport/hooks/useExpandableCheckboxData';
import { ReactComponent as DocumentIcon } from 'Svg/v2/16/document.svg';
import { ReactComponent as SmartphoneIcon } from 'Svg/v2/16/smartphone.svg';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/base/Expandable Checkbox/ExpandableCheckbox',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const ExpandableCheckboxComponent: Story<Props> = () => {
  const { handleSelect } = useExpandableCheckboxData({ data: businessUnits });

  return (
    <Container gap="16" flexdirection="column">
      {businessUnits.map((unit) => (
        <Component
          key={unit.id}
          id={unit.id}
          checkboxName={unit.name}
          checked={!!unit.checked}
          onCheckboxChange={() => {
            handleSelect(unit);
          }}
          collapseBtnTitle={`${unit.children?.length} Project`}
          showCollapseBtn={!!unit.children?.length}
        >
          {unit.children &&
            unit.children.map((project, index) => (
              <ExpandableCheckboxGroup
                key={project.id}
                firstChild={index === 0}
                groupName="Projects"
                groupIcon={<SmartphoneIcon />}
              >
                <ExpandableCheckboxItem
                  key={project.id}
                  id={project.id}
                  showCollapseBtn={!!project.children?.length}
                  collapseBtnTitle={`${project.children?.length} Subproject`}
                  checkboxName={project.name}
                  checked={!!project.checked}
                  onCheckboxChange={() => {
                    handleSelect(project);
                  }}
                >
                  {project.children &&
                    project.children.map((subproject, index) => (
                      <ExpandableCheckboxGroup
                        key={subproject.id}
                        firstChild={index === 0}
                        groupName="Subproject"
                        groupIcon={<DocumentIcon />}
                      >
                        <ExpandableCheckboxItem
                          key={subproject.id}
                          id={subproject.id}
                          checkboxName={subproject.name}
                          checked={!!subproject.checked}
                          showCollapseBtn={!!subproject.children?.length}
                          onCheckboxChange={() => {
                            handleSelect(subproject);
                          }}
                        />
                      </ExpandableCheckboxGroup>
                    ))}
                </ExpandableCheckboxItem>
              </ExpandableCheckboxGroup>
            ))}
        </Component>
      ))}
    </Container>
  );
};

export const ExpandableCheckbox = ExpandableCheckboxComponent.bind({});
ExpandableCheckbox.args = {};
