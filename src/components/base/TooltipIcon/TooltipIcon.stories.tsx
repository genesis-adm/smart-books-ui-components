import { TooltipIcon as Component } from 'Components/base/TooltipIcon';
import { ReactComponent as QuestionIcon } from 'Svg/v2/16/question.svg';
import React from 'react';

export default {
  title: 'Components/Base/Tooltip Icon',
};

export const TooltipIcon: React.FC = () => (
  <Component icon={<QuestionIcon />} message="Some text" />
);
