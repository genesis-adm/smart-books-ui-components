import { Tooltip } from 'Components/base/Tooltip';
import classNames from 'classnames';
import React from 'react';

import type {
  BackgroundColorNewTypes,
  BorderColorNewTypes,
  TextColorNewTypes,
} from '../../types/colorTypes';
import style from './TooltipIcon.module.scss';

export interface TooltipIconProps {
  size?: '12' | '16' | '24';
  shape?: 'square' | 'circle';
  background?: BackgroundColorNewTypes;
  color?: TextColorNewTypes;
  border?: BorderColorNewTypes;
  icon: React.ReactNode;
  sourceIconColor?: boolean;
  message: string | React.ReactNode;
  className?: string;
}

const TooltipIcon: React.FC<TooltipIconProps> = ({
  size = '24',
  shape = 'square',
  background = 'grey-10-black-100',
  color = 'grey-100-white-100',
  border = 'none',
  icon,
  sourceIconColor,
  message,
  className,
}) => (
  <Tooltip
    message={message}
    wrapperClassName={classNames(
      style.container,
      style[`size_${size}`],
      style[shape],
      style[`background_${background}`],
      style[`color_${color}`],
      style[`border_${border}`],
      className,
    )}
  >
    <span
      className={classNames(style.icon, {
        [style.icon_color]: !sourceIconColor,
      })}
    >
      {icon}
    </span>
  </Tooltip>
);

export default TooltipIcon;
