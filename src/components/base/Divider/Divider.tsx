import classNames from 'classnames';
import React from 'react';

import type { BackgroundColorStaticNewTypes } from '../../types/colorTypes';
import style from './Divider.module.scss';

interface DividerProps {
  type?: 'horizontal' | 'vertical';
  height?: '12' | '16' | '20' | '26' | '38' | 'full' | 'auto';
  position?: 'left' | 'right' | 'both';
  background?: BackgroundColorStaticNewTypes;
  fullHorizontalWidth?: boolean;
  className?: string;
}

const Divider: React.FC<DividerProps> = ({
  type = 'horizontal',
  height = '12',
  position,
  background = 'grey-20',
  fullHorizontalWidth,
  className,
}) => (
  <div
    className={classNames(
      style.divider,
      style[type],
      style[`height_${height}`],
      style[`background_${background}`],
      className,
      {
        [style[`position_${position}`]]: position,
        [style.fullHorizontalWidth]: type === 'horizontal' && fullHorizontalWidth,
      },
    )}
    data-vertical={type === 'vertical'}
  />
);

export default Divider;
