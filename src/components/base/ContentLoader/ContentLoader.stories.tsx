import { Meta, Story } from '@storybook/react';
import { ContentLoader as Component } from 'Components/base/ContentLoader';
import { ContentLoaderProps } from 'Components/base/ContentLoader/ContentLoader';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Content Loader',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const ContentLoaderComponent: Story<ContentLoaderProps> = (args) => {
  return <Component {...args} />;
};

export const ContentLoader = ContentLoaderComponent.bind({});
ContentLoader.args = {
  width: '200px',
  height: '40px',
  isLoading: true,
  type: 'square',
};
