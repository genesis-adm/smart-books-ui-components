import classNames from 'classnames';
import React, { CSSProperties, ReactElement, ReactNode } from 'react';

import { TestableProps } from '../../../types/general';
import styles from './ContentLoader.module.scss';

export interface ContentLoaderProps extends TestableProps {
  type?: 'square' | 'circle';
  isTableBody?: boolean;
  width?: string;
  height?: string;
  className?: string;
  isLoading: boolean;
  children?: ReactNode;
  isInsideComponent?: boolean;
  style?: CSSProperties;
}

const ContentLoader = ({
  type = 'square',
  isTableBody,
  width = '100%',
  height = '40px',
  isLoading,
  children,
  isInsideComponent,
  className,
  style,
  dataTestId,
}: ContentLoaderProps): ReactElement => {
  if (isLoading) {
    if (isTableBody) {
      return (
        <>
          {Array.from(Array(3)).map((_, index) => (
            <div
              key={index}
              className={classNames(styles.loaderWrapper, className, styles[`type_${type}`])}
              style={{
                width,
                height,
                marginBottom: '4px',
                ...style,
              }}
            >
              <div className={styles.loaderBody} />
            </div>
          ))}
        </>
      );
    }

    return (
      <div
        data-testid={dataTestId}
        className={classNames(styles.loaderWrapper, className, styles[`type_${type}`], {
          [styles.isInsideComponent]: isInsideComponent,
        })}
        style={{
          width,
          height,
          ...style,
        }}
      >
        <div className={styles.loaderBody} />
      </div>
    );
  }

  return <>{children}</>;
};

export default ContentLoader;
