import { Divider } from 'Components/base/Divider';
import { Icon } from 'Components/base/Icon';
import { Tooltip } from 'Components/base/Tooltip';
import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React, { ChangeEvent, FocusEvent, ReactElement, useState } from 'react';
import {
  NumberFormatValues,
  NumericFormat,
  NumericFormatProps,
  SourceInfo,
} from 'react-number-format';

import style from './InputCurrency.module.scss';

export interface InputCurrencyProps extends NumericFormatProps {
  currencyCode: string;
  currencyIcon?: JSX.Element;
  error?: boolean;
  className?: string;
}

const InputCurrency = ({
  currencyCode,
  currencyIcon,
  decimalScale = 6,
  thousandSeparator = true,
  placeholder = '0.000000',
  maxLength = 16,
  error,
  disabled,
  value,
  onChange,
  onValueChange,
  onFocus,
  onBlur,
  className,
  ...props
}: InputCurrencyProps): ReactElement => {
  const [focused, setFocused] = useState<boolean>(false);
  const [hovered, setHovered] = useState<boolean>(false);

  const handleChange = (e: ChangeEvent<HTMLInputElement>): void => {
    onChange?.(e);
  };
  const handleValueChange = (values: NumberFormatValues, sourceInfo: SourceInfo): void => {
    onValueChange?.(values, sourceInfo);
  };

  const handleFocus = (e: FocusEvent<HTMLInputElement>): void => {
    onFocus?.(e);
    setFocused(true);
  };
  const handleBlur = (e: FocusEvent<HTMLInputElement>): void => {
    onBlur?.(e);
    setFocused(false);
  };

  return (
    <Tooltip
      message=""
      wrapperClassName={classNames(
        style.wrapper,
        {
          [style.hovered]: hovered,
          [style.focused]: focused,
          [style.error]: error,
          [style.disabled]: disabled,
        },
        className,
      )}
      onMouseEnter={() => setHovered(true)}
      onMouseLeave={() => setHovered(false)}
    >
      <NumericFormat
        {...props}
        className={classNames(style.input)}
        thousandSeparator={thousandSeparator}
        decimalScale={decimalScale}
        placeholder={placeholder}
        maxLength={maxLength}
        disabled={disabled}
        value={value}
        onChange={handleChange}
        onValueChange={handleValueChange}
        onFocus={handleFocus}
        onBlur={handleBlur}
      />
      <Divider type="vertical" height="16" background="grey-90" />
      <Text>{currencyCode.toUpperCase()}</Text>
      {currencyIcon && <Icon icon={currencyIcon} />}
    </Tooltip>
  );
};

export default InputCurrency;
