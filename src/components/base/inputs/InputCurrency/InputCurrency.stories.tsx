import { Meta, Story } from '@storybook/react';
import {
  InputCurrency as Component,
  InputCurrencyProps as Props,
} from 'Components/base/inputs/InputCurrency';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Inputs/InputCurrency',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const InputCurrencyComponent: Story<Props> = (args) => <Component {...args} />;

export const InputCurrency = InputCurrencyComponent.bind({});
InputCurrency.args = {
  currencyCode: 'usd',
  thousandSeparator: true,
};
