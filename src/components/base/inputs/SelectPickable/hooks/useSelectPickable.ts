import {
  Option,
  SelectPickableHookProps,
} from 'Components/base/inputs/SelectPickable/SelectPickable.types';
import { useCallback, useEffect, useMemo, useState } from 'react';

export default function useSelectPickable({ defaultValue, value }: SelectPickableHookProps) {
  const [state, setState] = useState<Option>(defaultValue);
  const [isOpen, setOpen] = useState(false);
  const [focus, setFocus] = useState(false);

  const setSelectActive = useCallback((arg: boolean) => {
    setOpen(arg);
    setFocus(arg);
  }, []);

  const handleClick = useCallback(
    (data: Option) => {
      setState(data);
      setSelectActive(false);
    },
    [setSelectActive],
  );

  useEffect(() => {
    if (value) {
      setState(value);
    }
  }, [value]);

  return useMemo(
    () => ({
      isOpen,
      focus,
      setSelectActive,
      state,
      setState,
      onClick: handleClick,
    }),
    [isOpen, focus, setSelectActive, state, setState, handleClick],
  );
}
