import React from 'react';

export type SelectWidthType =
  | '66'
  | '84'
  | '112'
  | '138'
  | '140'
  | '150'
  | '180'
  | '190'
  | '192'
  | '200'
  | '215'
  | '220'
  | '230'
  | '248'
  | '260'
  | '280'
  | '300'
  | '320'
  | '408'
  | '552'
  | '672'
  | 'full';

export type SelectHeightType = '28' | '36' | '48';
export type SelectBackgroundType = 'grey-10' | 'white-100' | 'violet-90' | 'transparent';
export type SelectColorType = 'default' | 'light';

export interface SelectPickableContextProps {
  isOpen: boolean;
  state: Option | null;

  onClick(arg: Option): void;

  setSelectActive(arg: boolean): void;

  setState(arg: Option): void;
}

export interface SelectPickableHookProps {
  defaultValue: Option;
  value?: Option;
}

export interface Option {
  value: string;
  label: string;
}
export interface SelectPickableProps {
  className?: string;
  width?: SelectWidthType;
  height?: SelectHeightType;
  background?: SelectBackgroundType;
  color?: SelectColorType;
  defaultValue: Option;
  value?: Option;
  disabled?: boolean;
  onChange?(option: Option): void;

  children(args: {
    onClick(option: Option): void;
    state: Option | null;
    selectElement: Element;
  }): React.ReactElement;
}
