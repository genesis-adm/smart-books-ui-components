export interface OptionsPickableProps {
  className?: string;
  label: string;
  height?: '32' | '40';
  selected: boolean;
  onClick(): void;
}
