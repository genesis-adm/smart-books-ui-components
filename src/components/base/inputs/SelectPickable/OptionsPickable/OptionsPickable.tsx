import classNames from 'classnames';
import React, { ReactElement } from 'react';

import style from './OptionsPickable.module.scss';
import { OptionsPickableProps } from './OptionsPickable.types';

// Reminder: do not forget to add the following code to root index.ts
// export { OptionsPickable } from './components/OptionsPickable';

const OptionsPickable = ({
  className,
  label,
  height,
  selected,
  onClick,
}: OptionsPickableProps): ReactElement => (
  <li
    className={classNames(
      style.item,
      style[`height_${height}`],
      {
        [style.selected]: selected,
      },
      className,
    )}
    role="presentation"
    onClick={() => onClick()}
  >
    {label}
  </li>
);

export default OptionsPickable;
