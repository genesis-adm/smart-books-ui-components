import { Meta, Story } from '@storybook/react';
import { SelectPickable } from 'Components/base/inputs/SelectPickable';
import {
  OptionsPickable as Component,
  OptionsPickableProps as Props,
} from 'Components/base/inputs/SelectPickable/OptionsPickable/index';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Inputs/SelectPickable/OptionsPickable',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

type OptionsType = {
  value: string;
  label: string;
};
const OptionsPickableComponent: Story<Props> = (args) => {
  const options: OptionsType[] = [
    { value: 'tax inclusive', label: 'Tax inclusive' },
    { value: 'tax exclusive', label: 'Tax exclusive' },
  ];
  return (
    <>
      <Component {...args} />
      <SelectPickable height="36" width="150" background="grey-10" defaultValue={options[0]}>
        {({ onClick, state }) => (
          <>
            {options.map(({ value, label }) => {
              console.log('name', name);
              return (
                <Component
                  label={label}
                  selected={state?.value === value}
                  onClick={() => {
                    onClick({
                      label,
                      value,
                    });
                  }}
                />
              );
            })}
          </>
        )}
      </SelectPickable>
    </>
  );
};

export const OptionsPickable = OptionsPickableComponent.bind({});
OptionsPickable.args = {};
