import { Icon } from 'Components/base/Icon';
import useSelectPickable from 'Components/base/inputs/SelectPickable/hooks/useSelectPickable';
import { ReactComponent as CaretDownIcon } from 'Svg/v2/16/caret-down.svg';
import classNames from 'classnames';
import React, { ReactElement, createContext, useCallback, useMemo, useRef } from 'react';

import useClickOutside from '../../../../hooks/useClickOutside';
import useScrollOutside from '../../../../hooks/useScrollOutside';
import style from './SelectPickable.module.scss';
import { Option, SelectPickableContextProps, SelectPickableProps } from './SelectPickable.types';

// Reminder: do not forget to add the following code to root index.ts
// export { SelectPickable } from './components/SelectPickable';

export const SelectPickableContext = createContext({} as SelectPickableContextProps);

const SelectPickable = ({
  className,
  children,
  width,
  height,
  background = 'grey-10',
  color = 'default',
  defaultValue = {
    value: '',
    label: '',
  },
  value,
  disabled,
  onChange,
}: SelectPickableProps): ReactElement => {
  const wrapperRef = useRef<HTMLDivElement>(null);
  const selectElement = wrapperRef?.current;

  const { isOpen, focus, setSelectActive, onClick, state, setState } = useSelectPickable({
    defaultValue,
    value,
  });

  useClickOutside({
    ref: wrapperRef.current,
    cb: setSelectActive,
  });
  useScrollOutside({
    isListOpen: isOpen,
    cb: setSelectActive,
  });

  const handleToggle = () => {
    !disabled && setSelectActive(!isOpen);
  };
  const handleClick = useCallback(
    (value: Option) => {
      if (onChange) onChange(value);
      onClick(value);
    },
    [onChange, onClick],
  );

  const context = useMemo(
    () => ({
      isOpen,
      focus,
      setSelectActive,
      setState,
      onClick: handleClick,
      state,
    }),
    [state, setState, isOpen, focus, setSelectActive, handleClick],
  );

  const controls = useMemo(() => {
    if (!selectElement) return;
    return {
      onClick: handleClick,
      state,
      selectElement: selectElement,
    };
  }, [handleClick, state, selectElement]);

  return (
    <SelectPickableContext.Provider value={context}>
      <div
        ref={wrapperRef}
        className={classNames(style.wrapper, className, style[`width_${width}`])}
      >
        <div
          className={classNames(
            style.select,
            style[`height_${height}`],
            style[`background_${background}`],
            {
              [style.active]: isOpen,
              [style.disabled]: disabled,
            },
          )}
          onClick={handleToggle}
        >
          <span className={style.text}>{state.label}</span>
          <Icon
            icon={<CaretDownIcon />}
            path="inherit"
            className={classNames(style.arrow, style[`color_${color}`], {
              [style.active]: isOpen,
            })}
            clickThrough
          />
        </div>

        {isOpen && (
          <div className={classNames(style.dropdown, className)}>
            <ul className={style.list}>{controls && children({ ...controls })}</ul>
          </div>
        )}
      </div>
    </SelectPickableContext.Provider>
  );
};

export default SelectPickable;
