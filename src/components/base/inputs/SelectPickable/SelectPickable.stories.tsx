import { Meta, Story } from '@storybook/react';
import { OptionsPickable } from 'Components/base/inputs/SelectPickable/OptionsPickable';
import {
  SelectPickable as Component,
  SelectPickableProps as Props,
} from 'Components/base/inputs/SelectPickable/index';
import React from 'react';

import { Option } from './SelectPickable.types';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Inputs/SelectPickable',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const SelectPickableComponent: Story<Props> = (args) => {
  const options: Option[] = [
    { value: 'tax inclusive', label: 'Tax inclusive' },
    { value: 'tax exclusive', label: 'Tax exclusive' },
  ];
  return (
    <>
      <Component
        height="36"
        width="150"
        background="grey-10"
        defaultValue={options[0]}
        value={options[1]}
      >
        {({ onClick, state }) => (
          <>
            {options.map(({ value, label }) => {
              return (
                <OptionsPickable
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() => {
                    onClick({
                      label,
                      value,
                    });
                  }}
                />
              );
            })}
          </>
        )}
      </Component>
    </>
  );
};

export const SelectPickable = SelectPickableComponent.bind({});
SelectPickable.args = {};
