import { TooltipIcon } from 'Components/base/TooltipIcon';
import { ReactComponent as QuestionIcon } from 'Svg/16/question.svg';
import classNames from 'classnames';
import React from 'react';

import style from './InputLabel.module.scss';

export interface InputLabelProps {
  name: string;
  label?: string;
  secondaryLabel?: string;
  tooltip?: React.ReactNode;
  required?: boolean;
  disabled?: boolean;
  readOnly?: boolean;
  className?: string;
}

const InputLabel: React.FC<InputLabelProps> = ({
  name,
  label,
  secondaryLabel,
  tooltip,
  required,
  disabled,
  readOnly,
  className,
}) => (
  <div
    className={classNames(
      style.wrapper,
      {
        [style.mainShown]: label && !secondaryLabel,
        [style.secondaryShown]: !label && secondaryLabel,
        [style.bothShown]: label && secondaryLabel,
      },
      className,
    )}
  >
    {label && (
      <label
        className={classNames(style.label, {
          [style.required]: required,
          [style.disabled]: disabled || readOnly,
        })}
        htmlFor={name}
      >
        {label}
      </label>
    )}
    {(secondaryLabel || tooltip) && (
      <div className={style.secondary}>
        {secondaryLabel && <span className={style.secondaryLabel}>{secondaryLabel}</span>}
        {tooltip && (
          <TooltipIcon className={style.icon} size="16" icon={<QuestionIcon />} message={tooltip} />
        )}
      </div>
    )}
  </div>
);

export default InputLabel;
