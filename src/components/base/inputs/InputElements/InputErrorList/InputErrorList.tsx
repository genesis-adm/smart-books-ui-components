import { InputErrorItem } from 'Components/base/inputs/InputElements/InputErrorItem';
import React from 'react';

import { InputWidthType } from '../../../../../types/inputs';

export interface InputErrorListProps {
  width: InputWidthType;
  errorsList: string[];
  className?: string;
}

const InputErrorList: React.FC<InputErrorListProps> = ({ errorsList, width }) => {
  if (!errorsList) return null;
  return (
    <>
      {errorsList.map((error) => (
        <InputErrorItem key={error} message={error} width={width} />
      ))}
    </>
  );
};

export default InputErrorList;
