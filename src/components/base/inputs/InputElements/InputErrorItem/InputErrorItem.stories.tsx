import { Meta, Story } from '@storybook/react';
import {
  InputErrorItem as Component,
  InputErrorItemProps as Props,
} from 'Components/base/inputs/InputElements/InputErrorItem';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Inputs/Input Elements/InputErrorItem',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const InputErrorItemComponent: Story<Props> = (args) => (
  <>
    {Array.from(Array(7)).map((_, index) => (
      <Component key={index} {...args} />
    ))}
  </>
);

export const InputErrorItem = InputErrorItemComponent.bind({});
InputErrorItem.args = {
  width: '320',
  message: 'Error text is displyed here',
};
