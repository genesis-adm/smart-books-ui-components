import { Icon } from 'Components/base/Icon';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as AlertIcon } from 'Svg/v2/16/alert.svg';
import classNames from 'classnames';
import React from 'react';

import { InputWidthType } from '../../../../../types/inputs';
import style from './InputErrorItem.module.scss';

export interface InputErrorItemProps {
  message: string;
  width: InputWidthType;
  className?: string;
}

const InputErrorItem: React.FC<InputErrorItemProps> = ({ width, message, className }) => (
  <div className={classNames(style.component, style[`width_${width}`], className)}>
    <Icon icon={<AlertIcon />} path="red-90" />
    <Text className={style.text} color="red-90" noWrap>
      {message}
    </Text>
  </div>
);

export default InputErrorItem;
