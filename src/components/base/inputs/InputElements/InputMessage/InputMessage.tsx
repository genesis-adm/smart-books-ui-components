import classNames from 'classnames';
import React from 'react';

import style from './InputMessage.module.scss';

export interface InputMessageProps {
  type: 'normal' | 'success' | 'warning' | 'error';
  text: string;
  className?: string;
}

const InputMessage: React.FC<InputMessageProps> = ({ type = 'normal', text, className }) => (
  <div className={classNames(style.message, style[type], className)}>
    <span className={classNames(style.icon, style[type])}>!</span>
    <span>{text}</span>
  </div>
);

InputMessage.displayName = 'InputMessage';

export default InputMessage;
