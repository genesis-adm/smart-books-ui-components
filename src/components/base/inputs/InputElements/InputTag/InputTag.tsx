import { Tooltip } from 'Components/base/Tooltip';
import { IconButton } from 'Components/base/buttons/IconButton';
import InputErrorPopup from 'Components/base/inputs/InputErrorPopup/InputErrorPopup';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as ClearIcon } from 'Svg/v2/16/cross.svg';
import classNames from 'classnames';
import React, { CSSProperties, ReactNode, useEffect, useState } from 'react';

import { useBounding } from '../../../../../hooks/useBounding';
import styles from './InputTag.module.scss';

export interface TagType {
  id: string;
  label: string;

  onDelete(id: string): void;

  onClick?(): void;
}

export type TagBackground = 'violet' | 'grey' | 'grey_30' | 'blue';
export type TagWidth = 'auto' | '100' | '200';

export interface InputTagProps extends Omit<TagType, 'label'> {
  label: ReactNode;
  background?: TagBackground;
  width?: TagWidth;
  className?: string;
  style?: CSSProperties;
  tooltipMessage?: string;
  errorMessage?: string;
  isError?: boolean;
  onChangeBounding?: (domRect: DOMRect, id: string) => void;

  disabled?: boolean;
  readOnly?: boolean;
}

const InputTag: React.FC<InputTagProps> = ({
  id,
  label,
  className,
  style,
  onDelete,
  tooltipMessage,
  errorMessage,
  background = 'violet',
  width = '100',
  onClick,
  isError,
  onChangeBounding,
  disabled = false,
  readOnly = false,
}) => {
  const { ref, domRect } = useBounding<HTMLDivElement>();
  const [deleteHover, setDeleteHover] = useState(false);
  const [errorHover, setErrorHover] = useState(false);

  const deleteAddHandler = () => setDeleteHover(true);
  const deleteRemoveHandler = () => setDeleteHover(false);
  const errorAddHandler = () => setErrorHover(true);
  const errorRemoveHandler = () => setErrorHover(false);

  const handleDelete = (e: React.MouseEvent<HTMLDivElement>) => {
    e.stopPropagation();
    deleteRemoveHandler();
    onDelete(id);
  };

  useEffect(() => {
    if (!domRect) return;

    onChangeBounding?.(domRect, id);
  }, [onChangeBounding, domRect, id]);

  return (
    <div
      ref={ref}
      id={id}
      className={classNames(
        styles.tag,
        styles[`background_${background}`],

        { [styles.delete]: deleteHover || isError },
        className,
      )}
      style={style}
      onClick={onClick}
      onMouseOver={errorAddHandler}
      onMouseOut={errorRemoveHandler}
    >
      <Tooltip message={tooltipMessage}>
        <Text
          className={classNames(styles.text, styles[`width_${width}`])}
          type="body-regular-14"
          color="inherit"
          noWrap
        >
          {label}
        </Text>
        {!!(errorMessage && ref.current && errorHover) && (
          <InputErrorPopup inputWrapperElement={ref.current} errorMessage={errorMessage} />
        )}
      </Tooltip>

      {!disabled && !readOnly && (
        <IconButton
          size="16"
          icon={<ClearIcon />}
          onClick={handleDelete}
          onMouseOver={deleteAddHandler}
          onMouseOut={deleteRemoveHandler}
          background="inherit"
          color="inherit"
        />
      )}
    </div>
  );
};

export interface InputTagWrapperProps {
  tags: TagType[];
  tagColor?: TagBackground;
  tagWidth?: TagWidth;
  paddingBottom?: '0' | '4';
  className?: string;
  wrap?: boolean;
}

const InputTagWrapper: React.FC<InputTagWrapperProps> = ({
  tags,
  tagColor,
  tagWidth,
  paddingBottom = '4',
  className,
  wrap,
}) => (
  <div
    className={classNames(
      styles.wrapper,
      { [styles.empty]: tags.length === 0 },
      { [styles.wrap]: wrap },
      styles[`padding-bottom_${paddingBottom}`],
      className,
    )}
  >
    {tags.map(({ id, label, onDelete }) => (
      <InputTag
        key={id}
        id={id}
        label={label}
        onDelete={onDelete}
        background={tagColor}
        width={tagWidth}
      />
    ))}
  </div>
);

export { InputTag, InputTagWrapper };
