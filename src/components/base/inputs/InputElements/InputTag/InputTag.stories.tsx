import { Meta, Story } from '@storybook/react';
import {
  InputTag as Component,
  InputTagProps as Props,
} from 'Components/base/inputs/InputElements/InputTag';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Inputs/Input Elements/Input Tag',
  component: Component,
  argTypes: {
    className: hideProperty,
    onDelete: hideProperty,
  },
} as Meta;

const InputTagComponent: Story<Props> = (args) => <Component {...args} />;

export const InputTag = InputTagComponent.bind({});
InputTag.args = {
  label: 'Input tag label',
  id: 'tag',
};
