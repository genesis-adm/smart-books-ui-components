export { InputTag, InputTagWrapper } from './InputTag';
export type { InputTagProps, InputTagWrapperProps, TagType } from './InputTag';
