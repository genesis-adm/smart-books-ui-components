import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React from 'react';

import { InputWidthType } from '../../../../../types/inputs';
import type { TextColorStaticNewTypes } from '../../../../types/colorTypes';
import style from './InputDescription.module.scss';

export interface InputDescriptionProps {
  width?: InputWidthType;
  description: string | React.ReactElement;
  color?: TextColorStaticNewTypes;
  className?: string;
}

const InputDescription: React.FC<InputDescriptionProps> = ({
  width,
  description,
  color = 'grey-100',
  className,
}) => (
  <Text
    className={classNames(style.description, style[`width_${width}`], className)}
    color={color}
    display="block"
    noWrap
  >
    {description}
  </Text>
);

export default InputDescription;
