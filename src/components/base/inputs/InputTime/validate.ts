export const isValid = (val: string) => {
  const regexp = /^\d{0,2}?:?\d{0,2}:?\d{0,2}$/;

  const [hoursStr, minutesStr, secondsStr] = val.split(':');

  if (!regexp.test(val)) {
    return false;
  }

  const hours = Number(hoursStr);
  const minutes = Number(minutesStr);
  const seconds = Number(secondsStr);

  const isValidHour = (hour: number) => Number.isInteger(hour) && hour >= 0 && hour < 24;
  const isValidMinutes = (min: number) =>
    (Number.isInteger(min) && min >= 0 && min <= 59) || Number.isNaN(min);
  const isValidSeconds = (sec: number) =>
    (Number.isInteger(sec) && sec >= 0 && sec <= 59) || Number.isNaN(sec);

  if (!isValidHour(hours) || !isValidMinutes(minutes) || !isValidSeconds(seconds)) {
    return false;
  }

  // if (minutes < 10 && Number(minutesStr[0]) > 5) {
  //   return false;
  // }

  const valArr = val.indexOf(':') !== -1 ? val.split(':') : [val];
  const [hoursValue, minsValue, secondsValue] = valArr;

  // check mm, HH and ss
  if (hoursValue?.length && (parseInt(hoursValue, 10) < 0 || parseInt(hoursValue, 10) > 23)) {
    return false;
  }

  if (minsValue?.length && (parseInt(minsValue, 10) < 0 || parseInt(minsValue, 10) > 59)) {
    return false;
  }

  if (secondsValue?.length && (parseInt(secondsValue, 10) < 0 || parseInt(secondsValue, 10) > 59)) {
    return false;
  }

  return true;
};
