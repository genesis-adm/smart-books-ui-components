import { Meta, Story } from '@storybook/react';
import { InputTime as Component, InputTimeProps as Props } from 'Components/base/inputs/InputTime';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Inputs/Input Time',
  component: Component,
  argTypes: {
    name: hideProperty,
    timeValue: hideProperty,
    className: hideProperty,
    onChange: hideProperty,
    onBlur: hideProperty,
  },
} as Meta;

const InputTimeTemplate: Story<Props> = (args) => <Component {...args} />;

export const InputTime = InputTimeTemplate.bind({});
InputTime.args = {
  width: '300',
  flexgrow: '1',
  name: 'inputNameTest',
  label: 'Time input label',
  placeholder: '**:**:**',
  required: true,
  error: false,
  disabled: false,
  readOnly: false,
  description: 'Time input description',
  tooltip: 'Time input tooltip',
};
