import { Icon } from 'Components/base/Icon';
import { Tooltip } from 'Components/base/Tooltip';
import { InputDescription } from 'Components/base/inputs/InputElements/InputDescription';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as TimeIcon } from 'Svg/v2/16/clock.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import { InputWidthType } from '../../../../types/inputs';
import style from './InputTime.module.scss';
import { isValid } from './validate';

export interface InputTimeProps {
  width?: InputWidthType;
  flexgrow?: '0' | '1';
  // timeFormat: '12' | '24';
  error?: boolean;
  timeValue?: string;
  name: string;
  label?: string;
  tooltip?: React.ReactNode;
  description?: string;
  placeholder?: string;
  required?: boolean;
  disabled?: boolean;
  readOnly?: boolean;
  className?: string;

  onChange(arg: string): void;

  onBlur(arg: string): void;
}

const InputTime = React.forwardRef<HTMLInputElement, InputTimeProps>(
  (
    {
      width = '260',
      flexgrow,
      error,
      timeValue,
      name,
      label,
      tooltip,
      description,
      placeholder = 'HH:MM:SS',
      required,
      disabled,
      readOnly,
      onChange,
      onBlur,
      className,
    },
    ref,
  ) => {
    const [time, setTime] = useState<string>(timeValue || '');
    const [focused, setFocused] = useState<boolean>(false);
    const [hovered, setHovered] = useState<boolean>(false);
    const inputActive = focused || time;
    const [invalid, setInvalid] = useState<boolean>(false);
    const handleChange = ({ target: { value } }: React.ChangeEvent<HTMLInputElement>) => {
      if (isValid(value)) {
        setInvalid(false);
        setTime(value);
        onChange(value);
      } else {
        if (value.length > 8) {
          return;
        }
        setInvalid(true);
      }
    };
    const handleBlur = ({ target: { value } }: React.ChangeEvent<HTMLInputElement>) => {
      if (isValid(value) && value.length === 8) {
        onBlur(value);
        setFocused(false);
      } else {
        setInvalid(true);
        setFocused(false);
      }
    };
    const handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
      const { value } = event.target as HTMLInputElement;
      if (value.length === 1 && event.key === ':') {
        setTime(`0${value}`);
      }
      if (value.length === 4 && event.key === ':') {
        const [hours, minutes] = value.split(':');
        setTime(`${hours}:0${minutes}`);
      }
      if (
        (value.length === 2 || value.length === 5) &&
        value.charAt(value.length - 1) !== ':' &&
        event.key !== 'Backspace'
      ) {
        setTime((prevTime) => `${prevTime}:`);
      }
      if ((value.length === 4 || value.length === 7) && event.key === 'Backspace') {
        setTime((prevTime) => prevTime.slice(0, -1));
      }
      if (value.charAt(value.length - 1) === ':' && event.key === ':') {
        event.preventDefault();
      }
    };
    const tooltipMessage = disabled ? 'Disabled' : focused ? '' : tooltip;
    return (
      <div
        className={classNames(
          style.component,
          style[`width_${width}`],
          {
            [style[`flexgrow_${flexgrow}`]]: flexgrow,
          },
          className,
        )}
      >
        <Tooltip message={tooltipMessage} wrapperClassName={style.wrapper}>
          <input
            className={classNames(style.input, {
              [style.hovered]: hovered,
              [style.focused]: focused,
              [style.inputActive]: inputActive,
              [style.error]: error || invalid,
              [style.disabled]: disabled,
              [style.readonly]: readOnly,
            })}
            type="text"
            id={name}
            value={time}
            autoComplete="off"
            required={required}
            ref={ref}
            disabled={disabled}
            readOnly={readOnly}
            onChange={handleChange}
            onBlur={handleBlur}
            onKeyDown={handleKeyDown}
            onMouseEnter={() => setHovered(true)}
            onMouseLeave={() => setHovered(false)}
            onFocus={() => setFocused(true)}
          />
          <Text className={style.placeholder} type="body-regular-14" color="grey-90">
            <Text type="inherit" color="transparent">
              {time}
            </Text>
            {placeholder.substring(time.length)}
          </Text>
          <span className={style.label}>
            {label}
            {required && <span className={style.required}>*</span>}
          </span>
          <Icon className={style.icon} icon={<TimeIcon />} clickThrough path="inherit" />
        </Tooltip>
        {description && <InputDescription description={description} />}
      </div>
    );
  },
);

InputTime.displayName = 'InputTime';

export default InputTime;
