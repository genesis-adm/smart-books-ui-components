import { Meta, Story } from '@storybook/react';
import { InputDate as Component, InputDateProps as Props } from 'Components/base/inputs/InputDate';
import React, { useState } from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Inputs/InputDate',
  component: Component,
  argTypes: {
    name: hideProperty,
    selected: hideProperty,
    className: hideProperty,
    onChange: hideProperty,
    onFocus: hideProperty,
    onBlur: hideProperty,
    calendarStartDay: hideProperty,
    error: hideProperty,
    id: hideProperty,
    background: hideProperty,
    tooltipWidth: hideProperty,
    minDate: hideProperty,
    maxDate: hideProperty,
    tooltip: hideProperty,
  },
} as Meta;

const InputDateTemplate: Story<Props & { isError: boolean }> = ({ isError, ...args }) => {
  const [startDate, setStartDate] = useState<Date | null>(null);

  const error = isError ? { messages: ['Test error!!!'] } : undefined;

  return (
    <Component
      {...args}
      selected={startDate}
      onChange={(date: Date) => setStartDate(date)}
      error={error}
    />
  );
};

export const InputDate = InputDateTemplate.bind({});
InputDate.args = {
  disabled: false,
  readOnly: false,
  isError: false,
  isLoading: false,
  height: '48',
  width: '260',
  required: true,
  name: 'datepicker',
  label: 'Label of a datepicker',
  placeholder: 'Choose date',
  calendarStartDay: 1,
  popperDirection: 'auto',
  tooltip: 'Tooltip of a datepicker',
  selected: null,
};
