import { ContentLoader } from 'Components/base/ContentLoader';
import { Icon } from 'Components/base/Icon';
import { Tooltip } from 'Components/base/Tooltip';
import { ReactComponent as CalendarIcon } from 'Svg/v2/16/calendar.svg';
import classNames from 'classnames';
import { addDays, subDays } from 'date-fns';
import React, { useRef, useState } from 'react';
import DatePicker from 'react-datepicker';

import { InputBaseProps, InputWidthType } from '../../../../types/inputs';
import InputErrorPopup from '../InputErrorPopup/InputErrorPopup';
import style from './InputDate.module.scss';

export interface InputDateProps extends InputBaseProps {
  selected: Date | null;
  calendarStartDay?: 0 | 1 | 2 | 3 | 4 | 5 | 6;
  width: InputWidthType;
  height?: '36' | '48';
  popperDirection?: 'bottom' | 'top' | 'auto';
  minDate?: number;
  maxDate?: number;
  dateFormat?: string;
  placeholder?: string;

  onChange(date: Date): void;

  onFocus?(e: React.FocusEvent<HTMLInputElement>): void;

  onBlur?(e: React.FocusEvent<HTMLInputElement>): void;
}

const InputDate: React.FC<InputDateProps> = ({
  selected,
  calendarStartDay = 1,
  name,
  width,
  height = '48',
  label,
  required,
  popperDirection = 'auto',
  error,
  minDate,
  maxDate,
  dateFormat,
  placeholder,
  readOnly,
  disabled,
  tooltip,
  className,
  onChange,
  onFocus,
  onBlur,
  isLoading,
}) => {
  const wrapperRef = useRef<HTMLDivElement>(null);
  const [focused, setFocused] = useState(false);
  const [hovered, setHovered] = useState(false);
  const [hideTooltip, setHideTooltip] = useState(false);

  const isError = !!error?.messages?.filter((message) => !!message)?.length;

  const handleMouseEnter = () => {
    setHovered(true);
    !focused && setHideTooltip(false);
  };
  const handleMouseLeave = () => {
    setHovered(false);
  };
  const handleFocus = (e: React.FocusEvent<HTMLInputElement>) => {
    if (disabled || readOnly) return;

    onFocus?.(e);
    setFocused(true);
    setHideTooltip(true);
  };
  const handleBlur = (e: React.FocusEvent<HTMLInputElement>) => {
    if (disabled || readOnly) return;

    onBlur?.(e);
    setFocused(false);
    setHovered(false);
  };
  const handleChange = (date: Date) => {
    if (!(date instanceof Date && !isNaN(date.getTime()))) {
      setFocused(true);
    } else {
      setFocused(false);
      setHovered(false);
    }
    setHideTooltip(true);
    onChange(date);
  };

  const tooltipMessage = disabled ? 'Disabled' : hideTooltip ? '' : tooltip;

  return (
    <div
      ref={wrapperRef}
      className={classNames(
        style.wrapper,
        style[`width_${width}`],
        style[`height_${height}`],
        className,
      )}
    >
      {height === '48' && label && (
        <label
          className={classNames(style.label, {
            [style.required]: required,
            [style.hovered]: hovered,
            [style.focused]: focused,
            [style.notEmpty]: !!selected,
            [style.disabled]: disabled,
            [style.readOnly]: readOnly,
            [style.error]: isError,
          })}
          htmlFor={name}
        >
          {label}
        </label>
      )}
      <Tooltip
        message={tooltipMessage}
        wrapperClassName={style.input}
        onMouseEnter={handleMouseEnter}
        onMouseLeave={handleMouseLeave}
      >
        <DatePicker
          id={name}
          name={name}
          selected={selected}
          onChange={handleChange}
          className={classNames(
            'react-datepicker__input',
            `react-datepicker__input_height-${height}`,
            {
              'react-datepicker__input-error': isError,
              'react-datepicker__input-disabled': disabled,
              'react-datepicker__input-readonly': readOnly,
            },
            className,
          )}
          portalId="tooltip"
          calendarClassName="react-datepicker_dropdown"
          popperClassName={classNames(`react-datepicker-popper__width-${width}`)}
          calendarStartDay={calendarStartDay}
          minDate={minDate !== undefined ? subDays(new Date(), minDate) : undefined}
          maxDate={maxDate !== undefined ? addDays(new Date(), maxDate) : undefined}
          popperPlacement={popperDirection}
          showMonthDropdown
          showYearDropdown
          dateFormat={dateFormat}
          useShortMonthInDropdown
          yearDropdownItemNumber={4}
          disabledKeyboardNavigation
          showPopperArrow={false}
          readOnly={readOnly}
          disabled={disabled || readOnly}
          placeholderText={placeholder}
          onFocus={handleFocus}
          onBlur={handleBlur}
        />
        <Icon
          className={classNames(style.icon, { [style.error]: isError })}
          icon={<CalendarIcon />}
          path="inherit"
          clickThrough
        />
      </Tooltip>

      {isError && hovered && !focused && wrapperRef.current && (
        <InputErrorPopup inputWrapperElement={wrapperRef.current} errorMessage={error?.messages} />
      )}

      {isLoading && (
        <ContentLoader height="100%" isLoading={isLoading ?? false} isInsideComponent />
      )}
    </div>
  );
};

export default InputDate;
