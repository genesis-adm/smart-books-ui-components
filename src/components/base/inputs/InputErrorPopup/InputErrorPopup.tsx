import { Icon } from 'Components/base/Icon';
import { ReactComponent as DangerIcon } from 'Svg/v2/24/danger-square.svg';
import classNames from 'classnames';
import React, { ReactElement, ReactNode } from 'react';

import usePositioning, { Axis, PositionType } from '../../../../hooks/usePositioning';
import styles from './InputErrorPopup.module.scss';

export type InputErrorPopupProps = {
  inputWrapperElement: Element;
  staticPosition?: PositionType;
  direction?: Axis;
  errorMessage?: ReactNode;
};

const DEFAULT_WIDTH = '320';

const InputErrorPopup = ({
  inputWrapperElement,
  staticPosition,
  direction = 'vertical',
  errorMessage,
}: InputErrorPopupProps): ReactElement => {
  const { styles: dropWrapperStyle, mode } = usePositioning(
    inputWrapperElement,
    DEFAULT_WIDTH,
    staticPosition,
    direction,
  );

  return (
    <div
      className={classNames(styles.inputErrorPopup, styles[`mode_${mode}`])}
      style={dropWrapperStyle}
    >
      <div className={styles.titleWrapper}>
        <Icon icon={<DangerIcon />} />
        <div>
          <span className={styles.title}>{errorMessage}</span>
        </div>
      </div>
    </div>
  );
};

export default InputErrorPopup;
