export const showCountries = [
  'ar',
  'au',
  'at',
  'by',
  'be',
  'br',
  'vg',
  'bg',
  'ca',
  'ky',
  'cl',
  'cn',
  'co',
  'hr',
  'cu',
  'cy',
  'cz',
  'dk',
  'ee',
  'fi',
  'fr',
  'ge',
  'de',
  'gr',
  'hk',
  'hu',
  'is',
  'in',
  'id',
  'ir',
  'iq',
  'ie',
  'il',
  'it',
  'jp',
  'kz',
  'ke',
  'lu',
  'my',
  'mx',
  'md',
  'mc',
  'me',
  'ma',
  'nl',
  'ne',
  'ng',
  'no',
  'pk',
  'ph',
  'pl',
  'pt',
  'qa',
  'ro',
  'sa',
  'sg',
  'sk',
  'si',
  'za',
  'kr',
  'es',
  'se',
  'ch',
  'tw',
  'th',
  'tr',
  'vi',
  'ua',
  'ae',
  'gb',
  'us',
];

export const preferredCountries = ['cy', 'ua', 'gb', 'us'];

export const localizedCountries = {
  af: 'Afghanistan',
  al: 'Albania',
  dz: 'Algeria',
  am: 'Armenia',
  at: 'Austria',
  az: 'Azerbaijan',
  bh: 'Bahrain',
  bb: 'Barbados',
  bd: 'Bangladesh',
  by: 'Belarus',
  be: 'Belgium',
  bj: 'Benin',
  bt: 'Bhutan',
  ba: 'Bosnia and Herzegovina',
  br: 'Brazil',
  bg: 'Bulgaria',
  bi: 'Burundi',
  kh: 'Cambodia',
  cm: 'Cameroon',
  cv: 'Cape Verde',
  cf: 'Central African Republic',
  td: 'Chad',
  cn: 'China',
  km: 'Comoros',
  cd: 'Congo (DRC)',
  cg: 'Congo (Republic)',
  hr: 'Croatia',
  cy: 'Cyprus',
  cz: 'Czech Republic',
  dk: 'Denmark',
  do: 'Dominican Republic',
  eg: 'Egypt',
  gq: 'Equatorial Guinea',
  ee: 'Estonia',
  fk: 'Falkland Islands',
  fo: 'Faroe Islands',
  fi: 'Finland',
  gf: 'French Guyana',
  pf: 'French Polynesia',
  ge: 'Georgia',
  de: 'Germany',
  gh: 'Ghana',
  gr: 'Greece',
  gl: 'Greenland',
  gn: 'Guinea',
  gw: 'Guinea-Bissau',
  hk: 'Hong Kong',
  hu: 'Hungary',
  is: 'Iceland',
  in: 'India',
  ir: 'Iran',
  iq: 'Iraq',
  il: 'Israel',
  it: 'Italy',
  jp: 'Japan',
  jo: 'Jordan',
  kz: 'Kazakhstan',
  kw: 'Kuwait',
  kg: 'Kyrgyzstan',
  la: 'Laos',
  lv: 'Latvia',
  lb: 'Lebanon',
  ly: 'Libya',
  lt: 'Lithuania',
  mo: 'Macau',
  mg: 'Madagascar',
  mr: 'Mauritania',
  mu: 'Mauritius',
  mx: 'Mexico',
  md: 'Moldova',
  mn: 'Mongolia',
  me: 'Montenegro',
  ma: 'Morocco',
  mz: 'Mozambique',
  mm: 'Myanmar',
  na: 'Namibia',
  np: 'Nepal',
  nl: 'Netherlands',
  nc: 'New Caledonia',
  ne: 'Niger',
  kp: 'North Korea',
  mk: 'North Macedonia',
  no: 'Norway',
  om: 'Oman',
  pk: 'Pakistan',
  ps: 'Palestine',
  pa: 'Panama',
  pe: 'Peru',
  pl: 'Poland',
  qa: 'Qatar',
  re: 'Reunion',
  ro: 'Romania',
  mf: 'Saint Martin',
  pm: 'Saint Pierre and Miquelon',
  st: 'São Tomé and Príncipe',
  sa: 'Saudi Arabia',
  sn: 'Senegal',
  rs: 'Serbia',
  sk: 'Slovakia',
  si: 'Slovenia',
  so: 'Somalia',
  kr: 'Korea',
  ss: 'South Sudan',
  es: 'Spain',
  lk: 'Sri Lanka',
  sd: 'Sudan',
  se: 'Sweden',
  ch: 'Switzerland',
  sy: 'Syria',
  tw: 'Taiwan',
  th: 'Thailand',
  tn: 'Tunisia',
  tr: 'Turkey',
  ua: 'Ukraine',
  ae: 'United Arab Emirates',
  us: 'United States',
  uz: 'Uzbekistan',
  va: 'Vatican City',
  vn: 'Vietnam',
  wf: 'Wallis and Futuna',
  eh: 'Western Sahara',
  ye: 'Yemen',
};
