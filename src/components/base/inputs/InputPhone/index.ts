export { default as InputPhone } from './InputPhone';
export type { InputPhoneProps, PhoneNumberValue } from './InputPhone';
