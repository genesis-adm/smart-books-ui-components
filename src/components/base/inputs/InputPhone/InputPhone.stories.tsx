import { Meta, Story } from '@storybook/react';
import {
  InputPhone as Component,
  InputPhoneProps as Props,
} from 'Components/base/inputs/InputPhone';
import React, { useEffect, useState } from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Inputs/InputPhone',
  component: Component,
  argTypes: {
    id: hideProperty,
    name: hideProperty,
    value: hideProperty,
    error: hideProperty,
    className: hideProperty,
    onChange: hideProperty,
    onBlur: hideProperty,
    tooltipWidth: hideProperty,
    tooltip: hideProperty,
    defaultCountry: hideProperty,
  },
} as Meta;

const InputPhoneTemplate: Story<Props & { isError: boolean }> = (args) => {
  const [phoneNumber, setPhoneNumber] = useState('');
  const [isError, setIsError] = useState(false);

  const error = isError ? { messages: ['Test error!!!'] } : undefined;

  const handleChange = ({ phone, isValid }: { phone: string; isValid: boolean }) => {
    setPhoneNumber(phone);
    setIsError(!isValid);
  };

  useEffect(() => {
    setIsError(args.isError);
  }, [args.isError]);

  return <Component {...args} value={phoneNumber} onChange={handleChange} error={error} />;
};

export const InputPhone = InputPhoneTemplate.bind({});
InputPhone.args = {
  disabled: false,
  readOnly: false,
  isError: false,
  isLoading: false,
  width: '300',
  required: true,
  id: 'phone-id',
  name: 'phone-name',
  label: 'Phone number',
  background: 'grey-10',
};
