import { ContentLoader } from 'Components/base/ContentLoader';
import InputErrorPopup from 'Components/base/inputs/InputErrorPopup/InputErrorPopup';
import classNames from 'classnames';
import intlTelInput, { Plugin } from 'intl-tel-input';
import React, { useEffect, useRef, useState } from 'react';

import { InputBaseProps, InputWidthType } from '../../../../types/inputs';
import style from './InputPhone.module.scss';
import {
  localizedCountries,
  showCountries as onlyCountries,
  preferredCountries,
} from './countries';

export interface PhoneNumberValue {
  phone: string;
  isValid: boolean;
}

export interface InputPhoneProps
  extends InputBaseProps,
    Omit<React.InputHTMLAttributes<HTMLInputElement>, 'onChange' | 'onBlur' | 'name'> {
  width?: InputWidthType;
  defaultCountry?: keyof typeof localizedCountries;
  value: string;

  onChange?: (data: PhoneNumberValue) => void;
  onBlur?: (data: PhoneNumberValue) => void;
}

const InputPhone: React.FC<InputPhoneProps> = ({
  id,
  name,
  label,
  required,
  width = '300',
  error,
  defaultCountry = 'us',
  disabled = false,
  className,
  value,
  onChange,
  onBlur,
  isLoading = false,
  readOnly = false,
}) => {
  const wrapperRef = useRef<HTMLDivElement>(null);
  const inputWrapperElement = wrapperRef?.current;

  const [focused, setFocused] = useState(false);
  const [hovered, setHovered] = useState(false);

  const isError = !!error?.messages?.filter((message) => !!message)?.length;

  const inputActive = focused || value;
  const telInput = useRef<Plugin>();
  const browserLanguage = navigator.language.slice(0, 2);
  const selectedCountry = onlyCountries.includes(browserLanguage) ? browserLanguage : 'us';
  useEffect(() => {
    const input = document.querySelector(`#${id || name}`) as Element;
    telInput.current = intlTelInput(input, {
      nationalMode: false,
      initialCountry: defaultCountry || selectedCountry,
      preferredCountries,
      localizedCountries,
      onlyCountries,
      utilsScript: 'https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.19/js/utils.min.js',
    });
  }, [defaultCountry, id, selectedCountry, name]);

  const handleChange = (cb?: (data: PhoneNumberValue) => void) => () => {
    const result: PhoneNumberValue = {
      phone: telInput.current?.getNumber() ?? '',
      isValid: telInput.current?.isValidNumber() ?? false,
    };

    cb?.(result);
  };

  const handleBlur = () => {
    handleChange(onBlur);
    setFocused(false);
  };

  return (
    <div
      ref={wrapperRef}
      className={classNames(
        style.wrapper,
        style[`width_${width}`],
        {
          [style.disabled]: disabled,
          [style.readOnly]: readOnly,
          [style.error]: isError,
        },
        className,
      )}
    >
      <input
        className={classNames(style.input, {
          [style.hovered]: hovered,
          [style.focus]: focused,
          [style.inputActive]: inputActive,
          [style.paddingCorrection]: value === '+',
          [style.disabled]: disabled,
          [style.readOnly]: readOnly,
          [style.error]: isError,
        })}
        type="tel"
        name={name}
        id={id || name}
        value={value}
        required={required}
        disabled={disabled || readOnly || isLoading}
        readOnly={readOnly}
        onChange={handleChange(onChange)}
        onFocus={() => setFocused(true)}
        onBlur={handleBlur}
        onMouseEnter={() => setHovered(true)}
        onMouseLeave={() => setHovered(false)}
      />
      <span
        className={classNames(style.label, {
          [style.hovered]: hovered,
          [style.focus]: focused,
          [style.inputActive]: inputActive,
          [style.disabled]: disabled,
          [style.readOnly]: readOnly,
          [style.error]: isError,
        })}
      >
        {label}
        {required && <span className={style.required}>*</span>}
      </span>

      {isLoading && (
        <ContentLoader height="100%" isLoading={isLoading ?? false} isInsideComponent />
      )}

      {isError && hovered && !focused && inputWrapperElement && (
        <InputErrorPopup inputWrapperElement={inputWrapperElement} errorMessage={error?.messages} />
      )}
    </div>
  );
};

export default InputPhone;
