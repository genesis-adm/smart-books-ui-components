import { ContentLoader } from 'Components/base/ContentLoader';
import { Tooltip } from 'Components/base/Tooltip';
import classNames from 'classnames';
import React, { CSSProperties, ReactElement, ReactNode, useEffect, useRef } from 'react';

import useClickOutside from '../../../../hooks/useClickOutside';
import useHover from '../../../../hooks/useHover';
import { InputBackgroundType, InputHeightType, InputWidthType } from '../../../../types/inputs';
import { TooltipWidth } from '../../../../types/tooltips';
import { TextAreaMinHeight } from '../../../inputs/TextArea/TextArea.types';
import styles from './InputWrapper.module.scss';

type InputWrapperRenderProps = {
  inputWrapperElement: Element | null;
  isHovered: boolean;
};

export interface InputWrapperProps {
  width?: InputWidthType;
  height?: InputHeightType;
  tooltip?: ReactNode;
  tooltipWidth?: TooltipWidth;
  className?: string;
  style?: CSSProperties;
  children: (props: InputWrapperRenderProps) => ReactNode;
  background?: InputBackgroundType;

  isError?: boolean;

  onHover?: (value: boolean) => void;
  onClick?: (e: React.SyntheticEvent<Element, Event>) => void;
  onBlur?: () => void;
  isFocused?: boolean;
  isLoading?: boolean;
  onChangeHeight?: (height: number) => void;

  autoResizable?: boolean;

  disabled?: boolean;
  readOnly?: boolean;

  // textarea
  minHeight?: TextAreaMinHeight;
}

const InputWrapper = ({
  width,
  height,
  minHeight,
  background = 'grey-10',
  tooltip,
  tooltipWidth,
  className,
  children,
  isError,
  onHover,
  onClick,
  isFocused,
  isLoading = false,
  onBlur,
  onChangeHeight,
  style,
  disabled,
  autoResizable = false,
  readOnly = false,
}: InputWrapperProps): ReactElement => {
  const wrapperRef = useRef<HTMLDivElement>(null);

  const isWrapperHover = useHover(wrapperRef);
  const isView = readOnly || isLoading;

  const tooltipMessage = disabled && !readOnly ? 'Disabled' : isFocused ? '' : tooltip;

  useEffect(() => {
    if (disabled) return;

    onHover?.(isWrapperHover);
  }, [isWrapperHover, onHover, disabled]);

  useClickOutside({
    ref: wrapperRef?.current,
    cb: () => {
      if (disabled) return;
      onBlur?.();
    },
  });

  useEffect(() => {
    const clientRect = wrapperRef?.current?.getBoundingClientRect();

    if (!clientRect) return;

    onChangeHeight?.(clientRect?.height);
  }, [onChangeHeight]);

  return (
    <Tooltip
      ref={wrapperRef}
      message={tooltipMessage}
      tooltipWidth={tooltipWidth}
      wrapperClassName={classNames(
        styles.inputWrapper,
        styles[`width_${width}`],
        styles[`height_${height}`],
        styles[`minHeight_${minHeight}`],
        styles[`background_${background}`],
        {
          [styles.autoResizable]: autoResizable,
          [styles.hovered]: isWrapperHover && !disabled && !isView,
          [styles.focus]: isFocused,
          [styles.disabled]: disabled,
          [styles.view]: isView,
          [styles.error]: isError,
        },
        className,
      )}
      onClick={(e) => {
        if (disabled || isView) return;
        onClick?.(e);
      }}
      disabled={disabled}
      cursor="pointer"
      style={style}
      innerClassName={styles.innerTooltip}
    >
      {isLoading && <ContentLoader height="100%" isLoading={isLoading} isInsideComponent />}

      {children({
        inputWrapperElement: wrapperRef?.current,
        isHovered: isWrapperHover,
      })}
    </Tooltip>
  );
};

export default InputWrapper;
