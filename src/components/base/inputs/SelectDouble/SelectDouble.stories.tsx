import { Meta, Story } from '@storybook/react';
import { Container } from 'Components/base/grid/Container';
import {
  SelectDouble as Component,
  SelectDoubleProps as Props,
} from 'Components/base/inputs/SelectDouble';
import { OptionDoubleVerticalNew } from 'Components/base/inputs/SelectNew/options/OptionDoubleVerticalNew';
import { options } from 'Mocks/fakeOptions';
import { ReactComponent as Mastercard16Icon } from 'Svg/v2/16/logo-mastercard.svg';
import { ReactComponent as Mastercard24Icon } from 'Svg/v2/24/logo-mastercard.svg';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Select/Select Double',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const SelectDoubleComponent: Story<Props> = (args) => (
  <Container flexdirection="column" gap="4">
    <Component {...args}>
      {({ onClick, state, searchValue }) => (
        <>
          {options
            .filter(({ label }) => label.toLowerCase().includes(searchValue.toLowerCase()))
            .map(({ label, secondaryLabel, value }) => (
              <OptionDoubleVerticalNew
                tooltip
                key={value}
                label={label}
                secondaryLabel={secondaryLabel}
                selected={state?.value === value}
                onClick={() => onClick({ label, secondaryLabel, value })}
              />
            ))}
        </>
      )}
    </Component>
    <Component {...args} icon={<Mastercard16Icon />}>
      {({ onClick, state, searchValue }) => (
        <>
          {options
            .filter(({ label }) => label.toLowerCase().includes(searchValue.toLowerCase()))
            .map(({ label, secondaryLabel, value }) => (
              <OptionDoubleVerticalNew
                key={value}
                label={label}
                secondaryLabel={secondaryLabel}
                selected={state?.value === value}
                onClick={() => onClick({ label, secondaryLabel, value })}
              />
            ))}
        </>
      )}
    </Component>
    <Component {...args} icon={<Mastercard24Icon />}>
      {({ onClick, state, searchValue }) => (
        <>
          {options
            .filter(({ label }) => label.toLowerCase().includes(searchValue.toLowerCase()))
            .map(({ label, secondaryLabel, value }) => (
              <OptionDoubleVerticalNew
                key={value}
                label={label}
                secondaryLabel={secondaryLabel}
                selected={state?.value === value}
                onClick={() => onClick({ label, secondaryLabel, value })}
              />
            ))}
        </>
      )}
    </Component>
  </Container>
);

export const SelectDouble = SelectDoubleComponent.bind({});
SelectDouble.args = {
  label: 'Information label',
  placeholder: 'Search',
};
