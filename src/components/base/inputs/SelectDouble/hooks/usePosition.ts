import React, { useState } from 'react';

export default function usePosition() {
  const [position, setPosition] = useState('bottom-right');

  const setPositionOptions = (e: React.MouseEvent<HTMLElement>) => {
    if (
      e.clientY * 2 >
      (window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight)
    ) {
      setPosition('top-right');
    } else {
      setPosition('bottom-right');
    }
  };

  return {
    position,
    setPosition,
    setPositionOptions,
  };
}
