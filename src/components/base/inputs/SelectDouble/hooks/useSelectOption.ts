import { SelectDoubleContext } from 'Components/base/inputs/SelectDouble/SelectDouble';
import { Option } from 'Components/base/inputs/SelectDouble/SelectDouble.types';
import { useContext } from 'react';

export default function useSelectOption({ option }: { option: Option }) {
  const { onClick } = useContext(SelectDoubleContext);

  const handleClick = () => onClick(option);

  return {
    onClick: handleClick,
  };
}
