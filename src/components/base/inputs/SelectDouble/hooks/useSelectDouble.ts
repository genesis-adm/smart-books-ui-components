import { Option } from 'Components/base/inputs/SelectDouble/SelectDouble.types';
import { useCallback, useState } from 'react';

interface UseSelectDoubleProps {
  defaultValue: Option;
  value: Option;
}

export default function useSelectDouble({ defaultValue, value }: UseSelectDoubleProps) {
  const [state, setState] = useState<Option>(() => Object.assign({}, defaultValue, value));
  const [isOpen, setOpen] = useState(false);
  const [focus, setFocus] = useState(false);

  const setSelectActive = (arg: boolean) => {
    setOpen(arg);
    setFocus(arg);
  };

  const handleClick = useCallback((data: Option) => {
    setState(data);
    setSelectActive(false);
  }, []);

  const handleClickMultiple = useCallback((data: Option) => {
    setState(data);
  }, []);

  return {
    isOpen,
    focus,
    setSelectActive,
    state,
    setState,
    onClick: handleClick,
    onClickMultiple: handleClickMultiple,
  };
}
