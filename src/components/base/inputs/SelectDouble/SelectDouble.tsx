import { Icon } from 'Components/base/Icon';
import { Tooltip } from 'Components/base/Tooltip';
import useSelectDouble from 'Components/base/inputs/SelectDouble/hooks/useSelectDouble';
import { OptionNoResults } from 'Components/base/inputs/SelectNew/options/OptionNoResults';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as CaretDownIcon } from 'Svg/v2/16/caret-down.svg';
import classNames from 'classnames';
import React, {
  ReactElement,
  createContext,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';

import useClickOutside from '../../../../hooks/useClickOutside';
import usePositioning from '../../../../hooks/usePositioning';
import useScrollOutside from '../../../../hooks/useScrollOutside';
import style from './SelectDouble.module.scss';
import { Option, SelectDoubleContextProps, SelectDoubleProps } from './SelectDouble.types';

export const SelectDoubleContext = createContext({} as SelectDoubleContextProps);

const SelectDouble = ({
  width = '320',
  height = '48',
  name,
  disabled,
  // required,
  error,
  placeholder,
  label,
  icon,
  iconStaticColor,
  tooltip,
  readOnly,
  value = { label: '', secondaryLabel: '', value: '' },
  defaultValue = { label: '', secondaryLabel: '', value: '' },
  isEmpty,
  direction = 'vertical',
  staticPosition,
  reset,
  onChange,
  children,
  className,
}: SelectDoubleProps): ReactElement => {
  const wrapperRef = useRef<HTMLDivElement>(null);
  const [searchValue, setSearchValue] = useState<string | null>(null);

  // const tooltipMessage = disabled ? 'Disabled' : inputActive ? '' : tooltip;

  const { isOpen, focus, setSelectActive, onClick, onClickMultiple, state, setState } =
    useSelectDouble({
      defaultValue,
      value,
    });

  useClickOutside({ ref: wrapperRef.current, cb: setSelectActive });
  useScrollOutside({ isListOpen: isOpen, cb: setSelectActive });
  const { styles, mode } = usePositioning(
    wrapperRef.current,
    width || '320',
    // listMinWidth || elementWidth || '320',
    staticPosition,
    direction,
  );

  const handleToggle = () => {
    if (!isOpen && !disabled && (!readOnly || (!readOnly && !focus))) {
      setSelectActive(!isOpen);
    }
  };

  const handleClick = useCallback(
    (value: Option) => {
      if (onChange) onChange(value);
      setSearchValue(null);
      onClick(value);
    },
    [onChange, onClick],
  );

  const handleClickMultiple = useCallback(
    (value: Option) => {
      if (onChange) onChange(value);
      setSearchValue(null);
      onClickMultiple(value);
    },
    [onChange, onClickMultiple],
  );

  const context = useMemo(
    () => ({
      isOpen,
      focus,
      setSelectActive,
      setState,
      onClick: handleClick,
      onClickMultiple: handleClickMultiple,
      state,
    }),
    [state, setState, isOpen, focus, setSelectActive, handleClick, handleClickMultiple],
  );

  const controls = useMemo(
    () => ({
      onClick: handleClick,
      // onClickMultiple: handleClickMultiple,
      state,
      searchValue: searchValue || '',
    }),
    [handleClick, state, searchValue],
  );

  useEffect(() => {
    if (reset) setState({ label: '', secondaryLabel: '', value: '' });
  }, [reset, setState]);

  return (
    <SelectDoubleContext.Provider value={context}>
      <Tooltip
        ref={wrapperRef}
        message={tooltip}
        wrapperClassName={classNames(
          style.wrapper,
          style[`width_${width}`],
          style[`height_${height}`],
          {
            [style.active]: isOpen,
            [style.error]: error,
          },
          className,
        )}
        onClick={handleToggle}
      >
        {icon && (
          <Icon
            className={style.icon}
            icon={icon}
            staticColor={iconStaticColor}
            path="inherit"
            cursor="text"
            clickThrough
            transition="inherit"
          />
        )}
        <div className={style.main}>
          <Text
            type="inherit"
            className={classNames(style.label, {
              [style.active]: isOpen,
              [style.hidden]: !isOpen && state.value,
              [style.transitionOff]: isOpen && state.value,
            })}
            lineClamp="1"
          >
            {label}
          </Text>

          <input
            className={classNames(style.input, { [style.active]: isOpen })}
            name={name}
            placeholder={isOpen ? placeholder : ''}
            value={searchValue ?? state?.label}
            onChange={(e) => {
              if (!readOnly) setSearchValue(e.target.value);
            }}
            onBlur={() => {
              setSearchValue(null);
            }}
            disabled={disabled}
            readOnly={readOnly}
            autoComplete="off"
          />
          {state.secondaryLabel && !isOpen && (
            <Text className={style.secondaryValue} lineClamp="1" color="inherit">
              {state.secondaryLabel}
            </Text>
          )}
          {/*   <InputNew
            className={style.search}
          /> */}
        </div>
        <Icon
          icon={<CaretDownIcon />}
          path="inherit"
          className={classNames(style.caret, {
            [style.active]: isOpen,
            // [styles.error]: error,
          })}
          clickThrough
        />
        {isOpen && (
          <ul className={classNames(style.list, style[`mode_${mode}`])} style={styles}>
            {isEmpty ? <OptionNoResults /> : children(controls)}
          </ul>
        )}
      </Tooltip>
    </SelectDoubleContext.Provider>
  );
};

export default SelectDouble;
