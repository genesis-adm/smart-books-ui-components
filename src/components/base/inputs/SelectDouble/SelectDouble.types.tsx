import { ReactElement, ReactNode } from 'react';

import { Axis, PositionType } from '../../../../hooks/usePositioning';

export interface Option {
  value: string;
  label: string;
  secondaryLabel: string;
  [key: string]: unknown;
}

export interface SelectDoubleContextProps {
  isOpen: boolean;
  state: Option | null;
  onClick(arg: Option): void;
  setSelectActive(arg: boolean): void;
  setState(arg: Option): void;
}

export interface SelectDoubleProps {
  width?: '320';
  height?: '48';
  name: string;
  disabled?: boolean;
  // required?: boolean;
  error?: boolean;
  placeholder?: string;
  label: string;
  icon?: ReactNode;
  iconStaticColor?: boolean;
  tooltip?: ReactNode;
  value?: Option;
  defaultValue?: Option;
  isEmpty?: boolean;
  readOnly?: boolean;
  staticPosition?: PositionType;
  direction?: Axis;
  reset?: boolean;
  onChange?(option: Option): void;
  children(args: {
    onClick(option: Option): void;
    // onClickMultiple(option: Option): void;
    state: Option | null;
    searchValue: string;
  }): ReactElement;
  className?: string;
}

// import {
//   InputWidthType,
//   InputHeightType,
//   InputBackgroundType,
//   InputColorType,
//   InputBorderType,
// } from 'Components/base/inputs/InputNew';

// export interface SelectProps {
//   errorsList?: string[];
//   background?: InputBackgroundType;
//   color?: InputColorType;
//   border?: InputBorderType;
//   description?: string;
//   listMinWidth?: 42 | 150 | 180 | 192 | 200 | 300 | 320 | 624;
// }
