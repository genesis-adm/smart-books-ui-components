import { TextAlign } from 'Components/types/fontTypes';
import React, { InputHTMLAttributes } from 'react';

import { InputBackgroundType, InputBorderType, InputColorType } from '../../../../types/inputs';

export interface DescriptionInputProps extends InputHTMLAttributes<HTMLInputElement> {
  name: string;
  label: string;
  placeholder?: string;
  background?: InputBackgroundType;
  width?: '300' | 'full';
  color?: InputColorType;
  border?: InputBorderType;
  align?: TextAlign;
  required?: boolean;
  disabled?: boolean;
  readOnly?: boolean;
  active?: boolean;
  error?: boolean;
  errorsList?: string[];
  maxLength?: number;
  minHeight?: '36' | '88' | '134' | '155';
  tooltip?: React.ReactNode;
  description?: string;
  iconLeft?: React.ReactNode;
  className?: string;
  value: string;
  isLoading?: boolean;
  style?: React.CSSProperties;

  onChange(e: React.ChangeEvent<HTMLInputElement>): void;
}
