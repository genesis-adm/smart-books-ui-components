import { ContentLoader } from 'Components/base/ContentLoader';
import { Icon } from 'Components/base/Icon';
import { Tooltip } from 'Components/base/Tooltip';
import { InputDescription } from 'Components/base/inputs/InputElements/InputDescription';
import { InputErrorList } from 'Components/base/inputs/InputElements/InputErrorList';
import InputErrorPopup from 'Components/base/inputs/InputErrorPopup/InputErrorPopup';
import classNames from 'classnames';
import React, { ReactElement, useEffect, useLayoutEffect, useRef, useState } from 'react';

import styles from './DescriptionInput.module.scss';
import { DescriptionInputProps } from './DescriptionInput.types';

// Reminder: do not forget to add the following code to root index.ts
// export { DescriptionInput } from './components/base/inputs/DescriptionInput';

const DescriptionInput = ({
  name,
  label,
  placeholder = ' ',
  background = 'grey-10',
  color = 'default',
  border = 'default',
  width,
  align,
  required,
  disabled,
  readOnly,
  active,
  error,
  errorsList,
  maxLength = 200,
  minHeight = '88',
  tooltip,
  description,
  iconLeft,
  className,
  value,
  onChange,
  onFocus,
  onBlur,
  isLoading,
  style,
}: DescriptionInputProps): ReactElement => {
  const wrapperRef = useRef<HTMLDivElement>(null);
  const inputRef = useRef<HTMLInputElement>(null);
  const inputRefDep = inputRef.current && inputRef.current.clientWidth;

  useLayoutEffect(() => {
    if (inputRef.current && minHeight !== '36') {
      inputRef.current.style.height = '0px';
      const scrollHeight = inputRef.current.scrollHeight + 2;
      inputRef.current.style.height = scrollHeight + 'px';
    }
  }, [value]);
  const [focused, setFocused] = useState(false);
  const [hovered, setHovered] = useState(false);
  const [tooltipValue, setTooltipValue] = useState<React.ReactNode>(tooltip);

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    onChange?.(e);
  };
  const handleFocus = (e: React.FocusEvent<HTMLInputElement>) => {
    onFocus?.(e);
    setFocused(true);
  };
  const handleBlur = (e: React.FocusEvent<HTMLInputElement>) => {
    onBlur?.(e);
    setFocused(false);
  };
  useEffect(() => {
    if (
      !tooltip &&
      inputRef.current &&
      !focused &&
      (inputRef.current.scrollHeight > inputRef.current.clientHeight ||
        inputRef.current.scrollWidth > inputRef.current.clientWidth)
    ) {
      setTooltipValue(value);
    } else {
      setTooltipValue('');
    }
  }, [tooltip, value, inputRefDep, focused]);

  return (
    <>
      <Tooltip
        ref={wrapperRef}
        message={tooltipValue}
        wrapperClassName={classNames(styles.wrapper, className, styles[`width_${width}`], {})}
        innerClassName={styles.inner_tooltip}
      >
        <div
          className={styles.component}
          onMouseEnter={() => setHovered(true)}
          onMouseLeave={() => setHovered(false)}
        >
          <input
            className={classNames(
              styles.textarea,
              styles[`background_${background}`],
              styles[`color_${color}`],
              styles[`border_${border}`],
              styles[`minHeight_${minHeight}`],
              {
                [styles[`text-align_${align}`]]: align,
                [styles.active]: active,
                [styles.disabled]: disabled,
                [styles.error]: error || value.length >= maxLength,
              },
            )}
            style={style}
            id={name}
            maxLength={maxLength}
            placeholder={placeholder}
            required={required}
            disabled={disabled}
            readOnly={readOnly}
            value={value}
            onChange={handleChange}
            onFocus={handleFocus}
            onBlur={handleBlur}
            ref={inputRef}
          />
          {iconLeft && <Icon icon={iconLeft} path="inherit" className={styles.icon} />}
          <span className={classNames(styles.label, {})}>
            {label}
            {required && <span className={styles.required}>*</span>}
          </span>
          <span
            className={classNames(styles.counter, {
              [styles.error]: error,
            })}
          >
            <span
              className={classNames(styles.current, {
                [styles.limit]: value?.length >= maxLength,
                [styles.error]: value?.length === 0 && error,
              })}
            >
              {value?.length}
            </span>
            /{maxLength}
          </span>
          {isLoading && (
            <ContentLoader height="100%" isLoading={isLoading ?? false} isInsideComponent />
          )}
        </div>
      </Tooltip>
      {description && <InputDescription description={description} width="full" />}
      {errorsList && <InputErrorList errorsList={errorsList} width="full" />}

      {hovered && focused && wrapperRef.current && value.length >= maxLength && (
        <InputErrorPopup
          inputWrapperElement={wrapperRef.current}
          errorMessage={`Maximum ${maxLength} characters exceeded`}
        />
      )}
    </>
  );
};

export default DescriptionInput;
