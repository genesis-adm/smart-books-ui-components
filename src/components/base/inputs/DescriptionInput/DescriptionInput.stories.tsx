import { Meta, Story } from '@storybook/react';
import {
  DescriptionInput as Component,
  DescriptionInputProps as Props,
} from 'Components/base/inputs/DescriptionInput';
import { ReactComponent as CurrencyIcon } from 'Svg/v2/16/banknote.svg';
import React, { useState } from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Inputs/DescriptionInput',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const DescriptionInputComponent: Story<Props> = (args) => {
  const [val, setVal] = useState('');

  return (
    <Component
      {...args}
      value={val}
      onChange={(e) => {
        setVal(e.target.value);
      }}
    />
  );
};

export const DescriptionInput = DescriptionInputComponent.bind({});
DescriptionInput.args = {
  maxLength: 200,
  width: '300',
  placeholder: 'Enter',
  minHeight: '36',
  type: 'description',
  label: 'Description',
  iconLeft: <CurrencyIcon />,
};
