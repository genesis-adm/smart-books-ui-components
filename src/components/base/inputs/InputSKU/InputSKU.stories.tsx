import { Meta, Story } from '@storybook/react';
import type { TagType } from 'Components/base/inputs/InputElements/InputTag';
import { InputSKU as Component, InputSKUProps as Props } from 'Components/base/inputs/InputSKU';
import React, { useState } from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Inputs/InputSKU',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const InputSKUComponent: Story<Props> = (args) => {
  const [val, setVal] = useState('');
  const [SKUList, setSKUList] = useState<TagType[]>([]);
  const handleSKUDelete = (value: string) =>
    setSKUList((prevState) => {
      return prevState.filter((item: TagType) => item.id !== value);
    });
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setVal(e.target.value);
  };
  const handleSKUAdd = () => {
    if (!SKUList.find((item: TagType) => item.id === val)) {
      setSKUList((prevState) => [
        ...prevState,
        { id: val, label: val, onDelete: () => handleSKUDelete(val) },
      ]);
      setVal('');
    }
  };
  return (
    <Component
      {...args}
      value={val}
      onChange={handleChange}
      SKU={SKUList}
      onSKUAdd={handleSKUAdd}
      onClearAllSKU={() => {
        setSKUList([]);
      }}
    />
  );
};

export const InputSKU = InputSKUComponent.bind({});
InputSKU.args = {};
