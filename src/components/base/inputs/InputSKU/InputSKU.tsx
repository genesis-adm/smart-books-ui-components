import { Icon } from 'Components/base/Icon';
import type { TagType } from 'Components/base/inputs/InputElements/InputTag';
import { InputNew } from 'Components/base/inputs/InputNew';
import { ReactComponent as CloseIcon } from 'Svg/v2/16/cross.svg';
import { ReactComponent as QuestionIcon } from 'Svg/v2/16/question-in-filled-circle.svg';
import classNames from 'classnames';
import React from 'react';

import { InputWidthType } from '../../../../types/inputs';
import style from './InputSKU.module.scss';

export interface InputSKUProps extends React.InputHTMLAttributes<HTMLInputElement> {
  name: string;
  // label: string;
  width: InputWidthType;
  SKU: TagType[];
  required?: boolean;
  disabled?: boolean;
  error?: boolean;
  errorsList?: string[];
  // tooltip?: React.ReactNode;
  description?: string;
  className?: string;
  value: string;

  onSKUAdd(SKU: string): void;

  onClearAllSKU(): void;
}

const InputSKU: React.FC<InputSKUProps> = ({
  name,
  width = 'full',
  SKU,
  required,
  disabled,
  error,
  errorsList,
  description,
  className,
  value,
  onChange,
  onSKUAdd,
  onClearAllSKU,
}) => {
  const handleSKUAdd = ({ key }: React.KeyboardEvent<HTMLInputElement>) => {
    if (key === 'Enter' && value) {
      onSKUAdd(value);
    }
  };
  return (
    <InputNew
      className={classNames(style.input, className)}
      name={name}
      label="SKU"
      placeholder="Enter SKU"
      width={width}
      tooltip="You may add several product SKU"
      icon={<QuestionIcon />}
      action={
        SKU.length > 0 && (
          <Icon
            className={style.clear}
            icon={<CloseIcon />}
            path="inherit"
            cursor="pointer"
            onClick={onClearAllSKU}
          />
        )
      }
      elementPosition="left"
      tags={SKU}
      value={value}
      onChange={onChange}
      onKeyDown={handleSKUAdd}
      required={required}
      disabled={disabled}
      error={error}
      errorsList={errorsList}
      description={description}
    />
  );
};

export default InputSKU;
