import { Meta, Story } from '@storybook/react';
import { SelectDropdown } from 'Components/SelectDropdown';
import SelectDropdownBody from 'Components/SelectDropdown/components/SelectDropdownBody/SelectDropdownBody';
import { SelectDropdownFooter } from 'Components/SelectDropdown/components/SelectDropdownFooter';
import SelectDropdownItemList from 'Components/SelectDropdown/components/SelectDropdownItemList/SelectDropdownItemList';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { NumericInput } from 'Components/base/inputs/NumericInput';
import { SelectNew as Component, SelectProps as Props } from 'Components/base/inputs/SelectNew';
import { OptionNoResults } from 'Components/base/inputs/SelectNew/options/OptionNoResults';
import { OptionSingleNew } from 'Components/base/inputs/SelectNew/options/OptionSingleNew';
import { options } from 'Mocks/fakeOptions';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as Reload16Icon } from 'Svg/v2/16/reload.svg';
import React, { useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Select/Select New',
  component: Component,
  argTypes: {
    name: hideProperty,
    defaultValue: hideProperty,
    reset: hideProperty,
    onChange: hideProperty,
    className: hideProperty,
  },
} as Meta;

const SelectNewComponent: Story<Props> = (args) => {
  const [inputValue, setInputValue] = useState<number>();

  return (
    <Component {...args}>
      {({ onClick, state, searchValue, selectElement }) => {
        const filteredOptions = options.filter(({ label }) =>
          label.toLowerCase().includes(searchValue.toLowerCase()),
        );

        const isFilteredOptions = !!filteredOptions.length;

        return (
          <SelectDropdown selectElement={selectElement}>
            <SelectDropdownBody>
              {!isFilteredOptions && <OptionNoResults />}

              {isFilteredOptions && (
                <SelectDropdownItemList>
                  {filteredOptions.map(({ label, value }, index) => (
                    <OptionSingleNew
                      key={value}
                      label={label}
                      selected={state?.value === value}
                      icon={<Reload16Icon />}
                      onClick={() => {
                        onClick({
                          label: inputValue?.toString() ?? label,
                          value,
                        });
                      }}
                      input={
                        !(index % 2) ? (
                          <NumericInput
                            name="num"
                            label=""
                            height="28"
                            width="80"
                            decimalScale={0}
                            value={inputValue}
                            onChange={(event) => {
                              event.stopPropagation();
                            }}
                            onValueChange={({ floatValue }) => {
                              setInputValue(floatValue);
                            }}
                          />
                        ) : undefined
                      }
                    />
                  ))}
                </SelectDropdownItemList>
              )}
            </SelectDropdownBody>

            <SelectDropdownFooter>
              <Container justifycontent="flex-end" className={spacing.mX12}>
                <LinkButton icon={<PlusIcon />} onClick={() => {}}>
                  Create new product
                </LinkButton>
              </Container>
            </SelectDropdownFooter>
          </SelectDropdown>
        );
      }}
    </Component>
  );
};

export const SelectNew = SelectNewComponent.bind({});
SelectNew.args = {
  disabled: false,
  required: true,
  tooltip: 'Test tooltip info',
  width: '320',
  height: '48',
  placeholder: 'Select option',
  label: 'Label text',
  isLoading: false,
  // labelInfo: 'Tooltip text',
  // disabled: false,
  // isRequired: true,
  // reset: false,
  // background: 'gray',
  // color: 'dark',
  // tooltip: 'Tooltip',
  // padding: 'default',
  // noBorder: false,
};
