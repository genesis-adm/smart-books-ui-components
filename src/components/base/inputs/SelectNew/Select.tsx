import { ContentLoader } from 'Components/base/ContentLoader';
import { Icon } from 'Components/base/Icon';
import { InputDescription } from 'Components/base/inputs/InputElements/InputDescription';
import { InputNew } from 'Components/base/inputs/InputNew';
import useSelect from 'Components/base/inputs/SelectNew/hooks/useSelect';
import { ReactComponent as CaretDownIcon } from 'Svg/v2/16/caret-down.svg';
import classNames from 'classnames';
import React, { createContext, useCallback, useEffect, useMemo, useRef, useState } from 'react';

import useClickOutside from '../../../../hooks/useClickOutside';
import useScrollOutside from '../../../../hooks/useScrollOutside';
import style from './Select.module.scss';
import { AccordionOption, Option, SelectContextProps, SelectProps } from './Select.types';

export const SelectContext = createContext({} as SelectContextProps);

const Select = ({
  name,
  width = '320',
  height = '48',
  tooltip,
  disabled,
  readOnly,
  required,
  error,
  errorMessage,
  errorsList,
  placeholder,
  label,
  tags,
  tagColor,
  tagWidth,
  background = 'grey-10',
  color = 'default',
  border = 'default',
  description,
  icon,
  iconStaticColor,
  defaultValue = {
    label: '',
    value: '',
  },
  value,
  reset,
  onChange,
  onAccordionChange,
  children,
  className,
  isLoading,
  onSearch,
  wrapTags,
  accordionValue,
}: SelectProps) => {
  const wrapperRef = useRef<HTMLDivElement>(null);
  const selectElement = wrapperRef?.current;

  const {
    isOpen,
    focus,
    setSelectActive,
    onClick,
    onClickMultiple,
    onClickAccordion,
    state,
    accordionState,
    setState,
    setAccordionState,
  } = useSelect(defaultValue);
  const [searchValue, setSearchValue] = useState<string | null>(null);

  useClickOutside({
    ref: wrapperRef.current,
    cb: setSelectActive,
  });
  useScrollOutside({
    isListOpen: isOpen,
    cb: setSelectActive,
  });

  const handleToggle = () => {
    if (!isOpen && !disabled && (!readOnly || (!readOnly && !focus))) {
      setSelectActive(!isOpen);
    }
  };

  useEffect(() => {
    if (value) setState(value);
  }, [value]);

  useEffect(() => {
    if (accordionValue && !accordionState.length) {
      setAccordionState(accordionValue);
    }
  }, [accordionValue]);

  const handleOnSearchValue = useCallback(
    (value: string | null) => {
      setSearchValue(value);
      onSearch?.(value);
    },
    [setSearchValue, onSearch],
  );

  const handleClick = useCallback(
    (value: Option) => {
      if (onChange) onChange(value);
      handleOnSearchValue(null);
      onClick(value);
    },
    [onChange, onClick, handleOnSearchValue],
  );

  const handleClickMultiple = useCallback(
    (value: Option) => {
      if (onChange) onChange(value);
      handleOnSearchValue(null);
      onClickMultiple(value);
    },
    [onChange, onClickMultiple, handleOnSearchValue],
  );

  const handleClickAccordion = useCallback(
    (value: AccordionOption[]) => {
      if (onAccordionChange) onAccordionChange(value);
      handleOnSearchValue(null);
      onClickAccordion(value);
    },
    [onAccordionChange, handleOnSearchValue, onClickAccordion],
  );

  const handleSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
    e.stopPropagation();

    if (!readOnly) {
      handleOnSearchValue(e.target.value);
    }
  };

  const handleBlur = () => {
    handleOnSearchValue(null);
  };

  const context = useMemo(
    () => ({
      isOpen,
      focus,
      setSelectActive,
      setState,
      setAccordionState,
      onClick: handleClick,
      onClickMultiple: handleClickMultiple,
      state,
      accordionState,
    }),
    [
      state,
      setState,
      isOpen,
      focus,
      setSelectActive,
      setAccordionState,
      handleClick,
      handleClickMultiple,
      accordionState,
    ],
  );

  const controls = useMemo(() => {
    if (!selectElement) return;

    return {
      isOpen,
      onClick: handleClick,
      onClickMultiple: handleClickMultiple,
      onClickAccordion: handleClickAccordion,
      state,
      accordionState,
      setAccordionState,
      searchValue: searchValue || '',
      selectElement: selectElement,
    };
  }, [
    isOpen,
    handleClick,
    handleClickMultiple,
    handleClickAccordion,
    state,
    accordionState,
    searchValue,
    selectElement,
    setAccordionState,
  ]);

  useEffect(() => {
    if (reset) {
      setState({
        label: '',
        value: '',
      });
      setAccordionState([]);
    }
  }, [reset, setState, setAccordionState]);

  return (
    <SelectContext.Provider value={context}>
      <div
        ref={wrapperRef}
        className={classNames(style.wrapper, className, style[`width_${width}`])}
      >
        <div
          role="presentation"
          tabIndex={-1}
          onClick={handleToggle}
          className={classNames(style.select, style[`height_${height}`], {
            [style.disabled]: disabled,
            [style.editable]: !readOnly,
          })}
        >
          <InputNew
            name={name}
            width={width}
            height={height}
            placeholder={placeholder}
            label={label}
            tooltip={isOpen || !!errorMessage ? '' : tooltip}
            background={background}
            color={color}
            border={border}
            icon={icon}
            tags={tags}
            tagColor={tagColor}
            tagWidth={tagWidth}
            wrapTags={wrapTags}
            iconStaticColor={iconStaticColor}
            value={searchValue === null ? state?.label : searchValue}
            accordionArr={accordionState}
            onChange={handleSearch}
            onBlur={handleBlur}
            disabled={disabled}
            readOnly={readOnly}
            required={height === '48' && required}
            error={error}
            errorMessage={errorMessage}
            errorsList={errorsList}
            active={isOpen}
            elementPosition={icon ? 'left' : undefined}
            className={style.search}
          />
          <Icon
            icon={<CaretDownIcon />}
            path="inherit"
            className={classNames(style.arrow, style[`color_${color}`], {
              [style.active]: isOpen,
              [style.error]: error,
            })}
            clickThrough
          />
        </div>
        {description && <InputDescription description={description} width={width} />}

        {isOpen && controls && children(controls)}

        {isLoading && (
          <ContentLoader height="100%" isLoading={isLoading ?? false} isInsideComponent />
        )}
      </div>
    </SelectContext.Provider>
  );
};

export default Select;
