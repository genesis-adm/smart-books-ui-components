import { Meta, Story } from '@storybook/react';
import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { SelectNew } from 'Components/base/inputs/SelectNew';
import {
  OptionDoubleHorizontalNew as Component,
  OptionDoubleHorizontalNewProps as Props,
} from 'Components/base/inputs/SelectNew/options/OptionDoubleHorizontalNew';
import { options } from 'Mocks/fakeOptions';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Select/Options/Option Double Horizontal New',
  component: Component,
  argTypes: {
    label: hideProperty,
    secondaryLabel: hideProperty,
    selected: hideProperty,
    icon: hideProperty,
    onClick: hideProperty,
    className: hideProperty,
  },
} as Meta;

export const OptionDoubleHorizontalNew: Story<Props> = () => (
  <SelectNew
    onChange={() => {}}
    // defaultValue={{ label: '', value: '' }}
    label="Choose option"
    placeholder="Choose option"
    name="test"
  >
    {({ onClick, state, selectElement }) => (
      <SelectDropdown selectElement={selectElement}>
        <SelectDropdownBody>
          <SelectDropdownItemList>
            {options.map(({ label, secondaryLabel, value }) => (
              <Component
                key={value}
                label={label}
                secondaryLabel={secondaryLabel}
                selected={state?.value === value}
                onClick={() =>
                  onClick({
                    label,
                    value,
                  })
                }
              />
            ))}
          </SelectDropdownItemList>
        </SelectDropdownBody>
      </SelectDropdown>
    )}
  </SelectNew>
);
