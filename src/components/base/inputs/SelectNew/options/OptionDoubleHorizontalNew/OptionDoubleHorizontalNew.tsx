import classNames from 'classnames';
import React from 'react';

import style from './OptionDoubleHorizontalNew.module.scss';

export interface OptionDoubleHorizontalNewProps {
  label: string;
  secondaryLabel: string;
  selected: boolean;
  icon?: React.ReactNode;
  className?: string;
  onClick(): void;
}

const OptionDoubleHorizontalNew: React.FC<OptionDoubleHorizontalNewProps> = ({
  label,
  secondaryLabel,
  selected,
  icon,
  className,
  onClick,
}) => (
  <li
    className={classNames(style.item, { [style.selected]: selected }, className)}
    role="presentation"
    onClick={onClick}
  >
    <span className={style.label}>
      {icon}
      {label}
    </span>
    <span className={style.secondaryLabel}>{secondaryLabel}</span>
  </li>
);

export default OptionDoubleHorizontalNew;
