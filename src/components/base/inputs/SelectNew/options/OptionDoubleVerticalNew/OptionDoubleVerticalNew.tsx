import { Checkbox } from 'Components/base/Checkbox';
import { Icon } from 'Components/base/Icon';
import { Radio } from 'Components/base/Radio';
import { Tooltip } from 'Components/base/Tooltip';
import classNames from 'classnames';
import React, { ReactNode, useEffect, useRef, useState } from 'react';

import style from './OptionDoubleVerticalNew.module.scss';

export interface OptionDoubleVerticalNewProps {
  label: string;
  secondaryLabel: string;
  selected: boolean;
  icon?: ReactNode;
  iconStaticColor?: boolean;
  type?: 'radio' | 'checkbox';
  className?: string;
  tooltip?: boolean;
  onClick(): void;
}

const OptionDoubleVerticalNew: React.FC<OptionDoubleVerticalNewProps> = ({
  label,
  secondaryLabel,
  selected,
  icon,
  iconStaticColor,
  type,
  className,
  tooltip,
  onClick,
}) => {
  const textPrimaryRef = useRef<HTMLDivElement>(null);
  const textSecondaryRef = useRef<HTMLDivElement>(null);
  const [tooltipValue, setTooltipValue] = useState<React.ReactNode>();
  const [secondaryTooltipValue, setSecondaryTooltipValue] = useState<React.ReactNode>('');

  useEffect(() => {
    if (
      textPrimaryRef.current &&
      textPrimaryRef.current.scrollWidth > textPrimaryRef.current.clientWidth
    ) {
      setTooltipValue(label);
    }
  }, [label]);

  useEffect(() => {
    if (
      textSecondaryRef.current &&
      textSecondaryRef.current.scrollWidth > textSecondaryRef.current.clientWidth
    ) {
      setSecondaryTooltipValue(secondaryLabel);
    }
  }, [secondaryLabel]);

  return (
    <li
      className={classNames(style.item, { [style.selected]: selected }, className)}
      role="presentation"
      onClick={onClick}
    >
      {icon && (
        <Icon
          className={style.icon}
          icon={icon}
          path="inherit"
          staticColor={iconStaticColor}
          transition="inherit"
        />
      )}
      {type === 'radio' && <Radio id={label} name={label} checked={selected} onChange={onClick} />}
      {type === 'checkbox' && <Checkbox name={label} checked={selected} onChange={onClick} />}

      {tooltip ? (
        <div className={style.text}>
          <Tooltip message={tooltipValue}>
            <span ref={textPrimaryRef} className={style.label}>
              {label}
            </span>
          </Tooltip>
          <Tooltip message={secondaryTooltipValue}>
            <span ref={textSecondaryRef} className={style.secondaryLabel}>
              {secondaryLabel}
            </span>
          </Tooltip>
        </div>
      ) : (
        <div className={style.text}>
          <span className={style.label}>{label}</span>
          <span className={style.secondaryLabel}>{secondaryLabel}</span>
        </div>
      )}
    </li>
  );
};

export default OptionDoubleVerticalNew;
