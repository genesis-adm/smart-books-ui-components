import { Meta, Story } from '@storybook/react';
import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { SelectNew } from 'Components/base/inputs/SelectNew';
import {
  OptionDoubleVerticalNew as Component,
  OptionDoubleVerticalNewProps as Props,
} from 'Components/base/inputs/SelectNew/options/OptionDoubleVerticalNew';
import { options } from 'Mocks/fakeOptions';
import { ReactComponent as MastercardIcon } from 'Svg/v2/24/logo-mastercard.svg';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Select/Options/Option Double Vertical New',
  component: Component,
  argTypes: {
    label: hideProperty,
    secondaryLabel: hideProperty,
    selected: hideProperty,
    icon: hideProperty,
    onClick: hideProperty,
    className: hideProperty,
  },
} as Meta;

export const OptionDoubleVerticalNew: Story<Props> = () => (
  <SelectNew
    onChange={() => {}}
    // defaultValue={{ label: '', value: '' }}
    label="Choose option"
    placeholder="Choose option"
    name="test"
  >
    {({ onClick, state, searchValue, selectElement }) => (
      <SelectDropdown selectElement={selectElement}>
        <SelectDropdownBody>
          <SelectDropdownItemList>
            {options
              .filter(({ label }) => label.toLowerCase().includes(searchValue.toLowerCase()))
              .map(({ label, secondaryLabel, value }) => (
                <Component
                  key={value}
                  label={label}
                  icon={<MastercardIcon />}
                  iconStaticColor
                  secondaryLabel={secondaryLabel}
                  selected={state?.value === value}
                  tooltip
                  onClick={() =>
                    onClick({
                      label,
                      value,
                    })
                  }
                />
              ))}
          </SelectDropdownItemList>
        </SelectDropdownBody>
      </SelectDropdown>
    )}
  </SelectNew>
);
