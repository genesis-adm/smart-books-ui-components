import { Icon } from 'Components/base/Icon';
import { Tooltip } from 'Components/base/Tooltip';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as BulletDotIcon } from 'Svg/v2/16/bullet-dot.svg';
import { ReactComponent as ChevronIcon } from 'Svg/v2/16/chevron-right.svg';
import classNames from 'classnames';
import React, { Children, MouseEvent, ReactElement, useEffect, useRef, useState } from 'react';

import style from './AccordionOption.module.scss';
import { AccordionOptionProps } from './AccordionOption.types';

// Reminder: do not forget to add the following code to root index.ts
// export { AccordionOption } from './components/base/inputs/SelectNew/options/optionsWithAccordion/AccordionOption';

const AccordionOption = ({
  name,
  icon,
  selected,
  tooltip,
  nestingLevel = 1,
  isActive,
  isOpen,
  className,
  children,
  onClick,
  onIconClick,
}: AccordionOptionProps): ReactElement => {
  const textRef = useRef<HTMLDivElement>(null);
  const countArray = !!Children.toArray(children).length;

  const [tooltipValue, setTooltipValue] = useState<string>('');

  useEffect(() => {
    if (
      !tooltip &&
      textRef.current &&
      textRef.current.scrollHeight > textRef.current.clientHeight
    ) {
      setTooltipValue(name);
    }
  }, [tooltip, name]);

  const handleIconClick = (e: MouseEvent<HTMLDivElement>) => {
    e.stopPropagation();
    onIconClick && onIconClick(e);
  };
  return (
    <div className={classNames(style.wrapper, className)}>
      <div
        className={classNames(style.item, {
          [style.active]: isActive,
          [style.selected]: selected,
        })}
        onClick={onClick}
        style={{ paddingLeft: `${nestingLevel * 12}px` }}
      >
        <IconButton
          onClick={handleIconClick}
          size="16"
          shape="circle"
          background="transparent-grey-20"
          color="inherit"
          className={classNames(style.icon, { [style.active]: isActive, [style.opened]: isOpen })}
          icon={<Icon path="inherit" icon={countArray ? <ChevronIcon /> : <BulletDotIcon />} />}
        />
        <div className={style.text_with_icon}>
          {icon && <Icon icon={icon} path="inherit" />}
          <Tooltip wrapperClassName={style.name} message={tooltipValue}>
            <Text
              ref={textRef}
              type="caption-regular"
              lineClamp="1"
              color="inherit"
              transition="unset"
            >
              {name}
            </Text>
          </Tooltip>
        </div>
      </div>
      {isOpen && children}
    </div>
  );
};
export default AccordionOption;
