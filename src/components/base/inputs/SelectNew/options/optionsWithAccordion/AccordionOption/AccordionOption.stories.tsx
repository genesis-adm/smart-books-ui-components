import { Meta, Story } from '@storybook/react';
import {
  AccordionOption as Component,
  AccordionOptionProps as Props,
} from 'Components/base/inputs/SelectNew/options/optionsWithAccordion/AccordionOption';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Select/Options/OptionsWithAccordion/AccordionOption',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const AccordionOptionComponent: Story<Props> = (args) => <Component {...args} />;

export const AccordionOption = AccordionOptionComponent.bind({});
AccordionOption.args = { name: 'Label 1' };
