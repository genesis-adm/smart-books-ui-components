import React, { ReactNode } from 'react';

export interface AccordionOptionProps {
  name: string;
  icon?: ReactNode;
  selected: boolean;
  tooltip?: string;
  nestingLevel?: number;
  isActive: boolean;
  isOpen: boolean;
  className?: string;
  children?: ReactNode;
  onClick?(e: React.MouseEvent<HTMLDivElement>): void;
  onIconClick?(e: React.MouseEvent<HTMLDivElement>): void;
}
