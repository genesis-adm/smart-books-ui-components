import { Checkbox } from 'Components/base/Checkbox';
import { Radio } from 'Components/base/Radio';
import classNames from 'classnames';
import React from 'react';

import styles from './OptionSingleNew.module.scss';

export interface OptionSingleNewProps {
  label: string;
  selected: boolean;
  icon?: React.ReactNode;
  input?: React.ReactNode;
  height?: '32' | '40';
  type?: 'default' | 'radio' | 'checkbox';
  disabled?: boolean;
  className?: string;
  style?: React.CSSProperties;

  onClick(): void;
}

const OptionSingleNew: React.FC<OptionSingleNewProps> = ({
  label,
  selected,
  icon,
  input,
  type = 'default',
  disabled,
  height = '40',
  className,
  onClick,
  style,
}) => {
  const handleClick = () => {
    if (!disabled) onClick();
  };

  return (
    <li
      className={classNames(
        styles.item,
        styles[`height_${height}`],
        {
          [styles.selected]: selected,
          [styles.disabled]: disabled,
        },
        className,
      )}
      role="presentation"
      onClick={handleClick}
      style={style}
    >
      <div className={styles.labelWrapper}>
        {type === 'radio' && (
          <Radio
            id={label}
            name={label}
            disabled={disabled}
            checked={selected}
            onChange={onClick}
          />
        )}
        {type === 'checkbox' && (
          <Checkbox name={label} disabled={disabled} checked={selected} onChange={onClick} />
        )}
        {!!icon && <div className={styles.iconWrapper}>{icon}</div>}
        {!!label && <span className={styles.label}>{label}</span>}
      </div>

      {!!input && <div className={styles.input}>{input}</div>}
    </li>
  );
};

export default OptionSingleNew;
