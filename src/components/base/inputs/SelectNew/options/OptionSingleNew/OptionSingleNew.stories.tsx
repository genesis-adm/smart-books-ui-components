import { Meta, Story } from '@storybook/react';
import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { SelectNew } from 'Components/base/inputs/SelectNew';
import {
  OptionSingleNew as Component,
  OptionSingleNewProps as Props,
} from 'Components/base/inputs/SelectNew/options/OptionSingleNew';
import { options } from 'Mocks/fakeOptions';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Select/Options/Option Single New',
  component: Component,
  argTypes: {
    label: hideProperty,
    selected: hideProperty,
    icon: hideProperty,
    onClick: hideProperty,
    className: hideProperty,
    input: hideProperty,
  },
} as Meta;

const OptionSingleNewComponent: Story<Props> = (args) => (
  <SelectNew onChange={() => {}} placeholder="Choose option" label="Choose option" name="test">
    {({ onClick, state, selectElement }) => (
      <SelectDropdown selectElement={selectElement}>
        <SelectDropdownBody>
          <SelectDropdownItemList>
            {options.map(({ label, value }, index) => (
              <Component
                {...args}
                key={value}
                label={label}
                selected={state?.value === value}
                disabled={index === 2}
                onClick={() =>
                  onClick({
                    label,
                    value,
                  })
                }
              />
            ))}
          </SelectDropdownItemList>
        </SelectDropdownBody>
      </SelectDropdown>
    )}
  </SelectNew>
);
export const OptionSingleNew = OptionSingleNewComponent.bind({});
OptionSingleNew.args = {
  type: 'default',
  disabled: false,
  height: '40',
};
