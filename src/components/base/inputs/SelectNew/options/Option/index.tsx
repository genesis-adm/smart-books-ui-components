import { Option } from 'Components/base/inputs/SelectNew/Select.types';
import useSelectOption from 'Components/base/inputs/SelectNew/hooks/useSelectOption';
import React from 'react';

export interface OptionProps {
  option: Option;
  classNames?: string;
  children?: React.ReactNode;
}

const Option: React.FC<OptionProps> = ({ option, children, classNames }) => {
  const { onClick } = useSelectOption({ option });

  return (
    <li className={classNames} role="presentation" onClick={onClick}>
      {children}
    </li>
  );
};

export default Option;
