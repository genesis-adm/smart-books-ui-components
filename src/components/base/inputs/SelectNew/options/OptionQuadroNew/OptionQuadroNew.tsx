import classNames from 'classnames';
import React from 'react';

import style from './OptionQuadroNew.module.scss';

export interface OptionQuadroNewProps {
  label: string;
  secondaryLabel: string;
  tertiaryLabel: string;
  quadroLabel: string;
  selected: boolean;
  icon?: React.ReactNode;
  className?: string;
  onClick(): void;
}

const OptionQuadroNew: React.FC<OptionQuadroNewProps> = ({
  label,
  secondaryLabel,
  tertiaryLabel,
  quadroLabel,
  selected,
  icon,
  className,
  onClick,
}) => (
  <li
    className={classNames(style.item, { [style.selected]: selected }, className)}
    role="presentation"
    onClick={onClick}
  >
    <div className={style.left}>
      <span className={style.label}>
        {icon}
        {label}
      </span>
      <span className={style.tertiaryLabel}>{tertiaryLabel}</span>
      <span className={style.quadroLabel}>{quadroLabel}</span>
    </div>
    <div className={style.right}>
      <span className={style.secondaryLabel}>{secondaryLabel}</span>
    </div>
  </li>
);

export default OptionQuadroNew;
