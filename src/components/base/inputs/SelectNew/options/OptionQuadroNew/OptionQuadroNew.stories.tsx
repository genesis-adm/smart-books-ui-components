import { Meta, Story } from '@storybook/react';
import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { SelectNew } from 'Components/base/inputs/SelectNew';
import {
  OptionQuadroNew as Component,
  OptionQuadroNewProps as Props,
} from 'Components/base/inputs/SelectNew/options/OptionQuadroNew';
import { options } from 'Mocks/fakeOptions';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Select/Options/Option Quadro New',
  component: Component,
  argTypes: {
    label: hideProperty,
    secondaryLabel: hideProperty,
    tertiaryLabel: hideProperty,
    quadroLabel: hideProperty,
    selected: hideProperty,
    icon: hideProperty,
    onClick: hideProperty,
    className: hideProperty,
  },
} as Meta;

export const OptionQuadroNew: Story<Props> = () => (
  <SelectNew
    onChange={() => {}}
    // defaultValue={{ label: '', value: '' }}
    label="Choose option"
    placeholder="Choose option"
    name="test"
  >
    {({ onClick, state, selectElement }) => (
      <SelectDropdown selectElement={selectElement}>
        <SelectDropdownBody>
          <SelectDropdownItemList>
            {options.map(({ label, secondaryLabel, tertiaryLabel, quadroLabel, value }) => (
              <Component
                key={value}
                label={label}
                secondaryLabel={secondaryLabel}
                tertiaryLabel={tertiaryLabel}
                quadroLabel={quadroLabel}
                selected={state?.value === value}
                onClick={() =>
                  onClick({
                    label,
                    value,
                  })
                }
              />
            ))}
          </SelectDropdownItemList>
        </SelectDropdownBody>
      </SelectDropdown>
    )}
  </SelectNew>
);
