import { Meta, Story } from '@storybook/react';
import { SelectNew } from 'Components/base/inputs/SelectNew';
import {
  OptionNoResults as Component,
  OptionNoResultsProps as Props,
} from 'Components/base/inputs/SelectNew/options/OptionNoResults';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Select/Options/Option No Results',
  component: Component,
  argTypes: {
    label: hideProperty,
    selected: hideProperty,
    icon: hideProperty,
    onClick: hideProperty,
    className: hideProperty,
  },
} as Meta;

export const OptionNoResults: Story<Props> = () => (
  <SelectNew
    onChange={() => {}}
    // defaultValue={{ label: '', value: '' }}
    placeholder="Choose option"
    label="Choose option"
    name="test"
  >
    {(controls) => <Component />}
  </SelectNew>
);
