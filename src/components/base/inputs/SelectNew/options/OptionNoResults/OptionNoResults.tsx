import { Text } from 'Components/base/typography/Text';
import { ReactComponent as NoResultsIcon } from 'Svg/v2/124/empty-table.svg';
import classNames from 'classnames';
import React from 'react';

import style from './OptionNoResults.module.scss';

export interface OptionNoResultsProps {
  className?: string;
}

const OptionNoResults: React.FC<OptionNoResultsProps> = ({ className }) => (
  <div className={classNames(style.wrapper, className)}>
    <NoResultsIcon />
    <Text type="body-regular-14" align="center" color="grey-100" className={style.label}>
      Nothing found for your request
    </Text>
  </div>
);

export default OptionNoResults;
