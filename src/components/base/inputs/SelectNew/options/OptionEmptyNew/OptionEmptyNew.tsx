import classNames from 'classnames';
import React from 'react';

import style from './OptionEmptyNew.module.scss';

export interface OptionEmptyNewProps {
  className?: string;
}

const OptionEmptyNew: React.FC<OptionEmptyNewProps> = ({ className }) => (
  <li className={classNames(style.item, className)} role="presentation" onClick={() => {}}>
    <span className={style.label}>No results</span>
  </li>
);

export default OptionEmptyNew;
