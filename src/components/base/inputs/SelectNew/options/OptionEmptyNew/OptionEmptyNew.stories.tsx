import { Meta, Story } from '@storybook/react';
import { SelectNew } from 'Components/base/inputs/SelectNew';
import {
  OptionEmptyNew as Component,
  OptionEmptyNewProps as Props,
} from 'Components/base/inputs/SelectNew/options/OptionEmptyNew';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Select/Options/Option Empty New',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

export const OptionEmptyNew: Story<Props> = () => (
  <SelectNew
    onChange={() => {}}
    // defaultValue={{ label: '', value: '' }}
    label="Choose option"
    placeholder="Choose option"
    name="test"
  >
    {() => <Component />}
  </SelectNew>
);
