import classNames from 'classnames';
import React from 'react';

import style from './OptionTripleNew.module.scss';

export interface OptionTripleNewProps {
  label: string;
  secondaryLabel: string;
  tertiaryLabel: string;
  selected: boolean;
  icon?: React.ReactNode;
  className?: string;
  onClick(): void;
}

const OptionTripleNew: React.FC<OptionTripleNewProps> = ({
  label,
  secondaryLabel,
  tertiaryLabel,
  selected,
  icon,
  className,
  onClick,
}) => (
  <li
    className={classNames(style.item, { [style.selected]: selected }, className)}
    role="presentation"
    onClick={onClick}
  >
    <div className={style.left}>
      <span className={style.label}>
        {icon}
        {label}
      </span>
      <span className={style.tertiaryLabel}>{tertiaryLabel}</span>
    </div>
    <div className={style.right}>
      <span className={style.secondaryLabel}>{secondaryLabel}</span>
    </div>
  </li>
);

export default OptionTripleNew;
