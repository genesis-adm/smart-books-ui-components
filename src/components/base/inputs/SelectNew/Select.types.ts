import React from 'react';

import {
  InputBackgroundType,
  InputBorderType,
  InputColorType,
  InputHeightType,
  InputWidthType,
} from '../../../../types/inputs';
import { TagType } from '../InputElements/InputTag';
import { TagBackground, TagWidth } from '../InputElements/InputTag/InputTag';

export interface SelectContextProps {
  isOpen: boolean;
  state: Option | null;

  onClick(arg: Option): void;

  setSelectActive(arg: boolean): void;

  setState(arg: Option): void;
}

export interface AccordionOption {
  text: string;
  icon?: React.ReactNode;
}

export interface Option {
  value: string;
  label: string;

  [key: string]: unknown;
}

export interface SelectProps {
  name: string;
  width?: InputWidthType;
  height?: InputHeightType;
  tooltip?: React.ReactNode | string;
  disabled?: boolean;
  readOnly?: boolean;
  required?: boolean;
  error?: boolean;
  errorMessage?: string;
  errorsList?: string[];
  placeholder?: string;
  label: string;
  tags?: TagType[];
  tagColor?: TagBackground;
  tagWidth?: TagWidth;
  background?: InputBackgroundType;
  color?: InputColorType;
  border?: InputBorderType;
  icon?: React.ReactNode;
  iconStaticColor?: boolean;
  description?: string | React.ReactElement;
  defaultValue?: Option;
  value?: Option;
  reset?: boolean;
  wrapTags?: boolean;
  accordionValue?: AccordionOption[];

  onChange?(option: Option): void;

  onAccordionChange?(option: AccordionOption[]): void;

  children(args: {
    isOpen: boolean;
    onClick(option: Option): void;
    onClickMultiple(option: Option): void;
    onClickAccordion(option: AccordionOption[]): void;
    state: Option | null;
    accordionState: AccordionOption[];
    searchValue: string;
    selectElement: Element;
  }): React.ReactElement;

  className?: string;
  isLoading?: boolean;
  onSearch?: (value: string | null) => void;
}
