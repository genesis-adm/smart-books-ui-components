import { AccordionOption, Option } from 'Components/base/inputs/SelectNew/Select.types';
import { useCallback, useState } from 'react';

export default function useSelect(defaultValue: Option) {
  const [state, setState] = useState<Option>(defaultValue);
  const [accordionState, setAccordionState] = useState<AccordionOption[]>([]);
  const [isOpen, setOpen] = useState(false);
  const [focus, setFocus] = useState(false);

  const setSelectActive = (arg: boolean) => {
    setOpen(arg);
    setFocus(arg);
  };

  const handleClick = useCallback((data: Option) => {
    setState(data);
    setSelectActive(false);
  }, []);

  const handleClickMultiple = useCallback((data: Option) => {
    setState(data);
  }, []);

  const handleClickAccordion = useCallback((data: AccordionOption[]) => {
    setAccordionState(data);
    setSelectActive(false);
  }, []);

  return {
    isOpen,
    focus,
    setSelectActive,
    state,
    accordionState,
    setState,
    setAccordionState,
    onClick: handleClick,
    onClickMultiple: handleClickMultiple,
    onClickAccordion: handleClickAccordion,
  };
}
