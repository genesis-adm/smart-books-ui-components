import { Icon } from 'Components/base/Icon';
import { InputNew } from 'Components/base/inputs/InputNew';
import { ReactComponent as EyeCrossedIcon } from 'Svg/v2/16/eye-crossed.svg';
import { ReactComponent as EyeOpenedIcon } from 'Svg/v2/16/eye.svg';
import { ReactComponent as LockClosedIcon } from 'Svg/v2/16/lock-closed.svg';
import React, { useState } from 'react';

import { InputWidthType } from '../../../../types/inputs';
import style from './InputPasswordNew.module.scss';

export interface InputPasswordNewProps {
  name: string;
  label?: string;
  width: InputWidthType;
  value: string;
  error: boolean;
  errorMessage?: string;
  className?: string;

  onChange(e: React.ChangeEvent<HTMLInputElement>): void;
}

const InputPasswordNew: React.FC<InputPasswordNewProps> = ({
  name,
  label = 'Password',
  width,
  value,
  error,
  errorMessage,
  className,
  onChange,
}) => {
  const [inputType, setInputType] = useState<'text' | 'password'>('password');
  const handlePasswordToggleClick = () => {
    inputType === 'text' ? setInputType('password') : setInputType('text');
  };
  return (
    <InputNew
      name={name}
      className={className}
      type={inputType}
      label={label}
      width={width}
      error={error}
      errorMessage={errorMessage}
      value={value}
      onChange={onChange}
      icon={<Icon icon={<LockClosedIcon />} clickThrough />}
      elementPosition="left"
      action={
        <Icon
          className={style.clear}
          icon={inputType === 'password' ? <EyeOpenedIcon /> : <EyeCrossedIcon />}
          path="inherit"
          cursor="pointer"
          onClick={handlePasswordToggleClick}
        />
      }
    />
  );
};

export default InputPasswordNew;
