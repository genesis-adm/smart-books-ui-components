import { Meta, Story } from '@storybook/react';
import {
  InputPasswordNew as Component,
  InputPasswordNewProps as Props,
} from 'Components/base/inputs/InputPasswordNew';
import React, { useState } from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Inputs/Input Password New',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const InputPasswordNewComponent: Story<Props> = (args) => {
  const [val, setVal] = useState('');
  return (
    <Component
      {...args}
      value={val}
      onChange={(e) => {
        setVal(e.target.value);
      }}
    />
  );
};

export const InputPasswordNew = InputPasswordNewComponent.bind({});
InputPasswordNew.args = {};
