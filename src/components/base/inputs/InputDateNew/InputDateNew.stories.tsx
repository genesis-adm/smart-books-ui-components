import { Meta, Story } from '@storybook/react';
import { InputDate as Component, InputDateProps as Props } from 'Components/base/inputs/InputDate';
import React, { useState } from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Inputs/InputDate',
  component: Component,
  argTypes: {
    name: hideProperty,
    selected: hideProperty,
    className: hideProperty,
    onChange: hideProperty,
    onFocus: hideProperty,
    onBlur: hideProperty,
  },
} as Meta;

const InputDateTemplate: Story<Props> = ({ selected, onChange, ...args }) => {
  const [startDate, setStartDate] = useState<Date | null>(null);
  return <Component selected={startDate} onChange={(date: Date) => setStartDate(date)} {...args} />;
};

export const InputDate = InputDateTemplate.bind({});
InputDate.args = {
  name: 'datepicker',
  isLoading: false,
  label: 'Label of a datepicker',
  required: true,
  width: '260',
  placeholder: 'Choose date',
  calendarStartDay: 1,
  minDate: 0,
  maxDate: 0,
  readOnly: false,
  disabled: false,
  tooltip: 'Tooltip of a datepicker',
};
