import { ContentLoader } from 'Components/base/ContentLoader';
import { Divider } from 'Components/base/Divider';
import { Icon } from 'Components/base/Icon';
import { Tooltip } from 'Components/base/Tooltip';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { InputDescription } from 'Components/base/inputs/InputElements/InputDescription';
import { ReactComponent as CalendarIcon } from 'Svg/v2/16/calendar.svg';
import classNames from 'classnames';
import { addDays, subDays } from 'date-fns';
import React, { useRef, useState } from 'react';
import DatePicker from 'react-datepicker';

import { InputWidthType } from '../../../../types/inputs';
import InputErrorPopup from '../InputErrorPopup/InputErrorPopup';
import style from './InputDateNew.module.scss';

export interface InputDateProps {
  selected: Date;
  calendarStartDay: 0 | 1 | 2 | 3 | 4 | 5 | 6;
  name: string;
  width: InputWidthType;
  height?: '36' | '48';
  label: string;
  description?: string;
  required?: boolean;
  popperDirection?: 'bottom' | 'top' | 'auto' | 'bottom-end';
  flexgrow?: '0' | '1';
  error?: boolean;
  errorMessage?: string;
  minDate?: number;
  maxDate?: number;
  dateFormat?: string;
  placeholder?: string;
  readOnly?: boolean;
  disabled?: boolean;
  tooltip?: React.ReactNode;
  className?: string;

  onChange(date: Date): void;

  onFocus?(e: React.FocusEvent<HTMLInputElement>): void;

  onBlur?(e: React.FocusEvent<HTMLInputElement>): void;

  isLoading?: boolean;
}

export interface CalendarContainerProps {
  className?: string;
  children: React.ReactNode;
}

const InputDate: React.FC<InputDateProps> = ({
  selected,
  calendarStartDay,
  name,
  width,
  height = '48',
  label,
  description,
  required,
  popperDirection = 'auto',
  flexgrow,
  error,
  errorMessage,
  minDate,
  maxDate,
  dateFormat,
  placeholder,
  readOnly,
  disabled,
  tooltip,
  className,
  onChange,
  onFocus,
  onBlur,
  isLoading,
}) => {
  const wrapperRef = useRef<HTMLDivElement>(null);
  const previousValue = selected;
  const [currentValue, setCurrentValue] = useState(previousValue);
  const [openCalendar, setOpenCalendar] = useState(false);
  const [focused, setFocused] = useState(false);
  const [hovered, setHovered] = useState(false);
  const [hideTooltip, setHideTooltip] = useState(false);

  const handleMouseEnter = () => {
    setHovered(true);
    !focused && setHideTooltip(false);
  };
  const handleMouseLeave = () => {
    setHovered(false);
  };
  const handleFocus = (e: React.FocusEvent<HTMLInputElement>) => {
    onFocus?.(e);
    onChange(currentValue);
    setFocused(true);
    setHideTooltip(true);
    setOpenCalendar(true);
  };
  const handleBlur = (e: React.FocusEvent<HTMLInputElement>) => {
    onBlur?.(e);
    onChange(previousValue);
    setOpenCalendar(false);
    setFocused(false);
    setHovered(false);
  };
  const handleChange = (date: Date) => {
    onChange(date);
  };
  const handleApply = (date: Date) => {
    if (!(date instanceof Date && !isNaN(date.getTime()))) {
      setFocused(true);
    } else {
      setFocused(false);
      setHovered(false);
    }
    setHideTooltip(true);
    onChange(selected);
    setOpenCalendar(false);
    setCurrentValue(selected);
  };
  const tooltipMessage = disabled ? 'Disabled' : hideTooltip ? '' : tooltip;
  const CalendarMainContainer = ({ className, children }: CalendarContainerProps) => {
    return (
      <Container
        className={classNames(style.calendar_container, className)}
        background="white-100"
        flexdirection="column"
      >
        {children}
        <Divider fullHorizontalWidth />
        <Container className={style.button} justifycontent="flex-end" alignitems="center">
          <Button
            width="auto"
            height="32"
            onClick={() => handleApply(selected)}
            background="violet-90-violet-100"
            color="white-100"
          >
            Apply
          </Button>
        </Container>
      </Container>
    );
  };
  return (
    <div
      ref={wrapperRef}
      className={classNames(
        style.wrapper,
        style[`width_${width}`],
        style[`flexgrow_${flexgrow}`],
        style[`height_${height}`],
        className,
      )}
    >
      {height === '48' && label && (
        <label
          className={classNames(style.label, {
            [style.required]: required,
            [style.hovered]: hovered,
            [style.focused]: focused,
            [style.notEmpty]: !!selected,
            [style.error]: error,
            [style.disabled]: disabled || readOnly,
          })}
          htmlFor={name}
        >
          {label}
        </label>
      )}
      <Tooltip
        message={tooltipMessage}
        wrapperClassName={style.input}
        onMouseEnter={handleMouseEnter}
        onMouseLeave={handleMouseLeave}
      >
        <DatePicker
          id={name}
          name={name}
          selected={selected}
          onChange={handleChange}
          className={classNames(
            'react-datepicker__input',
            `react-datepicker__input_height-${height}`,
            {
              'react-datepicker__input-error': error,
              'react-datepicker__input-disabled': disabled,
              'react-datepicker__input-readonly': readOnly,
            },
            className,
          )}
          onClickOutside={() => {
            onChange(previousValue);
            setOpenCalendar(false);
          }}
          open={openCalendar}
          portalId="tooltip"
          calendarClassName="react-datepicker_inline"
          popperClassName={classNames(`react-datepicker-popper__width-${width}`)}
          calendarStartDay={calendarStartDay}
          minDate={minDate !== undefined ? subDays(new Date(), minDate) : undefined}
          maxDate={maxDate !== undefined ? addDays(new Date(), maxDate) : undefined}
          popperPlacement={popperDirection}
          showMonthDropdown
          showYearDropdown
          dateFormat={dateFormat}
          useShortMonthInDropdown
          yearDropdownItemNumber={4}
          disabledKeyboardNavigation
          showPopperArrow={false}
          readOnly={readOnly}
          disabled={disabled}
          placeholderText={placeholder}
          onFocus={handleFocus}
          onBlur={handleBlur}
          calendarContainer={CalendarMainContainer}
          value={currentValue.toLocaleDateString('en-US').toString()}
        />
        <Icon
          className={classNames(style.icon, { [style.error]: error })}
          icon={<CalendarIcon />}
          path="inherit"
          clickThrough
        />
      </Tooltip>

      {!!errorMessage && hovered && !focused && wrapperRef.current && (
        <InputErrorPopup inputWrapperElement={wrapperRef.current} errorMessage={errorMessage} />
      )}

      {description && <InputDescription description={description} width={width} />}
      {isLoading && (
        <ContentLoader height="100%" isLoading={isLoading ?? false} isInsideComponent />
      )}
    </div>
  );
};

export default InputDate;
