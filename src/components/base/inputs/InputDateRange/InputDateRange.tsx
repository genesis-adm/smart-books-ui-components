import { Icon } from 'Components/base/Icon';
import { Tooltip } from 'Components/base/Tooltip';
import { InputDescription } from 'Components/base/inputs/InputElements/InputDescription';
import { ReactComponent as CalendarIcon } from 'Svg/v2/16/calendar.svg';
import classNames from 'classnames';
import { addDays, subDays } from 'date-fns';
import React, { useState } from 'react';
import DatePicker from 'react-datepicker';

import { InputWidthType } from '../../../../types/inputs';
import style from './InputDateRange.module.scss';

export interface InputDateRangeProps {
  startDate: Date | null;
  endDate: Date | null;
  calendarStartDay: 0 | 1 | 2 | 3 | 4 | 5 | 6;
  name: string;
  width: InputWidthType;
  height?: '36' | '48';
  label: string;
  description?: string;
  required?: boolean;
  popperDirection?: 'bottom' | 'top' | 'auto';
  flexgrow?: '0' | '1';
  error?: boolean;
  minDate?: number;
  maxDate?: number;
  dateFormat?: string;
  placeholder?: string;
  readOnly?: boolean;
  disabled?: boolean;
  tooltip?: React.ReactNode;
  className?: string;

  onChange(dates: [Date | null, Date | null]): void;

  onFocus?(e: React.FocusEvent<HTMLInputElement>): void;

  onBlur?(e: React.FocusEvent<HTMLInputElement>): void;
}

const InputDateRange: React.FC<InputDateRangeProps> = ({
  startDate,
  endDate,
  calendarStartDay,
  name,
  width,
  height = '48',
  label,
  description,
  required,
  popperDirection = 'auto',
  flexgrow,
  error,
  minDate,
  maxDate,
  dateFormat,
  placeholder,
  readOnly,
  disabled,
  tooltip,
  className,
  onChange,
  onFocus,
  onBlur,
}) => {
  const [focused, setFocused] = useState(false);
  const [hovered, setHovered] = useState(false);
  const [hideTooltip, setHideTooltip] = useState(false);
  const handleMouseEnter = () => {
    setHovered(true);
    !focused && setHideTooltip(false);
  };
  const handleMouseLeave = () => {
    setHovered(false);
  };
  const handleFocus = (e: React.FocusEvent<HTMLInputElement>) => {
    onFocus?.(e);
    setFocused(true);
    setHideTooltip(true);
  };
  const handleBlur = (e: React.FocusEvent<HTMLInputElement>) => {
    onBlur?.(e);
    setFocused(false);
    setHovered(false);
  };
  const handleChange = (dates: [Date | null, Date | null]) => {
    if (!(dates instanceof Date && !isNaN(dates.getTime()))) {
      setFocused(true);
    } else {
      setFocused(false);
      setHovered(false);
    }
    setHideTooltip(true);
    onChange(dates);
  };
  const tooltipMessage = disabled ? 'Disabled' : hideTooltip ? '' : tooltip;
  return (
    <div
      className={classNames(
        style.wrapper,
        style[`width_${width}`],
        style[`flexgrow_${flexgrow}`],
        style[`height_${height}`],
        className,
      )}
    >
      {height === '48' && label && (
        <label
          className={classNames(style.label, {
            [style.required]: required,
            [style.hovered]: hovered,
            [style.focused]: focused,
            [style.notEmpty]: !!startDate && !!endDate,
            [style.error]: error,
            [style.disabled]: disabled || readOnly,
          })}
          htmlFor={name}
        >
          {label}
        </label>
      )}
      <Tooltip
        message={tooltipMessage}
        wrapperClassName={style.input}
        onMouseEnter={handleMouseEnter}
        onMouseLeave={handleMouseLeave}
      >
        <DatePicker
          id={name}
          name={name}
          startDate={startDate}
          endDate={endDate}
          onChange={handleChange}
          className={classNames(
            'react-datepicker__input',
            `react-datepicker__input_height-${height}`,
            {
              'react-datepicker__input-error': error,
              'react-datepicker__input-disabled': disabled,
              'react-datepicker__input-readonly': readOnly,
            },
            className,
          )}
          calendarClassName="react-datepicker_dropdown"
          popperClassName={classNames(`react-datepicker-popper__width-${width}`)}
          calendarStartDay={calendarStartDay}
          minDate={minDate ? subDays(new Date(), minDate) : null}
          maxDate={maxDate ? addDays(new Date(), maxDate) : null}
          popperPlacement={popperDirection}
          showMonthDropdown
          showYearDropdown
          dateFormat={dateFormat}
          useShortMonthInDropdown
          yearDropdownItemNumber={4}
          disabledKeyboardNavigation
          selectsRange
          showPopperArrow={false}
          readOnly={readOnly}
          disabled={disabled}
          placeholderText={placeholder}
          onFocus={handleFocus}
          onBlur={handleBlur}
        />
        <Icon className={style.icon} icon={<CalendarIcon />} path="inherit" clickThrough />
      </Tooltip>
      {description && <InputDescription description={description} width={width} />}
    </div>
  );
};

export default InputDateRange;
