import { Meta, Story } from '@storybook/react';
import {
  InputDateRange as Component,
  InputDateRangeProps as Props,
} from 'Components/base/inputs/InputDateRange';
import React, { useState } from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Inputs/InputDateRange',
  component: Component,
  argTypes: {
    name: hideProperty,
    selected: hideProperty,
    className: hideProperty,
    onChange: hideProperty,
    onFocus: hideProperty,
    onBlur: hideProperty,
  },
} as Meta;

const InputDateRangeTemplate: Story<Props> = (args) => {
  const [startDay, setStartDay] = useState<Date | null>(null);
  const [endDay, setEndDay] = useState<Date | null>(null);
  const changeHandler = (dates: [Date, null]) => {
    const [start, end] = dates;
    setStartDay(start);
    setEndDay(end);
  };
  return <Component {...args} startDate={startDay} endDate={endDay} onChange={changeHandler} />;
};

export const InputDateRange = InputDateRangeTemplate.bind({});
InputDateRange.args = {
  name: 'datepicker',
  label: 'Label of a datepicker',
  description: 'Description of a datepicker',
  required: true,
  width: '260',
  placeholder: 'Choose date',
  calendarStartDay: 1,
  minDate: 0,
  maxDate: 0,
  error: false,
  readOnly: false,
  disabled: false,
  tooltip: 'Tooltip of a datepicker',
};
