import { InputHeightType } from '../../../../types/inputs';

export const MAX_NUMERIC_VALUE = 999999999999.99;

export const defaultInputPaddingRightMapper: Record<InputHeightType, number> = {
  '24': 8,
  '28': 8,
  '36': 8,
  '48': 10,
  full: 10,
};
