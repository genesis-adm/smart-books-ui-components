import React, { CSSProperties, ReactElement, ReactNode, useEffect, useRef } from 'react';

import useResizeObserver from '../../../../../hooks/useResizeObserver';

type PlaceholderProps = {
  style?: CSSProperties;
  className?: string;

  onResize?: (size: { width: number; height: number }) => void;

  children?: ReactNode;
};

const InputPlaceholder = ({
  children,
  style,
  className,
  onResize,
}: PlaceholderProps): ReactElement => {
  const placeholderRef = useRef<HTMLSpanElement>(null);
  const [setPlaceholderObserverEntry, placeholderObserverEntry] = useResizeObserver();

  useEffect(() => {
    setPlaceholderObserverEntry(placeholderRef.current);
  }, [setPlaceholderObserverEntry]);

  useEffect(() => {
    if (placeholderObserverEntry.target) {
      onResize?.({
        width: placeholderObserverEntry.target.clientWidth,
        height: placeholderObserverEntry.target.clientHeight,
      });
    }
  }, [onResize, placeholderObserverEntry.target]);

  return (
    <span ref={placeholderRef} style={style} className={className}>
      {children}
    </span>
  );
};

export default InputPlaceholder;
