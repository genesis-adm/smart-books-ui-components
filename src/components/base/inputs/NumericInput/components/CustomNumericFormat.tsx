import { NumericInputLimit } from 'Components/base/inputs/NumericInput/NumericInput.types';
import { roundNumber } from 'Utils/numberUtils';
import React, { ReactElement } from 'react';
import { NumberFormatBase, SourceInfo, useNumericFormat } from 'react-number-format';
import { NumberFormatValues, NumericFormatProps } from 'react-number-format/types/types';

const CustomNumericFormat = (
  props: NumericFormatProps & {
    limit?: NumericInputLimit;
  },
): ReactElement => {
  const { limit, ...rest } = props;
  const { removeFormatting, onValueChange, ..._props } = useNumericFormat(rest);

  const _onValueChange = (values: NumberFormatValues, sourceInfo: SourceInfo) => {
    if (props.value && roundNumber(+props.value, props.decimalScale) === values.floatValue) return;

    onValueChange?.(
      {
        ...values,
        floatValue: values?.floatValue || 0,
      },
      sourceInfo,
    );
  };

  const isNegativeOnly = limit === 'negativeOnly' || limit === 'negativeOnlyColored';

  const _removeFormatting = (inputValue: string): string => {
    const convertedInputValue = isNegativeOnly ? addNegativeSign(inputValue) : inputValue;

    const _inputValue = removeFormatting?.(convertedInputValue) || '';

    return checkIsValue(_inputValue);
  };

  return (
    <NumberFormatBase
      {..._props}
      removeFormatting={_removeFormatting}
      onValueChange={_onValueChange}
    />
  );
};

const addNegativeSign = (inputValue: string): string => {
  if (!inputValue || inputValue[0] === '-') return inputValue;

  return inputValue.replaceAll(inputValue, `-${inputValue}`);
};

const checkIsValue = (valueWithoutFormatting: string): string => {
  const separatorPosition =
    valueWithoutFormatting.indexOf('.') >= 0 ? valueWithoutFormatting.indexOf('.') : undefined;

  const isValue =
    valueWithoutFormatting.slice(0, separatorPosition) &&
    !isNaN(Number(valueWithoutFormatting.slice(0, separatorPosition)));

  return isValue ? valueWithoutFormatting : '';
};

export default CustomNumericFormat;
