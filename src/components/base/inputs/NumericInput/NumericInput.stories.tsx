import { Meta, Story } from '@storybook/react';
import { OwnNumberFormatValues } from 'Components/base/inputs/NumericInput/NumericInput.types';
import {
  NumericInput as Component,
  NumericInputProps as Props,
} from 'Components/base/inputs/NumericInput/index';
import React, { useState } from 'react';
import { NumberFormatValues } from 'react-number-format/types/types';

const hideProperty = {
  table: {
    disable: true,
  },
};

const meta: Meta<typeof Component> = {
  title: 'Components/Base/Inputs/Numeric Input',
  component: Component,
  argTypes: {
    className: hideProperty,
    max: {
      type: 'number',
      defaultValue: 0,
    },
    width: {
      type: 'enum',
      control: 'select',
      options: ['44', '80', '114', '120', '134', '172', '260', '300', 'full'],
    },
    fixedDecimalScale: hideProperty,
    allowedDecimalSeparators: hideProperty,
    isAllowed: hideProperty,
    onValueChange: hideProperty,
    defaultValue: hideProperty,
    onBlur: hideProperty,
    onFocus: hideProperty,
    onChange: hideProperty,
    value: hideProperty,
    error: hideProperty,
    thousandSeparator: {
      type: 'string',
      defaultValue: ',',
    },
    decimalSeparator: {
      type: 'string',
      defaultValue: '.',
    },
  },
};

export default meta;

const NumericInputComponent: Story<Props & { max: number; isError: boolean }> = (args) => {
  const [value, setValue] = useState<number | null | undefined>(0);
  const error = args?.isError ? { messages: ['Test error!!!'] } : undefined;

  const handleChangeValue = (values: OwnNumberFormatValues): void => {
    setValue(values.floatValue);
  };

  const handleIsAllowed = args.max
    ? (values: NumberFormatValues) => (values.floatValue ?? 0) <= args.max
    : undefined;

  return (
    <Component
      {...args}
      value={value}
      onValueChange={handleChangeValue}
      isAllowed={handleIsAllowed}
      error={error}
    />
  );
};

export const NumericInput = NumericInputComponent.bind({});
NumericInput.args = {
  disabled: false,
  readOnly: false,
  isError: false,
  isLoading: false,
  background: 'grey-10',
  width: '172',
  height: '36',
  label: 'Label amount',
  required: true,
  decimalScale: 2,
};
