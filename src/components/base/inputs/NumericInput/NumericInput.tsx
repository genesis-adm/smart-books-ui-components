import { ContentLoader } from 'Components/base/ContentLoader';
import InputErrorPopup from 'Components/base/inputs/InputErrorPopup/InputErrorPopup';
import { InputWrapper } from 'Components/base/inputs/InputWrapper';
import CustomNumericFormat from 'Components/base/inputs/NumericInput/components/CustomNumericFormat';
import InputPlaceholder from 'Components/base/inputs/NumericInput/components/InputPlaceholder';
import {
  MAX_NUMERIC_VALUE,
  defaultInputPaddingRightMapper,
} from 'Components/base/inputs/NumericInput/constants';
import useInputEventsHandler from 'Components/base/inputs/NumericInput/hooks/useInputEventsHandler';
import useNumericInputPlaceholder from 'Components/base/inputs/NumericInput/hooks/useNumericInputPlaceholder';
import classNames from 'classnames';
import React, {
  Dispatch,
  ReactElement,
  SetStateAction,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import { NumberFormatValues } from 'react-number-format/types/types';

import styles from './NumericInput.module.scss';
import { NumericInputProps, OwnNumericInputValue } from './NumericInput.types';

const NumericInput = (props: NumericInputProps): ReactElement => {
  const {
    className,
    label,
    width,
    height = '48',
    required,
    prefix = '',
    suffix,
    isLoading = false,
    isLoadingSubValue = false,
    value,
    subValue,
    defaultValue,
    background,
    onChange,
    onClick,
    onValueChange,
    onFocus,
    onBlur,
    onKeyDown,
    isAllowed,
    disabled,
    error,
    limit,
    decimalScale = 2,
    allowedDecimalSeparators = [','],
    thousandSeparator = ',',
    decimalSeparator = '.',
    fixedDecimalScale = true,
    readOnly,
  } = props;

  const [currentValue, setCurrentValue] = useNumericStringValue(value);

  const inputRef = useRef<HTMLInputElement>(null);

  const [isInputHover, setInputHover] = useState(false);
  const [isInputFocus, setInputFocus] = useState(false);

  const isPositiveOnly = limit === 'positiveOnly' || limit === 'positiveOnlyColored';
  const isNegativeOnly = limit === 'negativeOnly' || limit === 'negativeOnlyColored';
  const isColored = limit === 'negativeOnlyColored' || limit === 'positiveOnlyColored';
  const defaultInputPaddingRight = defaultInputPaddingRightMapper[height];

  const isError = !!error?.messages?.filter((message) => !!message)?.length;

  const handleClickWrapper = (): void => {
    if (isInputFocus) return;

    inputRef?.current?.focus();
    setInputFocus(true);
  };

  const handleHoverWrapper = (value: boolean): void => {
    setInputHover(value);
  };

  const handleBlurWrapper = (): void => {
    inputRef?.current?.blur();
    setInputFocus(false);
  };

  const handleIsAllow = (values: NumberFormatValues): boolean =>
    Math.abs(values.floatValue ?? 0) <= MAX_NUMERIC_VALUE;

  const inputData = {
    value: currentValue,
    limit,
    prefix: limit === 'positiveOnlyColored' ? `+${prefix}` : prefix,
    allowNegative: !isPositiveOnly,
    decimalScale,
    thousandSeparator,
    decimalSeparator,
    allowedDecimalSeparators,
    fixedDecimalScale,
    disabled,
    isNegativeOnly,
    isAllowed: (values: NumberFormatValues) =>
      isAllowed ? isAllowed(values) : handleIsAllow(values),
  };

  const placeholder = useNumericInputPlaceholder(inputData);

  useInputEventsHandler({
    inputRef,
    inputData,
  });

  const isValue = !!currentValue || (!!defaultValue && !currentValue);

  const isLabelExpanded = isInputFocus || isValue;

  const inputStyle = {
    paddingRight: !isValue ? decimalScale * defaultInputPaddingRight : undefined,
  };

  useEffect(() => {
    if (disabled) setInputFocus(!disabled);
  }, [disabled]);

  return (
    <InputWrapper
      width={width}
      height={height}
      isFocused={isInputFocus}
      background={background}
      disabled={disabled}
      readOnly={readOnly}
      isError={isError}
      onClick={handleClickWrapper}
      onBlur={handleBlurWrapper}
      onHover={handleHoverWrapper}
      className={classNames(className, {
        [styles.subValueWrapper]: subValue,
      })}
    >
      {({ inputWrapperElement }) => (
        <>
          {height === '48' && (
            <span
              className={classNames(styles.label, {
                [styles.expand]: isLabelExpanded,
                [styles.focus]: isInputFocus,
                [styles.hover]: isInputHover,
                [styles.disabled]: disabled,
                [styles.readOnly]: readOnly,
                [styles.error]: isError,
              })}
            >
              {label}
              {required && <span className={styles.required}>*</span>}
            </span>
          )}

          <div
            className={classNames(styles.numericInputWrapper, {
              [styles.hidden]: !isLabelExpanded && height === '48',
              [styles[`numericInputWrapper_${height}`]]: height === '48',
            })}
          >
            <div className={styles.inputWrapper}>
              <CustomNumericFormat
                {...inputData}
                limit={limit}
                style={inputStyle}
                valueIsNumericString
                getInputRef={inputRef}
                onChange={onChange}
                onClick={onClick}
                onValueChange={(values, sourceInfo) => {
                  setCurrentValue(values.value);
                  onValueChange?.(
                    {
                      ...values,
                      floatValue: values?.floatValue || 0,
                    },
                    sourceInfo,
                  );
                }}
                onFocus={onFocus}
                onBlur={onBlur}
                onKeyDown={onKeyDown}
                readOnly={readOnly}
                className={classNames(styles.input, {
                  [styles[`input_${limit}`]]: isColored,
                  [styles[`input_${height}`]]: !!height,
                  [styles.disabled]: disabled,
                  [styles.readOnly]: readOnly,
                  [styles.error]: isError,
                })}
              />
              {!isValue && (
                <InputPlaceholder
                  className={classNames(styles.placeholder, {
                    [styles[`placeholder_${height}`]]: !!height,
                    [styles.hover]: isInputHover && (height === '36' || height === '28'),
                    [styles.focus]: isInputFocus && (height === '36' || height === '28'),
                    [styles.error]: isError,
                  })}
                >
                  {placeholder}
                </InputPlaceholder>
              )}
            </div>

            {!!suffix && (
              <span
                className={classNames(styles.suffix, {
                  [styles[`suffix_${height}`]]: !!height,
                  [styles.hover]: isInputHover && (height === '36' || height === '28'),
                  [styles.focus]: isInputFocus && (height === '36' || height === '28'),
                  [styles.error]: isError,
                })}
              >
                {suffix}
              </span>
            )}
          </div>

          {isLoading && <ContentLoader height="100%" isLoading={isLoading} isInsideComponent />}

          {isError && isInputHover && !isInputFocus && inputWrapperElement && (
            <InputErrorPopup
              inputWrapperElement={inputWrapperElement}
              errorMessage={error?.messages}
            />
          )}
          {!!subValue && height === '36' && (
            <span className={styles.subValue}>
              <ContentLoader
                className={styles.loader}
                height="11px"
                isLoading={isLoadingSubValue}
                isInsideComponent
              >
                {subValue}
              </ContentLoader>
            </span>
          )}
        </>
      )}
    </InputWrapper>
  );
};

export default NumericInput;

const useNumericStringValue = <S extends OwnNumericInputValue>(
  initialState?: S,
): [string | undefined, Dispatch<SetStateAction<string>>] => {
  const [stringInputValue, setStringInputValue] = useState<string>('');
  const currentValue =
    Number(initialState) === Number(stringInputValue) ? stringInputValue : undefined;

  useEffect(() => {
    setStringInputValue(initialState ? String(initialState) : '');
  }, [initialState]);

  return useMemo(() => [currentValue, setStringInputValue], [currentValue]);
};
