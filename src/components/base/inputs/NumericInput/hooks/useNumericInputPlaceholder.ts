import { NumericInputLimit } from 'Components/base/inputs/NumericInput/NumericInput.types';

const useNumericInputPlaceholder = (props: {
  decimalScale: number;
  fixedDecimalScale: boolean;
  prefix: string;
  decimalSeparator: string;
  limit?: NumericInputLimit;
}): string => {
  const { decimalScale, fixedDecimalScale, prefix, decimalSeparator, limit } = props;
  const addSign = (value: string, limit?: NumericInputLimit): string => {
    const isNegative = limit === 'negativeOnly' || limit === 'negativeOnlyColored';

    if (isNegative) return `-${value}`;

    return value;
  };

  const customPlaceholder =
    !decimalScale || !fixedDecimalScale
      ? `${prefix}0`
      : `${prefix}0${decimalSeparator}${Array.from(Array(decimalScale)).reduce(
          (acc) => '0' + acc,
          '',
        )}`;

  return addSign(customPlaceholder, limit);
};

export default useNumericInputPlaceholder;
