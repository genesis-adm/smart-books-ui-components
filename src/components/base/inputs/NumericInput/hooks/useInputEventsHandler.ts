import { RefObject } from 'react';

import useEventListener from '../../../../../hooks/useEventListener';

const useInputEventsHandler = ({
  inputRef,
  inputData: { decimalScale, isNegativeOnly },
}: {
  inputRef: RefObject<HTMLInputElement>;
  inputData: { decimalScale: number; isNegativeOnly: boolean };
}): void => {
  const input = inputRef.current;

  const handleInputCommaClick = (event: KeyboardEvent) => {
    const valueLength = input?.value?.length ?? 0;
    const caretPosition = {
      current: input?.selectionEnd,
      target: valueLength - decimalScale,
    };

    if (event.key === ',' && caretPosition.target - 1 === caretPosition.current) {
      input?.setSelectionRange(caretPosition.target, caretPosition.target);
    }
  };
  useEventListener('keydown', handleInputCommaClick, inputRef);

  const handleInputMinusClick = (event: KeyboardEvent) => {
    if (event.key === '-' && isNegativeOnly) {
      event.preventDefault();
    }
  };
  useEventListener('keydown', handleInputMinusClick, inputRef);
};

export default useInputEventsHandler;
