export { default as NumericInput } from './NumericInput';

export type { NumericInputProps } from './NumericInput.types';
