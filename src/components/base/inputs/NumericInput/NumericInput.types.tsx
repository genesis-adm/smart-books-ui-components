import {
  NumberFormatValues,
  NumericFormatProps,
  SourceInfo,
} from 'react-number-format/types/types';

import { InputBaseProps, InputHeightType, InputWidthType } from '../../../../types/inputs';

export type NumericInputLimit =
  | 'positiveOnly'
  | 'negativeOnly'
  | 'positiveOnlyColored'
  | 'negativeOnlyColored';

export type OwnNumericInputValue = number | null;

export type OwnNumberFormatValues = Omit<NumberFormatValues, 'floatValue'> & {
  floatValue: number;
};

export type OwnOnValueChange = (values: OwnNumberFormatValues, sourceInfo: SourceInfo) => void;

export type NumericInputProps = {
  width?: InputWidthType;
  height?: InputHeightType;

  onValueChange?: OwnOnValueChange;

  value?: OwnNumericInputValue;
  subValue?: string;
  defaultValue?: OwnNumericInputValue;

  isLoading?: boolean;
  isLoadingSubValue?: boolean;
  limit?: NumericInputLimit;
} & Omit<InputBaseProps, 'placeholder'> &
  Pick<
    NumericFormatProps,
    | 'onClick'
    | 'onChange'
    | 'onFocus'
    | 'onBlur'
    | 'onKeyDown'
    | 'disabled'
    | 'allowedDecimalSeparators'
    | 'thousandSeparator'
    | 'fixedDecimalScale'
    | 'decimalSeparator'
    | 'decimalScale'
    | 'prefix'
    | 'suffix'
    | 'isAllowed'
    | 'readOnly'
  >;
