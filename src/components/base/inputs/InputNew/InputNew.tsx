import { ContentLoader } from 'Components/base/ContentLoader';
import { Icon } from 'Components/base/Icon';
import { Tooltip } from 'Components/base/Tooltip';
import { InputDescription } from 'Components/base/inputs/InputElements/InputDescription';
import { InputErrorList } from 'Components/base/inputs/InputElements/InputErrorList';
import { InputTagWrapper } from 'Components/base/inputs/InputElements/InputTag';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as ArrowRightIcon } from 'Svg/v2/16/arrow-right.svg';
import classNames from 'classnames';
import React, { useRef, useState } from 'react';

import InputErrorPopup from '../InputErrorPopup/InputErrorPopup';
import style from './InputNew.module.scss';
import { InputNewProps } from './InputNew.types';

const Input = React.forwardRef<HTMLInputElement, InputNewProps>(
  (
    {
      type = 'text',
      name,
      label,
      secondaryLabel,
      accordionArr,
      placeholder,
      // placeholder = '',
      width,
      height = '48',
      disabled = false,
      error,
      errorMessage,
      errorsList,
      action,
      tooltip,
      disabledTooltip = 'Disabled',
      tooltipWidth,
      description,
      align,
      // suggestionActive,
      // flexgrow = '1',
      tags,
      tagColor,
      tagWidth,
      wrapTags,
      icon,
      iconStaticColor,
      symbol,
      symbolColor = 'black-100',
      elementPosition,
      background = 'grey-10',
      color = 'default',
      border = 'default',
      required,
      maxLength = 40,
      active,
      value,
      onClick,
      onChange,
      onBlur,
      onPaste,
      onKeyDown,
      onFocus,
      readOnly,
      // children,
      className,
      isLoading,
    },
    ref,
  ) => {
    const wrapperRef = useRef<HTMLDivElement>(null);

    const [focused, setFocused] = useState(false);
    const [hovered, setHovered] = useState(false);

    const inputActive = focused || active;

    const notEmpty =
      (tags && tags.length > 0) || (accordionArr && accordionArr.length > 0) || !!value;
    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
      onChange?.(e);
    };
    const handleFocus = (e: React.FocusEvent<HTMLInputElement>) => {
      onFocus?.(e);
      setFocused(true);
    };
    const handleBlur = (e: React.FocusEvent<HTMLInputElement>) => {
      onBlur?.(e);
      setFocused(false);
    };
    let tooltipMessage;

    if (disabled) {
      tooltipMessage = disabledTooltip;
    } else if (inputActive || !!tags) {
      tooltipMessage = '';
    } else {
      tooltipMessage = tooltip;
    }

    const renderTypeOfInput = () => {
      if (tags?.length) {
        return (
          <InputTagWrapper tags={tags} tagColor={tagColor} tagWidth={tagWidth} wrap={wrapTags} />
        );
      } else if (accordionArr?.length) {
        return (
          <div className={style.input}>
            <div className={style.accordion_entries}>
              {accordionArr.map(({ text, icon }, index) => (
                <>
                  {icon && <Icon icon={icon} />}
                  <Text
                    type="body-regular-14"
                    color={index !== accordionArr.length - 1 ? 'grey-100' : 'black-100'}
                  >
                    {text}
                  </Text>
                  {index !== accordionArr.length - 1 && (
                    <Icon icon={<ArrowRightIcon transform="scale(0.7)" />} path="grey-100" />
                  )}
                </>
              ))}
            </div>
          </div>
        );
      } else {
        return (
          <input
            type={type}
            id={name}
            maxLength={maxLength}
            autoComplete="off"
            placeholder={
              (height === '28' && required) || (height === '36' && required)
                ? `${placeholder}*`
                : placeholder
            }
            className={classNames(style.input, {
              [style[`text-align_${align}`]]: align,
            })}
            disabled={disabled}
            readOnly={readOnly}
            value={value}
            onChange={handleChange}
            onPaste={onPaste}
            onKeyDown={onKeyDown}
            onFocus={handleFocus}
            onBlur={handleBlur}
            ref={ref}
          />
        );
      }
    };

    return (
      <>
        <Tooltip
          ref={wrapperRef}
          message={tooltipMessage}
          tooltipWidth={tooltipWidth}
          wrapperClassName={classNames(
            style.wrapper,
            className,
            style[`width_${width}`],
            style[`height_${height}`],
            // style[`flexgrow_${flexgrow}`],
          )}
        >
          <div
            className={classNames(
              style.component,
              style[`background_${background}`],
              style[`color_${color}`],
              style[`border_${border}`],
              {
                [style.active]: inputActive,
                [style.notEmpty]: notEmpty,
                [style.tags]: tags && tags.length > 0,
                [style.hovered]: hovered,
                [style.disabled]: disabled,
                [style.readOnly]: readOnly,
                [style.error]: error,
                [style[`padding_${elementPosition}`]]: elementPosition,
              },
            )}
            onClick={onClick}
            onMouseEnter={() => setHovered(true)}
            onMouseLeave={() => setHovered(false)}
          >
            {renderTypeOfInput()}
            {icon && (
              <Icon
                className={classNames(style.icon, style[`icon_${elementPosition}`])}
                icon={icon}
                staticColor={iconStaticColor}
                path="inherit"
                cursor="text"
                clickThrough
              />
            )}
            {symbol && (
              <Text
                type="body-regular-14"
                color={symbolColor}
                className={classNames(style.symbol, style[`symbol_${elementPosition}`])}
              >
                {symbol}
              </Text>
            )}
            {(height === '48' || height === 'full') && (
              <span className={style.label}>
                {label}
                {required && <span className={style.required}>*</span>}
              </span>
            )}
            {height === '48' && secondaryLabel && (
              <span className={style.secondaryLabel}>{secondaryLabel}</span>
            )}
            {action}
            {isLoading && (
              <ContentLoader height="100%" isLoading={isLoading ?? false} isInsideComponent />
            )}
          </div>
        </Tooltip>
        {description && <InputDescription description={description} width={width} />}
        {errorsList && <InputErrorList errorsList={errorsList} width={width} />}

        {!!errorMessage && hovered && !focused && wrapperRef.current && (
          <InputErrorPopup inputWrapperElement={wrapperRef.current} errorMessage={errorMessage} />
        )}
      </>
    );
  },
);

Input.displayName = 'Input';

export default Input;
