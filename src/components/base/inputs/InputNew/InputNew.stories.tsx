import { Meta, Story } from '@storybook/react';
import { Icon } from 'Components/base/Icon';
import { InputNew as Component, InputNewProps as Props } from 'Components/base/inputs/InputNew';
import { ReactComponent as SearchIcon } from 'Svg/v2/16/search.svg';
import React, { useState } from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Inputs/Input New',
  component: Component,
  argTypes: {
    name: hideProperty,
    icon: hideProperty,
    symbol: hideProperty,
    SKU: hideProperty,
    className: hideProperty,
  },
} as Meta;

const InputNewComponent: Story<Props> = (args) => {
  const [val, setVal] = useState('');
  return (
    <Component
      value={val}
      onChange={(e) => {
        setVal(e.target.value);
      }}
      {...args}
    />
  );
};

const InputNewWithIconComponent: Story<Props> = (args) => {
  const [val, setVal] = useState('');
  return (
    <Component
      value={val}
      onChange={(e) => {
        setVal(e.target.value);
      }}
      icon={<Icon icon={<SearchIcon />} clickThrough />}
      {...args}
    />
  );
};

const InputNewWithSymbolComponent: Story<Props> = (args) => {
  const [val, setVal] = useState('');
  return (
    <Component
      value={val}
      onChange={(e) => {
        setVal(e.target.value);
      }}
      symbol="$"
      {...args}
    />
  );
};

export const InputNew = InputNewComponent.bind({});
export const InputNewWithIcon = InputNewWithIconComponent.bind({});
export const InputNewWithSymbol = InputNewWithSymbolComponent.bind({});
InputNew.args = {
  name: 'input-storybook',
  width: '320',
  height: '48',
  label: 'Label',
  placeholder: 'Placeholder',
  required: true,
  disabled: false,
  readOnly: false,
  error: false,
  background: 'grey-10',
};
InputNewWithIcon.args = {
  name: 'input-storybook-with-icon',
  width: '320',
  height: '48',
  elementPosition: 'left',
  label: 'Label',
  placeholder: 'Placeholder',
  required: true,
  disabled: false,
  readOnly: false,
  error: false,
  background: 'grey-10',
};
InputNewWithSymbol.args = {
  name: 'input-storybook-with-symbol',
  isLoading: false,
  width: '320',
  height: '48',
  elementPosition: 'left',
  symbolColor: 'grey-100',
  label: 'Label',
  placeholder: 'Placeholder',
  required: true,
  disabled: false,
  readOnly: false,
  error: false,
  background: 'grey-10',
};
