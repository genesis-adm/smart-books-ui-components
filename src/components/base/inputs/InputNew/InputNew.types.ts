import React from 'react';

import {
  InputBackgroundType,
  InputBorderType,
  InputColorType,
  InputHeightType,
  InputWidthType,
} from '../../../../types/inputs';
import type { TagType } from '../../../base/inputs/InputElements/InputTag';
import type { TextColorStaticNewTypes } from '../../../types/colorTypes';
import type { TextAlign } from '../../../types/fontTypes';
import { TagBackground, TagWidth } from '../InputElements/InputTag/InputTag';

export interface AccordionOption {
  text: string;
  icon?: React.ReactNode;
}

export interface InputNewProps extends React.InputHTMLAttributes<HTMLInputElement> {
  name: string;
  label: string;
  width: InputWidthType;
  maxLength?: number;
  height?: InputHeightType;
  secondaryLabel?: string;
  accordionArr?: AccordionOption[];
  tags?: TagType[];
  tagColor?: TagBackground;
  tagWidth?: TagWidth;
  icon?: React.ReactNode;
  iconStaticColor?: boolean;
  symbol?: string;
  symbolColor?: TextColorStaticNewTypes;
  elementPosition?: 'left' | 'right';
  background?: InputBackgroundType;
  color?: InputColorType;
  border?: InputBorderType;
  align?: TextAlign;
  required?: boolean;
  disabled?: boolean;
  readOnly?: boolean;
  active?: boolean;
  error?: boolean;
  errorMessage?: string;
  errorsList?: string[];
  action?: React.ReactNode;
  tooltip?: React.ReactNode;
  disabledTooltip?: React.ReactNode;
  tooltipWidth?: '350' | 'auto';
  description?: string;
  className?: string;
  isLoading?: boolean;
  wrapTags?: boolean;
}
