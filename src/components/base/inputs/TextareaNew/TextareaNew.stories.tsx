import { Meta, Story } from '@storybook/react';
import {
  TextareaNew as Component,
  TextareaNewProps as Props,
} from 'Components/base/inputs/TextareaNew';
import React, { useState } from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Inputs/Textarea New',
  component: Component,
  argTypes: {
    name: hideProperty,
    className: hideProperty,
    active: hideProperty,
    value: hideProperty,
    onChange: hideProperty,
  },
} as Meta;

const TextareaNewComponent: Story<Props> = (args) => {
  const [currentValue, setCurrentValue] = useState('');
  return (
    <Component {...args} value={currentValue} onChange={(e) => setCurrentValue(e.target.value)} />
  );
};

export const TextareaNew = TextareaNewComponent.bind({});
TextareaNew.args = {
  label: 'Label of textarea',
  name: 'textarea-storybook',
  tooltip: 'Tooltip of textarea',
  description: 'Description of textarea',
  placeholder: 'Enter your note...',
  maxLength: 200,
  error: false,
  required: true,
  disabled: false,
  readOnly: false,
  errorsList: ['Error #1'],
  background: 'grey-10',
  border: 'default',
  color: 'default',
  align: 'left',
  isLoading: true,
};
