import { ContentLoader } from 'Components/base/ContentLoader';
import { Tooltip } from 'Components/base/Tooltip';
import { InputDescription } from 'Components/base/inputs/InputElements/InputDescription';
import { InputErrorList } from 'Components/base/inputs/InputElements/InputErrorList';
import classNames from 'classnames';
import React, { TextareaHTMLAttributes, useLayoutEffect, useRef, useState } from 'react';

import { InputBackgroundType, InputBorderType, InputColorType } from '../../../../types/inputs';
import { TextAlign } from '../../../types/fontTypes';
import InputErrorPopup from '../InputErrorPopup/InputErrorPopup';
import styles from './TextareaNew.module.scss';

export interface TextareaNewProps extends TextareaHTMLAttributes<HTMLTextAreaElement> {
  name: string;
  label: string;
  placeholder?: string;
  background?: InputBackgroundType;
  color?: InputColorType;
  border?: InputBorderType;
  align?: TextAlign;
  required?: boolean;
  disabled?: boolean;
  readOnly?: boolean;
  active?: boolean;
  error?: boolean;
  errorsList?: string[];
  errorMessage?: string;
  maxLength?: number;
  minHeight?: '88' | '128' | '134' | '155';
  tooltip?: React.ReactNode;
  description?: string;
  hideCounter?: boolean;
  className?: string;
  value: string;
  isLoading?: boolean;
  style?: React.CSSProperties;

  onChange(e: React.ChangeEvent<HTMLTextAreaElement>): void;
}

const TextareaNew: React.FC<TextareaNewProps> = ({
  name,
  label,
  placeholder = ' ',
  background = 'grey-10',
  color = 'default',
  border = 'default',
  align,
  required,
  disabled,
  readOnly,
  active,
  error,
  errorsList,
  errorMessage,
  maxLength = 200,
  minHeight = '88',
  tooltip,
  description,
  hideCounter,
  className,
  value,
  onChange,
  onFocus,
  onBlur,
  isLoading,
  style,
}) => {
  const wrapperRef = useRef<HTMLDivElement>(null);
  const textAreaRef = useRef<HTMLTextAreaElement>(null);
  useLayoutEffect(() => {
    if (textAreaRef.current) {
      textAreaRef.current.style.height = '0px';
      const scrollHeight = textAreaRef.current.scrollHeight + 2;
      textAreaRef.current.style.height = scrollHeight + 'px';
    }
  }, [value]);
  const [focused, setFocused] = useState(false);
  const [hovered, setHovered] = useState(false);

  const handleChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    onChange?.(e);
  };
  const handleFocus = (e: React.FocusEvent<HTMLTextAreaElement>) => {
    onFocus?.(e);
    setFocused(true);
  };
  const handleBlur = (e: React.FocusEvent<HTMLTextAreaElement>) => {
    onBlur?.(e);
    setFocused(false);
  };

  const tooltipMessage = disabled ? 'Disabled' : focused ? '' : tooltip;

  return (
    <>
      <Tooltip
        ref={wrapperRef}
        message={tooltipMessage}
        wrapperClassName={classNames(styles.wrapper, className)}
      >
        <div
          className={styles.component}
          onMouseEnter={() => setHovered(true)}
          onMouseLeave={() => setHovered(false)}
        >
          <textarea
            className={classNames(
              styles.textarea,
              styles[`background_${background}`],
              styles[`color_${color}`],
              styles[`border_${border}`],
              styles[`minHeight_${minHeight}`],
              {
                [styles[`text-align_${align}`]]: align,
                [styles.active]: active,
                [styles.disabled]: disabled,
                [styles.error]: error,
              },
            )}
            style={style}
            id={name}
            maxLength={maxLength}
            placeholder={placeholder}
            required={required}
            disabled={disabled}
            readOnly={readOnly}
            value={value}
            onChange={handleChange}
            onFocus={handleFocus}
            onBlur={handleBlur}
            ref={textAreaRef}
          />
          <span className={styles.label}>
            {label}
            {required && <span className={styles.required}>*</span>}
          </span>
          <span
            className={classNames(styles.counter, {
              [styles.hidden]: !value || hideCounter,
              [styles.error]: error,
            })}
          >
            <span
              className={classNames(styles.current, {
                [styles.limit]: value?.length >= maxLength,
                [styles.error]: value?.length === 0 && error,
              })}
            >
              {value?.length}
            </span>
            /{maxLength}
          </span>
          {isLoading && (
            <ContentLoader height="100%" isLoading={isLoading ?? false} isInsideComponent />
          )}
        </div>
      </Tooltip>
      {description && <InputDescription description={description} width="full" />}
      {errorsList && <InputErrorList errorsList={errorsList} width="full" />}

      {!!errorMessage && hovered && !focused && wrapperRef.current && (
        <InputErrorPopup inputWrapperElement={wrapperRef.current} errorMessage={errorMessage} />
      )}
    </>
  );
};

export default TextareaNew;
