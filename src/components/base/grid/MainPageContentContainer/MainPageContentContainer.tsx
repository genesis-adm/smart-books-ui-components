import classNames from 'classnames';
import React from 'react';

import style from './MainPageContentContainer.module.scss';

export interface MainPageContentContainerProps {
  className?: string;
  children?: React.ReactNode;
}

const MainPageContentContainer: React.FC<MainPageContentContainerProps> = ({
  className,
  children,
}) => <div className={classNames(style.container, className)}>{children}</div>;

export default MainPageContentContainer;
