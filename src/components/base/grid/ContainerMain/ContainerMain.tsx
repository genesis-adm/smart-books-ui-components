import { Container } from 'Components/base/grid/Container';
import classNames from 'classnames';
import React from 'react';

import style from './ContainerMain.module.scss';

export interface ContainerMainProps {
  navOpened: boolean;
  className?: string;
  children?: React.ReactNode;
}

const ContainerMain: React.FC<ContainerMainProps> = ({ navOpened, className, children }) => (
  <Container
    flexgrow="1"
    flexdirection="column"
    transition
    background="grey-10"
    className={classNames(style.container, className, {
      [style.closed]: !navOpened,
    })}
  >
    {children}
  </Container>
);

export default ContainerMain;
