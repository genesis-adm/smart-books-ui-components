import { Container } from 'Components/base/grid/Container';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

interface SettingsContainerProps {
  width?: string;
  paddingX?: string;
  paddingY?: string;
  className?: string;
  children?: React.ReactNode;
}

const SettingsContainer: React.FC<SettingsContainerProps> = ({
  width = '645',
  paddingX = '40',
  paddingY = '30',
  className,
  children,
}) => (
  <Container
    background="white-100"
    flexgrow="1"
    radius
    overflow="auto"
    className={classNames(spacing[`pY${paddingY}`], spacing[`pX${paddingX}`], className)}
  >
    <Container flexdirection="column" className={classNames(spacing[`w${width}`], spacing.w100p)}>
      {children}
    </Container>
  </Container>
);

export default SettingsContainer;
