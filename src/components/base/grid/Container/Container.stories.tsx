import { Meta, Story } from '@storybook/react';
import { Container as Component, ContainerProps as Props } from 'Components/base/grid/Container';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Grid/Container',
  component: Component,
  argTypes: {
    className: hideProperty,
    onClick: hideProperty,
  },
} as Meta;

const ContainerTemplate: Story<Props> = (args) => <Component {...args} />;

export const Container = ContainerTemplate.bind({});
Container.args = {
  background: 'violet-90',
  radius: false,
  transition: true,
  fullscreen: true,
};
