import classNames from 'classnames';
import React, { CSSProperties, MouseEventHandler } from 'react';

import { TestableProps } from '../../../../types/general';
import type { BackgroundColorStaticNewTypes, BorderColorNewTypes } from '../../../types/colorTypes';
import type * as GridTypes from '../../../types/gridTypes';
import styles from './Container.module.scss';

export interface ContainerProps extends TestableProps {
  display?: GridTypes.Display;
  background?: BackgroundColorStaticNewTypes | 'transparent';
  border?: BorderColorNewTypes;
  flexdirection?: GridTypes.FlexDirection;
  justifycontent?: GridTypes.JustifyContent;
  alignitems?: GridTypes.AlignItems;
  aligncontent?: GridTypes.AlignContent;
  alignself?: GridTypes.AlignSelf;
  gap?:
    | '4'
    | '8'
    | '12'
    | '16'
    | '16-100'
    | '20'
    | '24'
    | '28'
    | '30'
    | '32'
    | '32-8'
    | '36'
    | '39'
    | '40'
    | '100'
    | '165';
  flexwrap?: GridTypes.FlexWrap;
  flexgrow?: GridTypes.FlexGrow;
  overflow?: GridTypes.Overflow;
  position?: GridTypes.Position;
  transition?: boolean;
  radius?: boolean;
  borderRadius?: '2' | '8' | '10' | '14' | '20' | '50per';
  customScroll?: boolean;
  fullscreen?: boolean;
  className?: string;
  style?: CSSProperties;
  children?: React.ReactNode;
  isFocusedElm?: boolean;

  onClick?(): void;

  onScroll?(event: React.UIEvent<HTMLDivElement>): void;

  onMouseEnter?: MouseEventHandler<HTMLDivElement>;
  onMouseLeave?: MouseEventHandler<HTMLDivElement>;
}

const Container: React.FC<ContainerProps> = ({
  isFocusedElm,
  display = 'flex',
  background = 'transparent',
  border,
  flexdirection,
  justifycontent,
  alignitems,
  aligncontent,
  alignself,
  gap,
  flexgrow,
  overflow,
  transition,
  radius,
  borderRadius,
  customScroll,
  flexwrap,
  position,
  className,
  fullscreen,
  style,
  onClick,
  children,
  dataTestId,
  onScroll,
  onMouseEnter,
  onMouseLeave,
}) => {
  const isFlex = display === 'flex' || display === 'inline-flex';
  return (
    <div
      data-testid={dataTestId}
      role="presentation"
      className={classNames(
        styles.container,
        styles[`background_${background}`],
        styles[`display_${display}`],
        {
          [styles.transition]: transition,
          [styles.radius]: radius,
          [styles[`border_${border}`]]: border,
          [styles[`position_${position}`]]: position,
          [styles[`borderRadius_${borderRadius}`]]: borderRadius,
          [styles.fullscreen]: fullscreen,
          [styles.customScroll]: customScroll,
          [styles.blurBackground]: isFocusedElm,
          [styles[`flex-direction_${flexdirection}`]]: isFlex && flexdirection,
          [styles[`justify-content_${justifycontent}`]]: isFlex && justifycontent,
          [styles[`align-items_${alignitems}`]]: isFlex && alignitems,
          [styles[`align-content_${aligncontent}`]]: isFlex && aligncontent,
          [styles[`align-self_${alignself}`]]: isFlex && alignself,
          [styles[`gap_${gap}`]]: isFlex && gap,
          [styles[`flex-wrap_${flexwrap}`]]: isFlex && flexwrap,
          [styles[`flex-grow_${flexgrow}`]]: flexgrow,
          [styles[`overflow_${overflow}`]]: overflow && isFlex,
        },
        className,
      )}
      onClick={onClick}
      style={style}
      onScroll={onScroll}
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
    >
      {children}
    </div>
  );
};

export default Container;
