import { Meta, Story } from '@storybook/react';
import { Radio as Component, RadioProps as Props } from 'Components/base/Radio';
import { Container } from 'Components/base/grid/Container';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Base/Radio',
  component: Component,
  argTypes: {
    id: hideProperty,
    name: hideProperty,
    checked: hideProperty,
    className: hideProperty,
    onChange: hideProperty,
  },
} as Meta;

const RadioComponent: Story<Props> = (args) => (
  <Container flexdirection="column" gap="4">
    {Array.from(Array(4)).map((_, index) => (
      <Component key={index} {...args} id={`${index}`}>
        Radio button #{index}
      </Component>
    ))}
  </Container>
);

export const Radio = RadioComponent.bind({});
Radio.args = {
  disabled: false,
  color: 'black-100',
  type: 'body-regular-14',
  name: 'storybook',
};
