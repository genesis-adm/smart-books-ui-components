import { Tooltip } from 'Components/base/Tooltip';
import classNames from 'classnames';
import React, { ReactElement } from 'react';

import { TestableProps } from '../../../types/general';
import type { TextColorStaticNewTypes } from '../../types/colorTypes';
import type { FontTypes } from '../../types/fontTypes';
import style from './Radio.module.scss';

export interface RadioProps extends TestableProps {
  id: string;
  name: string;
  checked?: boolean;
  disabled?: boolean;
  type?: FontTypes;
  color?: TextColorStaticNewTypes;
  checkedColor?: TextColorStaticNewTypes;
  className?: string;
  children?: React.ReactNode;
  hovered?: boolean;
  onChange(e: React.FormEvent<HTMLInputElement>): void;
}

const Radio = ({
  id,
  name,
  checked,
  disabled,
  type = 'body-regular-14',
  color = 'black-100',
  checkedColor = 'black-100',
  children,
  className,
  onChange,
  hovered,
  dataTestId,
}: RadioProps): ReactElement => (
  <label
    htmlFor={id}
    data-testid={dataTestId}
    className={classNames(
      style.wrapper,
      style[`type_${type}`],
      style[`color_${checked ? checkedColor : color}`],
      {
        [style.disabled]: disabled,
      },
      className,
    )}
  >
    <input
      id={id}
      className={classNames(style.radio, {
        [style.hover]: hovered,
      })}
      type="radio"
      name={name}
      checked={checked}
      onChange={onChange}
      disabled={disabled}
    />
    <Tooltip message={disabled && 'Disabled'} wrapperClassName={style.tooltip}>
      {children}
    </Tooltip>
  </label>
);

export default Radio;
