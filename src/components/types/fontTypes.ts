export type FontTypes =
  | 'inherit'
  | 'h1-bold'
  | 'h1-semibold'
  | 'h2-bold'
  | 'h2-semibold'
  | 'title-bold'
  | 'title-semibold'
  | 'title-regular'
  | 'body-medium'
  | 'body-regular-16'
  | 'body-bold-14'
  | 'body-regular-14'
  | 'button'
  | 'caption-uppercase'
  | 'caption-bold'
  | 'caption-semibold'
  | 'caption-regular'
  | 'caption-regular-height-20'
  | 'caption-regular-height-24'
  | 'text-medium'
  | 'text-regular'
  | 'subtext-bold'
  | 'subtext-semibold'
  | 'subtext-medium'
  | 'subtext-regular';
export type TextTransform = 'none' | 'lowercase' | 'uppercase' | 'capitalize' | 'first-letter';
export type TextAlign = 'center' | 'left' | 'right' | 'justify';
export type LineClamp = '1' | '2' | '3' | '4' | '5' | 'none';
