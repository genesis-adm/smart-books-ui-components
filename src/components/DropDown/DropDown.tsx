import classNames from 'classnames';
import React, { useEffect, useRef, useState } from 'react';

import useClickOutside from '../../hooks/useClickOutside';
import usePositioning from '../../hooks/usePositioning';
import useScrollOutside from '../../hooks/useScrollOutside';
import styles from './DropDown.module.scss';
import { DropDownProps, IControl } from './DropDown.types';

export const DropDownContext = React.createContext<Partial<IControl>>({});

const DropDown = ({
  children,
  control,
  axis = 'vertical',
  position,
  flexdirection = 'row',
  padding = '8',
  gap,
  opened = false,
  classNameOuter,
  classnameInner,
  currentStateInfo,
  customScroll,
  wrapperBorderColor,
  style,
  disableOutsideClick,
  stopDropdownPropagation,
  dataTestId,
}: DropDownProps) => {
  const [isOpen, setOpen] = useState(false);
  const wrapperRef = useRef(null);

  useClickOutside({
    ref: wrapperRef.current,
    cb: setOpen,
    isDisable: disableOutsideClick,
  });

  useScrollOutside({
    isListOpen: isOpen,
    cb: setOpen,
  });

  const { styles: positionStyles, mode } = usePositioning(
    wrapperRef.current,
    'auto',
    position,
    axis,
  );

  const handleStopDropPropagation = (e: React.MouseEvent<HTMLDivElement>) => {
    e.stopPropagation();
    e.preventDefault();
  };

  useEffect(() => {
    if (currentStateInfo) {
      currentStateInfo(isOpen);
    }
  }, [currentStateInfo, isOpen]);

  const handleOpen = () => {
    setOpen((prev) => !prev);
  };

  useEffect(() => {
    setOpen(opened);
  }, [opened]);

  return (
    <div
      ref={wrapperRef}
      className={classNames(styles.dropdownIcon, classNameOuter)}
      tabIndex={stopDropdownPropagation ? 1 : undefined}
      data-testid={dataTestId}
    >
      {control({
        handleOpen,
        isOpen,
      })}
      {isOpen && (
        <div
          style={positionStyles}
          className={classNames(styles.dropdownWrapper, classnameInner, {
            [styles[`border_${wrapperBorderColor}`]]: !!wrapperBorderColor,
          })}
          onMouseDown={(e) => stopDropdownPropagation && handleStopDropPropagation(e)}
        >
          <ul
            className={classNames(
              styles.scrolledContainer,
              styles[`direction_${mode}`],
              styles[`flex-direction_${flexdirection}`],
              styles[`padding_${padding}`],
              {
                [styles[`gap_${gap}`]]: gap,
                [styles.custom_scroll]: customScroll,
              },
            )}
            style={style}
          >
            <DropDownContext.Provider
              value={{
                handleOpen,
                isOpen,
              }}
            >
              {children}
            </DropDownContext.Provider>
          </ul>
        </div>
      )}
    </div>
  );
};

export default DropDown;
