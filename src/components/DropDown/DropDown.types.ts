import { Axis, PositionType } from 'hooks/usePositioning';
import React from 'react';

import { TestableProps } from '../../types/general';
import { BorderColorNewTypes } from '../types/colorTypes';

export interface IControl {
  handleOpen(): void;

  isOpen: boolean;
}

export type DropdownPositionType =
  | 'top-left'
  | 'top-right'
  | 'right-top'
  | 'right-bottom'
  | 'bottom-right'
  | 'bottom-left'
  | 'left-bottom'
  | 'left-top';

export interface DropDownProps extends TestableProps {
  opened?: boolean;
  axis?: Axis;
  position?: PositionType;
  flexdirection?: 'row' | 'row-reverse' | 'column' | 'column-reverse';
  padding: 'none' | '8' | '10' | '16' | '20' | '24' | '32';
  gap?: '0' | '4' | '8' | '12' | '16' | '20' | '24';
  classNameOuter?: string;
  classnameInner?: string;
  children?: React.ReactNode;
  control: (arg: IControl) => React.ReactNode;
  customScroll?: boolean;
  style?: React.CSSProperties;
  wrapperBorderColor?: BorderColorNewTypes;
  disableOutsideClick?: boolean;

  stopDropdownPropagation?: boolean;

  currentStateInfo?(arg: boolean): void;
}
