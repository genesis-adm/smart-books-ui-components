import { DropDown } from 'Components/DropDown';
import { DropDownItem } from 'Components/DropDownItem';
import { IconButton } from 'Components/base/buttons/IconButton';
import { ReactComponent as PencilIcon } from 'Svg/v2/16/pencil.svg';
import { ReactComponent as BtnIcon } from 'Svg/v2/16/three-dots.svg';
import React from 'react';

export default {
  title: 'REVISE LATER/DropDown',
};

export const DropDownDefault: React.FC = () => (
  <DropDown
    axis="horizontal"
    flexdirection="column"
    padding="10"
    control={({ handleOpen }) => (
      <IconButton size="lg" shape="circle" icon={<BtnIcon />} onClick={handleOpen} />
    )}
  >
    <DropDownItem onClick={() => {}} icon={<PencilIcon />}>
      Item 1
    </DropDownItem>
    <DropDownItem onClick={() => {}} icon={<PencilIcon />}>
      Item 2
    </DropDownItem>
    <DropDownItem onClick={() => {}} icon={<PencilIcon />}>
      Item 3
    </DropDownItem>
  </DropDown>
);
