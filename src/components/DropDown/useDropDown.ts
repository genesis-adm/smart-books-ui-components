import { useContext } from 'react';

import { DropDownContext } from './DropDown';

export default function useDropDown(): [() => void] | [undefined] {
  const { handleOpen: handleToggle } = useContext(DropDownContext);

  return [handleToggle];
}
