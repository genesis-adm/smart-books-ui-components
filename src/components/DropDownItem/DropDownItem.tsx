import { DropDownContext } from 'Components/DropDown';
import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React, { useContext } from 'react';

import style from './DropDownItem.module.scss';

export interface DropDownItemProps {
  type?: 'default' | 'button' | 'option';
  width?: '210' | '240';
  icon?: React.ReactNode;
  className?: string;
  children?: React.ReactNode;
  onClick(): void;
}

const DropDownItem: React.FC<DropDownItemProps> = ({
  type = 'default',
  width = '210',
  icon,
  onClick,
  className,
  children,
}) => {
  const { handleOpen } = useContext(DropDownContext);

  const handleClick = (e: React.MouseEvent<HTMLDivElement>) => {
    e.stopPropagation();
    onClick();
    if (handleOpen) handleOpen();
  };
  return (
    <div
      role="presentation"
      onClick={handleClick}
      className={classNames(style.component, style[`width_${width}`], style[type], className)}
    >
      <span className={style.icon}>{icon}</span>
      <Text type="button" color="inherit" className={style.text}>
        {children}
      </Text>
    </div>
  );
};
export default DropDownItem;
