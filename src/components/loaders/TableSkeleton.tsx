import { ContentLoader } from 'Components/base/ContentLoader';
import { Container } from 'Components/base/grid/Container';
import classNames from 'classnames';
import React from 'react';
import { FC, ReactElement, ReactNode } from 'react';

import spacing from '../../assets/styles/spacing.module.scss';

type TableLoaderWrapperProps = {
  isLoading: boolean;
  children: ReactNode;
  className?: string;
  rowPxHeight?: number;
  rowsNumber?: number;
  headerPxHeight?: number;
};

const TableSkeleton: FC<TableLoaderWrapperProps> = ({
  isLoading,
  children,
  className,
  headerPxHeight = 20,
  rowPxHeight = 40,
  rowsNumber = 3,
}: TableLoaderWrapperProps): ReactElement => {
  if (isLoading)
    return (
      <Container
        className={classNames(spacing.pb_auto, spacing.w100p, className)}
        gap="8"
        flexdirection="column"
      >
        <ContentLoader height={`${headerPxHeight}px`} isLoading />

        <Container flexdirection="column" gap="4">
          {Array.from(Array(rowsNumber)).map((_, index) => (
            <ContentLoader key={index} height={`${rowPxHeight}px`} isLoading />
          ))}
        </Container>
      </Container>
    );

  return <>{children}</>;
};

export default TableSkeleton;
