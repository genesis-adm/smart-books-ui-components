import { ContentLoader } from 'Components/base/ContentLoader';
import { Container } from 'Components/base/grid/Container';
import classNames from 'classnames';
import React, { FC, ReactElement, ReactNode } from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';
import styles from './ModalHeaderSkeleton.module.scss';

type ModalHeaderSkeletonProps = {
  titleWidthPx?: number;
  heightPx?: number;
  isLoading: boolean;
  children: ReactNode;
  optionsBtn?: boolean;
  border?: boolean;
};

const ModalHeaderSkeleton: FC<ModalHeaderSkeletonProps> = ({
  titleWidthPx = 421,
  heightPx = 60,
  isLoading,
  children,
  optionsBtn = false,
  border = false,
}: ModalHeaderSkeletonProps): ReactElement => {
  if (isLoading)
    return (
      <Container
        justifycontent="space-between"
        alignitems="center"
        className={classNames(styles.wrapper, { [styles.border]: border })}
        style={{ height: `${heightPx}px`, minHeight: `${heightPx}px` }}
      >
        <Container justifycontent="center" className={spacing.w100p}>
          <ContentLoader height="28px" width={`${titleWidthPx}px`} isLoading={isLoading} />
        </Container>

        <Container gap="16" position="absolute" className={styles.actionsWrapper}>
          {optionsBtn && (
            <ContentLoader height="24px" width="24px" type="circle" isLoading={isLoading} />
          )}
          <ContentLoader height="24px" width="24px" type="circle" isLoading={isLoading} />
        </Container>
      </Container>
    );

  return <>{children}</>;
};

export default ModalHeaderSkeleton;
