import { InputDescription } from 'Components/base/inputs/InputElements/InputDescription';
import { InputLabel } from 'Components/base/inputs/InputElements/InputLabel';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as PassIconClose } from 'Svg/16/eye-closed.svg';
import { ReactComponent as PassIcon } from 'Svg/16/eye.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import type { TextColorStaticNewTypes } from '../../types/colorTypes';
import type { TextAlign } from '../../types/fontTypes';
import style from './Input.module.scss';

export interface InputProps extends React.InputHTMLAttributes<HTMLInputElement> {
  name: string;
  label?: string;
  secondaryLabel?: string;
  tooltip?: React.ReactNode;
  title?: string;
  width?:
    | 'code'
    | 'rules'
    | 'modal-sm'
    | 'modal-md'
    | '248'
    | 'minimal'
    | 'default'
    | 'large'
    | 'full';
  height?: '40' | '50';
  flexgrow?: '0' | '1';
  padding?: '9-16' | '5-16';
  isRequired?: boolean;
  disabled?: boolean;
  align?: TextAlign;
  suggestionActive?: boolean;
  autoComplete?: 'on' | 'off' | 'none';
  icon?: React.ReactNode;
  iconPosition?: 'left' | 'right';
  symbol?: string;
  symbolPosition?: 'left' | 'right';
  symbolColor?: TextColorStaticNewTypes;
  color?: TextColorStaticNewTypes;
  type?: 'email' | 'number' | 'password' | 'search' | 'tel' | 'text';
  statusType?: 'normal' | 'success' | 'warning' | 'error';
  description?: string;
  maxLength?: number;
}

const Input = React.forwardRef<HTMLInputElement, InputProps>(
  (
    {
      type = 'text',
      statusType = 'normal',
      className,
      title,
      name,
      label,
      secondaryLabel,
      tooltip,
      placeholder,
      description,
      value,
      align,
      suggestionActive,
      autoComplete = 'off',
      width = 'default',
      height = '50',
      padding = '9-16',
      flexgrow = '1',
      icon,
      iconPosition = 'right',
      symbol,
      symbolPosition = 'right',
      symbolColor = 'black-100',
      color = 'black-100',
      maxLength = 40,
      onChange,
      onBlur,
      onPaste,
      onKeyDown,
      onFocus,
      isRequired,
      disabled,
      readOnly,
      children,
    },
    ref,
  ) => {
    const [open, setOpen] = useState<boolean>(false);
    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
      onChange?.(e);
    };

    const handleOpen = () => setOpen(!open);

    const typePass = open ? 'text' : 'password';

    return (
      <div
        className={classNames(
          style.input_wrapper,
          className,
          style[width],
          style[`flexgrow_${flexgrow}`],
        )}
      >
        {(label || secondaryLabel) && (
          <InputLabel
            name={name}
            label={label}
            secondaryLabel={secondaryLabel}
            tooltip={tooltip}
            required={isRequired}
            disabled={disabled}
            readOnly={readOnly}
          />
        )}
        <div className={style.input_field}>
          {icon && type !== 'password' && (
            <span className={classNames(style.icon, style[`icon_${iconPosition}`])}>{icon}</span>
          )}
          {symbol && (
            <Text
              type="body-regular-14"
              color={symbolColor}
              className={classNames(style.symbol, style[`symbol_${symbolPosition}`])}
            >
              {symbol}
            </Text>
          )}
          {type === 'password' && (
            <span
              role="presentation"
              onClick={handleOpen}
              className={classNames(style.icon, style[`icon_${iconPosition}`], style.icon_password)}
            >
              {open ? <PassIconClose /> : <PassIcon />}
            </span>
          )}
          {children}
          <input
            type={type === 'password' ? typePass : type}
            maxLength={width === 'code' ? 2 : maxLength}
            id={name}
            value={value}
            title={title}
            autoComplete={autoComplete}
            className={classNames(
              style.input,
              style[statusType],
              style[`text-align_${align}`],
              style[`padding_${padding}`],
              style[`color_${color}`],
              style[`height_${height}`],
              {
                [style.disabled]: disabled,
                [style.readonly]: readOnly,
                [style.suggestion]: suggestionActive,
              },
            )}
            placeholder={width === 'code' ? '**' : placeholder}
            ref={ref}
            disabled={disabled}
            readOnly={readOnly}
            onChange={handleChange}
            onBlur={onBlur}
            onPaste={onPaste}
            onKeyDown={onKeyDown}
            onFocus={onFocus}
          />
        </div>
        {description && <InputDescription description={description} />}
      </div>
    );
  },
);

Input.displayName = 'Input';

export default Input;
