import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React from 'react';

import style from './TransactionData.module.scss';

export interface TransactionDataProps {
  name: string;
  value: string;
  className?: string;
}

const TransactionData: React.FC<TransactionDataProps> = ({ name, value, className }) => (
  <Container justifycontent="space-between" className={classNames(style.component, className)}>
    <Text type="text-regular" color="grey-100">
      {name}
    </Text>
    <Text type="text-regular">{value}</Text>
  </Container>
);

export default TransactionData;
