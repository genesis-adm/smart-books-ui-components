import { Button } from 'Components/base/buttons/Button';
import { Text } from 'Components/base/typography/Text';
import { Logo } from 'Components/elements/Logo';
import { ReactComponent as UploadImageIcon } from 'Svg/24/addimage.svg';
import classNames from 'classnames';
import React, { useRef } from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';
import style from './ImageUpload.module.scss';

export interface ImageUploadProps {
  name: string;
  image: string;
  onChange(file: File): void;
  className?: string;
}

const ImageUpload: React.FC<ImageUploadProps> = ({ name, image, onChange, className }) => {
  const inputRef = useRef<HTMLInputElement>(null);
  const handleUpload = (): void => {
    if (inputRef.current) inputRef.current.click();
  };
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    if (!e.target.files) return;
    const file = e.target.files[0];
    onChange(file);
  };
  return (
    <div className={classNames(style.component, className)}>
      <div
        role="presentation"
        onClick={handleUpload}
        className={classNames(style.logo, {
          [style.image]: image,
          [style.empty]: !image,
        })}
      >
        {!name || !image ? (
          <UploadImageIcon />
        ) : (
          <Logo size="xl" content="company" name={name} img={image} />
        )}
        <input
          ref={inputRef}
          className={style.input}
          id="uploader"
          type="file"
          accept="image/*"
          onChange={handleChange}
        />
      </div>
      <div className={style.action}>
        <Text display="block" type="caption-regular">
          Upload a logo or a default logo will be used
        </Text>
        <Text display="block" className={spacing.mt4} type="text-regular" color="grey-100">
          JPG; PNG; Maximum file size is 5MB
        </Text>
        <Button
          height="small"
          className={spacing.mt10}
          onClick={handleUpload}
          background="grey-20"
          color="grey-100"
          border="grey-20-grey-90"
        >
          Choose file
        </Button>
      </div>
    </div>
  );
};

export default ImageUpload;
