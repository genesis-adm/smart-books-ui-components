export interface IHeader {
  label: string;
  value: string;
}
