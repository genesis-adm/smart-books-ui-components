import { Scrollbar } from 'Components/base/Scrollbar';
import classNames from 'classnames';
import React from 'react';

import style from './Table.module.scss';

export interface TableProps {
  tableHead?: React.ReactNode;
  pagination?: React.ReactNode;
  className?: string;
  children?: React.ReactNode;
}

const Table: React.FC<TableProps> = ({ tableHead, pagination, className, children }) => (
  <div className={classNames(style.container, className)}>
    <Scrollbar type="table" tableHead={tableHead}>
      {children}
    </Scrollbar>
    {pagination}
  </div>
);

export default Table;
