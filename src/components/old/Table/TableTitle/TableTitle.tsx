import { TooltipIcon } from 'Components/base/TooltipIcon';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as SortIcon } from 'Svg/10/sort.svg';
import { ReactComponent as QuestionIcon } from 'Svg/16/question.svg';
import classNames from 'classnames';
import React from 'react';

import type { TableCellWidth } from '../../../types/gridTypes';
import style from './TableTitle.module.scss';

interface TableTitleProps {
  title?: string;
  tooltip?: string;
  minWidth: TableCellWidth;
  flexgrow?: '0' | '1';
  sortIcon?: boolean;
  noSortIcon?: boolean;
  onlySortIcon?: boolean;
  children?: React.ReactNode;
  onClick(): void;
}

const TableTitle: React.FC<TableTitleProps> = ({
  title,
  tooltip,
  sortIcon,
  noSortIcon,
  onlySortIcon,
  minWidth,
  flexgrow = '0',
  onClick,
  children,
}) => {
  const sortIconActive = sortIcon || onlySortIcon;
  return (
    <span
      role="presentation"
      className={classNames(
        style.container,
        { [style.spacing]: !sortIcon && !noSortIcon, [style.onlySortIcon]: onlySortIcon },
        style[`min-width_${minWidth}`],
        [style[`flex-grow_${flexgrow}`]],
      )}
      onClick={onClick}
    >
      {title && (
        <Text className={style.text} noWrap type="text-regular" color="grey-100">
          {title}
        </Text>
      )}
      {tooltip && (
        <TooltipIcon
          className={style.tooltip}
          icon={<QuestionIcon />}
          size="16"
          message={tooltip}
        />
      )}
      {sortIconActive && <SortIcon className={style.icon} />}
      {children && children}
    </span>
  );
};

TableTitle.defaultProps = {
  title: '',
  tooltip: '',
  sortIcon: false,
  noSortIcon: false,
  onlySortIcon: false,
  flexgrow: '0',
};

export default TableTitle;
