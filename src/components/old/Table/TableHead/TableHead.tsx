import classNames from 'classnames';
import React from 'react';

import style from './TableHead.module.scss';

export interface TableHeadProps {
  margin?: 'default' | 'none' | '16';
  radius?: '0' | '10';
  children?: React.ReactNode;
}

const TableHead: React.FC<TableHeadProps> = ({ margin = 'default', radius = '10', children }) => (
  <div className={classNames(style.header, style[`margin_${margin}`], style[`radius_${radius}`])}>
    {children}
  </div>
);

export default TableHead;
