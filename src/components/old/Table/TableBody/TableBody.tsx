import classNames from 'classnames';
import React from 'react';

import type { Display } from '../../../types/gridTypes';
import style from './TableBody.module.scss';

interface TableBodyProps {
  noBackground?: boolean;
  margin?: 'default' | 'none';
  display?: Display;
  result?: number;
  className?: string;
  children?: React.ReactNode;
}

const TableBody: React.FC<TableBodyProps> = ({
  noBackground,
  margin = 'default',
  display = 'table',
  result = 1,
  className,
  children,
}) => (
  <div
    className={classNames(
      style.body,
      style[`display_${display}`],
      style[`margin_${margin}`],
      className,
      {
        [style.noBackground]: noBackground,
        [style.noResults]: !result,
        [style.margin_none]: result === 0,
      },
    )}
  >
    {children}
  </div>
);

TableBody.defaultProps = {
  noBackground: false,
  margin: 'default',
  display: 'table',
  result: 1,
  className: '',
};

export default TableBody;
