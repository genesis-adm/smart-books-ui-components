import classNames from 'classnames';
import React from 'react';

import style from './TableRow.module.scss';

interface TableRowProps {
  size?: 'extrasmall' | 'small' | 'medium';
  background?: 'primary' | 'secondary' | 'mixed';
  gap?: '0' | '10';
  expired?: boolean;
  children?: React.ReactNode;
  onClick?(event: React.MouseEvent<HTMLDivElement>): void;
}

const TableRow: React.FC<TableRowProps> = ({
  size = 'medium',
  background = 'primary',
  gap = '10',
  expired,
  children,
  onClick,
}) => (
  <div
    className={classNames(
      style.row,
      style[`size_${size}`],
      style[`gap_${gap}`],
      style[background],
      {
        [style.expired]: expired,
      },
    )}
    role="presentation"
    onClick={onClick}
  >
    {children}
  </div>
);

TableRow.defaultProps = {
  size: 'medium',
  background: 'primary',
  gap: '10',
  expired: false,
  onClick: () => {},
};

export default TableRow;
