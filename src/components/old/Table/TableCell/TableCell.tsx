import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React from 'react';

import type { TableCellWidth } from '../../../types/gridTypes';
import style from './TableCell.module.scss';

interface TableCellProps {
  title?: string;
  minWidth: TableCellWidth;
  background?: 'transparent' | 'white-100' | 'grey-10';
  flexgrow?: '0' | '1';
  flexdirection?: 'row' | 'row-reverse' | 'column' | 'column-reverse';
  justifycontent?:
    | 'center'
    | 'flex-start'
    | 'flex-end'
    | 'space-around'
    | 'space-between'
    | 'space-evenly';
  alignitems?: 'center' | 'flex-start' | 'flex-end' | 'stretch' | 'baseline';
  aligncontent?:
    | 'center'
    | 'flex-start'
    | 'flex-end'
    | 'space-around'
    | 'space-between'
    | 'stretch';
  children?: React.ReactNode;
}

const TableCell: React.FC<TableCellProps> = ({
  title,
  minWidth,
  background = 'transparent',
  flexgrow = '0',
  flexdirection = 'column',
  justifycontent = 'center',
  alignitems,
  aligncontent,
  children,
}) => (
  <div
    title={title || ''}
    className={classNames(
      style.cell,
      style[`min-width_${minWidth}`],
      style[`background_${background}`],
      style[`flex-grow_${flexgrow}`],
      style[`flex-direction_${flexdirection}`],
      style[`justify-content_${justifycontent}`],
      {
        [style[`align-items_${alignitems}`]]: alignitems,
        [style[`align-content_${aligncontent}`]]: aligncontent,
      },
    )}
  >
    {title && (
      <Text className={style.text} type="text-regular" color="grey-100">
        {title}
      </Text>
    )}
    {children && children}
  </div>
);

TableCell.defaultProps = {
  title: '',
  background: 'transparent',
  flexgrow: '0',
  flexdirection: 'column',
  justifycontent: 'center',
  alignitems: 'stretch',
  aligncontent: 'stretch',
};

export default TableCell;
