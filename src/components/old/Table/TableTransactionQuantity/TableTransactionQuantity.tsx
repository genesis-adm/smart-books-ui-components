import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React from 'react';

import style from './TableTransactionQuantity.module.scss';

interface TableTransactionQuantityProps {
  value: number;
}

const TableTransactionQuantity: React.FC<TableTransactionQuantityProps> = ({ value }) => (
  <div className={classNames(style.counter)}>
    <Text type="caption-semibold" color="violet-90">
      {value}
    </Text>
  </div>
);

export default TableTransactionQuantity;
