import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React from 'react';

import style from './PaginationButton.module.scss';

interface PaginationButtonProps {
  selected?: boolean;
  className?: string;
  children?: React.ReactNode;
  onClick(): void;
}

const PaginationButton: React.FC<PaginationButtonProps> = ({
  selected,
  className,
  children,
  onClick,
}) => (
  <div
    role="presentation"
    onClick={onClick}
    className={classNames(style.button, className, {
      [style.selected]: selected,
    })}
  >
    <Text type="button" align="center" color="black-100">
      {children}
    </Text>
  </div>
);

PaginationButton.defaultProps = {
  selected: false,
  className: '',
};

export default PaginationButton;
