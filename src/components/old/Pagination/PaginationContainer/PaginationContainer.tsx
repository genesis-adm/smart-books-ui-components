import { Container } from 'Components/base/grid/Container';
import React from 'react';

import style from './PaginationContainer.module.scss';

interface PaginationContainerProps {
  children?: React.ReactNode;
}

const PaginationContainer: React.FC<PaginationContainerProps> = ({ children }) => (
  <Container className={style.container} alignitems="center">
    {children}
  </Container>
);

export default PaginationContainer;
