import classNames from 'classnames';
import React from 'react';

import style from './Pagination.module.scss';

interface PaginationProps {
  className?: string;
  children?: React.ReactNode;
}

const Pagination: React.FC<PaginationProps> = ({ className, children }) => (
  <div className={classNames(style.pagination, className)}>{children}</div>
);

Pagination.defaultProps = {
  className: '',
};

export default Pagination;
