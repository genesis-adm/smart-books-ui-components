import { SelectContext } from 'Components/old/SelectOld/SelectOld';
import classNames from 'classnames';
import React, { useContext } from 'react';

import style from './OptionSingle.module.scss';

export interface OptionSingleProps {
  value: string;
  label: string;
  icon?: React.ReactNode;
  className?: string;
}

const OptionSingle: React.FC<OptionSingleProps> = ({ value, label, icon, className }) => {
  const { handleClick, selected } = useContext(SelectContext);
  const handleSelect = () => handleClick({ value, label });
  const isActive = value === selected?.value;
  return (
    <li
      className={classNames(style.item, className, { [style.item_active]: isActive })}
      role="presentation"
      onClick={handleSelect}
    >
      <span className={style.label}>
        {icon}
        {label}
      </span>
    </li>
  );
};

export default OptionSingle;
