import classNames from 'classnames';
import React from 'react';

import style from './OptionEmpty.module.scss';

export interface OptionEmptyProps {
  className?: string;
}

const OptionEmpty: React.FC<OptionEmptyProps> = ({ className }) => (
  <li className={classNames(style.item, className)} role="presentation" onClick={() => {}}>
    <span className={style.label}>No results</span>
  </li>
);

export default OptionEmpty;
