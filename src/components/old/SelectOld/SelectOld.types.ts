import React from 'react';

import type { TextColorStaticNewTypes } from '../../types/colorTypes';

export interface SelectContextProps {
  handleClick(arg: OptionProps): void;
  selected: OptionProps | null;
}

export interface OptionProps {
  value: string;
  secondaryValue?: string;
  label: string;
  icon?: React.ReactNode;
}

export type StatusType = 'normal' | 'success' | 'warning' | 'error';

export interface SelectOldProps {
  options?: OptionProps[];
  placeholder: string;
  label?: string;
  reset?: boolean;
  title?: string;
  background?: 'gray' | 'transparent';
  color?: TextColorStaticNewTypes;
  defaultValue?: OptionProps;
  size?:
    | 'minimal'
    | 'small'
    | 'modal-sm'
    | 'modal-md'
    | 'medium'
    | 'intermediate'
    | 'large'
    | 'rules'
    | 'default'
    | 'auto'
    | 'full';
  padding?: 'default' | 'minimal' | 'none';
  icon?: React.ReactNode;
  labelInfo?: string;
  noBorder?: boolean;
  statusType?: StatusType;
  isRequired?: boolean;
  disabled?: boolean;
  className?: string;
  children?: React.ReactNode;
  onChange(item: OptionProps): void;
}
