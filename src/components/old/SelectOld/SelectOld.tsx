import { TooltipIcon } from 'Components/base/TooltipIcon';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as CaretDownIcon } from 'Svg/16/caret-down.svg';
import { ReactComponent as QuestionIcon } from 'Svg/16/question.svg';
import classNames from 'classnames';
import React, { createContext, useContext, useEffect, useRef, useState } from 'react';

import useClickOutside from '../../../hooks/useClickOutside';
import style from './SelectOld.module.scss';
import { OptionProps, SelectContextProps, SelectOldProps } from './SelectOld.types';
import { OptionEmpty } from './SelectOptions/OptionEmpty';

export const SelectContext = createContext({} as SelectContextProps);

export const Option: React.FC<OptionProps> = ({ value, secondaryValue, label, icon }) => {
  const { handleClick, selected } = useContext(SelectContext);

  const handleSelect = () => handleClick({ value, label });
  const isActive = value === selected?.value;

  return (
    <li
      className={classNames(style.list_item, { [style.list_item_active]: isActive })}
      role="presentation"
      onClick={handleSelect}
    >
      <span className={style.list_item_main}>
        {icon}
        {label}
      </span>
      {secondaryValue && <span className={style.list_item_secondary}>{secondaryValue}</span>}
    </li>
  );
};

const SelectOld: React.FC<SelectOldProps> = ({
  options,
  placeholder,
  label,
  reset,
  title,
  defaultValue = null,
  size = 'default',
  background = 'gray',
  color = 'inherit',
  padding = 'default',
  icon,
  labelInfo,
  onChange,
  noBorder,
  children,
  statusType = 'normal',
  isRequired,
  disabled,
  className,
}) => {
  const [isOpen, setOpen] = useState(false);
  const [position, setPosition] = useState('bottom-right');
  const [selected, setSelected] = useState<OptionProps | null>(() => defaultValue);
  const wrapperRef = useRef(null);
  useClickOutside({ ref: wrapperRef.current, cb: setOpen });

  useEffect(() => {
    if (reset) setSelected(null);
  }, [reset]);

  const handleClick = (item: OptionProps) => {
    onChange(item);
    setSelected(item);
    setOpen(false);
  };

  const positionOptions = (e: React.MouseEvent<HTMLElement>) => {
    if (
      e.clientY * 2 >
      (window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight)
    ) {
      setPosition('top-right');
    } else {
      setPosition('bottom-right');
    }
  };

  const handleToggle = (e: React.MouseEvent<HTMLElement>) => {
    if (!disabled) {
      setOpen(!isOpen);
      positionOptions(e);
    }
  };
  const handleLabelOpenClick = (e: React.MouseEvent<HTMLElement>) => {
    if (!isOpen && !disabled) {
      setOpen(!isOpen);
      positionOptions(e);
    }
  };
  return (
    <SelectContext.Provider value={{ handleClick, selected }}>
      <div ref={wrapperRef} className={classNames(style.select_wrapper, style[size], className)}>
        {(label || labelInfo) && (
          <div className={style.label_wrapper}>
            {label && (
              <div
                role="presentation"
                className={classNames(style.label, {
                  [style.required]: isRequired,
                  [style.pointer]: !isOpen,
                })}
                onClick={handleLabelOpenClick}
              >
                {label}
              </div>
            )}
            {labelInfo && <TooltipIcon size="16" icon={<QuestionIcon />} message={labelInfo} />}
          </div>
        )}
        <div
          role="presentation"
          tabIndex={-1}
          title={title}
          onClick={handleToggle}
          className={classNames(
            style.select,
            style[size],
            style[`padding_${padding}`],
            style[background],
            style[statusType],
            {
              [style.no_border]: noBorder,
              [style.disabled]: disabled,
              [style.select_active]: isOpen,
            },
          )}
        >
          {icon && <span className={style.select_icon}>{icon}</span>}
          <Text
            className={style.select_text}
            type="body-regular-14"
            noWrap
            color={selected?.label || defaultValue?.label ? color : 'grey-90'}
          >
            {selected?.label || defaultValue?.label || placeholder}
          </Text>
          <CaretDownIcon
            className={classNames(style.select_arrow, style[`select_arrow_${color}`], {
              [style.select_arrow_checked]: isOpen,
            })}
          />
        </div>
        {isOpen && (
          <ul className={classNames(style.list, style[position])}>
            {!children && (!options || options.length === 0) && <OptionEmpty />}
            {children ??
              options?.map((item) => (
                <Option
                  key={item.value}
                  value={item.value}
                  secondaryValue={item.secondaryValue}
                  label={item.label}
                  icon={item.icon}
                />
              ))}
          </ul>
        )}
      </div>
    </SelectContext.Provider>
  );
};

export default SelectOld;
