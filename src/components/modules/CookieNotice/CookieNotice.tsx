import { Button } from 'Components/base/buttons/Button';
import { ButtonGroup } from 'Components/base/buttons/ButtonGroup';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as IconClose } from 'Svg/24/close-light.svg';
import React from 'react';

import style from './CookieNotice.module.scss';

interface CookieNoticeProps {
  children?: React.ReactNode;
  onClickAgree(): void;
  onClickManage(): void;
  onClickClose(): void;
}

const CookieNotice: React.FC<CookieNoticeProps> = ({
  children,
  onClickAgree,
  onClickManage,
  onClickClose,
}) => (
  <div className={style.container}>
    <Text className={style.text} type="body-regular-14" color="grey-100">
      {children}
    </Text>
    <div className={style.wrapper}>
      <ButtonGroup className={style.buttons}>
        <Button
          width="sm"
          height="small"
          background="transparent-grey-10"
          color="grey-100-black-100"
          onClick={onClickAgree}
        >
          Manage settings
        </Button>
        <Button width="sm" height="small" onClick={onClickManage}>
          I agree
        </Button>
      </ButtonGroup>
      <IconClose className={style.icon} onClick={onClickClose} />
    </div>
  </div>
);

export default CookieNotice;
