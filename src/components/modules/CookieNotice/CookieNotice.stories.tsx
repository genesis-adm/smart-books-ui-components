import { LinkButton } from 'Components/base/buttons/LinkButton';
import React from 'react';

import Component from './CookieNotice';

export default {
  title: 'Modules/Cookie Notice',
};

export const CookieNotice: React.FC = () => (
  <Component onClickAgree={() => {}} onClickManage={() => {}} onClickClose={() => {}}>
    We use our own cookies as well as third-party cookies on our websites to enhance your
    experience, analyze our traffic, and for security and marketing. For more info, see our{' '}
    <LinkButton color="grey-100-violet-90" type="body-regular-14" onClick={() => {}}>
      Cookie Policy
    </LinkButton>
    .
  </Component>
);
