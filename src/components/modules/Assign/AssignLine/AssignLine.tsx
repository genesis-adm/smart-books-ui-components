import { Icon } from 'Components/base/Icon';
import { Tag } from 'Components/base/Tag';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as ArrowsIcon } from 'Svg/v2/12/arrows.svg';
import { ReactComponent as StatusErrorIcon } from 'Svg/v2/16/cross-in-circle.svg';
import { ReactComponent as StatusSuccessIcon } from 'Svg/v2/16/tick-in-circle.svg';
import classNames from 'classnames';
import React, { ReactNode } from 'react';

import style from './AssignLine.module.scss';

export interface AssignLineProps {
  order: string;
  sourceValue: string;
  sourceSecondaryValue: string;
  isAssigned: boolean;
  className?: string;
  children?: ReactNode;
}

const AssignLine: React.FC<AssignLineProps> = ({
  order,
  sourceValue,
  sourceSecondaryValue,
  isAssigned,
  className,
  children,
}) => (
  <div className={classNames(style.line, className)}>
    <Text color="grey-100" className={style.order}>
      {order}
    </Text>
    <div className={style.text}>
      <Text noWrap>{sourceValue}</Text>
      <Text color="grey-100" noWrap>
        {sourceSecondaryValue}
      </Text>
    </div>
    <Icon
      className={style.arrows}
      icon={<ArrowsIcon />}
      path={isAssigned ? 'violet-90' : 'grey-100'}
    />
    {children}
    <Tag
      className={style.icon}
      icon={isAssigned ? <StatusSuccessIcon /> : <StatusErrorIcon />}
      color={isAssigned ? 'green' : 'red'}
      cursor="default"
    />
  </div>
);

export default AssignLine;
