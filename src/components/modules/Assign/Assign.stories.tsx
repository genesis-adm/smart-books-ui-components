import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownFooter } from 'Components/SelectDropdown/components/SelectDropdownFooter';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { SelectNew } from 'Components/base/inputs/SelectNew';
import { OptionDoubleVerticalNew } from 'Components/base/inputs/SelectNew/options/OptionDoubleVerticalNew';
import { options } from 'Mocks/fakeOptions';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import React from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';
import { AssignContainer } from './AssignContainer';
import { AssignLine } from './AssignLine';

export default {
  title: 'Modules/Assign',
};

export const Assign: React.FC = () => (
  <AssignContainer titleSource="Source" titleTarget="Target">
    <AssignLine
      order="1"
      sourceValue="BetterMe health & fit"
      sourceSecondaryValue="BetterMe"
      isAssigned={false}
    >
      <SelectNew
        onChange={() => {}}
        // defaultValue={{ label: '', value: '' }}
        label="Assign Product"
        placeholder="Assign Product"
        name="product"
        width="full"
      >
        {({ onClick, state, selectElement }) => (
          <SelectDropdown selectElement={selectElement}>
            <SelectDropdownBody>
              <SelectDropdownItemList>
                {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                  <OptionDoubleVerticalNew
                    key={value}
                    label={label}
                    secondaryLabel={secondaryLabel}
                    selected={state?.value === value}
                    onClick={() =>
                      onClick({
                        label,
                        value,
                      })
                    }
                  />
                ))}
              </SelectDropdownItemList>
            </SelectDropdownBody>

            <SelectDropdownFooter>
              <Container justifycontent="flex-end" className={spacing.mr12}>
                <LinkButton icon={<PlusIcon />} onClick={() => {}}>
                  Create new Product
                </LinkButton>
              </Container>
            </SelectDropdownFooter>
          </SelectDropdown>
        )}
      </SelectNew>
    </AssignLine>
    <AssignLine
      order="2"
      sourceValue="BetterMe health & fit"
      sourceSecondaryValue="BetterMe"
      isAssigned
    >
      <SelectNew
        onChange={() => {}}
        // defaultValue={{ label: '', value: '' }}
        label="Assign Product"
        placeholder="Assign Product"
        name="product"
        width="full"
      >
        {({ onClick, state, selectElement }) => (
          <SelectDropdown selectElement={selectElement}>
            <SelectDropdownBody>
              <SelectDropdownItemList>
                {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                  <OptionDoubleVerticalNew
                    key={value}
                    label={label}
                    secondaryLabel={secondaryLabel}
                    selected={state?.value === value}
                    onClick={() =>
                      onClick({
                        label,
                        value,
                      })
                    }
                  />
                ))}
              </SelectDropdownItemList>
            </SelectDropdownBody>

            <SelectDropdownFooter>
              <Container justifycontent="flex-end" className={spacing.mr12}>
                <LinkButton icon={<PlusIcon />} onClick={() => {}}>
                  Create new Product
                </LinkButton>
              </Container>
            </SelectDropdownFooter>
          </SelectDropdown>
        )}
      </SelectNew>
    </AssignLine>
  </AssignContainer>
);
