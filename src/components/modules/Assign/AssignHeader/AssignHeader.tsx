import { Divider } from 'Components/base/Divider';
import { Text } from 'Components/base/typography/Text';
import React from 'react';

import style from './AssignHeader.module.scss';

export interface AssignHeaderProps {
  titleSource: string;
  titleTarget: string;
}

const AssignHeader: React.FC<AssignHeaderProps> = ({ titleSource, titleTarget }) => (
  <div className={style.header}>
    <Text className={style.order} color="grey-100">
      #
    </Text>
    <div className={style.source}>
      <Divider height="20" type="vertical" background="grey-90" />
      <Text color="grey-100" noWrap>
        {titleSource}
      </Text>
    </div>
    <div className={style.target}>
      <Divider height="20" type="vertical" background="grey-90" />
      <Text color="grey-100" noWrap>
        {titleTarget}
      </Text>
    </div>
  </div>
);

export default AssignHeader;
