import { Container } from 'Components/base/grid/Container';
import { AssignHeader, AssignHeaderProps } from 'Components/modules/Assign/AssignHeader';
import classNames from 'classnames';
import React, { ReactNode } from 'react';

import style from './AssignContainer.module.scss';

export interface AssignContainerProps extends Partial<AssignHeaderProps> {
  width?: '620' | '660' | 'auto';
  className?: string;
  children?: ReactNode;
}

const AssignContainer: React.FC<AssignContainerProps> = ({
  width = 'auto',
  titleSource,
  titleTarget,
  className,
  children,
}) => (
  <Container
    customScroll
    className={classNames(style.container, style[`width_${width}`], className)}
    flexgrow="1"
    flexdirection="column"
    gap="24"
  >
    {titleSource && titleTarget && (
      <AssignHeader titleSource={titleSource} titleTarget={titleTarget} />
    )}
    {children}
  </Container>
);

export default AssignContainer;
