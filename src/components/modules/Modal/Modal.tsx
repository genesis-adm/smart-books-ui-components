import { Scrollbar } from 'Components/base/Scrollbar';
import classNames from 'classnames';
import React from 'react';

import style from './Modal.module.scss';
import { ModalProps } from './Modal.types';
import Portal from './ModalPortal';

const Modal = React.forwardRef<HTMLDivElement, ModalProps>(
  (
    {
      modalId,
      containerId,
      type = 'old',
      size,
      height = '495',
      overlay = 'black-100',
      background = 'white-100',
      flexdirection = 'column',
      padding = 'default',
      justifycontent,
      alignitems,
      aligncontent,
      header,
      footer,
      centered,
      pluginScrollDisabled,
      isAnimationDisabled,
      children,
      className,
      classNameModal,
      classNameModalMain,
      classNameModalBody,
      dataTestId,
    },
    ref,
  ) => (
    <Portal containerId={containerId}>
      <div data-testid={dataTestId} className={classNames(style.wrapper, style[`type_${type}`])}>
        {overlay && (
          <div
            className={classNames(style.overlay, {
              [style[`overlay_${overlay}`]]: overlay,
            })}
          />
        )}
        <div
          ref={ref}
          id={modalId}
          className={classNames(
            style.modal,
            style[`type_${type}`],
            style[`height_${height}`],
            style[`size_${size}`],
            style[`padding_${padding}`],
            className,
            classNameModal,
            {
              [style.centered]: centered,
              [style[`background_${background}`]]: background,
              [style.animationDisabled]: isAnimationDisabled,
            },
          )}
        >
          {header && header}
          <div className={classNames(style.main, classNameModalMain)}>
            <Scrollbar disabled={pluginScrollDisabled} type="modal">
              <div
                className={classNames(style.body, classNameModalBody, {
                  [style[`flex-direction_${flexdirection}`]]: flexdirection,
                  [style[`justify-content_${justifycontent}`]]: justifycontent,
                  [style[`align-content_${aligncontent}`]]: aligncontent,
                  [style[`align-items_${alignitems}`]]: alignitems,
                })}
              >
                {children}
              </div>
            </Scrollbar>
          </div>
          {footer && footer}
        </div>
      </div>
    </Portal>
  ),
);

Modal.displayName = 'Modal';

export default Modal;
