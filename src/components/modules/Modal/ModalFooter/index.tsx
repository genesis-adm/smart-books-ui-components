import classNames from 'classnames';
import React from 'react';

import { TestableProps } from '../../../../types/general';
import type { JustifyContent } from '../../../types/gridTypes';
import style from './ModalFooter.module.scss';

interface ModalFooterProps extends TestableProps {
  justifycontent?: JustifyContent;
  children?: React.ReactNode;
}

const ModalFooter: React.FC<ModalFooterProps> = ({
  justifycontent = 'center',
  children,
  dataTestId,
}) => (
  <div
    data-testid={dataTestId}
    className={classNames(style.footer, {
      [style[`justify-content_${justifycontent}`]]: justifycontent,
    })}
  >
    {children}
  </div>
);

ModalFooter.defaultProps = {
  justifycontent: 'center',
};

export default ModalFooter;
