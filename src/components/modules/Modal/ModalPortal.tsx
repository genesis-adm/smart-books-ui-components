import React, { useEffect, useState } from 'react';
import { createPortal } from 'react-dom';

import { Nullable } from '../../../types/general';

const modalRoot = document.getElementById('modals') as HTMLElement;

interface ModalPortalProps {
  children?: React.ReactNode;
  containerId?: string;
}

const ModalPortal: React.FC<ModalPortalProps> = ({ children, containerId }) => {
  const [container, setContainer] = useState<Nullable<Element>>(null);

  useEffect(() => {
    setContainer((containerId && document.getElementById(containerId)) || modalRoot);
  }, []);

  return container ? createPortal(children, container) : <></>;
};

export default ModalPortal;
