import { Meta, Story } from '@storybook/react';
import { DropDown } from 'Components/DropDown';
import { Tag } from 'Components/base/Tag';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import {
  ModalHeaderSales as Component,
  ModalHeaderSalesProps as Props,
} from 'Components/modules/Modal/elements/ModalHeaderSales';
import { ReactComponent as CreateInvoiceTitleIcon } from 'Svg/v2/16/clipboard-list.svg';
import { ReactComponent as DocumentIcon } from 'Svg/v2/16/file-blank.svg';
import { ReactComponent as DropdownIcon } from 'Svg/v2/16/three-dots.svg';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Modules/Modal/Elements/ModalHeaderSales',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const ModalHeaderSalesComponent: Story<Props> = (args) => <Component {...args} />;

export const ModalHeaderSales = ModalHeaderSalesComponent.bind({});
ModalHeaderSales.args = {
  id: '# 000001',
  legalEntityName: 'BetterMe Limited',
  title: 'Invoice',
  createdBy: 'Vitalii Kvasha',
  titleIcon: <CreateInvoiceTitleIcon />,
  action: (
    <DropDown
      flexdirection="column"
      padding="8"
      gap="4"
      control={({ handleOpen }) => (
        <IconButton
          icon={<DropdownIcon />}
          onClick={handleOpen}
          background="white-100"
          color="grey-100"
        />
      )}
    >
      <DropDownButton size="160" icon={<DocumentIcon />} onClick={() => {}}>
        Edit
      </DropDownButton>
      {/* <DropDownButton size="160" icon={<EyeIcon />} onClick={() => {}}>
        View source document
      </DropDownButton>
      <DropDownButton size="160" icon={<DocumentLinesIcon />} onClick={() => {}}>
        Bank transaction view
      </DropDownButton>
      <Divider type="horizontal" fullHorizontalWidth className={spacing.mY4} />
      <DropDownButton size="204" type="danger" icon={<TrashIcon />} onClick={() => {}}>
        Delete
      </DropDownButton> */}
    </DropDown>
  ),
  actionTag: (
    <Tag
      size="32"
      padding="8"
      color="grey"
      text="In progress"
      icon={<DocumentIcon />}
      cursor="default"
    />
  ),
};
