import { Icon } from 'Components/base/Icon';
import { Text } from 'Components/base/typography/Text';
import { Logo } from 'Components/elements/Logo';
import { ReactComponent as BriefcaseIcon } from 'Svg/v2/16/briefcase.svg';
import { ReactComponent as CrossIcon } from 'Svg/v2/16/cross.svg';
import classNames from 'classnames';
import React, { ReactNode } from 'react';

import style from './ModalHeaderSales.module.scss';

export interface ModalHeaderSalesProps {
  id: JSX.Element | string;
  title: JSX.Element | string;
  legalEntityName: JSX.Element | string;
  createdBy: JSX.Element | string;
  titleIcon: ReactNode;
  actionTag?: ReactNode;
  action?: ReactNode;
  className?: string;
  onClose(): void;
}

const ModalHeaderSales: React.FC<ModalHeaderSalesProps> = ({
  id,
  title,
  legalEntityName,
  createdBy,
  titleIcon,
  actionTag,
  action,
  className,
  onClose,
}) => (
  <div className={classNames(style.header, className)}>
    <div className={style.wrapper}>
      <div className={style.block}>
        <span className={style.iconCircle}>
          <Icon icon={titleIcon} path="grey-100" />
        </span>
        <div className={style.text}>
          <Text type="caption-regular" color="grey-100">
            {title}
          </Text>
          <Text type="body-medium">{id}</Text>
        </div>
      </div>
      <div className={style.block}>
        <span className={style.iconCircle}>
          <Icon icon={<BriefcaseIcon />} path="grey-100" />
        </span>
        <div className={style.text}>
          <Text type="caption-regular" color="grey-100">
            Legal entity
          </Text>
          <Text type="body-medium">{legalEntityName}</Text>
        </div>
      </div>
      <div className={style.block}>
        {typeof createdBy === 'string' ? (
          <Logo content="user" size="24" name={createdBy} />
        ) : (
          createdBy
        )}
        <div className={style.text}>
          <Text type="caption-regular" color="grey-100">
            Created by
          </Text>
          <Text type="body-medium">{createdBy}</Text>
        </div>
      </div>
    </div>
    <div className={style.actions}>
      {actionTag}
      {action}
      <div role="presentation" onClick={onClose} className={style.close}>
        <Icon className={style.icon} path="inherit" icon={<CrossIcon />} />
      </div>
    </div>
  </div>
);

export default ModalHeaderSales;
