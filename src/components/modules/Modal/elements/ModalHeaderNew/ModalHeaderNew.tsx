import { Icon } from 'Components/base/Icon';
import { Tooltip } from 'Components/base/Tooltip';
import { Text } from 'Components/base/typography/Text';
import { FontTypes } from 'Components/types/fontTypes';
import { ReactComponent as ArrowLeftIcon } from 'Svg/v2/16/arrow-left.svg';
import { ReactComponent as CrossIcon } from 'Svg/v2/16/cross.svg';
import classNames from 'classnames';
import React, { ReactNode } from 'react';

import { TestableProps } from '../../../../../types/general';
import style from './ModalHeaderNew.module.scss';

export interface ModalHeaderNewProps extends TestableProps {
  type?: 'default' | 'fullwidth';
  dropdownBtn?: ReactNode;
  action?: 'close' | 'back';
  closeSize?: '24' | '32';
  closeActionBackgroundColor?: 'white-100' | 'grey-10' | 'inherit';
  height?: '40' | '60' | '72' | '80' | '96' | '128';
  background?: 'white-100' | 'grey-10';
  title?: string | ReactNode;
  titlePosition?: 'left' | 'center' | 'right';
  fontType?: FontTypes;
  border?: boolean;
  icon?: React.ReactNode;
  tag?: React.ReactNode;
  radius?: boolean;
  className?: string;

  onClose(): void;

  closeBtnTooltip?: string;
}

const ModalHeaderNew: React.FC<ModalHeaderNewProps> = ({
  type = 'default',
  action = 'close',
  closeSize = '24',
  closeActionBackgroundColor = 'white-100',
  dropdownBtn,
  height = '72',
  background = 'white-100',
  title,
  titlePosition = 'left',
  fontType = 'title-bold',
  border,
  icon,
  tag,
  radius,
  className,
  onClose,
  closeBtnTooltip = 'Close',
  dataTestId,
}) => (
  <div
    data-testid={dataTestId}
    className={classNames(
      style.header,
      style[`type_${type}`],
      style[`height_${height}`],
      style[`background_${background}`],
      style[`titlePosition_${titlePosition}`],
      {
        [style.border]: border,
        [style.radius]: radius,
      },
      className,
    )}
  >
    {action === 'back' && (
      <div role="presentation" onClick={onClose} className={style.back}>
        <Icon className={style.icon} path="inherit" icon={<ArrowLeftIcon />} />
      </div>
    )}
    {title && (
      <div className={style.wrapper}>
        <Text type={fontType} color="inherit" align={titlePosition}>
          {title}
        </Text>
        {icon}
      </div>
    )}
    {tag && <span className={style.tag}>{tag}</span>}
    <div className={style.iconBtn_group}>
      {dropdownBtn}
      {action === 'close' && (
        <Tooltip message={closeBtnTooltip}>
          <div
            role="presentation"
            onClick={onClose}
            className={classNames(
              style.close,
              style[`closeSize_${closeSize}`],
              style[`background_${closeActionBackgroundColor}`],
            )}
          >
            <Icon className={style.icon} path="inherit" icon={<CrossIcon />} />
          </div>
        </Tooltip>
      )}
    </div>
  </div>
);

export default ModalHeaderNew;
