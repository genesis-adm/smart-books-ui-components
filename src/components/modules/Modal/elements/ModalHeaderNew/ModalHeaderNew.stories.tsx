import { Meta, Story } from '@storybook/react';
import { Tag } from 'Components/base/Tag';
import {
  ModalHeaderNew as Component,
  ModalHeaderNewProps as Props,
} from 'Components/modules/Modal/elements/ModalHeaderNew';
import { ReactComponent as DocumentIcon } from 'Svg/v2/16/file-blank.svg';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Modules/Modal/Elements/ModalHeaderNew',
  component: Component,
  argTypes: {
    className: hideProperty,
    onClose: hideProperty,
  },
} as Meta;

const ModalHeaderNewComponent: Story<Props> = (args) => <Component {...args} />;

export const ModalHeaderNew = ModalHeaderNewComponent.bind({});
ModalHeaderNew.args = {
  title: 'Modal header title',
  titlePosition: 'left',
  action: 'close',
  border: true,
  background: 'white-100',
  type: 'default',
  height: '80',
  closeSize: '32',
  tag: <Tag size="32" padding="8" color="green" text="New" icon={<DocumentIcon />} />,
};
