import { Meta, Story } from '@storybook/react';
import { Button } from 'Components/base/buttons/Button';
import {
  ModalFooterNew as Component,
  ModalFooterNewProps as Props,
} from 'Components/modules/Modal/elements/ModalFooterNew';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Modules/Modal/Elements/ModalFooterNew',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const ModalFooterNewComponent: Story<Props> = (args) => (
  <Component {...args}>
    <Button
      width="sm"
      height="48"
      background="white-100"
      color="black-100"
      border="grey-20-grey-90"
      onClick={() => {}}
    >
      Cancel
    </Button>
    <Button width="md" height="48" onClick={() => {}}>
      Create
    </Button>
  </Component>
);

export const ModalFooterNew = ModalFooterNewComponent.bind({});
ModalFooterNew.args = {
  background: 'white-100',
  border: true,
  justifycontent: 'space-between',
};
