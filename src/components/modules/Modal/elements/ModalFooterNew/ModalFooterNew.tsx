import classNames from 'classnames';
import React from 'react';

import { TestableProps } from '../../../../../types/general';
import type { JustifyContent } from '../../../../types/gridTypes';
import style from './ModalFooterNew.module.scss';

export interface ModalFooterNewProps extends TestableProps {
  background?: 'white-100' | 'grey-10';
  border?: boolean;
  gap?: '4' | '8' | '12' | '16' | '20' | '24';
  direction?: 'normal' | 'reverse';
  justifycontent?: JustifyContent;
  className?: string;
  children?: React.ReactNode;
}

const ModalFooterNew: React.FC<ModalFooterNewProps> = ({
  background = 'white-100',
  border,
  gap,
  direction = 'reverse',
  justifycontent = 'space-between',
  className,
  children,
  dataTestId,
}) => (
  <div
    data-testid={dataTestId}
    className={classNames(
      style.footer,
      style[`background_${background}`],
      style[`justify-content_${justifycontent}`],
      style[`direction_${direction}`],
      {
        [style.border]: border,
        [style[`gap_${gap}`]]: gap,
      },
      className,
    )}
  >
    {children}
  </div>
);

export default ModalFooterNew;
