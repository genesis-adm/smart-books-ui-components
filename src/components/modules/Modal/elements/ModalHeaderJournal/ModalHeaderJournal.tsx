import { Icon } from 'Components/base/Icon';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as CrossIcon } from 'Svg/v2/16/cross.svg';
import classNames from 'classnames';
import React, { ReactNode } from 'react';

import style from './ModalHeaderJournal.module.scss';

export interface ModalHeaderJournalProps {
  title: ReactNode;
  infoTag?: ReactNode;
  actionTag?: ReactNode;
  action?: ReactNode;
  className?: string;
  onClose(): void;
}

const ModalHeaderJournal: React.FC<ModalHeaderJournalProps> = ({
  title,
  infoTag,
  actionTag,
  action,
  className,
  onClose,
}) => (
  <div className={classNames(style.header, className)}>
    <div className={style.info}>
      <Text type="title-bold" color="inherit">
        {title}
      </Text>
      {infoTag}
    </div>
    <div className={style.actions}>
      {actionTag}
      {action}
      <div role="presentation" onClick={onClose} className={style.close}>
        <Icon className={style.icon} path="inherit" icon={<CrossIcon />} />
      </div>
    </div>
  </div>
);

export default ModalHeaderJournal;
