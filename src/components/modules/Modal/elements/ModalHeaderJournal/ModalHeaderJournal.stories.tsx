import { Meta, Story } from '@storybook/react';
import { Tag } from 'Components/base/Tag';
import {
  ModalHeaderJournal as Component,
  ModalHeaderJournalProps as Props,
} from 'Components/modules/Modal/elements/ModalHeaderJournal';
import { ReactComponent as DocumentIcon } from 'Svg/v2/16/file-blank.svg';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Modules/Modal/Elements/ModalHeaderJournal',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const ModalHeaderJournalComponent: Story<Props> = (args) => <Component {...args} />;

export const ModalHeaderJournal = ModalHeaderJournalComponent.bind({});
ModalHeaderJournal.args = {
  title: 'Journal entry # 000001',
  actionTag: (
    <Tag size="32" padding="8" color="green" text="New" icon={<DocumentIcon />} cursor="default" />
  ),
  infoTag: (
    <Tag
      size="32"
      padding="8"
      color="grey"
      text="View mode"
      icon={<DocumentIcon />}
      cursor="default"
    />
  ),
};
