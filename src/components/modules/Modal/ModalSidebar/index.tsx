import classNames from 'classnames';
import React from 'react';

import style from './ModalSidebar.module.scss';

interface ModalSidebarProps {
  size?: 'medium' | 'small';
  children?: React.ReactNode;
}

const ModalSidebar: React.FC<ModalSidebarProps> = ({ size = 'medium', children }) => (
  <div className={classNames(style.sidebar, style[size])}>{children}</div>
);

ModalSidebar.defaultProps = {
  size: 'medium',
};

export default ModalSidebar;
