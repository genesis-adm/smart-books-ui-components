import { TestableProps } from '../../../types/general';
import type * as GridTypes from '../../types/gridTypes';

export interface ModalProps extends TestableProps {
  modalId?: string;
  containerId?: string;
  type?: 'old' | 'new' | 'new-bottom' | 'small';
  size:
    | 'minimal'
    | 'full'
    | 'full-with-scroll'
    | 'xs'
    | 'sm'
    | 'md-narrow'
    | 'md-medium'
    | 'md-wide'
    | 'md-extrawide'
    | 'lg'
    | 'xl'
    | '352'
    | '450'
    | '530'
    | '580'
    | '614'
    | '630'
    | '800'
    | '820'
    | '912'
    | '920'
    | '960'
    | '990'
    | '1130'
    | '1288'
    | '1392'
    | '720'
    | '1040';
  height?: 'auto' | '220' | '336' | '495' | '540' | '575' | '690' | '739' | 'minimal';
  overlay?: 'black-100' | 'grey-10' | 'white-100' | 'violet-90' | 'none';
  background?: 'white-100' | 'grey-10';
  padding?: 'default' | 'none' | '24';
  flexdirection?: GridTypes.FlexDirection;
  justifycontent?: GridTypes.JustifyContent;
  alignitems?: GridTypes.AlignItems;
  aligncontent?: GridTypes.AlignContent;
  centered?: boolean;
  pluginScrollDisabled?: boolean;
  isAnimationDisabled?: boolean;
  header?: React.ReactNode;
  footer?: React.ReactNode;
  className?: string;
  classNameModal?: string;
  classNameModalMain?: string;
  classNameModalBody?: string;
  children?: React.ReactNode;
}
