import { Text } from 'Components/base/typography/Text';
import { ReactComponent as IconCloseDark } from 'Svg/24/close-gray.svg';
import { ReactComponent as IconCloseLight } from 'Svg/24/close-light.svg';
import classNames from 'classnames';
import React from 'react';

import { TestableProps } from '../../../../types/general';
import style from './ModalHeader.module.scss';

export interface ModalHeaderProps extends TestableProps {
  type?: 'default' | 'fullwidth';
  title?: string;
  titlePosition?: 'left' | 'center' | 'right';
  headerHeight?: 'small' | 'medium';
  backgroundColor?: 'grey-10' | 'white-100';
  borderBottom?: boolean;
  radius?: boolean;
  onClick?(): void;
}

const ModalHeader: React.FC<ModalHeaderProps> = ({
  type = 'default',
  title,
  titlePosition = 'left',
  headerHeight = 'medium',
  backgroundColor = 'grey-10',
  borderBottom = false,
  radius,
  dataTestId,
  onClick,
}) => (
  <div
    data-testid={dataTestId}
    className={classNames(
      style.header,
      style[type],
      style[headerHeight],
      style[`background_${backgroundColor}`],
      style[titlePosition],
      {
        [style.borderBottom]: borderBottom,
        [style.radius]: radius,
      },
    )}
  >
    {title && (
      <Text type="title-bold" align={titlePosition}>
        {title}
      </Text>
    )}
    <div className={style.action} role="presentation" onClick={onClick}>
      {backgroundColor === 'grey-10' ? <IconCloseLight /> : <IconCloseDark />}
    </div>
  </div>
);

export default ModalHeader;
