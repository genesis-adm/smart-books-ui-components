import { ButtonGroup } from 'Components/base/buttons/ButtonGroup';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { Option, SelectNew } from 'Components/base/inputs/SelectNew';
import { AccordionOption } from 'Components/base/inputs/SelectNew/Select.types';
import { OptionSingleNew } from 'Components/base/inputs/SelectNew/options/OptionSingleNew';
import { Text } from 'Components/base/typography/Text';
import { OptionProps } from 'Components/old/SelectOld/SelectOld.types';
import { ReactComponent as FlagRUIcon } from 'Svg/24/ru.svg';
import { ReactComponent as FlagENIcon } from 'Svg/24/us.svg';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';
import type {
  BackgroundColorStaticNewTypes,
  TextColorStaticNewTypes,
} from '../../../types/colorTypes';
import style from './ControlPanel.module.scss';

export interface ControlPanelProps {
  background?: BackgroundColorStaticNewTypes;
  color?: TextColorStaticNewTypes;
  className?: string;
  onClickPolicy(e: React.MouseEvent<HTMLButtonElement>): void;
  onClickTerms(e: React.MouseEvent<HTMLButtonElement>): void;
  onChangeSelect: (item: OptionProps | Option | AccordionOption[]) => void;
}

const options = [
  {
    label: 'English',
    value: 'en',
    icon: <FlagENIcon />,
  },
  {
    label: 'Russian',
    value: 'ru',
    icon: <FlagRUIcon />,
  },
];

const ControlPanel: React.FC<ControlPanelProps> = ({
  className,
  background = 'violet-90',
  color = 'white-100',
  onClickPolicy,
  onClickTerms,
  onChangeSelect,
}) => (
  <Container background={background} className={classNames(style.panel, className)} flexgrow="1">
    <Container className={spacing.w33p} background={background} alignitems="center" flexgrow="1">
      <Text type="body-regular-14" color="grey-90">
        Language:
      </Text>
      <SelectNew
        name="languageselect"
        width="112"
        height="28"
        readOnly
        border="transparent"
        background="transparent"
        color="light"
        onChange={onChangeSelect}
        placeholder="Language"
        label="Language"
        defaultValue={{ label: 'English', value: 'en' }}
      >
        {({ onClick, state }) => (
          <>
            {options.map(({ label, icon, value }) => (
              <OptionSingleNew
                key={value}
                label={label}
                icon={icon}
                selected={state?.value === value}
                onClick={() => onClick({ label, value })}
              />
            ))}
          </>
        )}
      </SelectNew>
    </Container>
    <Container
      className={spacing.w33p}
      background={background}
      justifycontent="center"
      alignitems="center"
      flexgrow="1"
    >
      <Text type="text-regular" color="grey-90">
        © 2020-
        {new Date().getFullYear()}
        &nbsp;SmartBooks Inc. All rights reserved.
      </Text>
    </Container>
    <Container
      className={spacing.w33p}
      background={background}
      justifycontent="flex-end"
      alignitems="center"
      flexgrow="1"
    >
      <ButtonGroup align="right">
        <LinkButton color={color} onClick={onClickPolicy}>
          Privacy Policy
        </LinkButton>
        <LinkButton color={color} onClick={onClickTerms}>
          Terms of Use
        </LinkButton>
      </ButtonGroup>
    </Container>
  </Container>
);

export default ControlPanel;
