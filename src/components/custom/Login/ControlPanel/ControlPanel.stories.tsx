import { Meta, Story } from '@storybook/react';
import {
  ControlPanel as Component,
  ControlPanelProps as Props,
} from 'Components/custom/Login/ControlPanel';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Login/Control Panel',
  component: Component,
  argTypes: {
    className: hideProperty,
    onChangeSelect: hideProperty,
    onClickPolicy: hideProperty,
    onClickTerms: hideProperty,
  },
} as Meta;

const ControlPanelComponent: Story<Props> = (args) => <Component {...args} />;

export const ControlPanel = ControlPanelComponent.bind({});
ControlPanel.args = {};
