import { Meta, Story } from '@storybook/react';
import {
  LoginUserPanel as Component,
  LoginUserPanelProps as Props,
} from 'Components/custom/Login/LoginUserPanel';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Login/Login User Panel',
  component: Component,
  argTypes: {
    className: hideProperty,
    onClickSettings: hideProperty,
    onClickLogout: hideProperty,
  },
} as Meta;

const LoginUserPanelComponent: Story<Props> = (args) => <Component {...args} />;

export const LoginUserPanel = LoginUserPanelComponent.bind({});
LoginUserPanel.args = {
  name: 'Name Surname',
};
