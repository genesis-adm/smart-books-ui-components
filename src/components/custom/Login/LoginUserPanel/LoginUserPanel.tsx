import { Divider } from 'Components/base/Divider';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as LogoutIcon } from 'Svg/16/logout.svg';
import { ReactComponent as SettingsIcon } from 'Svg/v2/16/settings.svg';
import React from 'react';

import style from './LoginUserPanel.module.scss';

export interface LoginUserPanelProps {
  name: string;
  onClickSettings(): void;
  onClickLogout(): void;
}

const LoginUserPanel: React.FC<LoginUserPanelProps> = ({
  name,
  onClickSettings,
  onClickLogout,
}) => (
  <div className={style.component}>
    <Text className={style.text}>{name}</Text>
    <Divider className={style.divider} type="vertical" />
    <IconButton
      size="xs"
      background="transparent"
      color="grey-100-black-100"
      icon={<SettingsIcon />}
      onClick={onClickSettings}
    />
    <Divider className={style.divider} type="vertical" />
    <IconButton
      size="xs"
      background="transparent"
      color="grey-100-black-100"
      icon={<LogoutIcon />}
      onClick={onClickLogout}
    />
  </div>
);

export default LoginUserPanel;
