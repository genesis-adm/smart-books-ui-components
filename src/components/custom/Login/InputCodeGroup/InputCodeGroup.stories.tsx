import React from 'react';

import InputCodeGroup from './InputCodeGroup';

export default {
  title: 'Components/Custom/Login/InputCodeGroup',
};

export const InputCodeGroupDefault: React.FC = () => (
  <InputCodeGroup align="center" onChange={() => {}} />
);
