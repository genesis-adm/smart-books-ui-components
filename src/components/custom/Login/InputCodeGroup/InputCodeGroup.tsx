import { ButtonGroup } from 'Components/base/buttons/ButtonGroup';
import { Input } from 'Components/old/Input';
import React, { useState } from 'react';

const MAX_LENGTH = 2;
const MIN_LENGTH = 0;
const MAX_FIELDS = 4;
const KEY_BACKSPACE = 'Backspace';

interface InputCodeGroupProps {
  align?: 'left' | 'center' | 'right';
  className?: string;
  onChange(code: string): void;
}

const parseString = (str: string) => {
  let text = str.slice(0);
  const res = [];

  while (text.length) {
    res.push(text.slice(0, 2));
    text = text.slice(2);
  }
  return res;
};

const InputCodeGroup: React.FC<InputCodeGroupProps> = ({
  align = 'center',
  className,
  onChange,
}) => {
  const [value, setValue] = useState<string[]>(() => []);

  const handleUpdate = (val: string[]) => {
    setValue(val);
    onChange(val.join('').slice(0, MAX_FIELDS * 2));
  };

  const handlePaste = (e: React.ClipboardEvent) => {
    e.preventDefault();
    const text: string = e.clipboardData.getData('Text');
    handleUpdate(parseString(text));
  };

  const handleChange = (index: number) => (e: React.ChangeEvent<HTMLInputElement>) => {
    const newValue = [...value];
    newValue[index] = e.target.value;
    handleUpdate(newValue);

    if (e.target.value.length === MAX_LENGTH) {
      const nextSibling = document.querySelector(`input[id=name-${index + 1}]`);
      (nextSibling as HTMLElement)?.focus();
    }
  };

  const handleKeyDown = (index: number) => (e: React.KeyboardEvent<HTMLInputElement>) => {
    const target = e.target as HTMLInputElement;
    if (target.value.length === MIN_LENGTH && e.key === KEY_BACKSPACE) {
      const prevSibling = document.querySelector(`input[id=name-${index - 1}]`);
      (prevSibling as HTMLElement)?.focus();
    }
  };

  return (
    <ButtonGroup className={className} align={align}>
      {Array.from(Array(MAX_FIELDS)).map((_, index) => (
        <Input
          key={index}
          value={value[index]}
          width="code"
          name={`name-${index}`}
          onPaste={handlePaste}
          onChange={handleChange(index)}
          onKeyDown={handleKeyDown(index)}
        />
      ))}
    </ButtonGroup>
  );
};

InputCodeGroup.defaultProps = {
  align: 'center',
  className: '',
};

export default InputCodeGroup;
