import { Icon } from 'Components/base/Icon';
import { Text } from 'Components/base/typography/Text';
import { Logo } from 'Components/elements/Logo';
import classNames from 'classnames';
import React, { ReactNode } from 'react';

import style from './EntryInfoTitle.module.scss';

export interface EntryInfoTitleProps {
  name: string;
  category: string;
  showAvatar?: boolean;
  className?: string;
  avatarIcon?: ReactNode;
}

const EntryInfoTitle: React.FC<EntryInfoTitleProps> = ({
  name,
  category,
  showAvatar,
  className,
  avatarIcon,
}) => (
  <div className={classNames(style.component, className)}>
    {showAvatar && avatarIcon && (
      <span className={style.iconCircle}>
        <Icon icon={avatarIcon} path="grey-100" />
      </span>
    )}
    {showAvatar && !avatarIcon && (
      <Logo content="user" radius="rounded" size="24" name={name} className={style.logoWrapper} />
    )}
    <div className={style.text}>
      <Text type="caption-regular" color="grey-100">
        {category}
      </Text>
      <Text type="body-medium">{name}</Text>
    </div>
  </div>
);

export default EntryInfoTitle;
