import { Meta, Story } from '@storybook/react';
import {
  EntryInfoTitle as Component,
  EntryInfoTitleProps as Props,
} from 'Components/custom/Accounting/Journal/EntryInfoTitle';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Accounting/Journal/EntryInfoTitle',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const EntryInfoTitleComponent: Story<Props> = (args) => <Component {...args} />;

export const EntryInfoTitle = EntryInfoTitleComponent.bind({});
EntryInfoTitle.args = {
  name: 'Vitalii Kvasha',
  category: 'Created by',
  showAvatar: true,
};
