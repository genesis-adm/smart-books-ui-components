import { useDropDown } from 'Components/DropDown';
import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React from 'react';

import style from './AccountingCardLE.module.scss';

export interface AccountingCardLEProps {
  name: string;
  className?: string;
  onClick(): void;
}

const AccountingCardLE: React.FC<AccountingCardLEProps> = ({ name, className, onClick }) => {
  const [handleOpen] = useDropDown();
  const handleClick = () => {
    onClick();
    if (handleOpen) handleOpen();
  };
  return (
    <div
      className={classNames(style.component, className)}
      onClick={handleClick}
      role="presentation"
    >
      <div className={style.logo}>
        <Text type="body-medium" color="white-100">
          {name.slice(0, 2).toUpperCase()}
        </Text>
      </div>
      <div className={style.information}>
        <Text type="body-medium" color="inherit" noWrap>
          {name}
        </Text>
      </div>
    </div>
  );
};

export default AccountingCardLE;
