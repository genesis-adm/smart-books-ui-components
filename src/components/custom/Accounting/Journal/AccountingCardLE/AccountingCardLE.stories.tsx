import { Meta, Story } from '@storybook/react';
import {
  AccountingCardLE as Component,
  AccountingCardLEProps as Props,
} from 'Components/custom/Accounting/Journal/AccountingCardLE';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Accounting/Journal/AccountingCardLE',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const AccountingCardLEComponent: Story<Props> = (args) => <Component {...args} />;

export const AccountingCardLE = AccountingCardLEComponent.bind({});
AccountingCardLE.args = {
  name: 'Legal entity name',
};
