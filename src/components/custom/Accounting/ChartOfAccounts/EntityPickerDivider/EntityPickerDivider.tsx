import { Icon } from 'Components/base/Icon';
import { ReactComponent as ArrowsIcon } from 'Svg/v2/16/arrows-switch.svg';
import classNames from 'classnames';
import React from 'react';

import style from './EntityPickerDivider.module.scss';

export interface EntityPickerDividerProps {
  isActive: boolean;
  className?: string;
}

const EntityPickerDivider: React.FC<EntityPickerDividerProps> = ({ isActive, className }) => (
  <div className={classNames(style.component, className)}>
    <span className={style.line} />
    <div className={classNames(style.swapper, { [style.active]: isActive })}>
      <Icon className={style.icon} icon={<ArrowsIcon />} clickThrough path="inherit" />
    </div>
  </div>
);

export default EntityPickerDivider;
