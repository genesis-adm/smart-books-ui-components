import { Meta, Story } from '@storybook/react';
import {
  AccordionAccountItem as Component,
  AccordionAccountItemProps as Props,
} from 'Components/custom/Accounting/ChartOfAccounts/AccordionAccountItem';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Accounting/Chart Of Accounts/Accordion Account Item',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const AccordionAccountItemComponent: Story<Props> = (args) => <Component {...args} />;

export const AccordionAccountItem = AccordionAccountItemComponent.bind({});
AccordionAccountItem.args = {};
