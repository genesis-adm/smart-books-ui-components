import { DropDown } from 'Components/DropDown';
import { Icon } from 'Components/base/Icon';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as ChevronIcon } from 'Svg/v2/12/chevron-right.svg';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as DropdownIcon } from 'Svg/v2/16/three-dots.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import classNames from 'classnames';
import React, { MouseEvent, ReactNode, useState } from 'react';

import style from './AccordionAccountItem.module.scss';

export interface AccordionAccountItemProps {
  name: string;
  counter: number;
  isActive: boolean;
  isOpen?: boolean;
  editDisabled?: boolean;
  deleteDisabled?: boolean;
  hideActions?: boolean;
  addButtonShown?: boolean;
  icon?: ReactNode;
  className?: string;
  onClick(): void;
  onAddClick(): void;
  onEdit?(e: React.MouseEvent<HTMLDivElement>): void;
  onDelete?(e: React.MouseEvent<HTMLDivElement>): void;
  children?: ReactNode;
}

const AccordionAccountItem: React.FC<AccordionAccountItemProps> = ({
  name,
  counter,
  isActive,
  isOpen,
  editDisabled = false,
  deleteDisabled = false,
  addButtonShown = true,
  hideActions,
  icon,
  className,
  onClick,
  onAddClick,
  onEdit,
  onDelete,
  children,
}) => {
  const [isActionsOpen, setActionsOpen] = useState<boolean>(false);
  const handleAddClick = (e: MouseEvent<HTMLDivElement>) => {
    e.stopPropagation();
    onAddClick();
  };
  const handleCurrentState = (arg: boolean): void => {
    setActionsOpen(arg);
  };

  const handleEditClick = (e: MouseEvent<HTMLDivElement>) => {
    e.stopPropagation();
    onEdit && onEdit(e);
  };
  const handleDeleteClick = (e: MouseEvent<HTMLDivElement>) => {
    e.stopPropagation();
    onDelete && onDelete(e);
  };
  return (
    <div className={classNames(style.wrapper, className)}>
      <div
        className={classNames(style.item, {
          [style.active]: isActive,
          [style.actionsOpen]: isActionsOpen,
        })}
        onClick={onClick}
      >
        <div className={style.main}>
          <Icon
            className={classNames(style.arrow, { [style.active]: isActive })}
            icon={<ChevronIcon />}
            path="grey-100"
            clickThrough
            transition="inherit"
          />
          <Icon icon={icon} path="grey-100" clickThrough />
          <Text type="body-regular-14" color="grey-100">
            {name}
          </Text>
        </div>
        <Text
          className={classNames({
            [style.counter]: !hideActions,
          })}
          type="body-regular-14"
          color="grey-100"
        >
          {counter}
        </Text>
        {!hideActions && (
          <DropDown
            currentStateInfo={handleCurrentState}
            classNameOuter={style.actions}
            flexdirection="column"
            position="bottom-right"
            padding="8"
            control={({ handleOpen }) => (
              <IconButton
                icon={<DropdownIcon />}
                shape="square"
                size="24"
                onClick={(e: MouseEvent<HTMLDivElement>) => {
                  e.stopPropagation();
                  handleOpen();
                }}
                background="transparent"
                color="grey-100-violet-90"
              />
            )}
          >
            <DropDownButton
              disabled={editDisabled}
              size="160"
              icon={<EditIcon />}
              onClick={handleEditClick}
            >
              Edit
            </DropDownButton>
            <DropDownButton
              disabled={deleteDisabled}
              size="160"
              type="danger"
              icon={<TrashIcon />}
              onClick={handleDeleteClick}
            >
              Delete
            </DropDownButton>
          </DropDown>
        )}
        {addButtonShown && (
          <IconButton
            className={style.add}
            icon={<PlusIcon />}
            color="grey-100-white-100"
            background="transparent-violet-90"
            shape="square"
            size="24"
            onClick={handleAddClick}
          />
        )}
      </div>
      {isOpen && children}
    </div>
  );
};

export default AccordionAccountItem;
