import { Text } from 'Components/base/typography/Text';
import initials from 'Utils/initials';
import classNames from 'classnames';
import React from 'react';

import style from './LegalEntityTitle.module.scss';

export interface LegalEntityTitleProps {
  name: string;
  className?: string;
}

const LegalEntityTitle: React.FC<LegalEntityTitleProps> = ({ name, className }) => (
  <div className={classNames(style.component, className)}>
    <div className={style.logo}>
      <Text type="caption-regular" transform="uppercase" color="violet-90">
        {initials(name)}
      </Text>
    </div>
    <Text type="body-medium">{name}</Text>
  </div>
);

export default LegalEntityTitle;
