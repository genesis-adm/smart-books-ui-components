import { Meta, Story } from '@storybook/react';
import {
  LegalEntityTitle as Component,
  LegalEntityTitleProps as Props,
} from 'Components/custom/Accounting/ChartOfAccounts/LegalEntityTitle';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Accounting/Chart Of Accounts/LegalEntityTitle',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const LegalEntityTitleComponent: Story<Props> = (args) => <Component {...args} />;

export const LegalEntityTitle = LegalEntityTitleComponent.bind({});
LegalEntityTitle.args = {
  name: 'Legal Entity title',
};
