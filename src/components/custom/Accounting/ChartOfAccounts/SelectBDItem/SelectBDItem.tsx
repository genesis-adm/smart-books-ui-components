import { Icon } from 'Components/base/Icon';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as TickIcon } from 'Svg/v2/16/tick.svg';
import classNames from 'classnames';
import React from 'react';

import style from './SelectBDItem.module.scss';

export interface SelectBDItemProps {
  name: string;
  isSelected: boolean;
  onClick(): void;
}

const SelectBDItem: React.FC<SelectBDItemProps> = ({ name, isSelected, onClick }) => (
  <div onClick={onClick} className={classNames(style.component, { [style.selected]: isSelected })}>
    <Text type="caption-regular" color="inherit">
      {name}
    </Text>
    <Icon className={style.tick} icon={<TickIcon />} path="inherit" />
  </div>
);

export default SelectBDItem;
