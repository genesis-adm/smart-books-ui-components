import { Meta, Story } from '@storybook/react';
import {
  SelectBDItem as Component,
  SelectBDItemProps as Props,
} from 'Components/custom/Accounting/ChartOfAccounts/SelectBDItem';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Accounting/Chart Of Accounts/SelectBDItem',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const SelectBDItemComponent: Story<Props> = (args) => <Component {...args} />;

export const SelectBDItem = SelectBDItemComponent.bind({});
SelectBDItem.args = {
  name: 'Business Division name',
  isSelected: true,
};
