import { Icon } from 'Components/base/Icon';
import { Tooltip } from 'Components/base/Tooltip';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as TickIcon } from 'Svg/v2/16/tick.svg';
import initials from 'Utils/initials';
import classNames from 'classnames';
import React, { useEffect, useRef, useState } from 'react';

import style from './EntityPicker.module.scss';

export interface EntityPickerProps {
  name: string;
  isActive: boolean;
  className?: string;
  onClick(): void;
}

const EntityPicker: React.FC<EntityPickerProps> = ({ name, isActive, className, onClick }) => {
  const textPrimaryRef = useRef<HTMLDivElement>(null);
  const [tooltipValue, setTooltipValue] = useState<React.ReactNode>();

  useEffect(() => {
    if (
      textPrimaryRef.current &&
      textPrimaryRef.current.scrollWidth > textPrimaryRef.current.clientWidth
    ) {
      setTooltipValue(name);
    }
  }, [name]);

  return (
    <div
      className={classNames(style.component, { [style.active]: isActive }, className)}
      onClick={onClick}
    >
      <div className={style.wrapper}>
        <div className={style.logo}>
          <Text color="inherit" type="body-regular-14" transform="uppercase">
            {initials(name)}
          </Text>
        </div>
        <Tooltip message={tooltipValue}>
          <Text
            ref={textPrimaryRef}
            className={style.text}
            type="body-regular-16"
            color="inherit"
            noWrap
          >
            {name}
          </Text>
        </Tooltip>
      </div>
      <Icon className={style.tick} icon={<TickIcon />} path="inherit" clickThrough />
    </div>
  );
};
export default EntityPicker;
