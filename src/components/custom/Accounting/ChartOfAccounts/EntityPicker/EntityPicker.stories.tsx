import { Meta, Story } from '@storybook/react';
import {
  EntityPicker as Component,
  EntityPickerProps as Props,
} from 'Components/custom/Accounting/ChartOfAccounts/EntityPicker';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Accounting/Chart Of Accounts/Entity Picker',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const EntityPickerComponent: Story<Props> = (args) => <Component {...args} />;

export const EntityPicker = EntityPickerComponent.bind({});
EntityPicker.args = {
  name: 'Entity Name',
  isActive: true,
};
