import { Meta, Story } from '@storybook/react';
import {
  AccountClassType as Component,
  AccountClassTypeProps as Props,
} from 'Components/custom/Accounting/ChartOfAccounts/AccountClassType';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Accounting/Chart Of Accounts/Account Class Type',
  component: Component,
  argTypes: {
    className: hideProperty,
    onClick: hideProperty,
  },
} as Meta;

const AccountClassTypeComponent: Story<Props> = (args) => <Component {...args} />;

export const AccountClassType = AccountClassTypeComponent.bind({});
AccountClassType.args = {
  type: 'Assets',
  disabled: false,
};
