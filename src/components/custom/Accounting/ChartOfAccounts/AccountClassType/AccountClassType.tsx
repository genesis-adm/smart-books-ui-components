import { Icon } from 'Components/base/Icon';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as LiabliltiesIcon } from 'Svg/v2/32/bank-cheque.svg';
import { ReactComponent as IncomeIcon } from 'Svg/v2/32/money-in.svg';
import { ReactComponent as ExpensesIcon } from 'Svg/v2/32/money-out.svg';
import { ReactComponent as AssetIcon } from 'Svg/v2/32/money-stack.svg';
import { ReactComponent as EquityIcon } from 'Svg/v2/32/scales.svg';
import classNames from 'classnames';
import React from 'react';

import style from './AccountClassType.module.scss';

export interface AccountClassTypeProps {
  type: 'Assets' | 'Liabilities' | 'Equity' | 'Income' | 'Expenses';
  className?: string;
  disabled?: boolean;
  onClick(): void;
}

const AccountClassTypes = {
  Assets: {
    name: 'Assets',
    icon: <AssetIcon />,
  },
  Liabilities: {
    name: 'Liabilities',
    icon: <LiabliltiesIcon />,
  },
  Equity: {
    name: 'Equity',
    icon: <EquityIcon />,
  },
  Income: {
    name: 'Income',
    icon: <IncomeIcon />,
  },
  Expenses: {
    name: 'Expenses',
    icon: <ExpensesIcon />,
  },
};

const AccountClassType: React.FC<AccountClassTypeProps> = ({
  type,
  className,
  disabled,
  onClick,
}) => {
  const handleClick = () => {
    if (!disabled) onClick();
  };
  return (
    <div
      className={classNames(
        style.wrapper,
        {
          [style.active]: !disabled,
          [style.disabled]: disabled,
        },
        className,
      )}
      role="presentation"
      onClick={handleClick}
    >
      <div className={style.container}>
        <Icon className={style.icon} icon={AccountClassTypes[type].icon} path="inherit" />
      </div>
      <Text className={style.text} type="body-medium" color="inherit">
        {AccountClassTypes[type].name}
      </Text>
    </div>
  );
};

export default AccountClassType;
