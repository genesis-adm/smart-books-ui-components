import { DropDown } from 'Components/DropDown';
import { Icon } from 'Components/base/Icon';
import { Tooltip } from 'Components/base/Tooltip';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as BulletDotIcon } from 'Svg/v2/12/bullet-dot.svg';
import { ReactComponent as ChevronIcon } from 'Svg/v2/12/chevron-right.svg';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as DropdownIcon } from 'Svg/v2/16/three-dots.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import classNames from 'classnames';
import React, { Children, MouseEvent, ReactNode, useEffect, useRef, useState } from 'react';

import style from './AccordionAccountSubItem.module.scss';

export interface AccordionAccountSubItemProps {
  name: string;
  tooltip?: string;
  icon?: ReactNode;
  iconTooltip?: string;
  nestingLevel?: number;
  isActive: boolean;
  isOpen: boolean;
  addButtonShown?: boolean;
  hideActions?: boolean;
  editDisabled?: boolean;
  deleteDisabled?: boolean;
  className?: string;
  children?: ReactNode;

  onClick?(e: React.MouseEvent<HTMLDivElement>): void;

  onIconClick?(e: React.MouseEvent<HTMLDivElement>): void;

  onAddClick?(e: React.MouseEvent<HTMLDivElement>): void;

  onEdit?(e: React.MouseEvent<HTMLDivElement>): void;

  onDelete?(e: React.MouseEvent<HTMLDivElement>): void;
}

const AccordionAccountSubItem = ({
  name,
  tooltip,
  icon,
  iconTooltip,
  nestingLevel = 1,
  isActive,
  isOpen,
  addButtonShown = true,
  editDisabled = false,
  deleteDisabled = false,
  hideActions,
  className,
  children,
  onClick,
  onIconClick,
  onAddClick,
  onEdit,
  onDelete,
}: AccordionAccountSubItemProps): React.ReactElement => {
  const textRef = useRef<HTMLDivElement>(null);
  const countArray = !!Children.toArray(children).length;
  const [isActionsOpen, setActionsOpen] = useState<boolean>(false);

  const [tooltipValue, setTooltipValue] = useState<string>('');

  useEffect(() => {
    if (
      !tooltip &&
      textRef.current &&
      textRef.current.scrollHeight > textRef.current.clientHeight
    ) {
      setTooltipValue(name);
    }
  }, [tooltip, name]);

  const handleAddClick = (e: MouseEvent<HTMLDivElement>) => {
    e.stopPropagation();
    onAddClick && onAddClick(e);
  };
  const handleIconClick = (e: MouseEvent<HTMLDivElement>) => {
    e.stopPropagation();
    onIconClick && onIconClick(e);
  };
  const handleEditClick = (e: MouseEvent<HTMLDivElement>) => {
    e.stopPropagation();
    onEdit && onEdit(e);
  };
  const handleDeleteClick = (e: MouseEvent<HTMLDivElement>) => {
    e.stopPropagation();
    onDelete && onDelete(e);
  };
  const handleCurrentState = (arg: boolean): void => {
    setActionsOpen(arg);
  };
  return (
    <div className={classNames(style.wrapper, className)}>
      <div
        className={classNames(style.item, {
          [style.active]: isActive,
          [style.actionsOpen]: isActionsOpen,
        })}
        onClick={onClick}
        style={{ paddingLeft: `${nestingLevel * 12}px` }}
      >
        <IconButton
          onClick={handleIconClick}
          size="16"
          shape="circle"
          background="transparent-grey-20"
          color="inherit"
          className={classNames(style.icon, {
            [style.active]: isActive,
            [style.opened]: isOpen,
          })}
          icon={<Icon path="inherit" icon={countArray ? <ChevronIcon /> : <BulletDotIcon />} />}
        />
        {icon && (
          <Tooltip message={iconTooltip}>
            <Icon icon={icon} path="inherit" className={style.icon_main} />
          </Tooltip>
        )}
        <Tooltip wrapperClassName={style.name} message={tooltipValue}>
          <Text
            ref={textRef}
            type="body-regular-14"
            lineClamp="1"
            color="inherit"
            transition="unset"
          >
            {name}
          </Text>
        </Tooltip>
        {!hideActions && (
          <DropDown
            currentStateInfo={handleCurrentState}
            classNameOuter={style.actions}
            flexdirection="column"
            position="bottom-right"
            padding="8"
            control={({ handleOpen }) => (
              <IconButton
                icon={<DropdownIcon />}
                shape="square"
                size="24"
                onClick={(e: MouseEvent<HTMLDivElement>) => {
                  e.stopPropagation();
                  handleOpen();
                }}
                background="transparent"
                color="grey-100-violet-90"
              />
            )}
          >
            <DropDownButton
              disabled={editDisabled}
              size="160"
              icon={<EditIcon />}
              onClick={handleEditClick}
            >
              Edit
            </DropDownButton>
            <DropDownButton
              disabled={deleteDisabled}
              size="160"
              type="danger"
              icon={<TrashIcon />}
              onClick={handleDeleteClick}
            >
              Delete
            </DropDownButton>
          </DropDown>
        )}
        {addButtonShown && (
          <IconButton
            className={style.add}
            icon={<PlusIcon />}
            color="grey-100-white-100"
            background="transparent-violet-90"
            shape="square"
            size="24"
            onClick={handleAddClick}
          />
        )}
      </div>
      {isOpen && children}
    </div>
  );
};

export default AccordionAccountSubItem;
