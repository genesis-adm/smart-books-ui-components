import { Meta, Story } from '@storybook/react';
import {
  AccordionAccountSubItem as Component,
  AccordionAccountSubItemProps as Props,
} from 'Components/custom/Accounting/ChartOfAccounts/AccordionAccountSubItem';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Accounting/Chart Of Accounts/Accordion Account SubItem',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const AccordionAccountSubItemComponent: Story<Props> = (args) => <Component {...args} />;

export const AccordionAccountSubItem = AccordionAccountSubItemComponent.bind({});
AccordionAccountSubItem.args = {};
