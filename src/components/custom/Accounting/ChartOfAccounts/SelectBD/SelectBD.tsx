import { DropDown } from 'Components/DropDown';
import { Icon } from 'Components/base/Icon';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as ChevronIcon } from 'Svg/v2/16/chevron-down.svg';
import classNames from 'classnames';
import React, { ReactNode } from 'react';

import style from './SelectBD.module.scss';

export interface SelectBDProps {
  nameActiveDB: string;
  className?: string;
  children?: ReactNode;
}

const SelectBD: React.FC<SelectBDProps> = ({ nameActiveDB, className, children }) => (
  <DropDown
    flexdirection="column"
    padding="none"
    control={({ isOpen, handleOpen }) => (
      <div
        className={classNames(style.wrapper, { [style.active]: isOpen }, className)}
        onClick={handleOpen}
      >
        <Text className={style.title} type="body-medium" color="inherit">
          {nameActiveDB}
        </Text>
        <Icon className={style.icon} icon={<ChevronIcon />} path="inherit" />
      </div>
    )}
  >
    <Container customScroll className={style.list} flexdirection="column" gap="4">
      {children}
    </Container>
  </DropDown>
);

export default SelectBD;
