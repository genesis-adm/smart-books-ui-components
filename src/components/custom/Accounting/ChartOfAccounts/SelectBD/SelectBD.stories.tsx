import { Meta, Story } from '@storybook/react';
import {
  SelectBD as Component,
  SelectBDProps as Props,
} from 'Components/custom/Accounting/ChartOfAccounts/SelectBD';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Accounting/Chart Of Accounts/SelectBD',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const SelectBDComponent: Story<Props> = (args) => <Component {...args} />;

export const SelectBD = SelectBDComponent.bind({});
SelectBD.args = {
  nameActiveDB: 'Selected Business division',
};
