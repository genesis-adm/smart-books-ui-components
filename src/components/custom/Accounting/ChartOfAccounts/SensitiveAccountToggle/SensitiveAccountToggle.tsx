import { Icon } from 'Components/base/Icon';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as LockClosedIcon } from 'Svg/v2/16/lock-closed.svg';
import { ReactComponent as LockOpenedIcon } from 'Svg/v2/16/lock-opened.svg';
import classNames from 'classnames';
import React from 'react';

import style from './SensitiveAccountToggle.module.scss';

export interface SensitiveAccountToggleProps {
  isSensitive: boolean;
  disabled?: boolean;
  onClick(): void;
  className?: string;
}

const SensitiveAccountToggle: React.FC<SensitiveAccountToggleProps> = ({
  isSensitive,
  disabled,
  className,
  onClick,
}) => {
  const handleClick = () => {
    if (!disabled) onClick();
  };
  return (
    <div
      className={classNames(
        style.component,
        { [style.sensitive]: isSensitive, [style.disabled]: disabled },
        className,
      )}
    >
      <Text type="caption-regular" color="grey-100">
        {isSensitive ? 'Sensitive account' : 'Non-sensitive account'}
      </Text>
      <div className={style.toggle} onClick={handleClick}>
        <div className={style.switcher}>
          <Icon
            icon={isSensitive ? <LockClosedIcon /> : <LockOpenedIcon />}
            path={isSensitive ? 'violet-90' : 'grey-100'}
          />
        </div>
      </div>
    </div>
  );
};

export default SensitiveAccountToggle;
