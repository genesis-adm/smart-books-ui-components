import { Meta, Story } from '@storybook/react';
import {
  SensitiveAccountToggle as Component,
  SensitiveAccountToggleProps as Props,
} from 'Components/custom/Accounting/ChartOfAccounts/SensitiveAccountToggle';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Accounting/Chart Of Accounts/SensitiveAccountToggle',
  component: Component,
  argTypes: {
    className: hideProperty,
    onClick: hideProperty,
  },
} as Meta;

const SensitiveAccountToggleComponent: Story<Props> = (args) => <Component {...args} />;

export const SensitiveAccountToggle = SensitiveAccountToggleComponent.bind({});
SensitiveAccountToggle.args = {
  isSensitive: true,
  disabled: false,
};
