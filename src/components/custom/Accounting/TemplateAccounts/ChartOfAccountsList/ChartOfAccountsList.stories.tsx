import { Meta, Story } from '@storybook/react';
import {
  ChartOfAccountsList as Component,
  ChartOfAccountsListProps as Props,
} from 'Components/custom/Accounting/TemplateAccounts/ChartOfAccountsList';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Accounting/Chart Of Accounts/ChartOfAccountsList',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const ChartOfAccountsListComponent: Story<Props> = (args) => <Component {...args} />;

export const ChartOfAccountsList = ChartOfAccountsListComponent.bind({});
ChartOfAccountsList.args = {};
