import { DropDown } from 'Components/DropDown';
import { Tooltip } from 'Components/base/Tooltip';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React, { ReactElement, ReactNode } from 'react';

import style from './ChartOfAccountsList.module.scss';

export interface ChartOfAccountsListProps {
  chartOfAccountsQuantity: number;
  children?: ReactNode;
}

const ChartOfAccountsList = ({
  chartOfAccountsQuantity,
  children,
}: ChartOfAccountsListProps): ReactElement => (
  <DropDown
    flexdirection="column"
    padding="16"
    control={({ isOpen, handleOpen }) => {
      const handleClick = (e: React.MouseEvent<HTMLDivElement>) => {
        e.stopPropagation();
        handleOpen();
      };
      return (
        <Tooltip
          message={!isOpen && 'Show all chart of accounts'}
          onClick={handleClick}
          wrapperClassName={classNames(style.wrapper, { [style.active]: isOpen })}
        >
          <Text color="inherit" transition="unset">
            {chartOfAccountsQuantity}
          </Text>
        </Tooltip>
      );
    }}
  >
    <Container className={style.list} customScroll flexdirection="column" gap="12">
      {children}
    </Container>
  </DropDown>
);

export default ChartOfAccountsList;
