import { Tooltip } from 'Components/base/Tooltip';
import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React, { ReactElement } from 'react';

import style from './CurrencyList.module.scss';

export interface CurrencyListProps {
  currencies: string[];
  className?: string;
}

const CurrencyList = ({ currencies, className }: CurrencyListProps): ReactElement => (
  <Tooltip
    message={currencies.length > 1 && currencies.join(' | ')}
    wrapperClassName={classNames(style.wrapper, className)}
  >
    <Text color="inherit">{currencies[0]}</Text>
    {currencies.length > 1 && (
      <>
        <span className={style.divider} />
        <Text color="inherit">+{currencies.length - 1}</Text>
      </>
    )}
  </Tooltip>
);

export default CurrencyList;
