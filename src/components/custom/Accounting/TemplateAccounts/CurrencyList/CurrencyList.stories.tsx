import { Meta, Story } from '@storybook/react';
import {
  CurrencyList as Component,
  CurrencyListProps as Props,
} from 'Components/custom/Accounting/TemplateAccounts/CurrencyList';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Accounting/Chart Of Accounts/CurrencyList',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const CurrencyListComponent: Story<Props> = (args) => <Component {...args} />;

export const CurrencyList = CurrencyListComponent.bind({});
CurrencyList.args = {
  currencies: ['eur', 'usd', 'uah'],
};
