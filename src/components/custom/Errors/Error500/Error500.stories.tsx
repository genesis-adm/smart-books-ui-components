import { Meta, Story } from '@storybook/react';
import { Error500 as Component, Error500Props as Props } from 'Components/custom/Errors/Error500';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Errors/Error500',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const Error500Component: Story<Props> = (args) => <Component {...args} />;

export const Error500 = Error500Component.bind({});
Error500.args = {};
