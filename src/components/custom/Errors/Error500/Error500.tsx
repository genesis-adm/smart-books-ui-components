import { Button } from 'Components/base/buttons/Button';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as ErrorImage } from 'Svg/500.svg';
import React, { ReactElement } from 'react';

import style from './Error500.module.scss';
import { Error500Props } from './Error500.types';

// Reminder: do not forget to add the following code to root index.ts
// export { Error500 } from './components/custom/Errors/Error500';

const Error500 = ({ onClick }: Error500Props): ReactElement => (
  <div className={style.container}>
    <ErrorImage />
    <div className={style.container_text}>
      <Text type="title-bold">Internal server error</Text>
      <Text type="body-regular-16" color="grey-100" align="center">
        We are fixing the problem. Please try again at a later stage.
      </Text>
    </div>
    <Button width="180" height="40" background="violet-90" onClick={onClick}>
      Reload
    </Button>
  </div>
);

export default Error500;
