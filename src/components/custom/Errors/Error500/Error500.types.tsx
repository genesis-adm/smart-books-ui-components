export interface Error500Props {
  className?: string;
  onClick(): void;
}
