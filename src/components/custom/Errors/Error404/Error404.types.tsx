export interface Error404Props {
  className?: string;
  onClick(): void;
}
