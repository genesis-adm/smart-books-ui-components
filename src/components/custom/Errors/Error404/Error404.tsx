import { Button } from 'Components/base/buttons/Button';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as ErrorImage } from 'Svg/404.svg';
import React, { ReactElement } from 'react';

import style from './Error404.module.scss';
import { Error404Props } from './Error404.types';

// Reminder: do not forget to add the following code to root index.ts
// export { Error404 } from './components/custom/Errors/Error404';

const Error404 = ({ onClick }: Error404Props): ReactElement => (
  <div className={style.container}>
    <ErrorImage />
    <div className={style.container_text}>
      <Text type="title-bold">Page Not Found</Text>
      <Text type="body-regular-16" color="grey-100" align="center">
        The page you&apos;re looking for doesn&apos;t seem to exist
      </Text>
    </div>
    <Button width="180" height="40" background="violet-90" onClick={onClick}>
      Reload
    </Button>
  </div>
);

export default Error404;
