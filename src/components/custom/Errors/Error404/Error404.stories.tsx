import { Meta, Story } from '@storybook/react';
import { Error404 as Component, Error404Props as Props } from 'Components/custom/Errors/Error404';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Errors/Error404',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const Error404Component: Story<Props> = (args) => <Component {...args} />;

export const Error404 = Error404Component.bind({});
Error404.args = {};
