import { Meta, Story } from '@storybook/react';
import { Error403 as Component, Error403Props as Props } from 'Components/custom/Errors/Error403';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Errors/Error403',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const Error403Component: Story<Props> = (args) => <Component {...args} />;

export const Error403 = Error403Component.bind({});
Error403.args = {};
