import { Text } from 'Components/base/typography/Text';
import { ReactComponent as ErrorImage } from 'Svg/403.svg';
import React, { ReactElement } from 'react';

import style from './Error403.module.scss';
import { Error403Props } from './Error403.types';

// Reminder: do not forget to add the following code to root index.ts
// export { Error403 } from './components/custom/errors/Error403';

const Error403 = ({ className }: Error403Props): ReactElement => (
  <div className={style.container}>
    <ErrorImage />
    <div className={style.container_text}>
      <Text type="title-bold">Access Denied</Text>
      <Text type="body-regular-16" color="grey-100" align="center">
        The page you&apos;re trying to access has restricted access. Please refer to your system
        administrator.
      </Text>
    </div>
  </div>
);

export default Error403;
