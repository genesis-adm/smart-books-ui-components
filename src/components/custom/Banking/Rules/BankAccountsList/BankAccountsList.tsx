import { DropDown } from 'Components/DropDown';
import Icon from 'Components/base/Icon/Icon';
import { Tooltip } from 'Components/base/Tooltip';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import handleAccountLength from 'Utils/handleAccountLength';
import handleStringLength from 'Utils/handleStringLength';
import classNames from 'classnames';
import React from 'react';

import style from './BankAccountsList.module.scss';
import { BankAccountsListProps } from './BankAccountsList.types';

const BankAccountsList: React.FC<BankAccountsListProps> = ({ bankAccounts, iconStaticColor }) => {
  if (bankAccounts.length === 0) {
    return <Text color="grey-100">None</Text>;
  }
  if (bankAccounts.length === 1) {
    const [{ bankName, accountNumber, currency, icon }] = bankAccounts;
    return (
      <Tooltip
        message={
          handleAccountLength(accountNumber).includes('******') ? (
            <Container flexdirection="column">
              <Text color="white-100" style={{ wordBreak: 'break-all' }}>
                {bankName}
              </Text>
              <Text color="white-100" style={{ wordBreak: 'break-all' }}>
                {accountNumber} | {currency}
              </Text>
            </Container>
          ) : (
            ''
          )
        }
        wrapperClassName={classNames(style.wrapper, style.simple)}
      >
        {icon && <Icon icon={icon} path="inherit" staticColor={iconStaticColor} />}
        <Text color="inherit">
          {handleStringLength(bankName, 18)} | {handleAccountLength(accountNumber)} | {currency}
        </Text>
      </Tooltip>
    );
  }
  return (
    <DropDown
      flexdirection="column"
      padding="8"
      control={({ isOpen, handleOpen }) => (
        <Tooltip
          message={isOpen ? '' : 'Show all bank accounts'}
          onClick={handleOpen}
          wrapperClassName={classNames(style.wrapper, style.multiple, { [style.active]: isOpen })}
          cursor="pointer"
        >
          <Text color="inherit">
            {handleStringLength(bankAccounts[0].bankName, 18)} |&nbsp;
            {handleAccountLength(bankAccounts[0].accountNumber)} | {bankAccounts[0].currency}
          </Text>
          <span className={style.divider} />
          <Text color="inherit">+{bankAccounts.length - 1}</Text>
        </Tooltip>
      )}
    >
      <Text className={style.total} color="grey-100">
        Total bank accounts: {bankAccounts.length}
      </Text>
      <ul className={style.list}>
        {bankAccounts.map(({ bankName, accountNumber, currency }, index) => (
          <div className={style.item} key={index}>
            <Text>{bankName}</Text>
            <Text color="grey-100">
              {currency} | {handleAccountLength(accountNumber)}
            </Text>
          </div>
        ))}
      </ul>
    </DropDown>
  );
};

export default BankAccountsList;
