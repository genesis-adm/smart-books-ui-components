import React from 'react';

export interface BankAccountType {
  bankName: string;
  accountNumber: string;
  currency: string;
  icon?: React.ReactNode;
}

export interface BankAccountsListProps {
  bankAccounts: BankAccountType[];
  iconStaticColor?: boolean;
}
