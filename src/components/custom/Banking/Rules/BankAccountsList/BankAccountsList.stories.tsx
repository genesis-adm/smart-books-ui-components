import { Meta, Story } from '@storybook/react';
import {
  BankAccountsList as Component,
  BankAccountsListProps as Props,
} from 'Components/custom/Banking/Rules/BankAccountsList';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Banking/BankAccountsList',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const BankAccountsListComponent: Story<Props> = (args) => <Component {...args} />;

export const BankAccountsList = BankAccountsListComponent.bind({});
BankAccountsList.args = {
  bankAccounts: [
    { bankName: 'Bank of America', accountNumber: 'GB36BARC20032684883328', currency: 'USD' },
  ],
};
