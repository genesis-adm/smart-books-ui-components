import { Meta, Story } from '@storybook/react';
import {
  RuleSplit as Component,
  RuleSplitProps as Props,
} from 'Components/custom/Banking/RuleSplit';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Banking/Rule Split',
  component: Component,
  argTypes: {
    name: hideProperty,
    className: hideProperty,
    onChange: hideProperty,
    onDelete: hideProperty,
  },
} as Meta;

const RuleSplitComponent: Story<Props> = ({ ...args }) => <Component {...args} />;

export const RuleSplit = RuleSplitComponent.bind({});
RuleSplit.args = {
  type: 'division',
  info: 'Information field',
};
