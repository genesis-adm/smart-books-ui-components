import { Icon } from 'Components/base/Icon';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import classNames from 'classnames';
import React from 'react';

import style from './RuleSplit.module.scss';

export interface RuleSplitProps {
  type: 'division' | 'counterparty' | 'thirdparty';
  showDeleteButton?: boolean;
  info?: string;
  className?: string;
  children?: React.ReactNode;
  onDelete?(): void;
}

const RuleSplit: React.FC<RuleSplitProps> = ({
  type,
  showDeleteButton,
  info,
  className,
  onDelete,
  children,
}) => (
  <div className={classNames(style.wrapper, className)}>
    <div className={classNames(style.inputs, style[`type_${type}`])}>
      {children}
      {showDeleteButton && (
        <Icon className={style.delete} icon={<TrashIcon />} onClick={onDelete} />
      )}
    </div>
    {info && (
      <Text display="block" className={style.info} color="grey-100">
        {info}
      </Text>
    )}
  </div>
);

export default RuleSplit;
