import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React from 'react';

import style from './BankingRulesAmount.module.scss';

export interface BankingRulesAmountProps {
  quantity: number;
  className?: string;
}

const BankingRulesAmount: React.FC<BankingRulesAmountProps> = ({ quantity, className }) => {
  if (quantity < 1) {
    return (
      <Text type="caption-regular" color="black-100">
        None
      </Text>
    );
  }
  return (
    <div className={classNames(style.component, className)}>
      <Text type="caption-semibold" color="violet-90" noWrap>
        {quantity}
      </Text>
    </div>
  );
};

export default BankingRulesAmount;
