import { Meta, Story } from '@storybook/react';
import {
  BankingRulesAmount as Component,
  BankingRulesAmountProps as Props,
} from 'Components/custom/Banking/BankingRulesAmount';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Banking/Banking Rules Amount',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const BankingRulesAmountComponent: Story<Props> = (args) => <Component {...args} />;

export const BankingRulesAmount = BankingRulesAmountComponent.bind({});
BankingRulesAmount.args = {
  quantity: 1,
};
