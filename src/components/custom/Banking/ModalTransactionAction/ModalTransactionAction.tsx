import { Button } from 'Components/base/buttons/Button';
import { ButtonGroup } from 'Components/base/buttons/ButtonGroup';
import { Modal, ModalFooter, ModalHeader } from 'Components/modules/Modal';
import React from 'react';

export interface ModalTransactionActionProps {
  height?: '495' | '690' | 'minimal';
  size?: 'full' | 'xs' | 'sm' | 'md-narrow' | 'md-wide' | 'md-extrawide' | 'lg' | 'xl';
  title?: string;
  titleActionButton: string;
  titleCancelButton: string;
  children?: React.ReactNode;
  onClose(): void;
  onAction(): void;
  onCancel(): void;
}

const ModalTransactionAction: React.FC<ModalTransactionActionProps> = ({
  height = '495',
  size = 'md-narrow',
  title,
  titleActionButton,
  titleCancelButton,
  children,
  onClose,
  onAction,
  onCancel,
}) => (
  <Modal
    size={size}
    centered
    overlay="black-100"
    height={height}
    header={
      <ModalHeader
        backgroundColor="grey-10"
        headerHeight="medium"
        title={title}
        titlePosition="center"
        onClick={onClose}
      />
    }
    footer={
      <ModalFooter justifycontent="center">
        <ButtonGroup align="center">
          <Button
            width="md"
            height="large"
            background="grey-20"
            color="black-100"
            border="grey-20-grey-90"
            onClick={onCancel}
          >
            {titleCancelButton}
          </Button>
          <Button width="md" height="large" onClick={onAction}>
            {titleActionButton}
          </Button>
        </ButtonGroup>
      </ModalFooter>
    }
  >
    {children}
  </Modal>
);

export default ModalTransactionAction;
