import { Meta, Story } from '@storybook/react';
import {
  CategoryOfMatch as Component,
  CategoryOfMatchProps as Props,
} from 'Components/custom/Banking/CategoryOfMatch';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Banking/CategoryOfMatch',
  component: Component,
  argTypes: {
    className: hideProperty,
    onClick: hideProperty,
  },
} as Meta;

const CategoryOfMatchComponent: Story<Props> = (args) => <Component {...args} />;

export const CategoryOfMatch = CategoryOfMatchComponent.bind({});
CategoryOfMatch.args = {
  chartOfAccount: 'Meals & entertainment',
  businessDivision: 'BetterME',
  counterValue: '+2',
};
