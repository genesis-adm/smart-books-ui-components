import { Counter } from 'Components/base/Counter';
import { Tag } from 'Components/base/Tag';
import { Tooltip } from 'Components/base/Tooltip';
import { Button } from 'Components/base/buttons/Button';
import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React, { SyntheticEvent } from 'react';

import style from './CategoryOfMatch.module.scss';

export interface CategoryOfMatchProps {
  chartOfAccount: string;
  businessDivision: string;
  counterValue?: string;
  className?: string;
  onSplitOpen?: (e: SyntheticEvent) => void;
  ruleName?: string;
  onAddCategory?: (e: SyntheticEvent) => void;
}

const CategoryOfMatch: React.FC<CategoryOfMatchProps> = ({
  chartOfAccount,
  businessDivision,
  counterValue,
  className,
  onSplitOpen,
  ruleName,
  onAddCategory,
}) => {
  const splitAvailable = !!chartOfAccount && !!businessDivision;
  return (
    <>
      {!splitAvailable && (
        <Button
          height="24"
          background="red-10-red-20"
          color="red-90"
          onClick={onAddCategory}
          width="full"
          font="text-medium"
        >
          Add category
        </Button>
      )}
      {splitAvailable && (
        <div className={style.container}>
          {ruleName && (
            <Tag
              text="Rule"
              font="subtext-regular"
              tooltip={ruleName}
              cursor="default"
              color="violetFilled"
              size="16"
              padding="0-4"
            />
          )}
          <Tooltip
            message="Open split"
            wrapperClassName={classNames(style.wrapper, className)}
            onClick={onSplitOpen}
          >
            <Text
              className={classNames(style.text, style.category)}
              type="text-medium"
              color="inherit"
              noWrap
            >
              {chartOfAccount}
            </Text>
            <div className={style.counterWrapper}>
              <Text
                className={classNames(style.text, style.company)}
                type="text-medium"
                color="inherit"
                noWrap
              >
                {businessDivision}
              </Text>
              {counterValue && <Counter className={style.counter} value={counterValue} />}
            </div>
          </Tooltip>
        </div>
      )}
    </>
  );
};

export default CategoryOfMatch;
