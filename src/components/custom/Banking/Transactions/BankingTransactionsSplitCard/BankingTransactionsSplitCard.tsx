import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';

export interface BankingTransactionsSplitCardProps {
  splitType: string;
  businessUnitName: string;
  legalEntity: string;
  accountName: string;
  accountClass: string;
  amount: string;
  isPositive: boolean;
  className?: string;
}

const BankingTransactionsSplitCard: React.FC<BankingTransactionsSplitCardProps> = ({
  splitType,
  businessUnitName,
  legalEntity,
  accountName,
  accountClass,
  amount,
  isPositive,
  className,
}) => (
  <Container
    background="grey-10"
    className={classNames(spacing.p24, spacing.w100p, className)}
    justifycontent="space-between"
    alignitems="center"
    radius
  >
    <Container flexdirection="column">
      <Text color="grey-100">{splitType}</Text>
      <Text type="body-medium">{businessUnitName}</Text>
      <Text className={spacing.mt16} color="grey-100">
        Legal entity:&nbsp;
        <Text display="inline-flex">{legalEntity}</Text>
      </Text>
      <Text className={spacing.mt8} color="grey-100">
        Account name:&nbsp;
        <Text display="inline-flex">{accountName}</Text>
      </Text>
      <Text className={spacing.mt8} color="grey-100">
        Account class:&nbsp;
        <Text display="inline-flex">{accountClass}</Text>
      </Text>
    </Container>
    <Text type="body-medium" color={isPositive ? 'green-90' : 'red-90'}>
      {amount}
    </Text>
  </Container>
);

export default BankingTransactionsSplitCard;
