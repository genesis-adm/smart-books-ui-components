import { Meta, Story } from '@storybook/react';
import {
  BankingTransactionsSplitCard as Component,
  BankingTransactionsSplitCardProps as Props,
} from 'Components/custom/Banking/Transactions/BankingTransactionsSplitCard';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Banking/Transactions/BankingTransactionsSplitCard',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const BankingTransactionsSplitCardComponent: Story<Props> = (args) => <Component {...args} />;

export const BankingTransactionsSplitCard = BankingTransactionsSplitCardComponent.bind({});
BankingTransactionsSplitCard.args = {
  splitType: 'Business division',
  businessUnitName: 'Genesis Media & Mobile Application',
  accountName: 'Direct advertising',
  accountClass: 'Revenue',
  amount: '+ $ 821,66',
  isPositive: true,
};
