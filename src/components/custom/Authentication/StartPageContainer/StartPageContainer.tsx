import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Text } from 'Components/base/typography/Text';
import { TextSlider } from 'Components/base/typography/TextSlider';
import { ReactComponent as SmartBooksLogoIcon } from 'Svg/v2/logo/smartbooks.svg';
import classNames from 'classnames';
import { useLottie } from 'lottie-react';
import React, { CSSProperties } from 'react';

import Waves from '../../../../assets/animations/waves.json';
import style from './StartPageContainer.module.scss';

export interface StartPageContainerProps {
  page: 'signIn' | 'signUp';
  title?: string;
  className?: string;
  onPrivacyPolicyClick?(): void;
  onTermsOfUseClick?(): void;
  children: React.ReactNode;
}

const StartPageContainer: React.FC<StartPageContainerProps> = ({
  page,
  title,
  className,
  onPrivacyPolicyClick,
  onTermsOfUseClick,
  children,
}) => {
  const styles: CSSProperties = {
    height: '100%',
    position: 'absolute',
    transform: 'rotate(90deg)',
    top: 40,
    left: 100,
  };
  const { View } = useLottie({ animationData: Waves }, styles);
  return (
    <div className={classNames(style.wrapper, className)}>
      <header className={style.header}>
        <SmartBooksLogoIcon className={style.logo} />
        <nav className={style.links}>
          {!!onPrivacyPolicyClick && (
            <LinkButton color="white-100" onClick={onPrivacyPolicyClick}>
              Privacy Policy
            </LinkButton>
          )}
          {!!onTermsOfUseClick && (
            <LinkButton color="white-100" onClick={onTermsOfUseClick}>
              Terms of Use
            </LinkButton>
          )}
        </nav>
      </header>
      <main className={style.main}>
        <div className={style.promo}>
          {View}
          {page === 'signIn' && (
            <span className={style.text}>
              Easy Way to
              <br />
              <TextSlider
                content={['Financial Accounting', 'Sales & Expenses', 'Banking & Paidlogs']}
              />
            </span>
          )}
          {page === 'signUp' && <span className={style.text}>Welcome on board 😎</span>}
        </div>
        <div className={style.modal}>
          {title && <h3 className={style.title}>{title}</h3>}
          {children}
        </div>
      </main>
      <footer className={style.footer}>
        <Text type="text-regular" color="inherit">
          © 2020-
          {new Date().getFullYear()}
          &nbsp;SmartBooks Inc. All rights reserved.
        </Text>
      </footer>
    </div>
  );
};

export default StartPageContainer;
