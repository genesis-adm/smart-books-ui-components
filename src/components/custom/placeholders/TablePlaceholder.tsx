import { ContentLoader } from 'Components/base/ContentLoader';
import { Container } from 'Components/base/grid/Container';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

type TablePlaceholderProps = {
  paddingX?: 'none' | '24';
  paddingY?: 'none' | '24';
};
const TablePlaceholder = ({ paddingX = '24', paddingY = '24' }: TablePlaceholderProps) => {
  return (
    <Container
      className={classNames(spacing.pb_auto, {
        [spacing.pX24]: paddingX === '24',
        [spacing.pY24]: paddingY === '24',
      })}
      gap="8"
      flexdirection="column"
    >
      <ContentLoader height="20px" isLoading />
      <ContentLoader height="60px" isLoading />
      <ContentLoader height="60px" isLoading />
      <ContentLoader height="60px" isLoading />
      <ContentLoader height="60px" isLoading />
    </Container>
  );
};

export default TablePlaceholder;
