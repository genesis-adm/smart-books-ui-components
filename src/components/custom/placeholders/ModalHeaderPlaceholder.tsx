import { ContentLoader } from 'Components/base/ContentLoader';
import { Container } from 'Components/base/grid/Container';
import classNames from 'classnames';
import React, { ReactElement, ReactNode } from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

type TitleAlignment = 'left' | 'center';
type ModalHeaderPlaceholderProps = {
  titleAlignment?: TitleAlignment;
  height?: '40' | '60' | '72' | '80';
  border?: boolean;
};

const ModalHeaderPlaceholder = ({
  titleAlignment = 'left',
  height = '80',
  border,
}: ModalHeaderPlaceholderProps): ReactElement => {
  const loader: Record<TitleAlignment, ReactNode> = {
    left: (
      <>
        <ContentLoader height="28px" width="421px" isLoading />
        <ContentLoader height="24px" width="24px" type="circle" isLoading />
      </>
    ),
    center: (
      <>
        <Container justifycontent="center" className={spacing.w100p}>
          <ContentLoader isLoading height="32px" width="360px" />
        </Container>
        <Container justifycontent="flex-end" gap="16">
          <ContentLoader height="24px" width="24px" type="circle" isLoading />
        </Container>
      </>
    ),
  };

  return (
    <Container
      justifycontent="space-between"
      alignitems="center"
      className={classNames(spacing.w100p, spacing.pX24, {
        [spacing[`h${height}fixed`]]: !!height,
      })}
      style={{ borderBottom: border ? '1px solid #edeff1' : undefined, flexShrink: 0 }}
    >
      {loader[titleAlignment]}
    </Container>
  );
};

export default ModalHeaderPlaceholder;
