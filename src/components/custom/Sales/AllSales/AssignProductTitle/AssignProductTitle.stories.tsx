import { Meta, Story } from '@storybook/react';
import {
  AssignProductTitle as Component,
  AssignProductTitleProps as Props,
} from 'Components/custom/Sales/AllSales/AssignProductTitle';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Sales/All Sales/Assign Product Title',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const AssignProductTitleComponent: Story<Props> = (args) => <Component {...args} />;

export const AssignProductTitle = AssignProductTitleComponent.bind({});
AssignProductTitle.args = {
  name: 'Google Irelend limited',
  type: 'Customer',
};
