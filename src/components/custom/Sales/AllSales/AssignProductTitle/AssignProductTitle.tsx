import { Divider } from 'Components/base/Divider';
import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React from 'react';

import style from './AssignProductTitle.module.scss';

export interface AssignProductTitleProps {
  type: string;
  name: string;
  className?: string;
}

const AssignProductTitle: React.FC<AssignProductTitleProps> = ({ type, name, className }) => (
  <div className={classNames(style.wrapper, className)}>
    <Divider height="20" type="vertical" background="grey-90" />
    <div className={style.text}>
      <Text color="grey-100" type="caption-regular" noWrap>
        {type}
      </Text>
      <Text type="body-medium" noWrap>
        {name}
      </Text>
    </div>
  </div>
);

export default AssignProductTitle;
