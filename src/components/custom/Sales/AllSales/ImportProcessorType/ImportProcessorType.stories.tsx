import { Meta, Story } from '@storybook/react';
import {
  ImportProcessorType as Component,
  ImportProcessorTypeProps as Props,
} from 'Components/custom/Sales/AllSales/ImportProcessorType';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Sales/All Sales/Import Processor Type',
  component: Component,
  argTypes: {
    className: hideProperty,
    onClick: hideProperty,
  },
} as Meta;

const ImportProcessorTypeComponent: Story<Props> = (args) => <Component {...args} />;

export const ImportProcessorType = ImportProcessorTypeComponent.bind({});
ImportProcessorType.args = {
  type: 'apple',
  disabled: false,
};
