import { Icon } from 'Components/base/Icon';
import { Text } from 'Components/base/typography/Text';
import { ImportProcessor } from 'Components/custom/Sales/AllSales/ImportProcessorType/ImportProcessorType.types';
import { ReactComponent as AdMobLogo } from 'Svg/v2/32/logo-admob.svg';
import { ReactComponent as AdSenseLogo } from 'Svg/v2/32/logo-adsense.svg';
import { ReactComponent as AdWordsLogo } from 'Svg/v2/32/logo-adwords.svg';
import { ReactComponent as AdXLogo } from 'Svg/v2/32/logo-adx.svg';
import { ReactComponent as AppleLogo } from 'Svg/v2/32/logo-apple.svg';
import { ReactComponent as FacebookLogo } from 'Svg/v2/32/logo-facebook.svg';
import { ReactComponent as GooglePlayLogo } from 'Svg/v2/32/logo-googleplay.svg';
import classNames from 'classnames';
import React from 'react';

import style from './ImportProcessorType.module.scss';

export interface ImportProcessorTypeProps {
  type: ImportProcessor;
  className?: string;
  disabled?: boolean;
  onClick(): void;
}

const ImportProcessorTypes = {
  apple: {
    name: 'Apple',
    icon: <AppleLogo />,
  },
  adX: {
    name: 'Google AdX',
    icon: <AdXLogo />,
  },
  adMob: {
    name: 'Google AdMob',
    icon: <AdMobLogo />,
  },
  facebook: {
    name: 'Facebook ads',
    icon: <FacebookLogo />,
  },
  adwords: {
    name: 'Google Adwords',
    icon: <AdWordsLogo />,
  },
  adsense: {
    name: 'Google AdSense',
    icon: <AdSenseLogo />,
  },
  googlePlay: {
    name: 'Google Play',
    icon: <GooglePlayLogo />,
  },
};

const ImportProcessorType: React.FC<ImportProcessorTypeProps> = ({
  type,
  className,
  disabled,
  onClick,
}) => {
  const handleClick = () => {
    if (!disabled) onClick();
  };
  return (
    <div
      className={classNames(
        style.wrapper,
        {
          [style.active]: !disabled,
          [style.disabled]: disabled,
        },
        className,
      )}
      role="presentation"
      onClick={handleClick}
    >
      <div className={style.container}>
        <Icon className={style.icon} icon={ImportProcessorTypes[type].icon} transition="inherit" />
      </div>
      <Text className={style.text} type="body-medium" color="inherit">
        {ImportProcessorTypes[type].name}
      </Text>
    </div>
  );
};

export default ImportProcessorType;
