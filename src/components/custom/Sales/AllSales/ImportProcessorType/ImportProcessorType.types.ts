export type ImportProcessor =
  | 'apple'
  | 'adX'
  | 'adMob'
  | 'facebook'
  | 'adwords'
  | 'adsense'
  | 'googlePlay';
