import { Counter } from 'Components/base/Counter';
import { Tooltip } from 'Components/base/Tooltip';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React from 'react';

import style from './ProductCell.module.scss';

export interface ProductCellProps {
  title: string;
  subtitle: string;
  counter?: number;
  tooltip?: string;
  className?: string;
}

const ProductCell: React.FC<ProductCellProps> = ({
  title,
  subtitle,
  counter,
  tooltip,
  className,
}) => (
  <Tooltip wrapperClassName={classNames(style.wrapper, className)} message={tooltip}>
    <Text className={style.title} color="inherit">
      {title}
    </Text>
    <Container alignitems="center" gap="4">
      <Text className={style.subtitle} color="inherit">
        {subtitle}
      </Text>
      {counter && <Counter value={`+${counter}`} />}
    </Container>
  </Tooltip>
);

export default ProductCell;
