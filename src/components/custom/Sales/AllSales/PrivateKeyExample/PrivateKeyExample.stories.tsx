import { Meta, Story } from '@storybook/react';
import {
  PrivateKeyExample as Component,
  PrivateKeyExampleProps as Props,
} from 'Components/custom/Sales/AllSales/PrivateKeyExample';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Sales/All Sales/PrivateKeyExample',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const PrivateKeyExampleComponent: Story<Props> = (args) => <Component {...args} />;

export const PrivateKeyExample = PrivateKeyExampleComponent.bind({});
PrivateKeyExample.args = {};
