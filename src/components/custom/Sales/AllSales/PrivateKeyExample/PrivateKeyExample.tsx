import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as ChevronIcon } from 'Svg/v2/12/chevron-up.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import style from './PrivateKeyExample.module.scss';

export interface PrivateKeyExampleProps {
  className?: string;
}

const PrivateKeyExample: React.FC<PrivateKeyExampleProps> = ({ className }) => {
  const [isOpen, setIsOpen] = useState(false);
  return (
    <div className={classNames(style.wrapper, className)}>
      <div className={style.main}>
        <div className={style.text}>
          <Text type="body-regular-14" color="grey-90">
            Example:&nbsp;
          </Text>
          <Text type="body-regular-14" color="grey-100">
            ----BEGIN PRIVATE KEY-----
          </Text>
        </div>
        <LinkButton
          icon={<ChevronIcon className={classNames(style.icon, { [style.isClosed]: !isOpen })} />}
          iconRight
          type="subtext-medium"
          onClick={() => setIsOpen((prev) => !prev)}
        >
          {isOpen ? 'Show less' : 'Show more'}
        </LinkButton>
      </div>
      <div className={classNames(style.secondary, { [style.isOpen]: isOpen })}>
        <Text type="body-regular-14" color="grey-100">
          MIGTAgEDSFDFByqGSM49AgEGCCqGSM49AwEHBHkwdwIBAQQgUlbUfHwoegGxPhvL
          hSeJnbHLP8HRo7nBPhJaRPsscRegCgYIKoZIzj0DAQehRANCAARn6DkQxhNK1bDP
          wh7dBLxaA4civvCHnv5P5TiatWwLyIoMCbCs3rl9i0MbgVnvQYYtHCvucOmOCvvCxSvKqhLm
        </Text>
        <Text type="body-regular-14" color="grey-100">
          -----END PRIVATE KEY-----
        </Text>
      </div>
    </div>
  );
};

export default PrivateKeyExample;
