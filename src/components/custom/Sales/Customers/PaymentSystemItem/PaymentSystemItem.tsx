import { Icon } from 'Components/base/Icon';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as CreditCardIcon } from 'Svg/v2/16/credit-card.svg';
import { ReactComponent as StarIcon } from 'Svg/v2/16/star-filled.svg';
import { ReactComponent as BankIcon } from 'Svg/v2/24/bank.svg';
import { ReactComponent as AfterpayIcon } from 'Svg/v2/24/logo-afterpay.svg';
import { ReactComponent as AmericanExpressIcon } from 'Svg/v2/24/logo-amex.svg';
import { ReactComponent as MastercardIcon } from 'Svg/v2/24/logo-mastercard.svg';
import { ReactComponent as VisaIcon } from 'Svg/v2/24/logo-visa.svg';
import React from 'react';

import style from './PaymentSystemItem.module.scss';
import accountNumberHandler from './accountNumberHandler';

export type PaymentSystemItemType =
  | 'visa'
  | 'mastercard'
  | 'afterpay'
  | 'bank'
  | 'amex'
  | 'discover';

export type ItemType = 'creditCard' | 'bankAccount' | 'wallet';

export interface PaymentSystemItemProps {
  type: ItemType;
  paymentSystem?: PaymentSystemItemType;
  name: string;
  accountNumber: string;
  currency: string;
  isDefault: boolean;
}

export const paymentSystemsPreset: Record<PaymentSystemItemType, JSX.Element> = {
  visa: <VisaIcon />,
  mastercard: <MastercardIcon />,
  afterpay: <AfterpayIcon />,
  bank: <BankIcon />,
  amex: <AmericanExpressIcon />,
  discover: <CreditCardIcon width="24px" />,
};

const PaymentSystemItem: React.FC<PaymentSystemItemProps> = ({
  type,
  paymentSystem = 'bank',
  name,
  accountNumber,
  currency,
  isDefault,
}) => (
  <div className={style.component}>
    <div className={style.wrapper}>
      {(type === 'creditCard' || type === 'wallet') && (
        <Icon
          className={style.icon}
          icon={paymentSystemsPreset[paymentSystem]}
          clickThrough
          staticColor
        />
      )}
      {type === 'bankAccount' && (
        <Icon className={style.icon} icon={<BankIcon />} path="inherit" clickThrough staticColor />
      )}
      <div className={style.text}>
        <Text type="body-regular-14">{name}</Text>
        <Text type="body-regular-14" color="grey-100">
          {accountNumberHandler(accountNumber, type)} | {currency}
        </Text>
      </div>
    </div>
    {isDefault && (
      <div className={style.default}>
        <Icon className={style.icon} icon={<StarIcon />} path="inherit" clickThrough />
        <Text type="caption-regular" color="inherit">
          Default
        </Text>
      </div>
    )}
  </div>
);

export default PaymentSystemItem;
