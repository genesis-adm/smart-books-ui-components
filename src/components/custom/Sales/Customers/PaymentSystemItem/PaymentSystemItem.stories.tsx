import React from 'react';

import PaymentSystemItem from './PaymentSystemItem';

export default {
  title: 'Components/Custom/Sales/Customers/Payment System Items',
};

export const ItemCreditCard: React.FC = () => (
  <PaymentSystemItem
    type="creditCard"
    paymentSystem="mastercard"
    name="MonoBank"
    accountNumber="0000111122224567"
    currency="USD"
    isDefault
  />
);
export const ItemBankAccount: React.FC = () => (
  <PaymentSystemItem
    type="bankAccount"
    name="BNP Paribas"
    accountNumber="368617189371892371298746015"
    currency="USD"
    isDefault
  />
);
export const ItemWallet: React.FC = () => (
  <PaymentSystemItem
    type="wallet"
    paymentSystem="afterpay"
    name="AfterPay"
    accountNumber="1231231238999"
    currency="USD"
    isDefault
  />
);
