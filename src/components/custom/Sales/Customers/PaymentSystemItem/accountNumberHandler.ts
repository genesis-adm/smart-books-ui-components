import { ItemType } from './PaymentSystemItem';

const accountNumberHandler = (account: string, type: ItemType, showSymbols = 4) => {
  const { length } = account;

  switch (type) {
    case 'creditCard':
      return `****${account.substring(length - showSymbols, length)}`;
    case 'bankAccount':
      return `${account.substring(0, showSymbols)}******${account.substring(
        length - showSymbols,
        length,
      )}`;
    case 'wallet':
      return `****${account.substring(length - showSymbols, length - showSymbols + 1)}XXX`;
    default:
      return account;
  }
};

export default accountNumberHandler;
