import { DropDown } from 'Components/DropDown';
import { Icon } from 'Components/base/Icon';
import { Tooltip } from 'Components/base/Tooltip';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as BankIcon } from 'Svg/v2/16/bank.svg';
import { ReactComponent as CreditCardIcon } from 'Svg/v2/16/credit-card.svg';
import { ReactComponent as WalletIcon } from 'Svg/v2/16/wallet.svg';
import classNames from 'classnames';
import React, { ReactNode } from 'react';

import { ItemType } from '../PaymentSystemItem/PaymentSystemItem';
import style from './PaymentMethodDropDown.module.scss';

export interface PaymentMethodDropDownProps {
  type: ItemType;
  entityName: string;
  accountNumber: string;
  currency: string;
  paymentMethodsQuantity: number;
  children?: ReactNode;
}

export const ItemTypeIconPreset: Record<ItemType | string, JSX.Element> = {
  creditCard: <CreditCardIcon />,
  bankAccount: <BankIcon />,
  wallet: <WalletIcon />,
};

const PaymentMethodDropDown: React.FC<PaymentMethodDropDownProps> = ({
  type,
  entityName,
  accountNumber,
  currency,
  paymentMethodsQuantity,
  children,
}) => {
  if (paymentMethodsQuantity < 1) {
    return <Text color="grey-100">None</Text>;
  }

  if (paymentMethodsQuantity === 1) {
    return (
      <div className={classNames(style.wrapper, style.single)}>
        <Icon icon={ItemTypeIconPreset[type]} path="inherit" />
        <Text color="inherit">
          {entityName} | {accountNumber} | {currency}
        </Text>
      </div>
    );
  }

  return (
    <DropDown
      flexdirection="column"
      padding="16"
      classnameInner={style.list}
      customScroll
      control={({ isOpen, handleOpen }) => (
        <Tooltip
          message={isOpen ? '' : 'Show all payment methods'}
          onClick={handleOpen}
          wrapperClassName={classNames(style.wrapper, style.multiple, { [style.active]: isOpen })}
        >
          <Icon icon={ItemTypeIconPreset[type]} path="inherit" transition="inherit" />
          <Text color="inherit">
            {entityName} | {accountNumber} | {currency}
          </Text>
          <span className={style.divider} />
          <Text color="inherit">+{paymentMethodsQuantity - 1}</Text>
        </Tooltip>
      )}
    >
      <Container flexdirection="column" gap="16">
        {children}
      </Container>
    </DropDown>
  );
};

export default PaymentMethodDropDown;
