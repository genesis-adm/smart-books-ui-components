import { Meta, Story } from '@storybook/react';
import { Divider } from 'Components/base/Divider';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import {
  PaymentMethodDropDown as Component,
  PaymentMethodDropDownProps as Props,
} from 'Components/custom/Sales/Customers/PaymentMethodDropDown';
import React from 'react';

import { PaymentSystemItem } from '../PaymentSystemItem';
import { PaymentSystemItemType } from '../PaymentSystemItem/PaymentSystemItem';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Sales/Customers/PaymentMethodDropDown',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const PaymentMethodDropDownComponent: Story<Props> = (args) => <Component {...args} />;

export const PaymentMethodDropDown = PaymentMethodDropDownComponent.bind({});
PaymentMethodDropDown.args = {
  type: 'creditCard',
  entityName: 'Bank of America',
  accountNumber: '4830*******6278',
  currency: 'USD',
  paymentMethodsQuantity: 7,
  // children: (
  //   <>
  //     <Container flexdirection="column" gap="12">
  //       <Text type="body-medium" color="grey-100">
  //         Credit Card
  //       </Text>
  //       <Container flexdirection="column" gap="8">
  //         {paymentMethodsListExample.creditCard.map(
  //           ({ paymentSystem, name, accountNumber, currency, isDefault }) => (
  //             <PaymentSystemItem
  //               type="creditCard"
  //               key={accountNumber}
  //               paymentSystem={paymentSystem as PaymentSystemItemType}
  //               name={name}
  //               accountNumber={accountNumber}
  //               currency={currency}
  //               isDefault={isDefault}
  //             />
  //           ),
  //         )}
  //       </Container>
  //     </Container>
  //     <Divider fullHorizontalWidth />
  //     <Container flexdirection="column" gap="12">
  //       <Text type="body-medium" color="grey-100">
  //         Bank Account
  //       </Text>
  //       <Container flexdirection="column" gap="8">
  //         {paymentMethodsListExample.bankAccount.map(
  //           ({ name, accountNumber, currency, isDefault }) => (
  //             <PaymentSystemItem
  //               type="bankAccount"
  //               key={accountNumber}
  //               name={name}
  //               accountNumber={accountNumber}
  //               currency={currency}
  //               isDefault={isDefault}
  //             />
  //           ),
  //         )}
  //       </Container>
  //     </Container>
  //     <Divider fullHorizontalWidth />
  //     <Container flexdirection="column" gap="12">
  //       <Text type="body-medium" color="grey-100">
  //         Wallet
  //       </Text>
  //       <Container flexdirection="column" gap="8">
  //         {paymentMethodsListExample.wallet.map(
  //           ({ name, paymentSystem, accountNumber, currency, isDefault }) => (
  //             <PaymentSystemItem
  //               type="wallet"
  //               key={accountNumber}
  //               paymentSystem={paymentSystem as PaymentSystemItemType}
  //               name={name}
  //               accountNumber={accountNumber}
  //               currency={currency}
  //               isDefault={isDefault}
  //             />
  //           ),
  //         )}
  //       </Container>
  //     </Container>
  //   </>
  // ),
};
