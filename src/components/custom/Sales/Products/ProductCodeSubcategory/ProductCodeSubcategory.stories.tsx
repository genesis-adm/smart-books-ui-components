import { Meta, Story } from '@storybook/react';
import {
  ProductCodeSubcategory as Component,
  ProductCodeSubcategoryProps as Props,
} from 'Components/custom/Sales/Products/ProductCodeSubcategory';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Sales/Products/ProductCodeSubcategory',
  component: Component,
  argTypes: {
    className: hideProperty,
    onChange: hideProperty,
  },
} as Meta;

const ProductCodeSubcategoryComponent: Story<Props> = (args) => <Component {...args} />;

export const ProductCodeSubcategory = ProductCodeSubcategoryComponent.bind({});
ProductCodeSubcategory.args = {
  label: 'Component label',
  checked: false,
  disabled: false,
  id: 'item-unique-id',
  name: 'items-common-name',
};
