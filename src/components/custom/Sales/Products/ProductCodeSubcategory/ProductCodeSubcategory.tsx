import { Radio } from 'Components/base/Radio';
import classNames from 'classnames';
import React from 'react';

import style from './ProductCodeSubcategory.module.scss';

export interface ProductCodeSubcategoryProps {
  id: string;
  name: string;
  label: string;
  checked: boolean;
  disabled: boolean;
  className?: string;
  onChange(): void;
}

const ProductCodeSubcategory: React.FC<ProductCodeSubcategoryProps> = ({
  id,
  name,
  label,
  checked,
  disabled,
  className,
  onChange,
}) => (
  <Radio
    className={classNames(style.component, className)}
    id={id}
    name={name}
    checked={checked}
    disabled={disabled}
    type="text-regular"
    color="grey-100"
    onChange={onChange}
  >
    {label}
  </Radio>
);

export default ProductCodeSubcategory;
