import { Icon } from 'Components/base/Icon';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as TickIcon } from 'Svg/v2/16/tick.svg';
import classNames from 'classnames';
import React, { useRef, useState } from 'react';

import useClickOutside from '../../../../../hooks/useClickOutside';
import style from './ProductCodeCard.module.scss';

export interface ProductCodeCardProps {
  productCode: string;
  description: string;
  details?: string;
  active: boolean;
  className?: string;
  onClick(): void;
}

const ProductCodeCard: React.FC<ProductCodeCardProps> = ({
  productCode,
  description,
  details,
  active,
  className,
  onClick,
}) => {
  const wrapperRef = useRef(null);
  const [showDetails, setShowDetails] = useState(false);
  useClickOutside({ ref: wrapperRef.current, cb: setShowDetails });
  return (
    <div
      role="presentation"
      className={classNames(style.card, { [style.active]: active }, className)}
      onClick={onClick}
      ref={wrapperRef}
    >
      <Icon
        className={classNames(style.tick, { [style.active]: active })}
        icon={<TickIcon />}
        path="inherit"
      />
      <Text type="caption-bold" color="inherit" className={style.code} transition="unset">
        {productCode}
      </Text>
      <Text type="subtext-regular" className={style.description} color="grey-100">
        <span className={style.pretext}>Description:&nbsp;</span>
        {description}
      </Text>
      {details && (
        <Text
          type="subtext-regular"
          className={classNames(style.details, { [style.hidden]: !showDetails })}
          color="grey-100"
        >
          <span className={style.pretext}>Details:&nbsp;</span>
          {details}
        </Text>
      )}
      <span>
        <LinkButton
          className={classNames(style.toggle, { [style.hidden]: !showDetails })}
          type="subtext-medium"
          onClick={() => {
            setShowDetails((prevState) => !prevState);
          }}
        >
          {showDetails ? 'Show less' : 'Show more'}
        </LinkButton>
      </span>
    </div>
  );
};

export default ProductCodeCard;
