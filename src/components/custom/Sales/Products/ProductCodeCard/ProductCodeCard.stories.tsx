import { Meta, Story } from '@storybook/react';
import {
  ProductCodeCard as Component,
  ProductCodeCardProps as Props,
} from 'Components/custom/Sales/Products/ProductCodeCard';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Sales/Products/ProductCodeCard',
  component: Component,
  argTypes: {
    className: hideProperty,
    onClick: hideProperty,
  },
} as Meta;

const ProductCodeCardComponent: Story<Props> = (args) => <Component {...args} />;

export const ProductCodeCard = ProductCodeCardComponent.bind({});
ProductCodeCard.args = {
  active: false,
  productCode: 'PC040405',
  description: 'Clothing and related products (business-to-customer)-gym suits',
  details: 'Clothing and related products (business-to-customer)-gym suits',
};
