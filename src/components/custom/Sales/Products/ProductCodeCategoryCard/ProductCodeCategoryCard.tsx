import { Icon } from 'Components/base/Icon';
import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React from 'react';

import style from './ProductCodeCategoryCard.module.scss';

export interface ProductCodeCategoryCardProps {
  icon: React.ReactNode;
  title: string;
  className?: string;
  onClick(): void;
}

const ProductCodeCategoryCard: React.FC<ProductCodeCategoryCardProps> = ({
  icon,
  title,
  className,
  onClick,
}) => (
  <div role="presentation" className={classNames(style.card, className)} onClick={onClick}>
    <Icon className={style.icon} icon={icon} path="inherit" />
    <Text className={style.title} type="text-medium" color="inherit" transition="unset">
      {title}
    </Text>
  </div>
);

export default ProductCodeCategoryCard;
