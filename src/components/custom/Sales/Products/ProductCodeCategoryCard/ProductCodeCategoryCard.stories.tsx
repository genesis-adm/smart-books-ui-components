import { Meta, Story } from '@storybook/react';
import {
  ProductCodeCategoryCard as Component,
  ProductCodeCategoryCardProps as Props,
} from 'Components/custom/Sales/Products/ProductCodeCategoryCard';
import { ReactComponent as ClockIcon } from 'Svg/v2/24/clock.svg';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Sales/Products/ProductCodeCategoryCard',
  component: Component,
  argTypes: {
    icon: hideProperty,
    className: hideProperty,
    onClick: hideProperty,
  },
} as Meta;

const ProductCodeCategoryCardComponent: Story<Props> = (args) => <Component {...args} />;

export const ProductCodeCategoryCard = ProductCodeCategoryCardComponent.bind({});
ProductCodeCategoryCard.args = {
  icon: <ClockIcon />,
  title: 'Miscellaneous services',
};
