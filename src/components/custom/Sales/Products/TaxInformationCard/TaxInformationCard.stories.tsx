import { Meta, Story } from '@storybook/react';
import {
  TaxInformationCard as Component,
  TaxInformationCardProps as Props,
} from 'Components/custom/Sales/Products/TaxInformationCard';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Sales/Products/TaxInformationCard',
  component: Component,
  argTypes: {
    className: hideProperty,
    onEdit: hideProperty,
    onDelete: hideProperty,
  },
} as Meta;

const TaxInformationCardComponent: Story<Props> = (args) => <Component {...args} />;

export const TaxInformationCard = TaxInformationCardComponent.bind({});
TaxInformationCard.args = {
  taxCode: 'PC040405',
  taxCodeDescription: 'Clothing and related products (business-to-customer)-gym suits ',
  category: 'Administrative and support services',
  subcategory: 'Accessories and supplies',
};
