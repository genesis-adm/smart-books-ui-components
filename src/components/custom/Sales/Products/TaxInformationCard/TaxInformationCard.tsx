import { Divider } from 'Components/base/Divider';
import { Icon } from 'Components/base/Icon';
import { Tooltip } from 'Components/base/Tooltip';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as InfoIcon } from 'Svg/v2/16/info.svg';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import classNames from 'classnames';
import React from 'react';

import style from './TaxInformationCard.module.scss';

export interface TaxInformationCardProps {
  taxCode: string;
  taxCodeDescription: string;
  category: string;
  subcategory: string;
  className?: string;
  onEdit(): void;
  onDelete(): void;
}

const TaxInformationCard: React.FC<TaxInformationCardProps> = ({
  taxCode,
  taxCodeDescription,
  category,
  subcategory,
  className,
  onEdit,
  onDelete,
}) => (
  <div className={classNames(style.wrapper, className)}>
    <div className={style.code}>
      <Text className={style.title} type="caption-regular" color="grey-100">
        Tax code
      </Text>
      <Tooltip
        message={
          <Text type="caption-regular" color="white-100">
            <Text type="caption-bold" color="white-100">
              Description:&nbsp;
            </Text>
            {taxCodeDescription}
          </Text>
        }
      >
        <div className={style.item}>
          <Text type="caption-bold" color="white-100">
            {taxCode}
          </Text>
          <Icon className={style.info} icon={<InfoIcon />} path="white-100" />
        </div>
      </Tooltip>
    </div>
    <Divider type="vertical" height="20" />
    <div className={style.category}>
      <Text className={style.title} type="caption-regular" color="grey-100">
        Category
      </Text>
      <Text className={style.text} type="body-regular-14">
        {category}
      </Text>
    </div>
    <Divider type="vertical" height="20" />
    <div className={style.subcategory}>
      <Text className={style.title} type="caption-regular" color="grey-100">
        Subcategory
      </Text>
      <Text className={style.text} type="body-regular-14">
        {subcategory}
      </Text>
    </div>
    <Divider type="vertical" height="20" />
    <div className={style.action}>
      <IconButton
        className={style.icon}
        tooltip="Edit"
        icon={<EditIcon />}
        size="24"
        color="grey-100-yellow-90"
        background="transparent-yellow-10"
        onClick={onEdit}
      />
      <IconButton
        className={style.icon}
        tooltip="Delete"
        icon={<TrashIcon />}
        size="24"
        color="grey-100-red-90"
        background="transparent-red-10"
        onClick={onDelete}
      />
    </div>
  </div>
);

export default TaxInformationCard;
