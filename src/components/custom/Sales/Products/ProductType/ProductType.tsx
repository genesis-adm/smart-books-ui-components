import { Icon } from 'Components/base/Icon';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as NonInventoryIcon } from 'Svg/v2/32/box-crossed.svg';
import { ReactComponent as InventoryIcon } from 'Svg/v2/32/box.svg';
import { ReactComponent as ServiceIcon } from 'Svg/v2/32/clipboard.svg';
import { ReactComponent as DigitalGoodsIcon } from 'Svg/v2/32/devices.svg';
import { ReactComponent as BundleIcon } from 'Svg/v2/32/puzzle.svg';
import classNames from 'classnames';
import React from 'react';

import style from './ProductType.module.scss';

export interface ProductTypeProps {
  type: 'digitalgoods' | 'service' | 'bundle' | 'inventory' | 'noninventory';
  className?: string;
  disabled?: boolean;
  onClick(): void;
}

const productTypes = {
  digitalgoods: {
    name: 'Digital goods',
    icon: <DigitalGoodsIcon />,
  },
  service: {
    name: 'Service',
    icon: <ServiceIcon />,
  },
  bundle: {
    name: 'Bundle',
    icon: <BundleIcon />,
  },
  inventory: {
    name: 'Inventory',
    icon: <InventoryIcon />,
  },
  noninventory: {
    name: 'Non-inventory',
    icon: <NonInventoryIcon />,
  },
};

const ProductType: React.FC<ProductTypeProps> = ({ type, className, disabled, onClick }) => {
  const handleClick = () => {
    if (!disabled) onClick();
  };
  return (
    <div
      className={classNames(
        style.wrapper,
        {
          [style.active]: !disabled,
          [style.disabled]: disabled,
        },
        className,
      )}
      role="presentation"
      onClick={handleClick}
    >
      <div className={style.container}>
        <Icon className={style.icon} icon={productTypes[type].icon} path="inherit" />
      </div>
      <Text className={style.text} type="body-medium" color="inherit">
        {productTypes[type].name}
      </Text>
    </div>
  );
};

export default ProductType;
