import { Meta, Story } from '@storybook/react';
import {
  ProductType as Component,
  ProductTypeProps as Props,
} from 'Components/custom/Sales/Products/ProductType';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Sales/Products/Product Type',
  component: Component,
  argTypes: {
    className: hideProperty,
    onClick: hideProperty,
  },
} as Meta;

const ProductTypeComponent: Story<Props> = (args) => <Component {...args} />;

export const ProductType = ProductTypeComponent.bind({});
ProductType.args = {
  type: 'digitalgoods',
  disabled: false,
};
