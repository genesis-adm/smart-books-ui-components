import { DropDown } from 'Components/DropDown';
import { Icon } from 'Components/base/Icon';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as CaretDownIcon } from 'Svg/v2/16/caret-down.svg';
import classNames from 'classnames';
import React from 'react';

import style from './ProductCodeCategoryDropdown.module.scss';

export interface ProductCodeCategoryDropdownProps {
  categoryIcon: React.ReactNode;
  categoryName: string;
  className?: string;
  children?: React.ReactNode;
}

const ProductCodeCategoryDropdown: React.FC<ProductCodeCategoryDropdownProps> = ({
  categoryIcon,
  categoryName,
  className,
  children,
}) => (
  <DropDown
    axis="vertical"
    position="bottom-left"
    flexdirection="row"
    padding="none"
    classNameOuter={classNames(style.component, className)}
    control={({ isOpen, handleOpen }) => (
      <div
        role="presentation"
        onClick={handleOpen}
        className={classNames(style.control, { [style.active]: isOpen })}
      >
        <Icon className={style.icon} icon={categoryIcon} path="inherit" />
        <Text className={style.name} type="body-medium" color="inherit">
          {categoryName}
        </Text>
        <Icon className={style.caret} icon={<CaretDownIcon />} path="inherit" />
      </div>
    )}
  >
    <Container className={style.wrapper} flexdirection="column">
      <Text className={style.text} type="body-medium" color="black-100">
        Choose category
      </Text>
      <Container className={style.categories} flexwrap="wrap" gap="8">
        {children}
      </Container>
    </Container>
  </DropDown>
);
export default ProductCodeCategoryDropdown;
