import { Meta, Story } from '@storybook/react';
import {
  ProductCodeCategoryDropdown as Component,
  ProductCodeCategoryDropdownProps as Props,
} from 'Components/custom/Sales/Products/ProductCodeCategoryDropdown';
import { ReactComponent as TestIcon } from 'Svg/v2/24/clock.svg';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Sales/Products/ProductCodeCategoryDropdown',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const ProductCodeCategoryDropdownComponent: Story<Props> = (args) => <Component {...args} />;

export const ProductCodeCategoryDropdown = ProductCodeCategoryDropdownComponent.bind({});
ProductCodeCategoryDropdown.args = {
  categoryIcon: <TestIcon />,
  categoryName: 'Administrative and support services',
};
