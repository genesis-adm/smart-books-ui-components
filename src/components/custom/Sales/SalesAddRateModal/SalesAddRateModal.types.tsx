import React from 'react';

export interface SalesAddRateModalProps {
  title?: string;
  footer: React.ReactNode;
  children?: React.ReactNode;
  productName: string;
  amount: number;
  rate: number;
  totalAmount: number;
  onClose(): void;
}
