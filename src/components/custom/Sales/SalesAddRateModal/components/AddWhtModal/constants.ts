import { TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';

export const whtTableColumns: TableTitleNewProps[] = [
  {
    id: 'whtName',
    minWidth: '420',
    title: 'WHT Name',
    required: true,
    padding: 'default',
  },
  {
    id: 'whtRate',
    minWidth: '166',
    title: 'WHT Rate',
    align: 'right',
    fixedWidth: true,
  },
  {
    id: 'whtAmount',
    minWidth: '194',
    title: 'WHT Amount',
    align: 'right',
    fixedWidth: true,
  },
  {
    id: 'whtActions',
    minWidth: '70',
    title: 'Actions',
    align: 'right',
  },
];
