import { IconButton } from 'Components/base/buttons/IconButton';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { InputNew } from 'Components/base/inputs/InputNew';
import { NumericInput } from 'Components/base/inputs/NumericInput';
import { OwnNumberFormatValues } from 'Components/base/inputs/NumericInput/NumericInput.types';
import { Text } from 'Components/base/typography/Text';
import AddWhtModalFooter from 'Components/custom/Sales/SalesAddRateModal/components/AddWhtModal/components/AddWhtModalFooter';
import AddWhtModalHeader from 'Components/custom/Sales/SalesAddRateModal/components/AddWhtModal/components/AddWhtModalHeader';
import { whtTableColumns } from 'Components/custom/Sales/SalesAddRateModal/components/AddWhtModal/constants';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { Modal } from 'Components/modules/Modal';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import classNames from 'classnames';
import React, { FC, ReactElement, useState } from 'react';

import spacing from '../../../../../../assets/styles/spacing.module.scss';

type AddWhtModalProps = {
  onClose?: () => void;
};

const AddWhtModal: FC<AddWhtModalProps> = ({ onClose }: AddWhtModalProps): ReactElement => {
  const [whtRate, setWhtRate] = useState<number>();
  const changeWhtRate = ({ floatValue }: OwnNumberFormatValues) => {
    setWhtRate(floatValue);
  };

  const [whtAmount, setWhtAmount] = useState<number>();
  const changeWhtAmount = ({ floatValue }: OwnNumberFormatValues) => {
    setWhtAmount(floatValue);
  };

  const tableColumns = {
    whtName: ({ id = 'whtName', minWidth }: TableTitleNewProps) => (
      <TableCellNew id={id} minWidth={minWidth} alignitems="flex-start" padding="8">
        <InputNew name={id || 'whtName'} label="Add" placeholder="Add" width="full" height="36" />
      </TableCellNew>
    ),
    whtRate: ({ id = 'whtRate', minWidth }: TableTitleNewProps) => (
      <TableCellNew
        id={id}
        minWidth={minWidth}
        alignitems="flex-end"
        justifycontent="flex-end"
        fixedWidth
      >
        <NumericInput
          name={id}
          label=""
          width="134"
          height="36"
          value={whtRate}
          onValueChange={changeWhtRate}
        />
      </TableCellNew>
    ),
    whtAmount: ({ id = 'whtAmount', minWidth }: TableTitleNewProps) => (
      <TableCellNew
        id={id}
        minWidth={minWidth}
        alignitems="center"
        justifycontent="flex-end"
        fixedWidth
      >
        <Container display="flex" justifycontent="space-between" gap="8" alignitems="center">
          <Text color="grey-100">or</Text>
          <NumericInput
            name={id}
            label=""
            width="134"
            height="36"
            value={whtAmount}
            onValueChange={changeWhtAmount}
          />
        </Container>
      </TableCellNew>
    ),

    whtActions: ({ id = 'actions', minWidth }: TableTitleNewProps) => (
      <TableCellNew
        id={id}
        minWidth={minWidth}
        alignitems="center"
        justifycontent="center"
        fixedWidth
      >
        <IconButton
          icon={<TrashIcon />}
          size="28"
          onClick={() => {}}
          background={'grey-10-red-10'}
          color="grey-100-red-90"
        />
      </TableCellNew>
    ),
  };

  return (
    <Modal
      size="960"
      pluginScrollDisabled
      centered
      overlay="black-100"
      padding="24"
      header={<AddWhtModalHeader onClose={onClose} />}
      footer={<AddWhtModalFooter />}
    >
      <TableNew
        noResults={false}
        className={classNames(spacing.h400fixed)}
        tableHead={
          <TableHeadNew>
            {whtTableColumns.map((item) => (
              <TableTitleNew key={item.id} {...item} onClick={() => {}} />
            ))}
          </TableHeadNew>
        }
      >
        <TableBodyNew>
          {Array.from(Array(12)).map((_, index) => (
            <TableRowNew key={index} gap="8">
              {whtTableColumns.map((props) =>
                tableColumns?.[props?.id as keyof typeof tableColumns]?.(props),
              )}
            </TableRowNew>
          ))}
          <Container className={spacing.mt24} justifycontent="flex-end">
            <LinkButton icon={<PlusIcon />} onClick={() => {}}>
              Add WHT
            </LinkButton>
          </Container>
        </TableBodyNew>
      </TableNew>
    </Modal>
  );
};

export default AddWhtModal;
