import { Button } from 'Components/base/buttons/Button';
import { ButtonGroup } from 'Components/base/buttons/ButtonGroup';
import { ModalFooter } from 'Components/modules/Modal';
import React, { FC, ReactElement } from 'react';

const AddWhtModalFooter: FC = (): ReactElement => {
  return (
    <ModalFooter justifycontent="center">
      <ButtonGroup align="right">
        <Button navigation width="180" height="48" onClick={() => {}}>
          Save
        </Button>
      </ButtonGroup>
    </ModalFooter>
  );
};

export default AddWhtModalFooter;
