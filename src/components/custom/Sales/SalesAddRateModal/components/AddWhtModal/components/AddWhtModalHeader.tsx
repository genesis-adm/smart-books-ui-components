import { Divider } from 'Components/base/Divider';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { CircledIcon } from 'Components/graphics/CircledIcon';
import { ReactComponent as SparklesIcon } from 'Svg/12/sparkles.svg';
import { ReactComponent as CrossFilledIcon } from 'Svg/v2/16/cross.svg';
import classNames from 'classnames';
import React, { FC, ReactElement } from 'react';

import spacing from '../../../../../../../assets/styles/spacing.module.scss';
import TextWithTooltip from '../../../../../../base/typography/TextWithTooltip/TextWithTooltip';

type AddWhtModalHeaderProps = {
  onClose?: () => void;
};

const AddWhtModalHeader: FC<AddWhtModalHeaderProps> = ({
  onClose,
}: AddWhtModalHeaderProps): ReactElement => {
  const items = [
    { title: 'WHT base amount, €', value: '0.00 €' },
    { title: 'Total WHT rate, %', value: '0.00 %' },
    { title: 'Total WHT amount, €', value: '0.00 €' },
  ];

  return (
    <Container
      className={classNames(spacing.w100p, spacing.p24)}
      alignitems="center"
      justifycontent="space-between"
      flexwrap="wrap"
      style={{ borderBottom: '1px solid #EDEFF1' }}
      background={'grey-10'}
    >
      <Container gap="24" alignitems="center">
        <Container flexdirection="column" alignitems="flex-start" gap="8">
          <Text type="title-bold">Add WHT</Text>
        </Container>
        <Divider type="vertical" height="26" background={'grey-90'} />
        <Container>
          <Container gap="12" alignitems="center">
            <CircledIcon size="24" background={'grey-20'} icon={<SparklesIcon />} />
            <Container flexdirection="column">
              <Text type="caption-regular" color="grey-100">
                Product
              </Text>
              <TextWithTooltip
                className={spacing.w230}
                type="body-medium"
                text="BetterMe IOSdsjkfghtrughurghrtbgkhrtjkhbjgk"
              />
            </Container>
          </Container>
        </Container>
      </Container>
      <Container gap="24" alignitems="center">
        {items?.map(({ title, value }) => (
          <Container key={title} flexdirection="column" alignitems="flex-end">
            <Text type="subtext-regular" color="grey-90">
              {title}
            </Text>
            <Text type="body-bold-14">{value}</Text>
          </Container>
        ))}

        <IconButton
          size="32"
          icon={<CrossFilledIcon />}
          onClick={onClose}
          background={'white-100'}
          color="grey-100"
          shape="circle"
        />
      </Container>
    </Container>
  );
};

export default AddWhtModalHeader;
