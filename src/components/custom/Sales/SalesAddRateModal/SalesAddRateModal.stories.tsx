import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { Divider } from 'Components/base/Divider';
import { Tag } from 'Components/base/Tag';
import { Button } from 'Components/base/buttons/Button';
import { ButtonGroup } from 'Components/base/buttons/ButtonGroup';
import { IconButton } from 'Components/base/buttons/IconButton';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { InputNew } from 'Components/base/inputs/InputNew';
import { NumericInput } from 'Components/base/inputs/NumericInput';
import { OwnNumberFormatValues } from 'Components/base/inputs/NumericInput/NumericInput.types';
import { OptionSingleNew } from 'Components/base/inputs/SelectNew/options/OptionSingleNew';
import { Text } from 'Components/base/typography/Text';
import TextWithTooltip from 'Components/base/typography/TextWithTooltip/TextWithTooltip';
import AddWhtModal from 'Components/custom/Sales/SalesAddRateModal/components/AddWhtModal';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableSelect } from 'Components/elements/Table/TableSelect';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { CircledIcon } from 'Components/graphics/CircledIcon';
import { Modal, ModalFooter } from 'Components/modules/Modal';
import { TableCellWidth } from 'Components/types/gridTypes';
import { options } from 'Mocks/fakeOptions';
import { ReactComponent as SparklesIcon } from 'Svg/12/sparkles.svg';
import { ReactComponent as CrossFilledIcon } from 'Svg/v2/16/cross.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Sales/All Sales/Modals/Add Rate Modal',
};

interface AddTaxModalProps {
  onClose: () => void;
}

export const AddTaxModal: React.FC<AddTaxModalProps> = ({ onClose }: AddTaxModalProps) => {
  const tableHeadTaxes = [
    {
      minWidth: '220',
      title: 'Tax Name',
      required: true,
    },
    {
      minWidth: '220',
      title: 'Tax type',
      required: true,
    },
    {
      minWidth: '150',
      title: 'Tax Rate',
      align: 'right',
      fixedWidth: true,
      required: true,
    },
    {
      minWidth: '150',
      title: 'Tax Amount',
      align: 'right',
      fixedWidth: true,
      required: true,
    },
    {
      minWidth: '60',
      title: 'Actions',
      align: 'right',
    },
  ];

  const [inputValue, setInputValue] = useState<number>();
  const handleChange = ({ floatValue }: OwnNumberFormatValues) => {
    setInputValue(floatValue);
  };

  return (
    <Modal
      size="960"
      pluginScrollDisabled
      centered
      overlay="black-100"
      header={
        <Container
          className={classNames(spacing.w100p, spacing.p24)}
          alignitems="center"
          justifycontent="space-between"
          flexwrap="wrap"
          style={{ borderBottom: '1px solid #EDEFF1' }}
          background="grey-10"
        >
          <Container gap="24" alignitems="center">
            <Container flexdirection="column" alignitems="flex-start" gap="8">
              <Text type="title-bold">Add Taxes</Text>
              <Tag size="24" padding="4-8" color="grey" text="Tax inclusive" cursor="default" />
            </Container>
            <Divider type="vertical" height="26" background="grey-90" />
            <Container>
              <Container gap="12" alignitems="center">
                <CircledIcon size="24" background="grey-20" icon={<SparklesIcon />} />
                <Container flexdirection="column">
                  <Text type="caption-regular" color="grey-100">
                    Product
                  </Text>
                  <TextWithTooltip
                    className={spacing.w230}
                    type="body-medium"
                    text="BetterMe IOS"
                  />
                </Container>
              </Container>
            </Container>
          </Container>
          <Container gap="24" alignitems="center">
            <Container flexdirection="column" alignitems="flex-end">
              <Text type="caption-regular" color="grey-100">
                Taxable amount, €
              </Text>
              <Text type="body-medium">0.00 $</Text>
            </Container>
            <Container flexdirection="column" alignitems="flex-end">
              <Text type="caption-regular" color="grey-100">
                Total Tax rate, %
              </Text>
              <Text type="body-medium">0.00 %</Text>
            </Container>
            <Container flexdirection="column" alignitems="flex-end">
              <Text type="caption-regular" color="grey-100">
                Total Tax amount, €
              </Text>
              <Text type="body-medium">0.00 $</Text>
            </Container>
            <IconButton
              size="32"
              icon={<CrossFilledIcon />}
              onClick={() => onClose()}
              background="white-100"
              color="grey-100"
              shape="circle"
            />
          </Container>
        </Container>
      }
      footer={
        <ModalFooter justifycontent="center">
          <ButtonGroup align="right">
            <Button navigation width="md" onClick={() => {}}>
              Save
            </Button>
          </ButtonGroup>
        </ModalFooter>
      }
    >
      <TableNew
        noResults={false}
        className={classNames(spacing.h400fixed, spacing.p24)}
        tableHead={
          <TableHeadNew>
            {tableHeadTaxes.map((item, index) => (
              <TableTitleNew
                key={index}
                minWidth={item.minWidth as TableCellWidth}
                required={item.required as TableTitleNewProps['required']}
                fixedWidth={item.fixedWidth}
                align={item.align as TableTitleNewProps['align']}
                title={item.title}
                onClick={() => {}}
              />
            ))}
          </TableHeadNew>
        }
      >
        <TableBodyNew>
          {Array.from(Array(4)).map((_, index) => (
            <TableRowNew key={index} active={index === 1} gap="8">
              <TableCellNew minWidth="220" alignitems="flex-start" padding="8">
                <InputNew name="taxName" label="Add" width="full" placeholder="Add" height="36" />
              </TableCellNew>
              <TableCellNew minWidth="220" alignitems="center" padding="8">
                <TableSelect
                  name="taxType"
                  onChange={() => {}}
                  placeholder="Add"
                  isRowActive={index === 2}
                  border="grey-20"
                >
                  {({ onClick, state, selectElement }) => (
                    <SelectDropdown selectElement={selectElement}>
                      <SelectDropdownBody>
                        <SelectDropdownItemList>
                          {options.map(({ label, value }) => (
                            <OptionSingleNew
                              key={value}
                              label={label}
                              selected={state?.value === value}
                              onClick={() =>
                                onClick({
                                  label,
                                  value,
                                })
                              }
                            />
                          ))}
                        </SelectDropdownItemList>
                      </SelectDropdownBody>
                    </SelectDropdown>
                  )}
                </TableSelect>
              </TableCellNew>
              <TableCellNew minWidth="150" alignitems="center" justifycontent="flex-end" fixedWidth>
                <NumericInput
                  name="num-2"
                  label=""
                  width="134"
                  height="36"
                  value={inputValue}
                  onValueChange={handleChange}
                />
              </TableCellNew>
              <TableCellNew minWidth="150" alignitems="center" justifycontent="flex-end" fixedWidth>
                <Container
                  display="flex"
                  justifycontent="space-between"
                  gap="8"
                  alignitems="center"
                >
                  <Text color="grey-100">or</Text>
                  <NumericInput
                    name="num-1"
                    label=""
                    width="134"
                    height="36"
                    value={inputValue}
                    onValueChange={handleChange}
                  />
                </Container>
              </TableCellNew>
              <TableCellNew minWidth="60" alignitems="center" justifycontent="flex-end" fixedWidth>
                <IconButton
                  icon={<TrashIcon />}
                  onClick={() => {}}
                  background="transparent"
                  color="grey-90"
                />
              </TableCellNew>
            </TableRowNew>
          ))}
          <Container className={classNames(spacing.mt16, spacing.mr16)} justifycontent="flex-end">
            <LinkButton icon={<PlusIcon />} onClick={() => {}}>
              Add tax
            </LinkButton>
          </Container>
        </TableBodyNew>
      </TableNew>
    </Modal>
  );
};
export const AddFeesModal: React.FC<AddTaxModalProps> = ({ onClose }: AddTaxModalProps) => {
  const tableHeadFees = [
    {
      minWidth: '220',
      title: 'Fee Name',
      required: true,
      padding: 'default',
    },
    {
      minWidth: '220',
      title: 'Fee type',
      padding: 'default',
    },
    {
      minWidth: '150',
      title: 'Fee Rate',
      align: 'right',
      fixedWidth: true,
    },
    {
      minWidth: '150',
      title: 'Fee Amount',
      align: 'right',
      fixedWidth: true,
    },
    {
      minWidth: '60',
      title: 'Actions',
      align: 'right',
    },
  ];

  const [inputValue, setInputValue] = useState<number>();
  const handleChange = ({ floatValue }: OwnNumberFormatValues) => {
    setInputValue(floatValue);
  };

  return (
    <Modal
      size="960"
      pluginScrollDisabled
      centered
      overlay="black-100"
      header={
        <Container
          className={classNames(spacing.w100p, spacing.p24)}
          alignitems="center"
          justifycontent="space-between"
          flexwrap="wrap"
          style={{ borderBottom: '1px solid #EDEFF1' }}
          background="grey-10"
        >
          <Container gap="24" alignitems="center">
            <Container flexdirection="column" alignitems="flex-start" gap="8">
              <Text type="title-bold">Add Fees</Text>
            </Container>
            <Divider type="vertical" height="26" background="grey-90" />
            <Container>
              <Container gap="12" alignitems="center">
                <CircledIcon size="24" background="grey-20" icon={<SparklesIcon />} />
                <Container flexdirection="column">
                  <Text type="caption-regular" color="grey-100">
                    Product
                  </Text>
                  <TextWithTooltip
                    className={spacing.w230}
                    type="body-medium"
                    text="BetterMe IOSdsjkfghtrughurghrtbgkhrtjkhbjgk"
                  />
                </Container>
              </Container>
            </Container>
          </Container>
          <Container gap="24" alignitems="center">
            <Container flexdirection="column" alignitems="flex-end">
              <Text type="caption-regular" color="grey-100">
                Fee base amount, €
              </Text>
              <Text type="body-medium">0.00 $</Text>
            </Container>
            <Container flexdirection="column" alignitems="flex-end">
              <Text type="caption-regular" color="grey-100">
                Total Fee rate, %
              </Text>
              <Text type="body-medium">0.00 %</Text>
            </Container>
            <Container flexdirection="column" alignitems="flex-end">
              <Text type="caption-regular" color="grey-100">
                Total Fee amount, €
              </Text>
              <Text type="body-medium">0.00 $</Text>
            </Container>
            <IconButton
              size="32"
              icon={<CrossFilledIcon />}
              onClick={() => onClose()}
              background="white-100"
              color="grey-100"
              shape="circle"
            />
          </Container>
        </Container>
      }
      footer={
        <ModalFooter justifycontent="center">
          <ButtonGroup align="right">
            <Button navigation width="md" onClick={() => {}}>
              Save
            </Button>
          </ButtonGroup>
        </ModalFooter>
      }
    >
      <TableNew
        noResults={false}
        className={classNames(spacing.h400fixed, spacing.p24)}
        tableHead={
          <TableHeadNew>
            {tableHeadFees.map((item, index) => (
              <TableTitleNew
                key={index}
                minWidth={item.minWidth as TableCellWidth}
                required={item.required as TableTitleNewProps['required']}
                fixedWidth={item.fixedWidth}
                align={item.align as TableTitleNewProps['align']}
                padding={item.padding as TableTitleNewProps['padding']}
                title={item.title}
                onClick={() => {}}
              />
            ))}
          </TableHeadNew>
        }
      >
        <TableBodyNew>
          {Array.from(Array(4)).map((_, index) => (
            <TableRowNew key={index} gap="8">
              <TableCellNew minWidth="220" alignitems="flex-start" padding="8">
                <InputNew name="feeName" label="Add" placeholder="Add" width="full" height="36" />
              </TableCellNew>
              <TableCellNew minWidth="220" alignitems="flex-start" padding="8">
                <InputNew name="feeType" label="Add" placeholder="Add" width="full" height="36" />
              </TableCellNew>
              <TableCellNew
                minWidth="150"
                alignitems="flex-end"
                justifycontent="flex-end"
                fixedWidth
              >
                <NumericInput
                  name="num-1"
                  label=""
                  width="134"
                  height="36"
                  value={inputValue}
                  onValueChange={handleChange}
                />
              </TableCellNew>
              <TableCellNew minWidth="150" alignitems="center" justifycontent="flex-end" fixedWidth>
                <Container
                  display="flex"
                  justifycontent="space-between"
                  gap="8"
                  alignitems="center"
                >
                  <Text color="grey-100">or</Text>
                  <NumericInput
                    name="num-2"
                    label=""
                    width="134"
                    height="36"
                    value={inputValue}
                    onValueChange={handleChange}
                  />
                </Container>
              </TableCellNew>
              <TableCellNew minWidth="60" alignitems="center" justifycontent="flex-end" fixedWidth>
                <IconButton
                  icon={<TrashIcon />}
                  onClick={() => {}}
                  background="transparent"
                  color="grey-90"
                />
              </TableCellNew>
            </TableRowNew>
          ))}
          <Container className={classNames(spacing.mt16, spacing.mr16)} justifycontent="flex-end">
            <LinkButton icon={<PlusIcon />} onClick={() => {}}>
              Add fee
            </LinkButton>
          </Container>
        </TableBodyNew>
      </TableNew>
    </Modal>
  );
};

export const AddWhtModalStory = AddWhtModal;
