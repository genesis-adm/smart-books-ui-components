import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { Logo } from 'Components/elements/Logo';
import classNames from 'classnames';
import React from 'react';

import style from './NavigationCompany.module.scss';

interface NavigationCompanyProps {
  active?: boolean;
  logo: string | null | undefined;
  color?: string | undefined;
  companyName: string;
  companyType: string;
  onClick(): void;
}

const NavigationCompany: React.FC<NavigationCompanyProps> = ({
  active,
  logo,
  color,
  companyName,
  companyType,
  onClick,
}) => (
  <Container
    className={classNames(style.card, {
      [style.mini]: !active,
    })}
    flexdirection="column"
    alignitems="center"
  >
    <Container className={style.toggle} onClick={onClick}>
      <Logo content="company" size="lg" border="dark" name={companyName} img={logo} color={color} />
    </Container>
    <Text
      className={classNames(style.text, style.name)}
      align="center"
      type="body-regular-14"
      color="white-100"
      transform="uppercase"
    >
      {companyName}
    </Text>
    <Text
      className={classNames(style.text, style.type)}
      align="center"
      type="text-regular"
      color="grey-100"
      transform="capitalize"
    >
      {companyType}
    </Text>
  </Container>
);

NavigationCompany.defaultProps = {
  active: false,
  color: undefined,
};

export default NavigationCompany;
