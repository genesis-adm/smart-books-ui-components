import { Container } from 'Components/base/grid/Container';
import classNames from 'classnames';
import React from 'react';

import style from './Navigation.module.scss';

interface NavigationProps {
  opened?: boolean;
  children?: React.ReactNode;
}

const Navigation: React.FC<NavigationProps> = ({ opened, children }) => (
  <Container
    className={classNames(style.navigation, {
      [style.default]: !opened,
    })}
    background="black-100"
    flexdirection="column"
    justifycontent="space-between"
    // overflow="y-auto" // test
  >
    {children}
  </Container>
);

Navigation.defaultProps = {
  opened: false,
};

export default Navigation;
