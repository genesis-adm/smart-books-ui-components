import { NavigationSB } from 'Components/custom/Stories/NavigationSB';
import React, { useState } from 'react';

export default {
  title: 'Components/Custom/Navigation',
};

export const Navigation: React.FC = () => {
  const [isOpen, setIsOpen] = useState(true);
  const toggleOpen = () => setIsOpen((prevState) => !prevState);
  return <NavigationSB opened={isOpen} onToggleOpen={toggleOpen} />;
};
