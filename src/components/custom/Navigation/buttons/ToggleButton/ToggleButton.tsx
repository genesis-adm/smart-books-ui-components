import { Icon } from 'Components/base/Icon';
import { NavigationButton } from 'Components/custom/Navigation/buttons/NavigationButton';
import { ReactComponent as ChevronIcon } from 'Svg/v2/16/chevron-double-right.svg';
import classNames from 'classnames';
import React from 'react';

import style from './ToggleButton.module.scss';

export interface ToggleButtonProps {
  toggled: boolean;
  className?: string;
  onClick?(): void;
}

const ToggleButton: React.FC<ToggleButtonProps> = ({ toggled, className, onClick }) => (
  <NavigationButton
    type="toggle"
    title={toggled ? '' : 'Show menu'}
    className={classNames(style.button, className, {
      [style.toggled]: toggled,
    })}
    icon={<Icon icon={<ChevronIcon />} path="white-100" />}
    onClick={onClick}
    minimized={toggled}
  >
    Hide menu
  </NavigationButton>
);

export default ToggleButton;
