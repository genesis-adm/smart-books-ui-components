import { Tooltip } from 'Components/base/Tooltip';
import classNames from 'classnames';
import React from 'react';

import style from './NavigationButton.module.scss';

export interface NavigationButtonProps extends React.InputHTMLAttributes<HTMLButtonElement> {
  type?: 'navigation' | 'toggle';
  title: string;
  icon?: React.ReactNode;
  selected?: boolean;
  minimized: boolean;
  className?: string;
  onClick?(): void;
}

const NavigationButton: React.FC<NavigationButtonProps> = ({
  type = 'navigation',
  title,
  icon,
  selected,
  minimized,
  className,
  children,
  onClick,
}) => (
  <Tooltip message={minimized ? '' : title}>
    <button
      type="button"
      className={classNames(
        style.button,
        style[`type_${type}`],
        {
          [style.selected]: selected,
          [style.minimized]: !minimized,
        },
        className,
      )}
      onClick={onClick}
    >
      {icon && <span className={style.icon}>{icon}</span>}
      {children && <span className={style.text}>{children}</span>}
    </button>
  </Tooltip>
);

export default NavigationButton;
