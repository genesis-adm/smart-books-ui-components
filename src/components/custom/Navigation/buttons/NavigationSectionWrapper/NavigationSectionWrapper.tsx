import classNames from 'classnames';
import React, { ReactElement, ReactNode, useState } from 'react';

import style from './NavigationSectionWrapper.module.scss';

export interface NavigationSectionWrapperProps {
  className?: string;
  mainButton: ReactNode;
  sectionButtons: ReactNode;
}

const NavigationSectionWrapper = ({
  className,
  mainButton,
  sectionButtons,
}: NavigationSectionWrapperProps): ReactElement => {
  const [showList, setShowList] = useState(false);

  const handleShowList = () => {
    setShowList(true);
  };

  const handleHideList = () => {
    setShowList(false);
  };

  return (
    <div
      onClick={handleHideList}
      onMouseEnter={handleShowList}
      onMouseLeave={handleHideList}
      className={classNames(style.wrapper, className)}
    >
      {mainButton}
      {sectionButtons && showList && <div className={style.list}>{sectionButtons}</div>}
    </div>
  );
};

export default NavigationSectionWrapper;
