import { Icon } from 'Components/base/Icon';
import { Tooltip } from 'Components/base/Tooltip';
import classNames from 'classnames';
import React, { ReactElement, ReactNode } from 'react';

import style from './NavigationButtonNew.module.scss';

export interface NavigationButtonNewProps {
  id: string;
  title: string;
  icon?: ReactNode;
  isSelected: boolean;
  showFullButton?: boolean;
  showTooltip?: boolean;
  iconStaticColor?: boolean;
  type?: 'module' | 'section';
  className?: string;
  onClick?(): void;
}

const NavigationButtonNew = ({
  id,
  title,
  icon,
  isSelected,
  showFullButton = true,
  showTooltip,
  iconStaticColor,
  type = 'section',
  className,
  onClick,
}: NavigationButtonNewProps): ReactElement => (
  <Tooltip
    wrapperClassName={classNames(style.wrapper, className)}
    message={showFullButton || showTooltip ? '' : title}
  >
    <button
      id={id}
      type="button"
      data-type={type}
      className={classNames(style.button, {
        [style.selected]: isSelected,
        [style.minimized]: !showFullButton,
      })}
      onClick={onClick}
    >
      {icon && <Icon staticColor={iconStaticColor} icon={icon} transition="inherit" />}
      <span className={style.text}>{title}</span>
    </button>
  </Tooltip>
);

export default NavigationButtonNew;
