import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { Logo } from 'Components/elements/Logo';
import { ReactComponent as CheckedIcon } from 'Svg/16/check.svg';
import classNames from 'classnames';
import React from 'react';

import style from './CompanyDropDownItem.module.scss';

interface CompanyDropDownItemProps {
  selected?: boolean;
  logo: string | null | undefined;
  color?: string | undefined;
  companyName: string;
  onClick(): void;
}

const CompanyDropDownItem: React.FC<CompanyDropDownItemProps> = ({
  selected,
  logo,
  color,
  companyName,
  onClick,
}) => (
  <div
    role="presentation"
    className={classNames(style.item, {
      [style.selected]: selected,
    })}
    onClick={onClick}
  >
    <Container className={style.logo}>
      <Logo content="company" name={companyName} img={logo} color={color} />
    </Container>
    <Text className={style.text} type="body-regular-14">
      {companyName}
    </Text>
    {selected && <CheckedIcon className={style.check} />}
  </div>
);

CompanyDropDownItem.defaultProps = {
  selected: false,
  color: undefined,
};

export default CompanyDropDownItem;
