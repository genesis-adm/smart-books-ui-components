import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as SearchIcon } from 'Svg/v2/16/search.svg';
import classNames from 'classnames';
import React from 'react';

import style from './CompanyDropDownBrowseAll.module.scss';

interface CompanyDropDownBrowseAllProps {
  onClick(): void;
}

const CompanyDropDownBrowseAll: React.FC<CompanyDropDownBrowseAllProps> = ({ onClick }) => (
  <Container className={classNames(style.item)} alignitems="center" onClick={onClick}>
    <Container className={style.logo}>
      <SearchIcon />
    </Container>
    <Text className={style.text} type="body-regular-14">
      Browse all accounts
    </Text>
  </Container>
);
export default CompanyDropDownBrowseAll;
