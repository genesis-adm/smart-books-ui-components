import { Text } from 'Components/base/typography/Text';
import { ReactComponent as CalendarImage } from 'Svg/calendar.svg';
import classNames from 'classnames';
import React, { ReactElement } from 'react';

import style from './EndOfNotifications.module.scss';
import { EndOfNotificationsProps } from './EndOfNotifications.types';

// Reminder: do not forget to add the following code to root index.ts
// export { EndOfNotifications } from './components/custom/Notifications/EndOfNotifications';

const EndOfNotifications = ({ className }: EndOfNotificationsProps): ReactElement => (
  <div className={classNames(style.container, className)}>
    <CalendarImage />
    <div className={style.container_text}>
      <Text type="caption-regular" color="grey-100" align="center">
        That&apos;s all your notifications from the last 30 days.
      </Text>
    </div>
  </div>
);

export default EndOfNotifications;
