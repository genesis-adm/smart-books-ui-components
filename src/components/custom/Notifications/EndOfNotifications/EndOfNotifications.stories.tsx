import { Meta, Story } from '@storybook/react';
import {
  EndOfNotifications as Component,
  EndOfNotificationsProps as Props,
} from 'Components/custom/Notifications/EndOfNotifications';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'components/custom/Notifications/EndOfNotifications',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const EndOfNotificationsComponent: Story<Props> = (args) => <Component {...args} />;

export const EndOfNotifications = EndOfNotificationsComponent.bind({});
EndOfNotifications.args = {};
