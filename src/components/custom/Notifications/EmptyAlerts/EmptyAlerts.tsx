import { Text } from 'Components/base/typography/Text';
import { ReactComponent as EmptyAlert } from 'Svg/no-alerts.svg';
import classNames from 'classnames';
import React, { ReactElement } from 'react';

import style from './EmptyAlerts.module.scss';
import { EmptyAlertsProps } from './EmptyAlerts.types';

// Reminder: do not forget to add the following code to root index.ts
// export { EmptyAlerts } from './components/custom/Notifications/EmptyAlerts';

const EmptyAlerts = ({ className }: EmptyAlertsProps): ReactElement => (
  <div className={classNames(style.container, className)}>
    <EmptyAlert />
    <Text color="grey-100" type="body-regular-14" align="center">
      All clear. No alert right now.
    </Text>
  </div>
);

export default EmptyAlerts;
