import { Meta, Story } from '@storybook/react';
import {
  EmptyAlerts as Component,
  EmptyAlertsProps as Props,
} from 'Components/custom/Notifications/EmptyAlerts';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'components/custom/Notifications/EmptyAlerts',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const EmptyAlertsComponent: Story<Props> = (args) => <Component {...args} />;

export const EmptyAlerts = EmptyAlertsComponent.bind({});
EmptyAlerts.args = {};
