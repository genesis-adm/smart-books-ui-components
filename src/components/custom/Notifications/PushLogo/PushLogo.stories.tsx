import { Meta, Story } from '@storybook/react';
import {
  PushLogo as Component,
  PushLogoProps as Props,
} from 'Components/custom/Notifications/PushLogo';
import { ReactComponent as EditIcon } from 'Svg/8/pencil.svg';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'components/custom/Notifications/PushLogo',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const PushLogoComponent: Story<Props> = (args) => <Component {...args} />;

export const PushLogo = PushLogoComponent.bind({});
PushLogo.args = {
  name: 'Vitaliy Kvasha',
  // img: 'https://picsum.photos/100',
  border: 'none',
  radius: 'rounded',
  icon: <EditIcon />,
  iconBackground: 'violet-90',
};
