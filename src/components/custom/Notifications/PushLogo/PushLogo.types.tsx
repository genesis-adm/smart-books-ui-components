import { BackgroundColorNewTypes } from 'Components/types/colorTypes';
import React from 'react';

export interface PushLogoProps {
  className?: string;
  border?: 'white' | 'dark' | 'light' | 'none';
  radius?: 'default' | 'rounded';
  name: string;
  color?: string | undefined;
  img?: string | null | undefined;
  icon?: React.ReactNode;
  iconBackground?: BackgroundColorNewTypes;
  onClick?(): void;
}
