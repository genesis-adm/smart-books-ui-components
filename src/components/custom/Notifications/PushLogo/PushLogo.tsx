import { Text } from 'Components/base/typography/Text';
import hexToRGB from 'Utils/hexToRGB';
import initials from 'Utils/initials';
import classNames from 'classnames';
import React, { ReactElement, useMemo } from 'react';

import style from './PushLogo.module.scss';
import { PushLogoProps } from './PushLogo.types';

// Reminder: do not forget to add the following code to root index.ts
// export { PushLogo } from './components/custom/Notifications/PushLogo';

const PushLogo = ({
  className,
  name,
  img,
  border,
  radius,
  onClick,
  color,
  icon,
  iconBackground,
}: PushLogoProps): ReactElement => {
  const defaultColor = '#6367F6';
  const actualColor = color || defaultColor;
  const bgColor = useMemo(
    () => (!img ? { background: hexToRGB(actualColor, '.12') } : {}),
    [img, actualColor],
  );
  return (
    <div className={classNames(style.component, className)}>
      <div
        role="presentation"
        style={bgColor}
        className={classNames(
          style.container,
          style[`border_${border}`],
          style[`radius_${radius}`],
          {
            [style.avatar]: !img,
            [style.wrapper]: img,
          },
        )}
        onClick={onClick}
      >
        {!img && (
          <Text type="body-regular-14" color="violet-90">
            {initials(name)}
          </Text>
        )}
        {img && <img className={style.wrapper_img} src={img} alt={name} draggable={false} />}
        <div className={classNames(style.status_overlay, style[`background_${iconBackground}`])}>
          <div className={style.icon}>{icon}</div>
        </div>
      </div>
    </div>
  );
};

export default PushLogo;
