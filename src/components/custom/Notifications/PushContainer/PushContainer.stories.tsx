import { Meta, Story } from '@storybook/react';
import {
  PushContainer as Component,
  PushContainerProps as Props,
} from 'Components/custom/Notifications/PushContainer';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'components/custom/Notifications/PushContainer',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const PushContainerComponent: Story<Props> = (args) => <Component {...args} />;

export const PushContainer = PushContainerComponent.bind({});
PushContainer.args = {
  unread: true,
  onClick() {
    console.log('clicked');
  },
};
