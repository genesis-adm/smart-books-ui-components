import { Tooltip } from 'Components/base/Tooltip';
import classNames from 'classnames';
import React, { ReactElement } from 'react';

import style from './PushContainer.module.scss';
import { PushContainerProps } from './PushContainer.types';

// Reminder: do not forget to add the following code to root index.ts
// export { PushContainer } from './components/custom/Notifications/PushContainer';

const PushContainer = ({
  className,
  children,
  unread,
  onClick,
}: PushContainerProps): ReactElement => (
  <div role="presentation" className={classNames(style.component, className)}>
    <Tooltip message="Mark as read">
      <div className={classNames(style.dot, { [style.hide_dot]: !unread })} onClick={onClick} />
    </Tooltip>
    {children}
  </div>
);

export default PushContainer;
