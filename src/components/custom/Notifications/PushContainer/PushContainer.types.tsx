import React from 'react';

export interface PushContainerProps {
  className?: string;
  children: React.ReactNode;
  unread?: boolean;
  onClick?(): void;
}
