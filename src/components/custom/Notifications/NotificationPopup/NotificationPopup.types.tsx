import React, { ReactNode } from 'react';

export interface NotificationPopupProps {
  showPopup?: boolean;
  children?: ReactNode;

  className?: string;
  style?: React.CSSProperties;

  shortDelay?: number;
  longDelay?: number;
  onTimeoutAction?(): void;
  onCloseAction?(): void;
}
