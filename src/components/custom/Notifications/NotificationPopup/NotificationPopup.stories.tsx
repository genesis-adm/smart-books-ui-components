import { Meta, Story } from '@storybook/react';
import {
  NotificationPopup as Component,
  NotificationPopupProps as Props,
} from 'Components/custom/Notifications/NotificationPopup';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Notifications/NotificationPopup',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const NotificationPopupComponent: Story<Props> = (args) => <Component {...args} />;

export const NotificationPopup = NotificationPopupComponent.bind({});
NotificationPopup.args = {};
