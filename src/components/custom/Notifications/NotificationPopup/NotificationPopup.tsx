import { IconButton } from 'Components/base/buttons/IconButton';
import { ReactComponent as CloseIcon } from 'Svg/v2/16/cross.svg';
import classNames from 'classnames';
import React, { ReactElement, useEffect, useRef, useState } from 'react';

import styles from './NotificationPopup.module.scss';
import { NotificationPopupProps } from './NotificationPopup.types';

const SHORT_DElAY = 3000;
const LONG_DElAY = 5000;

const NotificationPopup = ({
  children,
  showPopup,
  shortDelay = SHORT_DElAY,
  longDelay = LONG_DElAY,
  onTimeoutAction,
  onCloseAction,
  className,
  style,
}: NotificationPopupProps): ReactElement => {
  const [isVisible, setIsVisible] = useState<boolean>(false);
  const [isHovering, setIsHovering] = useState(false);

  const prevHoverRef = useRef<boolean>(false);

  const closePopupManually = () => {
    setIsVisible(false);
    onCloseAction?.();
  };

  useEffect(() => {
    prevHoverRef.current = isHovering;
  }, [isHovering]);

  const timerDuration = prevHoverRef.current ? shortDelay : longDelay;

  useEffect(() => {
    if (!isHovering) {
      const timer = setTimeout(() => {
        setIsVisible(false);
        onTimeoutAction?.();
      }, timerDuration);

      return () => {
        clearTimeout(timer);
      };
    }
  }, [isHovering]);

  useEffect(() => {
    setIsVisible(showPopup || false);
  }, [showPopup]);

  const handleMouseEnter = () => setIsHovering(true);
  const handleMouseLeave = () => setIsHovering(false);

  if (!isVisible) return <></>;

  return (
    <div
      className={classNames(styles.wrapper, className)}
      style={style}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
    >
      <div className={styles.contentContainer}>
        <div className={styles.childrenContainer}>{children}</div>
        <IconButton
          icon={<CloseIcon />}
          size="24"
          background={'transparent-grey-20'}
          color={'grey-100'}
          className={styles.closeBtn}
          tooltip="Close"
          onClick={closePopupManually}
        />
      </div>
      <span className={styles.popupArrow}></span>
    </div>
  );
};

export default NotificationPopup;
