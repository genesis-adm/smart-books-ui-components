import { Meta, Story } from '@storybook/react';
import {
  UserProfileCard as Component,
  UserProfileCardProps as Props,
} from 'Components/custom/Users/UserProfileCard';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Users/User Profile Card',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const UserProfileCardComponent: Story<Props> = (args) => <Component {...args} />;

export const UserProfileCard = UserProfileCardComponent.bind({});
UserProfileCard.args = {
  name: 'Vitalii Kvasha',
  status: 'accepted',
  position: 'Project manager',
  email: 'vitaliikvasha@gmail.com',
  phone: '+380442569991',
};
