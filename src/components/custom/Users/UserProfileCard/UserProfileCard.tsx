import { Divider } from 'Components/base/Divider';
import { Tag, TagColorType } from 'Components/base/Tag';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as UserHeader } from 'Svg/colored/userheader.svg';
import { ReactComponent as CrossIcon } from 'Svg/v2/16/cross-in-circle.svg';
import { ReactComponent as EnvelopeIcon } from 'Svg/v2/16/envelope-filled.svg';
import { ReactComponent as ExclamationIcon } from 'Svg/v2/16/exclamation-in-circle.svg';
import { ReactComponent as PhoneIcon } from 'Svg/v2/16/phone.svg';
import { ReactComponent as TickIcon } from 'Svg/v2/16/tick-in-circle.svg';
import { ReactComponent as TickCircleIcon } from 'Svg/v2/16/tick-in-filled-circle.svg';
import initials from 'Utils/initials';
import classNames from 'classnames';
import React, { ReactElement } from 'react';

import style from './UserProfileCard.module.scss';

type StatusType = 'accepted' | 'invited' | 'blocked';

interface StatusPresetType {
  text: string;
  color: Extract<TagColorType, 'green' | 'yellow' | 'red'>;
  icon: JSX.Element;
}

export interface UserProfileCardProps {
  name: string;
  status: StatusType;
  position: string;
  email: string;
  phone: string;
}

const StatusPreset: Record<StatusType, StatusPresetType> = {
  accepted: {
    text: 'Accepted',
    color: 'green',
    icon: <TickIcon />,
  },
  invited: {
    text: 'Invited',
    color: 'yellow',
    icon: <ExclamationIcon />,
  },
  blocked: {
    text: 'Blocked',
    color: 'red',
    icon: <CrossIcon />,
  },
};

const UserProfileCard = ({
  name,
  status,
  position,
  email,
  phone,
}: UserProfileCardProps): ReactElement => (
  <div className={style.component}>
    <div className={style.header}>
      <UserHeader />
      <div className={style.logo}>
        <Text type="body-medium" color="white-100">
          {initials(name)}
        </Text>
      </div>
    </div>
    <div className={style.main}>
      <div className={style.title}>
        <Text type="title-bold">{name}</Text>
        <Tag
          icon={StatusPreset[status].icon}
          color={StatusPreset[status].color}
          text={StatusPreset[status].text}
          padding="4-8"
          cursor="default"
        />
      </div>
      <Text className={style.position} type="body-regular-14" color="grey-100">
        {position}
      </Text>
      <div className={style.contacts}>
        <Tag color="grey" cursor="default" shape="circle" icon={<EnvelopeIcon />} />
        <div className={style.data}>
          <Text type="caption-regular">Email:</Text>
          <Text noWrap type="body-regular-16">
            {email}
          </Text>
        </div>
      </div>
      <Divider className={style.divider} fullHorizontalWidth />
      <div className={style.contacts}>
        <Tag color="grey" cursor="default" shape="circle" icon={<PhoneIcon />} />
        <div className={style.data}>
          <Text type="caption-regular">Phone:</Text>
          <Text type="body-regular-16">{phone}</Text>
        </div>
      </div>
    </div>
  </div>
);

export default UserProfileCard;
