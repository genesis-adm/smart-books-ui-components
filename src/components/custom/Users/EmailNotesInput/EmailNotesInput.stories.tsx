import { Meta, Story } from '@storybook/react';
import {
  EmailNotesInput as Component,
  EmailNotesInputProps as Props,
} from 'Components/custom/Users/EmailNotesInput';
import { TagType } from 'Components/custom/Users/EmailNotesInput/TagsEditable/TagsEditable.types';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Users/Email Notes Input',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const EmailNotesInputComponent: Story<Props> = (args) => <Component {...args} />;

export const EmailNotesInput = EmailNotesInputComponent.bind({});
EmailNotesInput.args = {
  onChange(tags: TagType[]) {},
};
