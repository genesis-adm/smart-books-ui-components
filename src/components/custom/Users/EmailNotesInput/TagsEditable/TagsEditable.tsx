import { Tooltip } from 'Components/base/Tooltip';
import { InputTag } from 'Components/base/inputs/InputElements/InputTag';
import { modifyTextColorPartial } from 'Utils/search';
import classNames from 'classnames';
import React, { ChangeEvent, ReactElement, useCallback, useEffect, useState } from 'react';

import style from './TagsEditable.module.scss';
import { TagType, TagsEditableProps } from './TagsEditable.types';

// Reminder: do not forget to add the following code to root index.ts
// export { TagsEditable } from './components/custom/Users/EmailNotesInput/TagsEditable';

const getEmptyTag = () => ({ id: Date.now(), label: '' });

const TagsEditable = React.forwardRef<HTMLInputElement, TagsEditableProps>(
  (
    {
      onChange,
      tagColor,
      tagWidth,
      paddingBottom = '4',
      className,
      value,
      search = '',
      placeholder = 'Invite Users by Email',
      isEditable,
      autoFocus,
      readOnly,
      allowSpaces,
      validationPattern,
      validateValues,
    },
    ref,
  ): ReactElement => {
    const [tags, setTags] = useState<TagType[]>(() => [getEmptyTag()]);
    const [editableTagId, setIsEditing] = useState<number | null>();
    const [isHelperShown, setIsHelperShown] = useState<boolean>(false);
    const trimCollectionLast = (collection: TagType[]) =>
      collection.slice(0, collection.length - 1);

    const updateTags = useCallback(
      (index: number) => {
        let newTags = validateValues ? validateValues(tags) : [...tags];

        if (index === newTags.length - 1) {
          newTags = [...newTags, getEmptyTag()];
        }

        setTags(newTags);
        onChange(trimCollectionLast(newTags));
        setIsEditing(null);
      },
      [onChange, tags, validateValues],
    );

    const handleTagClick = useCallback(
      ({ id }: TagType) =>
        () => {
          isEditable && setIsEditing(id);
        },
      [isEditable],
    );

    const handleInputChange = useCallback(
      (index: number) => (e: ChangeEvent<HTMLInputElement>) => {
        let items = [...tags];
        const item = { ...items[index] };
        item.label = allowSpaces ? e.target.value : e.target.value.replace(/\s/g, '');
        items[index] = item;

        validationPattern && setIsHelperShown(validationPattern.test(item.label));
        if (!item.label && editableTagId) {
          items = items.filter((_item) => _item.id !== item.id);
        }
        setTags(items);
      },
      [allowSpaces, editableTagId, tags, validationPattern],
    );

    const handleDeleteTag = useCallback(
      (itemId: number) => () => {
        const updatedTags = tags.filter(({ id }: TagType) => id !== itemId);
        const newTags = validateValues ? validateValues(updatedTags) : updatedTags;
        setTags(newTags);
        onChange(trimCollectionLast(newTags));
      },
      [onChange, tags, validateValues],
    );

    const handleHelperClick = useCallback((index: number) => () => updateTags(index), [updateTags]);

    const handleAdd = useCallback(
      (index: number) =>
        ({ key }: React.KeyboardEvent<HTMLInputElement>) => {
          if (key === 'Enter' && !!tags[index].label) {
            updateTags(index);
          }
        },
      [tags, updateTags],
    );

    useEffect(() => {
      if (value) setTags([...value, getEmptyTag()]);
    }, [value]);

    return (
      <div
        className={classNames(
          style.wrapper,
          { [style.empty]: !tags.length },
          style[`padding-bottom_${paddingBottom}`],
          className,
        )}
      >
        {tags.map(({ id, label, error }, index) => (
          <div key={id} className={className}>
            {editableTagId === id || index === tags.length - 1 ? (
              <div className={style.container}>
                <input
                  ref={ref}
                  className={style.input}
                  value={label}
                  onChange={handleInputChange(index)}
                  onKeyDown={handleAdd(index)}
                  spellCheck={false}
                  placeholder={placeholder}
                  autoFocus={!!editableTagId || autoFocus}
                />
                {isHelperShown && label && (
                  <div className={style.helper} onClick={handleHelperClick(index)}>
                    <div className={style.text}>Add email:</div>
                    <div
                      className={classNames(style.label, { [style.wrap__text]: label.length > 60 })}
                    >
                      {label}
                    </div>
                  </div>
                )}
              </div>
            ) : (
              <Tooltip message={isEditable ? 'Edit' : ''} cursor="pointer">
                <InputTag
                  key={id}
                  id={id.toString()}
                  label={modifyTextColorPartial(label, search)}
                  onDelete={handleDeleteTag(id)}
                  readOnly={readOnly}
                  background={
                    search?.length && label?.toLowerCase().includes(search.toLowerCase())
                      ? 'blue'
                      : tagColor
                  }
                  width={tagWidth}
                  onClick={handleTagClick({ id, label })}
                  errorMessage={error?.message}
                  isError={error?.isError}
                />
              </Tooltip>
            )}
          </div>
        ))}
      </div>
    );
  },
);

TagsEditable.displayName = 'TagsEditable';

export default TagsEditable;
