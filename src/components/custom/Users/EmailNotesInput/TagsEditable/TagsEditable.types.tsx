export interface TagType {
  id: number;
  label: string;
  onClick?(): void;
  error?: {
    isError: boolean;
    message?: string;
  };
}

export type TagBackground = 'violet' | 'grey' | 'grey_30';
export type TagWidth = 'auto' | '100' | '200';
export interface TagsEditableProps {
  tagColor?: TagBackground;
  tagWidth?: TagWidth;
  paddingBottom?: '0' | '4';
  className?: string;
  value?: TagType[];
  search?: string;
  placeholder?: string;
  isEditable?: boolean;
  autoFocus?: boolean;
  readOnly?: boolean;
  allowSpaces?: boolean;
  validationPattern?: RegExp;
  onChange(tags: TagType[]): void;
  validateValues?: (tags: TagType[]) => TagType[];
}

export interface TagsEditableRef {
  resetTags: () => void;
}
