import { Divider } from 'Components/base/Divider';
import { Icon } from 'Components/base/Icon';
import { TagsEditable } from 'Components/custom/Users/EmailNotesInput/TagsEditable';
import { TagType } from 'Components/custom/Users/EmailNotesInput/TagsEditable/TagsEditable.types';
import { ReactComponent as EnvelopeFilledIcon } from 'Svg/v2/16/envelope-filled.svg';
import { ReactComponent as PinIcon } from 'Svg/v2/16/pin.svg';
import classNames from 'classnames';
import React, { ChangeEvent, ReactElement } from 'react';

import style from './EmailNotesInput.module.scss';

const emailRegex = new RegExp(/^[^\s@]+@[^\s@]+\.[^\s@]+$/);

export interface EmailNotesInputProps {
  notesValue: string;
  onNotesChange(e: ChangeEvent<HTMLInputElement>): void;
  className?: string;
  onChange(tags: TagType[]): void;
}

const EmailNotesInput = ({
  notesValue,
  onChange,
  onNotesChange,
  className,
}: EmailNotesInputProps): ReactElement => {
  const handleNotesChange = (e: ChangeEvent<HTMLInputElement>) => {
    onNotesChange?.(e);
  };

  return (
    <div className={classNames(style.wrapper, className)}>
      <div className={style.mail}>
        <Icon className={style.envelope} icon={<EnvelopeFilledIcon />} path="grey-100" />
        <TagsEditable
          validationPattern={emailRegex}
          validateValues={validateValues}
          onChange={onChange}
          paddingBottom="0"
          tagColor="grey"
          tagWidth="auto"
          autoFocus
          isEditable
        />
      </div>

      <Divider fullHorizontalWidth />

      <div className={style.notes}>
        <Icon icon={<PinIcon />} path="grey-100" />

        <input
          className={style.input}
          value={notesValue}
          onChange={handleNotesChange}
          spellCheck={false}
          placeholder="Notes"
        />
      </div>
    </div>
  );
};

export default EmailNotesInput;

const validateValues = (tags: TagType[]) => {
  return tags.map((tag) => ({ ...tag, error: { isError: !emailRegex.test(tag.label) } }));
};
