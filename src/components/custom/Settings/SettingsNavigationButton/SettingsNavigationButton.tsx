import classNames from 'classnames';
import React from 'react';

import style from './SettingsNavigationButton.module.scss';

export interface SettingsNavigationButtonProps {
  type?: 'general' | 'action';
  title: string;
  selected?: boolean;
  icon?: React.ReactNode;
  className?: string;
  onClick(): void;
}

const SettingsNavigationButton: React.FC<SettingsNavigationButtonProps> = ({
  type = 'general',
  title,
  selected,
  icon,
  className,
  onClick,
}) => (
  <button
    type="button"
    title={title}
    className={classNames(style.button, style[type], className, {
      [style.selected]: selected,
    })}
    onClick={onClick}
  >
    {icon && <span className={style.icon}>{icon}</span>}
    <span className={style.text}>{title}</span>
  </button>
);

export default SettingsNavigationButton;
