import { Scrollbar } from 'Components/base/Scrollbar';
import classNames from 'classnames';
import React from 'react';

import style from './SettingsNavigation.module.scss';

interface SettingsNavigationProps {
  className?: string;
  children?: React.ReactNode;
}

const SettingsNavigation: React.FC<SettingsNavigationProps> = ({ className, children }) => (
  <div className={classNames(style.container, className)}>{children}</div>
);

SettingsNavigation.defaultProps = {
  className: '',
};

export default SettingsNavigation;
