import { Modal, ModalHeader } from 'Components/modules/Modal';
import React from 'react';

interface ModalSettingsProps {
  title?: string;
  className?: string;
  children?: React.ReactNode;
  onClose?(): void;
}

const ModalSettings: React.FC<ModalSettingsProps> = ({ title, children, onClose, className }) => (
  <Modal
    size="full"
    overlay="grey-10"
    background="grey-10"
    className={className}
    pluginScrollDisabled
    header={
      <ModalHeader
        backgroundColor="white-100"
        headerHeight="small"
        title={title}
        titlePosition="left"
        onClick={onClose}
        radius
      />
    }
  >
    {children}
  </Modal>
);

ModalSettings.defaultProps = {
  title: '',
  className: '',
  onClose: () => {},
};

export default ModalSettings;
