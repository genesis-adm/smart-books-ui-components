import { Button } from 'Components/base/buttons/Button';
import { ButtonGroup } from 'Components/base/buttons/ButtonGroup';
import { Modal, ModalFooter, ModalHeader } from 'Components/modules/Modal';
import React from 'react';

interface SettingsModalProps {
  title?: string;
  textButtonLeft: string;
  textButtonRight: string;
  children?: React.ReactNode;
  onClose(): void;
  onClickLeft(): void;
  onClickRight(): void;
}

const SettingsModal: React.FC<SettingsModalProps> = ({
  title,
  textButtonLeft,
  textButtonRight,
  children,
  onClose,
  onClickLeft,
  onClickRight,
}) => (
  <Modal
    size="sm"
    centered
    overlay="black-100"
    header={
      <ModalHeader
        backgroundColor="grey-10"
        headerHeight="small"
        title={title}
        titlePosition="center"
        onClick={onClose}
      />
    }
    footer={
      <ModalFooter justifycontent="center">
        <ButtonGroup align="center">
          <Button width="md" onClick={onClickLeft}>
            {textButtonLeft}
          </Button>
          <Button width="md" onClick={onClickRight}>
            {textButtonRight}
          </Button>
        </ButtonGroup>
      </ModalFooter>
    }
  >
    {children}
  </Modal>
);

SettingsModal.defaultProps = {
  title: '',
};

export default SettingsModal;
