import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Text } from 'Components/base/typography/Text';
import React from 'react';

import style from './SettingsInfoAction.module.scss';

interface SettingsInfoActionProps {
  title: string;
  value: string;
  actionText?: string;
  onClick?(): void;
}

const SettingsInfoAction: React.FC<SettingsInfoActionProps> = ({
  title,
  value,
  actionText,
  onClick,
}) => (
  <div className={style.container}>
    <Text className={style.title} type="body-regular-14" color="grey-100">
      {title}
    </Text>
    <div className={style.wrapper}>
      <Text className={style.value} type="body-regular-14">
        {value}
      </Text>
      {onClick && <LinkButton onClick={onClick}>{actionText}</LinkButton>}
    </div>
  </div>
);

SettingsInfoAction.defaultProps = {
  actionText: '',
  onClick: () => {},
};

export default SettingsInfoAction;
