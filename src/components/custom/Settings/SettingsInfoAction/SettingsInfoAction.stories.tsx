import { SettingsInfoAction as Component } from 'Components/custom/Settings/SettingsInfoAction';
import React from 'react';

export default {
  title: 'Components/Custom/Settings/Settings Info Action',
};

export const SettingsInfoAction: React.FC = () => (
  <Component
    title="Email address"
    value="karine.mnatsakanian@mail.com"
    actionText="Change"
    onClick={() => {}}
  />
);
