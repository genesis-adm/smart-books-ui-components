import { ButtonGroup } from 'Components/base/buttons/ButtonGroup';
import { Modal, ModalFooter, ModalHeader } from 'Components/modules/Modal';
import React from 'react';

export interface SettingsModalBDProps {
  title?: string;
  footer: React.ReactNode;
  children?: React.ReactNode;
  onClose(): void;
}

const SettingsModalBD: React.FC<SettingsModalBDProps> = ({ title, footer, children, onClose }) => (
  <Modal
    size="md-narrow"
    centered
    overlay="black-100"
    header={
      <ModalHeader
        backgroundColor="grey-10"
        headerHeight="small"
        title={title}
        titlePosition="center"
        onClick={onClose}
      />
    }
    footer={
      <ModalFooter justifycontent="center">
        <ButtonGroup align="center">{footer}</ButtonGroup>
      </ModalFooter>
    }
  >
    {children}
  </Modal>
);

export default SettingsModalBD;
