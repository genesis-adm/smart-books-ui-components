import { ButtonGroup } from 'Components/base/buttons/ButtonGroup';
import { Modal, ModalFooter, ModalHeader } from 'Components/modules/Modal';
import React from 'react';

interface SettingsModalBDWideProps {
  title?: string;
  footer?: React.ReactNode;
  children?: React.ReactNode;
  onClose(): void;
}

const SettingsModalBDWide: React.FC<SettingsModalBDWideProps> = ({
  title,
  footer,
  children,
  onClose,
}) => (
  <Modal
    size="md-extrawide"
    centered
    overlay="black-100"
    header={
      <ModalHeader
        backgroundColor="grey-10"
        headerHeight="small"
        title={title}
        titlePosition="center"
        onClick={onClose}
      />
    }
    footer={
      footer && (
        <ModalFooter justifycontent="center">
          <ButtonGroup align="center">{footer}</ButtonGroup>
        </ModalFooter>
      )
    }
  >
    {children}
  </Modal>
);

SettingsModalBDWide.defaultProps = {
  footer: null,
  title: '',
};

export default SettingsModalBDWide;
