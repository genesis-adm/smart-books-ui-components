import { Divider } from 'Components/base/Divider';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import React from 'react';

export interface SettingsTitleProps {
  title: string;
  className?: string;
}

const SettingsTitle: React.FC<SettingsTitleProps> = ({ title, className }) => (
  <Container className={className} alignitems="center">
    <Text type="text-medium" color="grey-100">
      {title}
    </Text>
    <Divider type="horizontal" position="right" />
  </Container>
);

export default SettingsTitle;
