import { Meta, Story } from '@storybook/react';
import {
  SettingsTitle as Component,
  SettingsTitleProps as Props,
} from 'Components/custom/Settings/SettingsTitle';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Settings/Settings Title',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const SettingsTitleComponent: Story<Props> = (args) => <Component {...args} />;

export const SettingsTitle = SettingsTitleComponent.bind({});
SettingsTitle.args = {
  title: 'Authentication',
};
