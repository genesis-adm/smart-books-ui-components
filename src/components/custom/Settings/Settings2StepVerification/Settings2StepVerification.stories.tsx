import { Settings2StepVerification as Component } from 'Components/custom/Settings/Settings2StepVerification';
import React from 'react';

export default {
  title: 'Components/Custom/Settings/Settings 2-Step Verification',
};

export const Settings2StepVerification: React.FC = () => <Component onChange={() => {}} />;
