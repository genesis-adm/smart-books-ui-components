import { Switch } from 'Components/base/Switch';
import { Text } from 'Components/base/typography/Text';
import React from 'react';

import style from './Settings2StepVerification.module.scss';

interface Settings2StepVerificationProps {
  onChange(): void;
}

const Settings2StepVerification: React.FC<Settings2StepVerificationProps> = ({ onChange }) => (
  <div className={style.container}>
    <div className={style.action}>
      <Text className={style.title} type="body-regular-14" color="grey-100">
        Two-Step Authentication
      </Text>
      <Switch id="default" checked onChange={onChange} />
    </div>
    <Text className={style.description} type="text-regular">
      Email Two-Step Authentication enabled for each user.
    </Text>
    <Text className={style.description} type="text-regular">
      All accounts are protected with an additional password.
    </Text>
    <Text className={style.description} type="text-regular">
      Users cannot finish a registration without confirmation verification code
    </Text>
  </div>
);

export default Settings2StepVerification;
