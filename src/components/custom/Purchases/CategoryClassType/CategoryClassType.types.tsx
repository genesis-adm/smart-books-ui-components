export interface CategoryClassTypeProps {
  className?: string;
  name: string;
  icon?: React.ReactNode;
  added?: boolean;
  onClick(): void;
}
