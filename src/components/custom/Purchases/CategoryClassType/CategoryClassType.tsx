import { Icon } from 'Components/base/Icon';
import { Button } from 'Components/base/buttons/Button';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as CheckIcon } from 'Svg/16/check.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import classNames from 'classnames';
import React, { ReactElement } from 'react';

import style from './CategoryClassType.module.scss';
import { CategoryClassTypeProps } from './CategoryClassType.types';

// Reminder: do not forget to add the following code to root index.ts
// export { CategoryClassType } from './components/custom/Purchases/CategoryClassType';

const CategoryClassType = ({
  className,
  name,
  icon,
  onClick,
  added,
}: CategoryClassTypeProps): ReactElement => (
  <div className={classNames(style.component, className, { [style.added]: added })}>
    <div className={style.text_wrapper}>
      <Icon icon={icon} path="inherit" />
      <Text type="body-medium" color="inherit">
        {name}
      </Text>
    </div>
    {!added ? (
      <Button iconLeft={<PlusIcon />} onClick={onClick} background="transparent" color="inherit">
        Add
      </Button>
    ) : (
      <Button iconLeft={<CheckIcon />} background="blue-10" height="28" color="violet-90">
        Added
      </Button>
    )}
  </div>
);

export default CategoryClassType;
