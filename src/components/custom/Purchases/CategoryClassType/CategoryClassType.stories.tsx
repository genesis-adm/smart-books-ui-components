import { Meta, Story } from '@storybook/react';
import {
  CategoryClassType as Component,
  CategoryClassTypeProps as Props,
} from 'Components/custom/Purchases/CategoryClassType';
import { ReactComponent as SpeakerIcon } from 'Svg/categories/16/speakerphone.svg';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Purchases/CategoryClassType',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const CategoryClassTypeComponent: Story<Props> = (args) => <Component {...args} />;

export const CategoryClassType = CategoryClassTypeComponent.bind({});
CategoryClassType.args = { name: 'Advertising', icon: <SpeakerIcon /> };
