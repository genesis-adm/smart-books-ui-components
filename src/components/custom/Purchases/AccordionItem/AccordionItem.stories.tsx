import { Meta, Story } from '@storybook/react';
import {
  AccordionItem as Component,
  AccordionItemProps as Props,
} from 'Components/custom/Purchases/AccordionItem';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Purchases/AccordionItem',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const AccordionItemComponent: Story<Props> = (args) => <Component {...args} />;

export const AccordionItem = AccordionItemComponent.bind({});
AccordionItem.args = {};
