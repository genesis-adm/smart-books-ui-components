import { DropDown } from 'Components/DropDown';
import { Icon } from 'Components/base/Icon';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as MinusCircleIcon } from 'Svg/16/minus-circle.svg';
import { ReactComponent as ChevronIcon } from 'Svg/v2/16/chevron-right.svg';
import { ReactComponent as DocumentIcon } from 'Svg/v2/16/file-blank.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as DropdownIcon } from 'Svg/v2/16/three-dots.svg';
import classNames from 'classnames';
import React, { MouseEvent, ReactElement, useState } from 'react';

import style from './AccordionItem.module.scss';
import { AccordionItemProps } from './AccordionItem.types';

// Reminder: do not forget to add the following code to root index.ts
// export { AccordionItem } from './components/custom/Purchases/AccordionItem';

const AccordionItem = ({
  name,
  color = 'black-100',
  counter,
  state = 'draft',
  isActive,
  addButtonShown = true,
  icon,
  className,
  onClick,
  onAddClick,
  children,
  dropdown,
}: AccordionItemProps): ReactElement => {
  const [isActionsOpen, setActionsOpen] = useState<boolean>(false);
  const handleAddClick = (e: MouseEvent<HTMLDivElement>) => {
    e.stopPropagation();
    onAddClick();
  };
  const handleCurrentState = (arg: boolean): void => {
    setActionsOpen(arg);
  };
  return (
    <div className={classNames(style.wrapper, className)}>
      <div
        className={classNames(style.item, {
          [style.active]: isActive,
          [style.disabled]: state === 'disabled',
          [style.actionsOpen]: isActionsOpen,
        })}
        onClick={onClick}
      >
        <div className={style.main}>
          <Icon
            className={classNames(style.arrow, { [style.active]: isActive })}
            icon={<ChevronIcon />}
            path="black-100"
            clickThrough
            transition="inherit"
          />
          <Icon icon={icon} path="grey-100" clickThrough />
          <Text type="body-medium" color={isActive ? 'black-100' : color}>
            {name}
          </Text>
        </div>
        {counter && (
          <Text className={style.counter} type="body-medium">
            {counter}
          </Text>
        )}
        {state === 'draft' && (
          <Icon icon={<DocumentIcon />} path="grey-90" className={style.state_icon} />
        )}
        {state === 'disabled' && (
          <Icon icon={<MinusCircleIcon />} path="grey-90" className={style.state_icon} />
        )}
        <DropDown
          currentStateInfo={handleCurrentState}
          classNameOuter={style.actions}
          flexdirection="column"
          position="bottom-right"
          padding="8"
          control={({ handleOpen }) => (
            <IconButton
              icon={<DropdownIcon />}
              shape="square"
              size="24"
              onClick={(e: MouseEvent<HTMLDivElement>) => {
                e.stopPropagation();
                handleOpen();
              }}
              background="transparent"
              color="grey-100-violet-90"
            />
          )}
        >
          {dropdown}
        </DropDown>
        {addButtonShown && (
          <IconButton
            className={style.add}
            icon={<PlusIcon />}
            color="grey-100"
            background="transparent"
            shape="square"
            size="24"
            onClick={handleAddClick}
          />
        )}
      </div>
      {isActive && children}
    </div>
  );
};

export default AccordionItem;
