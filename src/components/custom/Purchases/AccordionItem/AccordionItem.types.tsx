import { TextColorNewTypes } from 'Components/types/colorTypes';
import { ReactNode } from 'react';

type ItemState = 'active' | 'draft' | 'disabled';

export interface AccordionItemProps {
  name: string;
  color?: TextColorNewTypes;
  state?: ItemState;
  dropdown?: ReactNode;
  counter?: number;
  isActive: boolean;
  addButtonShown?: boolean;
  icon?: ReactNode;
  className?: string;
  onClick(): void;
  onAddClick(): void;
  children?: ReactNode;
}
