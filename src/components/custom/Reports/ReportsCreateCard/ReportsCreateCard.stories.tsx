import { Meta, Story } from '@storybook/react';
import {
  ReportsCreateCard as Component,
  ReportsCreateCardProps as Props,
} from 'Components/custom/Reports/ReportsCreateCard';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Reports/ReportsCreateCard',
  component: Component,
  argTypes: {
    onCreate: hideProperty,
  },
} as Meta;

const ReportsCreateCardComponent: Story<Props> = (args) => <Component {...args} />;

export const ReportsCreateCard = ReportsCreateCardComponent.bind({});
ReportsCreateCard.args = {
  type: 'balance',
};
