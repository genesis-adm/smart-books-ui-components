import { Icon } from 'Components/base/Icon';
import { Button } from 'Components/base/buttons/Button';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as ReportsForThePeriodGraphic } from 'Svg/colored/report-for-the-period-graphic.svg';
import { ReactComponent as ReportsOutstandingBalanceChart } from 'Svg/colored/reports-outstanding-balance-graphic.svg';
import { ReactComponent as TrialBalanceChart } from 'Svg/colored/trialBalance.svg';
import { ReactComponent as AccountRegisterChart } from 'Svg/colored/trialRegister.svg';
import classNames from 'classnames';
import React, { ReactElement } from 'react';

import style from './ReportsCreateCard.module.scss';

const cardPreset = {
  balance: {
    title: 'Trial Balance Report',
    text: 'Trial Balance Report is a financial statement that summarizes the balances for each chart of account in the general ledger at a specific point in time, specifically for a particular Legal Entity or Business division.',
    chart: <TrialBalanceChart />,
  },
  register: {
    title: 'Account Register Report',
    text: 'Account Register Report is a financial statement that provides turnover of a selected chart of account in both the base currency and the original currency.',
    chart: <AccountRegisterChart />,
  },
  forThePeriod: {
    title: 'Trial Вalance for the period',
    text: 'Trial Balance for the Period is a financial report that summarizes the balances and turnovers of all the general ledger accounts within a specific accounting period for a particular Legal Entity or Business Unit in both base and original currency.',
    chart: <ReportsForThePeriodGraphic />,
  },
  bankStatement: {
    title: 'Bank Statement Report',
    text: 'Bank Statement Report is a financial report that provides a summary of the outstanding balances for all bank accounts as of selected date in both the base currency and the original currency',
    chart: <ReportsOutstandingBalanceChart />,
  },
};

type CardType = keyof typeof cardPreset;

export interface ReportsCreateCardProps {
  type: CardType;
  onCreate(): void;
}

const ReportsCreateCard = ({ type, onCreate }: ReportsCreateCardProps): ReactElement => (
  <div className={classNames(style.wrapper, style[`type_${type}`])}>
    <Text className={style.title} type="title-semibold" color="white-100">
      {cardPreset[type].title}
    </Text>
    <Text className={style.text} type="body-regular-14" color="grey-20" transition="unset">
      {cardPreset[type].text}
    </Text>
    <Button
      width="180"
      height="48"
      color="inherit"
      background="white-100"
      className={style.button}
      onClick={onCreate}
    >
      Create
    </Button>
    <Icon className={style.chart} icon={cardPreset[type].chart} />
  </div>
);

export default ReportsCreateCard;
