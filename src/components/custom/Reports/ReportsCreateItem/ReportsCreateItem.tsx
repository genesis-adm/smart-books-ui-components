import { Radio } from 'Components/base/Radio';
import { Tooltip } from 'Components/base/Tooltip';
import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React, { ReactElement, useEffect, useRef, useState } from 'react';

import style from './ReportsCreateItem.module.scss';

export type ReportsCreateItemProps = {
  id: string;
  mainTextColor?: 'grey-100' | 'black-100';
  name: string;
  checked: boolean;
  mainText: string;
  secondaryText?: string;
  className?: string;
  onClick(): void;
};

const ReportsCreateItem = ({
  id,
  mainTextColor = 'black-100',
  name,
  checked,
  mainText,
  secondaryText,
  className,
  onClick,
}: ReportsCreateItemProps): ReactElement => {
  const textPrimaryRef = useRef<HTMLDivElement>(null);
  const textSecondaryRef = useRef<HTMLDivElement>(null);
  const [tooltipValue, setTooltipValue] = useState<React.ReactNode>();
  const [secondaryTooltipValue, setSecondaryTooltipValue] = useState<React.ReactNode>();

  useEffect(() => {
    if (
      textPrimaryRef.current &&
      textPrimaryRef.current.scrollWidth > textPrimaryRef.current.clientWidth
    ) {
      setTooltipValue(mainText);
    }
  }, [mainText]);

  useEffect(() => {
    if (
      textSecondaryRef.current &&
      textSecondaryRef.current.scrollWidth > textSecondaryRef.current.clientWidth
    ) {
      setSecondaryTooltipValue(secondaryText);
    }
  }, [secondaryText]);

  return (
    <div
      onClick={onClick}
      className={classNames(style.component, { [style.checked]: checked }, className)}
    >
      <Radio id={id} name={name} checked={checked} onChange={onClick} />
      <div className={style.info}>
        <Tooltip message={tooltipValue}>
          <Text
            ref={textPrimaryRef}
            type="body-regular-14"
            className={classNames(style.text, style[`color_${mainTextColor}`])}
            color="inherit"
            noWrap
          >
            {mainText}
          </Text>
        </Tooltip>
        <Tooltip message={secondaryTooltipValue}>
          <Text ref={textSecondaryRef} color="grey-100" noWrap>
            {secondaryText}
          </Text>
        </Tooltip>
      </div>
    </div>
  );
};

export default ReportsCreateItem;
