import { Meta, Story } from '@storybook/react';
import { Container } from 'Components/base/grid/Container';
import {
  ReportsCreateItem as Component,
  ReportsCreateItemProps as Props,
} from 'Components/custom/Reports/ReportsCreateItem';
import React, { useState } from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Reports/ReportsCreateItem',
  component: Component,
  argTypes: {
    id: hideProperty,
    name: hideProperty,
    checked: hideProperty,
    mainText: hideProperty,
    secondaryText: hideProperty,
    className: hideProperty,
    onClick: hideProperty,
  },
} as Meta;

const ReportsCreateItemComponent: Story<Props> = (args) => {
  const [active, setActive] = useState<number | null>(null);

  return (
    <Container flexdirection="column" gap="4">
      {Array.from(Array(10)).map((_, index) => (
        <Component
          key={index}
          {...args}
          id={`${index}`}
          mainText={`Main text #${index + 1}`}
          secondaryText={`Secondary text #${index + 1}`}
          checked={active === index}
          onClick={() => setActive(index)}
        />
      ))}
    </Container>
  );
};

export const ReportsCreateItem = ReportsCreateItemComponent.bind({});
ReportsCreateItem.args = {};
