import { Meta, Story } from '@storybook/react';
import {
  ReportsTotalValues as Component,
  ReportsTotalValuesProps as Props,
} from 'Components/custom/Reports/ReportsTotalValues';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Reports/ReportsTotalValues',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const ReportsTotalValuesComponent: Story<Props> = (args) => <Component {...args} />;

const data = [
  ['EUR', '2000.00 €', '-1000.00 €'],
  ['EUR', '2000.00 €', '-1000.00 €'],
  ['EUR', '2000.00 €', '-1000.00 €'],
];

export const ReportsTotalValues = ReportsTotalValuesComponent.bind({});
ReportsTotalValues.args = {
  type: 'simple',
  title: 'Subtotal page 1',
  data,
  filterActive: true,
};
