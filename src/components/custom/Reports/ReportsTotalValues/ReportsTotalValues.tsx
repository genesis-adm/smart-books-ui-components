import { Icon } from 'Components/base/Icon';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as ChevronIcon } from 'Svg/v2/16/chevron-down.svg';
import classNames from 'classnames';
import React, { ReactElement, useState } from 'react';

import style from './ReportsTotalValues.module.scss';

interface DataType {
  width: string;
  value: string;
  textAlign?: 'left' | 'center' | 'right';
}

export interface ReportsTotalValuesProps {
  type: 'simple' | 'balance';
  title: string;
  data: string[][] | DataType[][];
  filterActive?: boolean;
  className?: string;
}

const ReportsTotalValues = ({
  type,
  title,
  data,
  filterActive,
  className,
}: ReportsTotalValuesProps): ReactElement => {
  const { length } = data;
  const [listOpened, setListOpened] = useState<boolean>(length < 3);

  const handleListToggle = () => {
    if (length > 2) {
      setListOpened((prev) => !prev);
    }
  };

  if (type === 'simple') {
    return (
      <div className={classNames(style.wrapper, className)}>
        <div
          className={classNames(style.component, {
            [style.showActiveTag]: filterActive,
            [style.mini]: !listOpened,
          })}
        >
          {length < 3 ? (
            <Text type="caption-semibold" color="grey-100">
              {title}
            </Text>
          ) : (
            <Container alignitems="flex-start">
              <LinkButton type="caption-semibold" onClick={handleListToggle}>
                {title} ({length})
              </LinkButton>
              <Icon
                className={classNames(style.arrow, { [style.active]: listOpened })}
                icon={<ChevronIcon />}
                path="violet-100"
                transition="inherit"
                onClick={handleListToggle}
                cursor="pointer"
              />
            </Container>
          )}
          <div className={style.data}>
            {data.map((row, index) => (
              <div key={`${row}-${index}`} className={style.row}>
                <Text
                  className={style.currency}
                  type="caption-semibold"
                  align="center"
                  style={getStyle(row[0])}
                >
                  {typeof row[0] === 'object' ? row[0].value : row[0]}
                </Text>
                {row.slice(1).map((item, index) => (
                  <Text
                    type="caption-semibold"
                    align="right"
                    key={`${item}-${index}`}
                    className={style.sum}
                    style={getStyle(item)}
                  >
                    {typeof item === 'object' ? item.value : item}
                  </Text>
                ))}
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }

  return (
    <div className={classNames(style.wrapper, className)}>
      <div
        className={classNames(style.component, {
          [style.showActiveTag]: filterActive,
          [style.mini]: !listOpened,
        })}
      >
        {length < 3 ? (
          <Text type="caption-semibold" color="grey-100">
            {title}
          </Text>
        ) : (
          <Container alignitems="flex-start">
            <LinkButton type="caption-semibold" onClick={handleListToggle}>
              {title} ({length})
            </LinkButton>
            <Icon
              className={classNames(style.arrow, { [style.active]: listOpened })}
              icon={<ChevronIcon />}
              path="violet-100"
              transition="inherit"
              onClick={handleListToggle}
              cursor="pointer"
            />
          </Container>
        )}
        <div className={style.data}>
          {data.map((row, index) => (
            <div key={`${row}-${index}`} className={style.row}>
              {row.map((item, index) => (
                <Text
                  type="caption-semibold"
                  align="right"
                  key={`${item}-${index}`}
                  className={style.sum}
                  style={getStyle(item)}
                >
                  {typeof item === 'object' ? item.value : item}
                </Text>
              ))}
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

const getStyle = (item: DataType | string) => {
  return typeof item === 'object' && 'width' in item
    ? { width: item.width, textAlign: item.textAlign }
    : undefined;
};

export default ReportsTotalValues;
