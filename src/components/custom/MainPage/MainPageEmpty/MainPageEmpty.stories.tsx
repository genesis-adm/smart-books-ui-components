import { Meta, Story } from '@storybook/react';
import {
  MainPageEmpty as Component,
  MainPageEmptyProps as Props,
} from 'Components/custom/MainPage/MainPageEmpty';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Main Page/Main Page Empty',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const MainPageEmptyComponent: Story<Props> = (args) => <Component {...args} />;

export const MainPageEmpty = MainPageEmptyComponent.bind({});
MainPageEmpty.args = {
  title: 'Create new Customers',
  description:
    'You have no transactions created. Your created transactions will be displayed here.',
};
