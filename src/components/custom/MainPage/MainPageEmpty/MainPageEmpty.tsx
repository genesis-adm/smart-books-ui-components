import { Text } from 'Components/base/typography/Text';
import { ReactComponent as Empty124Icon } from 'Svg/v2/124/empty.svg';
import React from 'react';

import style from './MainPageEmpty.module.scss';

export interface MainPageEmptyProps {
  title?: string;
  description: string;
  action?: React.ReactNode;
}

const MainPageEmpty: React.FC<MainPageEmptyProps> = ({ title, description, action }) => (
  <div className={style.component}>
    <Empty124Icon />
    <Text className={style.title} type="title-bold" align="center">
      {title}
    </Text>
    <Text className={style.description} type="body-regular-16" color="grey-100" align="center">
      {description}
    </Text>
    <div className={style.action}>{action}</div>
  </div>
);

export default MainPageEmpty;
