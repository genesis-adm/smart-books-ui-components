import { Container } from 'Components/base/grid/Container';
import { MenuTab, MenuTabWrapper } from 'Components/base/tabs/MenuTab';
import { Text } from 'Components/base/typography/Text';
import React, { ReactElement } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

export type Tab = { id: string; title: string };
type MenuTabsProps = { title: string; tabs: Tab[]; currentTabId: string };

const MenuTabs = ({ title, tabs, currentTabId }: MenuTabsProps): ReactElement => {
  return (
    <Container flexdirection="column" className={spacing.mX32}>
      <Text type="h1-semibold">{title}</Text>
      <MenuTabWrapper className={spacing.mt24}>
        {tabs.map((tab) => (
          <MenuTab id={tab.id} active={tab.id === currentTabId} onClick={() => {}}>
            {tab.title}
          </MenuTab>
        ))}
      </MenuTabWrapper>
    </Container>
  );
};

export default MenuTabs;
