import { DropDown } from 'Components/DropDown';
import { DropDownUser } from 'Components/DropDownUser';
import { BetaTag } from 'Components/base/BetaTag';
import { Divider } from 'Components/base/Divider';
import { Switch } from 'Components/base/Switch';
import { Button } from 'Components/base/buttons/Button';
import { ButtonGroup } from 'Components/base/buttons/ButtonGroup';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { GeneralTab, GeneralTabWrapper } from 'Components/base/tabs/GeneralTab';
import { Text } from 'Components/base/typography/Text';
import { ListsDropdown } from 'Components/custom/Lists/ListsDropdown';
import { ListsDropdownItem, ListsDropdownTitle } from 'Components/custom/Lists/ListsDropdownItem';
import EndOfNotifications from 'Components/custom/Notifications/EndOfNotifications/EndOfNotifications';
import { PushContainer } from 'Components/custom/Notifications/PushContainer';
import { PushLogo } from 'Components/custom/Notifications/PushLogo';
import { ReactComponent as BellIcon } from 'Svg/8/bell.svg';
import { ReactComponent as EditIcon } from 'Svg/8/pencil.svg';
import { ReactComponent as PlusIcon } from 'Svg/8/plus.svg';
import { ReactComponent as TrashIcon } from 'Svg/8/trash.svg';
import { ReactComponent as LogoutIcon } from 'Svg/16/logout.svg';
import { ReactComponent as SalesIcon } from 'Svg/v2/16/charts.svg';
import { ReactComponent as HistoryIcon } from 'Svg/v2/16/clock-filled.svg';
import { ReactComponent as DownloadIcon } from 'Svg/v2/16/download.svg';
import { ReactComponent as EyeIcon } from 'Svg/v2/16/eye.svg';
import { ReactComponent as AccountingIcon } from 'Svg/v2/16/lists-accounting.svg';
import { ReactComponent as BankingIcon } from 'Svg/v2/16/lists-banking.svg';
import { ReactComponent as OtherIcon } from 'Svg/v2/16/lists-other.svg';
import { ReactComponent as Plus16Icon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as GearIcon } from 'Svg/v2/16/settings.svg';
import { ReactComponent as DropdownIcon } from 'Svg/v2/16/three-dots.svg';
import { ReactComponent as UploadIcon } from 'Svg/v2/16/upload.svg';
import { ReactComponent as NotificationIcon } from 'Svg/v2/20/bell-filled.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';
import img from '../../../../assets/svg/smart-books.svg';

const HeaderSB: React.FC = () => {
  const [readNotification, setReadNotification] = useState(true);
  const [checkOnlyShowUnread, setCheckOnlyShowUnread] = useState(true);
  const handleReadNotification = () => {
    setReadNotification(false);
  };

  const handleCheckOnlyShowUnread = () => {
    setCheckOnlyShowUnread(!checkOnlyShowUnread);
  };
  return (
    <Container
      justifycontent="flex-end"
      className={classNames(spacing.mt12, spacing.mr16, spacing.ml16)}
    >
      <ButtonGroup align="right" nospace>
        <ListsDropdown>
          <Container flexdirection="column" gap="4">
            <ListsDropdownTitle label="Sales" icon={<SalesIcon />} />
            <ListsDropdownItem label="All sales" onClick={() => {}} />
            <ListsDropdownItem label="Customers" onClick={() => {}} />
            <ListsDropdownItem label="Products" onClick={() => {}} />
          </Container>
          <Container flexdirection="column" gap="4">
            <ListsDropdownTitle label="Banking" icon={<BankingIcon />} />
            <ListsDropdownItem label="Transactions" onClick={() => {}} />
            <ListsDropdownItem label="Bank accounts" onClick={() => {}} />
            <ListsDropdownItem label="Rules" onClick={() => {}} />
          </Container>
          <Container flexdirection="column" gap="4">
            <ListsDropdownTitle label="Accounting" icon={<AccountingIcon />} />
            <ListsDropdownItem label="Chart of accounts" onClick={() => {}} />
            <ListsDropdownItem label="Journals" onClick={() => {}} />
          </Container>
          <Divider type="vertical" height="auto" />
          <Container flexdirection="column" gap="4">
            <ListsDropdownTitle label="Other" icon={<OtherIcon />} />
            <ListsDropdownItem label="Import page" icon={<UploadIcon />} onClick={() => {}} />
            <ListsDropdownItem label="Export page" icon={<DownloadIcon />} onClick={() => {}} />
            <ListsDropdownItem
              label="Create integration"
              icon={<Plus16Icon />}
              onClick={() => {}}
            />
          </Container>
        </ListsDropdown>
        <Divider background={'grey-90'} type="vertical" className={spacing.mX20} />
        <BetaTag>
          <IconButton
            size="32"
            background={'transparent-blue-10'}
            color="grey-100-violet-90"
            tooltip="Audit log"
            icon={<HistoryIcon transform="scale(1.3)" />}
            onClick={() => {}}
          />
        </BetaTag>
        <Divider background={'grey-90'} type="vertical" className={spacing.mX20} />
        <DropDown
          padding="none"
          flexdirection="column"
          customScroll
          classnameInner={classNames(spacing.w500fixed)}
          control={({ handleOpen }) => (
            <IconButton
              size="32"
              background={'transparent-blue-10'}
              color="grey-100-violet-90"
              counter="2"
              tooltip="Notifications"
              icon={<NotificationIcon />}
              onClick={handleOpen}
            />
          )}
        >
          <Container
            flexdirection="column"
            position="fixed"
            background={'white-100'}
            className={classNames(spacing.w500fixed, spacing.z_ind2)}
          >
            <Container
              justifycontent="space-between"
              className={classNames(spacing.p24, spacing.pb_auto, spacing.h300max)}
            >
              <Text type="body-medium">Notifications</Text>
              <DropDown
                padding="10"
                control={({ handleOpen }) => (
                  <IconButton
                    icon={<DropdownIcon />}
                    onClick={handleOpen}
                    background={'transparent'}
                  />
                )}
              >
                <Container flexdirection="column" gap="8">
                  <DropDownButton onClick={() => {}} icon={<EyeIcon />} size="204">
                    Mark all as read
                  </DropDownButton>
                  <Divider fullHorizontalWidth />
                  <Container
                    alignitems="center"
                    gap="40"
                    className={classNames(spacing.pX8, spacing.pY12)}
                  >
                    <Text color="grey-100" type="caption-regular">
                      Only show unread
                    </Text>
                    <Switch
                      id="unread"
                      checked={checkOnlyShowUnread}
                      onChange={handleCheckOnlyShowUnread}
                    />
                  </Container>
                </Container>
              </DropDown>
            </Container>
            <GeneralTabWrapper className={spacing.pX24}>
              <GeneralTab id="all" active onClick={() => {}} counter={12}>
                All
              </GeneralTab>
              <GeneralTab id="information" active={false} onClick={() => {}} counter={2}>
                Information
              </GeneralTab>
              <GeneralTab id="alerts" active={false} onClick={() => {}} counter={10}>
                Alerts
              </GeneralTab>
            </GeneralTabWrapper>
          </Container>
          <Container
            flexdirection="column"
            className={spacing.mt90}
            style={{ background: 'linear-gradient( rgba(192, 196, 205, 0.10), transparent)' }}
          >
            <Text type="button" className={classNames(spacing.p24, spacing.pb_auto)}>
              Today
            </Text>
            <PushContainer unread={readNotification} onClick={handleReadNotification}>
              <Container alignitems="flex-start" gap="12">
                <PushLogo
                  img={img}
                  name="Vitaliy Kvasha"
                  icon={<BellIcon />}
                  iconBackground="violet-90"
                  radius="rounded"
                />
                <Container flexdirection="column">
                  <Text type="body-regular-14">File_name successfully uploaded number records</Text>
                  <Text type="text-regular" color="grey-100">
                    1 min ago
                  </Text>
                </Container>
              </Container>
            </PushContainer>
            <PushContainer>
              <Container alignitems="flex-start" gap="12">
                <PushLogo
                  img={img}
                  name="Vitaliy Kvasha"
                  icon={<BellIcon />}
                  iconBackground="violet-90"
                  radius="rounded"
                />
                <Container flexdirection="column">
                  <Text type="body-regular-14">Journals successfully exported</Text>
                  <Text type="text-regular" color="grey-100">
                    12:03
                  </Text>
                  <Button height="24" className={spacing.mt8}>
                    Open
                  </Button>
                </Container>
              </Container>
            </PushContainer>
            <PushContainer>
              <Container alignitems="flex-start" gap="12">
                <PushLogo
                  name="Vitaliy Kvasha"
                  icon={<PlusIcon />}
                  iconBackground="green-90"
                  radius="rounded"
                />
                <Container flexdirection="column">
                  <Container alignitems="center" gap="4">
                    <Text type="button">Vitaliy Kvasha </Text>
                    <Text type="body-regular-14" color="grey-100">
                      created new Bank transition
                    </Text>
                    <Text type="button"> ID1234567</Text>
                  </Container>
                  <Text type="text-regular" color="grey-100">
                    12:03
                  </Text>
                  <Button height="24" className={spacing.mt8}>
                    Open
                  </Button>
                </Container>
              </Container>
            </PushContainer>
            <Text type="button" className={classNames(spacing.p24, spacing.pb_auto)}>
              Yesterday
            </Text>
            <PushContainer>
              <Container alignitems="flex-start" gap="12">
                <PushLogo
                  name="Vitaliy Kvasha"
                  icon={<EditIcon />}
                  iconBackground="violet-90"
                  radius="rounded"
                />
                <Container flexdirection="column">
                  <Container alignitems="center" gap="4">
                    <Text type="button">Vitaliy Kvasha </Text>
                    <Text type="body-regular-14" color="grey-100">
                      updated Bank transition
                    </Text>
                    <Text type="button"> ID1234567</Text>
                  </Container>
                  <Text type="text-regular" color="grey-100">
                    19 may 23 12:03
                  </Text>
                  <Button height="24" className={spacing.mt8}>
                    Open
                  </Button>
                </Container>
              </Container>
            </PushContainer>
            <PushContainer>
              <Container alignitems="flex-start" gap="12">
                <PushLogo
                  name="Vitaliy Kvasha"
                  icon={<TrashIcon />}
                  iconBackground="red-90"
                  radius="rounded"
                />
                <Container flexdirection="column">
                  <Container alignitems="center" gap="4">
                    <Text type="button">Vitaliy Kvasha </Text>
                    <Text type="body-regular-14" color="grey-100">
                      deleted Bank transition
                    </Text>
                    <Text type="button"> ID1234567</Text>
                  </Container>
                  <Text type="text-regular" color="grey-100">
                    12:03
                  </Text>
                  <Button height="24" className={spacing.mt8}>
                    Open
                  </Button>
                </Container>
              </Container>
            </PushContainer>
            <EndOfNotifications />
          </Container>
        </DropDown>
        <Divider background={'grey-90'} type="vertical" className={spacing.mX20} />
        <DropDownUser user="Karine Mnatsakanian">
          <DropDownButton icon={<GearIcon />} onClick={() => {}}>
            My account settings
          </DropDownButton>
          <Divider className={classNames(spacing.mX10, spacing.mY6)} />
          <DropDownButton icon={<LogoutIcon />} onClick={() => {}}>
            Sign out
          </DropDownButton>
        </DropDownUser>
      </ButtonGroup>
    </Container>
  );
};

export default HeaderSB;
