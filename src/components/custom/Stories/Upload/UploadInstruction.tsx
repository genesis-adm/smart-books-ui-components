import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

const UploadInstruction = () => (
  <Container className={classNames(spacing.mt40, spacing.w440)} flexdirection="column" gap="4">
    <Text type="body-regular-14">Instruction for upload file:</Text>
    <Text type="body-regular-14">• All your information must be in one file</Text>
    <Text type="body-regular-14">
      • The top row of your file must contain a header title for each column of information
    </Text>
    <Container flexdirection="column">
      <Text type="body-regular-14">• To make sure your layout matches SmartBooks,</Text>
      <Container>
        <LinkButton type="body-regular-14" onClick={() => {}}>
          download this sample CSV
        </LinkButton>
      </Container>
    </Container>
  </Container>
);

export default UploadInstruction;
