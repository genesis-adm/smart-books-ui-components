import { UploadContainer } from 'Components/custom/Upload/UploadContainer';
import { UploadInfo } from 'Components/custom/Upload/UploadInfo';
import { UploadItemField } from 'Components/custom/Upload/UploadItemField';
import { checkFileType } from 'Utils/fileHandlers';
import React from 'react';

import { Nullable } from '../../../../types/general';

type UploadFileProps = {
  file: Nullable<File>;
  setFile: (file: File | null) => void;
  className?: string;
  style?: React.CSSProperties;
};
const UploadFile = ({ file, setFile, className, style }: UploadFileProps) => {
  const handleFileUpload = (item: File) => {
    setFile(item);
  };
  const handleFileDelete = () => {
    setFile(null);
  };
  const handleFileDrop = (e: React.DragEvent<HTMLElement>) => {
    const dropFile = e.dataTransfer.files[0];
    if (checkFileType(dropFile, ['csv'])) {
      setFile(e.dataTransfer.files[0]);
    }
  };
  return (
    <UploadContainer className={className} onDrop={handleFileDrop} style={style}>
      <UploadItemField
        name="uploader"
        files={file}
        acceptFilesType="csv"
        onChange={handleFileUpload}
        onDelete={handleFileDelete}
      />
      <UploadInfo
        mainText={file ? file.name : 'Drag and drop or Choose file'}
        secondaryText="CSV; Maximum file size is 5MB"
      />
    </UploadContainer>
  );
};

export default UploadFile;
