import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { Search } from 'Components/inputs/Search';
import { Select } from 'Components/inputs/Select';
import { SelectOption } from 'Components/inputs/Select/Select.types';
import { OptionWithTwoLabels } from 'Components/inputs/Select/options/OptionWithTwoLabels';
import { options } from 'Mocks/fakeOptions';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

type BusinessUnitsSelectProps = {
  value?: SelectOption;
  onChange?(): void;
};

const BusinessUnitsSelect = ({ value, onChange }: BusinessUnitsSelectProps) => {
  const [searchValue, setSearchValue] = useState<string>('');

  return (
    <Select label="Business unit" placeholder="Choose" width="300" required value={value}>
      {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
        <SelectDropdown selectElement={selectElement} width="408">
          <SelectDropdownBody>
            <Search
              className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
              width="full"
              height="40"
              name="search"
              placeholder="Search option"
              value={searchValue}
              onChange={(e) => setSearchValue(e.target.value)}
              searchParams={[]}
              onSearch={() => {}}
              onClear={() => {}}
            />
            <SelectDropdownItemList>
              {options.map((value, index) => (
                <OptionWithTwoLabels
                  id={value.label}
                  key={index}
                  tooltip
                  label={value?.label}
                  secondaryLabel={value.subLabel}
                  selected={!Array.isArray(selectValue) && selectValue?.value === value.value}
                  onClick={() => {
                    onOptionClick({
                      value: value?.value,
                      label: value?.label,
                      subLabel: value?.subLabel,
                    });
                    closeDropdown();
                    onChange?.();
                  }}
                />
              ))}
            </SelectDropdownItemList>
          </SelectDropdownBody>
        </SelectDropdown>
      )}
    </Select>
  );
};

export default BusinessUnitsSelect;
