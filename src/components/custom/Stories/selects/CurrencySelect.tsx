import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { Search } from 'Components/inputs/Search';
import { Select } from 'Components/inputs/Select';
import { SelectHeight, SelectOption } from 'Components/inputs/Select/Select.types';
import { OptionWithSingleLabel } from 'Components/inputs/Select/options/OptionWithSingleLabel';
import { currencyOptions } from 'Mocks/fakeOptions';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';
import { InputWidthType } from '../../../../types/inputs';

type CurrencySelectProps = {
  width?: InputWidthType;
  height?: SelectHeight;
  value?: SelectOption;
  defaultValue?: SelectOption;
  label?: string;
  onChange?(): void;
};

const CurrencySelect = ({
  value,
  label,
  onChange,
  width = '192',
  height,
  defaultValue,
}: CurrencySelectProps) => {
  const [searchValue, setSearchValue] = useState('');

  return (
    <Select
      label={label ? label : 'Currency'}
      placeholder="Choose option"
      width={width}
      height={height}
      required
      value={value}
      defaultValue={defaultValue}
    >
      {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
        <SelectDropdown selectElement={selectElement}>
          <SelectDropdownBody>
            <Search
              className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
              width="full"
              height="40"
              name="search"
              placeholder="Search option"
              value={searchValue}
              onChange={(e) => setSearchValue(e.target.value)}
              searchParams={[]}
              onSearch={() => {}}
              onClear={() => {}}
            />
            <SelectDropdownItemList>
              {currencyOptions.map((value, index) => (
                <OptionWithSingleLabel
                  id={value.label}
                  key={index}
                  label={`${value?.label} - ${value.fullLabel}`}
                  selected={!Array.isArray(selectValue) && selectValue?.value === value.value}
                  onClick={() => {
                    onOptionClick({
                      value: value?.value,
                      label: value?.label,
                    });
                    closeDropdown();
                    onChange?.();
                  }}
                />
              ))}
            </SelectDropdownItemList>
          </SelectDropdownBody>
        </SelectDropdown>
      )}
    </Select>
  );
};

export default CurrencySelect;
