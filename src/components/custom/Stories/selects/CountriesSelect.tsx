import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { Search } from 'Components/inputs/Search';
import { Select } from 'Components/inputs/Select';
import { SelectOption } from 'Components/inputs/Select/Select.types';
import { OptionWithSingleLabel } from 'Components/inputs/Select/options/OptionWithSingleLabel';
import { countrySelectOptions } from 'Mocks/fakeOptions';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';
import { InputWidthType } from '../../../../types/inputs';

type CountriesSelectProps = {
  width?: InputWidthType;
  required?: boolean;
  value?: SelectOption;
  onChange?(): void;
};

const CountriesSelect = ({ value, onChange, width = 'full', required }: CountriesSelectProps) => {
  const [searchValue, setSearchValue] = useState<string>('');

  return (
    <Select label="Country" placeholder="Choose" width={width} required={required} value={value}>
      {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
        <SelectDropdown selectElement={selectElement} width={width}>
          <SelectDropdownBody>
            <Search
              className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
              width="full"
              height="40"
              name="search"
              placeholder="Search option"
              value={searchValue}
              onChange={(e) => setSearchValue(e.target.value)}
              searchParams={[]}
              onSearch={() => {}}
              onClear={() => {}}
            />
            <SelectDropdownItemList>
              {countrySelectOptions
                ?.filter((item) => item?.label?.toLowerCase()?.includes(searchValue?.toLowerCase()))
                .map((value, index) => (
                  <OptionWithSingleLabel
                    id={value.label}
                    key={index}
                    label={value?.label}
                    selected={!Array.isArray(selectValue) && selectValue?.value === value.value}
                    onClick={() => {
                      onOptionClick(value);
                      closeDropdown();
                      onChange?.();
                    }}
                  />
                ))}
            </SelectDropdownItemList>
          </SelectDropdownBody>
        </SelectDropdown>
      )}
    </Select>
  );
};

export default CountriesSelect;
