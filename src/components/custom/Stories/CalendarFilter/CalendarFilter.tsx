import { DatePicker } from 'Components/base/DatePicker';
import { Divider } from 'Components/base/Divider';
import { Radio } from 'Components/base/Radio';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { FilterTab, FilterTabWrapper } from 'Components/base/tabs/FilterTab';
import { Text } from 'Components/base/typography/Text';
import { FilterItemsContainer } from 'Components/elements/filters/FilterItemsContainer';
import { calendarPresetsFilterData } from 'Mocks/fakeOptions';
import classNames from 'classnames';
import React, { FC, useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

export const CalendarFilter: FC = () => {
  const [active, setActive] = useState<string>('presets');
  const [checked, setChecked] = useState<string>('This week');
  const [startDay, setStartDay] = useState<Date | null>(new Date());
  const [endDay, setEndDay] = useState<Date | null>(null);
  const changeDayHandler = (dates: [Date | null, Date | null]) => {
    const [start, end] = dates;
    setStartDay(start);
    setEndDay(end);
  };
  return (
    <Container
      className={classNames(spacing.p16)}
      radius
      flexdirection="column"
      background="white-100"
      style={{ boxShadow: '0px 10px 30px rgba(192, 196, 205, 0.3)' }}
      border="grey-20"
    >
      <Text color="grey-100" type="body-regular-14">
        Date:
      </Text>
      <FilterItemsContainer className={classNames(spacing.mt8)} maxHeight="344">
        <Container justifycontent="center">
          <FilterTabWrapper>
            <FilterTab id="presets" onClick={setActive} active={active === 'presets'}>
              Presets
            </FilterTab>
            <FilterTab id="custom" onClick={setActive} active={active === 'custom'}>
              Custom range
            </FilterTab>
          </FilterTabWrapper>
        </Container>
        {active === 'presets' && (
          <Container flexdirection="column" className={spacing.pY10}>
            {calendarPresetsFilterData.slice(0, 4).map(({ label, endDate, startDate }) => (
              <Radio
                key={label}
                className={classNames(spacing.p8, spacing.mY2)}
                id={label}
                name="period"
                onChange={() => {
                  setChecked(label);
                }}
                checked={checked === label}
                color="grey-100"
              >
                <Container className={spacing.w100p} justifycontent="space-between">
                  <Text
                    type="caption-regular"
                    color={checked === label ? 'violet-90' : 'black-100'}
                  >
                    {label}
                  </Text>
                  <Text type="caption-regular" color="inherit">
                    {startDate} / {endDate}
                  </Text>
                </Container>
              </Radio>
            ))}
            <Divider fullHorizontalWidth className={spacing.mY8} />
            {calendarPresetsFilterData.slice(4).map(({ label, endDate, startDate }) => (
              <Radio
                key={label}
                className={classNames(spacing.p8, spacing.mY2)}
                id={label}
                name="period"
                onChange={() => {
                  setChecked(label);
                }}
                color="grey-100"
                checked={checked === label}
              >
                <Container className={spacing.w100p} justifycontent="space-between">
                  <Text
                    type="caption-regular"
                    color={checked === label ? 'violet-90' : 'black-100'}
                  >
                    {label}
                  </Text>
                  <Text type="caption-regular" color="inherit">
                    {startDate} / {endDate}
                  </Text>
                </Container>
              </Radio>
            ))}
          </Container>
        )}
        {active === 'custom' && (
          <Container flexdirection="column" alignitems="center" className={spacing.pY10}>
            <DatePicker
              name="calendar"
              withRange
              withPresets
              startDate={startDay}
              endDate={endDay}
              onChange={changeDayHandler}
              className={spacing.mt12}
            />
          </Container>
        )}
      </FilterItemsContainer>
      <Divider fullHorizontalWidth />
      <Container
        className={classNames(spacing.m16, spacing.w288fixed)}
        justifycontent="flex-end"
        alignitems="center"
      >
        <Button
          width="auto"
          height="32"
          padding="8"
          font="text-medium"
          onClick={() => {}}
          background="violet-90-violet-100"
          color="white-100"
        >
          Apply
        </Button>
      </Container>
    </Container>
  );
};
