import { Divider } from 'Components/base/Divider';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { SettingsNavigation } from 'Components/custom/Settings/SettingsNavigation';
import { SettingsNavigationButton } from 'Components/custom/Settings/SettingsNavigationButton';
import { Logo } from 'Components/elements/Logo';
import React from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

type ActiveTab =
  | 'Organization'
  | 'Preferences'
  | 'Advanced'
  | 'Security'
  | 'Business units'
  | 'Legal entities'
  | 'Users';

interface NavigationSettingsSBProps {
  selected: ActiveTab;
}

const NavigationSettingsSB: React.FC<NavigationSettingsSBProps> = ({ selected }) => (
  <SettingsNavigation className={spacing.mr20}>
    <Container className={spacing.w100p} flexdirection="column" alignitems="center" flexgrow="1">
      <Logo content="company" size="lg" name="Genesis" color="#FF7052" />
      <Text className={spacing.mt20} type="body-medium" align="center" transform="uppercase">
        Genesis
      </Text>
      <Divider className={spacing.mY20} fullHorizontalWidth />
      <SettingsNavigationButton
        title="Organization"
        selected={selected === 'Organization'}
        onClick={() => {}}
      />
      <SettingsNavigationButton
        title="Preferences"
        selected={selected === 'Preferences'}
        onClick={() => {}}
      />
      <SettingsNavigationButton
        title="Advanced"
        selected={selected === 'Advanced'}
        onClick={() => {}}
      />
      <SettingsNavigationButton
        title="Security"
        selected={selected === 'Security'}
        onClick={() => {}}
      />
      <SettingsNavigationButton
        title="Business units"
        selected={selected === 'Business units'}
        onClick={() => {}}
      />
      <SettingsNavigationButton
        title="Legal entities"
        selected={selected === 'Legal entities'}
        onClick={() => {}}
      />
      <SettingsNavigationButton title="Users" selected={selected === 'Users'} onClick={() => {}} />
    </Container>
  </SettingsNavigation>
);

export default NavigationSettingsSB;
