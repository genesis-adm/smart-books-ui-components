import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { Icon } from 'Components/base/Icon';
import { Tooltip } from 'Components/base/Tooltip';
import { Container } from 'Components/base/grid/Container';
import { Option } from 'Components/base/inputs/SelectNew/Select.types';
import { Text } from 'Components/base/typography/Text';
import { Search } from 'Components/inputs/Search';
import { Select } from 'Components/inputs/Select';
import { SelectOption } from 'Components/inputs/Select/Select.types';
import { SelectValue } from 'Components/inputs/Select/hooks/useSelectValue';
import { ReactComponent as ArrowLeftIcon } from 'Svg/v2/16/arrow-left.svg';
import { ReactComponent as InfoIcon } from 'Svg/v2/16/info.svg';
import classNames from 'classnames';
import React, { ReactElement, useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';
import { Nullable } from '../../../../types/general';
import { InputBackgroundType, InputWidthType } from '../../../../types/inputs';

export interface MatchColumnsSelectProps {
  name: string;
  label: string;
  placeholder: string;
  defaultValue?: Option;
  value?: Option;
  required?: boolean;
  width?: InputWidthType;
  background?: InputBackgroundType;
  tooltip?: string;
  textWidth?: '130' | '160' | '260';
  className?: string;
  disabled?: boolean;

  onChange?(option: Nullable<SelectOption>): void;

  children(args: {
    onClick(option: SelectOption): void;
    state?: SelectValue<false>;
    closeDropdown?: () => void;
  }): ReactElement;
}

const MatchColumnsSelect: React.FC<MatchColumnsSelectProps> = ({
  name,
  label,
  placeholder,
  defaultValue,
  value,
  required,
  background = 'transparent',
  tooltip,
  width = '320',
  onChange,
  children,
  disabled,
}) => {
  const [searchValue, setSearchValue] = useState('');
  return (
    <Container alignitems="center">
      <Container className={classNames(spacing.h50min, spacing.mr20)} alignitems="center">
        {tooltip && (
          <Tooltip cursor="pointer" wrapperClassName={classNames(spacing.mr4)} message={tooltip}>
            <Icon icon={<InfoIcon />} path="grey-90" />
          </Tooltip>
        )}
        <Text
          type="body-regular-14"
          color="grey-100"
          className={classNames(spacing.w200min, spacing.w130)}
          noWrap
        >
          {name}
        </Text>
        <ArrowLeftIcon />
      </Container>
      <Select
        value={value}
        defaultValue={defaultValue}
        onChange={(option) => onChange && onChange(option)}
        background={background}
        placeholder={placeholder}
        label={label}
        disabled={disabled}
        width={width}
        height="48"
        required={required}
      >
        {({ onOptionClick, selectValue, closeDropdown, selectElement }) => (
          <SelectDropdown selectElement={selectElement}>
            <SelectDropdownBody>
              <Search
                className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                width="full"
                height="40"
                name="search"
                placeholder="Search option"
                value={searchValue}
                onChange={(e) => setSearchValue(e.target.value)}
                searchParams={[]}
                onSearch={() => {}}
                onClear={() => {}}
              />
              <SelectDropdownItemList>
                {children({
                  onClick: onOptionClick,
                  state: selectValue,
                  closeDropdown,
                })}
              </SelectDropdownItemList>
            </SelectDropdownBody>
          </SelectDropdown>
        )}
      </Select>
    </Container>
  );
};

export default MatchColumnsSelect;
