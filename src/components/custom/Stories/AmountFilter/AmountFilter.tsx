import { Divider } from 'Components/base/Divider';
import { Radio } from 'Components/base/Radio';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { NumericInput } from 'Components/base/inputs/NumericInput';
import { OwnNumberFormatValues } from 'Components/base/inputs/NumericInput/NumericInput.types';
import { Text } from 'Components/base/typography/Text';
import { FilterItemsContainer } from 'Components/elements/filters/FilterItemsContainer';
import { priceFilterItem } from 'Mocks/purchasesFakeData';
import classNames from 'classnames';
import React, { FC, useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

export const AmountFilter: FC = () => {
  const [inputValue, setInputValue] = useState<number>();
  const handleInputChange = ({ floatValue }: OwnNumberFormatValues) => {
    setInputValue(floatValue);
  };

  return (
    <>
      <Container
        className={classNames(spacing.mY16, spacing.ml16, spacing.mr8, spacing.w296fixed)}
        flexdirection="column"
      >
        <FilterItemsContainer maxHeight="344" gap="4" className={spacing.mt8}>
          <Text type="body-regular-14" color="grey-100">
            Please choose filter type
          </Text>
          <Container flexdirection="column">
            <Container gap="8" flexwrap="wrap" className={classNames(spacing.mb16)}>
              {priceFilterItem.map(({ title, checked }) => (
                <Radio
                  key={title}
                  id={title}
                  name={title}
                  checked={checked}
                  className={classNames(spacing.mt20, spacing.w120fixed)}
                  onChange={() => {}}
                >
                  <Text type="caption-regular" color="grey-100">
                    {title}
                  </Text>
                </Radio>
              ))}
            </Container>
            <NumericInput
              name="num"
              label=""
              defaultValue={0}
              value={inputValue}
              onValueChange={handleInputChange}
            />
          </Container>
        </FilterItemsContainer>
      </Container>
      <Divider fullHorizontalWidth />
      <Container
        className={classNames(spacing.m16, spacing.w288fixed)}
        justifycontent="space-between"
        alignitems="center"
        flexdirection="row-reverse"
      >
        <Button
          width="auto"
          height="32"
          padding="8"
          font="text-medium"
          onClick={() => {}}
          background="violet-90-violet-100"
          color="white-100"
        >
          Apply
        </Button>
        <Button
          width="auto"
          height="32"
          padding="8"
          font="text-medium"
          onClick={() => {}}
          background="blue-10-red-10"
          color="violet-90-red-90"
        >
          Clear all
        </Button>
      </Container>
    </>
  );
};
