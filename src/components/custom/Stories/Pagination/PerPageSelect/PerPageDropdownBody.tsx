import { DropDownContext } from 'Components/DropDown';
import { SelectRowsPerPageDropdownType } from 'Components/custom/Stories/Pagination/PerPageSelect/PerPageSelect';
import { OptionWithSingleLabel } from 'Components/inputs/Select/options/OptionWithSingleLabel';
import { rowsPerPageOptions } from 'Mocks/fakeOptions';
import React, { useContext } from 'react';

const PerPageDropdownBody = ({ onOptionClick, selectedValue }: SelectRowsPerPageDropdownType) => {
  const { handleOpen, isOpen } = useContext(DropDownContext);
  return (
    <>
      {rowsPerPageOptions.map(({ label, value }) => (
        <OptionWithSingleLabel
          key={value}
          label={label}
          type="default"
          selected={selectedValue === value}
          onClick={() => {
            onOptionClick(value, isOpen);
            if (handleOpen) {
              handleOpen();
            }
          }}
          id={value}
        />
      ))}
    </>
  );
};

export default PerPageDropdownBody;
