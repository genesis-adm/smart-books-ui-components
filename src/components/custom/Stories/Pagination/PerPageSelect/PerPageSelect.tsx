import { DropDown } from 'Components/DropDown';
import { Tag, TagColorType } from 'Components/base/Tag';
import PerPageDropdownBody from 'Components/custom/Stories/Pagination/PerPageSelect/PerPageDropdownBody';
import { ReactComponent as ChevronDown } from 'Svg/v2/12/chevron-down.svg';
import { ReactComponent as ChevronUp } from 'Svg/v2/12/chevron-up.svg';
import React from 'react';

export type SelectRowsPerPageDropdownType = {
  selectedValue: string;
  tagColor?: TagColorType;
  onOptionClick(value: string, isOpen?: boolean): void;
};

const PerPageSelect = ({
  selectedValue,
  tagColor = 'grey-10',
  onOptionClick,
}: SelectRowsPerPageDropdownType) => {
  return (
    <DropDown
      padding="8"
      flexdirection="column"
      control={({ handleOpen, isOpen }) => (
        <Tag
          text={selectedValue}
          color={tagColor}
          cursor="pointer"
          additionalIconRight={isOpen ? <ChevronUp /> : <ChevronDown />}
          onClick={handleOpen}
          onClickAdditionalIconRight={handleOpen}
        />
      )}
    >
      <PerPageDropdownBody
        selectedValue={selectedValue}
        onOptionClick={(value) => {
          onOptionClick(value);
        }}
      />
    </DropDown>
  );
};

export default PerPageSelect;
