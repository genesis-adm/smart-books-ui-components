import { Divider } from 'Components/base/Divider';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { PaginationButtonNew } from 'Components/elements/Pagination/PaginationButtonNew';
import { PaginationContainerNew } from 'Components/elements/Pagination/PaginationContainerNew';
import { PaginationDirectionButton } from 'Components/elements/Pagination/PaginationDirectionButton';
import { PaginationInput } from 'Components/elements/Pagination/PaginationInput';
import React, { FC } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

type PaginationProps = {
  borderTop?: 'none' | 'default';
  padding?: '0' | '16';
  customPagination?: React.ReactNode;
  elementsBackground?: 'grey-10' | 'white-100';
  action?: React.ReactNode;
  isLoading?: boolean;
};
export const Pagination: FC<PaginationProps> = ({
  borderTop,
  padding,
  customPagination,
  elementsBackground = 'grey-10',
  action,
  isLoading,
}: PaginationProps) => {
  return (
    <PaginationContainerNew
      quantityText="Showing 1-15 of 659,813 items"
      borderTop={borderTop}
      padding={padding}
      action={action}
      customPagination={customPagination}
      isLoading={isLoading}
    >
      <PaginationDirectionButton direction="left" onClick={() => {}} />
      {Array.from(Array(6)).map((_, index) => (
        <PaginationButtonNew
          background={elementsBackground}
          value={index + 1}
          selected={index === 1}
          onClick={() => {}}
        />
      ))}
      <PaginationDirectionButton direction="right" onClick={() => {}} />
      <Divider height="20" type="vertical" className={spacing.mX16} />
      <Container alignitems="center" gap="8">
        <Text type="subtext-regular" color="grey-100">
          Go to page
        </Text>
        <PaginationInput background={elementsBackground} />
      </Container>
    </PaginationContainerNew>
  );
};
