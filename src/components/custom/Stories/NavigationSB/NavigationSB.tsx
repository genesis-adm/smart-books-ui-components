import { DropDown } from 'Components/DropDown';
import { Divider } from 'Components/base/Divider';
import { Button } from 'Components/base/buttons/Button';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { Navigation } from 'Components/custom/Navigation';
import { CompanyDropDownItem } from 'Components/custom/Navigation/CompanyDropDownItem';
import { NavigationCompany } from 'Components/custom/Navigation/NavigationCompany';
import { NavigationButton } from 'Components/custom/Navigation/buttons/NavigationButton';
import { NavigationButtonNew } from 'Components/custom/Navigation/buttons/NavigationButtonNew';
import { NavigationSectionWrapper } from 'Components/custom/Navigation/buttons/NavigationSectionWrapper';
import { ToggleButton } from 'Components/custom/Navigation/buttons/ToggleButton';
import { ReactComponent as SalesIcon } from 'Svg/16/chart-light.svg';
import { ReactComponent as PaidlogIcon } from 'Svg/16/credit-card-light.svg';
import { ReactComponent as ExpensesIcon } from 'Svg/16/folder-light.svg';
import { ReactComponent as OverviewIcon } from 'Svg/16/overview-light.svg';
import { ReactComponent as ReportsIcon } from 'Svg/16/piechart-light.svg';
import { ReactComponent as InfoIcon } from 'Svg/16/question-light.svg';
import { ReactComponent as SettingsIcon } from 'Svg/16/settings-light.svg';
import { ReactComponent as AccountingIcon } from 'Svg/16/suitcase-light.svg';
import { ReactComponent as BankingIcon } from 'Svg/16/wallet-light.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { navigationNameFormatter } from 'Utils/navigationNameFormatter';
import classNames from 'classnames';
import React, { useCallback, useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

const buttonList = [
  {
    title: 'Overview',
    icon: <OverviewIcon />,
  },
  {
    title: 'Banking',
    icon: <BankingIcon />,
    subButtons: [
      { sectionTitle: 'Transactions' },
      { sectionTitle: 'Bank accounts' },
      { sectionTitle: 'Rules' },
    ],
  },
  {
    title: 'Sales',
    icon: <SalesIcon />,
    subButtons: [
      { sectionTitle: 'All sales' },
      { sectionTitle: 'Customers' },
      { sectionTitle: 'Products' },
    ],
  },
  {
    title: 'Expenses',
    icon: <ExpensesIcon />,
  },
  {
    title: 'Accounting',
    icon: <AccountingIcon />,
    subButtons: [
      { sectionTitle: 'Journal' },
      { sectionTitle: 'Chart of Accounts' },
      { sectionTitle: 'Template account' },
    ],
  },
  {
    title: 'Reports',
    icon: <ReportsIcon />,
  },
  {
    title: 'Paidlog',
    icon: <PaidlogIcon />,
    subButtons: [
      { sectionTitle: 'Transactions' },
      { sectionTitle: 'Vendors' },
      { sectionTitle: 'Text' },
    ],
  },
];
export interface NavigationSBProps {
  opened: boolean;
  className?: string;
  onToggleOpen(): void;
}

const NavigationSB: React.FC<NavigationSBProps> = ({ opened, onToggleOpen }) => {
  const [activeSection, setActiveSection] = useState('overview');

  const handleSetActiveSection = useCallback(
    (section: string) => () => {
      setActiveSection(section);
    },
    [],
  );

  return (
    <Navigation opened={opened}>
      <Container flexdirection="column" alignitems="center">
        <DropDown
          axis="horizontal"
          flexdirection="column"
          padding="10"
          control={({ handleOpen }) => (
            <NavigationCompany
              companyName="Genesis"
              companyType="Organisation"
              logo="https://picsum.photos/50"
              // color={options?.color}
              onClick={handleOpen}
              active={opened}
            />
          )}
        >
          <Text className={spacing.mb5} type="caption-semibold" color="grey-100">
            Launch the organization:
          </Text>
          <CompanyDropDownItem
            companyName="Genesis"
            logo="https://picsum.photos/50"
            onClick={() => {}}
            selected
          />
          <CompanyDropDownItem
            companyName="JiJi"
            logo="https://picsum.photos/60"
            onClick={() => {}}
          />
          <CompanyDropDownItem companyName="Solid" logo="" color="#7AC72D" onClick={() => {}} />
          <Divider className={spacing.mt4} />
          <LinkButton
            className={classNames(spacing.mX_auto, spacing.mt15, spacing.mb10)}
            icon={<PlusIcon />}
            onClick={() => {}}
          >
            Create organization account
          </LinkButton>
        </DropDown>
        <Button
          width="full"
          height="medium"
          iconLeft={<PlusIcon />}
          onlyIcon={!opened}
          textTransition="inherit"
          onClick={() => {}}
        >
          Add new
        </Button>
        <Container
          className={classNames(spacing.mt12, spacing.w100p)}
          flexdirection="column"
          gap="12"
        >
          {buttonList.map(({ title, icon, subButtons }) => {
            const buttonID = navigationNameFormatter(title);
            const defaultOption = subButtons
              ? navigationNameFormatter(`${buttonID}-${subButtons[0].sectionTitle}`)
              : navigationNameFormatter(title);
            return (
              <NavigationSectionWrapper
                key={buttonID}
                mainButton={
                  <NavigationButtonNew
                    type="module"
                    id={buttonID}
                    isSelected={activeSection.includes(buttonID)}
                    title={title}
                    icon={icon}
                    showFullButton={opened}
                    showTooltip={!!subButtons}
                    onClick={handleSetActiveSection(defaultOption)}
                  />
                }
                sectionButtons={
                  subButtons ? (
                    <>
                      {subButtons.map(({ sectionTitle }) => {
                        const subButtonID = navigationNameFormatter(`${title}-${sectionTitle}`);
                        return (
                          <NavigationButtonNew
                            key={subButtonID}
                            id={subButtonID}
                            title={sectionTitle}
                            isSelected={activeSection === subButtonID}
                            onClick={handleSetActiveSection(subButtonID)}
                          />
                        );
                      })}
                    </>
                  ) : null
                }
              />
            );
          })}
        </Container>
      </Container>
      <Container flexdirection="column">
        <NavigationButton
          minimized={opened}
          title="Settings"
          icon={<SettingsIcon />}
          onClick={() => {}}
        >
          Settings
        </NavigationButton>
        <NavigationButton minimized={opened} title="Help" icon={<InfoIcon />} onClick={() => {}}>
          Help
        </NavigationButton>
        <ToggleButton toggled={opened} onClick={onToggleOpen} />
      </Container>
    </Navigation>
  );
};

export default NavigationSB;
