import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ReactComponent as ArrowLeftIcon } from 'Svg/v2/16/arrow-left.svg';
import { ReactComponent as ArrowRightIcon } from 'Svg/v2/16/arrow-right.svg';
import { ReactComponent as CSVIcon } from 'Svg/v2/16/file-csv.svg';
import React from 'react';

type ImportModalFooter = {
  isActive?: boolean;
  modalType: 'upload' | 'matchColumns' | 'previewMode';
  hideExportListButton?: boolean;
};
const ImportModalFooter = ({ isActive, modalType, hideExportListButton }: ImportModalFooter) => {
  const footerData = {
    upload: {
      step: '1',
      nextStep: 'Match columns',
      showBackButton: false,
      showExportListButton: !hideExportListButton,
      background: 'white-100',
    },
    matchColumns: {
      step: '2',
      nextStep: 'Preview mode',
      showBackButton: true,
      showExportListButton: false,
      background: 'grey-10',
    },
    previewMode: {
      step: '3',
      nextStep: 'Preview mode',
      showBackButton: true,
      showExportListButton: false,
      background: 'grey-10',
    },
  };
  return (
    <ModalFooterNew
      justifycontent="space-between"
      border
      background={footerData[modalType].background as 'grey-10' | 'white-100'}
    >
      <Container alignitems="center" gap="16" flexgrow="1" justifycontent="flex-end">
        <Container flexdirection="column">
          <Text type="body-regular-14" color="grey-100">
            Step: {footerData[modalType].step} of 3
          </Text>
          <Text type="body-regular-14" color="grey-100">
            Next: {footerData[modalType].nextStep}
          </Text>
        </Container>
        <Button
          height="50"
          navigation
          iconRight={<ArrowRightIcon />}
          width="230"
          background={isActive ? 'violet-90-violet-100' : 'grey-90'}
          onClick={() => {}}
        >
          {modalType !== 'previewMode' ? 'Next' : `Import 39 rows`}
        </Button>
      </Container>
      {footerData[modalType].showBackButton && (
        <Button
          height="50"
          width="150"
          navigation
          iconLeft={<ArrowLeftIcon />}
          background={'white-100'}
          color="black-100"
          border="grey-20-grey-90"
          onClick={() => {}}
        >
          Back
        </Button>
      )}
      {footerData[modalType].showExportListButton && (
        <Button
          height="40"
          background={'white-100-blue-10'}
          color="grey-100-violet-90"
          onClick={() => {}}
          iconLeft={<CSVIcon />}
        >
          Export Chart of account list.CSV
        </Button>
      )}
    </ModalFooterNew>
  );
};

export default ImportModalFooter;
