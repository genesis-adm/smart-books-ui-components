import { IconSB as Icon, IconContainerSB as Wrapper } from 'Components/custom/Stories/IconSB';
import React from 'react';

import {
  // 16px
  Alert16Icon, // 32px
  Alert32Icon, // 24px
  AnalysisChart24Icon, // 10px
  AppleLogoIcon52,
  ArrowDown10Icon,
  ArrowDown16Icon,
  ArrowLeft16Icon,
  ArrowRight16Icon,
  ArrowRightNarrow16Icon,
  ArrowUp10Icon,
  ArrowUp16Icon, // 12px
  Arrows12Icon,
  ArrowsSwitch16Icon,
  BanIcon,
  Bank16Icon,
  Bank24Icon,
  BankCheque16Icon,
  BankCheque32Icon,
  Banknote16Icon,
  BookOpened24Icon,
  Box32Icon,
  BoxCrossed32Icon,
  Briefcase12Icon,
  Briefcase16Icon,
  BriefcaseFilledDollar16Icon,
  Building24Icon,
  BulletDot16Icon,
  Calendar16Icon,
  Calendar24Icon,
  CalendarFilled16Icon,
  CaretBoth10Icon,
  CaretDown16Icon,
  Cash16Icon,
  Cash24Icon,
  Charts16Icon,
  ChefHat24Icon,
  ChevronDoubleLeft16Icon,
  ChevronDoubleRight16Icon,
  ChevronDown16Icon,
  ChevronLeft12Icon,
  ChevronLeft16Icon,
  ChevronRight12Icon,
  ChevronRight16Icon,
  ChevronUp12Icon,
  ChevronUp16Icon,
  Clipboard24Icon,
  Clipboard32Icon,
  ClipboardCurrency16Icon, // 56px
  ClipboardCurrency56Icon,
  ClipboardList12Icon,
  ClipboardList16Icon,
  ClipboardList56Icon,
  ClipboardTick16Icon,
  Clock16Icon,
  Clock24Icon,
  ClockInFilledCircle16Icon,
  Clothes24Icon,
  Copy16Icon,
  CreditCard16Icon,
  CreditCard24Icon,
  Cross16Icon,
  CrossInCircle16Icon,
  CrossInFilledCircle16Icon,
  Delivery24Icon,
  Devices24Icon,
  Devices32Icon,
  DishPlateCover24Icon,
  DocumentSearch16Icon,
  Download16Icon,
  Download24Icon,
  Drag16Icon, // 124px
  Empty124Icon,
  EmptyTable124Icon,
  Envelope16Icon,
  Erase16Icon,
  ExclamationInCircle16Icon,
  ExclamationInFilledTriangle16Icon,
  Eye16Icon,
  EyeCrossed16Icon,
  Fastfood24Icon,
  File56Icon,
  FileAdd16Icon,
  FileAdd24Icon,
  FileBlank16Icon,
  FileCSV16Icon,
  FileDownload16Icon,
  FilePDF16Icon,
  FileText16Icon,
  FileTextFilled16Icon,
  FileXLS16Icon,
  Filter16Icon,
  Flash10Icon,
  Gear16Icon,
  Google16Icon,
  GooglePlayLogoIcon52,
  Hand10Icon,
  Hand12Icon,
  Hand16Icon,
  HeartRate24Icon,
  Hospital24Icon,
  Info12Icon,
  Info16Icon,
  Info32Icon,
  InfoInCircle16Icon,
  Invoice16Icon,
  Key24Icon,
  Lines16Icon,
  Link16Icon,
  ListsAccounting16Icon,
  ListsBanking16Icon,
  ListsOther16Icon,
  LockClosed16Icon,
  LockOpened16Icon,
  LogoAdMob24Icon,
  LogoAdMob32Icon,
  LogoAdSense24Icon,
  LogoAdSense32Icon,
  LogoAdWords24Icon,
  LogoAdWords32Icon,
  LogoAdX24Icon,
  LogoAdX32Icon,
  LogoAfterpay16Icon,
  LogoAfterpay24Icon,
  LogoAfterpay32Icon,
  LogoAmericanExpress16Icon,
  LogoAmericanExpress24Icon,
  LogoAmex32Icon,
  LogoApple24Icon,
  LogoApple32Icon,
  LogoFacebook24Icon,
  LogoFacebook32Icon,
  LogoGooglePlay24Icon,
  LogoGooglePlay32Icon,
  LogoMastercard16Icon,
  LogoMastercard24Icon,
  LogoMastercard32Icon,
  LogoVisa16Icon,
  LogoVisa24Icon,
  LogoVisa32Icon,
  MoneyIn16Icon,
  MoneyIn32Icon,
  MoneyOut16Icon,
  MoneyOut32Icon,
  MoneyStack16Icon,
  MoneyStack32Icon,
  Notification24Icon,
  Pencil16Icon,
  Person16Icon,
  Piechart16Icon,
  Plus16Icon,
  Puzzle32Icon,
  Question12Icon,
  Question16Icon,
  QuestionInCircle16Icon,
  QuestionInFilledCircle16Icon,
  QuestionTransparent12Icon,
  QuestionTransparent16Icon,
  ReceiptDiscount24Icon,
  Reload16Icon, // 40px
  Reload40Icon,
  Scales16Icon,
  Scales32Icon,
  Search16Icon,
  Search32Icon, // 50px
  Search50Icon,
  Settings16Icon,
  ShoppingCart24Icon,
  Sigma16Icon,
  Sort10Icon,
  Sort16Icon,
  Star16Icon,
  StarFilled16Icon,
  Stop16Icon,
  SuitcaseS24Icon,
  Table180Icon,
  ThreeDots16Icon,
  Tick16Icon,
  TickInCircle16Icon,
  TickInFilledCircle16Icon,
  Trash16Icon,
  Trashbox16Icon,
  Trashbox32Icon,
  UndoRightRoundIcon16,
  Upload16Icon,
  UsersGroupRoundedIcon,
  Wallet16Icon,
  Wallet24Icon,
  Warning16Icon,
} from './IconsList';

export default {
  title: 'Icons',
};

export const Icons: React.FC = () => (
  <>
    <Wrapper size="10">
      <Icon icon={<ArrowDown10Icon />} path="v2/10/arrow-down.svg" />
      <Icon icon={<ArrowUp10Icon />} path="v2/10/arrow-up.svg" />
      <Icon icon={<CaretBoth10Icon />} path="v2/10/caret-both.svg" />
      <Icon icon={<Flash10Icon />} path="v2/10/flash.svg" />
      <Icon icon={<Hand10Icon />} path="v2/10/hand.svg" />
      <Icon icon={<Sort10Icon />} path="v2/10/sort.svg" />
    </Wrapper>
    <Wrapper size="12">
      <Icon icon={<Arrows12Icon />} path="v2/12/arrows.svg" />
      <Icon icon={<Briefcase12Icon />} path="v2/12/briefcase.svg" />
      <Icon icon={<ChevronLeft12Icon />} path="v2/12/chevron-left.svg" />
      <Icon icon={<ChevronRight12Icon />} path="v2/12/chevron-right.svg" />
      <Icon icon={<ChevronUp12Icon />} path="v2/12/chevron-up.svg" />
      <Icon icon={<ClipboardList12Icon />} path="v2/12/clipboard-list.svg" />
      <Icon icon={<Hand12Icon />} path="v2/12/hand.svg" />
      <Icon icon={<Info12Icon />} path="v2/12/info.svg" />
      <Icon icon={<QuestionTransparent12Icon />} path="v2/12/question-transparent.svg" />
      <Icon icon={<Question12Icon />} path="v2/12/question.svg" />
    </Wrapper>
    <Wrapper size="16">
      <Icon icon={<Alert16Icon />} path="v2/16/alert.svg" />
      <Icon icon={<ArrowDown16Icon />} path="v2/16/arrow-down.svg" />
      <Icon icon={<ArrowLeft16Icon />} path="v2/16/arrow-left.svg" />
      <Icon icon={<ArrowRightNarrow16Icon />} path="v2/16/arrow-right-narrow.svg" />
      <Icon icon={<ArrowRight16Icon />} path="v2/16/arrow-right.svg" />
      <Icon icon={<ArrowUp16Icon />} path="v2/16/arrow-up.svg" />
      <Icon icon={<ArrowsSwitch16Icon />} path="v2/16/arrows-switch.svg" />
      <Icon icon={<BanIcon />} path="v2/16/ban.svg" />
      <Icon icon={<BankCheque16Icon />} path="v2/16/bank-cheque.svg" />
      <Icon icon={<Bank16Icon />} path="v2/16/bank.svg" />
      <Icon icon={<Banknote16Icon />} path="v2/16/banknote.svg" />
      <Icon icon={<BriefcaseFilledDollar16Icon />} path="v2/16/briefcase-filled-dollar.svg" />
      <Icon icon={<Briefcase16Icon />} path="v2/16/briefcase.svg" />
      <Icon icon={<BulletDot16Icon />} path="v2/16/bullet-dot.svg" />
      <Icon icon={<CalendarFilled16Icon />} path="v2/16/calendar-filled.svg" />
      <Icon icon={<Calendar16Icon />} path="v2/16/calendar.svg" />
      <Icon icon={<Cash16Icon />} path="v2/16/cash.svg" />
      <Icon icon={<CaretDown16Icon />} path="v2/16/caret-down.svg" />
      <Icon icon={<Charts16Icon />} path="v2/16/charts.svg" />
      <Icon icon={<ChevronDoubleLeft16Icon />} path="v2/16/chevron-double-left.svg" />
      <Icon icon={<ChevronDoubleRight16Icon />} path="v2/16/chevron-double-right.svg" />
      <Icon icon={<ChevronDown16Icon />} path="v2/16/chevron-down.svg" />
      <Icon icon={<ChevronLeft16Icon />} path="v2/16/chevron-left.svg" />
      <Icon icon={<ChevronRight16Icon />} path="v2/16/chevron-right.svg" />
      <Icon icon={<ChevronUp16Icon />} path="v2/16/chevron-up.svg" />
      <Icon icon={<ClipboardCurrency16Icon />} path="v2/16/clipboard-currency.svg" />
      <Icon icon={<ClipboardList16Icon />} path="v2/16/clipboard-list.svg" />
      <Icon icon={<ClipboardTick16Icon />} path="v2/16/clipboard-tick.svg" />
      <Icon icon={<ClockInFilledCircle16Icon />} path="v2/16/clock-in-filled-circle.svg" />
      <Icon icon={<Clock16Icon />} path="v2/16/clock.svg" />
      <Icon icon={<Copy16Icon />} path="v2/16/copy.svg" />
      <Icon icon={<CreditCard16Icon />} path="v2/16/credit-card.svg" />
      <Icon icon={<CrossInCircle16Icon />} path="v2/16/cross-in-circle.svg" />
      <Icon icon={<CrossInFilledCircle16Icon />} path="v2/16/cross-in-filled-circle.svg" />
      <Icon icon={<Cross16Icon />} path="v2/16/cross.svg" />
      <Icon icon={<Download16Icon />} path="v2/16/download.svg" />
      <Icon icon={<Drag16Icon />} path="v2/16/drag.svg" />
      <Icon icon={<Envelope16Icon />} path="v2/16/envelope.svg" />
      <Icon icon={<Erase16Icon />} path="v2/16/erase.svg" />
      <Icon icon={<ExclamationInCircle16Icon />} path="v2/16/exclamation-in-circle.svg" />
      <Icon
        icon={<ExclamationInFilledTriangle16Icon />}
        path="v2/16/exclamation-in-filled-triangle.svg"
      />
      <Icon icon={<EyeCrossed16Icon />} path="v2/16/eye-crossed.svg" />
      <Icon icon={<Eye16Icon />} path="v2/16/eye.svg" />
      <Icon icon={<FileAdd16Icon />} path="v2/16/file-add.svg" />
      <Icon icon={<FileBlank16Icon />} path="v2/16/file-blank.svg" />
      <Icon icon={<FileCSV16Icon />} path="v2/16/file-csv.svg" />
      <Icon icon={<FileDownload16Icon />} path="v2/16/file-download.svg" />
      <Icon icon={<FilePDF16Icon />} path="v2/16/file-pdf.svg" />
      <Icon icon={<FileTextFilled16Icon />} path="v2/16/file-text-filled.svg" />
      <Icon icon={<FileText16Icon />} path="v2/16/file-text.svg" />
      <Icon icon={<FileXLS16Icon />} path="v2/16/file-xls.svg" />
      <Icon icon={<Filter16Icon />} path="v2/16/filter.svg" />
      <Icon icon={<Gear16Icon />} path="v2/16/gear.svg" />
      <Icon icon={<Google16Icon />} path="v2/16/google.svg" />
      <Icon icon={<Hand16Icon />} path="v2/16/hand.svg" />
      <Icon icon={<InfoInCircle16Icon />} path="v2/16/info-in-circle.svg" />
      <Icon icon={<Info16Icon />} path="v2/16/info.svg" />
      <Icon icon={<Invoice16Icon />} path="v2/16/invoice.svg" />
      <Icon icon={<Lines16Icon />} path="v2/16/lines.svg" />
      <Icon icon={<Link16Icon />} path="v2/16/link.svg" />
      <Icon icon={<ListsAccounting16Icon />} path="v2/16/lists-accounting.svg" />
      <Icon icon={<ListsBanking16Icon />} path="v2/16/lists-banking.svg" />
      <Icon icon={<ListsOther16Icon />} path="v2/16/lists-other.svg" />
      <Icon icon={<LockClosed16Icon />} path="v2/16/lock-closed.svg" />
      <Icon icon={<LockOpened16Icon />} path="v2/16/lock-opened.svg" />
      <Icon icon={<LogoAfterpay16Icon />} path="v2/16/logo-afterpay.svg" />
      <Icon icon={<LogoAmericanExpress16Icon />} path="v2/16/logo-amex.svg" />
      <Icon icon={<LogoMastercard16Icon />} path="v2/16/logo-mastercard.svg" />
      <Icon icon={<LogoVisa16Icon />} path="v2/16/logo-mastervisacard.svg" />
      <Icon icon={<MoneyIn16Icon />} path="v2/16/money-in.svg" />
      <Icon icon={<MoneyOut16Icon />} path="v2/16/money-out.svg" />
      <Icon icon={<MoneyStack16Icon />} path="v2/16/money-stack.svg" />
      <Icon icon={<Pencil16Icon />} path="v2/16/pencil.svg" />
      <Icon icon={<Person16Icon />} path="v2/16/person.svg" />
      <Icon icon={<Piechart16Icon />} path="v2/16/piechart.svg" />
      <Icon icon={<Plus16Icon />} path="v2/16/plus.svg" />
      <Icon icon={<QuestionInCircle16Icon />} path="v2/16/question-in-circle.svg" />
      <Icon icon={<QuestionInFilledCircle16Icon />} path="v2/16/question-in-filled-circle.svg" />
      <Icon icon={<QuestionTransparent16Icon />} path="v2/16/question-transparent.svg" />
      <Icon icon={<Question16Icon />} path="v2/16/question.svg" />
      <Icon icon={<Reload16Icon />} path="v2/16/reload.svg" />
      <Icon icon={<Scales16Icon />} path="v2/16/scales.svg" />
      <Icon icon={<Search16Icon />} path="v2/16/search.svg" />
      <Icon icon={<DocumentSearch16Icon />} path="v2/16/document-search.svg" />
      <Icon icon={<Settings16Icon />} path="v2/16/settings.svg" />
      <Icon icon={<Sigma16Icon />} path="v2/16/sigma.svg" />
      <Icon icon={<Sort16Icon />} path="v2/16/sort.svg" />
      <Icon icon={<StarFilled16Icon />} path="v2/16/star-filled.svg" />
      <Icon icon={<Star16Icon />} path="v2/16/star.svg" />
      <Icon icon={<Stop16Icon />} path="v2/16/stop.svg" />
      <Icon icon={<ThreeDots16Icon />} path="v2/16/three-dots.svg" />
      <Icon icon={<TickInCircle16Icon />} path="v2/16/tick-in-circle.svg" />
      <Icon icon={<TickInFilledCircle16Icon />} path="v2/16/tick-in-filled-circle.svg" />
      <Icon icon={<Tick16Icon />} path="v2/16/tick.svg" />
      <Icon icon={<Trashbox16Icon />} path="v2/16/trashbox.svg" />
      <Icon icon={<Trash16Icon />} path="v2/16/trash.svg" />
      <Icon icon={<UsersGroupRoundedIcon />} path="v2/16/users-group-rounded.svg" />
      <Icon icon={<Upload16Icon />} path="v2/16/upload.svg" />
      <Icon icon={<Wallet16Icon />} path="v2/16/wallet.svg" />
      <Icon icon={<Warning16Icon />} path="v2/16/warning.svg" />
      <Icon icon={<UndoRightRoundIcon16 />} path="v2/16/undoLeftRoundSquare.svg" />
    </Wrapper>
    <Wrapper size="24">
      <Icon icon={<AnalysisChart24Icon />} path="v2/24/analysis-chart.svg" />
      <Icon icon={<Bank24Icon />} path="v2/24/bank.svg" />
      <Icon icon={<BookOpened24Icon />} path="v2/24/book-opened.svg" />
      <Icon icon={<Building24Icon />} path="v2/24/building.svg" />
      <Icon icon={<ChefHat24Icon />} path="v2/24/chef-hat.svg" />
      <Icon icon={<Calendar24Icon />} path="v2/24/calendar.svg" />
      <Icon icon={<Cash24Icon />} path="v2/24/cash.svg" />
      <Icon icon={<Clipboard24Icon />} path="v2/24/clipboard.svg" />
      <Icon icon={<Clock24Icon />} path="v2/24/clock.svg" />
      <Icon icon={<Clothes24Icon />} path="v2/24/clothes.svg" />
      <Icon icon={<CreditCard24Icon />} path="v2/24/credit-card.svg" />
      <Icon icon={<Delivery24Icon />} path="v2/24/delivery.svg" />
      <Icon icon={<Devices24Icon />} path="v2/24/devices.svg" />
      <Icon icon={<Download24Icon />} path="v2/24/download.svg" />
      <Icon icon={<DishPlateCover24Icon />} path="v2/24/dish-plate-cover.svg" />
      <Icon icon={<Fastfood24Icon />} path="v2/24/fastfood.svg" />
      <Icon icon={<FileAdd24Icon />} path="v2/24/file-add.svg" />
      <Icon icon={<HeartRate24Icon />} path="v2/24/heart-rate.svg" />
      <Icon icon={<Hospital24Icon />} path="v2/24/hospital.svg" />
      <Icon icon={<Key24Icon />} path="v2/24/key.svg" />
      <Icon icon={<LogoAdMob24Icon />} path="v2/24/logo-admob.svg" />
      <Icon icon={<LogoAdSense24Icon />} path="v2/24/logo-adsense.svg" />
      <Icon icon={<LogoAdWords24Icon />} path="v2/24/logo-adwords.svg" />
      <Icon icon={<LogoAdX24Icon />} path="v2/24/logo-adx.svg" />
      <Icon icon={<LogoAfterpay24Icon />} path="v2/24/logo-afterpay.svg" />
      <Icon icon={<LogoAmericanExpress24Icon />} path="v2/24/logo-amex.svg" />
      <Icon icon={<LogoApple24Icon />} path="v2/24/logo-apple.svg" />
      <Icon icon={<LogoFacebook24Icon />} path="v2/24/logo-facebook.svg" />
      <Icon icon={<LogoGooglePlay24Icon />} path="v2/24/logo-googleplay.svg" />
      <Icon icon={<LogoMastercard24Icon />} path="v2/24/logo-mastercard.svg" />
      <Icon icon={<LogoVisa24Icon />} path="v2/24/logo-visa.svg" />
      <Icon icon={<Notification24Icon />} path="v2/24/notification.svg" />
      <Icon icon={<ReceiptDiscount24Icon />} path="v2/24/receipt-discount.svg" />
      <Icon icon={<ShoppingCart24Icon />} path="v2/24/shopping-cart.svg" />
      <Icon icon={<SuitcaseS24Icon />} path="v2/24/suitcase-s.svg" />
      <Icon icon={<Wallet24Icon />} path="v2/24/wallet.svg" />
    </Wrapper>
    <Wrapper size="32">
      <Icon icon={<Alert32Icon />} path="v2/32/alert.svg" />
      <Icon icon={<BankCheque32Icon />} path="v2/32/bank-cheque.svg" />
      <Icon icon={<BoxCrossed32Icon />} path="v2/32/box-crossed.svg" />
      <Icon icon={<Box32Icon />} path="v2/32/box.svg" />
      <Icon icon={<Clipboard32Icon />} path="v2/32/clipboard.svg" />
      <Icon icon={<Devices32Icon />} path="v2/32/devices.svg" />
      <Icon icon={<Info32Icon />} path="v2/32/info.svg" />
      <Icon icon={<LogoAdMob32Icon />} path="v2/32/logo-admob.svg" />
      <Icon icon={<LogoAdSense32Icon />} path="v2/32/logo-adsense.svg" />
      <Icon icon={<LogoAdWords32Icon />} path="v2/32/logo-adwords.svg" />
      <Icon icon={<LogoAdX32Icon />} path="v2/32/logo-adx.svg" />
      <Icon icon={<LogoAfterpay32Icon />} path="v2/32/logo-afterpay.svg" />
      <Icon icon={<LogoAmex32Icon />} path="v2/32/logo-amex.svg" />
      <Icon icon={<LogoApple32Icon />} path="v2/32/logo-apple.svg" />
      <Icon icon={<LogoFacebook32Icon />} path="v2/32/logo-facebook.svg" />
      <Icon icon={<LogoGooglePlay32Icon />} path="v2/32/logo-googleplay.svg" />
      <Icon icon={<LogoMastercard32Icon />} path="v2/32/logo-mastercard.svg" />
      <Icon icon={<LogoVisa32Icon />} path="v2/32/logo-visa.svg" />
      <Icon icon={<MoneyIn32Icon />} path="v2/32/money-in.svg" />
      <Icon icon={<MoneyOut32Icon />} path="v2/32/money-out.svg" />
      <Icon icon={<MoneyStack32Icon />} path="v2/32/money-stack.svg" />
      <Icon icon={<Puzzle32Icon />} path="v2/32/puzzle.svg" />
      <Icon icon={<Scales32Icon />} path="v2/32/scales.svg" />
      <Icon icon={<Search32Icon />} path="v2/32/search.svg" />
      <Icon icon={<Trashbox32Icon />} path="v2/32/trashbox.svg" />
    </Wrapper>
    <Wrapper size="40">
      <Icon icon={<Reload40Icon />} path="v2/40/reload.svg" />
    </Wrapper>
    <Wrapper size="50">
      <Icon icon={<Search50Icon />} path="v2/50/search.svg" />
    </Wrapper>
    <Wrapper size="52">
      <Icon icon={<GooglePlayLogoIcon52 />} path="v2/52/google_play_logo_52.svg" />
      <Icon icon={<AppleLogoIcon52 />} path="v2/52/apple_logo_black_52.svg" />
    </Wrapper>
    <Wrapper size="56">
      <Icon icon={<ClipboardCurrency56Icon />} path="v2/56/clipboard-currency.svg" />
      <Icon icon={<ClipboardList56Icon />} path="v2/56/clipboard-list.svg" />
      <Icon icon={<File56Icon />} path="v2/56/file.svg" />
    </Wrapper>
    <Wrapper size="124">
      <Icon icon={<Empty124Icon />} path="v2/124/empty.svg" />
      <Icon icon={<EmptyTable124Icon />} path="v2/124/empty-table.svg" />
    </Wrapper>
    <Wrapper size="180">
      <Icon icon={<Table180Icon />} path="v2/180/table.svg" />
    </Wrapper>
  </>
);
