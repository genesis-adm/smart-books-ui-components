// 10px Import
import { ReactComponent as ArrowDown10Icon } from 'Svg/v2/10/arrow-down.svg';
import { ReactComponent as ArrowUp10Icon } from 'Svg/v2/10/arrow-up.svg';
import { ReactComponent as CaretBoth10Icon } from 'Svg/v2/10/caret-both.svg';
import { ReactComponent as Flash10Icon } from 'Svg/v2/10/flash.svg';
import { ReactComponent as Hand10Icon } from 'Svg/v2/10/hand.svg';
import { ReactComponent as Sort10Icon } from 'Svg/v2/10/sort.svg';
// 12px Import
import { ReactComponent as Arrows12Icon } from 'Svg/v2/12/arrows.svg';
import { ReactComponent as Briefcase12Icon } from 'Svg/v2/12/briefcase.svg';
import { ReactComponent as ChevronLeft12Icon } from 'Svg/v2/12/chevron-left.svg';
import { ReactComponent as ChevronRight12Icon } from 'Svg/v2/12/chevron-right.svg';
import { ReactComponent as ChevronUp12Icon } from 'Svg/v2/12/chevron-up.svg';
import { ReactComponent as ClipboardList12Icon } from 'Svg/v2/12/clipboard-list.svg';
import { ReactComponent as Hand12Icon } from 'Svg/v2/12/hand.svg';
import { ReactComponent as Info12Icon } from 'Svg/v2/12/info.svg';
import { ReactComponent as QuestionTransparent12Icon } from 'Svg/v2/12/question-transparent.svg';
import { ReactComponent as Question12Icon } from 'Svg/v2/12/question.svg';
// 16px Import
import { ReactComponent as Trash16Icon } from 'Svg/v2/16//trash.svg';
import { ReactComponent as Alert16Icon } from 'Svg/v2/16/alert.svg';
import { ReactComponent as ArrowDown16Icon } from 'Svg/v2/16/arrow-down.svg';
import { ReactComponent as ArrowLeft16Icon } from 'Svg/v2/16/arrow-left.svg';
import { ReactComponent as ArrowRightNarrow16Icon } from 'Svg/v2/16/arrow-right-narrow.svg';
import { ReactComponent as ArrowRight16Icon } from 'Svg/v2/16/arrow-right.svg';
import { ReactComponent as ArrowUp16Icon } from 'Svg/v2/16/arrow-up.svg';
import { ReactComponent as ArrowsSwitch16Icon } from 'Svg/v2/16/arrows-switch.svg';
import { ReactComponent as BanIcon } from 'Svg/v2/16/ban.svg';
import { ReactComponent as BankCheque16Icon } from 'Svg/v2/16/bank-cheque.svg';
import { ReactComponent as Bank16Icon } from 'Svg/v2/16/bank.svg';
import { ReactComponent as Banknote16Icon } from 'Svg/v2/16/banknote.svg';
import { ReactComponent as BriefcaseFilledDollar16Icon } from 'Svg/v2/16/briefcase-filled-dollar.svg';
import { ReactComponent as Briefcase16Icon } from 'Svg/v2/16/briefcase.svg';
import { ReactComponent as BulletDot16Icon } from 'Svg/v2/16/bullet-dot.svg';
import { ReactComponent as Calculator16Icon } from 'Svg/v2/16/calculator.svg';
import { ReactComponent as CalendarFilled16Icon } from 'Svg/v2/16/calendar-filled.svg';
import { ReactComponent as Calendar16Icon } from 'Svg/v2/16/calendar.svg';
import { ReactComponent as CaretDown16Icon } from 'Svg/v2/16/caret-down.svg';
import { ReactComponent as Cash16Icon } from 'Svg/v2/16/cash.svg';
import { ReactComponent as Charts16Icon } from 'Svg/v2/16/charts.svg';
import { ReactComponent as ChevronDoubleLeft16Icon } from 'Svg/v2/16/chevron-double-left.svg';
import { ReactComponent as ChevronDoubleRight16Icon } from 'Svg/v2/16/chevron-double-right.svg';
import { ReactComponent as ChevronDown16Icon } from 'Svg/v2/16/chevron-down.svg';
import { ReactComponent as ChevronLeft16Icon } from 'Svg/v2/16/chevron-left.svg';
import { ReactComponent as ChevronRight16Icon } from 'Svg/v2/16/chevron-right.svg';
import { ReactComponent as ChevronUp16Icon } from 'Svg/v2/16/chevron-up.svg';
import { ReactComponent as ClipboardCurrency16Icon } from 'Svg/v2/16/clipboard-currency.svg';
import { ReactComponent as ClipboardList16Icon } from 'Svg/v2/16/clipboard-list.svg';
import { ReactComponent as ClipboardTick16Icon } from 'Svg/v2/16/clipboard-tick.svg';
import { ReactComponent as ClockInFilledCircle16Icon } from 'Svg/v2/16/clock-filled.svg';
import { ReactComponent as Clock16Icon } from 'Svg/v2/16/clock.svg';
import { ReactComponent as Copy16Icon } from 'Svg/v2/16/copy.svg';
import { ReactComponent as CreditCard16Icon } from 'Svg/v2/16/credit-card.svg';
import { ReactComponent as CrossInCircle16Icon } from 'Svg/v2/16/cross-in-circle.svg';
import { ReactComponent as CrossInFilledCircle16Icon } from 'Svg/v2/16/cross-in-filled-circle.svg';
import { ReactComponent as Cross16Icon } from 'Svg/v2/16/cross.svg';
import { ReactComponent as DocumentSearch16Icon } from 'Svg/v2/16/document-search.svg';
import { ReactComponent as Download16Icon } from 'Svg/v2/16/download.svg';
import { ReactComponent as Drag16Icon } from 'Svg/v2/16/drag.svg';
import { ReactComponent as Envelope16Icon } from 'Svg/v2/16/envelope.svg';
import { ReactComponent as Erase16Icon } from 'Svg/v2/16/erase.svg';
import { ReactComponent as ExclamationInCircle16Icon } from 'Svg/v2/16/exclamation-in-circle.svg';
import { ReactComponent as ExclamationInFilledCircleIcon } from 'Svg/v2/16/exclamation-in-filled-circle.svg';
import { ReactComponent as ExclamationInFilledTriangle16Icon } from 'Svg/v2/16/exclamation-in-filled-triangle.svg';
import { ReactComponent as EyeCrossed16Icon } from 'Svg/v2/16/eye-crossed.svg';
import { ReactComponent as Eye16Icon } from 'Svg/v2/16/eye.svg';
import { ReactComponent as FileAdd16Icon } from 'Svg/v2/16/file-add.svg';
import { ReactComponent as FileBlank16Icon } from 'Svg/v2/16/file-blank.svg';
import { ReactComponent as FileCSV16Icon } from 'Svg/v2/16/file-csv.svg';
import { ReactComponent as FileDownload16Icon } from 'Svg/v2/16/file-download.svg';
import { ReactComponent as FilePDF16Icon } from 'Svg/v2/16/file-pdf.svg';
import { ReactComponent as FileTextFilled16Icon } from 'Svg/v2/16/file-text-filled.svg';
import { ReactComponent as FileText16Icon } from 'Svg/v2/16/file-text.svg';
import { ReactComponent as FileXLS16Icon } from 'Svg/v2/16/file-xls.svg';
import { ReactComponent as Filter16Icon } from 'Svg/v2/16/filter.svg';
import { ReactComponent as Gear16Icon } from 'Svg/v2/16/gear.svg';
import { ReactComponent as Google16Icon } from 'Svg/v2/16/google.svg';
import { ReactComponent as Hand16Icon } from 'Svg/v2/16/hand.svg';
import { ReactComponent as InfoInCircle16Icon } from 'Svg/v2/16/info-in-circle.svg';
import { ReactComponent as Info16Icon } from 'Svg/v2/16/info.svg';
import { ReactComponent as Invoice16Icon } from 'Svg/v2/16/invoice.svg';
import { ReactComponent as Lines16Icon } from 'Svg/v2/16/lines.svg';
import { ReactComponent as Link16Icon } from 'Svg/v2/16/link.svg';
import { ReactComponent as ListsAccounting16Icon } from 'Svg/v2/16/lists-accounting.svg';
import { ReactComponent as ListsBanking16Icon } from 'Svg/v2/16/lists-banking.svg';
import { ReactComponent as ListsOther16Icon } from 'Svg/v2/16/lists-other.svg';
import { ReactComponent as LockClosed16Icon } from 'Svg/v2/16/lock-closed.svg';
import { ReactComponent as LockOpened16Icon } from 'Svg/v2/16/lock-opened.svg';
import { ReactComponent as LogoAfterpay16Icon } from 'Svg/v2/16/logo-afterpay.svg';
import { ReactComponent as LogoAmericanExpress16Icon } from 'Svg/v2/16/logo-amex.svg';
import { ReactComponent as LogoMastercard16Icon } from 'Svg/v2/16/logo-mastercard.svg';
import { ReactComponent as LogoVisa16Icon } from 'Svg/v2/16/logo-visa.svg';
import { ReactComponent as MoneyIn16Icon } from 'Svg/v2/16/money-in.svg';
import { ReactComponent as MoneyOut16Icon } from 'Svg/v2/16/money-out.svg';
import { ReactComponent as MoneyStack16Icon } from 'Svg/v2/16/money-stack.svg';
import { ReactComponent as Pencil16Icon } from 'Svg/v2/16/pencil.svg';
import { ReactComponent as Person16Icon } from 'Svg/v2/16/person.svg';
import { ReactComponent as Piechart16Icon } from 'Svg/v2/16/piechart.svg';
import { ReactComponent as Plus16Icon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as QuestionInCircle16Icon } from 'Svg/v2/16/question-in-circle.svg';
import { ReactComponent as QuestionInFilledCircle16Icon } from 'Svg/v2/16/question-in-filled-circle.svg';
import { ReactComponent as QuestionTransparent16Icon } from 'Svg/v2/16/question-transparent.svg';
import { ReactComponent as Question16Icon } from 'Svg/v2/16/question.svg';
import { ReactComponent as Reload16Icon } from 'Svg/v2/16/reload.svg';
import { ReactComponent as Scales16Icon } from 'Svg/v2/16/scales.svg';
import { ReactComponent as Search16Icon } from 'Svg/v2/16/search.svg';
import { ReactComponent as Settings16Icon } from 'Svg/v2/16/settings.svg';
import { ReactComponent as Sigma16Icon } from 'Svg/v2/16/sigma.svg';
import { ReactComponent as Sort16Icon } from 'Svg/v2/16/sort.svg';
import { ReactComponent as StarInCircleIcon } from 'Svg/v2/16/star-circle.svg';
import { ReactComponent as StarFilled16Icon } from 'Svg/v2/16/star-filled.svg';
import { ReactComponent as Star16Icon } from 'Svg/v2/16/star.svg';
import { ReactComponent as Stop16Icon } from 'Svg/v2/16/stop.svg';
import { ReactComponent as ThreeDots16Icon } from 'Svg/v2/16/three-dots.svg';
import { ReactComponent as TickInCircle16Icon } from 'Svg/v2/16/tick-in-circle.svg';
import { ReactComponent as TickInFilledCircle16Icon } from 'Svg/v2/16/tick-in-filled-circle.svg';
import { ReactComponent as Tick16Icon } from 'Svg/v2/16/tick.svg';
import { ReactComponent as Trashbox16Icon } from 'Svg/v2/16/trashbox.svg';
import { ReactComponent as UndoRightRoundIcon16 } from 'Svg/v2/16/undoLeftRoundSquare.svg';
import { ReactComponent as Upload16Icon } from 'Svg/v2/16/upload.svg';
import { ReactComponent as UserRoundedIcon } from 'Svg/v2/16/user-rounded.svg';
import { ReactComponent as UsersGroupRoundedIcon } from 'Svg/v2/16/users-group-rounded.svg';
import { ReactComponent as Wallet16Icon } from 'Svg/v2/16/wallet.svg';
import { ReactComponent as Warning16Icon } from 'Svg/v2/16/warning.svg';
import { ReactComponent as CompanyIcon } from 'Svg/v2/20/buildings.svg';
// 20px Import
import { ReactComponent as IndividualsIcon } from 'Svg/v2/20/user-id.svg';
// 24px Import
import { ReactComponent as AnalysisChart24Icon } from 'Svg/v2/24/analysis-chart.svg';
import { ReactComponent as Bank24Icon } from 'Svg/v2/24/bank.svg';
import { ReactComponent as BookOpened24Icon } from 'Svg/v2/24/book-opened.svg';
import { ReactComponent as Building24Icon } from 'Svg/v2/24/building.svg';
import { ReactComponent as Calendar24Icon } from 'Svg/v2/24/calendar.svg';
import { ReactComponent as Cash24Icon } from 'Svg/v2/24/cash.svg';
import { ReactComponent as ChefHat24Icon } from 'Svg/v2/24/chef-hat.svg';
import { ReactComponent as Clipboard24Icon } from 'Svg/v2/24/clipboard.svg';
import { ReactComponent as Clock24Icon } from 'Svg/v2/24/clock.svg';
import { ReactComponent as Clothes24Icon } from 'Svg/v2/24/clothes.svg';
import { ReactComponent as CreditCard24Icon } from 'Svg/v2/24/credit-card.svg';
import { ReactComponent as Delivery24Icon } from 'Svg/v2/24/delivery.svg';
import { ReactComponent as Devices24Icon } from 'Svg/v2/24/devices.svg';
import { ReactComponent as DishPlateCover24Icon } from 'Svg/v2/24/dish-plate-cover.svg';
import { ReactComponent as Download24Icon } from 'Svg/v2/24/download.svg';
import { ReactComponent as Fastfood24Icon } from 'Svg/v2/24/fastfood.svg';
import { ReactComponent as FileAdd24Icon } from 'Svg/v2/24/file-add.svg';
import { ReactComponent as HeartRate24Icon } from 'Svg/v2/24/heart-rate.svg';
import { ReactComponent as Hospital24Icon } from 'Svg/v2/24/hospital.svg';
import { ReactComponent as Key24Icon } from 'Svg/v2/24/key.svg';
import { ReactComponent as LogoAdMob24Icon } from 'Svg/v2/24/logo-admob.svg';
import { ReactComponent as LogoAdSense24Icon } from 'Svg/v2/24/logo-adsense.svg';
import { ReactComponent as LogoAdWords24Icon } from 'Svg/v2/24/logo-adwords.svg';
import { ReactComponent as LogoAdX24Icon } from 'Svg/v2/24/logo-adx.svg';
import { ReactComponent as LogoAfterpay24Icon } from 'Svg/v2/24/logo-afterpay.svg';
import { ReactComponent as LogoAmericanExpress24Icon } from 'Svg/v2/24/logo-amex.svg';
import { ReactComponent as LogoApple24Icon } from 'Svg/v2/24/logo-apple.svg';
import { ReactComponent as LogoFacebook24Icon } from 'Svg/v2/24/logo-facebook.svg';
import { ReactComponent as LogoGooglePlay24Icon } from 'Svg/v2/24/logo-googleplay.svg';
import { ReactComponent as LogoMastercard24Icon } from 'Svg/v2/24/logo-mastercard.svg';
import { ReactComponent as LogoVisa24Icon } from 'Svg/v2/24/logo-visa.svg';
import { ReactComponent as Notification24Icon } from 'Svg/v2/24/notification.svg';
import { ReactComponent as ReceiptDiscount24Icon } from 'Svg/v2/24/receipt-discount.svg';
import { ReactComponent as ShoppingCart24Icon } from 'Svg/v2/24/shopping-cart.svg';
import { ReactComponent as SuitcaseS24Icon } from 'Svg/v2/24/suitcase-s.svg';
import { ReactComponent as UserIcon } from 'Svg/v2/24/user-check.svg';
import { ReactComponent as Wallet24Icon } from 'Svg/v2/24/wallet.svg';
// 32px Import
import { ReactComponent as Alert32Icon } from 'Svg/v2/32/alert.svg';
import { ReactComponent as BankCheque32Icon } from 'Svg/v2/32/bank-cheque.svg';
import { ReactComponent as BoxCrossed32Icon } from 'Svg/v2/32/box-crossed.svg';
import { ReactComponent as Box32Icon } from 'Svg/v2/32/box.svg';
import { ReactComponent as Clipboard32Icon } from 'Svg/v2/32/clipboard.svg';
import { ReactComponent as Devices32Icon } from 'Svg/v2/32/devices.svg';
import { ReactComponent as Info32Icon } from 'Svg/v2/32/info.svg';
import { ReactComponent as LogoAdMob32Icon } from 'Svg/v2/32/logo-admob.svg';
import { ReactComponent as LogoAdSense32Icon } from 'Svg/v2/32/logo-adsense.svg';
import { ReactComponent as LogoAdWords32Icon } from 'Svg/v2/32/logo-adwords.svg';
import { ReactComponent as LogoAdX32Icon } from 'Svg/v2/32/logo-adx.svg';
import { ReactComponent as LogoAfterpay32Icon } from 'Svg/v2/32/logo-afterpay.svg';
import { ReactComponent as LogoAmex32Icon } from 'Svg/v2/32/logo-amex.svg';
import { ReactComponent as LogoApple32Icon } from 'Svg/v2/32/logo-apple.svg';
import { ReactComponent as LogoFacebook32Icon } from 'Svg/v2/32/logo-facebook.svg';
import { ReactComponent as LogoGooglePlay32Icon } from 'Svg/v2/32/logo-googleplay.svg';
import { ReactComponent as LogoMastercard32Icon } from 'Svg/v2/32/logo-mastercard.svg';
import { ReactComponent as LogoVisa32Icon } from 'Svg/v2/32/logo-visa.svg';
import { ReactComponent as MoneyIn32Icon } from 'Svg/v2/32/money-in.svg';
import { ReactComponent as MoneyOut32Icon } from 'Svg/v2/32/money-out.svg';
import { ReactComponent as MoneyStack32Icon } from 'Svg/v2/32/money-stack.svg';
import { ReactComponent as Puzzle32Icon } from 'Svg/v2/32/puzzle.svg';
import { ReactComponent as Scales32Icon } from 'Svg/v2/32/scales.svg';
import { ReactComponent as Search32Icon } from 'Svg/v2/32/search.svg';
import { ReactComponent as TaggedBellIcon } from 'Svg/v2/32/tagged-bell.svg';
import { ReactComponent as Trashbox32Icon } from 'Svg/v2/32/trashbox.svg';
// 40px Import
import { ReactComponent as Reload40Icon } from 'Svg/v2/40/reload.svg';
// 50px Import
import { ReactComponent as Search50Icon } from 'Svg/v2/50/search.svg';
// 52ps Import
import { ReactComponent as AppleLogoIcon52 } from 'Svg/v2/52/apple_logo_black_52.svg';
import { ReactComponent as GooglePlayLogoIcon52 } from 'Svg/v2/52/google_play_logo_52.svg';
// 56px Import
import { ReactComponent as ClipboardCurrency56Icon } from 'Svg/v2/56/clipboard-currency.svg';
import { ReactComponent as ClipboardList56Icon } from 'Svg/v2/56/clipboard-list.svg';
import { ReactComponent as File56Icon } from 'Svg/v2/56/file.svg';
import { ReactComponent as EmptyTable124Icon } from 'Svg/v2/124/empty-table.svg';
// 124px Import
import { ReactComponent as Empty124Icon } from 'Svg/v2/124/empty.svg';
// 180px Import
import { ReactComponent as Table180Icon } from 'Svg/v2/180/table.svg';
import { ReactComponent as ManWithDocs } from 'Svg/v2/colored/man-with-docs.svg';

// 10px Export
export { ArrowDown10Icon, ArrowUp10Icon, CaretBoth10Icon, Flash10Icon, Hand10Icon, Sort10Icon };
// 12px Export
export {
  Arrows12Icon,
  Briefcase12Icon,
  ChevronLeft12Icon,
  ChevronRight12Icon,
  ChevronUp12Icon,
  ClipboardList12Icon,
  Hand12Icon,
  Info12Icon,
  QuestionTransparent12Icon,
  Question12Icon,
};
// 16px Export
export {
  Alert16Icon,
  ArrowDown16Icon,
  ArrowLeft16Icon,
  ArrowRightNarrow16Icon,
  ArrowRight16Icon,
  ArrowUp16Icon,
  ArrowsSwitch16Icon,
  BanIcon,
  BankCheque16Icon,
  Bank16Icon,
  Banknote16Icon,
  BriefcaseFilledDollar16Icon,
  Briefcase16Icon,
  BulletDot16Icon,
  Calculator16Icon,
  CalendarFilled16Icon,
  Calendar16Icon,
  Cash16Icon,
  CaretDown16Icon,
  Charts16Icon,
  ChevronDoubleLeft16Icon,
  ChevronDoubleRight16Icon,
  ChevronDown16Icon,
  ChevronLeft16Icon,
  ChevronRight16Icon,
  ChevronUp16Icon,
  ClipboardCurrency16Icon,
  ClipboardList16Icon,
  ClipboardTick16Icon,
  ClockInFilledCircle16Icon,
  Clock16Icon,
  Copy16Icon,
  CreditCard16Icon,
  CrossInCircle16Icon,
  CrossInFilledCircle16Icon,
  Cross16Icon,
  Download16Icon,
  Drag16Icon,
  Envelope16Icon,
  Erase16Icon,
  ExclamationInCircle16Icon,
  ExclamationInFilledTriangle16Icon,
  EyeCrossed16Icon,
  Eye16Icon,
  FileAdd16Icon,
  FileBlank16Icon,
  FileCSV16Icon,
  FileDownload16Icon,
  FilePDF16Icon,
  FileTextFilled16Icon,
  FileText16Icon,
  FileXLS16Icon,
  Filter16Icon,
  Gear16Icon,
  Google16Icon,
  Hand16Icon,
  InfoInCircle16Icon,
  Info16Icon,
  Invoice16Icon,
  Lines16Icon,
  Link16Icon,
  ListsAccounting16Icon,
  ListsBanking16Icon,
  ListsOther16Icon,
  LockClosed16Icon,
  LockOpened16Icon,
  LogoAfterpay16Icon,
  LogoAmericanExpress16Icon,
  LogoMastercard16Icon,
  LogoVisa16Icon,
  MoneyIn16Icon,
  MoneyOut16Icon,
  MoneyStack16Icon,
  Pencil16Icon,
  Person16Icon,
  Piechart16Icon,
  Plus16Icon,
  QuestionInCircle16Icon,
  QuestionInFilledCircle16Icon,
  QuestionTransparent16Icon,
  Question16Icon,
  Reload16Icon,
  Scales16Icon,
  Search16Icon,
  Settings16Icon,
  Sigma16Icon,
  Sort16Icon,
  StarFilled16Icon,
  Star16Icon,
  Stop16Icon,
  ThreeDots16Icon,
  TickInCircle16Icon,
  TickInFilledCircle16Icon,
  Tick16Icon,
  Trashbox16Icon,
  Upload16Icon,
  Wallet16Icon,
  Warning16Icon,
  DocumentSearch16Icon,
  Trash16Icon,
  StarInCircleIcon,
  UserRoundedIcon,
  ExclamationInFilledCircleIcon,
  UsersGroupRoundedIcon,
  UndoRightRoundIcon16,
};
// 20px Export
export { IndividualsIcon, CompanyIcon };

// 24px Export
export {
  AnalysisChart24Icon,
  Bank24Icon,
  BookOpened24Icon,
  Building24Icon,
  ChefHat24Icon,
  Calendar24Icon,
  Cash24Icon,
  Clipboard24Icon,
  Clock24Icon,
  Clothes24Icon,
  CreditCard24Icon,
  Delivery24Icon,
  Devices24Icon,
  DishPlateCover24Icon,
  Download24Icon,
  Fastfood24Icon,
  FileAdd24Icon,
  HeartRate24Icon,
  Hospital24Icon,
  Key24Icon,
  LogoAdMob24Icon,
  LogoAdSense24Icon,
  LogoAdWords24Icon,
  LogoAdX24Icon,
  LogoAfterpay24Icon,
  LogoAmericanExpress24Icon,
  LogoApple24Icon,
  LogoFacebook24Icon,
  LogoGooglePlay24Icon,
  LogoMastercard24Icon,
  LogoVisa24Icon,
  Notification24Icon,
  ReceiptDiscount24Icon,
  ShoppingCart24Icon,
  SuitcaseS24Icon,
  Wallet24Icon,
  UserIcon,
};
// 32px Export
export {
  Alert32Icon,
  BankCheque32Icon,
  BoxCrossed32Icon,
  Box32Icon,
  Clipboard32Icon,
  Devices32Icon,
  Info32Icon,
  LogoAdMob32Icon,
  LogoAdSense32Icon,
  LogoAdWords32Icon,
  LogoAdX32Icon,
  LogoAfterpay32Icon,
  LogoAmex32Icon,
  LogoApple32Icon,
  LogoFacebook32Icon,
  LogoGooglePlay32Icon,
  LogoMastercard32Icon,
  LogoVisa32Icon,
  MoneyIn32Icon,
  MoneyOut32Icon,
  MoneyStack32Icon,
  Puzzle32Icon,
  Scales32Icon,
  Search32Icon,
  Trashbox32Icon,
  TaggedBellIcon,
};
// 40px Export
export { Reload40Icon };
// 50px Export
export { Search50Icon };
// 52px Export
export { AppleLogoIcon52, GooglePlayLogoIcon52 };
// 56px Export
export { ClipboardCurrency56Icon, ClipboardList56Icon, File56Icon };
// 124px Export
export { Empty124Icon, EmptyTable124Icon };
// 180px Export
export { Table180Icon };
export { ManWithDocs };
