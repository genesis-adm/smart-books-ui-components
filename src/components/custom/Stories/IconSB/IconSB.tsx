import { Tooltip } from 'Components/base/Tooltip';
import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React from 'react';

import style from './IconSB.module.scss';

interface IconSBProps {
  icon: React.ReactNode;
  path: string;
}

interface IconContainerSBProps {
  size: '10' | '12' | '16' | '24' | '32' | '40' | '50' | '52' | '56' | '124' | '180';
  children?: React.ReactNode;
}

const IconSB: React.FC<IconSBProps> = ({ icon, path }) => (
  <Tooltip wrapperClassName={style.element} message={path}>
    <div className={style.icon}>{icon}</div>
  </Tooltip>
);

const IconContainerSB: React.FC<IconContainerSBProps> = ({ size, children }) => (
  <div className={style.wrapper}>
    <Text type="title-bold" align="center" className={style.title}>
      {size}
      px
    </Text>
    <div className={style.container}>
      <div className={classNames(style.component, style[`size_${size}`])}>{children}</div>
    </div>
  </div>
);

export { IconSB, IconContainerSB };
