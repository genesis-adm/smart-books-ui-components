import { Meta, Story } from '@storybook/react';
import {
  ExportStatus as Component,
  ExportStatusProps as Props,
} from 'Components/custom/Export/ExportStatus';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Export/Export Status',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const ExportStatusComponent: Story<Props> = (args) => <Component {...args} />;

export const ExportStatus = ExportStatusComponent.bind({});
ExportStatus.args = {
  status: 'success',
};
