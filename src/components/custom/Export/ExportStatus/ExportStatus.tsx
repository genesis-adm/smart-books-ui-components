import { Icon } from 'Components/base/Icon';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as PendingIcon } from 'Svg/v2/16/clock.svg';
import { ReactComponent as FailedIcon } from 'Svg/v2/16/cross.svg';
import { ReactComponent as DoneIcon } from 'Svg/v2/16/tick.svg';
import classNames from 'classnames';
import React from 'react';

import style from './ExportStatus.module.scss';

const content = {
  new: {
    icon: <PendingIcon />,
    text: 'In progress',
  },
  failed: {
    icon: <FailedIcon />,
    text: 'Failed',
  },
  success: {
    icon: <DoneIcon />,
    text: 'Done',
  },
};

export interface ExportStatusProps {
  status: 'new' | 'failed' | 'success';
  className?: string;
}

const ExportStatus: React.FC<ExportStatusProps> = ({ status, className }) => (
  <div className={classNames(style.component, className)}>
    <Icon
      display="flex"
      className={classNames(style.icon, style[`status_${status}`])}
      icon={content[status].icon}
      path="inherit"
    />
    <Text className={style.text} type="caption-regular">
      {content[status].text}
    </Text>
  </div>
);

export default ExportStatus;
