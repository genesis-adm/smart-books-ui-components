import { DropDownContext } from 'Components/DropDown';
import { Icon } from 'Components/base/Icon';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as ArrowIcon } from 'Svg/v2/16/arrow-right-narrow.svg';
import classNames from 'classnames';
import React, { ReactNode, useContext } from 'react';

import style from './ListsDropdownItem.module.scss';

export interface ListsDropdownTitleProps {
  label: string;
  icon?: ReactNode;
  className?: string;
}

export interface ListsDropdownItemProps extends ListsDropdownTitleProps {
  onClick(): void;
}

const ListsDropdownTitle: React.FC<ListsDropdownTitleProps> = ({ label, icon, className }) => (
  <div className={classNames(style.title, className)}>
    <Icon className={style.icon} icon={icon} staticColor />
    <Text type="body-regular-16" color="black-100">
      {label}
    </Text>
  </div>
);

const ListsDropdownItem: React.FC<ListsDropdownItemProps> = ({
  label,
  icon,
  className,
  onClick,
}) => {
  const { handleOpen } = useContext(DropDownContext);

  const handleClick = (e: React.MouseEvent<HTMLDivElement>) => {
    e.stopPropagation();
    onClick();
    if (handleOpen) handleOpen();
  };

  return (
    <div className={classNames(style.component, className)} onClick={handleClick}>
      <Icon className={style.arrow} icon={<ArrowIcon />} path="inherit" />
      {icon && <Icon className={style.icon} icon={icon} path="inherit" />}
      <Text type="body-regular-16" color="inherit" transition="unset">
        {label}
      </Text>
    </div>
  );
};

export { ListsDropdownItem, ListsDropdownTitle };
