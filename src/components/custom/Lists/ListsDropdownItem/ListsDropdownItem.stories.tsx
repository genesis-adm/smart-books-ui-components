import { Meta, Story } from '@storybook/react';
import {
  ListsDropdownItem as Component,
  ListsDropdownItemProps as Props,
} from 'Components/custom/Lists/ListsDropdownItem';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Lists/ListsDropdownItem',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const ListsDropdownItemComponent: Story<Props> = (args) => <Component {...args} />;

export const ListsDropdownItem = ListsDropdownItemComponent.bind({});
ListsDropdownItem.args = {
  label: 'Lists item name',
};
