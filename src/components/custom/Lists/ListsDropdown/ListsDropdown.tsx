import { DropDown } from 'Components/DropDown';
import { IconButton } from 'Components/base/buttons/IconButton';
import { ReactComponent as ListsIcon } from 'Svg/v2/20/overview-filled.svg';
import classNames from 'classnames';
import React, { ReactNode } from 'react';

import style from './ListsDropdown.module.scss';

export interface ListsDropdownProps {
  className?: string;
  children?: ReactNode;
}

const ListsDropdown: React.FC<ListsDropdownProps> = ({ className, children }) => (
  <DropDown
    classNameOuter={classNames(style.wrapper, className)}
    classnameInner={style.container}
    padding="32"
    gap="24"
    control={({ isOpen, handleOpen }) => (
      <IconButton
        size="32"
        background="transparent-blue-10"
        color="grey-100-violet-90"
        tooltip={!isOpen && 'Lists'}
        icon={<ListsIcon />}
        onClick={handleOpen}
      />
    )}
  >
    {children}
  </DropDown>
);

export default ListsDropdown;
