import { Meta, Story } from '@storybook/react';
import {
  ListsDropdown as Component,
  ListsDropdownProps as Props,
} from 'Components/custom/Lists/ListsDropdown';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Lists/Lists Dropdown',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const ListsDropdownComponent: Story<Props> = (args) => <Component {...args} />;

export const ListsDropdown = ListsDropdownComponent.bind({});
ListsDropdown.args = {};
