import { Tooltip } from 'Components/base/Tooltip';
import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React, { useEffect, useRef, useState } from 'react';

import style from './ItemMultiselectName.module.scss';

export interface ItemMultiselectNameProps {
  name: string;
  className?: string;
}

const ItemMultiselectName: React.FC<ItemMultiselectNameProps> = ({ name, className }) => {
  const [textOverflow, setTextOverflow] = useState(false);
  const textRef = useRef<HTMLDivElement>(null);
  useEffect(() => {
    if (textRef.current) {
      setTextOverflow(textRef.current.offsetWidth > 88);
    }
  }, [name]);

  return (
    <Tooltip
      message={textOverflow ? name : ''}
      wrapperClassName={classNames(
        style.nameWrapper,
        { [style.textOverflow]: textOverflow },
        className,
      )}
    >
      <Text className={style.name} ref={textRef} align="center" color="grey-100" noWrap>
        {name}
      </Text>
    </Tooltip>
  );
};

export default ItemMultiselectName;
