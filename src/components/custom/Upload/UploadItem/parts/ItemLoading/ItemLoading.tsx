import { UploadCircle } from 'Components/custom/Upload/UploadCircle';
import classNames from 'classnames';
import React from 'react';

import style from './ItemLoading.module.scss';

export interface ItemLoadingProps {
  className?: string;
}

const ItemLoading: React.FC<ItemLoadingProps> = ({ className }) => (
  <div className={classNames(style.component, className)}>
    <UploadCircle />
  </div>
);

export default ItemLoading;
