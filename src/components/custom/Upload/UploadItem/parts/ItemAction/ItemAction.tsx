import { Icon } from 'Components/base/Icon';
import { Text } from 'Components/base/typography/Text';
import { HoverContext } from 'Components/custom/Upload/UploadContainer/UploadContainer';
import { ReactComponent as DownloadIcon } from 'Svg/v2/24/download.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/32/trashbox.svg';
import { ReactComponent as ReloadIcon } from 'Svg/v2/40/reload.svg';
import classNames from 'classnames';
import React, { useContext } from 'react';

import style from './ItemAction.module.scss';

export interface ItemActionProps {
  isLoaded: boolean;
  isError: boolean;
  className?: string;
  onDelete(): void;
  onReload?(): void;
  onDownload?(): void;
}

const ItemAction: React.FC<ItemActionProps> = ({
  isLoaded,
  isError,
  className,
  onDelete,
  onReload,
  onDownload,
}) => {
  const { mode } = useContext(HoverContext);
  const handleClick = () => {
    if (isLoaded && mode === 'upload') onDelete();
    else if (isLoaded && mode === 'download') onDownload?.();
    else if (isError) onReload?.();
  };
  return (
    <div
      className={classNames(
        style.action,
        {
          [style.delete]: isLoaded && mode === 'upload',
          [style.download]: isLoaded && mode === 'download',
          [style.error]: isError,
        },
        className,
      )}
      role="presentation"
      onClick={handleClick}
    >
      {isLoaded && mode === 'upload' && (
        <>
          <Icon icon={<TrashIcon />} path="white-100" />
          <Text className={style.text} type="button" color="white-100">
            Delete
          </Text>
        </>
      )}
      {isLoaded && mode === 'download' && (
        <>
          <Icon icon={<DownloadIcon />} path="white-100" />
          <Text className={style.download} type="button" color="white-100">
            Download
          </Text>
        </>
      )}
      {isError && <Icon icon={<ReloadIcon />} path="white-100" />}
    </div>
  );
};

export default ItemAction;
