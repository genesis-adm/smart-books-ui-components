import { ItemLoading } from 'Components/custom/Upload/UploadItem/parts/ItemLoading';
import { Logo } from 'Components/elements/Logo';
import fileReader from 'Utils/fileReader';
import React, { ReactElement, useEffect, useState } from 'react';

import { OwnFile } from '../../../../../../../types/general';

type ImgLoadedProps = {
  name: string;
  file: OwnFile;
};

const ImagePreview = ({ name, file }: ImgLoadedProps): ReactElement => {
  const [imgSrc, setImgSrc] = useState('');

  useEffect(() => {
    file?.src ? setImgSrc(file?.src) : fileToBase64(file).then((src) => setImgSrc(src));
  }, [file]);

  return imgSrc ? <Logo size="xl" content="company" name={name} img={imgSrc} /> : <ItemLoading />;
};

export default ImagePreview;

const fileToBase64 = async (item: File): Promise<string> => {
  return await fileReader(item);
};
