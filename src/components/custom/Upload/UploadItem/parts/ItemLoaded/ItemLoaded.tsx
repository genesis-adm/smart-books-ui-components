import { Icon } from 'Components/base/Icon';
import { Text } from 'Components/base/typography/Text';
import ImagePreview from 'Components/custom/Upload/UploadItem/parts/ItemLoaded/components/ImagePreview';
import { ReactComponent as SuccessIcon } from 'Svg/v2/16/tick.svg';
import { ReactComponent as FileIcon } from 'Svg/v2/56/file.svg';
import { fileExtensionByName, fileTypeByExtension } from 'Utils/fileHandlers';
import React from 'react';

import { OwnFile } from '../../../../../../types/general';
import type { BackgroundColorStaticNewTypes } from '../../../../../types/colorTypes';
import style from './ItemLoaded.module.scss';

export interface ItemLoadedProps {
  file: OwnFile;
}

const ItemLoaded: React.FC<ItemLoadedProps> = ({ file }) => {
  const { name } = file;

  const fileExtension = fileExtensionByName(name);
  const fileType = fileTypeByExtension(fileExtension);

  let color: BackgroundColorStaticNewTypes;
  if (fileType === 'excel') {
    color = 'green-90';
  } else if (fileType === 'pdf') {
    color = 'red-90';
  } else {
    color = 'violet-90';
  }

  if (fileType === 'image') return <ImagePreview name={name} file={file} />;

  return (
    <>
      <div className={style.success}>
        <Icon icon={<SuccessIcon />} path="white-100" />
      </div>
      <div className={style.file}>
        <Icon icon={<FileIcon />} path={color} />
        <Text className={style.filetype} type="filename" color="white-100" noWrap>
          {fileExtension}
        </Text>
      </div>
    </>
  );
};

export default ItemLoaded;
