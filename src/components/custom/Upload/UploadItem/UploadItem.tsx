import React from 'react';

import { OwnFile } from '../../../../types/general';
import style from './UploadItem.module.scss';
import { ItemAction } from './parts/ItemAction';
import { ItemLoaded } from './parts/ItemLoaded';
import { ItemMultiselectName } from './parts/ItemMultiselectName';

export interface UploadItemProps {
  file: OwnFile;
  isLoaded: boolean;
  isError: boolean;
  multi?: boolean;
  onDelete(file: OwnFile): void;
  onReload?(): void;
  onDownload?(file: OwnFile): void;
}

const UploadItem: React.FC<UploadItemProps> = ({
  file,
  isLoaded,
  isError,
  multi = false,
  onDelete,
  onReload,
  onDownload,
}) => {
  const { name } = file;

  const remove = (): void => {
    onDelete?.(file);
  };

  const download = (): void => {
    onDownload?.(file);
  };

  return (
    <div className={style.wrapper}>
      <div className={style.component}>
        {isLoaded && <ItemLoaded file={file} />}

        <ItemAction
          className={style.action}
          isLoaded={isLoaded}
          isError={isError}
          onDelete={remove}
          onReload={onReload}
          onDownload={download}
        />
      </div>

      {multi && <ItemMultiselectName name={name} />}
    </div>
  );
};

export default UploadItem;
