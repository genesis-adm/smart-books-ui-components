import { Icon } from 'Components/base/Icon';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as ImageAddIcon } from 'Svg/24/addimage.svg';
import { ReactComponent as FileAddIcon } from 'Svg/v2/24/file-add.svg';
import { ReactComponent as FileVoidIcon } from 'Svg/v2/40/file.svg';
import classNames from 'classnames';
import React, { useContext, useRef } from 'react';

import { FileType } from '../../../../types/general';
import { HoverContext } from '../UploadContainer/UploadContainer';
import style from './UploadInput.module.scss';

export type UploadInputLimit = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10;

export interface UploadInputProps {
  name: string;
  acceptedFilesType?: FileType;
  limit?: UploadInputLimit;
  onChange?: (file: File) => void;
}

const UploadInput: React.FC<UploadInputProps> = ({
  name,
  acceptedFilesType,
  limit = 1,
  onChange,
}) => {
  const inputRef = useRef<HTMLInputElement>(null);

  const { hoverValue, setHoverValue, mode } = useContext(HoverContext);

  const multi = limit > 1;

  const onMouseEnterHandler = () => {
    setHoverValue(true);
  };

  const onMouseLeaveHandler = () => {
    setHoverValue(false);
  };

  const changeInput = ({ target }: React.ChangeEvent<HTMLInputElement>): void => {
    const { files } = target;

    if (!files) return;

    if (multi) {
      const validLengthArray = Array.from(files).slice(0, limit);

      validLengthArray.forEach((file) => {
        onChange?.(file);
      });
    } else {
      onChange?.(files[0]);
    }

    (inputRef.current as HTMLInputElement).value = '';
  };

  const handleClick = (): void => {
    if (inputRef.current && mode === 'upload') inputRef.current.click();
  };

  const icon = acceptedFilesType === 'image' ? <ImageAddIcon /> : <FileAddIcon />;
  const accept = inputAcceptByFileType(acceptedFilesType);

  return (
    <div
      role="presentation"
      className={classNames(style.wrapper, {
        [style.hover]: hoverValue && mode === 'upload',
      })}
      onClick={handleClick}
      onMouseEnter={onMouseEnterHandler}
      onMouseLeave={onMouseLeaveHandler}
    >
      {mode === 'upload' && (
        <>
          <Icon icon={icon} path="inherit" />
          <Text className={style.text} color="inherit" transition="unset">
            Add new
          </Text>
          <input
            ref={inputRef}
            className={style.input}
            id={name}
            type="file"
            multiple={multi}
            accept={accept}
            onChange={changeInput}
          />
        </>
      )}
      {mode === 'download' && <Icon icon={<FileVoidIcon />} path="inherit" />}
    </div>
  );
};

export default UploadInput;

const inputAcceptByFileType = (fileType?: FileType): string => {
  switch (fileType) {
    case 'image':
      return 'image/*';
    case 'excel':
      return '.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel';
    case 'pdf':
      return '.pdf';
    case 'csv':
      return '.csv';
    default:
      return '*';
  }
};
