import { Meta, Story } from '@storybook/react';
import {
  UploadContainer as Component,
  UploadContainerProps as Props,
} from 'Components/custom/Upload/UploadContainer';
import { UploadInfo } from 'Components/custom/Upload/UploadInfo';
import { UploadItemField } from 'Components/custom/Upload/UploadItemField';
import { checkFileType, fileExtensionByName, isFileInArray } from 'Utils/fileHandlers';
import React, { useEffect, useState } from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Modules/Upload',
  component: Component,
  argTypes: {
    className: hideProperty,
    onDrop: hideProperty,
  },
} as Meta;

export const UploadFileSingle: Story<Props> = () => {
  const [file, setFile] = useState<File | null>(null);
  const handleFileUpload = (item: File) => {
    setFile(item);
  };
  const handleFileDelete = () => {
    setFile(null);
  };
  const handleFileDrop = (e: React.DragEvent<HTMLElement>) => {
    const dropFile = e.dataTransfer.files[0];
    if (checkFileType(dropFile, ['xls', 'xlsx', 'csv'])) {
      setFile(e.dataTransfer.files[0]);
    }
  };
  return (
    <Component onDrop={handleFileDrop}>
      <UploadItemField
        name="uploader"
        files={file}
        acceptFilesType="excel"
        onChange={handleFileUpload}
        onDelete={handleFileDelete}
      />
      <UploadInfo
        mainText={file ? file.name : 'Drag and drop or Choose file'}
        secondaryText="XLS; XLSX; CSV; Maximum file size is 5MB"
      />
    </Component>
  );
};

export const UploadImageSingle: Story<Props> = () => {
  const [image, setImage] = useState<File | null>(null);
  const handleImageUpload = (item: File) => {
    setImage(item);
  };
  const handleImageDelete = () => {
    setImage(null);
  };
  const handleImageDrop = (e: React.DragEvent<HTMLElement>) => {
    const dropImage = e.dataTransfer.files[0];
    if (checkFileType(dropImage, ['jpg', 'jpeg', 'png', 'webp'])) {
      setImage(dropImage);
    }
  };

  return (
    <Component onDrop={handleImageDrop}>
      <UploadItemField
        name="uploader"
        files={image}
        acceptFilesType="image"
        onChange={handleImageUpload}
        onDelete={handleImageDelete}
      />
      <UploadInfo
        mainText={image ? image.name : 'Drag and drop or Choose file'}
        secondaryText="All image formats; Maximum file size is 5MB"
      />
    </Component>
  );
};

export const UploadFileMulti: Story<Props> = () => {
  const [files, setFiles] = useState<File[]>([]);
  const handleFileUpload = (file: File) => {
    if (!isFileInArray(file, files)) {
      setFiles((prevState) => [...prevState, file]);
    }
  };
  const handleFileDelete = ({ name, size }: { name: string; size: number }) => {
    setFiles((prev) => prev.filter((file) => !(file.name === name && file.size === size)));
  };
  const handleFileDrop = (e: React.DragEvent<HTMLElement>) => {
    const fileArray = e.dataTransfer.files;
    Array.from(fileArray).forEach((file) => {
      if (checkFileType(file, ['xls', 'xlsx', 'csv']) && !isFileInArray(file, files)) {
        setFiles((prevState) => [...prevState, file]);
      }
    });
  };

  return (
    <Component onDrop={handleFileDrop}>
      <UploadItemField
        name="uploader"
        files={files}
        limit={10}
        onChange={handleFileUpload}
        onDelete={handleFileDelete}
      />
      {!files.length && (
        <UploadInfo
          mainText="Drag and drop or Choose file"
          secondaryText="XLS; XLSX; CSV; Maximum file size is 5MB"
        />
      )}
    </Component>
  );
};

export const UploadImageMulti: Story<Props> = () => {
  const [images, setImages] = useState<File[]>([]);
  const handleImageUpload = (image: File) => {
    if (!isFileInArray(image, images)) {
      setImages((prevState) => [...prevState, image]);
    }
  };
  const handleImageDelete = ({ name, size }: { name: string; size: number }) => {
    setImages((prev) => prev.filter((image) => !(image.name === name && image.size === size)));
  };

  const handleImageDrop = (e: React.DragEvent<HTMLElement>) => {
    const imageArray = e.dataTransfer.files;

    Array.from(imageArray).forEach((image) => {
      if (checkFileType(image, ['jpg', 'jpeg', 'png', 'webp']) && !isFileInArray(image, images)) {
        setImages((prevState) => [...prevState, image]);
      }
    });
  };
  return (
    <Component onDrop={handleImageDrop}>
      <UploadItemField
        name="uploader"
        files={images}
        acceptFilesType="image"
        limit={10}
        onChange={handleImageUpload}
        onDelete={handleImageDelete}
      />
      {!images.length && (
        <UploadInfo
          mainText="Drag and drop or Choose file"
          secondaryText="All images; Maximum file size is 5MB"
        />
      )}
    </Component>
  );
};

export const UploadSingleDownloadMode: Story<Props> = () => {
  // const [file, setFile] = useState<File | null>(null);
  const [file, setFile] = useState<File | null>(new File([''], 'excel.xlsx'));
  const handleFileUpload = (item: File) => {
    setFile(item);
  };
  const handleFileDelete = () => {
    setFile(null);
  };
  const handleFileDrop = (e: React.DragEvent<HTMLElement>) => {
    const dropFile = e.dataTransfer.files[0];
    if (checkFileType(dropFile, ['xls', 'xlsx', 'csv'])) {
      setFile(e.dataTransfer.files[0]);
    }
  };
  return (
    <Component onDrop={handleFileDrop} mode="download">
      <UploadItemField
        name="uploader"
        files={file}
        acceptFilesType="excel"
        onChange={handleFileUpload}
        onDelete={handleFileDelete}
        onReload={() => {}}
        onDownload={() => console.log('downloaded')}
      />
      <UploadInfo
        mainText={file ? file.name : 'No files uploaded'}
        secondaryText="View mode. You can't upload files."
      />
    </Component>
  );
};

export const UploadMultiDownloadMode: Story<Props> = () => {
  const filesArray = Array.from(Array(10)).map(
    (_, index) => new File([''], `excel-${index + 1}.xlsx`),
  );
  // const [files, setFiles] = useState<File[]>([]);
  const [files, setFiles] = useState<File[]>(filesArray);
  const handleFileUpload = (file: File) => {
    if (!isFileInArray(file, files)) {
      setFiles((prevState) => [...prevState, file]);
    }
  };
  const handleFileDelete = ({ name, size }: { name: string; size: number }) => {
    setFiles((prev) => prev.filter((file) => !(file.name === name && file.size === size)));
  };
  const handleFileDrop = (e: React.DragEvent<HTMLElement>) => {
    const fileArray = e.dataTransfer.files;
    Array.from(fileArray).forEach((file) => {
      if (checkFileType(file, ['xls', 'xlsx', 'csv']) && !isFileInArray(file, files)) {
        setFiles((prevState) => [...prevState, file]);
      }
    });
  };
  return (
    <Component onDrop={handleFileDrop} mode="download">
      <UploadItemField
        name="uploader"
        files={files}
        acceptFilesType="excel"
        limit={10}
        onChange={handleFileUpload}
        onDelete={handleFileDelete}
        onReload={() => {}}
        onDownload={() => console.log('downloaded')}
      />
      {!files.length && (
        <UploadInfo
          mainText="No files uploaded - multiple loader"
          secondaryText="View mode. You can't upload files."
        />
      )}
    </Component>
  );
};
