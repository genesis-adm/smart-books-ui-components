import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React from 'react';

import style from './UploadInfo.module.scss';

export interface UploadInfoProps {
  mainText: string;
  secondaryText: string;
  className?: string;
}

const UploadInfo: React.FC<UploadInfoProps> = ({ mainText, secondaryText, className }) => (
  <Container className={classNames(style.container, className)} flexdirection="column">
    <Text className={style.main} type="button" noWrap>
      {mainText}
    </Text>
    <Text className={style.secondary} color="grey-100" noWrap>
      {secondaryText}
    </Text>
  </Container>
);

export default UploadInfo;
