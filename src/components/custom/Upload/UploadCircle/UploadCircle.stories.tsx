import { Meta, Story } from '@storybook/react';
import {
  UploadCircle as Component,
  UploadCircleProps as Props,
} from 'Components/custom/Upload/UploadCircle';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Graphics/Upload Circle',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const UploadCircleComponent: Story<Props> = (args) => <Component {...args} />;

export const UploadCircle = UploadCircleComponent.bind({});
UploadCircle.args = {};
