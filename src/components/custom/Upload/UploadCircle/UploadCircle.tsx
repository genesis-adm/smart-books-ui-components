import classNames from 'classnames';
import React from 'react';

import style from './UploadCircle.module.scss';

export interface UploadCircleProps {
  className?: string;
}

const UploadCircle: React.FC<UploadCircleProps> = ({ className }) => (
  <div className={classNames(style.loading, className)} />
);

export default UploadCircle;
