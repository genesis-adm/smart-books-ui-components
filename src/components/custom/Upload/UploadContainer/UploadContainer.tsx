import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React, { CSSProperties, createContext, useMemo, useState } from 'react';

import styles from './UploadContainer.module.scss';

type Mode = 'upload' | 'download';

export interface UploadContainerProps {
  mode?: Mode;
  className?: string;
  children?: React.ReactNode;
  onDrop(e: React.DragEvent<HTMLElement>): void;
  style?: CSSProperties;
}

interface UploadContainerContextProps {
  mode: Mode;
  hoverValue: boolean;
  setHoverValue(value: boolean): void;
}

export const HoverContext = createContext({} as UploadContainerContextProps);

const UploadContainer: React.FC<UploadContainerProps> = ({
  mode = 'upload',
  className,
  onDrop,
  children,
  style,
}) => {
  const [hover, setHover] = useState(false);
  const [inDropZone, setInDropZone] = useState(false);
  const handleDragEnter = (e: React.DragEvent<HTMLElement>) => {
    if (!inDropZone) {
      e.preventDefault();
      e.stopPropagation();
      setInDropZone(true);
      setHover(true);
    }
  };
  const handleDragLeave = (e: React.DragEvent<HTMLElement>) => {
    e.preventDefault();
    e.stopPropagation();
    setInDropZone(false);
    setHover(false);
  };
  const handleDrop = (e: React.DragEvent<HTMLElement>) => {
    handleDragLeave(e);
    onDrop?.(e);
  };
  const handleDragOver = (e: React.DragEvent<HTMLElement>) => {
    e.preventDefault();
    e.stopPropagation();
  };
  return (
    <HoverContext.Provider
      value={useMemo(
        () => ({
          hoverValue: hover,
          setHoverValue: setHover,
          mode,
        }),
        [hover, mode],
      )}
    >
      <div
        onDragEnter={handleDragEnter}
        className={classNames(
          styles.container,
          {
            [styles.hover]: hover,
            [styles.disabled]: mode === 'download',
          },
          className,
        )}
        style={style}
      >
        {inDropZone && (
          <div
            onDrop={handleDrop}
            onDragOver={handleDragOver}
            onDragLeave={handleDragLeave}
            className={styles.dropzone}
          >
            <Text type="title-regular" className={styles.text} color="violet-90">
              Drag files here
            </Text>
          </div>
        )}
        {children}
      </div>
    </HoverContext.Provider>
  );
};

export default UploadContainer;
