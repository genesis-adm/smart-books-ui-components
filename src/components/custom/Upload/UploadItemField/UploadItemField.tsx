import { HoverContext } from 'Components/custom/Upload/UploadContainer/UploadContainer';
import { UploadInput } from 'Components/custom/Upload/UploadInput';
import { UploadInputLimit } from 'Components/custom/Upload/UploadInput/UploadInput';
import { UploadItem } from 'Components/custom/Upload/UploadItem';
import classNames from 'classnames';
import React, { FC, ReactElement, useContext } from 'react';

import { FileType, Nullable, OwnFile } from '../../../../types/general';
import styles from './UploadItemField.module.scss';

export type UploadItemFieldProps = {
  name: string;
  files: Nullable<OwnFile | OwnFile[]> | undefined;
  previewSrc?: string;
  limit?: UploadInputLimit;
  acceptFilesType?: FileType;
  isLoaded?: boolean;
  isError?: boolean;
  onChange: (file: File) => void;
  onDelete: (file: OwnFile) => void;
  onReload?: () => void;
  onDownload?: (file: OwnFile) => void;
  className?: string;
};

const UploadItemField: FC<UploadItemFieldProps> = ({
  name,
  files,
  limit = 1,
  acceptFilesType,
  isLoaded = true,
  isError = false,
  onChange,
  onDelete,
  onReload,
  onDownload,
  className,
}: UploadItemFieldProps): ReactElement => {
  const { mode } = useContext(HoverContext);

  const validFiles = files ? (Array.isArray(files) ? files : [files]) : undefined;

  const multi = limit > 1;

  const showInput =
    ((limit <= 1 && !validFiles?.length) ||
      (limit > 1 && (!validFiles || validFiles.length < limit))) &&
    mode !== 'download';

  return (
    <div className={classNames(styles.wrapper, className)}>
      {validFiles?.map((file) => (
        <UploadItem
          key={`${file.name}-${file.size}`}
          file={file}
          multi={multi}
          isLoaded={isLoaded}
          isError={isError}
          onDelete={onDelete}
          onReload={onReload}
          onDownload={onDownload}
        />
      ))}

      {showInput && (
        <UploadInput
          name={name}
          acceptedFilesType={acceptFilesType}
          onChange={onChange}
          limit={limit}
        />
      )}
    </div>
  );
};

export default UploadItemField;
