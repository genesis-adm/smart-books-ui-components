import { Meta, Story } from '@storybook/react';
import {
  RulesAddButton as Component,
  RulesAddButtonProps as Props,
} from 'Components/custom/Rules/buttons/RulesAddButton';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Rules/Rules Add Button',
  component: Component,
  argTypes: {
    className: hideProperty,
    onClick: hideProperty,
  },
} as Meta;

const RulesAddButtonComponent: Story<Props> = (args) => <Component {...args} />;

export const RulesAddButton = RulesAddButtonComponent.bind({});
RulesAddButton.args = {
  name: 'Genesis',
};
