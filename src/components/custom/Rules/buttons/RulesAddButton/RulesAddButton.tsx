import { IconButton } from 'Components/base/buttons/IconButton';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import React from 'react';

export interface RulesAddButtonProps {
  name: string;
  className?: string;
  onClick(): void;
}

const RulesAddButton: React.FC<RulesAddButtonProps> = ({ name, className, onClick }) => (
  <IconButton
    size="48"
    background="violet-90-violet-100"
    color="white-100"
    tooltip={
      <>
        <Text display="block" color="white-100">
          Create a new rule for:
        </Text>
        <Text display="block" color="white-100">
          {name}
        </Text>
      </>
    }
    className={className}
    icon={<PlusIcon />}
    onClick={onClick}
  />
);

export default RulesAddButton;
