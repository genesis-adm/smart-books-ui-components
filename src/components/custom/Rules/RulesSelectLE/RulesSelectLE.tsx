import { DropDown } from 'Components/DropDown';
import { Icon } from 'Components/base/Icon';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as BriefcaseIcon } from 'Svg/v2/16/briefcase.svg';
import { ReactComponent as CaretDownIcon } from 'Svg/v2/16/caret-down.svg';
import classNames from 'classnames';
import React from 'react';

import style from './RulesSelectLE.module.scss';

export interface RulesSelectLEProps {
  name: string;
  className?: string;
  children?: React.ReactNode;
}

const RulesSelectLE: React.FC<RulesSelectLEProps> = ({ name, className, children }) => (
  <DropDown
    flexdirection="column"
    padding="none"
    control={({ handleOpen, isOpen }) => (
      <div
        role="presentation"
        className={classNames(style.component, { [style.active]: isOpen }, className)}
        onClick={handleOpen}
      >
        <Icon className={style.icon} icon={<BriefcaseIcon />} path="inherit" transition="unset" />
        <Text className={style.name} type="body-medium" color="inherit" transition="unset" noWrap>
          {name}
        </Text>
        <span className={style.divider} />
        <IconButton
          className={classNames(style.button, { [style.rotate]: isOpen })}
          size="40"
          icon={<CaretDownIcon />}
          onClick={handleOpen}
          background="inherit"
          color="inherit"
        />
      </div>
    )}
  >
    {children}
  </DropDown>
);

export default RulesSelectLE;
