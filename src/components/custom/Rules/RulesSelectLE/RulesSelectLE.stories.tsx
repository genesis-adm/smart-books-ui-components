import { Meta, Story } from '@storybook/react';
import {
  RulesSelectLE as Component,
  RulesSelectLEProps as Props,
} from 'Components/custom/Rules/RulesSelectLE';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Rules/Rules Select LE',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const RulesSelectLEComponent: Story<Props> = (args) => <Component {...args} />;

export const RulesSelectLE = RulesSelectLEComponent.bind({});
RulesSelectLE.args = {
  name: 'BetterMe Business Holdings Corp',
};
