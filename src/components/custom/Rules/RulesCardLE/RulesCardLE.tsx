import { useDropDown } from 'Components/DropDown';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React from 'react';

import style from './RulesCardLE.module.scss';

export interface RulesCardLEProps {
  name: string;
  rulesAmount: number;
  selected: boolean;
  className?: string;
  onClick(): void;
  onAddRule(): void;
}

const RulesCardLE: React.FC<RulesCardLEProps> = ({
  name,
  rulesAmount,
  selected,
  className,
  onClick,
  onAddRule,
}) => {
  const [handleOpen] = useDropDown();
  const handleClick = () => {
    onClick();
    if (handleOpen) handleOpen();
  };

  return (
    <div
      className={classNames(style.component, { [style.selected]: selected }, className)}
      onClick={handleClick}
      role="presentation"
    >
      <div className={style.logo}>
        <Text type="body-medium" color="white-100">
          {name.slice(0, 2).toUpperCase()}
        </Text>
      </div>
      <div className={style.information}>
        <Text type="body-medium" color="inherit" noWrap>
          {name}
        </Text>
        {rulesAmount ? (
          <Text type="body-regular-14" color="inherit">
            Rules:&nbsp;
            {rulesAmount}
          </Text>
        ) : (
          <div className={style.link}>
            <LinkButton onClick={onAddRule}>+ Add rule</LinkButton>
          </div>
        )}
      </div>
    </div>
  );
};

export default RulesCardLE;
