import { Meta, Story } from '@storybook/react';
import {
  RulesCardLE as Component,
  RulesCardLEProps as Props,
} from 'Components/custom/Rules/RulesCardLE';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Custom/Rules/Rules Card LE',
  component: Component,
  argTypes: {
    className: hideProperty,
    onClick: hideProperty,
    onAddRule: hideProperty,
  },
} as Meta;

const RulesCardLEComponent: Story<Props> = (args) => <Component {...args} />;

export const RulesCardLE = RulesCardLEComponent.bind({});
RulesCardLE.args = {
  name: 'BetterMe Business Holdings Corp',
  rulesAmount: 0,
  selected: false,
};
