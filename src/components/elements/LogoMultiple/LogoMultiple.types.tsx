export interface LogoMultipleProps {
  className?: string;
  items: string[];
  maxCount: number;
  size?: 'xs' | '24' | '32' | '44' | 'sm' | 'md' | 'lg' | 'xl';
  border?: 'white' | 'dark' | 'light' | 'none';
  radius?: 'default' | 'rounded';
  content: 'user' | 'company';
  color?: string | undefined;
  img?: string | null | undefined;
  marginLeft?: '5' | '9';
}
