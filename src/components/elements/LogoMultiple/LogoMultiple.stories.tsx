import { Meta, Story } from '@storybook/react';
import {
  LogoMultiple as Component,
  LogoMultipleProps as Props,
} from 'Components/elements/LogoMultiple';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/elements/LogoMultiple',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const LogoMultipleComponent: Story<Props> = (args) => <Component {...args} />;

export const LogoMultiple = LogoMultipleComponent.bind({});
LogoMultiple.args = {
  items: Array.from(Array(39)).map(() => 'Karine Mnatsakanian'),
  maxCount: 2,
  radius: 'rounded',
  content: 'user',
  size: '24',
  marginLeft: '5',
};
