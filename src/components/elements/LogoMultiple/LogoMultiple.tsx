import { Logo } from 'Components/elements/Logo';
import classNames from 'classnames';
import React, { ReactElement, useEffect, useState } from 'react';

import style from './LogoMultiple.module.scss';
import { LogoMultipleProps } from './LogoMultiple.types';

// Reminder: do not forget to add the following code to root index.ts
// export { LogoMultiple } from './components/elements/LogoMultiple';

const LogoMultiple = ({
  className,
  items,
  maxCount,
  color,
  img,
  radius,
  content,
  size,
  border,
  marginLeft,
}: LogoMultipleProps): ReactElement => {
  const [logosArr, setLogosArr] = useState<string[]>(items);
  useEffect(() => {
    if (items.length > maxCount) {
      const maxAmountOfLogos = items.slice(0, maxCount);
      setLogosArr(maxAmountOfLogos);
    }
  }, [items, maxCount]);

  return (
    <div className={classNames(style.component, className)}>
      {logosArr.map((item, index) => (
        <Logo
          key={index}
          name={item}
          radius={radius}
          content={content}
          color={color}
          img={img}
          size={size}
          border={border}
          className={classNames(style.item, style[`margin_left_${marginLeft}`])}
        />
      ))}
      {items.length > maxCount && (
        <Logo
          name=""
          text={`+${items.length - maxCount}`}
          radius={radius}
          content={content}
          color={color}
          img={img}
          size={size}
          border={border}
          className={classNames(style.item, style[`margin_left_${marginLeft}`])}
        />
      )}
    </div>
  );
};

export default LogoMultiple;
