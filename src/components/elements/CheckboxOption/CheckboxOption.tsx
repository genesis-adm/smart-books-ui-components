import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React, { SyntheticEvent } from 'react';

import style from './CheckboxOption.module.scss';

export interface CheckboxOptionProps {
  checked: boolean;
  label: string;
  secondaryLabel?: string;
  direction?: 'horizontal' | 'vertical';
  tertiaryLabel?: string;
  quadroLabel?: string;
  blurred?: boolean;
  className?: string;
  onClick?(e: SyntheticEvent): void;
}

const CheckboxOption: React.FC<CheckboxOptionProps> = ({
  checked,
  label,
  secondaryLabel,
  direction,
  tertiaryLabel,
  quadroLabel,
  blurred,
  className,
  onClick,
}) => (
  <div
    role="presentation"
    className={classNames(
      style.wrapper,
      {
        [style.checked]: checked,
        [style[`direction_${direction}`]]: direction && !(tertiaryLabel && quadroLabel),
        [style.blurred]: blurred,
      },
      className,
    )}
    onClick={onClick}
  >
    <div className={style.left}>
      <Text type="caption-semibold" className={style.main}>
        {label}
      </Text>
      {secondaryLabel && tertiaryLabel && quadroLabel && (
        <Text type="caption-regular" className={style.tertiary}>
          {tertiaryLabel}
        </Text>
      )}
      {secondaryLabel && tertiaryLabel && quadroLabel && (
        <Text type="caption-regular" className={style.quadro}>
          {quadroLabel}
        </Text>
      )}
    </div>
    {secondaryLabel && (
      <div className={style.right}>
        <Text type="caption-regular" color="grey-100" className={style.secondary}>
          {secondaryLabel}
        </Text>
      </div>
    )}
  </div>
);

export default CheckboxOption;
