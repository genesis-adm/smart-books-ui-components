import { Meta, Story } from '@storybook/react';
import { Checkbox } from 'Components/base/Checkbox';
import {
  CheckboxOption as Component,
  CheckboxOptionProps as Props,
} from 'Components/elements/CheckboxOption';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Elements/CheckboxOption',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const CheckboxOptionComponent: Story<Props> = (args) => (
  <Checkbox name="test" checked onChange={() => {}}>
    <Component {...args} />
  </Checkbox>
);

export const CheckboxOption = CheckboxOptionComponent.bind({});
CheckboxOption.args = {
  checked: true,
  label: 'MainLabel',
  secondaryLabel: 'SecLabel',
  tertiaryLabel: 'TerLabel',
  quadroLabel: 'QuadLabel',
};
