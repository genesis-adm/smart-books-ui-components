import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React from 'react';

import style from './PaginationButtonNew.module.scss';

export interface PaginationButtonNewProps {
  selected: boolean;
  value: string | number;
  background?: 'grey-10' | 'white-100';
  className?: string;
  onClick(): void;
}

const PaginationButtonNew: React.FC<PaginationButtonNewProps> = ({
  background = 'grey-10',
  selected,
  value,
  className,
  onClick,
}) => (
  <div
    role="presentation"
    onClick={onClick}
    className={classNames(style.button, className, style[`background_${background}`], {
      [style.selected]: selected,
    })}
  >
    <Text type="subtext-regular" color="inherit">
      {value}
    </Text>
  </div>
);

export default PaginationButtonNew;
