import { Tooltip } from 'Components/base/Tooltip';
import { IconButton } from 'Components/base/buttons/IconButton';
import { ReactComponent as ChevronLeftIcon } from 'Svg/v2/16/chevron-left.svg';
import { ReactComponent as ChevronRightIcon } from 'Svg/v2/16/chevron-right.svg';
import React from 'react';

export interface PaginationDirectionButtonProps {
  direction: 'left' | 'right';
  disabled?: boolean;
  onClick(): void;
  className?: string;
}

const PaginationDirectionButton: React.FC<PaginationDirectionButtonProps> = ({
  direction,
  disabled,
  onClick,
  className,
}) => {
  const icons = {
    left: <ChevronLeftIcon />,
    right: <ChevronRightIcon />,
  };
  const handleClick = () => {
    !disabled && onClick();
  };
  return (
    <Tooltip message={disabled && 'Disable'}>
      <IconButton
        className={className}
        size="24"
        icon={icons[direction]}
        background={'grey-10-grey-20'}
        color="grey-100"
        disabled={disabled}
        onClick={handleClick}
      />
    </Tooltip>
  );
};

export default PaginationDirectionButton;
