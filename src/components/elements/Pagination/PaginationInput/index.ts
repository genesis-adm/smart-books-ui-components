export { default as PaginationInput } from './PaginationInput';
export type { PaginationInputProps } from './PaginationInput.types';
