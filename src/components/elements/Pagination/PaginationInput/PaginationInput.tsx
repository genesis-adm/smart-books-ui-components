import InputErrorPopup from 'Components/base/inputs/InputErrorPopup/InputErrorPopup';
import InputWrapper from 'Components/base/inputs/InputWrapper/InputWrapper';
import InputPlaceholder from 'Components/base/inputs/NumericInput/components/InputPlaceholder';
import classNames from 'classnames';
import React, { ReactElement, useRef, useState } from 'react';
import { NumberFormatValues, NumericFormat, SourceInfo } from 'react-number-format';

import style from './PaginationInput.module.scss';
import { PaginationInputProps } from './PaginationInput.types';

// Reminder: do not forget to add the following code to root index.ts
// export { PaginationInput } from './components/elements/Pagination/PaginationInput';

const PaginationInput = ({
  background,
  error,
  className,
  onValueChange,
  onButtonClick,
}: PaginationInputProps): ReactElement => {
  const inputRef = useRef<HTMLInputElement>(null);

  const [currentValue, setCurrentValue] = useState<string>();
  const [isInputFocused, setInputFocused] = useState(false);
  const [isInputHovered, setInputHovered] = useState(false);

  const isError = !!error?.messages?.filter((message) => !!message)?.length;

  const handleValueChange = (values: NumberFormatValues, sourceInfo: SourceInfo) => {
    setCurrentValue(values?.value);
    onValueChange?.(
      {
        ...values,
        floatValue: values?.floatValue || 0,
      },
      sourceInfo,
    );
  };
  const handleButtonClick = (e: React.MouseEvent<HTMLButtonElement>) => {
    e.stopPropagation();
    e.preventDefault();
    if (onButtonClick && !isError) {
      onButtonClick();
      setInputFocused(false);
      setCurrentValue('');
    }
  };
  const handleClickWrapper = (): void => {
    if (isInputFocused) return;

    inputRef?.current?.focus();
    setInputFocused(true);
  };

  const handleBlurWrapper = (): void => {
    inputRef?.current?.blur();
    setInputFocused(false);
  };

  const handleHoverWrapper = (value: boolean): void => {
    setInputHovered(value);
  };

  return (
    <InputWrapper
      height="24"
      width="96"
      isError={isError}
      background={background}
      isFocused={isInputFocused}
      onClick={handleClickWrapper}
      onBlur={handleBlurWrapper}
      onHover={handleHoverWrapper}
      className={classNames(style.wrapper, className)}
    >
      {({ inputWrapperElement }) => (
        <>
          <NumericFormat
            value={currentValue}
            onValueChange={handleValueChange}
            className={classNames(style.input, { [style.error]: isError })}
            getInputRef={inputRef}
          />
          {!currentValue && (
            <InputPlaceholder className={style.placeholder}>Number</InputPlaceholder>
          )}
          <button
            className={classNames(style.button, {
              [style.active]: (currentValue && isInputFocused) || currentValue,
              [style.error]: isError,
            })}
            onClick={handleButtonClick}
          >
            Go
          </button>
          {isError && isInputHovered && isInputFocused && inputWrapperElement && (
            <InputErrorPopup
              inputWrapperElement={inputWrapperElement}
              errorMessage={error?.messages}
            />
          )}
        </>
      )}
    </InputWrapper>
  );
};

export default PaginationInput;
