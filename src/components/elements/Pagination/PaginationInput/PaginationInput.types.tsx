import { OwnNumberFormatValues } from 'Components/base/inputs/NumericInput/NumericInput.types';
import { SourceInfo } from 'react-number-format/types/types';

import { InputBackgroundType, InputError } from '../../../../types/inputs';

export interface PaginationInputProps {
  background?: InputBackgroundType;
  error?: InputError;
  onValueChange?: (values: OwnNumberFormatValues, sourceInfo: SourceInfo) => void;
  onButtonClick?(): void;
  className?: string;
}
