import { Meta, Story } from '@storybook/react';
import {
  PaginationInput as Component,
  PaginationInputProps as Props,
} from 'Components/elements/Pagination/PaginationInput';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Draft Components/PaginationInput',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const PaginationInputComponent: Story<Props> = (args) => <Component {...args} />;

export const PaginationInput = PaginationInputComponent.bind({});
PaginationInput.args = {
  error: { messages: ['Error'] },
};
