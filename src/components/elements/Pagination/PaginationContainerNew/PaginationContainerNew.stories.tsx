import { Meta, Story } from '@storybook/react';
import { Divider } from 'Components/base/Divider';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import PerPageSelect from 'Components/custom/Stories/Pagination/PerPageSelect/PerPageSelect';
import { PaginationButtonNew } from 'Components/elements/Pagination/PaginationButtonNew';
import {
  PaginationContainerNew as Component,
  PaginationContainerNewProps as Props,
} from 'Components/elements/Pagination/PaginationContainerNew';
import { PaginationDirectionButton } from 'Components/elements/Pagination/PaginationDirectionButton';
import { PaginationInput } from 'Components/elements/Pagination/PaginationInput';
import { rowsPerPageOptions } from 'Mocks/fakeOptions';
import React, { useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Elements/Pagination',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const PaginationContainerNewComponent: Story<Props> = (args) => {
  const [rowsPerPage, setRowsPerPage] = useState<string>(rowsPerPageOptions[0].value);
  return (
    <Component
      {...args}
      customPagination={
        <Container gap="16" alignitems="center">
          <Text type="subtext-regular" color="grey-100">
            Rows per Page
          </Text>

          <PerPageSelect
            selectedValue={rowsPerPage}
            onOptionClick={(value) => {
              setRowsPerPage(value);
            }}
          />
        </Container>
      }
    >
      <PaginationDirectionButton
        direction="left"
        onClick={() => {}}
        disabled
        className={spacing.mr8}
      />
      <PaginationButtonNew value={1} selected onClick={() => {}} />
      <PaginationButtonNew value={2} selected={false} onClick={() => {}} />
      <PaginationButtonNew value={3} selected={false} onClick={() => {}} />
      <PaginationButtonNew value={4} selected={false} onClick={() => {}} />
      <PaginationButtonNew value="..." selected={false} onClick={() => {}} />
      <PaginationButtonNew value={100} selected={false} onClick={() => {}} />
      <PaginationDirectionButton direction="right" onClick={() => {}} className={spacing.ml8} />
      <Divider height="20" type="vertical" className={spacing.mX16} />
      <Container alignitems="center" gap="8">
        <Text type="subtext-regular" color="grey-100">
          Go to page
        </Text>
        <PaginationInput />
      </Container>
    </Component>
  );
};

export const Pagination = PaginationContainerNewComponent.bind({});
Pagination.args = {
  quantityText: 'Showing 1-15 of 659,813 items',
};
