import { ContentLoader } from 'Components/base/ContentLoader';
import { Divider } from 'Components/base/Divider';
import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React, { ReactNode } from 'react';

import style from './PaginationContainerNew.module.scss';

export interface PaginationContainerNewProps {
  quantityText?: string;
  borderTop?: 'default' | 'none';
  padding?: '0' | '16';
  action?: ReactNode;
  customPagination?: ReactNode;
  isLoading?: boolean;
  className?: string;
  children?: React.ReactNode;
}

const PaginationContainerNew: React.FC<PaginationContainerNewProps> = ({
  quantityText,
  borderTop = 'default',
  padding = '16',
  action,
  customPagination,
  isLoading,
  className,
  children,
}) => (
  <div
    className={classNames(
      style.container,
      style[`borderTop_${borderTop}`],
      style[`padding_${padding}`],
      className,
    )}
  >
    <ContentLoader width="480px" height="40px" isLoading={!quantityText || !!isLoading}>
      <div className={style.pagination}>
        {customPagination}
        {customPagination && <Divider height="20" type="vertical" className={style.divider} />}
        {quantityText && (
          <Text type="subtext-regular" className={style.text}>
            {quantityText}
          </Text>
        )}
        <div className={style.buttons}>{children}</div>
      </div>

      {action}
    </ContentLoader>
  </div>
);

export default PaginationContainerNew;
