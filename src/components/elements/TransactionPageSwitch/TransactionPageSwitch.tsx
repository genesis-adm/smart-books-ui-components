import { IconButton } from 'Components/base/buttons/IconButton';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as LeftIcon } from 'Svg/12/chevron-left.svg';
import { ReactComponent as RightIcon } from 'Svg/12/chevron-right.svg';
import classNames from 'classnames';
import React from 'react';

import style from './TransactionPageSwitch.module.scss';

export interface TransactionPageSwitchProps {
  page: number;
  totalPages: number;
  isViewMode?: boolean;
  className?: string;
  bottom?: 'none' | '72';
  onPrevPage(): void;
  onNextPage(): void;
}

const TransactionPageSwitch: React.FC<TransactionPageSwitchProps> = ({
  page,
  totalPages,
  isViewMode,
  className,
  bottom = '72',
  onPrevPage,
  onNextPage,
}) => (
  <div className={classNames(style.component, style[`bottom_${bottom}`], className)}>
    {isViewMode && (
      <>
        <Text type="body-regular-14" color="inherit">
          View Mode
        </Text>
        <Text type="body-regular-14" color="inherit">
          |
        </Text>
      </>
    )}
    <IconButton
      size="24"
      icon={<LeftIcon />}
      onClick={onPrevPage}
      background="transparent"
      color="inherit"
    />
    <Text className={style.text} type="body-regular-14" color="inherit">
      {page}
      &nbsp;/&nbsp;
      {totalPages}
    </Text>
    <IconButton
      size="24"
      icon={<RightIcon />}
      onClick={onNextPage}
      background="transparent"
      color="inherit"
    />
  </div>
);

export default TransactionPageSwitch;
