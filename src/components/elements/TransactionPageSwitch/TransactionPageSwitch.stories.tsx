import { Meta, Story } from '@storybook/react';
import {
  TransactionPageSwitch as Component,
  TransactionPageSwitchProps as Props,
} from 'Components/elements/TransactionPageSwitch';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Elements/Transaction Page Switch',
  component: Component,
  argTypes: {
    className: hideProperty,
    onClickLeft: hideProperty,
    onClickRight: hideProperty,
  },
} as Meta;

const TransactionPageSwitchComponent: Story<Props> = (args) => <Component {...args} />;

export const TransactionPageSwitch = TransactionPageSwitchComponent.bind({});
TransactionPageSwitch.args = {
  page: 1,
  totalPages: 28,
};
