import { Meta, Story } from '@storybook/react';
import {
  LegalEntityItem as Component,
  LegalEntityItemProps as Props,
} from 'Components/elements/LegalEntityItem';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Elements/Legal Entity Item',
  component: Component,
  argTypes: {
    className: hideProperty,
    onClick: hideProperty,
  },
} as Meta;

const LegalEntityItemComponent: Story<Props> = (args) => <Component {...args} />;

export const LegalEntityItem = LegalEntityItemComponent.bind({});
LegalEntityItem.args = {
  name: 'Legal entity name',
  address: 'Legal entity full address',
};
