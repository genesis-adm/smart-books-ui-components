import { Text } from 'Components/base/typography/Text';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import classNames from 'classnames';
import React from 'react';

import style from './LegalEntityItem.module.scss';

export interface LegalEntityItemProps {
  name: string;
  address: string;
  className?: string;
  onClick(): void;
}

const LegalEntityItem: React.FC<LegalEntityItemProps> = ({ name, address, className, onClick }) => (
  <div className={classNames(style.component, className)}>
    <div className={style.container}>
      <Text type="body-regular-14" noWrap>
        {name}
      </Text>
    </div>
    <div className={style.container}>
      <Text type="body-regular-14" color="grey-100" noWrap>
        {address}
      </Text>
    </div>
    <div className={style.action} role="presentation" onClick={onClick}>
      <TrashIcon />
    </div>
  </div>
);

export default LegalEntityItem;
