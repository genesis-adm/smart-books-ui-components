import { Checkbox } from 'Components/base/Checkbox';
import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React from 'react';

import style from './BusinessDivisionSelect.module.scss';

export interface BusinessDivisionSelectProps {
  checked: boolean;
  division: string;
  company: string;
  className?: string;
  onChange(): void;
}

const BusinessDivisionSelect: React.FC<BusinessDivisionSelectProps> = ({
  checked,
  division,
  company,
  className,
  onChange,
}) => (
  <div className={classNames(style.component, className)}>
    <Checkbox
      size="large"
      className={style.checkbox}
      name={division.toLowerCase()}
      checked={checked}
      onChange={onChange}
    />
    <div className={style.container}>
      <Text type="body-regular-14" noWrap>
        {division}
      </Text>
      <Text type="caption-regular" color="grey-100" noWrap>
        {company}
      </Text>
    </div>
  </div>
);

export default BusinessDivisionSelect;
