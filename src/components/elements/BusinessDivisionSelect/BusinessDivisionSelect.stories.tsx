import { Meta, Story } from '@storybook/react';
import {
  BusinessDivisionSelect as Component,
  BusinessDivisionSelectProps as Props,
} from 'Components/elements/BusinessDivisionSelect';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Elements/Business Division Select',
  component: Component,
  argTypes: {
    className: hideProperty,
    onChange: hideProperty,
  },
} as Meta;

const BusinessDivisionSelectComponent: Story<Props> = (args) => <Component {...args} />;

export const BusinessDivisionSelect = BusinessDivisionSelectComponent.bind({});
BusinessDivisionSelect.args = {
  division: 'Genesis Media & Mobile Application',
  company: 'Company in GENESIS',
  checked: true,
};
