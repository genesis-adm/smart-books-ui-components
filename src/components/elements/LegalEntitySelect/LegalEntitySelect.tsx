import { Checkbox } from 'Components/base/Checkbox';
import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React from 'react';

import style from './LegalEntitySelect.module.scss';

export interface LegalEntitySelectProps {
  checked: boolean;
  name: string;
  address: string;
  className?: string;
  onChange(): void;
}

const LegalEntitySelect: React.FC<LegalEntitySelectProps> = ({
  checked,
  name,
  address,
  className,
  onChange,
}) => (
  <div className={classNames(style.component, className)}>
    <Checkbox
      size="large"
      className={style.checkbox}
      name={name.toLowerCase()}
      checked={checked}
      onChange={onChange}
    />
    <div className={style.container}>
      <Text type="body-regular-14" noWrap>
        {name}
      </Text>
    </div>
    <div className={style.container}>
      <Text type="body-regular-14" color="grey-100" noWrap>
        {address}
      </Text>
    </div>
  </div>
);

export default LegalEntitySelect;
