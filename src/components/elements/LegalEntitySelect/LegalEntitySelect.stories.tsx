import { Meta, Story } from '@storybook/react';
import {
  LegalEntitySelect as Component,
  LegalEntitySelectProps as Props,
} from 'Components/elements/LegalEntitySelect';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Elements/Legal Entity Select',
  component: Component,
  argTypes: {
    className: hideProperty,
    onChange: hideProperty,
  },
} as Meta;

const LegalEntitySelectComponent: Story<Props> = (args) => <Component {...args} />;

export const LegalEntitySelect = LegalEntitySelectComponent.bind({});
LegalEntitySelect.args = {
  checked: true,
  name: 'Legal entity name',
  address: 'Legal entity full address',
};
