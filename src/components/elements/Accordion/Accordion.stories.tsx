import { Meta, Story } from '@storybook/react';
import { Accordion as Component, AccordionProps as Props } from 'Components/elements/Accordion';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Elements/Accordion',
  component: Component,
  argTypes: {
    className: hideProperty,
    onClick: hideProperty,
  },
} as Meta;

const AccordionComponent: Story<Props> = (args) => <Component {...args} />;

export const Accordion = AccordionComponent.bind({});
Accordion.args = {
  title: 'Accordion test title',
};
