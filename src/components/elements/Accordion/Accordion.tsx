import { Icon } from 'Components/base/Icon';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as ChevronIcon } from 'Svg/v2/16/chevron-right.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import style from './Accordion.module.scss';

export interface AccordionProps {
  title: string;
  className?: string;
  onClick(): void;
  children?: React.ReactNode;
}

const Accordion: React.FC<AccordionProps> = ({ title, className, onClick, children }) => {
  const [open, setOpen] = useState(false);
  const handleClick = () => {
    onClick();
    setOpen((prevState) => !prevState);
  };

  return (
    <div className={classNames(style.wrapper, className)}>
      <span className={style.title} onClick={handleClick}>
        <Icon
          className={classNames(style.icon, { [style.active]: open })}
          icon={<ChevronIcon />}
          path="inherit"
        />
        <Text className={style.text} type="body-medium">
          {title}
        </Text>
      </span>
      <div
        // hidden={!open}
        className={classNames(style.content, { [style.visible]: open })}
      >
        {children}
      </div>
    </div>
  );
};

export default Accordion;
