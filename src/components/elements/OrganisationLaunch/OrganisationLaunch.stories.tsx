import { Meta, Story } from '@storybook/react';
import {
  OrganisationLaunch as Component,
  OrganisationLaunchProps as Props,
} from 'Components/elements/OrganisationLaunch';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Elements/Organisation Launch',
  component: Component,
  argTypes: {
    className: hideProperty,
    onLaunch: hideProperty,
  },
} as Meta;

const OrganisationLaunchComponent: Story<Props> = (args) => <Component {...args} />;

export const OrganisationLaunch = OrganisationLaunchComponent.bind({});
OrganisationLaunch.args = {
  name: 'Genesis',
  type: 'Organisation',
  background: 'white-100',
  logo: 'https://picsum.photos/60',
  color: '#7AC72D',
};
