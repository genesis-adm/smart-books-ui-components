import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Text } from 'Components/base/typography/Text';
import { Logo } from 'Components/elements/Logo';
import { ReactComponent as ArrowRight } from 'Svg/v2/16/arrow-right.svg';
import classNames from 'classnames';
import React from 'react';

import style from './OrganisationLaunch.module.scss';

export interface OrganisationLaunchProps {
  name: string;
  type: string;
  logo?: string;
  color?: string;
  background?: 'white-100' | 'grey-10';
  className?: string;
  onLaunch(): void;
}

const OrganisationLaunch: React.FC<OrganisationLaunchProps> = ({
  name,
  type,
  logo,
  color,
  background = 'grey-10',
  onLaunch,
  className,
}) => (
  <div className={classNames(style.component, className)}>
    <Logo className={style.logo} content="company" size="md" name={name} img={logo} color={color} />
    <div
      className={classNames(style.card, {
        [style[`card_${background}`]]: background,
      })}
    >
      <div className={style.text}>
        <Text className={style.text_name} type="body-medium" transform="capitalize">
          {name}
        </Text>
        <Text
          className={style.text_type}
          type="body-medium"
          transform="capitalize"
          color="grey-100"
        >
          &nbsp;/&nbsp;
          {type}
        </Text>
      </div>
      <LinkButton color="black-100-violet-90" icon={<ArrowRight />} iconRight onClick={onLaunch}>
        To launch
      </LinkButton>
    </div>
  </div>
);

export default OrganisationLaunch;
