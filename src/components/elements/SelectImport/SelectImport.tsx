import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { Icon } from 'Components/base/Icon';
import { Tooltip } from 'Components/base/Tooltip';
import { SelectNew } from 'Components/base/inputs/SelectNew';
import { Option } from 'Components/base/inputs/SelectNew/Select.types';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as ArrowLeftIcon } from 'Svg/v2/16/arrow-left.svg';
import { ReactComponent as InfoIcon } from 'Svg/v2/16/info.svg';
import classNames from 'classnames';
import React, { ReactElement } from 'react';

import { InputBackgroundType } from '../../../types/inputs';
import style from './SelectImport.module.scss';

export interface SelectImportProps {
  name: string;
  label: string;
  placeholder: string;
  defaultValue?: Option;
  value?: Option;
  required?: boolean;
  background?: InputBackgroundType;
  tooltip?: string;
  textWidth?: '130' | '160' | '260';
  className?: string;
  disabled?: boolean;

  onChange?(option: Option): void;

  children(args: {
    onClick(option: Option): void;
    onClickMultiple(option: Option): void;
    state: Option | null;
    searchValue: string;
  }): ReactElement;
}

const SelectImport: React.FC<SelectImportProps> = ({
  name,
  label,
  placeholder,
  defaultValue,
  value,
  required,
  background = 'transparent',
  tooltip,
  textWidth = '130',
  className,
  onChange,
  children,
  disabled,
}) => (
  <div className={classNames(style.wrapper, { [style.disabled]: disabled }, className)}>
    <div className={classNames(style.text)}>
      <Text
        type="body-regular-14"
        color="grey-100"
        className={classNames(style.name, style[`textWidth_${textWidth}`])}
        noWrap
      >
        {tooltip && (
          <Tooltip cursor="pointer" wrapperClassName={style.tooltip} message={tooltip}>
            <Icon icon={<InfoIcon />} path="inherit" />
          </Tooltip>
        )}
        {name}
        {required && (
          <Text type="inherit" color="red-90">
            *
          </Text>
        )}
      </Text>
      <ArrowLeftIcon />
    </div>

    <SelectNew
      name={name}
      value={value}
      defaultValue={defaultValue}
      onChange={onChange}
      background={background}
      placeholder={placeholder}
      label={label}
      disabled={disabled}
    >
      {({ onClick, onClickMultiple, state, searchValue, selectElement }) => (
        <SelectDropdown selectElement={selectElement}>
          <SelectDropdownBody>
            <SelectDropdownItemList>
              {children({
                onClick,
                onClickMultiple,
                state,
                searchValue,
              })}
            </SelectDropdownItemList>
          </SelectDropdownBody>
        </SelectDropdown>
      )}
    </SelectNew>
  </div>
);

export default SelectImport;
