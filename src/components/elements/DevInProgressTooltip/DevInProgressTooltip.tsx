import { Icon } from 'Components/base/Icon';
import { Tooltip } from 'Components/base/Tooltip';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as StopIcon } from 'Svg/v2/16/stop.svg';
import React from 'react';

export interface DevInProgressTooltipProps {
  className?: string;
  children: React.ReactNode;
  disabled?: boolean;
}

const DevInProgressTooltip: React.FC<DevInProgressTooltipProps> = ({
  className,
  children,
  disabled,
}) => {
  if (disabled) return <>{children}</>;

  return (
    <Tooltip
      message={
        <Container gap="8" alignitems="center">
          <Icon icon={<StopIcon />} path="white-100" />
          <Text type="body-regular-14" color="white-100">
            Development in progress
          </Text>
        </Container>
      }
      wrapperClassName={className}
    >
      {children}
    </Tooltip>
  );
};

export default DevInProgressTooltip;
