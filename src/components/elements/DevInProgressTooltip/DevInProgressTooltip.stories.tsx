import { Meta, Story } from '@storybook/react';
import {
  DevInProgressTooltip as Component,
  DevInProgressTooltipProps as Props,
} from 'Components/elements/DevInProgressTooltip';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Elements/DevInProgressTooltip',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const DevInProgressTooltipComponent: Story<Props> = (args) => (
  <Component {...args}>Children</Component>
);

export const DevInProgressTooltip = DevInProgressTooltipComponent.bind({});
DevInProgressTooltip.args = {};
