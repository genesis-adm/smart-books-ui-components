import { Logo } from 'Components/elements/Logo';
import classNames from 'classnames';
import React from 'react';

import style from './LogoDouble.module.scss';

export interface LogoDoubleProps {
  organizationName: string;
  organizationImg?: string;
  organizationColor?: string;
  businessDivisionName: string;
  businessDivisionImg?: string;
  businessDivisionColor?: string;
  className?: string;
}

const LogoDouble: React.FC<LogoDoubleProps> = ({
  organizationName,
  organizationImg,
  organizationColor,
  businessDivisionName,
  businessDivisionImg,
  businessDivisionColor,
  className,
}) => (
  <div className={classNames(style.component, className)}>
    <Logo
      className={classNames(style.logo, style.logo_1)}
      border="light"
      content="company"
      size="md"
      name={organizationName}
      img={organizationImg}
      color={organizationColor}
    />
    <Logo
      className={classNames(style.logo, style.logo_2)}
      border="light"
      content="company"
      size="md"
      name={businessDivisionName}
      img={businessDivisionImg}
      color={businessDivisionColor}
    />
  </div>
);

export default LogoDouble;
