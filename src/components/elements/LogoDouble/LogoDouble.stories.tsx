import { Meta, Story } from '@storybook/react';
import { LogoDouble as Component, LogoDoubleProps as Props } from 'Components/elements/LogoDouble';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Elements/Logo Double',
  component: Component,
  argTypes: {
    className: hideProperty,
    organizationImg: hideProperty,
    businessDivisionImg: hideProperty,
  },
} as Meta;

const LogoDoubleComponent: Story<Props> = (args) => <Component {...args} />;

export const LogoDouble = LogoDoubleComponent.bind({});
LogoDouble.args = {
  organizationName: 'Genesis',
  organizationColor: '#EEB509',
  businessDivisionName: 'SmartBooks',
  businessDivisionColor: '#7540EE',
};
