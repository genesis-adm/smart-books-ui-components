import { Meta, Story } from '@storybook/react';
import {
  ExportDropdown as Component,
  ExportDropdownProps as Props,
} from 'Components/elements/ExportDropdown';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Elements/ExportDropdown',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const ExportDropdownComponent: Story<Props> = (args) => <Component {...args} />;

export const ExportDropdown = ExportDropdownComponent.bind({});
ExportDropdown.args = {};
