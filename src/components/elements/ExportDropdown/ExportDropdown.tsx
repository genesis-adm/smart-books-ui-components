import { DropDown } from 'Components/DropDown';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { DevInProgressTooltip } from 'Components/elements/DevInProgressTooltip';
import { ReactComponent as DownloadIcon } from 'Svg/v2/16/download.svg';
import { ReactComponent as CSVIcon } from 'Svg/v2/16/file-csv.svg';
import { ReactComponent as XLSIcon } from 'Svg/v2/16/file-xls.svg';
import React from 'react';

export interface ExportDropdownProps {
  className?: string;
  disabled?: boolean;
  enablePropagation?: boolean;
  onExportToXLS(): void;
  onExportToCSV(): void;
}

const ExportDropdown: React.FC<ExportDropdownProps> = ({
  className,
  disabled,
  enablePropagation,
  onExportToXLS,
  onExportToCSV,
}) => (
  <DropDown
    classNameOuter={className}
    flexdirection="column"
    padding="8"
    control={({ isOpen, handleOpen }) => (
      <IconButton
        size="40"
        background="grey-10-grey-20"
        color="grey-100"
        icon={<DownloadIcon />}
        tooltip={isOpen ? '' : 'Export files'}
        onClick={() => !disabled && handleOpen()}
        disabled={disabled}
        enablePropagation={enablePropagation}
      />
    )}
  >
    <DevInProgressTooltip>
      <DropDownButton size="160" icon={<XLSIcon />} onClick={onExportToXLS}>
        Export to XLS
      </DropDownButton>
    </DevInProgressTooltip>
    <DropDownButton size="160" icon={<CSVIcon />} onClick={onExportToCSV}>
      Export to CSV
    </DropDownButton>
  </DropDown>
);

export default ExportDropdown;
