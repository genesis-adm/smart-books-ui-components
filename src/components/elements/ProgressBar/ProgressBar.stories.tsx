import { Meta, Story } from '@storybook/react';
import {
  ProgressBar as Component,
  ProgressBarProps as Props,
} from 'Components/elements/ProgressBar';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Elements/Progress Bar',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const ProgressBarComponent: Story<Props> = (args) => <Component {...args} />;

export const ProgressBar = ProgressBarComponent.bind({});
ProgressBar.args = {
  align: 'left',
  steps: 3,
  activeStep: 1,
};
