import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React from 'react';

import style from './ProgressBar.module.scss';

export interface ProgressBarProps {
  steps: number;
  activeStep: number;
  align?: 'left' | 'center' | 'right';
  className?: string;
}

const ProgressBar: React.FC<ProgressBarProps> = ({
  steps,
  activeStep,
  align = 'left',
  className,
}) => {
  let currentStep: number;
  if (activeStep > steps) {
    currentStep = steps;
  } else if (activeStep < 1) {
    currentStep = 1;
  } else {
    currentStep = activeStep;
  }
  return (
    <div className={classNames(style.progress_bar, style[align], className)}>
      <Text className={style.text} type="body-regular-14" color="violet-100">
        Step&nbsp;
        {currentStep}
      </Text>
      {Array.from(Array(steps)).map((_, index) => (
        <span
          key={index}
          className={classNames(style.item, { [style.active]: index < currentStep })}
        />
      ))}
    </div>
  );
};

export default ProgressBar;
