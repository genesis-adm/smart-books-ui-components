import { DropDown } from 'Components/DropDown';
import { Icon } from 'Components/base/Icon';
import { Tooltip } from 'Components/base/Tooltip';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import handleAccountLength from 'Utils/handleAccountLength';
import handleStringLength from 'Utils/handleStringLength';
import classNames from 'classnames';
import React, { ReactElement, ReactNode } from 'react';

import style from './AccountsList.module.scss';
import { defaultIconPreset, paymentEntitiesIconPreset } from './AccountsListIconPreset';
import { ItemType, PaymentEntitiesType } from './AccountsListItem/AccountsListItem.types';

export interface AccountsListProps {
  type: ItemType;
  paymentEntity?: PaymentEntitiesType;
  name: string;
  accountNumber?: string;
  currency: string;
  accountListLength?: number;
  className?: string;
  children?: ReactNode;
}

const AccountsList = ({
  type,
  paymentEntity,
  name,
  accountNumber = '',
  currency,
  accountListLength = 1,
  className,
  children,
}: AccountsListProps): ReactElement => {
  const paymentEntityIcon = paymentEntity && paymentEntitiesIconPreset[paymentEntity];

  const accountInformation =
    type === 'cash'
      ? `${handleStringLength(name, 16)} | ${currency.toUpperCase()}`
      : `${handleStringLength(name, 16)} | ${handleAccountLength(
          accountNumber,
        )} | ${currency.toUpperCase()}`;

  if (accountListLength < 1) {
    return <Text color="grey-100">None</Text>;
  }

  if (accountListLength === 1) {
    return (
      <Tooltip
        message={
          type !== 'cash' && handleAccountLength(accountNumber).includes('******') ? (
            <Container flexdirection="column">
              <Text color="white-100">{name}</Text>
              <Text color="white-100">
                {accountNumber} | {currency.toUpperCase()}
              </Text>
            </Container>
          ) : (
            ''
          )
        }
        wrapperClassName={classNames(style.wrapper, style.single, className)}
      >
        <Icon
          icon={paymentEntityIcon ?? defaultIconPreset[type]}
          clickThrough
          staticColor={!!paymentEntityIcon}
          path={!paymentEntityIcon ? 'inherit' : undefined}
        />
        <Text color="inherit">{accountInformation}</Text>
      </Tooltip>
    );
  }

  return (
    <DropDown
      flexdirection="column"
      padding="16"
      control={({ isOpen, handleOpen }) => (
        <Tooltip
          message={isOpen ? '' : 'Show all'}
          onClick={handleOpen}
          wrapperClassName={classNames(
            style.wrapper,
            style.multiple,
            { [style.active]: isOpen },
            className,
          )}
        >
          <Icon
            icon={paymentEntityIcon ?? defaultIconPreset[type]}
            clickThrough
            staticColor={!!paymentEntityIcon}
            path={!paymentEntityIcon ? 'inherit' : undefined}
            transition="inherit"
          />
          <Text color="inherit">{accountInformation}</Text>
          <span className={style.divider} />
          <Text color="inherit">+{accountListLength - 1}</Text>
        </Tooltip>
      )}
    >
      <ul className={style.list}>{children}</ul>
    </DropDown>
  );
};

export default AccountsList;
