import { Meta, Story } from '@storybook/react';
import { Text } from 'Components/base/typography/Text';
import {
  AccountsListItem,
  AccountsList as Component,
  AccountsListProps as Props,
} from 'Components/elements/AccountsList';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Elements/AccountsList',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const AccountsListComponent: Story<Props> = (args) => <Component {...args} />;

export const AccountsList = AccountsListComponent.bind({});
AccountsList.args = {
  type: 'cash',
  // paymentEntity: 'mastercard',
  name: 'Monobank',
  accountNumber: '4141141432322323',
  currency: 'uah',
  accountListLength: 1,
  children: (
    <>
      <Text type="body-medium" color="grey-100">
        Credit card
      </Text>
      {Array.from(Array(3)).map((_, index) => (
        <AccountsListItem
          key={index}
          type="creditCard"
          name="Monobank"
          accountNumber="4141141432322323"
          currency="uah"
          // paymentEntity="mastercard"
        />
      ))}
    </>
  ),
};
