import { ReactComponent as BankIcon } from 'Svg/v2/24/bank.svg';
import { ReactComponent as CashIcon } from 'Svg/v2/24/cash.svg';
import { ReactComponent as CreditCardIcon } from 'Svg/v2/24/credit-card.svg';
import { ReactComponent as AfterpayIcon } from 'Svg/v2/24/logo-afterpay.svg';
import { ReactComponent as AmericanExpressIcon } from 'Svg/v2/24/logo-amex.svg';
import { ReactComponent as MastercardIcon } from 'Svg/v2/24/logo-mastercard.svg';
import { ReactComponent as VisaIcon } from 'Svg/v2/24/logo-visa.svg';
import { ReactComponent as WalletIcon } from 'Svg/v2/24/wallet.svg';
import React from 'react';

import { ItemType, PaymentEntitiesType } from './AccountsListItem.types';

export const defaultIconPreset: Record<ItemType, JSX.Element> = {
  creditCard: <CreditCardIcon />,
  bankAccount: <BankIcon />,
  wallet: <WalletIcon />,
  cash: <CashIcon />,
};

export const paymentEntitiesIconPreset: Record<PaymentEntitiesType, JSX.Element> = {
  visa: <VisaIcon />,
  mastercard: <MastercardIcon />,
  amex: <AmericanExpressIcon />,
  afterpay: <AfterpayIcon />,
};
