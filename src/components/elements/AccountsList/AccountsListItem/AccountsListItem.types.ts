export type ItemType = 'creditCard' | 'bankAccount' | 'wallet' | 'cash';

type CardsEntitiesList = 'visa' | 'mastercard' | 'amex';
type WalletsEntitiesList = 'afterpay';
export type PaymentEntitiesType = CardsEntitiesList | WalletsEntitiesList | string;

export interface AccountsListItemProps {
  type: ItemType;
  paymentEntity?: PaymentEntitiesType;
  name: string;
  accountNumber?: string;
  currency: string;
  isDefault?: boolean;
}
