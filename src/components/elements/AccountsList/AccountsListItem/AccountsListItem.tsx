import { Icon } from 'Components/base/Icon';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as StarIcon } from 'Svg/v2/16/star-filled.svg';
import React, { ReactElement } from 'react';

import style from './AccountsListItem.module.scss';
import { AccountsListItemProps } from './AccountsListItem.types';
import { defaultIconPreset, paymentEntitiesIconPreset } from './AccountsListItemIconPreset';
import accountListItemAccountHandler from './accountListItemAccountHandler';

const AccountsListItem = ({
  type,
  paymentEntity,
  name,
  accountNumber,
  currency,
  isDefault,
}: AccountsListItemProps): ReactElement => {
  const paymentEntityIcon = paymentEntity && paymentEntitiesIconPreset[paymentEntity];

  return (
    <div className={style.component}>
      <div className={style.wrapper}>
        <Icon
          className={style.icon}
          icon={paymentEntityIcon ?? defaultIconPreset[type]}
          clickThrough
          staticColor={!!paymentEntityIcon}
          path={!paymentEntityIcon ? 'grey-100' : undefined}
        />
        <div className={style.text}>
          <Text type="body-regular-14">{name}</Text>
          <Text type="body-regular-14" color="grey-100">
            {accountNumber &&
              type !== 'cash' &&
              `${accountListItemAccountHandler(accountNumber, type)} | `}
            {currency.toUpperCase()}
          </Text>
        </div>
      </div>
      {isDefault && (
        <div className={style.default}>
          <Icon className={style.icon} icon={<StarIcon />} path="inherit" clickThrough />
          <Text type="caption-regular" color="inherit">
            Default
          </Text>
        </div>
      )}
    </div>
  );
};

export default AccountsListItem;
