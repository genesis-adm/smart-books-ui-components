import { Meta, Story } from '@storybook/react';
import {
  AccountsListItem as Component,
  AccountsListItemProps as Props,
} from 'Components/elements/AccountsList/AccountsListItem';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Elements/AccountsListItem',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const AccountsListItemComponent: Story<Props> = (args) => <Component {...args} />;

export const AccountsListItem = AccountsListItemComponent.bind({});
AccountsListItem.args = {
  type: 'creditCard',
  paymentEntity: 'visa',
  name: 'MonoBank',
  accountNumber: '4411441144112233',
  currency: 'uah',
  isDefault: true,
};
