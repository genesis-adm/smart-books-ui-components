import { Meta, Story } from '@storybook/react';
import { UserSelect as Component, UserSelectProps as Props } from 'Components/elements/UserSelect';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Elements/User Select',
  component: Component,
  argTypes: {
    className: hideProperty,
    onChange: hideProperty,
    onLinkClick: hideProperty,
  },
} as Meta;

const UserSelectComponent: Story<Props> = (args) => <Component {...args} />;

export const UserSelect = UserSelectComponent.bind({});
UserSelect.args = {
  checked: true,
  name: 'User name',
  organization: 'Organization name',
};
