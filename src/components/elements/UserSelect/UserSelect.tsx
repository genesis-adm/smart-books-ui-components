import { Checkbox } from 'Components/base/Checkbox';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as LinkIcon } from 'Svg/v2/16/link.svg';
import classNames from 'classnames';
import React from 'react';

import style from './UserSelect.module.scss';

export interface UserSelectProps {
  checked: boolean;
  name: string;
  organization: string;
  className?: string;
  onChange(): void;
  onLinkClick(): void;
}

const UserSelect: React.FC<UserSelectProps> = ({
  checked,
  name,
  organization,
  className,
  onChange,
  onLinkClick,
}) => (
  <div className={classNames(style.component, className)}>
    <Checkbox
      size="large"
      className={style.checkbox}
      name={name.toLowerCase()}
      checked={checked}
      onChange={onChange}
    />
    <div className={style.container}>
      <Text type="body-regular-14" noWrap>
        {name}
      </Text>
      <LinkIcon onClick={onLinkClick} />
    </div>
    <div className={style.container}>
      <Text type="body-regular-14" color="grey-100" noWrap>
        {organization}
      </Text>
    </div>
  </div>
);

export default UserSelect;
