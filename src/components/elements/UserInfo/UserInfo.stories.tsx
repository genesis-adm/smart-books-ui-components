import { Meta, Story } from '@storybook/react';
import { UserInfo as Component, UserInfoProps as Props } from 'Components/elements/UserInfo';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Elements/User Info',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const UserInfoComponent: Story<Props> = (args) => <Component {...args} />;

export const UserInfo = UserInfoComponent.bind({});
UserInfo.args = {
  name: 'Artem Holba',
  isLogo: true,
};
