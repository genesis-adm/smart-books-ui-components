import { Tooltip } from 'Components/base/Tooltip';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { Logo } from 'Components/elements/Logo';
import classNames from 'classnames';
import React, { useEffect, useRef, useState } from 'react';

import style from './UserInfo.module.scss';
import { UserInfoProps } from './UserInfo.types';

const UserInfo: React.FC<UserInfoProps> = ({
  name,
  isLogo,
  secondaryText,
  secondaryTooltip,
  className,
  tooltip,
}) => {
  const textPrimaryRef = useRef<HTMLDivElement>(null);
  const textSecondaryRef = useRef<HTMLDivElement>(null);

  const [tooltipValue, setTooltipValue] = useState<React.ReactNode>(tooltip);
  const [secondaryTooltipValue, setSecondaryTooltipValue] =
    useState<React.ReactNode>(secondaryTooltip);

  useEffect(() => {
    if (
      !tooltip &&
      textPrimaryRef.current &&
      (textPrimaryRef.current.scrollHeight > textPrimaryRef.current.clientHeight ||
        textPrimaryRef.current.scrollWidth > textPrimaryRef.current.clientWidth)
    ) {
      setTooltipValue(name);
    }
  }, [tooltip, name]);

  useEffect(() => {
    if (
      !secondaryTooltip &&
      textSecondaryRef.current &&
      (textSecondaryRef.current.scrollHeight > textSecondaryRef.current.clientHeight ||
        textSecondaryRef.current.scrollWidth > textSecondaryRef.current.clientWidth)
    ) {
      setSecondaryTooltipValue(secondaryText);
    }
  }, [secondaryTooltip, secondaryText]);

  return (
    <div className={classNames(style.userInfoWrapper, className)}>
      {isLogo && (
        <Logo content="user" size="24" radius="rounded" name={name} className={style.logoWrapper} />
      )}
      <Container flexgrow="1" flexdirection="column" className={style.container}>
        <Tooltip message={tooltipValue}>
          <Text
            ref={textPrimaryRef}
            type="caption-regular"
            noWrap
            className={classNames({ [style.isLogo]: isLogo })}
          >
            {name}
          </Text>
        </Tooltip>
        <Tooltip message={secondaryTooltipValue}>
          <Text
            ref={textSecondaryRef}
            color="grey-100"
            noWrap
            className={classNames({ [style.isLogo]: isLogo })}
          >
            {secondaryText}
          </Text>
        </Tooltip>
      </Container>
    </div>
  );
};

export default UserInfo;
