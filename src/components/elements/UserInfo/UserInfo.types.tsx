import React from 'react';

export interface UserInfoProps {
  isLogo?: boolean;
  name: string;
  secondaryText?: string;
  className?: string;
  tooltip?: React.ReactNode;
  secondaryTooltip?: React.ReactNode;
}
