import { ReactComponent as AfterpayIcon } from 'Svg/v2/32/logo-afterpay.svg';
import { ReactComponent as AmericanExpressIcon } from 'Svg/v2/32/logo-amex.svg';
import { ReactComponent as MastercardIcon } from 'Svg/v2/32/logo-mastercard.svg';
import { ReactComponent as VisaIcon } from 'Svg/v2/32/logo-visa.svg';
import React from 'react';

export type PaymentSystemType = 'visa' | 'mastercard' | 'amex' | 'afterpay';

export const paymentSystemsPreset: Record<PaymentSystemType | string, JSX.Element> = {
  visa: <VisaIcon />,
  mastercard: <MastercardIcon />,
  amex: <AmericanExpressIcon />,
  afterpay: <AfterpayIcon />,
};
