import { Meta, Story } from '@storybook/react';
import {
  BillingCardItem as Component,
  BillingCardItemProps as Props,
} from 'Components/elements/BillingCardItem';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Elements/Billing Card Item',
  component: Component,
  argTypes: {
    className: hideProperty,
    onDelete: hideProperty,
    onEdit: hideProperty,
    onToggleDefault: hideProperty,
  },
} as Meta;

const BillingCardItemComponent: Story<Props> = (args) => <Component {...args} />;

export const BillingCardItem = BillingCardItemComponent.bind({});
BillingCardItem.args = {
  title: 'Title',
  secondaryTitle: 'Secondary title',
  tertiaryTitle: 'Tertiary title',
  account: 'Account number',
  currency: 'USD',
  paymentSystem: 'mastercard',
  isDefault: true,
  isBlurred: false,
};
