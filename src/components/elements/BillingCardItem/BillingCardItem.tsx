import { Icon } from 'Components/base/Icon';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import { ReactComponent as StarFilledIcon } from 'Svg/v2/16/star-filled.svg';
import { ReactComponent as StarIcon } from 'Svg/v2/16/star.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import classNames from 'classnames';
import React from 'react';

import style from './BillingCardItem.module.scss';
import { PaymentSystemType, paymentSystemsPreset } from './paymentSystems';

export interface BillingCardItemProps {
  title: string;
  secondaryTitle?: string;
  tertiaryTitle?: string;
  account: string;
  currency?: string;
  paymentSystem?: PaymentSystemType | string;
  isDefault?: boolean;
  isBlurred?: boolean;
  className?: string;
  onToggleDefault(): void;
  onEdit(): void;
  onDelete(): void;
}

const BillingCardItem: React.FC<BillingCardItemProps> = ({
  title,
  secondaryTitle,
  tertiaryTitle,
  account,
  currency,
  paymentSystem,
  isDefault,
  isBlurred,
  onToggleDefault,
  onEdit,
  onDelete,
  className,
}) => (
  <div className={classNames(style.card, { [style.blurred]: isBlurred }, className)}>
    <div className={style.action}>
      {isDefault && (
        <div className={style.default}>
          <Icon icon={<StarFilledIcon />} path="grey-90" />
          <Text color="grey-90">Default</Text>
        </div>
      )}
      <div className={style.controls}>
        <IconButton
          icon={isDefault ? <StarFilledIcon /> : <StarIcon />}
          tooltip={isDefault ? 'Unselect as default' : 'Select as default'}
          size="24"
          background="transparent"
          color="grey-100-violet-90"
          onClick={onToggleDefault}
        />
        <IconButton
          icon={<EditIcon />}
          size="24"
          background="transparent"
          color="grey-100-yellow-90"
          onClick={onEdit}
        />
        <IconButton
          icon={<TrashIcon />}
          size="24"
          background="transparent"
          color="grey-100-red-90"
          onClick={onDelete}
        />
      </div>
    </div>
    <div className={style.info}>
      <div className={style.title}>
        <Text type="body-regular-14" lineClamp="2">
          {title}
        </Text>
        <Text type="caption-regular" color="grey-90" lineClamp="2">
          {tertiaryTitle}
        </Text>
      </div>
      <div className={style.account}>
        {secondaryTitle && (
          <Text type="body-regular-14" color="grey-100">
            {secondaryTitle}
          </Text>
        )}
        <Text type="body-regular-14" color="grey-100">
          {account}
        </Text>
        {currency && (
          <div className={style.data}>
            <Text type="body-medium" color="grey-90">
              {currency}
            </Text>
            {paymentSystem &&
              (paymentSystem in paymentSystemsPreset ? (
                <Icon icon={paymentSystemsPreset[paymentSystem]} clickThrough staticColor />
              ) : null)}
          </div>
        )}
      </div>
    </div>
  </div>
);

export default BillingCardItem;
