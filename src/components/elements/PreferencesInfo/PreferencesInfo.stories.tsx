import { Meta, Story } from '@storybook/react';
import {
  PreferencesInfo as Component,
  PreferencesInfoProps as Props,
} from 'Components/elements/PreferencesInfo';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Elements/Preferences Info',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const PreferencesInfoComponent: Story<Props> = (args) => <Component {...args} />;

export const PreferencesInfo = PreferencesInfoComponent.bind({});
PreferencesInfo.args = {
  active: true,
  conditionText: 'Warn if duplicate journal number is used',
};
