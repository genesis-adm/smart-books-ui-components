import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as IconNegative } from 'Svg/20/error.svg';
import { ReactComponent as IconSuccess } from 'Svg/20/success.svg';
import React from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

export interface PreferencesInfoProps {
  active: boolean;
  conditionText: string;
  className?: string;
}

const PreferencesInfo: React.FC<PreferencesInfoProps> = ({ active, conditionText, className }) => (
  <Container alignitems="center" className={className}>
    {active ? <IconSuccess className={spacing.mr16} /> : <IconNegative className={spacing.mr16} />}
    <Text type="body-regular-14" color="black-100">
      {conditionText}
    </Text>
  </Container>
);

export default PreferencesInfo;
