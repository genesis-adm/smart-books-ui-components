import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React from 'react';

import style from './MoreDB.module.scss';

export interface MoreDBProps {
  quantity: number;
  className?: string;
}

const MoreDB: React.FC<MoreDBProps> = ({ quantity, className }) => {
  const checkQuantity = quantity > 0;
  return (
    <>
      {checkQuantity && (
        <div className={classNames(style.component, className)}>
          <Text type="caption-regular" color="grey-100">
            +{quantity}
          </Text>
        </div>
      )}
    </>
  );
};

export default MoreDB;
