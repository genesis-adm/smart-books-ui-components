import { Meta, Story } from '@storybook/react';
import { MoreDB as Component, MoreDBProps as Props } from 'Components/elements/MoreDB';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Elements/MoreDB',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const MoreDBComponent: Story<Props> = (args) => <Component {...args} />;

export const MoreDB = MoreDBComponent.bind({});
MoreDB.args = {
  quantity: 332,
};
