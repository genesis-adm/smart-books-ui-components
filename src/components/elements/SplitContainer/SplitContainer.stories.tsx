import { Meta, Story } from '@storybook/react';
import { OptionDoubleHorizontalNew } from 'Components/base/inputs/SelectNew/options/OptionDoubleHorizontalNew';
import { OptionDoubleVerticalNew } from 'Components/base/inputs/SelectNew/options/OptionDoubleVerticalNew';
import { RuleSplit } from 'Components/custom/Banking/RuleSplit';
import { InputAutocomplete } from 'Components/elements/InputAutocomplete';
import { InputAutocompleteItemDoubleHorizontal } from 'Components/elements/InputAutocomplete/InputAutocompleteItemDoubleHorizontal';
import {
  SplitContainer as Component,
  SplitContainerProps as Props,
} from 'Components/elements/SplitContainer';
import { Input } from 'Components/old/Input';
import { options, suggestions } from 'Mocks/fakeOptions';
import React, { useState } from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Elements/Split Container',
  component: Component,
  argTypes: {
    className: hideProperty,
    onDelete: hideProperty,
  },
} as Meta;

const SplitContainerComponent: Story<Props> = (args) => {
  const [isOpen, setOpen] = useState(false);
  const handleChange = () => setOpen(true);
  const handleClick = () => {
    setOpen(false);
  };
  return (
    <Component {...args}>
      <InputAutocomplete
        className={spacing.mt8}
        width="full"
        insideRules
        input={
          <Input
            name="chartofaccount"
            autoComplete="off"
            width="full"
            label="Choose account"
            isRequired
            placeholder="Please choose account"
            onChange={handleChange}
          />
        }
      >
        {isOpen &&
          suggestions.map((item) => (
            <InputAutocompleteItemDoubleHorizontal
              label={item.bankname}
              secondaryLabel="Expenses"
              key={item.id}
              onClick={handleClick}
            />
          ))}
        {/* {isOpen && <InputAutocompleteText label="Please enter 2 or more characters" />} */}
      </InputAutocomplete>
      <RuleSplit showDeleteButton type="division">
        {options.map((item) => (
          <OptionDoubleHorizontalNew
            key={item.value}
            label={item.label}
            selected={false}
            secondaryLabel={item.secondaryLabel}
            onClick={() => {}}
          />
        ))}
      </RuleSplit>
      <RuleSplit showDeleteButton type="division">
        {options.map((item) => (
          <OptionDoubleVerticalNew
            key={item.value}
            label={item.label}
            secondaryLabel={item.secondaryLabel}
            selected={false}
            onClick={() => {}}
          />
        ))}
      </RuleSplit>
    </Component>
  );
};

export const SplitContainer = SplitContainerComponent.bind({});
SplitContainer.args = {
  splitTotalSum: 0,
  splitTotalCount: 2,
  splitCurrentNumber: 2,
};
