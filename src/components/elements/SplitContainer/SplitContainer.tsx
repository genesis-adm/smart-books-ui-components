import { Divider } from 'Components/base/Divider';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import classNames from 'classnames';
import React from 'react';

import style from './SplitContainer.module.scss';

export interface SplitContainerProps {
  splitType: 'percent' | 'amount';
  splitCurrency?: string;
  splitTotalSum: number;
  splitTotalCount: number;
  splitCurrentNumber: number;
  className?: string;
  children?: React.ReactNode;
  onDelete(e: React.MouseEvent<HTMLDivElement>): void;
}

const SplitContainer: React.FC<SplitContainerProps> = ({
  splitType,
  splitCurrency,
  splitTotalSum,
  splitTotalCount,
  splitCurrentNumber,
  className,
  onDelete,
  children,
}) => {
  const splitUnit = splitType === 'percent' ? '%' : splitCurrency;
  const splitUnitText = splitType === 'percent' ? 'Split percent' : 'Split amount';
  return (
    <div className={classNames(style.component, className)}>
      <Container className={style.title} alignitems="center">
        <Text type="body-regular-14" color="grey-100">
          Split
          {splitTotalCount > 1 && (
            <Text type="inherit" color="inherit">
              &nbsp;
              {splitCurrentNumber}
            </Text>
          )}
        </Text>
        <Divider type="horizontal" position="both" />
        <Text type="body-regular-14" color="grey-100">
          {splitUnitText}
          &nbsp;
          {splitTotalSum.toFixed(2)}
          &nbsp;
          {splitUnit}
        </Text>
        {splitTotalCount > 1 && (
          <IconButton
            className={style.button}
            icon={<TrashIcon />}
            size="minimal"
            color="grey-100-red-90"
            background="transparent"
            onClick={onDelete}
          />
        )}
      </Container>
      {children}
    </div>
  );
};

export default SplitContainer;
