import { Icon } from 'Components/base/Icon';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as TrashIcon } from 'Svg/v2/32/trashbox.svg';
import classNames from 'classnames';
import React, { ReactElement } from 'react';

import style from './IconsPickerAction.module.scss';
import { IconsPickerActionProps } from './IconsPickerAction.types';

// Reminder: do not forget to add the following code to root index.ts
// export { IconsPickerAction } from './components/elements/IconsPicker/IconsPickerAction';

const IconsPickerAction = ({
  className,
  isLoaded,
  onDelete,
}: IconsPickerActionProps): ReactElement => {
  const handleClick = () => {
    if (isLoaded) onDelete();
  };
  return (
    <div
      className={classNames(
        style.action,
        {
          [style.delete]: isLoaded,
        },
        className,
      )}
      role="presentation"
      onClick={handleClick}
    >
      {isLoaded && (
        <>
          <Icon icon={<TrashIcon />} path="white-100" />
          <Text className={style.text} type="button" color="white-100">
            Delete
          </Text>
        </>
      )}
    </div>
  );
};

export default IconsPickerAction;
