export interface IconsPickerActionProps {
  isLoaded: boolean;
  className?: string;
  onDelete(): void;
}
