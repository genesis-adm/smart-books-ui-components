import { pickerIcons } from 'Pages/Paidlog/Categories/constants';
import { ReactNode } from 'react';

export interface IconsPickerProps {
  className?: string;
  icon?: ReactNode;

  onClick(icon: { id: keyof typeof pickerIcons; icon: ReactNode }): void;

  onDelete(): void;
}
