import { Meta, Story } from '@storybook/react';
import {
  IconsPicker as Component,
  IconsPickerProps as Props,
} from 'Components/elements/IconsPicker';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/elements/IconsPicker',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const IconsPickerComponent: Story<Props> = (args) => <Component {...args} />;

export const IconsPicker = IconsPickerComponent.bind({});
IconsPicker.args = {};
