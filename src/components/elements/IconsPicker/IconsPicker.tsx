import { DropDown, useDropDown } from 'Components/DropDown';
import { Icon } from 'Components/base/Icon';
import { Text } from 'Components/base/typography/Text';
import { IconsPickerAction } from 'Components/elements/IconsPicker/IconsPickerAction';
import { pickerIcons } from 'Pages/Paidlog/Categories/constants';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import classNames from 'classnames';
import React, { ReactElement, ReactNode } from 'react';

import style from './IconsPicker.module.scss';
import { IconsPickerProps } from './IconsPicker.types';

const IconsPicker = ({ className, onClick, icon, onDelete }: IconsPickerProps): ReactElement => {
  return (
    <DropDown
      padding="24"
      flexdirection="column"
      classnameInner={style.picker_container}
      position="bottom-left"
      control={({ handleOpen }) => (
        <div className={classNames(style.component, className)}>
          <div className={style.component_text} onClick={!icon ? handleOpen : onDelete}>
            {icon ? (
              <Icon icon={icon} className={style.chosen_icon} />
            ) : (
              <>
                <Icon icon={<PlusIcon />} path="violet-90" />
                <Text color="violet-90" type="button">
                  Add new
                </Text>
              </>
            )}
            {icon && <IconsPickerAction isLoaded={!!icon} onDelete={onDelete} />}
          </div>
        </div>
      )}
    >
      <DropBody onClick={onClick} />
    </DropDown>
  );
};

export default IconsPicker;

const DropBody = ({
  onClick,
}: {
  onClick?: (icon: { id: keyof typeof pickerIcons; icon: ReactNode }) => void;
}): ReactElement => {
  const [handleToggle] = useDropDown();

  return (
    <>
      <Text color="grey-100" type="body-regular-14">
        Please choose Icons
      </Text>

      <div className={style.icons}>
        {(Object.entries(pickerIcons) as [keyof typeof pickerIcons, ReactNode][]).map(
          ([id, icon]) => (
            <div
              key={id}
              className={style.icon}
              onClick={() => {
                onClick?.({
                  id,
                  icon,
                });
                handleToggle?.();
              }}
            >
              <Icon icon={icon} />
            </div>
          ),
        )}
      </div>
    </>
  );
};
