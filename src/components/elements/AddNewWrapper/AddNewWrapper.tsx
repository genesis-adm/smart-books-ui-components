import classNames from 'classnames';
import React, { ReactNode } from 'react';

import style from './AddNewWrapper.module.scss';

export interface AddNewWrapperProps {
  className?: string;
  children?: ReactNode;
}

const AddNewWrapper: React.FC<AddNewWrapperProps> = ({ className, children }) => (
  <div className={classNames(style.wrapper, className)}>{children}</div>
);

export default AddNewWrapper;
