import { IconButton } from 'Components/base/buttons/IconButton';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as CloseIcon } from 'Svg/v2/16/cross.svg';
import classNames from 'classnames';
import React from 'react';

import style from './NotificationNew.module.scss';

export interface NotificationNewProps {
  rootElementId?: string;
  content: React.ReactNode;
  icon?: React.ReactNode;
  action?: React.ReactNode;
  iconRotate?: boolean;
  type?: 'default' | 'success' | 'warning' | 'error';
  position?: 'absolute' | 'fixed';
  className?: string;
  onClose(e: React.MouseEvent): void;
}

const NotificationNew: React.FC<NotificationNewProps> = ({
  rootElementId,
  content,
  icon,
  action,
  iconRotate,
  type,
  position = 'absolute',
  className,
  onClose,
}) => (
  <div
    className={classNames(
      style.wrapper,
      style[`type_${type}`],
      style[`position_${position}`],
      className,
    )}
  >
    <div className={style.main}>
      <div
        className={classNames(style.icon, {
          [style.rotate]: iconRotate,
        })}
      >
        {icon}
      </div>

      <div className={style.content}>
        {typeof content === 'string' ? (
          <Text className={style.content} type="body-regular-16" color="inherit">
            {content}
          </Text>
        ) : (
          <div className={style.content}>{content}</div>
        )}
      </div>

      <IconButton
        className={style.close}
        size="28"
        background={'transparent'}
        color="white-100"
        icon={<CloseIcon />}
        onClick={onClose}
      />
    </div>

    {action && <div className={style.action}>{action}</div>}
  </div>
);

export default NotificationNew;
