import { Meta, Story } from '@storybook/react';
import { Icon } from 'Components/base/Icon';
import { Button } from 'Components/base/buttons/Button';
import {
  NotificationNew as Component,
  NotificationNewProps as Props,
} from 'Components/elements/NotificationNew';
import { ReactComponent as NotificationIcon } from 'Svg/v2/32/alert.svg';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Elements/NotificationNew',
  component: Component,
  argTypes: {
    icon: hideProperty,
    action: hideProperty,
    className: hideProperty,
    onClose: hideProperty,
  },
} as Meta;

const NotificationNewComponent: Story<Props> = (args) => (
  <Component
    {...args}
    icon={<Icon icon={<NotificationIcon />} path="inherit" />}
    action={
      <>
        <Button background={'transparent-black-90'} height="32" onClick={() => {}}>
          No
        </Button>
        <Button height="32" onClick={() => {}}>
          Yes
        </Button>
      </>
    }
  />
);

export const NotificationNew = NotificationNewComponent.bind({});
NotificationNew.args = {
  content: 'Please add Category of match to Accept transactions',
  type: 'default',
  position: 'absolute',
  iconRotate: false,
};
