import { Meta, Story } from '@storybook/react';
import {
  UserRoleItem as Component,
  UserRoleItemProps as Props,
} from 'Components/elements/UserRoleItem';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Elements/User Role Item',
  component: Component,
  argTypes: {
    className: hideProperty,
    onLinkClick: hideProperty,
    onDelete: hideProperty,
  },
} as Meta;

const UserRoleItemComponent: Story<Props> = (args) => <Component {...args} />;

export const UserRoleItem = UserRoleItemComponent.bind({});
UserRoleItem.args = {
  organization: 'Organization name',
  userrole: 'User role',
  img: 'https://picsum.photos/60',
  color: '#7AC72D',
};
