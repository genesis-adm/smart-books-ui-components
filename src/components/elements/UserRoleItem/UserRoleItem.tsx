import { Text } from 'Components/base/typography/Text';
import { Logo } from 'Components/elements/Logo';
import { ReactComponent as LinkIcon } from 'Svg/v2/16/link.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import classNames from 'classnames';
import React from 'react';

import style from './UserRoleItem.module.scss';

export interface UserRoleItemProps {
  organization: string;
  userrole: string;
  color?: string;
  img?: string;
  className?: string;
  onLinkClick(): void;
  onDelete(): void;
}

const UserRoleItem: React.FC<UserRoleItemProps> = ({
  organization,
  userrole,
  color,
  img,
  className,
  onLinkClick,
  onDelete,
}) => (
  <div className={classNames(style.component, className)}>
    <div className={classNames(style.container, style.container_large)}>
      <Logo
        size="md"
        className={style.logo}
        content="company"
        name={organization}
        color={color}
        img={img}
      />
      <Text type="body-regular-14" noWrap>
        {organization}
      </Text>
    </div>
    <div className={classNames(style.container, style.container_small)}>
      <Text type="body-regular-14" noWrap>
        {userrole}
      </Text>
      <LinkIcon onClick={onLinkClick} />
    </div>
    <div className={style.action} role="presentation" onClick={onDelete}>
      <TrashIcon />
    </div>
  </div>
);

export default UserRoleItem;
