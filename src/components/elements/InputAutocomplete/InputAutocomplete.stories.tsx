import { Meta, Story } from '@storybook/react';
import {
  InputAutocomplete as Component,
  InputAutocompleteProps as Props,
} from 'Components/elements/InputAutocomplete';
import { Input } from 'Components/old/Input';
import { suggestions } from 'Mocks/fakeOptions';
import React, { useState } from 'react';

// import { InputAutocompleteItem } from './InputAutocompleteItem';
// import { ReactComponent as SearchIcon } from 'Svg/16/searchAlt.svg';
import { InputAutocompleteItemDoubleHorizontal } from './InputAutocompleteItemDoubleHorizontal';

// import { InputAutocompleteText } from './InputAutocompleteText';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Elements/Input Autocomplete',
  component: Component,
  argTypes: {
    input: hideProperty,
    className: hideProperty,
  },
} as Meta;

const InputAutocompleteComponent: Story<Props> = ({ input, ...args }) => {
  const [isOpen, setOpen] = useState(false);
  const handleChange = () => setOpen(true);
  const handleClick = () => {
    setOpen(false);
  };
  // return (
  //   <InputAutocomplete
  //     input={<Input name="InputDefault" autoComplete="off" icon={<SearchIcon />}
  //     iconPosition="right" suggestionActive={isOpen} width="full"
  //     placeholder="Placeholder of default input" onChange={handleChange} />}
  //     {...args}
  //   >
  //     {isOpen && <InputAutocompleteText label="Please enter 2 or more characters" />}
  //   </InputAutocomplete>
  // );
  return (
    <Component
      input={
        <Input
          name="InputDefault"
          autoComplete="off"
          suggestionActive={isOpen}
          width="full"
          placeholder="Placeholder of default input"
          onChange={handleChange}
        />
      }
      {...args}
    >
      {isOpen &&
        suggestions.map((item) => (
          <InputAutocompleteItemDoubleHorizontal
            label={item.bankname}
            secondaryLabel="Expenses"
            key={item.id}
            onClick={handleClick}
          />
        ))}
    </Component>
  );
};

export const InputAutocomplete = InputAutocompleteComponent.bind({});
InputAutocomplete.args = {
  width: '320',
  showLink: true,
};
