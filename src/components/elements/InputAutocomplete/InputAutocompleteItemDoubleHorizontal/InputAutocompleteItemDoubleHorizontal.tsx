import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React from 'react';

import style from './InputAutocompleteItemDoubleHorizontal.module.scss';

export interface InputACItemDoubleHorizontalProps {
  label: string;
  secondaryLabel: string;
  className?: string;
  onClick(): void;
}

const InputAutocompleteItemDoubleHorizontal: React.FC<InputACItemDoubleHorizontalProps> = ({
  label,
  secondaryLabel,
  className,
  onClick,
}) => (
  <div role="presentation" className={classNames(style.component, className)} onClick={onClick}>
    <Text className={style.label} type="caption-regular" color="black-100" noWrap>
      {label}
    </Text>
    <Text className={style.secondaryLabel} type="caption-regular" color="grey-100" noWrap>
      {secondaryLabel}
    </Text>
  </div>
);

export default InputAutocompleteItemDoubleHorizontal;
