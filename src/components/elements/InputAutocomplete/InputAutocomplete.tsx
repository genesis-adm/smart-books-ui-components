import { LinkButton } from 'Components/base/buttons/LinkButton';
import classNames from 'classnames';
import React from 'react';

import style from './InputAutocomplete.module.scss';

export interface InputAutocompleteProps {
  input: React.ReactNode;
  insideRules?: boolean;
  width?: '320' | 'full';
  showLink?: boolean;
  className?: string;
  children?: React.ReactNode;
}

const InputAutocomplete: React.FC<InputAutocompleteProps> = ({
  input,
  insideRules,
  width = '320',
  showLink,
  className,
  children,
}) => (
  <div
    className={classNames(
      style.wrapper,
      style[`width_${width}`],
      {
        [style.rules]: insideRules,
      },
      className,
    )}
  >
    {input}
    {children && (
      <>
        {showLink && (
          <LinkButton
            className={style.link}
            type="caption-regular"
            color="grey-100-violet-90"
            onClick={() => {}}
          >
            New
          </LinkButton>
        )}
        {!!React.Children.toArray(children).length && (
          <div className={classNames(style.autocomplete, className)}>{children}</div>
        )}
      </>
    )}
  </div>
);
export default InputAutocomplete;
