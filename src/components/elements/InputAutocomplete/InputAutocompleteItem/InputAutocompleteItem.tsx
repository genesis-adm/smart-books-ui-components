import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React from 'react';

import style from './InputAutocompleteItem.module.scss';

export interface InputAutocompleteItemProps {
  label: string;
  className?: string;
  onClick(): void;
}

const InputAutocompleteItem: React.FC<InputAutocompleteItemProps> = ({
  label,
  className,
  onClick,
}) => (
  <div role="presentation" className={classNames(style.component, className)} onClick={onClick}>
    <Text className={style.text} type="caption-regular" color="inherit" noWrap>
      {label}
    </Text>
  </div>
);

export default InputAutocompleteItem;
