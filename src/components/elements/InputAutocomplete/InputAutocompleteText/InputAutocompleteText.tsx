import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React from 'react';

import style from './InputAutocompleteText.module.scss';

export interface InputAutocompleteTextProps {
  label: string;
  className?: string;
}

const InputAutocompleteText: React.FC<InputAutocompleteTextProps> = ({ label, className }) => (
  <div role="presentation" className={classNames(style.component, className)}>
    <Text className={style.text} type="caption-regular" color="grey-100" noWrap>
      {label}
    </Text>
  </div>
);

export default InputAutocompleteText;
