import { Meta, Story } from '@storybook/react';
import { Currencies as Component, CurrenciesProps as Props } from 'Components/elements/Currencies';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Elements/Currencies',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const CurrenciesComponent: Story<Props> = (args) => <Component {...args} />;

export const Currencies = CurrenciesComponent.bind({});
Currencies.args = {
  selectedCurrencyValue: '1',
  selectedCurrency: 'eur',
  baseCurrencyValue: '1.01',
  baseCurrency: 'usd',
  showCustomRate: false,
};
