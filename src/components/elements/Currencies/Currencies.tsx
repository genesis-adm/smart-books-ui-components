import { DropDown } from 'Components/DropDown';
import { Icon } from 'Components/base/Icon';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as EURIcon } from 'Svg/colored/eur.svg';
import { ReactComponent as UAHIcon } from 'Svg/colored/uah.svg';
import { ReactComponent as USDIcon } from 'Svg/colored/usd.svg';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import classNames from 'classnames';
import React, { ChangeEvent } from 'react';
import { NumberFormatValues, SourceInfo } from 'react-number-format';

import style from './Currencies.module.scss';
import CurrenciesCustomRate from './CurrenciesCustomRate';

type CurrencyType = 'EUR' | 'USD' | 'UAH';

interface CurrencyContent {
  name: 'EUR' | 'USD' | 'UAH';
  icon: JSX.Element;
}

export interface CurrenciesProps {
  selectedCurrencyValue: string;
  selectedCurrency: string;
  baseCurrencyValue: string;
  baseCurrency: string;
  background?: 'grey-10' | 'white-100';
  height?: '32' | '24';
  opened?: boolean;
  showCustomRate?: boolean;
  baseCurrencyInputValue?: string;

  onChange?(e: ChangeEvent<HTMLInputElement>): void;

  onValueChange?(values: NumberFormatValues, sourceInfo: SourceInfo): void;

  onClose?(): void;

  onApply?(): void;

  className?: string;
  disableOutsideClick?: boolean;
}

export const currenciesList: Record<CurrencyType, CurrencyContent> = {
  USD: {
    name: 'USD',
    icon: <USDIcon />,
  },
  EUR: {
    name: 'EUR',
    icon: <EURIcon />,
  },
  UAH: {
    name: 'UAH',
    icon: <UAHIcon />,
  },
};

const Currencies: React.FC<CurrenciesProps> = ({
  selectedCurrencyValue,
  selectedCurrency,
  baseCurrencyValue,
  baseCurrency,
  background = 'grey-10',
  height = '32',
  opened,
  showCustomRate,
  baseCurrencyInputValue,
  onChange,
  onValueChange,
  onClose,
  onApply,
  className,
  disableOutsideClick,
}) => {
  const isSelectedCurrencyInList =
    selectedCurrency === 'EUR' || selectedCurrency === 'USD' || selectedCurrency === 'UAH';
  const isBaseCurrencyInList =
    baseCurrency === 'EUR' || baseCurrency === 'USD' || baseCurrency === 'UAH';
  return (
    <div
      className={classNames(
        style.component,
        style[`background_${background}`],
        style[`height_${height}`],
        className,
      )}
    >
      <Text type="text-regular">{selectedCurrencyValue}</Text>
      <Text type="text-regular">{selectedCurrency.toUpperCase()}</Text>
      {isSelectedCurrencyInList && <Icon icon={currenciesList[selectedCurrency].icon} />}
      <Text type="text-regular">=</Text>
      <Text type="text-regular">{baseCurrencyValue}</Text>
      <Text type="text-regular">{baseCurrency.toUpperCase()}</Text>
      {isBaseCurrencyInList && <Icon icon={currenciesList[baseCurrency].icon} />}
      {showCustomRate && (
        <DropDown
          flexdirection="column"
          padding="16"
          gap="16"
          opened={opened}
          disableOutsideClick={disableOutsideClick}
          control={({ isOpen, handleOpen }) => (
            <IconButton
              className={style.edit}
              size="24"
              background={isOpen ? 'blue-10-blue-20' : 'grey-10-grey-20'}
              color={isOpen ? 'violet-90' : 'grey-100'}
              icon={<EditIcon />}
              tooltip={isOpen ? '' : 'Edit FX rates'}
              onClick={handleOpen}
            />
          )}
        >
          <CurrenciesCustomRate
            onClose={onClose}
            onApply={onApply}
            onChange={onChange}
            onValueChange={onValueChange}
            baseCurrencyInputValue={baseCurrencyInputValue}
            selectedCurrencyValue={selectedCurrencyValue}
            selectedCurrency={selectedCurrency}
            baseCurrencyValue={baseCurrencyValue}
            baseCurrency={baseCurrency}
          />
        </DropDown>
      )}
    </div>
  );
};

export default Currencies;
