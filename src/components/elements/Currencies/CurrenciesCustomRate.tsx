import { DropDownContext } from 'Components/DropDown';
import { Divider } from 'Components/base/Divider';
import { Icon } from 'Components/base/Icon';
import { Button } from 'Components/base/buttons/Button';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { InputCurrency } from 'Components/base/inputs/InputCurrency';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as CrossIcon } from 'Svg/v2/16/cross.svg';
import React, { ChangeEvent, ReactElement, useContext } from 'react';
import { NumberFormatValues, SourceInfo } from 'react-number-format';

import { currenciesList } from './Currencies';
import style from './Currencies.module.scss';

interface CurrenciesCustomRateProps {
  selectedCurrencyValue: string;
  selectedCurrency: string;
  baseCurrencyValue: string;
  baseCurrency: string;
  baseCurrencyInputValue?: string;

  onClose?(): void;

  onApply?(): void;

  onChange?(e: ChangeEvent<HTMLInputElement>): void;

  onValueChange?(values: NumberFormatValues, sourceInfo: SourceInfo): void;
}

const CurrenciesCustomRate = ({
  selectedCurrencyValue,
  selectedCurrency,
  baseCurrencyInputValue,
  baseCurrency,
  onChange,
  onValueChange,
  onClose,
  onApply,
}: CurrenciesCustomRateProps): ReactElement => {
  const { handleOpen } = useContext(DropDownContext);

  const handleClose = () => {
    if (onClose) onClose();
    if (handleOpen) handleOpen();
  };

  const handleApply = () => {
    if (onApply) onApply();
    if (handleOpen) handleOpen();
  };

  const isSelectedCurrencyInList =
    selectedCurrency === 'EUR' || selectedCurrency === 'USD' || selectedCurrency === 'UAH';
  const isBaseCurrencyInList =
    baseCurrency === 'EUR' || baseCurrency === 'USD' || baseCurrency === 'UAH';

  return (
    <>
      <Container justifycontent="space-between" alignitems="center">
        <Text type="body-regular-14" color="grey-100">
          FX Rates
        </Text>
        <IconButton
          size="24"
          background="white-100-grey-10"
          color="black-100"
          icon={<CrossIcon />}
          tooltip="Close"
          onClick={handleClose}
        />
      </Container>
      <div className={style.customWrapper}>
        <Text type="subtext-regular" color="grey-100" className={style.from}>
          From
        </Text>
        <Text type="subtext-regular" color="grey-100" className={style.to}>
          To
        </Text>
        <Container className={style.selected} alignitems="center" gap="4">
          <Text type="text-regular">{selectedCurrencyValue}</Text>
          <Text type="text-regular">{selectedCurrency.toUpperCase()}</Text>
          {isSelectedCurrencyInList && <Icon icon={currenciesList[selectedCurrency].icon} />}
        </Container>
        <Text className={style.equal}>=</Text>
        <InputCurrency
          className={style.base}
          currencyCode={baseCurrency}
          currencyIcon={isBaseCurrencyInList ? currenciesList[baseCurrency].icon : undefined}
          value={baseCurrencyInputValue}
          allowNegative={false}
          onChange={onChange}
          onValueChange={onValueChange}
        />
      </div>
      <Divider fullHorizontalWidth />
      <Container justifycontent="flex-end">
        <Button
          width="auto"
          height="32"
          padding="8"
          font="text-medium"
          onClick={handleApply}
          background="violet-90-violet-100"
          color="white-100"
        >
          Apply
        </Button>
      </Container>
    </>
  );
};

export default CurrenciesCustomRate;
