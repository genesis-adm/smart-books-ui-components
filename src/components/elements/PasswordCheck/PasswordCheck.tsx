import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as PassError } from 'Svg/12/error.svg';
import { ReactComponent as PassOK } from 'Svg/12/success.svg';
import React from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

export interface PasswordCheckProps {
  valid: boolean;
  conditionText: string;
  className?: string;
}

const PasswordCheck: React.FC<PasswordCheckProps> = ({ valid, conditionText, className }) => (
  <Container alignitems="center" className={className}>
    {valid ? <PassOK className={spacing.mr5} /> : <PassError className={spacing.mr5} />}
    <Text type="text-regular" color={valid ? 'green-90' : 'black-100'}>
      {conditionText}
    </Text>
  </Container>
);

export default PasswordCheck;
