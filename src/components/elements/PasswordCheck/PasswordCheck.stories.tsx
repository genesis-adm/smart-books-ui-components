import { Meta, Story } from '@storybook/react';
import {
  PasswordCheck as Component,
  PasswordCheckProps as Props,
} from 'Components/elements/PasswordCheck';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Elements/Password Check',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const PasswordCheckComponent: Story<Props> = (args) => <Component {...args}>Test</Component>;

export const PasswordCheck = PasswordCheckComponent.bind({});
PasswordCheck.args = {
  valid: true,
  conditionText: 'At least one number and one special character',
};
