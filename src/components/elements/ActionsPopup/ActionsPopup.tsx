import { IconButton } from 'Components/base/buttons/IconButton';
import { ReactComponent as LeftIcon } from 'Svg/24/angle-left.svg';
import { ReactComponent as RightIcon } from 'Svg/24/angle-right.svg';
import { ReactComponent as ExclamationInFilledTriangle16Icon } from 'Svg/v2/16/exclamation-in-filled-triangle.svg';
import { ReactComponent as TickInFilledCircle16Icon } from 'Svg/v2/16/tick-in-filled-circle.svg';
import { ReactComponent as TickInFilledCircle34Icon } from 'Svg/v2/34/check-circle.svg';
import { ReactComponent as Delete34Icon } from 'Svg/v2/34/delete.svg';
import { ReactComponent as ExclamationInFilledTriangle34Icon } from 'Svg/v2/34/exclamation.svg';
import classNames from 'classnames';
import React, { JSX, ReactElement } from 'react';

import style from './ActionsPopup.module.scss';
import { ActionType, ActionsPopupProps } from './ActionsPopup.types';

// Reminder: do not forget to add the following code to root index.ts
// export { ActionsPopup } from './components/elements/ActionsPopup';

const ActionsPopup = ({
  className,
  itemName,
  itemNumber,
  itemId,
  firstButtonText,
  secondButtonText,
  firstButtonIcon,
  secondButtonIcon,
  onFirstButtonClick,
  onSecondButtonClick,
  onNextButtonClick,
  onPreviousButtonClick,
  type,
  itemsAmount,
}: ActionsPopupProps): ReactElement => {
  const actionTypes: Record<ActionType, JSX.Element> = {
    actionButtons: (
      <div className={style.buttons_container}>
        <button
          type="button"
          className={classNames(style.exclude_button)}
          onClick={onFirstButtonClick}
        >
          <span className={style.icon}>
            {firstButtonIcon || <ExclamationInFilledTriangle16Icon />}
          </span>
          <span>{firstButtonText || 'Exclude'}</span>
        </button>
        <button type="button" className={style.accept_button} onClick={onSecondButtonClick}>
          <span className={style.icon}>{secondButtonIcon || <TickInFilledCircle16Icon />}</span>
          <span>{secondButtonText || 'Accept'}</span>
        </button>
      </div>
    ),
    accepted: (
      <div className={style.status_container}>
        <span className={style.icon}>
          <TickInFilledCircle34Icon />
        </span>
        <span className={style.accepted}>Accepted</span>
      </div>
    ),
    excluded: (
      <div className={style.status_container}>
        <span className={style.icon}>
          <ExclamationInFilledTriangle34Icon />
        </span>
        <span className={style.excluded}>Excluded</span>
      </div>
    ),
    deleted: (
      <div className={style.status_container}>
        <span className={style.icon}>
          <Delete34Icon />
        </span>
        <span className={style.deleted}>Deleted</span>
      </div>
    ),
  };

  return (
    <div className={classNames(style.component, className)}>
      <div className={style.text_container}>
        <span>
          {itemNumber} / {itemsAmount}
        </span>
        <div className={style.page_switcher_container}>
          <IconButton
            size="24"
            icon={<LeftIcon />}
            onClick={onPreviousButtonClick}
            background="transparent"
            color="inherit"
          />
          <span className={style.title}>
            {itemName} # {itemId}
          </span>
          <IconButton
            size="24"
            icon={<RightIcon />}
            onClick={onNextButtonClick}
            background="transparent"
            color="inherit"
          />
        </div>
      </div>
      {actionTypes[type]}
    </div>
  );
};

export default ActionsPopup;
