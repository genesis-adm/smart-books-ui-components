import { Meta, Story } from '@storybook/react';
import {
  ActionsPopup as Component,
  ActionsPopupProps as Props,
} from 'Components/elements/ActionsPopup';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Elements/ActionsPopup',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const ActionsPopupComponent: Story<Props> = (args) => <Component {...args} />;

export const ActionsPopup = ActionsPopupComponent.bind({});
ActionsPopup.args = {
  itemName: 'Transaction',
  itemId: '123213',
  itemNumber: 1,
  itemsAmount: 30,
  type: 'accepted',
};
