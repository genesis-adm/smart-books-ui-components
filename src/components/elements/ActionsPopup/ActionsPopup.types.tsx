import { ReactNode } from 'react';

export type ActionType = 'actionButtons' | 'accepted' | 'excluded' | 'deleted';

export interface ActionsPopupProps {
  type: ActionType;
  itemNumber: number;
  itemsAmount: number;
  itemName: string;
  itemId: string | number;
  firstButtonText?: string;
  secondButtonText?: string;
  firstButtonIcon?: ReactNode;
  secondButtonIcon?: ReactNode;
  onPreviousButtonClick(): void;
  onNextButtonClick(): void;
  onFirstButtonClick?(): void;
  onSecondButtonClick?(): void;
  className?: string;
}
