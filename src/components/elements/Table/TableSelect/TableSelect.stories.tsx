import { Meta, Story } from '@storybook/react';
import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { Icon } from 'Components/base/Icon';
import { OptionSingleNew } from 'Components/base/inputs/SelectNew/options/OptionSingleNew';
import {
  TableSelect as Component,
  TableSelectProps as Props,
} from 'Components/elements/Table/TableSelect';
import { options } from 'Mocks/fakeOptions';
import { ReactComponent as InformationIcon } from 'Svg/v2/16/info.svg';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Elements/Table/Table Select',
  component: Component,
  argTypes: {
    name: hideProperty,
    defaultValue: hideProperty,
    reset: hideProperty,
    onChange: hideProperty,
    className: hideProperty,
  },
} as Meta;

const TableSelectComponent: Story<Props> = (args) => (
  <Component {...args}>
    {({ onClick, state, searchValue, selectElement }) => (
      <SelectDropdown selectElement={selectElement} width="full">
        <SelectDropdownBody>
          <SelectDropdownItemList>
            {options
              .filter(({ label }) => label.toLowerCase().includes(searchValue.toLowerCase()))
              .map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() =>
                    onClick({
                      label,
                      value,
                    })
                  }
                />
              ))}
          </SelectDropdownItemList>
        </SelectDropdownBody>
      </SelectDropdown>
    )}
  </Component>
);

export const TableSelect = TableSelectComponent.bind({});
TableSelect.args = {
  isRowActive: false,
  disabled: false,
  error: false,
  tooltip: 'Test tooltip info',
  placeholder: 'Select option',
  icon: <Icon icon={<InformationIcon />} clickThrough />,
};
