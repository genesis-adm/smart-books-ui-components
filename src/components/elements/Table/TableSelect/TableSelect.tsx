import { Icon } from 'Components/base/Icon';
import { Tooltip } from 'Components/base/Tooltip';
import InputErrorPopup from 'Components/base/inputs/InputErrorPopup/InputErrorPopup';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as CaretDownIcon } from 'Svg/v2/16/caret-down.svg';
import classNames from 'classnames';
import React, { createContext, useCallback, useEffect, useMemo, useRef, useState } from 'react';

import useClickOutside from '../../../../hooks/useClickOutside';
import useScrollOutside from '../../../../hooks/useScrollOutside';
import style from './TableSelect.module.scss';
import { Option, TableSelectContextProps, TableSelectProps } from './TableSelect.types';
import useTableSelect from './hooks/useTableSelect';

export const TableSelectContext = createContext({} as TableSelectContextProps);

const DEFAULT_VALUE = {
  label: '',
  value: '',
};

const TableSelect = ({
  type = 'single',
  name,
  tooltip,
  disabled,
  error,
  errorMessage,
  placeholder,
  icon,
  border = 'none',
  isRowActive,
  defaultValue = DEFAULT_VALUE,
  value,
  reset,
  onChange,
  children,
  className,
  onSearch,
}: TableSelectProps) => {
  const wrapperRef = useRef<HTMLDivElement>(null);
  const selectElement = wrapperRef?.current;
  const [hovered, setHovered] = useState(false);

  const { isOpen, focus, setSelectActive, onClick, onClickMultiple, state, setState } =
    useTableSelect(defaultValue);

  useClickOutside({
    ref: wrapperRef.current,
    cb: setSelectActive,
  });
  useScrollOutside({
    isListOpen: isOpen,
    cb: setSelectActive,
  });

  const [searchValue, setSearchValue] = useState<string | null>(null);

  useEffect(() => {
    if (value) setState(value);
  }, [value]);

  const handleOnSearchValue = useCallback(
    (value: string | null) => {
      setSearchValue(value);
      onSearch?.(value);
    },
    [setSearchValue, onSearch],
  );

  const handleToggle = () => {
    if (!disabled && !focus) {
      setSelectActive(!isOpen);
    }
  };

  const handleClick = useCallback(
    (value: Option) => {
      if (onChange) onChange(value);
      handleOnSearchValue(null);
      onClick(value);
    },
    [onChange, onClick, handleOnSearchValue],
  );

  const handleClickMultiple = useCallback(
    (value: Option) => {
      if (onChange) onChange(value);
      handleOnSearchValue(null);
      onClickMultiple(value);
    },
    [onChange, onClickMultiple, handleOnSearchValue],
  );

  const handleSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
    e.stopPropagation();

    handleOnSearchValue(e.target.value);
  };

  const context = useMemo(
    () => ({
      isOpen,
      focus,
      setSelectActive,
      setState,
      onClick: handleClick,
      onClickMultiple: handleClickMultiple,
      state,
    }),
    [state, setState, isOpen, focus, setSelectActive, handleClick, handleClickMultiple],
  );

  const controls = useMemo(() => {
    if (!selectElement) return;

    return {
      onClick: handleClick,
      onClickMultiple: handleClickMultiple,
      state,
      searchValue: searchValue || '',
      selectElement: selectElement,
    };
  }, [handleClick, handleClickMultiple, state, searchValue, selectElement]);

  useEffect(() => {
    if (reset) {
      setState(DEFAULT_VALUE);
    }
  }, [reset, setState]);

  return (
    <TableSelectContext.Provider value={context}>
      <div ref={wrapperRef} className={classNames(style.wrapper, className)}>
        <Tooltip wrapperClassName={style.tooltip} message={isOpen ? '' : tooltip}>
          <div
            role="presentation"
            tabIndex={-1}
            onClick={handleToggle}
            onMouseEnter={() => setHovered(true)}
            onMouseLeave={() => setHovered(false)}
            className={classNames(style.select, style[`border_${border}`], {
              [style.activeRow]: isRowActive,
              [style.focused]: focus,
              [style.disabled]: disabled,
              [style.error]: error,
            })}
          >
            {icon && (
              <Icon
                className={style.icon}
                icon={icon}
                path="inherit"
                cursor={focus ? 'text' : 'pointer'}
                transition="inherit"
              />
            )}
            <div className={classNames(style.field, style[`type_${type}`])}>
              <input
                id={name}
                className={style.input}
                placeholder={placeholder}
                value={searchValue === null ? state?.label : searchValue}
                onChange={handleSearch}
                disabled={disabled}
                autoComplete="off"
              />
              {(type === 'doubleHorizontal' || type == 'doubleVertical') && !isOpen && (
                <Text className={style.secondaryLabel} lineClamp="1" color="grey-100">
                  {state?.secondaryLabel}
                </Text>
              )}
            </div>
            <Icon
              icon={<CaretDownIcon />}
              path="inherit"
              className={classNames(style.arrow, {
                [style.active]: isOpen,
                [style.error]: error,
              })}
              clickThrough
            />
          </div>
        </Tooltip>

        {!!errorMessage && hovered && !focus && wrapperRef.current && (
          <InputErrorPopup inputWrapperElement={wrapperRef.current} errorMessage={errorMessage} />
        )}

        {isOpen && controls && children(controls)}
      </div>
    </TableSelectContext.Provider>
  );
};

export default TableSelect;
