export { default as TableSelect } from './TableSelect';
export type { TableSelectProps, Option } from './TableSelect.types';
