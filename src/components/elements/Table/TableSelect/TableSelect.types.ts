import { BorderColorNewTypes } from 'Components/types/colorTypes';
import { ReactElement } from 'react';

export interface TableSelectContextProps {
  isOpen: boolean;
  state: Option | null;

  onClick(arg: Option): void;

  setSelectActive(arg: boolean): void;

  setState(arg: Option): void;
}

export interface Option {
  value: string;
  label: string;
  secondaryLabel?: string;

  [key: string]: unknown;
}

export interface TableSelectProps {
  type?: 'single' | 'doubleHorizontal' | 'doubleVertical';
  name: string;
  tooltip?: ReactElement | string;
  disabled?: boolean;
  error?: boolean;
  errorMessage?: string;
  placeholder?: string;
  icon?: ReactElement;
  border?: BorderColorNewTypes;
  defaultValue?: Option;
  value?: Option;
  isRowActive?: boolean;
  reset?: boolean;

  onChange?(option: Option): void;

  children(args: {
    onClick(option: Option): void;
    onClickMultiple(option: Option): void;
    state: Option | null;
    searchValue: string;
    selectElement: Element;
  }): ReactElement;

  className?: string;
  onSearch?: (value: string | null) => void;
}
