import React, { useState } from 'react';

export default function usePosition() {
  const [position, setPosition] = useState('bottom-left');

  const setPositionOptions = (e: React.MouseEvent<HTMLElement>) => {
    if (
      e.clientY * 2 >
      (window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight)
    ) {
      setPosition('top-left');
    } else {
      setPosition('bottom-left');
    }
  };

  return {
    position,
    setPosition,
    setPositionOptions,
  };
}
