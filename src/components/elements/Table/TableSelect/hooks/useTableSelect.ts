import { useCallback, useState } from 'react';

import { Option } from '../TableSelect.types';

export default function useTableSelect(defaultValue: Option) {
  const [state, setState] = useState<Option>(defaultValue);
  const [isOpen, setOpen] = useState(false);
  const [focus, setFocus] = useState(false);

  const setSelectActive = (arg: boolean) => {
    setOpen(arg);
    setFocus(arg);
  };

  const handleClick = useCallback((data: Option) => {
    setState(data);
    setSelectActive(false);
  }, []);

  const handleClickMultiple = useCallback((data: Option) => {
    setState(data);
  }, []);

  return {
    isOpen,
    focus,
    setSelectActive,
    state,
    setState,
    onClick: handleClick,
    onClickMultiple: handleClickMultiple,
  };
}
