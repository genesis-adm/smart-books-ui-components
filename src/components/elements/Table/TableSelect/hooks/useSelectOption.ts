import { SelectContext } from 'Components/base/inputs/SelectNew/Select';
import { Option } from 'Components/base/inputs/SelectNew/Select.types';
import { useContext } from 'react';

export default function useSelectOption({ option }: { option: Option }) {
  const { onClick } = useContext(SelectContext);

  const handleClick = () => onClick(option);

  return {
    onClick: handleClick,
  };
}
