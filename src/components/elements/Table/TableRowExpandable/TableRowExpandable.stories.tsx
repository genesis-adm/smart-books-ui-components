import { DropDown } from 'Components/DropDown';
import { Icon } from 'Components/base/Icon';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as LockClosedIcon } from 'Svg/v2/16/lock-closed.svg';
import { ReactComponent as LockOpenedIcon } from 'Svg/v2/16/lock-opened.svg';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import { ReactComponent as DropdownIcon } from 'Svg/v2/16/three-dots.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import React from 'react';

import { TableCellNew } from '../TableCellNew';
import { TableCellTextNew } from '../TableCellTextNew';
import { TableRowExpandable, TableRowExpandableWrapper } from './TableRowExpandable';

export default {
  title: 'Components/Elements/Table/TableRowExpandable',
};

const mainRow = {
  name: 'Account name',
  currency: 'USD',
  isSensitive: true,
  accountClass1: 'Asset',
  accountClass2: 'Current',
  type: 'Cash and cash equivalents',
  detailType: 'Cash in banks',
  description: 'Description text',
  businessDivision: 'Business division name',
  legalEntity: 'Legal entity name',
  onEdit: () => {},
  onDelete: () => {},
};

export const TableRowExpandableDefault: React.FC = () => (
  <TableRowExpandableWrapper isActive>
    <TableRowExpandable
      type="primary"
      isRowVisible
      isToggleActive
      onToggle={() => {}}
    ></TableRowExpandable>
    <TableRowExpandable isRowVisible isToggleActive onToggle={() => {}}></TableRowExpandable>
    <TableRowExpandable isRowVisible>
      <TableCellNew minWidth="220" alignitems="center" gap="8">
        <Icon icon={mainRow.isSensitive ? <LockOpenedIcon /> : <LockClosedIcon />} path="grey-90" />
        <Container flexdirection="column">
          <Text noWrap>{mainRow.name}</Text>
          <Text color="grey-100" noWrap>
            {mainRow.currency}
          </Text>
        </Container>
      </TableCellNew>
      <TableCellTextNew
        minWidth="160"
        value={mainRow.accountClass1}
        secondaryValue={mainRow.accountClass2}
        secondaryColor="grey-100"
      />
      <TableCellTextNew
        minWidth="240"
        value={mainRow.type}
        secondaryValue={mainRow.detailType}
        secondaryColor="grey-100"
      />
      <TableCellTextNew minWidth="190" value={mainRow.description} flexgrow="1" />
      <TableCellTextNew
        minWidth="280"
        value={mainRow.businessDivision}
        secondaryValue={mainRow.legalEntity}
        secondaryColor="grey-100"
      />
      <TableCellNew minWidth="100" justifycontent="center" alignitems="center">
        <DropDown
          flexdirection="column"
          padding="8"
          control={({ handleOpen }) => (
            <IconButton
              icon={<DropdownIcon />}
              onClick={handleOpen}
              background="transparent"
              color="grey-100-violet-90"
            />
          )}
        >
          <DropDownButton size="160" icon={<EditIcon />} onClick={mainRow.onEdit}>
            Edit
          </DropDownButton>
          <DropDownButton size="160" type="danger" icon={<TrashIcon />} onClick={mainRow.onDelete}>
            Delete
          </DropDownButton>
        </DropDown>
      </TableCellNew>
    </TableRowExpandable>
  </TableRowExpandableWrapper>
);
