import { IconButton } from 'Components/base/buttons/IconButton';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { ReactComponent as ChevronIcon } from 'Svg/v2/16/chevron-up.svg';
import classNames from 'classnames';
import React, { ReactNode } from 'react';

import style from './TableRowExpandable.module.scss';

export interface TableRowExpandableProps {
  type?: 'primary' | 'secondary' | 'tertiary';
  isToggleActive?: boolean;
  isRowVisible: boolean;
  className?: string;
  onToggle?(): void;
  onClick?(): void;
  children?: ReactNode;
}

const TableRowExpandable: React.FC<TableRowExpandableProps> = ({
  type = 'secondary',
  isToggleActive,
  isRowVisible,
  className,
  onToggle,
  onClick,
  children,
}) => (
  <div
    className={classNames(
      style.row,
      style[`type_${type}`],
      { [style.visible]: isRowVisible, [style.active]: isToggleActive },
      className,
    )}
    onClick={onClick}
  >
    <TableCellNew
      className={style.cell}
      minWidth="48"
      padding="8"
      justifycontent="center"
      alignitems="center"
    >
      {onToggle && (
        <IconButton
          size="24"
          background="inherit"
          border="grey-20"
          color="black-100"
          className={style.button}
          icon={
            <ChevronIcon
              className={classNames(style.control, { [style.opened]: isToggleActive })}
            />
          }
          onClick={onToggle}
        />
      )}
      {type === 'tertiary' && <span className={style.connector} />}
    </TableCellNew>
    {children}
  </div>
);

interface TableRowExpandableWrapperProps {
  isActive: boolean;
  className?: string;
  children?: ReactNode;
}

const TableRowExpandableWrapper: React.FC<TableRowExpandableWrapperProps> = ({
  isActive,
  className,
  children,
}) => (
  <div className={classNames(style.wrapper, { [style.active]: isActive }, className)}>
    {children}
  </div>
);

export { TableRowExpandable, TableRowExpandableWrapper };
