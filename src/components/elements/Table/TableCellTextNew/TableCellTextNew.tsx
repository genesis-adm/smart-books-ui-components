import { Counter } from 'Components/base/Counter';
import { Icon } from 'Components/base/Icon';
import { Tag } from 'Components/base/Tag';
import { Tooltip } from 'Components/base/Tooltip';
import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React, { useEffect, useRef, useState } from 'react';

import { TestableProps } from '../../../../types/general';
import type { TagColorType } from '../../../base/Tag';
import type {
  BackgroundColorStaticNewTypes,
  TextColorStaticNewTypes,
} from '../../../types/colorTypes';
import type { FontTypes, LineClamp, TextAlign } from '../../../types/fontTypes';
import type {
  AlignContent,
  AlignItems,
  FlexDirection,
  FlexGrow,
  JustifyContent,
  TableCellWidth,
} from '../../../types/gridTypes';
import style from './TableCellTextNew.module.scss';

export interface TableCellTextNewProps extends TestableProps {
  id?: string;
  minWidth: TableCellWidth;
  fixedWidth?: boolean;
  applyFlexBasis?: boolean;
  flexgrow?: FlexGrow;
  flexdirection?: FlexDirection;
  justifycontent?: JustifyContent;
  alignitems?: AlignItems;
  aligncontent?: AlignContent;
  noWrap?: boolean;
  gap?: '4' | '8';
  padding?: '16' | '8' | '12';
  value: string;
  type?: FontTypes;
  align?: TextAlign;
  color?: TextColorStaticNewTypes;
  lineClamp?: LineClamp;
  tooltip?: React.ReactNode;
  counter?: string;
  counterTooltip?: React.ReactNode;
  secondaryValue?: string;
  tagIcon?: React.ReactNode;
  tagText?: string;
  tagColor?: TagColorType;
  tagTooltip?: React.ReactNode;
  secondaryType?: FontTypes;
  secondaryAlign?: TextAlign;
  secondaryColor?: TextColorStaticNewTypes;
  secondaryLineClamp?: LineClamp;
  secondaryTooltip?: React.ReactNode;
  secondaryCounter?: string;
  secondaryCounterTooltip?: React.ReactNode;
  secondaryTagIcon?: React.ReactNode;
  secondaryTagText?: string;
  secondaryTagColor?: TagColorType;
  secondaryTagTooltip?: React.ReactNode;
  iconLeft?: React.ReactNode;
  iconColor?: BackgroundColorStaticNewTypes;
  className?: string;
}

const TableCellTextNew: React.FC<TableCellTextNewProps> = ({
  id,
  minWidth,
  fixedWidth,
  applyFlexBasis,
  flexgrow,
  flexdirection = 'column',
  justifycontent = 'center',
  alignitems,
  aligncontent,
  noWrap,
  gap,
  padding = '16',
  value,
  type = 'text-regular',
  align,
  color = 'black-100',
  lineClamp = '1',
  tooltip,
  counter,
  counterTooltip,
  tagIcon,
  tagText,
  tagColor,
  tagTooltip,
  secondaryValue,
  secondaryType,
  secondaryAlign,
  secondaryColor = 'grey-100',
  secondaryLineClamp = '1',
  secondaryTooltip,
  secondaryCounter,
  secondaryCounterTooltip,
  secondaryTagIcon,
  secondaryTagText,
  secondaryTagColor,
  secondaryTagTooltip,
  iconLeft,
  iconColor,
  className,
  dataTestId,
}) => {
  const textPrimaryRef = useRef<HTMLDivElement>(null);
  const textSecondaryRef = useRef<HTMLDivElement>(null);
  const [tooltipValue, setTooltipValue] = useState<React.ReactNode>(tooltip);
  const [secondaryTooltipValue, setSecondaryTooltipValue] =
    useState<React.ReactNode>(secondaryTooltip);

  useEffect(() => {
    if (
      !tooltip &&
      textPrimaryRef.current &&
      (textPrimaryRef.current.scrollHeight > textPrimaryRef.current.clientHeight ||
        textPrimaryRef.current.scrollWidth > textPrimaryRef.current.clientWidth)
    ) {
      setTooltipValue(value);
    }
  }, [tooltip, value]);
  useEffect(() => {
    if (
      !secondaryTooltip &&
      textSecondaryRef.current &&
      textSecondaryRef.current.scrollHeight > textSecondaryRef.current.clientHeight
    ) {
      setSecondaryTooltipValue(secondaryValue);
    }
  }, [secondaryTooltip, secondaryValue]);

  return (
    <div
      data-testid={dataTestId}
      id={id}
      className={classNames(
        style.cell,
        style[`min-width_${minWidth}`],
        style[`padding_${padding}`],
        {
          [style[`max-width_${minWidth}`]]: fixedWidth,
          [style[`flex-basis_${minWidth}`]]: applyFlexBasis,
          [style[`flex-grow_${flexgrow}`]]: flexgrow,
        },
        className,
      )}
    >
      {!!iconLeft && <Icon path={iconColor} icon={iconLeft} />}

      <div
        className={classNames(style.textWrapper, {
          [style[`flex-direction_${flexdirection}`]]: flexdirection,
          [style[`justify-content_${justifycontent}`]]: justifycontent,
          [style[`align-items_${alignitems}`]]: alignitems,
          [style[`align-content_${aligncontent}`]]: aligncontent,
          [style[`gap_${gap}`]]: gap,
        })}
      >
        <div
          className={classNames(style.primary, {
            [style[`align_${align}`]]: align,
          })}
        >
          <Tooltip message={tooltipValue}>
            <Text
              ref={textPrimaryRef}
              className={style.text}
              lineClamp={lineClamp}
              type={type}
              color={color}
              align={align}
              noWrap={noWrap}
            >
              {value}
            </Text>
          </Tooltip>
          {counter && (
            <Tooltip wrapperClassName={style.counter} message={counterTooltip} cursor="help">
              <Counter value={counter} />
            </Tooltip>
          )}
          {(tagIcon || tagText) && (
            <Tag
              className={style.tag}
              size="16"
              padding="3"
              color={tagColor}
              icon={tagIcon}
              text={tagText}
              tooltip={tagTooltip}
            />
          )}
        </div>
        <div
          className={classNames(style.secondary, {
            [style[`align_${secondaryAlign}`]]: secondaryAlign,
          })}
        >
          {secondaryValue && (
            <Tooltip message={secondaryTooltipValue}>
              <Text
                ref={textSecondaryRef}
                className={style.text}
                lineClamp={secondaryLineClamp}
                type={secondaryType}
                color={secondaryColor}
                align={secondaryAlign}
              >
                {secondaryValue}
              </Text>
            </Tooltip>
          )}
          {secondaryCounter && (
            <Tooltip
              wrapperClassName={style.counter}
              message={secondaryCounterTooltip}
              cursor="help"
            >
              <Counter value={secondaryCounter} />
            </Tooltip>
          )}
          {(secondaryTagIcon || secondaryTagText) && (
            <Tag
              className={classNames({ [style.tag]: secondaryValue })}
              size="16"
              padding="2-8"
              color={secondaryTagColor}
              icon={secondaryTagIcon}
              text={secondaryTagText}
              tooltip={secondaryTagTooltip}
            />
          )}
        </div>
      </div>
    </div>
  );
};

export default TableCellTextNew;
