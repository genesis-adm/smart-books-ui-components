import { TextareaNew, TextareaNewProps } from 'Components/base/inputs/TextareaNew';
import React, { ReactElement } from 'react';

const TableTextarea = ({
  name,
  value,
  error,
  errorMessage,
  className,
  onChange,
}: TextareaNewProps): ReactElement => (
  <TextareaNew
    className={className}
    style={{ paddingLeft: '0' }}
    name={name}
    label={''}
    value={value}
    onChange={onChange}
    error={error}
    errorMessage={errorMessage}
    background="transparent"
    border="transparent"
    hideCounter
  />
);

export default TableTextarea;
