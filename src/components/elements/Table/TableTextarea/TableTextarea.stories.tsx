import { Meta, Story } from '@storybook/react';
import { TextareaNewProps } from 'Components/base/inputs/TextareaNew';
import { TableTextarea as Component } from 'Components/elements/Table/TableTextarea';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Elements/Table/TableTextarea',
  component: Component,
  argTypes: {
    label: hideProperty,
    placeholder: hideProperty,
    background: hideProperty,
    color: hideProperty,
    border: hideProperty,
    align: hideProperty,
    required: hideProperty,
    disabled: hideProperty,
    readOnly: hideProperty,
    active: hideProperty,
    errorsList: hideProperty,
    maxLength: hideProperty,
    minHeight: hideProperty,
    tooltip: hideProperty,
    description: hideProperty,
    hideCounter: hideProperty,
    isLoading: hideProperty,
    style: hideProperty,
    value: hideProperty,
    className: hideProperty,
    onChange: hideProperty,
  },
} as Meta;

const TableTextareaComponent: Story<TextareaNewProps> = (args) => <Component {...args} />;

export const TableTextarea = TableTextareaComponent.bind({});
TableTextarea.args = {
  error: false,
  errorMessage: 'This is table textarea error message',
  name: 'table-textarea-sb',
};
