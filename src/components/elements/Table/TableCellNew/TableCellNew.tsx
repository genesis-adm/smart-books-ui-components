import classNames from 'classnames';
import React from 'react';

import { TestableProps } from '../../../../types/general';
import type {
  AlignContent,
  AlignItems,
  FlexDirection,
  FlexGrow,
  JustifyContent,
  TableCellWidth,
} from '../../../types/gridTypes';
import style from './TableCellNew.module.scss';

export interface TableCellNewProps
  extends TestableProps,
    React.DetailedHTMLProps<React.HtmlHTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  minWidth: TableCellWidth;
  fixedWidth?: boolean;
  applyFlexBasis?: boolean;
  flexgrow?: FlexGrow;
  padding?: '0' | '8' | '12' | '16' | '8-0' | '12-0' | '16-0';
  flexdirection?: FlexDirection;
  justifycontent?: JustifyContent;
  alignitems?: AlignItems;
  aligncontent?: AlignContent;
  gap?: '4' | '8' | '12' | '20' | '80';
  className?: string;
  children?: React.ReactNode;
}

const TableCellNew: React.FC<TableCellNewProps> = ({
  minWidth,
  fixedWidth,
  applyFlexBasis,
  flexgrow,
  padding = '16',
  flexdirection,
  justifycontent,
  alignitems,
  aligncontent,
  gap,
  children,
  className,
  dataTestId,
  ...restProps
}) => (
  <div
    {...restProps}
    data-testid={dataTestId}
    className={classNames(
      style.cell,
      style[`min-width_${minWidth}`],
      style[`flex-grow_${flexgrow}`],
      style[`padding_${padding}`],
      {
        [style[`max-width_${minWidth}`]]: fixedWidth,
        [style[`flex-basis_${minWidth}`]]: applyFlexBasis,
        [style[`flex-direction_${flexdirection}`]]: flexdirection,
        [style[`justify-content_${justifycontent}`]]: justifycontent,
        [style[`align-items_${alignitems}`]]: alignitems,
        [style[`align-content_${aligncontent}`]]: aligncontent,
        [style[`gap_${gap}`]]: gap,
      },
      className,
    )}
  >
    {children}
  </div>
);

export default TableCellNew;
