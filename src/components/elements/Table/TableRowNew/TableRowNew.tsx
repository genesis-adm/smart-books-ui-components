import { IconButton } from 'Components/base/buttons/IconButton';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import InputErrorPopup from 'Components/base/inputs/InputErrorPopup/InputErrorPopup';
import { ReactComponent as CaretIcon } from 'Svg/v2/16/caret-down.svg';
import classNames from 'classnames';
import React, { useRef, useState } from 'react';

import { useToggle } from '../../../../hooks/useToggle';
import style from './TableRowNew.module.scss';

export interface TableRowNewProps
  extends React.DetailedHTMLProps<React.HtmlHTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  size?: '28' | '40' | '36' | '52' | '60' | 'auto' | 'auto-104';
  background?:
    | 'mixed'
    | 'white'
    | 'focusActive'
    | 'focusActiveOutlined'
    | 'focusDisabled'
    | 'blue-10'
    | 'grey-10'
    | 'transparent'
    | 'transparent-with-border'
    | 'info';
  cursor?: 'auto' | 'pointer';
  gap?: '8';
  active?: boolean;
  isError?: boolean;
  errorMessage?: string;
  showCollapseBtn?: boolean;
  toggleStatus?: boolean;
  className?: string;
  onClick?(event: React.MouseEvent<HTMLDivElement>): void;
  children?: React.ReactNode;
}

const TableRowNew = React.forwardRef<HTMLDivElement, TableRowNewProps>(
  (
    {
      size = '60',
      background = 'grey-10',
      cursor = 'pointer',
      gap,
      showCollapseBtn,
      toggleStatus,
      className,
      active,
      isError,
      errorMessage,
      onClick,
      children,
      ...restProps
    },
    ref,
  ) => {
    const wrapperRef = useRef<HTMLDivElement>(null);

    const [hovered, setHovered] = useState(false);

    const { isOpen, onToggle } = useToggle(toggleStatus);
    const handleCollapseBtnClick = (e: React.MouseEvent<HTMLDivElement>) => {
      e.stopPropagation();
      onToggle();
    };

    const handleCollapseShowClick = (e: React.MouseEvent<HTMLButtonElement>) => {
      e.stopPropagation();
      onToggle();
    };

    const handleMouseEnter = () => {
      setHovered(true);
    };
    const handleMouseLeave = () => {
      setHovered(false);
    };

    return (
      <div
        {...restProps}
        ref={ref || wrapperRef}
        className={classNames(
          style.row,
          style[`size_${size}`],
          { [style[`background_${background}`]]: !isError },
          style[`cursor_${cursor}`],
          {
            [style[`gap_${gap}`]]: gap,
            [style.shownAll]: isOpen || toggleStatus,
            [style.active]: active,
            [style.error]: isError,
            [style.overflowY]:
              (showCollapseBtn || !toggleStatus) && (size === 'auto' || size === 'auto-104'),
          },
          // style[`gap_${gap}`],
          className,
        )}
        role="presentation"
        onClick={onClick}
        onMouseEnter={handleMouseEnter}
        onMouseLeave={handleMouseLeave}
      >
        {children}
        {showCollapseBtn && size === 'auto' && (
          <IconButton
            size="24"
            background={'transparent-blue-10'}
            color="grey-100-violet-90"
            icon={<CaretIcon />}
            onClick={handleCollapseBtnClick}
            className={classNames(style.collapseBtnWrapper, {
              [style.opened]: isOpen,
            })}
          />
        )}
        {showCollapseBtn && size === 'auto-104' && (
          <LinkButton
            className={classNames(style.collapseLinkWrapper, { [style.opened]: isOpen })}
            icon={<CaretIcon />}
            iconRight
            type="subtext-medium"
            onClick={handleCollapseShowClick}
          >
            {isOpen ? 'Show less' : 'Show more'}
          </LinkButton>
        )}
        {isError && errorMessage && hovered && wrapperRef.current && (
          <InputErrorPopup inputWrapperElement={wrapperRef.current} errorMessage={errorMessage} />
        )}
      </div>
    );
  },
);

TableRowNew.displayName = 'TableRowNew';

export default TableRowNew;
