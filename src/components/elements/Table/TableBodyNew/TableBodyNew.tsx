import classNames from 'classnames';
import React from 'react';

import style from './TableBodyNew.module.scss';

export interface TableBodyNewProps {
  className?: string;
  children?: React.ReactNode;
}

const TableBodyNew: React.FC<TableBodyNewProps> = ({ className, children }) => (
  <div className={classNames(style.component, className)}>{children}</div>
);

export default TableBodyNew;
