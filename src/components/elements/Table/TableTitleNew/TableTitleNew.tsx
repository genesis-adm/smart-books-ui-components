import { Icon } from 'Components/base/Icon';
import { Tooltip } from 'Components/base/Tooltip';
import { TooltipIcon } from 'Components/base/TooltipIcon';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as SortIcon } from 'Svg/v2/10/sort.svg';
import { ReactComponent as QuestionIcon } from 'Svg/v2/16/question.svg';
import classNames from 'classnames';
import React, { ReactNode } from 'react';

import { FontTypes } from '../../../types/fontTypes';
import type { FlexGrow, TableCellWidth } from '../../../types/gridTypes';
import style from './TableTitleNew.module.scss';

export interface SortingType {
  sortType: 'ascending' | 'descending' | null;
  tooltipText: string;
}

export interface TableTitleNewProps {
  id?: string;
  title?: ReactNode;
  secondaryTitle?: ReactNode;
  sorting?: SortingType;
  minWidth: TableCellWidth;
  fixedWidth?: boolean;
  applyFlexBasis?: boolean;
  padding?: 'default' | 'none' | '0';
  background?: 'white-100' | 'grey-10';
  radius?: '0' | '10';
  font?: FontTypes;
  flexgrow?: FlexGrow;
  align?: 'left' | 'center' | 'right';
  icon?: ReactNode;
  iconRight?: ReactNode;
  helpText?: ReactNode;
  required?: boolean;
  className?: string;
  onClick?(): void;
}

const TableTitleNew: React.FC<TableTitleNewProps> = ({
  id,
  title,
  secondaryTitle,
  sorting,
  minWidth,
  fixedWidth,
  applyFlexBasis,
  padding = 'default',
  background = 'white-100',
  font = 'text-regular',
  radius,
  flexgrow,
  align,
  icon,
  iconRight,
  helpText,
  required,
  className,
  onClick,
}) => {
  const handleClick = () => {
    sorting && onClick?.();
  };
  const sortingActive = sorting?.sortType === 'ascending' || sorting?.sortType === 'descending';
  return (
    <>
      <div className={style.divider} />
      <span
        id={id}
        className={classNames(
          style.wrapper,
          style[`min-width_${minWidth}`],
          style[`flex-grow_${flexgrow}`],
          style[`padding_${padding}`],
          style[`background_${background}`],
          {
            [style[`radius_${radius}`]]: radius,
            [style[`max-width_${minWidth}`]]: fixedWidth,
            [style[`flex-basis_${minWidth}`]]: applyFlexBasis,
            [style[`align_${align}`]]: align,
          },
          className,
        )}
      >
        <Tooltip
          message={sorting?.tooltipText || ''}
          wrapperClassName={classNames(style.content, {
            [style.sorting]: !!sorting,
            [style.sortingActive]: sortingActive,
          })}
          onClick={handleClick}
        >
          {!!title && typeof title === 'string' && (
            <>
              {icon && <Icon icon={icon} className={style.title_icon} />}
              <Text
                className={style.title}
                lineClamp="1"
                type={font}
                color="inherit"
                transition="unset"
                align={align}
              >
                {title}
                {secondaryTitle && (
                  <Text type="inherit" color="grey-90">
                    &nbsp;{secondaryTitle}
                  </Text>
                )}
                {!!title && required && (
                  <Text type="inherit" display="inline-flex" color="red-90">
                    *
                  </Text>
                )}
              </Text>
            </>
          )}
          {!!title && typeof title !== 'string' && (
            <span className={style.title}>
              {title}
              {!!title && required && (
                <Text type={font} color="red-90">
                  *
                </Text>
              )}
            </span>
          )}
          {!!sorting && (
            <Icon
              className={classNames(style.sorting_icon, style[`sorting_${sorting.sortType}`])}
              icon={<SortIcon />}
              path="inherit"
              transition="inherit"
            />
          )}
        </Tooltip>

        {iconRight && iconRight}

        {!!helpText && (
          <TooltipIcon
            background={'grey-10'}
            color="grey-100"
            icon={<QuestionIcon />}
            size="16"
            message={helpText}
          />
        )}
      </span>
    </>
  );
};

export default TableTitleNew;
