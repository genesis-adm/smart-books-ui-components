import { Icon } from 'Components/base/Icon';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as TableEmpty124Icon } from 'Svg/v2/124/empty-table.svg';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

export interface TableNoResultsNewProps {
  className?: string;
}

const TableNoResultsNew: React.FC<TableNoResultsNewProps> = ({ className }) => (
  <Container
    flexdirection="column"
    alignitems="center"
    justifycontent="center"
    gap="16"
    className={classNames(spacing.w100p, spacing.h100p, className)}
  >
    <Icon icon={<TableEmpty124Icon />} />
    <Text type="title-bold">Ooops...</Text>
    <Text type="body-regular-16" color="grey-100">
      Nothing found for your request
    </Text>
  </Container>
);

export default TableNoResultsNew;
