import { TableNoResultsNew } from 'Components/elements/Table/TableNoResultsNew';
import classNames from 'classnames';
import React from 'react';

import styles from './TableNew.module.scss';

export interface TableNewProps {
  tableHead?: React.ReactNode;
  noResults?: boolean;
  className?: string;
  children?: React.ReactNode;
}

const TableNew: React.FC<TableNewProps> = ({ tableHead, noResults, className, children }) => (
  <div className={classNames(styles.container, className)}>
    {noResults ? (
      <TableNoResultsNew />
    ) : (
      <>
        {tableHead}
        {children}
      </>
    )}
  </div>
);

export default TableNew;
