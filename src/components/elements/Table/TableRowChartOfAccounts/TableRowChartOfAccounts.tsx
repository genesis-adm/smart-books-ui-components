import { DropDown } from 'Components/DropDown';
import { Icon } from 'Components/base/Icon';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { ReactComponent as ChevronIcon } from 'Svg/v2/16/chevron-up.svg';
import { ReactComponent as LockClosedIcon } from 'Svg/v2/16/lock-closed.svg';
import { ReactComponent as LockOpenedIcon } from 'Svg/v2/16/lock-opened.svg';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import { ReactComponent as DropdownIcon } from 'Svg/v2/16/three-dots.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import classNames from 'classnames';
import React, { useEffect, useState } from 'react';

import style from './TableRowChartOfAccounts.module.scss';

export interface AccountRowType {
  name: string;
  currency: string;
  isSensitive: boolean;
  accountClass1: string;
  accountClass2: string;
  type: string;
  detailType: string;
  description: string;
  businessDivision: string;
  legalEntity: string;
  onEdit(): void;
  onDelete(): void;
}

export interface TableRowChartOfAccountsProps {
  rowData: AccountRowType[];
  isClosedAll?: boolean;
  className?: string;
}

const TableRowChartOfAccounts: React.FC<TableRowChartOfAccountsProps> = ({
  rowData,
  isClosedAll = false,
  className,
}) => {
  const [mainRow, ...restRows] = rowData;
  const [isRowOpen, setIsRowOpen] = useState<boolean>(false);
  useEffect(() => {
    if (isClosedAll) setIsRowOpen(false);
  }, [isClosedAll]);
  return (
    <div className={classNames(style.wrapper, className)}>
      <div className={style.main}>
        <TableCellNew minWidth="48" padding="8" justifycontent="center" alignitems="center">
          <IconButton
            size="24"
            background="transparent"
            border="grey-20"
            color="black-100"
            icon={
              <ChevronIcon className={classNames(style.control, { [style.opened]: isRowOpen })} />
            }
            onClick={() => setIsRowOpen((prev) => !prev)}
          />
        </TableCellNew>
        <TableCellNew minWidth="220" alignitems="center" gap="8">
          <Icon
            icon={mainRow.isSensitive ? <LockOpenedIcon /> : <LockClosedIcon />}
            path="grey-90"
          />
          <Container flexdirection="column">
            <Text noWrap>{mainRow.name}</Text>
            <Text color="grey-100" noWrap>
              {mainRow.currency}
            </Text>
          </Container>
        </TableCellNew>
        <TableCellTextNew
          minWidth="160"
          value={mainRow.accountClass1}
          secondaryValue={mainRow.accountClass2}
          secondaryColor="grey-100"
        />
        <TableCellTextNew
          minWidth="240"
          value={mainRow.type}
          secondaryValue={mainRow.detailType}
          secondaryColor="grey-100"
        />
        <TableCellTextNew minWidth="190" value={mainRow.description} flexgrow="1" />
        <TableCellTextNew
          minWidth="280"
          value={mainRow.businessDivision}
          secondaryValue={mainRow.legalEntity}
          secondaryColor="grey-100"
        />
        <TableCellNew minWidth="100" justifycontent="center" alignitems="center">
          <DropDown
            flexdirection="column"
            padding="8"
            control={({ handleOpen }) => (
              <IconButton
                icon={<DropdownIcon />}
                onClick={handleOpen}
                background="transparent"
                color="grey-100-violet-90"
              />
            )}
          >
            <DropDownButton size="160" icon={<EditIcon />} onClick={mainRow.onEdit}>
              Edit
            </DropDownButton>
            <DropDownButton
              size="160"
              type="danger"
              icon={<TrashIcon />}
              onClick={mainRow.onDelete}
            >
              Delete
            </DropDownButton>
          </DropDown>
        </TableCellNew>
      </div>
      {restRows && (
        <div
          style={{ maxHeight: !isRowOpen ? '0px' : `${restRows.length * 60}px` }}
          className={classNames(style.rest, { [style.visible]: isRowOpen })}
        >
          {restRows.map((item, index) => (
            <div key={index} className={style.secondaryRow}>
              <TableCellNew minWidth="48" padding="8" />
              <TableCellNew minWidth="220" alignitems="center" gap="8">
                <Icon
                  icon={item.isSensitive ? <LockOpenedIcon /> : <LockClosedIcon />}
                  path="grey-90"
                />
                <Container flexdirection="column">
                  <Text noWrap>{item.name}</Text>
                  <Text color="grey-100" noWrap>
                    {item.currency}
                  </Text>
                </Container>
              </TableCellNew>
              <TableCellTextNew
                minWidth="160"
                value={item.accountClass1}
                secondaryValue={item.accountClass2}
                secondaryColor="grey-100"
              />
              <TableCellTextNew
                minWidth="240"
                value={item.type}
                secondaryValue={item.detailType}
                secondaryColor="grey-100"
              />
              <TableCellTextNew minWidth="190" value={item.description} flexgrow="1" />
              <TableCellTextNew
                minWidth="280"
                value={item.businessDivision}
                secondaryValue={item.legalEntity}
                secondaryColor="grey-100"
              />
              <TableCellNew minWidth="100" justifycontent="center" alignitems="center">
                <DropDown
                  flexdirection="column"
                  padding="8"
                  control={({ handleOpen }) => (
                    <IconButton
                      icon={<DropdownIcon />}
                      onClick={handleOpen}
                      background="transparent"
                      color="grey-100-violet-90"
                    />
                  )}
                >
                  <DropDownButton size="160" icon={<EditIcon />} onClick={item.onEdit}>
                    Edit
                  </DropDownButton>
                  <DropDownButton
                    size="160"
                    type="danger"
                    icon={<TrashIcon />}
                    onClick={item.onDelete}
                  >
                    Delete
                  </DropDownButton>
                </DropDown>
              </TableCellNew>
            </div>
          ))}
        </div>
      )}
    </div>
  );
};

export default TableRowChartOfAccounts;
