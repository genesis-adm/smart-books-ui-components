import classNames from 'classnames';
import React, { CSSProperties } from 'react';

import { TestableProps } from '../../../../types/general';
import styles from './TableHeadNew.module.scss';

export interface TableHeadNewProps extends TestableProps {
  radius?: '10' | '0';
  background?: 'white-100' | 'grey-10' | 'transparent';
  margin?: 'default' | 'none';
  style?: CSSProperties;
  className?: string;
  children?: React.ReactNode;
}

const TableHeadNew: React.FC<TableHeadNewProps> = ({
  radius = '0',
  background = 'white-100',
  margin = 'default',
  style,
  className,
  children,
  dataTestId,
}) => (
  <div
    style={style}
    className={classNames(
      styles.header,
      styles[`radius_${radius}`],
      styles[`margin_${margin}`],
      styles[`background_${background}`],
      className,
    )}
    data-testid={dataTestId}
  >
    {children}
  </div>
);

export default TableHeadNew;
