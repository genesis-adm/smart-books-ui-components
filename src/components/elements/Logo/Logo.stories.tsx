import { Meta, Story } from '@storybook/react';
import { Logo as Component, LogoProps as Props } from 'Components/elements/Logo';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Elements/Logo',
  component: Component,
  argTypes: {
    className: hideProperty,
    onClick: hideProperty,
  },
} as Meta;

const LogoComponent: Story<Props> = (args) => <Component {...args} />;

export const Logo = LogoComponent.bind({});
Logo.args = {
  size: 'sm',
  content: 'user',
  name: 'Karine Mnatsakanian',
  img: 'https://picsum.photos/100',
  border: 'none',
  // color: '#333333',
};
