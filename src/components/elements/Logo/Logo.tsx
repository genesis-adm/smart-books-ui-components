import { Counter } from 'Components/base/Counter';
import { Icon } from 'Components/base/Icon';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as GalleryIcon } from 'Svg/v2/24/gallery.svg';
import hexToRGB from 'Utils/hexToRGB';
import initials from 'Utils/initials';
import classNames from 'classnames';
import React, { useMemo } from 'react';

import style from './Logo.module.scss';

export interface LogoProps {
  size?: 'xs' | '24' | '32' | '44' | 'sm' | 'md' | 'lg' | 'xl';
  border?: 'white' | 'dark' | 'light' | 'none';
  radius?: 'default' | 'rounded';
  content: 'user' | 'company' | 'picture';
  counter?: string;
  name: string;
  text?: string;
  color?: string | undefined;
  img?: string | null | undefined;
  className?: string;
  onClick?(): void;
}

const Logo: React.FC<LogoProps> = ({
  size = 'sm',
  border = 'light',
  radius = 'default',
  content,
  counter,
  name,
  text,
  color,
  img,
  className,
  onClick,
}) => {
  const defaultColor = '#6367F6';
  const actualColor = color || defaultColor;
  const bgColor = useMemo(
    () => (content === 'company' && !img ? { background: hexToRGB(actualColor, '.12') } : {}),
    [content, img, actualColor],
  );
  return (
    <div
      className={classNames(
        style.component,
        style[`size_${size}`],
        style[`radius_${radius}`],
        className,
      )}
    >
      <div
        role="presentation"
        style={bgColor}
        className={classNames(
          style.container,
          style[`border_${border}`],
          style[`radius_${radius}`],
          {
            [style.avatar]: content === 'user',
            [style.wrapper]: content === 'company' || content === 'picture',
          },
        )}
        onClick={onClick}
      >
        {content === 'user' && (
          <Text
            type={
              size === 'lg' || size === 'xl'
                ? 'avatar-big'
                : size === 'md'
                ? 'body-medium'
                : 'avatar-small'
            }
            color="violet-90"
          >
            {initials(name) || text}
          </Text>
        )}
        {(content === 'company' || content === 'picture') && img && (
          <img className={style.wrapper_img} src={img} alt={name} draggable={false} />
        )}
        {content === 'picture' && !img && (
          <Icon icon={<GalleryIcon />} />
          // <img className={style.no_img} src={photograph} alt={name} draggable={false} />
        )}
        {content === 'company' && !img && (
          <h6
            className={classNames(style.wrapper_letter, {
              [style.letter_medium]: size !== 'xs' && size !== 'xl',
              [style.letter_small]: size === 'xs',
              [style.letter_big]: size === 'xl',
              [style.letter_text]: text,
            })}
            style={{ color: actualColor }}
          >
            {(name && name.charAt(0)) || text}
          </h6>
        )}
      </div>
      {counter && <Counter className={style.counter} value={counter} />}
    </div>
  );
};

export default Logo;
