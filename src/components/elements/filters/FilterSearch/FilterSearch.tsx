import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { InputNew } from 'Components/base/inputs/InputNew';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as ClearIcon } from 'Svg/v2/16/cross.svg';
import { ReactComponent as SearchIcon } from 'Svg/v2/16/search.svg';
import classNames from 'classnames';
import React, { useEffect, useRef, useState } from 'react';

import { InputWidthType } from '../../../../types/inputs';
import style from './FilterSearch.module.scss';

export interface FilterSearchProps {
  title: string;
  value: string;
  inputWidth?: InputWidthType;
  className?: string;

  onChange(val: string): void;
}

const FilterSearch: React.FC<FilterSearchProps> = ({
  title,
  value,
  inputWidth = '248',
  className,
  onChange,
}) => {
  const [open, setOpen] = useState(false);
  const inputRef = useRef<HTMLInputElement>(null);

  const handleChange = ({ target }: React.ChangeEvent<HTMLInputElement>) => {
    onChange(target.value);
  };
  const handleOpen = () => setOpen(true);
  const handleClose = () => {
    setOpen(false);
    onChange('');
  };

  useEffect(() => {
    if (inputRef.current && open) {
      inputRef.current.focus();
    }
  }, [open]);

  return (
    <Container
      className={classNames(style.container, className)}
      justifycontent="space-between"
      alignitems="center"
    >
      {!open && (
        <>
          <Text type="text-medium" color="grey-100">
            {title}
          </Text>
          <IconButton
            size="32"
            icon={<SearchIcon />}
            onClick={handleOpen}
            background="blue-10"
            color="violet-90-violet-100"
          />
        </>
      )}
      {open && (
        <>
          <InputNew
            ref={inputRef}
            label="Search"
            height="36"
            icon={<SearchIcon />}
            elementPosition="left"
            width={inputWidth}
            name="filtersearch"
            value={value}
            onChange={handleChange}
          />
          <IconButton
            size="32"
            icon={<ClearIcon />}
            onClick={handleClose}
            background="blue-10"
            color="violet-90-violet-100"
          />
        </>
      )}
    </Container>
  );
};

export default FilterSearch;
