import { Meta, Story } from '@storybook/react';
import {
  FiltersWrapper as Component,
  FiltersWrapperProps as Props,
} from 'Components/elements/filters/FiltersWrapper';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Elements/Filters/FiltersWrapper',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const FiltersWrapperComponent: Story<Props> = (args) => <Component {...args} />;

export const FiltersWrapper = FiltersWrapperComponent.bind({});
FiltersWrapper.args = {};
