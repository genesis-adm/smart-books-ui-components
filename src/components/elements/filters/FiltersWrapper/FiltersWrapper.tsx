import classNames from 'classnames';
import React from 'react';

import style from './FiltersWrapper.module.scss';

export interface FiltersWrapperProps {
  active: boolean;
  margins?: 'default' | 'none';
  className?: string;
  children?: React.ReactNode;
}

const FiltersWrapper: React.FC<FiltersWrapperProps> = ({
  active,
  margins = 'default',
  className,
  children,
}) => (
  <div
    className={classNames(
      style.component,
      style[`margins_${margins}`],
      { [style.active]: active },
      className,
    )}
  >
    {children}
  </div>
);

export default FiltersWrapper;
