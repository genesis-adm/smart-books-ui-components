import { Meta, Story } from '@storybook/react';
import {
  FilterToggleButton as Component,
  FilterToggleButtonProps as Props,
} from 'Components/elements/filters/FilterToggleButton';
import React, { useState } from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Elements/Filters/FilterToggleButton',
  component: Component,
  argTypes: {
    showFilters: hideProperty,
    className: hideProperty,
    onClick: hideProperty,
    onClear: hideProperty,
  },
} as Meta;

const FilterToggleButtonComponent: Story<Props> = ({ showFilters, onClick, onClear, ...args }) => {
  const [filterActive, setFilterActive] = useState(false);
  return (
    <Component
      showFilters={filterActive}
      onClick={() => {
        setFilterActive((prevState) => !prevState);
      }}
      onClear={() => setFilterActive(false)}
      {...args}
    />
  );
};

export const FilterToggleButton = FilterToggleButtonComponent.bind({});
FilterToggleButton.args = {};
