import { Icon } from 'Components/base/Icon';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as CaretDownIcon } from 'Svg/v2/16/caret-down.svg';
import { ReactComponent as ClearIcon } from 'Svg/v2/16/cross.svg';
import { ReactComponent as FilterIcon } from 'Svg/v2/16/filter.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import style from './FilterToggleButton.module.scss';

export interface FilterToggleButtonProps {
  showFilters: boolean;
  filtersSelected: boolean;
  className?: string;
  onClick(): void;
  onClear(): void;
}

const FilterToggleButton: React.FC<FilterToggleButtonProps> = ({
  showFilters,
  filtersSelected,
  className,
  onClick,
  onClear,
}) => {
  const [clearHover, setClearHover] = useState(false);
  const clearAddHandler = () => {
    setClearHover(true);
  };
  const clearRemoveHandler = () => {
    setClearHover(false);
  };
  const handleClear = (e: React.MouseEvent<HTMLDivElement>) => {
    e.stopPropagation();
    clearRemoveHandler();
    onClear();
  };
  return (
    <div
      role="presentation"
      className={classNames(
        style.button,
        {
          [style.active]: showFilters,
          [style.showReset]: filtersSelected,
          [style.clear]: clearHover,
        },
        className,
      )}
      onClick={onClick}
    >
      <Icon className={style.filter} icon={<FilterIcon />} path="inherit" clickThrough />
      <Text className={style.text} type="body-regular-14" color="inherit" transition="unset">
        Filters
      </Text>
      <Icon className={style.arrow} icon={<CaretDownIcon />} path="inherit" clickThrough />
      <span className={style.divider} />
      <div
        className={style.reset}
        onClick={handleClear}
        onMouseOver={clearAddHandler}
        onMouseOut={clearRemoveHandler}
      >
        <ClearIcon className={style.cross} />
      </div>
    </div>
  );
};

export default FilterToggleButton;
