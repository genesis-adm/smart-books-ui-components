import { Meta, Story } from '@storybook/react';
import { Divider } from 'Components/base/Divider';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import {
  FilterDropDown as Component,
  FilterDropDownProps as Props,
} from 'Components/elements/filters/FilterDropDown';
import { FilterDropDownItem } from 'Components/elements/filters/FilterDropDownItem';
import { FilterItemsContainer } from 'Components/elements/filters/FilterItemsContainer';
import { FilterSearch } from 'Components/elements/filters/FilterSearch';
import { suggestions } from 'Mocks/fakeOptions';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Elements/Filters/Filter DropDown',
  component: Component,
  argTypes: {
    className: hideProperty,
    onClear: hideProperty,
  },
} as Meta;

const FilterDropDownComponent: Story<Props> = (args) => {
  const [searchValue, setSearchValue] = useState('');
  const filteredItems = suggestions.filter((item) => item.bankname.includes(searchValue));
  const handleChange = (value: string) => {
    setSearchValue(value);
  };

  return (
    <Component {...args}>
      <Container className={classNames(spacing.m16, spacing.w288fixed)} flexdirection="column">
        <FilterSearch title="Account type:" value={searchValue} onChange={handleChange} />
        <FilterItemsContainer className={spacing.mt8}>
          {filteredItems.map((item) => (
            <FilterDropDownItem
              key={item.id}
              id={item.id}
              type="item"
              checked={false}
              blurred={false}
              value={item.bankname}
              onChange={() => {}}
            />
          ))}
        </FilterItemsContainer>
      </Container>
      <Divider fullHorizontalWidth />
      <Container
        className={classNames(spacing.m16, spacing.w288fixed)}
        justifycontent="space-between"
        alignitems="center"
        flexdirection="row-reverse"
      >
        <Button
          width="auto"
          height="32"
          padding="8"
          font="text-medium"
          onClick={() => {}}
          background="violet-90-violet-100"
          color="white-100"
        >
          Apply
        </Button>
        <Button
          width="auto"
          height="32"
          padding="8"
          font="text-medium"
          onClick={() => {}}
          background="blue-10-red-10"
          color="violet-90-red-90"
        >
          Clear all
        </Button>
      </Container>
    </Component>
  );
};

export const FilterDropDown = FilterDropDownComponent.bind({});
FilterDropDown.args = {
  title: 'Filter title',
  value: 'Selected',
  selectedAmount: 3,
};
