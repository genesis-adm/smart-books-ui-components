import { DropDown } from 'Components/DropDown';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as CaretDownIcon } from 'Svg/v2/16/caret-down.svg';
import { ReactComponent as ClearIcon } from 'Svg/v2/16/cross.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import style from './FilterDropDown.module.scss';

export interface FilterDropDownProps {
  title: string;
  value: string;
  selectedAmount: number;
  background?: 'grey-10' | 'white-100';
  className?: string;
  children?: React.ReactNode;
  onClear(): void;
}

const FilterDropDown: React.FC<FilterDropDownProps> = ({
  title,
  value,
  selectedAmount,
  background = 'grey-10',
  className,
  children,
  onClear,
}) => {
  const [clearHover, setClearHover] = useState(false);
  const clearAddHandler = () => {
    setClearHover(true);
  };
  const clearRemoveHandler = () => {
    setClearHover(false);
  };
  const handleClear = (e: React.MouseEvent<HTMLDivElement>) => {
    e.stopPropagation();
    clearRemoveHandler();
    onClear();
  };

  return (
    <DropDown
      flexdirection="column"
      padding="none"
      control={({ handleOpen, isOpen }) => {
        const handleShowDropdown = (e: React.MouseEvent<HTMLElement>): void => {
          e.stopPropagation();
          handleOpen();
        };

        return (
          <div className={classNames(style.wrapper, className)}>
            <Text className={style.title} type="text-regular" color="grey-100">
              {title}
            </Text>
            <div
              role="presentation"
              className={classNames(style.control, style[`background_${background}`], {
                [style.active]: selectedAmount >= 1,
                [style.remove]: clearHover,
              })}
              onClick={handleShowDropdown}
            >
              <Text className={style.value} type="text-regular" color="inherit">
                {selectedAmount === 0 ? 'All' : value}
              </Text>
              {selectedAmount >= 2 && (
                <Text className={style.multiselect} type="text-regular" color="inherit">
                  &nbsp;+
                  {selectedAmount - 1}
                </Text>
              )}
              <span className={style.divider} />
              {selectedAmount < 1 && (
                <IconButton
                  className={classNames(style.button, { [style.rotate]: isOpen })}
                  size="32"
                  icon={<CaretDownIcon />}
                  onClick={handleShowDropdown}
                  background="inherit"
                  color="inherit"
                />
              )}
              {selectedAmount >= 1 && (
                <IconButton
                  size="32"
                  icon={<ClearIcon />}
                  onClick={handleClear}
                  onMouseOver={clearAddHandler}
                  onMouseOut={clearRemoveHandler}
                  background="inherit"
                  color="inherit"
                />
              )}
            </div>
          </div>
        );
      }}
    >
      {children}
    </DropDown>
  );
};

export default FilterDropDown;
