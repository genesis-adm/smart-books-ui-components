import { FilterEmpty } from 'Components/elements/filters/FilterEmpty';
import classNames from 'classnames';
import React, { Children } from 'react';

import style from './FilterItemsContainer.module.scss';

export interface FilterItemsContainerProps {
  maxHeight?: '200' | '256' | '344' | 'none';
  gap?: '0' | '4';
  children?: React.ReactNode;
  className?: string;
}

const FilterItemsContainer: React.FC<FilterItemsContainerProps> = ({
  maxHeight = '256',
  gap = '4',
  className,
  children,
}) => (
  <ul
    className={classNames(
      style.component,
      style[`maxHeight_${maxHeight}`],
      style[`gap_${gap}`],
      className,
    )}
  >
    {Children.toArray(children).length === 0 ? <FilterEmpty /> : children}
  </ul>
);

export default FilterItemsContainer;
