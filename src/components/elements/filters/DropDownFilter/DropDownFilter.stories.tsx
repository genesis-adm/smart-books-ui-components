import { Meta, Story } from '@storybook/react';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import {
  DropDownFilter as Component,
  DropDownFilterProps as Props,
} from 'Components/elements/filters/DropDownFilter';
import { FilterItemsContainer } from 'Components/elements/filters/FilterItemsContainer';
import { Search } from 'Components/inputs/Search';
import { OptionWithTwoLabels } from 'Components/inputs/Select/options/OptionWithTwoLabels';
import { createdByFilterItems } from 'Mocks/fakeOptions';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Elements/Filters/DropDownFilter',
  component: Component,
  argTypes: {
    className: hideProperty,
    onClear: hideProperty,
  },
} as Meta;

const APPLIED_ITEMS = [
  {
    value: '1',
    label: 'User 1',
  },
  {
    value: '2',
    label: 'User 2 long long long long long long long long long long long long',
  },
  {
    value: '3',
    label: 'User 3',
  },
];

const DropDownFilterComponent: Story<Props> = (args) => {
  const [filterQuery, setFilterQuery] = useState<string[]>(
    APPLIED_ITEMS?.map(({ value }) => value),
  );
  const [searchValue, setSearchValue] = useState('');

  const createdByFilteredItems = createdByFilterItems.filter((item) =>
    item.label.includes(searchValue),
  );

  return (
    <Component
      {...args}
      items={createdByFilteredItems}
      appliedItemsValues={filterQuery}
      onChange={setFilterQuery}
    >
      {({ checkedState, appliedItems, nonAppliedItems }) => (
        <>
          <Search
            width="full"
            height="40"
            name="search"
            placeholder="Search Created by"
            value={searchValue}
            onChange={(e) => setSearchValue(e.target.value)}
            onClear={() => setSearchValue('')}
          />
          <FilterItemsContainer className={classNames(spacing.mt8, spacing.w400fixed)}>
            <SelectDropdownItemList
              type="multi"
              isSelectFilled={!!appliedItems?.length}
              selectedItems={appliedItems?.map(({ value, label, subLabel }) => (
                <OptionWithTwoLabels
                  key={value}
                  id={value}
                  tooltip
                  label={label}
                  secondaryLabel={subLabel}
                  type="checkbox"
                  selected={checkedState.values.includes(value)}
                  onClick={() => {
                    if (checkedState.values.includes(value)) {
                      checkedState.setValues(
                        checkedState.values.filter((checkedValue) => checkedValue !== value),
                      );
                      return;
                    }

                    checkedState.setValues([...checkedState.values, value]);
                  }}
                />
              ))}
              otherItems={nonAppliedItems?.map(({ value, label, subLabel }) => (
                <OptionWithTwoLabels
                  key={value}
                  id={value}
                  tooltip
                  label={label}
                  secondaryLabel={subLabel}
                  type="checkbox"
                  selected={checkedState.values.includes(value)}
                  onClick={() => {
                    if (checkedState.values.includes(value)) {
                      checkedState.setValues(
                        checkedState.values.filter((checkedValue) => checkedValue !== value),
                      );
                      return;
                    }

                    checkedState.setValues([...checkedState.values, value]);
                  }}
                />
              ))}
            />
          </FilterItemsContainer>
        </>
      )}
    </Component>
  );
};

export const DropDownFilter = DropDownFilterComponent.bind({});
DropDownFilter.args = {
  title: 'Filter title',
};
