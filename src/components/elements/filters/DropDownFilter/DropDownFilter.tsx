import { DropDown } from 'Components/DropDown';
import DropDownFilterBody from 'Components/elements/filters/DropDownFilter/components/DropDownFilterBody';
import DropDownFilterDrop from 'Components/elements/filters/DropDownFilter/components/DropDownFilterDrop';
import React, { FC, ReactElement, useCallback, useEffect, useMemo, useState } from 'react';

import useStateValues, { UseStateValues } from '../../../../hooks/useStateValues';
import { FilterItem } from './DropDownFilter.types';

export type DropDownFilterProps = {
  items?: FilterItem[];
  appliedItemsValues?: string[];
  title?: string;
  filterBodyClassName?: string;
  background?: 'grey-10' | 'white-100';
  onChange?: (appliedItemsValues: string[]) => void;

  children?: (params: {
    items?: FilterItem[];
    checkedState: UseStateValues;
    appliedItems?: FilterItem[];
    nonAppliedItems?: FilterItem[];
  }) => void;
};

const DropDownFilter: FC<DropDownFilterProps> = ({
  title,
  items,
  appliedItemsValues = [],
  filterBodyClassName,
  children,
  background,
  onChange,
}: DropDownFilterProps): ReactElement => {
  const [appliedFilterItems, setAppliedFilterItems] = useState<FilterItem[]>([]);

  const itemsValues = useMemo(() => items?.map(({ value }) => value) || [], [items]);

  const checkedState = useStateValues(itemsValues);
  const { setValues: setCheckedValues } = checkedState;

  const firstAppliedItemLabel = appliedFilterItems?.[0]?.label;
  const appliedItemsNumber = appliedFilterItems?.length;

  const clearAppliedItems = (action?: () => void) => (): void => {
    setAppliedFilterItems([]);
    onChange?.([]);
    action?.();
  };

  const changeAppliedItems = useCallback(
    (appliedItemsValues: string[]) => {
      const newAppliedItems = items?.filter(({ value }) => appliedItemsValues?.includes(value));

      if (!newAppliedItems) return;

      setAppliedFilterItems(newAppliedItems);
      onChange?.(appliedItemsValues);
    },
    [onChange, items],
  );

  useEffect(() => {
    const appliedItems = items?.filter(({ value }) => appliedItemsValues?.includes(value)) || [];

    setAppliedFilterItems(appliedItems);
    setCheckedValues(appliedItems.map(({ value }) => value));
  }, [appliedItemsValues]);

  const filterTooltipMessage =
    appliedFilterItems?.length > 1 ? (
      <>
        {appliedFilterItems?.map(({ label }, index) => (
          <>
            {label}
            {appliedFilterItems?.length !== index + 1 && <br />}
          </>
        ))}
      </>
    ) : (
      ''
    );

  const nonAppliedItems = items?.filter(
    ({ value }) => !appliedFilterItems?.find(({ value: appliedValue }) => appliedValue === value),
  );

  const params = useMemo(
    () => ({
      items,
      checkedState,
      appliedItems: appliedFilterItems,
      nonAppliedItems,
    }),
    [items, checkedState, appliedFilterItems, nonAppliedItems],
  );

  return (
    <DropDown
      flexdirection="column"
      padding="none"
      control={({ isOpen, handleOpen }) => (
        <DropDownFilterBody
          title={title}
          itemLabel={firstAppliedItemLabel}
          className={filterBodyClassName}
          appliedItemsNumber={appliedItemsNumber}
          tooltipMessage={filterTooltipMessage}
          onClick={handleOpen}
          onClear={clearAppliedItems(isOpen ? handleOpen : undefined)}
          background={background}
          opened={isOpen}
        />
      )}
    >
      <DropDownFilterDrop
        title="Created by"
        itemsValues={items?.map(({ value }) => value)}
        checkedState={checkedState}
        onApply={changeAppliedItems}
      >
        <>{children?.(params)}</>
      </DropDownFilterDrop>
    </DropDown>
  );
};

export default DropDownFilter;
