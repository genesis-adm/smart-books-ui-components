import { Tooltip } from 'Components/base/Tooltip';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Text } from 'Components/base/typography/Text';
import styles from 'Components/elements/filters/DropDownFilter/DropDownFilter.module.scss';
import { ReactComponent as CaretDownIcon } from 'Svg/v2/16/caret-down.svg';
import { ReactComponent as ClearIcon } from 'Svg/v2/16/cross.svg';
import classNames from 'classnames';
import React, { FC, ReactElement, ReactNode } from 'react';

import { useToggle } from '../../../../../hooks/useToggle';

type DropDownFilterBodyProps = {
  title?: string;
  className?: string;
  opened?: boolean;
  itemLabel?: string;
  appliedItemsNumber?: number;
  background?: 'grey-10' | 'white-100';

  tooltipMessage?: ReactNode;

  onClick?: (e: any) => void;
  onClear?: () => void;
};

const DropDownFilterBody: FC<DropDownFilterBodyProps> = ({
  title,
  className,
  opened,
  itemLabel,
  appliedItemsNumber = 0,
  tooltipMessage,
  background = 'grey-10',
  onClick,
  onClear,
}: DropDownFilterBodyProps): ReactElement => {
  const { isOpen: isButtonHovered, onOpen: mouseOverButton, onClose: mouseOutButton } = useToggle();
  const isAppliedItems = !!appliedItemsNumber;

  return (
    <div className={classNames(styles.wrapper, className)}>
      {!!title && (
        <Text className={styles.title} type="text-regular" color="grey-100">
          {title}
        </Text>
      )}

      <div
        role="presentation"
        className={classNames(styles.control, styles[`background_${background}`], {
          [styles.appliedItems]: isAppliedItems,
          [styles.clearFilter]: isAppliedItems && isButtonHovered,
        })}
        onClick={onClick}
      >
        <Tooltip
          message={isAppliedItems ? tooltipMessage : undefined}
          wrapperClassName={styles.label}
        >
          <Text className={styles.value} type="text-regular" color="inherit">
            {isAppliedItems ? itemLabel : 'All'}
          </Text>
          {appliedItemsNumber >= 2 && (
            <Text className={styles.multiselect} type="text-regular" color="inherit">
              &nbsp;+
              {appliedItemsNumber - 1}
            </Text>
          )}
        </Tooltip>

        <span className={styles.divider} />

        <IconButton
          size="32"
          className={
            isAppliedItems ? undefined : classNames(styles.button, { [styles.rotate]: opened })
          }
          icon={isAppliedItems ? <ClearIcon /> : <CaretDownIcon />}
          background={'inherit'}
          color="inherit"
          tooltip={isAppliedItems ? 'Clear filter' : undefined}
          onClick={isAppliedItems ? onClear : onClick}
          onMouseOver={mouseOverButton}
          onMouseOut={mouseOutButton}
        />
      </div>
    </div>
  );
};

export default DropDownFilterBody;
