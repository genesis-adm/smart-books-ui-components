import { useDropDown } from 'Components/DropDown';
import { Divider } from 'Components/base/Divider';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import style from 'Components/elements/filters/FilterDropDown/FilterDropDown.module.scss';
import { OptionsFooter } from 'Components/inputs/Select/options/OptionsFooter';
import classNames from 'classnames';
import React, { FC, ReactElement, ReactNode, useEffect } from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';
import { UseStateValues } from '../../../../../hooks/useStateValues';

type DropDownFilterDropProps = {
  itemsValues?: string[];
  checkedState: UseStateValues;

  title?: string;

  children?: ReactNode;
  onApply?: (appliedItemsIds: string[]) => void;
};

const DropDownFilterDrop: FC<DropDownFilterDropProps> = ({
  title,
  itemsValues,
  checkedState,
  children,
  onApply,
}: DropDownFilterDropProps): ReactElement => {
  const [handleToggle] = useDropDown();

  const {
    values: checkedValues,
    setAllValues: setAllCheckedValues,
    clearAllValues: clearAllCheckedValues,
  } = checkedState;

  const itemsNumber = itemsValues?.length || 0;
  const isItemsAndSelectedItemsDifferent = checkedValues.length !== itemsNumber;

  const handleSelectAllAction = itemsNumber
    ? (): void => {
        isItemsAndSelectedItemsDifferent ? setAllCheckedValues() : clearAllCheckedValues();
      }
    : undefined;

  const clearChecked = itemsNumber ? clearAllCheckedValues : undefined;

  const apply = (): void => {
    onApply?.(checkedValues);
    handleToggle?.();
  };

  useEffect(() => {
    return () => {
      clearAllCheckedValues();
    };
  }, [clearAllCheckedValues]);

  return (
    <>
      <Container className={classNames(spacing.m16)} flexdirection="column" gap="16">
        {!!title && (
          <Text className={style.title} type="text-regular" color="grey-100">
            {title}
          </Text>
        )}

        <>{children}</>
      </Container>

      <Divider fullHorizontalWidth />

      <OptionsFooter
        className={classNames(spacing.ml16)}
        optionsLength={itemsNumber}
        selectedOptionsNumber={checkedValues.length}
        checked={checkedValues.length === itemsNumber}
        onSelectChange={handleSelectAllAction}
        onClear={clearChecked}
        onApply={apply}
      />
    </>
  );
};

export default DropDownFilterDrop;
