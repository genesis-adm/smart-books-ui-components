export type FilterItem = {
  value: string;
  label: string;
  subLabel?: string;
  tertiaryLabel?: string;
  quadroLabel?: string;
};
