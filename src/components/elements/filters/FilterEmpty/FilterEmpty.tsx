import { Icon } from 'Components/base/Icon';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as EmptyIcon } from 'Svg/v2/124/empty-table.svg';
import classNames from 'classnames';
import React from 'react';

import style from './FilterEmpty.module.scss';

export interface FilterEmptyProps {
  className?: string;
}

const FilterEmpty: React.FC<FilterEmptyProps> = ({ className }) => (
  <div className={classNames(style.component, className)}>
    <Icon icon={<EmptyIcon />} />
    <Text type="body-regular-16" color="grey-100">
      Nothing found for your request
    </Text>
  </div>
);

export default FilterEmpty;
