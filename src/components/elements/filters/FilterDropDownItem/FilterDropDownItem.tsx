import { Checkbox } from 'Components/base/Checkbox';
import { Radio } from 'Components/base/Radio';
import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React, { ReactNode } from 'react';

import style from './FilterDropDownItem.module.scss';

export interface FilterDropDownItemProps {
  id: string;
  type: 'item' | 'all';
  element?: 'checkbox' | 'radio';
  width?: 'default' | 'minimal';
  checked: boolean;
  blurred: boolean;
  value: string | ReactNode;
  secondaryValue?: string;
  className?: string;
  onChange(): void;
}

const FilterDropDownItem: React.FC<FilterDropDownItemProps> = ({
  id,
  type = 'item',
  element = 'checkbox',
  width = 'default',
  checked,
  blurred,
  value,
  secondaryValue,
  className,
  onChange,
}) => (
  <div
    role="presentation"
    className={classNames(
      style.component,
      style[`min-width_${width}`],
      {
        [style.stacked]: secondaryValue,
      },
      className,
    )}
    onClick={onChange}
    data-type={type}
  >
    {element === 'checkbox' && (
      <Checkbox
        className={classNames(style.checkbox, {
          [style.blurred]: blurred,
          [style.checked]: checked,
        })}
        checked={checked}
        onChange={onChange}
        name={id.toLowerCase().replace(/\s+/g, '-')}
      />
    )}
    {element === 'radio' && (
      <Radio
        className={classNames(style.checkbox, {
          [style.blurred]: blurred,
          [style.checked]: checked,
        })}
        checked={checked}
        onChange={onChange}
        id={id.toLowerCase().replace(/\s+/g, '-')}
        name={id.toLowerCase().replace(/\s+/g, '-')}
      />
    )}
    <div className={style.wrapper}>
      <Text
        className={classNames(style.text, style.text_main)}
        type="caption-regular"
        color={checked && !blurred ? 'black-100' : 'grey-100'}
      >
        {value}
      </Text>
      {secondaryValue && (
        <Text
          className={classNames(style.text, style.text_secondary)}
          type="caption-regular"
          color={checked && !blurred ? 'black-100' : 'grey-100'}
        >
          {secondaryValue}
        </Text>
      )}
    </div>
  </div>
);

export default FilterDropDownItem;
