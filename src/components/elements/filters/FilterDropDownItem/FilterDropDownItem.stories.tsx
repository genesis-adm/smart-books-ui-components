import { Meta, Story } from '@storybook/react';
import {
  FilterDropDownItem as Component,
  FilterDropDownItemProps as Props,
} from 'Components/elements/filters/FilterDropDownItem';
import React from 'react';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Components/Elements/Filters/Filter DropDown Item',
  component: Component,
  argTypes: {
    id: hideProperty,
    className: hideProperty,
    onChange: hideProperty,
  },
} as Meta;

const FilterDropDownItemComponent: Story<Props> = (args) => <Component {...args} />;

export const FilterDropDownItem = FilterDropDownItemComponent.bind({});
FilterDropDownItem.args = {
  value: 'Main value',
  secondaryValue: 'Secondary value',
  checked: true,
  blurred: false,
  type: 'item',
  width: 'minimal',
  id: 'dropdown-item',
};
