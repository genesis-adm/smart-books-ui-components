import React, { ReactElement, ReactNode, useCallback, useMemo, useState } from 'react';

export type ContextValue<TData> = {
  contextData: TData;
  setContextData: (data: Partial<TData>) => void;
};

type ContextProviderProps<TData> = {
  children: ReactNode;
  initialState?: TData;
  Context: React.Context<ContextValue<TData>>;
};

const ContextProvider = <TData extends object>({
  children,
  initialState = {} as TData,
  Context,
}: ContextProviderProps<TData>): ReactElement => {
  const [data, setData] = useState<TData>(initialState);

  const setContextData = useCallback((data: Partial<TData>) => {
    setData((prev) => ({
      ...prev,
      ...data,
    }));
  }, []);

  const value = useMemo(() => ({ contextData: data, setContextData }), [data, setContextData]);

  return <Context.Provider value={value}>{children}</Context.Provider>;
};

export default ContextProvider;
