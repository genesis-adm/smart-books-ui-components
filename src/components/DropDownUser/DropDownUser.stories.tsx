import { Container } from 'Components/base/grid/Container';
import React from 'react';

import DropDownUser from './DropDownUser';

export default {
  title: 'REVISE LATER/DropDownUser',
};

export const DropDownUserDefault: React.FC = () => (
  <Container justifycontent="center">
    <DropDownUser user="Karine Mnatsakanian" />
  </Container>
);
