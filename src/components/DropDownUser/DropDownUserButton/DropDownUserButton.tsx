import { DropDownContext } from 'Components/DropDown/DropDown';
import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React, { useContext } from 'react';

import style from './DropDownUserButton.module.scss';

interface DropDownUserButtonProps {
  icon?: React.ReactNode;
  children?: React.ReactNode;
  onClick(): void;
}

const DropDownUserButton: React.FC<DropDownUserButtonProps> = ({ children, icon, onClick }) => {
  const { handleOpen } = useContext(DropDownContext);

  const handleClick = () => {
    onClick();
    if (handleOpen) handleOpen();
  };

  return (
    <div role="presentation" onClick={handleClick} className={classNames(style.button)}>
      <span className={style.icon}>{icon}</span>
      <Text type="caption-semibold" color="grey-100" noWrap className={style.text}>
        {children}
      </Text>
    </div>
  );
};

DropDownUserButton.defaultProps = {
  icon: null,
};

export default DropDownUserButton;
