import { DropDown } from 'Components/DropDown';
import { Icon } from 'Components/base/Icon';
import { Tooltip } from 'Components/base/Tooltip';
import { Container } from 'Components/base/grid/Container';
import { Logo } from 'Components/elements/Logo';
import { ReactComponent as CaretIcon } from 'Svg/v2/10/caret-both.svg';
import React from 'react';

interface DropDownUserProps {
  user: string;
  children?: React.ReactNode;
}

const DropDownUser: React.FC<DropDownUserProps> = ({ user, children }) => (
  <DropDown
    flexdirection="column"
    padding="10"
    control={({ isOpen, handleOpen }) => (
      <Tooltip message={isOpen ? '' : 'My account'}>
        <Container alignitems="center" gap="4">
          <Logo radius="rounded" size="32" content="user" name={user} onClick={handleOpen} />
          <Icon icon={<CaretIcon />} path="grey-100" cursor="pointer" onClick={handleOpen} />
        </Container>
      </Tooltip>
    )}
  >
    {children}
  </DropDown>
);

export default DropDownUser;
