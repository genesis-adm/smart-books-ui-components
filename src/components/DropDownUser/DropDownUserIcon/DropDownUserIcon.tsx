import { Text } from 'Components/base/typography/Text';
import React from 'react';

import style from './DropDownUserIcon.module.scss';

interface DropDownUserIconProps {
  children?: React.ReactNode;
  onClick(): void;
}

const DropDownUserIcon: React.FC<DropDownUserIconProps> = ({ onClick, children }) => (
  <button onClick={onClick} className={style.avatar} type="button">
    <Text type="avatar-small" color="violet-90">
      {children}
    </Text>
  </button>
);

export default DropDownUserIcon;
