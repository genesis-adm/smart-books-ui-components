import classNames from 'classnames';
import React, {
  Dispatch,
  ReactElement,
  ReactNode,
  SetStateAction,
  useEffect,
  useMemo,
  useState,
} from 'react';

import usePositioning, { Axis, PositionType } from '../../hooks/usePositioning';
import { InputWidthType } from '../../types/inputs';
import { SelectValue } from '../inputs/Select/hooks/useSelectValue';
import styles from './SelectDropdown.module.scss';

export type SelectDropdownWidth = InputWidthType;

export type Children<IsMulti extends boolean> = IsMulti extends false
  ? ReactNode
  : (args: {
      selectedOptions: SelectValue<IsMulti>;
      setSelectedOptions: Dispatch<SetStateAction<SelectValue<IsMulti>>>;
    }) => ReactElement;

export type SelectDropdownProps<IsMulti extends boolean = false> = {
  selectValue?: SelectValue<IsMulti>;
  isMulti?: IsMulti;
  selectElement: Element;
  staticPosition?: PositionType;
  direction?: Axis;
  width?: SelectDropdownWidth;
  children: Children<IsMulti>;
  className?: string;
};

const DEFAULT_WIDTH = '320';

const SelectDropdown = <IsMulti extends boolean = false>({
  selectElement,
  selectValue,
  staticPosition,
  direction = 'vertical',
  width,
  children,
  className,
}: SelectDropdownProps<IsMulti>): ReactElement => {
  const [selectedOptions, setSelectedOptions] = useState<SelectValue<true>>([]);

  const transformedWidth = width === 'full' ? selectElement.clientWidth : width;
  const elementWidth = transformedWidth || selectElement.clientWidth || DEFAULT_WIDTH;

  const { styles: dropWrapperStyle, mode } = usePositioning(
    selectElement,
    elementWidth,
    staticPosition,
    direction,
  );

  useEffect(() => {
    if (Array.isArray(selectValue) || selectValue == null) {
      setSelectedOptions(selectValue || []);
    }
  }, [selectValue]);

  const controls = useMemo(
    () => ({
      selectedOptions,
      setSelectedOptions,
    }),
    [selectedOptions, setSelectedOptions],
  );

  return (
    <div
      className={classNames(styles.selectDropdownWrapper, styles[`mode_${mode}`], className)}
      style={dropWrapperStyle}
    >
      {children ? (
        typeof children === 'function' ? (
          !!controls && children(controls)
        ) : (
          children
        )
      ) : (
        <></>
      )}
    </div>
  );
};

export default SelectDropdown;
