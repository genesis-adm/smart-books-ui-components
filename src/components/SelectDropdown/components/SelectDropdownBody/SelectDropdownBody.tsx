import classNames from 'classnames';
import React, { ReactElement, ReactNode } from 'react';

import styles from './SelectDropdownBody.module.scss';

export type SelectDropdownBodyProps = {
  children?: ReactNode;
  className?: string;
};

const SelectDropdownBody = ({ className, children }: SelectDropdownBodyProps): ReactElement => {
  return <div className={classNames(styles.selectDropBody, className)}>{children}</div>;
};

export default SelectDropdownBody;
