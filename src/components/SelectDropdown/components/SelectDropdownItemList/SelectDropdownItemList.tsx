import classNames from 'classnames';
import React, { ReactElement, ReactNode } from 'react';

import styles from './SelectDropdownItemList.module.scss';

type SingleProps = {
  children?: ReactNode;
};

type MultiProps = {
  selectedItems?: ReactNode[];
  otherItems?: ReactNode[];
  isSelectFilled: boolean;
};

type OwnSelectDropdownItemListProps = {
  single: SingleProps;
  multi: MultiProps;
};

type SelectType = 'single' | 'multi';

export type SelectDropdownItemListProps<Type extends SelectType = 'single'> =
  OwnSelectDropdownItemListProps[Type] & {
    type?: Type;
    className?: string;
    onScroll?: () => void;
  };

const defaultType = 'single';

const SelectDropdownItemList = <Type extends SelectType = typeof defaultType>({
  type = 'single' as Type,
  className,
  onScroll,
  ...props
}: SelectDropdownItemListProps<Type>): ReactElement => {
  const handleScroll = (e: React.UIEvent<HTMLUListElement>) => {
    const targetElement = e.currentTarget;

    const bottom =
      targetElement.scrollHeight - targetElement.scrollTop <= targetElement.clientHeight + 10;

    if (bottom) {
      onScroll?.();
    }
  };

  return (
    <ul className={classNames(styles.selectDropItemList, className)} onScroll={handleScroll}>
      {type === 'single' && <Single {...props} />}
      {type === 'multi' && <Multi {...(props as SelectDropdownItemListProps<'multi'>)} />}
    </ul>
  );
};

export default SelectDropdownItemList;

const Single = ({ children }: SingleProps): ReactElement => <>{children}</>;

const Multi = ({
  selectedItems = [],
  otherItems = [],
  isSelectFilled,
}: MultiProps): ReactElement => (
  <div className={styles.multiSelectItemsWrapper}>
    {isSelectFilled ? (
      <>
        {!!selectedItems?.length && (
          <>
            <span className={styles.title}>Selected</span>
            {selectedItems}
          </>
        )}

        {!!otherItems?.length && (
          <>
            <span className={styles.title}>Other</span>
            {otherItems}
          </>
        )}
      </>
    ) : (
      [...selectedItems, ...otherItems]
    )}
  </div>
);
