import { Divider } from 'Components/base/Divider';
import classNames from 'classnames';
import React, { ReactElement, ReactNode } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';
import styles from './SelectDropdownFooter.module.scss';

export type SelectDropdownFooterProps = {
  className?: string;
  children?: ReactNode;
};

const SelectDropdownFooter = ({ className, children }: SelectDropdownFooterProps): ReactElement => {
  return (
    <div className={classNames(styles.selectDropdownFooter, className)}>
      <Divider type="horizontal" fullHorizontalWidth className={spacing.mY8} />
      {children}
    </div>
  );
};

export default SelectDropdownFooter;
