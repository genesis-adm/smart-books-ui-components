import {
  CSV_FILE_EXTENSIONS,
  EXCEL_FILE_EXTENSIONS,
  IMAGE_FILE_EXTENSIONS,
  PDF_FILE_EXTENSIONS,
} from '../constants/file';
import { FileType } from '../types/general';

export const checkFileType = (file: File, types: string[]) => {
  const fileType = file.name.split('.').pop() as string;
  return types.includes(fileType);
};

export const isFileInArray = (file: File, array: File[]) => {
  const { name, size } = file;
  return array.find((element) => element.name === name && element.size === size);
};

export const fileExtensionByName = (name: string): string =>
  name.split('.').pop()?.toLocaleLowerCase() || '';

export const fileTypeByExtension = (extension: string): FileType | undefined => {
  if (EXCEL_FILE_EXTENSIONS?.includes(extension)) return 'excel';

  if (IMAGE_FILE_EXTENSIONS?.includes(extension)) return 'image';

  if (PDF_FILE_EXTENSIONS?.includes(extension)) return 'pdf';

  if (CSV_FILE_EXTENSIONS?.includes(extension)) return 'csv';

  return;
};
