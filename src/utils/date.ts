import { format } from 'date-fns';

export const formatApiDate = (date: string): string => format(new Date(date), 'dd MMM yyyy');
