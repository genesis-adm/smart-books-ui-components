export const navigationNameFormatter = (str: string): string =>
  str.replaceAll(' ', '').toLowerCase();
