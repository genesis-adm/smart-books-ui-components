const handleAccountLength = (account: string, limit = 8, showSymbols = 4) => {
  const { length } = account;
  return length > limit
    ? `${account.substring(0, showSymbols)}****${account.substring(length - showSymbols, length)}`
    : account;
};

export default handleAccountLength;
