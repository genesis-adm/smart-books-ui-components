import React from 'react';

export const modifyTextColorPartial = (
  str: string,
  regEx: string,
): (string | React.ReactElement)[] => {
  const regex = new RegExp(regEx, 'gi');
  const parts = str.split(regex);
  const matches = str.match(regex) || [];

  return parts.flatMap((part, index) =>
    index < matches.length
      ? [
          part,
          React.createElement(
            'span',
            { key: index, style: { color: '#6367F6', fontWeight: '500' } },
            matches[index],
          ),
        ]
      : part,
  );
};
