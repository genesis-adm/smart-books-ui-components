export const viewportWidth =
  window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
export const viewportHeight =
  window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
