export const roundNumber = (value: number, decimalScale?: number): number => {
  const multiplier = decimalScale
    ? Number('1' + Array.from(Array(decimalScale), () => '0').join(''))
    : 1;

  return Math.round(value * multiplier) / multiplier;
};
