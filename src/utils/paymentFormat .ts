export function formatCardNumber(cardNumber: string) {
  const cardNumberWithoutSymbols = cardNumber.replace(/\s+/g, '').replace(/[^0-9]/gi, '');
  // TODO Якщо видалити if то не буде працювати видалення карти
  if ([4, 9, 14].includes(cardNumber.length)) return cardNumber;

  return cardNumberWithoutSymbols
    .replace(/(\d{4})/, '$1 ')
    .replace(/(\d{4}) (\d{4})/, '$1 $2 ')
    .replace(/(\d{4}) (\d{4}) (\d{4})/, '$1 $2 $3 ')
    .replace(/(\d{4}) (\d{4}) (\d{4}) (\d{4})/, '$1 $2 $3 $4');
}

export function formatRoutingNumber(routing: string) {
  const routingWithoutSymbols = routing.replace(/\s+/g, '');

  if ([4, 8].includes(routingWithoutSymbols.length)) return routing;

  return routingWithoutSymbols
    .replace(/(\S{4})/, '$1 ')
    .replace(/(\S{4}) (\S{4})/, '$1 $2 ')
    .replace(/(\S{4}) (\S{4}) (\S{0-1})/, '$1 $2 $3');
}

export function formatBIC(bic: string): string {
  const bicWithoutSymbols = bic.replace(/\s+/g, '');
  if ([4, 7, 10].includes(bic.length)) return bic;

  return bicWithoutSymbols
    .replace(/(\S{4})/, '$1 ')
    .replace(/(\S{4}) (\S{2})/, '$1 $2 ')
    .replace(/(\S{4}) (\S{2}) (\S{2})/, '$1 $2 $3 ')
    .replace(/(\S{4}) (\S{2}) (\S{2}) (\S{4})/, '$1 $2 $3 $4');
}

export function formatIban(iban: string): string {
  const ibanWithoutSymbols = iban.replace(/\W/g, '');
  if ([2, 5, 17].includes(iban.length)) return iban;

  return ibanWithoutSymbols
    .replace(/(\S{2})/, '$1 ')
    .replace(/(\S{2}) (\d{2})/, '$1 $2 ')
    .replace(/(\S{2}) (\d{2}) (\d{11})/, '$1 $2 $3 ');
}
