const handleStringLength = (value: string, limit: number) => {
  const { length } = value;
  return length > limit ? `${value.substring(0, limit - 2)}...` : value;
};

export default handleStringLength;
