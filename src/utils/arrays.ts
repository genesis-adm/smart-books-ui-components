export const deleteArrayItemByIndex = <Array extends unknown[]>(
  array: Array,
  index: number,
): Array => {
  const arrayHalfBefore = array?.slice(0, index);
  const arrayHalfAfter = array?.slice(index + 1);

  return arrayHalfBefore?.concat(arrayHalfAfter) as Array;
};
