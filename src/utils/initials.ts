const initials = (str: string): string =>
  str
    .split(' ')
    .map((c) => c.charAt(0))
    .join('')
    .substring(0, 2);

export default initials;
