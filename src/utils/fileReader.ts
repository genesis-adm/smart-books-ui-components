const fileReader = (file: File): Promise<string> =>
  new Promise((resolve, reject) => {
    const reader: FileReader = new FileReader();
    reader.onloadend = () => {
      resolve(String(reader.result));
    };
    reader.onerror = reject;
    reader.readAsDataURL(file);
  });

export default fileReader;
