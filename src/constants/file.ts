export const IMAGE_FILE_EXTENSIONS = ['jpg', 'jpeg', 'png', 'webp'];
export const EXCEL_FILE_EXTENSIONS = ['xlsx', 'xls', 'csv'];
export const PDF_FILE_EXTENSIONS = ['pdf'];
export const CSV_FILE_EXTENSIONS = ['csv'];
