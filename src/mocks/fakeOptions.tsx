// Attention: this file is temporary and is used only for development
// in Storybook for mocking fake data in Select
import { ReactComponent as BankSuitcaseIcon } from 'Svg/16/suitcase-dollar.svg';
import handleAccountLength from 'Utils/handleAccountLength';
import React from 'react';

export const options = [
  {
    icon: <BankSuitcaseIcon />,
    value: 'value-1',
    label: 'Main label 1',
    secondaryLabel: 'SecLabel 1',
    tertiaryLabel: 'TerLabel 1',
    quadroLabel: 'QuadLabel 1',
    subLabel: '4830QZ995647769596875246278',
  },
  {
    value: 'value-2',
    label: 'Main label 2',
    secondaryLabel: 'SecLabel 2',
    tertiaryLabel: 'TerLabel 2',
    quadroLabel: 'QuadLabel 2',
    subLabel: 'Cash',
  },
  {
    value: 'value-3',
    label: 'Main label 3 very very very very very very very very very very long',
    secondaryLabel: 'SecLabel 3 very very very very very very very very very very long',
    tertiaryLabel: 'TerLabel 3 very very very very very very very very very very long',
    quadroLabel: 'QuadLabel 3 very very very very very very very very very very long',
    subLabel: 'Cashadfmgkmsdflkgfdlkgjmdfk.gjlfdkgjlfdgjlfdgkjlgjldfjgdld',
  },
  {
    value: 'value-4',
    label: 'Main label 4',
    secondaryLabel: 'SecLabel 4',
    tertiaryLabel: 'TerLabel 4',
    quadroLabel: 'QuadLabel 4',
  },
  {
    value: 'value-5',
    label: 'Main label 5',
    secondaryLabel: 'SecLabel 5',
    tertiaryLabel: 'TerLabel 5',
    quadroLabel: 'QuadLabel 5',
  },
  {
    value: 'value-6',
    label: 'Main label 6',
    secondaryLabel: 'SecLabel 6',
    tertiaryLabel: 'TerLabel 6',
    quadroLabel: 'QuadLabel 6',
  },
  {
    value: 'value-7',
    label: 'Main label 7',
    secondaryLabel: 'SecLabel 7',
    tertiaryLabel: 'TerLabel 7',
    quadroLabel: 'QuadLabel 7',
  },
];

export const currencyOptions = [
  {
    value: '1-UAH',
    label: 'UAH',
    fullLabel: 'Ukrainian hryvnia',
  },
  {
    value: '2-USD',
    label: 'USD',
    fullLabel: 'United States dollar',
  },
  {
    value: '3-EUR',
    label: 'EUR',
    fullLabel: 'Euro',
  },
  {
    value: '4-UAH',
    label: 'UAH',
    fullLabel: 'Ukrainian hryvnia',
  },
  {
    value: '5-USD',
    label: 'USD',
    fullLabel: 'United States dollar',
  },
  {
    value: '6-EUR',
    label: 'EUR',
    fullLabel: 'Euro',
  },
  {
    value: '7-UAH',
    label: 'UAH',
    fullLabel: 'Ukrainian hryvnia',
  },
  {
    value: '8-USD',
    label: 'USD',
    fullLabel: 'United States dollar',
  },
  {
    value: '9-EUR',
    label: 'EUR',
    fullLabel: 'Euro',
  },
  {
    value: '10-UAH',
    label: 'UAH',
    fullLabel: 'Ukrainian hryvnia',
  },
  {
    value: '11-USD',
    label: 'USD',
    fullLabel: 'United States dollar',
  },
  {
    value: '12-EUR',
    label: 'EUR',
    fullLabel: 'Euro',
  },
  {
    value: '13-UAH',
    label: 'UAH',
    fullLabel: 'Ukrainian hryvnia',
  },
  {
    value: '14-USD',
    label: 'USD',
    fullLabel: 'United States dollar',
  },
  {
    value: '15-EUR',
    label: 'EUR',
    fullLabel: 'Euro',
  },
  {
    value: '16-UAH',
    label: 'UAH',
    fullLabel: 'Ukrainian hryvnia',
  },
  {
    value: '17-USD',
    label: 'USD',
    fullLabel: 'United States dollar',
  },
  {
    value: '18-EUR',
    label: 'EUR',
    fullLabel: 'Euro',
  },
  {
    value: '19-UAH',
    label: 'UAH',
    fullLabel: 'Ukrainian hryvnia',
  },
  {
    value: '20-USD',
    label: 'USD',
    fullLabel: 'United States dollar',
  },
  {
    value: '21-EUR',
    label: 'EUR',
    fullLabel: 'Euro',
  },
  {
    value: '22-UAH',
    label: 'UAH',
    fullLabel: 'Ukrainian hryvnia',
  },
  {
    value: '23-USD',
    label: 'USD',
    fullLabel: 'United States dollar',
  },
  {
    value: '24-EUR',
    label: 'EUR',
    fullLabel: 'Euro',
  },
];

export const rowsPerPageOptions = [
  {
    value: '25',
    label: '25',
  },
  {
    value: '50',
    label: '50',
  },
  {
    value: '75',
    label: '75',
  },
  {
    value: '100',
    label: '100',
  },
  {
    value: '200',
    label: '200',
  },
];

export const errors = ['Error 1', 'Error 2'];

export const createdByFilterItems = [
  {
    value: '1',
    label: 'User 1',
  },
  {
    value: '2',
    label: 'User 2 long long long long long long long long long long long long',
  },
  {
    value: '3',
    label: 'User 3',
  },
  {
    value: '4',
    label: 'User 4',
  },
  {
    value: '5',
    label: 'User 5',
  },
  {
    value: '6',
    label: 'User 6 long long long long long long long long',
  },
  {
    value: '7',
    label: 'User 7',
  },
  {
    value: '8',
    label: 'User 8',
  },
  {
    value: '9',
    label: 'User 9',
  },
  {
    value: '10',
    label: 'User 10',
  },
  {
    value: '11',
    label: 'User 11',
  },
];

export const suggestions = [
  {
    id: '1',
    bankname: 'Bank #1 name',
  },
  {
    id: '2',
    bankname: 'Bank #2 long long long long long long long long name',
  },
  {
    id: '3',
    bankname: 'Bank #3 name',
  },
  {
    id: '4',
    bankname: 'Bank #4 name',
  },
  {
    id: '5',
    bankname: 'Bank #5 name',
  },
  {
    id: '6',
    bankname: 'Bank #6 long long long long long long long long name',
  },
  {
    id: '7',
    bankname: 'Bank #7 name',
  },
  {
    id: '8',
    bankname: 'Bank #8 name',
  },
  {
    id: '9',
    bankname: 'Bank #9 name',
  },
  {
    id: '10',
    bankname: 'Bank #10 name',
  },
  {
    id: '11',
    bankname: 'Bank #11 name',
  },
];

export const paymentMethodsListExample = {
  creditCard: [
    {
      paymentSystem: 'mastercard',
      name: 'MonoBank',
      accountNumber: '5375414105879865',
      currency: 'UAH',
      isDefault: true,
      type: 'creditCard',
    },
    {
      paymentSystem: 'visa',
      name: 'PrivatBank',
      accountNumber: '5167985500142976',
      currency: 'USD',
      isDefault: false,
      type: 'creditCard',
    },
    {
      paymentSystem: 'discover',
      name: 'Discover',
      accountNumber: '5167985500142976',
      currency: 'USD',
      isDefault: false,
      type: 'creditCard',
    },
  ],
  bankAccount: [
    {
      name: 'PrivatBank',
      accountNumber: 'UA423345154152377949287722586',
      currency: 'UAH',
      isDefault: false,
      type: 'bankAccount',
    },
  ],
  wallet: [
    {
      paymentSystem: 'afterpay',
      name: 'Afterpay',
      accountNumber: '1231231237990',
      currency: 'USD',
      isDefault: false,
      type: 'wallet',
    },
    {
      paymentSystem: 'afterpay',
      name: 'Afterpay',
      accountNumber: '1231231223990',
      currency: 'USD',
      isDefault: false,
      type: 'wallet',
    },
    {
      paymentSystem: 'afterpay',
      name: 'Afterpay',
      accountNumber: '1531231222990',
      currency: 'USD',
      isDefault: false,
      type: 'wallet',
    },
    {
      paymentSystem: 'afterpay',
      name: 'Afterpay',
      accountNumber: '1531231221990',
      currency: 'USD',
      isDefault: false,
      type: 'wallet',
    },
  ],
};

export const calendarPresetsFilterData: {
  label: string;
  startDate: string;
  endDate: string;
}[] = [
  {
    label: 'This week',
    startDate: '28.08.2023',
    endDate: '29.08.2023',
  },
  {
    label: 'This month',
    startDate: '01.08.2023',
    endDate: '29.08.2023',
  },
  {
    label: 'This quarter',
    startDate: '01.07.2023',
    endDate: '29.08.2023',
  },
  {
    label: 'This year',
    startDate: '01.01.2023',
    endDate: '29.08.2023',
  },
  {
    label: 'Last week',
    startDate: '28.08.2023',
    endDate: '27.08.2023',
  },
  {
    label: 'Last month',
    startDate: '01.07.2023',
    endDate: '31.07.2023',
  },
  {
    label: 'Last quarter',
    startDate: '01.04.2023',
    endDate: '30.06.2023',
  },
  {
    label: 'Last year',
    startDate: '01.01.2022',
    endDate: '31.12.2022',
  },
];

export const bankAccountMultiSelectValues = [
  {
    value: 'East-West United Bank 1',
    bank: 'East-West United Bank',
    accountNumber: handleAccountLength('CY68902000010000020105002'),
    currency: 'USD',
    type: 'Cash',
    legalEntity: 'Obrio Limited',
  },
  {
    value: 'East-West United Bank 2',
    bank: 'East-West United Bank',
    accountNumber: 'CY68902000010000020105002',
    currency: 'USD',
    type: 'Cash',
    legalEntity: 'Obrio Limited',
  },
  {
    value: 'East-West United Bank 3',
    bank: 'East-West United Bank',
    accountNumber: 'CY68902000010000020105002',
    currency: 'USD',
    type: 'Cash',
    legalEntity: 'Obrio Limited',
  },
  {
    value: 'East-West United Bank 4',
    bank: 'East-West United Bank',
    accountNumber: 'CY68902000010000020105002',
    currency: 'USD',
    type: 'Cash',
    legalEntity: 'Obrio Limited',
  },
  {
    value: 'East-West United Bank 5',
    bank: 'East-West United Bank',
    accountNumber: 'CY68902000010000020105002',
    currency: 'USD',
    type: 'Cash',
    legalEntity: 'Obrio Limited',
  },
];

export const legalEntities = [
  {
    id: 'le 1',
    title: 'Legal entity veeeeeeery veeeeeeeeeryyyyyy loooooooong 1',
  },
  {
    id: 'le 2',
    title: 'Legal entity 2',
  },
  {
    id: 'le 3',
    title: 'Legal entity 3',
  },
  {
    id: 'le 4',
    title: 'Legal entity 4',
  },
];

export const businessUnits = [
  {
    id: 'bu 1',
    title: 'Business unit veeeeeeery veeeeeeeeeryyyyyy loooooooong 1',
  },
  {
    id: 'bu 2',
    title: 'Business unit 2',
  },
  {
    id: 'bu 3',
    title: 'Business unit 3',
  },
  {
    id: 'bu 4',
    title: 'Business unit 4',
  },
];

export const customers = [
  { id: 'customer1', name: 'Customer name 1', type: 'Company', isTaxAgent: false },
  {
    id: 'customer2',
    name: 'Customer name 2 loooooooooooooooong naaaaame',
    type: 'Individuals',
    isTaxAgent: true,
  },
  { id: 'customer3', name: 'Customer name 3', type: 'Individuals', isTaxAgent: true },
  { id: 'customer4', name: 'Customer name 4', type: 'Company', isTaxAgent: false },
];

export const productOptions = [
  {
    id: 'product1',
    name: 'Product name 1',
    businessUnit: 'Business unit name',
    type: 'Product type',
  },
  {
    id: 'product2',
    name: 'Product name 2',
    businessUnit: 'Business unit name',
    type: 'Product type',
  },
  {
    id: 'product3',
    name: 'Product name 3',
    businessUnit: 'Business unit name',
    type: 'Product type',
  },
  {
    id: 'product4',
    name: 'Product name loooooooooooooooooooongggg dfhjfgihg jfidf 4',
    businessUnit: 'Business unit name',
    type: 'Product type',
  },
  {
    id: 'product5',
    name: 'Product name 5',
    businessUnit: 'Business unit name dfkvgjewjrgwiofoqfwwelgj',
    type: 'Product type',
  },
];

export const countrySelectOptions = [
  { value: 'Ukraine', label: 'Ukraine' },
  { value: 'Poland', label: 'Poland' },
  { value: 'Germany', label: 'Germany' },
  { value: 'Italy', label: 'Italy' },
  { value: 'Spain', label: 'Spain' },
];
