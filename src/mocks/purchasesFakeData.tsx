import { Checkbox } from 'Components/base/Checkbox';
import { AmountFilter } from 'Components/custom/Stories/AmountFilter/AmountFilter';
import { CalendarFilter } from 'Components/custom/Stories/CalendarFilter/CalendarFilter';
import { ReactComponent as RejectedIcon } from 'Svg/16/minus-circle.svg';
import { ReactComponent as BankSuitcaseIcon } from 'Svg/16/suitcase-dollar.svg';
import { ReactComponent as ProcessingIcon } from 'Svg/v2/16/clock-filled.svg';
import { ReactComponent as CancelledIcon } from 'Svg/v2/16/cross-in-filled-circle.svg';
import { ReactComponent as AllStatusIcon } from 'Svg/v2/16/lines.svg';
import { ReactComponent as AcceptedIcon } from 'Svg/v2/16/tick-in-filled-circle.svg';
import handleAccountLength from 'Utils/handleAccountLength';
import React, { ReactNode } from 'react';

export type CustomColumnType = {
  id: string;
  title: string;
};
export const sorting = {
  sortType: null,
  tooltipText: 'Sorting tooltip text',
};
export const tableHead = [
  {
    minWidth: '90',
    title: 'Picture',
    padding: 'none',
    active: true,
  },
  { minWidth: '160', title: 'Name / SKU', sorting, active: true },
  { minWidth: '150', title: 'Barcode', active: true },
  { minWidth: '100', title: 'Units', sorting, active: false },
  { minWidth: '130', title: 'Item type', sorting, active: false },
  { minWidth: '160', title: 'Category', sorting, active: true },
  { minWidth: '160', title: 'Vendors', sorting, active: true },
  { minWidth: '160', title: 'Notes', active: false },
  { minWidth: '100', title: 'Currency', sorting, active: true },
  { minWidth: '160', title: 'Pre-agreed price, excl taxes', align: 'right', active: true },
  { minWidth: '100', title: 'Taxes', align: 'right', active: true },
  {
    minWidth: '160',
    title: 'Pre-agreed price, incl taxes',
    align: 'right',
    sorting,
    active: true,
  },
  { minWidth: '100', title: 'Actions', align: 'center', active: true },
];
export const approvalsTableHead = [
  {
    minWidth: '48',
    title: <Checkbox name="tickAll" checked={false} onChange={() => {}} />,
    padding: 'none',
    active: true,
  },
  {
    minWidth: '170',
    title: 'ID / Create Date',
    sorting,
    active: true,
  },
  { minWidth: '220', title: 'Created by', active: true },
  { minWidth: '180', title: 'Vendor', sorting, active: true },
  { minWidth: '180', title: 'Categories', active: true },
  { minWidth: '180', title: 'Due Date', sorting, active: true },
  { minWidth: '120', title: 'Currency', align: 'center', active: true },
  { minWidth: '180', title: 'Bill amount', sorting, align: 'right', active: true },
  { minWidth: '170', title: 'Paid amount', align: 'right', active: false },
  { minWidth: '160', title: 'Unpaid amount', align: 'right', active: false },
  { minWidth: '180', title: 'Approval status', align: 'center', active: true },
  { minWidth: '180', title: 'Payment status', align: 'center', active: false },
  {
    minWidth: '250',
    title: 'Paid from Legal entity',
    active: false,
  },
  {
    minWidth: '310',
    title: 'Paid from Bank account',
    active: false,
  },
  {
    minWidth: '350',
    title: 'Payment type → Pay to account',
    active: false,
  },
  {
    minWidth: '170',
    title: 'Treasury manager',
    active: false,
  },
  { minWidth: '100', title: 'Actions', align: 'center', active: true },
];
export const customColumnItems: CustomColumnType[] = [
  {
    id: 'units',
    title: 'Units',
  },
  {
    id: 'itemType',
    title: 'Item type',
  },
  {
    id: 'notes',
    title: 'Notes',
  },
];
export const approvalsCustomColumnItems: CustomColumnType[] = [
  {
    id: 'paidAmount',
    title: 'Paid amount',
  },
  {
    id: 'unpaidAmount',
    title: 'Unpaid amount',
  },
  {
    id: 'paymentStatus',
    title: 'Payment status',
  },
  {
    id: 'paidFromLegalEntity',
    title: 'Paid from Legal entity',
  },
  {
    id: 'paidFromBankAccount',
    title: 'Paid from Bank account',
  },
  {
    id: 'paymentType',
    title: 'Payment type → Pay to account',
  },
  {
    id: 'treasuryManager',
    title: 'Treasury manager',
  },
];
export const filters: { title: string }[] = [
  { title: 'Name' },
  { title: 'SKU' },
  { title: 'Type' },
  { title: 'Category' },
  { title: 'Vendors' },
];

export const categoriesFilters: { title: string }[] = [
  { title: 'Name' },
  { title: 'Status' },
  { title: 'Owner' },
];
export const billsAndApprovalsFilters: { title: string; component?: ReactNode }[] = [
  { title: 'Create date', component: <CalendarFilter /> },
  { title: 'Due date', component: <CalendarFilter /> },
  { title: 'Bill amount', component: <AmountFilter /> },
  { title: 'Vendor' },
  { title: 'Requestor' },
  { title: 'Approval status' },
  { title: 'Payment status' },
];
export const paymentsFilters: { title: string; component?: ReactNode }[] = [
  { title: 'Create date', component: <CalendarFilter /> },
  { title: 'Vendor' },
  { title: 'Currency' },
  { title: 'Payment amount', component: <AmountFilter /> },
  { title: 'Payment status' },
  { title: 'Payment date' },
  { title: 'Paid from Bank account' },
  { title: 'Payment method' },
  { title: 'Treasury Manager' },
];
export const priceFilterItem: { title: string; checked?: boolean }[] = [
  { title: 'Equals', checked: true },
  { title: 'Does not Equal' },
  { title: 'Greater Than' },
  { title: 'Less Than' },
  { title: 'Between' },
];
export const dictionariesTabs: { name: string; active: boolean }[] = [
  { name: 'Vendors', active: false },
  { name: 'Categories', active: false },
  { name: 'Items', active: true },
];

export const filterOptions = [
  {
    id: '1',
    option: 'Option #1 name',
  },
  {
    id: '2',
    option: 'Option #2 long long long long long long long long name',
  },
  {
    id: '3',
    option: 'Option #3 name',
  },
  {
    id: '4',
    option: 'Option #4 name',
  },
  {
    id: '5',
    option: 'Option #5 name',
  },
  {
    id: '6',
    option: 'Option #6 long long long long long long long long name',
  },
  {
    id: '7',
    option: 'Option #7 name',
  },
  {
    id: '8',
    option: 'Option #8 name',
  },
  {
    id: '9',
    option: 'Option #9 name',
  },
  {
    id: '10',
    option: 'Option #10 name',
  },
  {
    id: '11',
    option: 'Option #11 name',
  },
];

export const billsAndApprovalsTabs: { name: string; active: boolean; icon?: ReactNode }[] = [
  { name: 'All', active: true, icon: <AllStatusIcon /> },
  { name: 'Awaiting for approval', active: false, icon: <ProcessingIcon /> },
  { name: 'Approved', active: false, icon: <AcceptedIcon /> },
  { name: 'Rejected', active: false, icon: <RejectedIcon /> },
  { name: 'Canceled', active: false, icon: <CancelledIcon /> },
];

export const paymentsTabs: { name: string; active: boolean; icon?: ReactNode }[] = [
  { name: 'All', active: true, icon: <AllStatusIcon /> },
  { name: 'Unpaid', active: false, icon: <ProcessingIcon /> },
  { name: 'Send', active: false, icon: <AcceptedIcon /> },
  { name: 'Paid', active: false, icon: <AcceptedIcon /> },
];

export const billsTableHead = [
  {
    minWidth: '150',
    title: 'ID / Create Date',
    padding: 'default',
    sorting,
  },
  { minWidth: '200', title: 'Created by' },
  { minWidth: '150', title: 'Vendor', sorting },
  { minWidth: '150', title: 'Categories' },
  { minWidth: '160', title: 'Due Date', sorting },
  { minWidth: '100', title: 'Currency' },
  { minWidth: '140', title: 'Bill amount', align: 'right', sorting },
  { minWidth: '150', title: 'Paid amount', align: 'right' },
  { minWidth: '160', title: 'Unpaid amount', align: 'right' },
  { minWidth: '280', title: 'Approval status / Payment status' },
  {
    minWidth: '230',
    title: 'Paid from Legal entity',
    sorting,
  },
  {
    minWidth: '310',
    title: 'Paid from Bank account',
    sorting,
  },
  {
    minWidth: '350',
    title: 'Payment type → Pay to account',
    sorting,
  },
  {
    minWidth: '170',
    title: 'Treasury manager',
    sorting,
  },
  { minWidth: '100', title: 'Actions', align: 'center', padding: '4' },
];

export const paymentsTableHead = [
  {
    minWidth: '170',
    title: 'ID / Create Date',
    padding: 'default',
    sorting,
  },
  { minWidth: '180', title: 'Vendor', sorting },
  { minWidth: '170', title: 'Bills' },
  { minWidth: '170', title: 'Categories' },
  { minWidth: '100', title: 'Currency', sorting },
  { minWidth: '150', title: 'Payment amount', align: 'right', sorting },
  { minWidth: '200', title: 'Status / Date' },
  {
    minWidth: '250',
    title: 'Paid from Legal entity',
  },
  {
    minWidth: '310',
    title: 'Paid from Bank account',
  },
  {
    minWidth: '350',
    title: 'Payment type → Pay to account',
  },
  {
    minWidth: '190',
    title: 'Treasury manager',
    sorting,
  },
  { minWidth: '100', title: 'Actions', align: 'center', padding: '4' },
];

export const bankAccounts = {
  mainLabel: 'Bank of America',
  secondLabel: handleAccountLength('4830QZ995647769596875246278', 8),
  thirdLabel: 'USD',
  icon: <BankSuitcaseIcon />,
};
