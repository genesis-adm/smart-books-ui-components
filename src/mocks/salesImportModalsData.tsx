import { Checkbox } from 'Components/base/Checkbox';
import React from 'react';

type ColumnsType = {
  id: string;
  name: string;
  required?: boolean;
  unchecked?: boolean;
};
export const generalColumns: ColumnsType[] = [
  {
    id: 'transactionNumber',
    name: 'Sales transaction number',
    required: true,
  },
  {
    id: 'transactionDate',
    name: 'Transaction date',
    required: true,
  },
  {
    id: 'taxAgent',
    name: 'Is a tax agent?',
  },
];
export const customerColumns: ColumnsType[] = [
  {
    id: 'customer',
    name: 'Customer',
    required: true,
  },
  {
    id: 'customerEmail',
    name: 'Customer Email',
  },
  {
    id: 'currency',
    name: 'Currency',
    required: true,
  },
  {
    id: 'paymentTerms',
    name: 'Payment terms (number of days)',
  },
  {
    id: 'notes',
    name: 'Notes',
  },
];
export const productColumns: ColumnsType[] = [
  {
    id: 'product',
    name: 'Product (Name)',
    required: true,
  },
  {
    id: 'unitPrice',
    name: 'Unit Price',
    required: true,
  },
  {
    id: 'QTY',
    name: 'QTY',
    required: true,
  },
  {
    id: 'discount',
    name: 'Discount',
  },
];
export const taxColumns: ColumnsType[] = [
  {
    id: 'name',
    name: 'Tax name',
    required: true,
  },
  {
    id: 'rate',
    name: 'Tax rate',
    required: true,
  },
  {
    id: 'type',
    name: 'Tax Type',
    required: true,
  },
  {
    id: 'amount',
    name: 'Tax amount',
    required: true,
  },
];

export const feeColumns: ColumnsType[] = [
  {
    id: 'name',
    name: 'Fee name',
    required: true,
  },
  {
    id: 'feeRate',
    name: 'Fee rate',
    required: true,
  },
  {
    id: 'type',
    name: 'Fee Type',
  },
  {
    id: 'feeAmount',
    name: 'Fee amount',
    required: true,
  },
];
export const buyerInformationColumns: ColumnsType[] = [
  {
    id: 'name',
    name: 'Name',
  },
  {
    id: 'email',
    name: 'Email',
  },
  {
    id: 'currency',
    name: 'Currency',
  },
  {
    id: 'phone',
    name: 'Phone',
  },
];
export const buyerAddressColumns: ColumnsType[] = [
  {
    id: 'country',
    name: 'Country',
  },
  {
    id: 'state',
    name: 'State',
  },
  {
    id: 'city',
    name: 'City',
  },
  {
    id: 'zip',
    name: 'Zip Code',
  },
];

const sorting = {
  sortType: null,
  tooltipText: 'Sorting tooltip text',
};
export const tableHead = [
  {
    minWidth: '48',
    title: <Checkbox name="tickAll" checked onChange={() => {}} tick="unselect" />,
    padding: 'none',
  },
  { minWidth: '200', title: 'Sales transaction number*', sorting },
  { minWidth: '150', title: 'Transaction date*', sorting },
  { minWidth: '130', title: 'TAX calculations' },
  { minWidth: '180', title: 'Customer*' },
  { minWidth: '120', title: 'Tax agent' },
  { minWidth: '100', title: 'Currency*' },
  { minWidth: '280', title: 'Customer Email' },
  { minWidth: '140', title: 'Payment terms' },
  { minWidth: '300', title: 'Notes' },
  { minWidth: '180', title: 'Product* (Name)' },
  { minWidth: '100', title: 'QTY*' },
  { minWidth: '160', title: 'Unit Price*' },
  { minWidth: '160', title: 'Discount' },
  { minWidth: '180', title: 'Tax name*' },
  { minWidth: '160', title: 'Tax Type*' },
  { minWidth: '160', title: 'Tax rate*' },
  { minWidth: '160', title: 'Tax amount*' },
  { minWidth: '180', title: 'Fee name*', marketplace: true },
  { minWidth: '160', title: 'Fee Type*', marketplace: true },
  { minWidth: '160', title: 'Fee rate*', marketplace: true },
  { minWidth: '160', title: 'Fee amount*', marketplace: true },
  { minWidth: '180', title: 'Вuyer name*', marketplace: true },
  { minWidth: '130', title: 'Вuyer Currency', marketplace: true },
  { minWidth: '280', title: 'Вuyer Email', marketplace: true },
  { minWidth: '280', title: 'Вuyer Phone', marketplace: true },
  { minWidth: '260', title: 'Buyer Billing address - Country*', marketplace: true },
  { minWidth: '240', title: 'Buyer Billing address - State*', marketplace: true },
  { minWidth: '240', title: 'Buyer Billing address - City', marketplace: true },
  { minWidth: '240', title: 'Buyer Billing address - Zip Code', marketplace: true },
  { minWidth: '240', title: 'Buyer Shipping address - Country*', marketplace: true },
  { minWidth: '260', title: 'Buyer Shipping address - State/Region*', marketplace: true },
  { minWidth: '240', title: 'Buyer Shipping address - City', marketplace: true },
  { minWidth: '240', title: 'Buyer Shipping address - Zip Code', marketplace: true },
  { minWidth: '110', title: 'Status' },
  { minWidth: '120', title: 'Action', align: 'center' },
];
