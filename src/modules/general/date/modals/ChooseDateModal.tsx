import { DatePicker } from 'Components/base/DatePicker';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import classNames from 'classnames';
import { differenceInDays } from 'date-fns';
import React, { FC, useEffect, useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';
import { Nullable } from '../../../../types/general';

type ChooseDateModalProps = {
  title?: string;
  value?: Nullable<Date>;
  onSubmit?: (value: Nullable<Date>) => void;
  modalId?: string;
  containerId?: string;
  onClose: () => void;
};

const ChooseDateModal: FC<ChooseDateModalProps> = ({
  title = 'Please Add Date',
  value = null,
  onSubmit,
  onClose,
  modalId,
  containerId,
}: ChooseDateModalProps) => {
  const [date, setDate] = useState<Nullable<Date>>(value);

  const changeDate = () => {
    onSubmit?.(date);
    onClose();
  };

  useEffect(() => {
    if (value && date && !differenceInDays(value, date)) return;

    setDate(value);
  }, [value]);

  return (
    <Modal
      modalId={modalId}
      containerId={containerId}
      size="450"
      pluginScrollDisabled
      padding="24"
      header={
        <ModalHeaderNew
          title={title}
          height="60"
          fontType="body-medium"
          titlePosition="center"
          border
          onClose={onClose}
        />
      }
      footer={
        <ModalFooterNew border className={spacing.p_24}>
          <Button height="40" width="100" onClick={changeDate} disabled={!date}>
            Save
          </Button>
        </ModalFooterNew>
      }
    >
      <Container justifycontent="center" className={spacing.pt8}>
        <Container
          className={classNames(spacing.w320fixed)}
          radius
          flexdirection="column"
          background={'white-100'}
          style={{ boxShadow: '0px 10px 30px rgba(192, 196, 205, 0.3)' }}
          border="grey-20"
        >
          <DatePicker name="calendar" selected={date} onChange={setDate} />
        </Container>
      </Container>
    </Modal>
  );
};

export default ChooseDateModal;
