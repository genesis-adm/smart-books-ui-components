import { useCallback, useEffect } from 'react';

export interface ScrollOutsideProps {
  isListOpen: boolean;

  cb(arg: boolean): void;
}

export default function useScrollOutside({ isListOpen, cb }: ScrollOutsideProps): void {
  const handleScrollOutside = useCallback(
    (event: Event) => {
      const element = event.target as HTMLElement;
      if (
        isListOpen &&
        element.nodeName !== 'UL' &&
        element.nodeName !== 'INPUT' &&
        element.ariaLabel !== 'dialogContainer'
      ) {
        event.stopPropagation();
        cb(false);
      }
    },
    [isListOpen, cb],
  );
  useEffect(() => {
    document.addEventListener('scroll', handleScrollOutside, true);
    return () => document.removeEventListener('scroll', handleScrollOutside, true);
  }, [handleScrollOutside]);
}
