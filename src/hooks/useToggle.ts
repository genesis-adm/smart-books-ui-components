import { useCallback, useState } from 'react';

export const useToggle = (initialState = false) => {
  const [isOpen, setIsOpen] = useState<boolean>(initialState);

  const onOpen = useCallback(() => {
    setIsOpen(true);
  }, []);

  const onClose = useCallback(() => {
    setIsOpen(false);
  }, []);

  const onToggle = useCallback(() => {
    setIsOpen((prev) => !prev);
  }, []);

  return {
    onOpen,
    onClose,
    onToggle,
    isOpen,
  };
};
