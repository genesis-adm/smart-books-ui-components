import { RefObject, useEffect, useMemo, useRef, useState } from 'react';

export const useBounding = <T extends HTMLElement>(): {
  ref: RefObject<T>;
  domRect: DOMRect | undefined;
} => {
  const [domRect, setDom] = useState<DOMRect>();
  const ref = useRef<T>(null);

  useEffect(() => {
    const clientRect = ref?.current?.getBoundingClientRect();

    if (!clientRect) return;

    setDom(clientRect);
  }, []);

  return useMemo(
    () => ({
      ref,
      domRect,
    }),
    [domRect],
  );
};
