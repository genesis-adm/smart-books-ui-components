import { useEffect } from 'react';

export interface ClickOutsideProps {
  ref: HTMLDivElement | null;
  isDisable?: boolean;
  cb(arg: boolean): void;
}

export default function useClickOutside({ ref, cb, isDisable = false }: ClickOutsideProps): void {
  useEffect(() => {
    if (isDisable) return;

    function handleClickOutside(event: MouseEvent) {
      if (ref && !ref?.contains(event.target as Node)) {
        cb(false);
      }
    }

    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [ref]);
}
