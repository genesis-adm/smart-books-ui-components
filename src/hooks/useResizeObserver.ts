import { Dispatch, SetStateAction, useCallback, useLayoutEffect, useRef, useState } from 'react';

const useResizeObserver = (): [
  Dispatch<SetStateAction<HTMLElement | null>>,
  ResizeObserverEntry,
] => {
  const [observerEntry, setObserverEntry] = useState<ResizeObserverEntry>(
    {} as ResizeObserverEntry,
  );
  const [node, setNode] = useState<HTMLElement | null>(null);
  const observer = useRef<ResizeObserver>();

  const observe = useCallback(() => {
    observer.current = new ResizeObserver(([entry]: ResizeObserverEntry[]) =>
      setObserverEntry(entry),
    );
    if (node) observer.current.observe(node);
  }, [node]);

  const disconnect = useCallback(() => observer.current?.disconnect(), []);

  useLayoutEffect(() => {
    observe();
    return () => disconnect();
  }, [disconnect, observe]);

  return [setNode, observerEntry];
};

export default useResizeObserver;
