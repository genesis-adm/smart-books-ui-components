import { Dispatch, SetStateAction, useState } from 'react';

type UseSizeValue = {
  width: number;
  height: number;
};
const useSize = (
  initialState?: UseSizeValue,
): [UseSizeValue, Dispatch<SetStateAction<UseSizeValue>>] => {
  const [size, setSize] = useState(
    initialState || {
      width: 0,
      height: 0,
    },
  );

  return [size, setSize];
};

export default useSize;
