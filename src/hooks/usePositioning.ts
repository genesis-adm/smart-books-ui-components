import { CSSProperties, useEffect } from 'react';

import useResizeObserver from './useResizeObserver';

export type Axis = 'horizontal' | 'vertical';
// Axis means the overall direction of dropdown relatively to the wrapper
type DirectionsVerticalTypes = 'bottom-right' | 'bottom-left' | 'top-right' | 'top-left';
type DirectionsHorizontalTypes = 'right-bottom' | 'right-top' | 'left-bottom' | 'left-top';
export type PositionType = DirectionsHorizontalTypes | DirectionsVerticalTypes;

// Left part of a variable defines the direction of a dropdown, right part - side of a wrapper to which this dropdown is attached

function positionTypeHandler(rect: DOMRect, axis: Axis = 'vertical', separator = 2): PositionType {
  const verticalBottomPart = rect.y * separator > window.innerHeight;
  const horizontalRightPart = rect.x * separator > window.innerWidth;
  if (axis === 'vertical') {
    if (verticalBottomPart) {
      return horizontalRightPart ? 'top-right' : 'top-left';
    }
    return horizontalRightPart ? 'bottom-right' : 'bottom-left';
  } else {
    if (verticalBottomPart) {
      return horizontalRightPart ? 'left-bottom' : 'right-bottom';
    }
    return horizontalRightPart ? 'left-top' : 'right-top';
  }
}

export default function usePositioning(
  ref: Element | null,
  width: number | string,
  staticPosition: PositionType | null = null,
  axis: Axis = 'vertical',
) {
  const [setPlaceholderObserverEntry] = useResizeObserver();

  const clientRect = ref?.getBoundingClientRect();

  useEffect(() => {
    if (!ref) return;

    setPlaceholderObserverEntry(ref as HTMLElement);
  }, [ref, setPlaceholderObserverEntry]);

  if (!ref || !clientRect) return {};

  const mode = staticPosition || positionTypeHandler(clientRect, axis);
  const styles = {} as CSSProperties;

  styles.width = `${width}px`;

  switch (mode) {
    case 'bottom-left':
      styles.left = `${clientRect.left}px`;
      styles.top = `${clientRect.bottom + 10}px`;

      return {
        styles,
        mode,
      };
    case 'bottom-right':
      styles.right = `${window.innerWidth - clientRect.right}px`;
      styles.top = `${clientRect.bottom + 10}px`;
      return {
        styles,
        mode,
      };
    case 'top-left':
      styles.left = `${clientRect.left}px`;
      styles.bottom = `${window.innerHeight - clientRect.top + 10}px`;
      return {
        styles,
        mode,
      };
    case 'top-right':
      styles.right = `${window.innerWidth - clientRect.right}px`;
      styles.bottom = `${window.innerHeight - clientRect.top + 10}px`;
      return {
        styles,
        mode,
      };
    case 'left-bottom':
      styles.right = `${window.innerWidth - clientRect.left + 10}px`;
      styles.bottom = `${window.innerHeight - clientRect.bottom}px`;
      return {
        styles,
        mode,
      };
    case 'right-bottom':
      styles.left = `${clientRect.right + 10}px`;
      styles.bottom = `${window.innerHeight - clientRect.bottom}px`;
      return {
        styles,
        mode,
      };
    case 'left-top':
      styles.right = `${window.innerWidth - clientRect.left + 10}px`;
      styles.top = `${clientRect.top}px`;
      return {
        styles,
        mode,
      };
    case 'right-top':
      styles.left = `${clientRect.right + 10}px`;
      styles.top = `${clientRect.top}px`;
      return {
        styles,
        mode,
      };
  }
}
