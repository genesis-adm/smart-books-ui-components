import React, { ReactNode, useEffect, useState } from 'react';

export const useTooltipValue = (
  ref: React.RefObject<HTMLDivElement | HTMLSpanElement>,
  tooltipText: string | ReactNode,
) => {
  const [tooltipValue, setTooltipValue] = useState<React.ReactNode>();

  useEffect(() => {
    if (
      ref?.current &&
      (ref?.current.scrollWidth > ref?.current.clientWidth ||
        ref?.current.scrollHeight > ref?.current.clientHeight)
    ) {
      setTooltipValue(tooltipText);
    } else {
      setTooltipValue('');
    }
  }, [tooltipText, ref, ref.current]);

  return tooltipValue;
};
