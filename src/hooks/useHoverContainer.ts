import { useCallback, useMemo, useState } from 'react';

const useHoverContainer = () => {
  const [isHovered, setIsHovered] = useState(false);

  const hover = useCallback(() => {
    setIsHovered(true);
  }, []);

  const leave = useCallback(() => {
    setIsHovered(false);
  }, []);

  return useMemo(
    () => ({ isHovered, containerHoverProps: { onMouseEnter: hover, onMouseLeave: leave } }),
    [isHovered, hover, leave],
  );
};

export default useHoverContainer;
