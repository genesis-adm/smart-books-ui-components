import { useCallback, useRef } from 'react';

import useEventListener from './useEventListener';

type Axis = 'horizontal' | 'vertical';

const getPosition = (axis: Axis, coords: { x: number; y: number }) => {
  const position = {
    x: '',
    y: '',
  };

  if (axis === 'vertical') {
    if (
      coords.x * 1.4 >
      (window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth)
    ) {
      position.x = 'right';
    } else {
      position.x = 'left';
    }
    if (
      coords.y * 1.4 >
      (window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight)
    ) {
      position.y = 'top';
    } else {
      position.y = 'bottom';
    }
    return `${position.y}-${position.x}`;
  }
  if (
    coords.x * 1.25 >
    (window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth)
  ) {
    position.x = 'left';
  } else {
    position.x = 'right';
  }
  if (
    coords.y * 1.25 >
    (window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight)
  ) {
    position.y = 'bottom';
  } else {
    position.y = 'top';
  }
  return `${position.x}-${position.y}`;
};

const useDropDownPosition = (axis: Axis) => {
  const direction = useRef('');

  const handler = useCallback(
    ({ clientX, clientY }: MouseEvent) => {
      direction.current = getPosition(axis, {
        x: clientX,
        y: clientY,
      });
    },
    [axis],
  );

  useEventListener('mouseover', handler);

  return {
    handleDirection: () => direction.current,
  };
};

export default useDropDownPosition;
