import { useCallback, useMemo, useState } from 'react';

type Steps<StepId extends string> = Array<{ id: StepId; title: string }>;

export type UseSteps<StepId extends string> = {
  stepId: StepId;
  stepNumber: number;
  stepsLength: number;
  currentStepTitle: string;
  nextStepTitle: string;
  setStepId: (stepId: StepId) => void;
  isFirstStep: boolean;
  isLastStep: boolean;
  prevStep: () => void;
  nextStep: () => void;
};

const useSteps = <StepId extends string>(
  steps: Steps<StepId>,
  initialStepId?: StepId,
): UseSteps<StepId> => {
  const [stepId, setStepId] = useState<StepId>(initialStepId || steps[0].id);

  const stepsLength = steps?.length;

  const currentStepArrayIndex = steps?.map(({ id }) => id)?.indexOf(stepId);

  const isFirstStep = currentStepArrayIndex === 0;
  const isLastStep = steps.length === currentStepArrayIndex + 1;

  const stepNumber = currentStepArrayIndex + 1;

  const currentStepTitle = steps?.[currentStepArrayIndex]?.title;
  const nextStepTitle = steps?.[currentStepArrayIndex + 1]?.title || '';

  const prevStep = useCallback((): void => {
    if (currentStepArrayIndex === 0) return;

    const prevStepId = steps[currentStepArrayIndex - 1]?.id;

    setStepId(prevStepId);
  }, [steps, currentStepArrayIndex]);

  const nextStep = useCallback((): void => {
    const nextStepId = steps[currentStepArrayIndex + 1]?.id;

    if (!nextStepId) return;

    setStepId(nextStepId);
  }, [steps, currentStepArrayIndex]);

  return useMemo(
    () => ({
      stepId,
      stepNumber,
      stepsLength,
      currentStepTitle,
      nextStepTitle,
      setStepId,
      isFirstStep,
      isLastStep,
      prevStep,
      nextStep,
    }),
    [
      stepId,
      currentStepTitle,
      nextStepTitle,
      stepNumber,
      isFirstStep,
      isLastStep,
      prevStep,
      nextStep,
      stepsLength,
    ],
  );
};

export default useSteps;
