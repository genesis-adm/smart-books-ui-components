import { Dispatch, SetStateAction, useCallback, useMemo, useState } from 'react';

export type UseStateValues = {
  values: string[];
  setValues: Dispatch<SetStateAction<string[]>>;
  setAllValues: () => void;
  clearAllValues: () => void;
};

const useStateValues = (itemsValues: string[]): UseStateValues => {
  const [values, setValues] = useState<string[]>([]);

  const setAllValues = useCallback(() => {
    setValues(itemsValues);
  }, [itemsValues]);

  const clearAllValues = useCallback((): void => {
    setValues([]);
  }, []);

  return useMemo(
    () => ({
      values,
      setValues,
      setAllValues,
      clearAllValues,
    }),
    [values, setAllValues, clearAllValues],
  );
};

export default useStateValues;
