import { ReactNode } from 'react';

import { TooltipWidth } from './tooltips';

export type InputWidthType =
  | '66'
  | '80'
  | '84'
  | '90'
  | '96'
  | '110'
  | '112'
  | '114'
  | '120'
  | '124'
  | '130'
  | '134'
  | '138'
  | '140'
  | '144'
  | '150'
  | '157'
  | '170'
  | '172'
  | '180'
  | '190'
  | '192'
  | '200'
  | '215'
  | '220'
  | '230'
  | '248'
  | '260'
  | '270'
  | '280'
  | '300'
  | '311'
  | '320'
  | '338'
  | '354'
  | '370'
  | '375'
  | '408'
  | '416'
  | '480'
  | '502'
  | '542'
  | '552'
  | '624'
  | '672'
  | 'full';

export type InputHeightType = '24' | '28' | '36' | '48' | 'full'; // TODO full => delete

export type InputBackgroundType = 'grey-10' | 'white-100' | 'transparent';

export type InputError = {
  messages: (string | undefined)[];
};

export type InputBaseProps = {
  name: string;
  label: string;

  id?: string;
  tooltip?: ReactNode;
  tooltipWidth?: TooltipWidth;

  background?: InputBackgroundType;

  isLoading?: boolean;
  disabled?: boolean;
  readOnly?: boolean;
  required?: boolean;

  className?: string;

  error?: InputError;
};

export type InputColorType = 'default' | 'light';
export type InputBorderType = 'default' | 'light' | 'transparent';
