export type Nullable<T> = T | null;

export type FileType = 'excel' | 'image' | 'pdf' | 'csv';

export type OwnFile = File & {
  id?: number;
  fileManagerId?: string;
  src?: string;
};

export type TabItem<Key extends string = string> = {
  id: Key;
  label: string;
};

export interface TestableProps {
  dataTestId?: string;
}

export type WithTestId<T> = T & TestableProps;
