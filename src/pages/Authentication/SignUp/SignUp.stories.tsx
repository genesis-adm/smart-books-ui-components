import { Button } from 'Components/base/buttons/Button';
import { FormContainerNew } from 'Components/base/forms/FormContainerNew';
import { Container } from 'Components/base/grid/Container';
import { InputNew } from 'Components/base/inputs/InputNew';
import { InputPasswordNew } from 'Components/base/inputs/InputPasswordNew';
import { Text } from 'Components/base/typography/Text';
import { StartPageContainer } from 'Components/custom/Authentication/StartPageContainer';
import { Logo } from 'Components/elements/Logo';
import { PasswordCheck } from 'Components/elements/PasswordCheck';
import { ReactComponent as ArrowRightIcon } from 'Svg/v2/16/arrow-right.svg';
import React from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Authentication/Sign Up New',
};

export const SignUpNew: React.FC = () => (
  <StartPageContainer page="signUp" onPrivacyPolicyClick={() => {}} onTermsOfUseClick={() => {}}>
    <Container flexdirection="column" alignitems="center" gap="8">
      <Logo content="user" radius="rounded" name="vitaliikvasha@gmail.com" size="lg" />
      <Text type="body-medium">vitaliikvasha@gmail.com</Text>
      <Text type="body-regular-14" color="grey-100">
        Please add your name and surname
      </Text>
    </Container>

    <FormContainerNew className={spacing.mt24} gap="24" onSubmit={() => {}}>
      <Container gap="20">
        <InputNew
          name="firstName"
          label="First name"
          width="230"
          value="Vitalii"
          onChange={() => {}}
        />
        <InputNew
          name="lastName"
          label="Last name"
          width="230"
          value="Kvasha"
          onChange={() => {}}
        />
      </Container>
      <InputPasswordNew
        name="password"
        width="full"
        value="veryStrongPassword"
        onChange={() => {}}
        error={false}
      />
      <InputPasswordNew
        label="Confirm password"
        name="confirmPassword"
        width="full"
        value="veryStrongPassword"
        onChange={() => {}}
        error={false}
      />
      <Container flexdirection="column" gap="8">
        <PasswordCheck valid={false} conditionText="At least 8 characters" />
        <PasswordCheck valid conditionText="Both uppercase and lowercase letters" />
        <PasswordCheck
          valid={false}
          conditionText="At least one number and one special character"
        />
      </Container>
      <Button
        submit
        navigation
        iconRight={<ArrowRightIcon />}
        width="full"
        onClick={(e) => e.preventDefault()}
      >
        Save and continue
      </Button>
    </FormContainerNew>
  </StartPageContainer>
);
