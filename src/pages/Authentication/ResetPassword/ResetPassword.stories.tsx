import { Button } from 'Components/base/buttons/Button';
import { FormContainer } from 'Components/base/forms/FormContainer';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ControlPanel } from 'Components/custom/Login/ControlPanel';
import { PasswordCheck } from 'Components/elements/PasswordCheck';
import { Input } from 'Components/old/Input';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Authentication/Reset Password',
};

export const ResetPassword: React.FC = () => (
  <Container fullscreen background="violet-90" justifycontent="center" alignitems="center">
    <Container
      background="white-100"
      className={classNames(spacing.w920, spacing.pY60)}
      radius
      flexgrow="1"
      justifycontent="center"
    >
      <Container
        className={spacing.w450}
        flexdirection="column"
        alignitems="center"
        justifycontent="center"
        flexgrow="1"
      >
        <Text type="h1-semibold">Reset password</Text>
        <FormContainer onSubmit={() => {}}>
          <Input
            className={spacing.mt30}
            width="full"
            type="email"
            name="Email"
            placeholder="Dynamically inserted email"
            label="Email address"
            disabled
            isRequired
          />
          <Input
            className={spacing.mt15}
            width="full"
            type="password"
            name="Password"
            placeholder="Password"
            label="Enter password"
            isRequired
          />
          <Input
            className={spacing.mt15}
            width="full"
            type="password"
            name="PasswordConfirm"
            placeholder="Confirm password"
            label="Confirm password"
            isRequired
          />
          <PasswordCheck
            valid={false}
            conditionText="At least 8 characters"
            className={spacing.mt15}
          />
          <PasswordCheck
            valid
            conditionText="Both uppercase and lowercase letters"
            className={spacing.mt10}
          />
          <PasswordCheck
            valid={false}
            conditionText="At least one number and one special character"
            className={spacing.mt10}
          />
          <Button submit className={spacing.mt30} width="full" onClick={() => {}}>
            Update password
          </Button>
        </FormContainer>
      </Container>
    </Container>
    <ControlPanel onChangeSelect={() => {}} onClickPolicy={() => {}} onClickTerms={() => {}} />
  </Container>
);
