import { Scrollbar } from 'Components/base/Scrollbar';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ControlPanel } from 'Components/custom/Login/ControlPanel';
import ModalHeader from 'Components/modules/Modal/ModalHeader';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Authentication/Terms Of Use',
};

export const TermsOfUse: React.FC = () => (
  <Container fullscreen background="violet-90" justifycontent="center" alignitems="center">
    <Container
      background="white-100"
      className={classNames(spacing.w920, spacing.h75vh)}
      overflow="hidden"
      flexdirection="column"
      radius
      flexgrow="1"
      justifycontent="center"
    >
      <ModalHeader />
      <Container
        className={classNames(spacing.w100p, spacing.h100p)}
        flexdirection="column"
        alignitems="center"
      >
        <Text className={spacing.mt30} type="h2-semibold">
          Terms of use
        </Text>
        <Container className={classNames(spacing.w100p, spacing.h100p, spacing.mt20, spacing.pb30)}>
          <Scrollbar type="modal">
            <Container className={spacing.w100p} justifycontent="center">
              <Container className={spacing.w690} flexdirection="column">
                <Text type="body-regular-14" align="justify">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum mattis
                  hendrerit sapien, eget congue odio ornare placerat. Praesent eu porta mauris.
                  Quisque vitae nunc sit amet quam efficitur ultricies. Morbi pretium, enim nec
                  cursus mollis, ipsum erat consequat ligula, ut blandit nisl dui varius ipsum. Nunc
                  fringilla mauris tristique luctus accumsan. Phasellus at mauris eros. Fusce et leo
                  eget orci vulputate gravida non quis mi. Fusce viverra est lacus, at sodales orci
                  fermentum sit amet. Morbi sollicitudin purus magna.
                </Text>
                <Text className={spacing.mt20} type="body-regular-14" align="justify">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum mattis
                  hendrerit sapien, eget congue odio ornare placerat. Praesent eu porta mauris.
                  Quisque vitae nunc sit amet quam efficitur ultricies. Morbi pretium, enim nec
                  cursus mollis, ipsum erat consequat ligula, ut blandit nisl dui varius ipsum. Nunc
                  fringilla mauris tristique luctus accumsan. Phasellus at mauris eros. Fusce et leo
                  eget orci vulputate gravida non quis mi. Fusce viverra est lacus, at sodales orci
                  fermentum sit amet. Morbi sollicitudin purus magna.
                </Text>
                <Text className={spacing.mt20} type="body-regular-14" align="justify">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum mattis
                  hendrerit sapien, eget congue odio ornare placerat. Praesent eu porta mauris.
                  Quisque vitae nunc sit amet quam efficitur ultricies. Morbi pretium, enim nec
                  cursus mollis, ipsum erat consequat ligula, ut blandit nisl dui varius ipsum. Nunc
                  fringilla mauris tristique luctus accumsan. Phasellus at mauris eros. Fusce et leo
                  eget orci vulputate gravida non quis mi. Fusce viverra est lacus, at sodales orci
                  fermentum sit amet. Morbi sollicitudin purus magna.
                </Text>
                <Text className={spacing.mt20} type="body-regular-14" align="justify">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum mattis
                  hendrerit sapien, eget congue odio ornare placerat. Praesent eu porta mauris.
                  Quisque vitae nunc sit amet quam efficitur ultricies. Morbi pretium, enim nec
                  cursus mollis, ipsum erat consequat ligula, ut blandit nisl dui varius ipsum. Nunc
                  fringilla mauris tristique luctus accumsan. Phasellus at mauris eros. Fusce et leo
                  eget orci vulputate gravida non quis mi. Fusce viverra est lacus, at sodales orci
                  fermentum sit amet. Morbi sollicitudin purus magna.
                </Text>
                <Text className={spacing.mt20} type="body-regular-14" align="justify">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum mattis
                  hendrerit sapien, eget congue odio ornare placerat. Praesent eu porta mauris.
                  Quisque vitae nunc sit amet quam efficitur ultricies. Morbi pretium, enim nec
                  cursus mollis, ipsum erat consequat ligula, ut blandit nisl dui varius ipsum. Nunc
                  fringilla mauris tristique luctus accumsan. Phasellus at mauris eros. Fusce et leo
                  eget orci vulputate gravida non quis mi. Fusce viverra est lacus, at sodales orci
                  fermentum sit amet. Morbi sollicitudin purus magna.
                </Text>
                <Text className={spacing.mt20} type="body-regular-14" align="justify">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum mattis
                  hendrerit sapien, eget congue odio ornare placerat. Praesent eu porta mauris.
                  Quisque vitae nunc sit amet quam efficitur ultricies. Morbi pretium, enim nec
                  cursus mollis, ipsum erat consequat ligula, ut blandit nisl dui varius ipsum. Nunc
                  fringilla mauris tristique luctus accumsan. Phasellus at mauris eros. Fusce et leo
                  eget orci vulputate gravida non quis mi. Fusce viverra est lacus, at sodales orci
                  fermentum sit amet. Morbi sollicitudin purus magna.
                </Text>
                <Text className={spacing.mt20} type="body-regular-14" align="justify">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum mattis
                  hendrerit sapien, eget congue odio ornare placerat. Praesent eu porta mauris.
                  Quisque vitae nunc sit amet quam efficitur ultricies. Morbi pretium, enim nec
                  cursus mollis, ipsum erat consequat ligula, ut blandit nisl dui varius ipsum. Nunc
                  fringilla mauris tristique luctus accumsan. Phasellus at mauris eros. Fusce et leo
                  eget orci vulputate gravida non quis mi. Fusce viverra est lacus, at sodales orci
                  fermentum sit amet. Morbi sollicitudin purus magna.
                </Text>
                <Text className={spacing.mt20} type="body-regular-14" align="justify">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum mattis
                  hendrerit sapien, eget congue odio ornare placerat. Praesent eu porta mauris.
                  Quisque vitae nunc sit amet quam efficitur ultricies. Morbi pretium, enim nec
                  cursus mollis, ipsum erat consequat ligula, ut blandit nisl dui varius ipsum. Nunc
                  fringilla mauris tristique luctus accumsan. Phasellus at mauris eros. Fusce et leo
                  eget orci vulputate gravida non quis mi. Fusce viverra est lacus, at sodales orci
                  fermentum sit amet. Morbi sollicitudin purus magna.
                </Text>
                <Text className={spacing.mt20} type="body-regular-14" align="justify">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum mattis
                  hendrerit sapien, eget congue odio ornare placerat. Praesent eu porta mauris.
                  Quisque vitae nunc sit amet quam efficitur ultricies. Morbi pretium, enim nec
                  cursus mollis, ipsum erat consequat ligula, ut blandit nisl dui varius ipsum. Nunc
                  fringilla mauris tristique luctus accumsan. Phasellus at mauris eros. Fusce et leo
                  eget orci vulputate gravida non quis mi. Fusce viverra est lacus, at sodales orci
                  fermentum sit amet. Morbi sollicitudin purus magna.
                </Text>
              </Container>
            </Container>
          </Scrollbar>
        </Container>
      </Container>
    </Container>
    <ControlPanel onChangeSelect={() => {}} onClickPolicy={() => {}} onClickTerms={() => {}} />
  </Container>
);
