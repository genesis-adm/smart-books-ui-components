import { Button } from 'Components/base/buttons/Button';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { FormContainer } from 'Components/base/forms/FormContainer';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ControlPanel } from 'Components/custom/Login/ControlPanel';
import { Input } from 'Components/old/Input';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Authentication/Forgot Password',
};

export const ForgotPassword: React.FC = () => (
  <Container fullscreen background="violet-90" justifycontent="center" alignitems="center">
    <Container
      background="white-100"
      className={classNames(spacing.w920, spacing.pY60)}
      radius
      flexgrow="1"
      justifycontent="center"
    >
      <Container
        className={spacing.w450}
        flexdirection="column"
        alignitems="center"
        justifycontent="center"
        flexgrow="1"
      >
        <Text align="center" type="h1-semibold">
          Forgot password?
        </Text>
        <FormContainer onSubmit={() => {}}>
          <Input
            className={spacing.mt30}
            width="full"
            type="email"
            name="Email"
            placeholder="Email address"
            label="Enter email address"
            isRequired
          />
          <Button submit className={spacing.mt30} width="full" onClick={() => {}}>
            Reset password
          </Button>
        </FormContainer>
        <Container className={spacing.mt20} justifycontent="center">
          <Text type="body-regular-14" color="grey-100">
            Don&apos;t have an account?&nbsp;
          </Text>
          <LinkButton onClick={() => {}}>Sign up</LinkButton>
        </Container>
        <Container className={spacing.mt5} justifycontent="center">
          <Text type="body-regular-14" color="grey-100">
            Already have an account?&nbsp;
          </Text>
          <LinkButton onClick={() => {}}>Sign in</LinkButton>
        </Container>
      </Container>
    </Container>
    <ControlPanel onChangeSelect={() => {}} onClickPolicy={() => {}} onClickTerms={() => {}} />
  </Container>
);
