import { Checkbox } from 'Components/base/Checkbox';
import { Divider } from 'Components/base/Divider';
import { Icon } from 'Components/base/Icon';
import { Button } from 'Components/base/buttons/Button';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { FormContainerNew } from 'Components/base/forms/FormContainerNew';
import { Container } from 'Components/base/grid/Container';
import { InputNew } from 'Components/base/inputs/InputNew';
import { InputPasswordNew } from 'Components/base/inputs/InputPasswordNew';
import { Text } from 'Components/base/typography/Text';
import { StartPageContainer } from 'Components/custom/Authentication/StartPageContainer';
import { Input } from 'Components/old/Input';
import { ReactComponent as ErrorIcon } from 'Svg/24/error.svg';
import { ReactComponent as EnvelopeFilledIcon } from 'Svg/v2/16/envelope-filled.svg';
import { ReactComponent as GoogleIcon } from 'Svg/v2/16/google.svg';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Authentication/Sign In',
};

export const SignIn: React.FC = () => (
  <StartPageContainer
    page="signIn"
    title="Sign in"
    onPrivacyPolicyClick={() => {}}
    onTermsOfUseClick={() => {}}
  >
    {/* <Button
      className={spacing.mt32}
      width="full"
      color="black-100"
      background="white-100"
      iconLeft={<GoogleIcon />}
      sourceIconColorLeft
      shadow="default"
      onClick={() => {}}
    >
      Login with Google
    </Button>
    <Container className={spacing.mt24} justifycontent="center" alignitems="center">
      <Divider type="horizontal" position="left" />
      <Text type="body-regular-14">or</Text>
      <Divider type="horizontal" position="right" />
    </Container> */}
    <FormContainerNew className={spacing.mt24} onSubmit={() => {}}>
      <InputNew
        name="email"
        label="Email address"
        width="full"
        value="admin@smartbooks.dev"
        icon={<Icon icon={<EnvelopeFilledIcon />} clickThrough />}
        elementPosition="left"
        onChange={() => {}}
      />
      <InputPasswordNew
        className={spacing.mt24}
        name="password"
        width="full"
        value="veryStrongPassword"
        onChange={() => {}}
        error={false}
      />
      {/* <Container className={spacing.mt16}>
        <Text type="body-regular-14" color="grey-100">
          Forgot password?&nbsp;
        </Text>
        <LinkButton onClick={() => {}}>Reset password</LinkButton>
      </Container> */}
      {/* <Container className={spacing.mt24}>
        <Checkbox
          name="keepsigned"
          font="text-regular"
          onChange={() => {}}
          textcolor="grey-100"
          checked={false}
        >
          Keep me signed in on this computer
        </Checkbox>
      </Container> */}
      <Button submit className={spacing.mt32} width="full" onClick={() => {}}>
        Sign in
      </Button>
      {/* <Container className={spacing.mt32} justifycontent="center">
        <Text type="body-regular-14" color="grey-100">
          Don&apos;t have an account?&nbsp;
        </Text>
        <LinkButton onClick={() => {}}>Sign up</LinkButton>
      </Container> */}
    </FormContainerNew>
  </StartPageContainer>
);
