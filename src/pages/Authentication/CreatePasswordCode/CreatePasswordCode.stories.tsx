import { Button } from 'Components/base/buttons/Button';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { FormContainer } from 'Components/base/forms/FormContainer';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ControlPanel } from 'Components/custom/Login/ControlPanel';
import { InputCodeGroup } from 'Components/custom/Login/InputCodeGroup';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Authentication/Create Password Code',
};

export const CreatePasswordCode: React.FC = () => (
  <Container fullscreen background="violet-90" justifycontent="center" alignitems="center">
    <Container
      background="white-100"
      className={classNames(spacing.w920, spacing.pY60)}
      radius
      flexgrow="1"
      justifycontent="center"
    >
      <Container className={spacing.w450} flexdirection="column" alignitems="center">
        <Text align="center" type="h1-semibold">
          Protect your data
        </Text>
        <Text align="center" type="h1-semibold">
          with 2-step authentication
        </Text>
        <Text className={spacing.mt30} align="center" type="body-regular-14">
          A message with a confirmation code has been sent to the email
        </Text>
        <Text align="center" type="body-regular-14">
          karine.mnatsakanian@mymail.com
        </Text>
        <FormContainer onSubmit={() => {}} alignitems="center">
          <InputCodeGroup className={spacing.mt30} onChange={() => {}} />
          <Button submit className={spacing.mt30} width="xl" height="large" onClick={() => {}}>
            Protect account
          </Button>
        </FormContainer>
        <Text className={spacing.mt30} type="text-regular">
          If you don&apos;t receive a code please
        </Text>
        <Text className={spacing.mt10} type="text-medium">
          Resend email after 04:54
        </Text>
        <LinkButton className={spacing.mt10} type="text-regular" onClick={() => {}}>
          Resend email
        </LinkButton>
      </Container>
    </Container>
    <ControlPanel onChangeSelect={() => {}} onClickPolicy={() => {}} onClickTerms={() => {}} />
  </Container>
);
