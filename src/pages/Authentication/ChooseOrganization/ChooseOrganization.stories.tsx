import { Scrollbar } from 'Components/base/Scrollbar';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ControlPanel } from 'Components/custom/Login/ControlPanel';
import { LoginUserPanel } from 'Components/custom/Login/LoginUserPanel';
import { OrganisationLaunch } from 'Components/elements/OrganisationLaunch';
import { Search } from 'Components/inputs/Search';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Authentication/Choose Organization',
};

export const ChooseOrganization: React.FC = () => (
  <Container fullscreen background="violet-90" justifycontent="center" alignitems="center">
    <Container
      background="white-100"
      className={classNames(spacing.w920, spacing.p24, spacing.h65vh)}
      radius
      flexgrow="1"
      justifycontent="center"
    >
      <LoginUserPanel
        name="Karine Mnatsakanian"
        onClickSettings={() => {}}
        onClickLogout={() => {}}
      />
      <Container
        className={spacing.w640}
        flexdirection="column"
        alignitems="center"
        justifycontent="center"
        flexgrow="1"
      >
        <Text className={spacing.mt90} type="h1-semibold">
          Choose organization
        </Text>
        <LinkButton className={spacing.mt20} icon={<PlusIcon />} onClick={() => {}}>
          Create organization account
        </LinkButton>
        <Search
          width="full"
          className={spacing.mt40}
          name="search"
          placeholder="Start searching here..."
        />
        <Container
          flexdirection="column"
          className={classNames(spacing.mt20, spacing.w100p, spacing.h200)}
          flexgrow="1"
        >
          <Scrollbar type="modal">
            <OrganisationLaunch
              name="Genesis"
              type="Organisation"
              background="white-100"
              logo="https://picsum.photos/60"
              onLaunch={() => {}}
            />
            <OrganisationLaunch
              name="Genesis"
              type="Organisation"
              background="white-100"
              logo="https://picsum.photos/70"
              onLaunch={() => {}}
            />
            <OrganisationLaunch
              name="Genesis"
              type="Organisation"
              background="white-100"
              logo="https://picsum.photos/80"
              onLaunch={() => {}}
            />
            <OrganisationLaunch
              name="long long name capitalized"
              type="Organisation"
              background="white-100"
              onLaunch={() => {}}
            />
            <OrganisationLaunch
              name="JiJi"
              type="Organisation"
              background="white-100"
              color="#FF7052"
              onLaunch={() => {}}
            />
            <OrganisationLaunch
              name="Genesis"
              type="Organisation"
              background="white-100"
              logo="https://picsum.photos/200"
              onLaunch={() => {}}
            />
          </Scrollbar>
        </Container>
      </Container>
    </Container>
    <ControlPanel onChangeSelect={() => {}} onClickPolicy={() => {}} onClickTerms={() => {}} />
  </Container>
);
