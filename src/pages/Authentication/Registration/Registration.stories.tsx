import { Divider } from 'Components/base/Divider';
import { Button } from 'Components/base/buttons/Button';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { FormContainer } from 'Components/base/forms/FormContainer';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ControlPanel } from 'Components/custom/Login/ControlPanel';
import { PasswordCheck } from 'Components/elements/PasswordCheck';
import { Input } from 'Components/old/Input';
import { ReactComponent as GoogleIcon } from 'Svg/16/google.svg';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Authentication/Sign Up Step 1',
};

export const SignUpStep1: React.FC = () => (
  <Container fullscreen background="violet-90" justifycontent="center" alignitems="center">
    <Container
      background="white-100"
      className={classNames(spacing.w920, spacing.pt50, spacing.pb35)}
      radius
      flexgrow="1"
      justifycontent="center"
    >
      <Container
        className={spacing.w450}
        flexdirection="column"
        alignitems="center"
        justifycontent="center"
        flexgrow="1"
      >
        <Text type="h1-semibold">Create your account</Text>
        <Button className={spacing.mt30} width="full" iconLeft={<GoogleIcon />} onClick={() => {}}>
          Sign up with Google
        </Button>
        <Container className={spacing.mt15} justifycontent="center" alignitems="center">
          <Divider type="horizontal" position="left" />
          <Text type="body-regular-14">or</Text>
          <Divider type="horizontal" position="right" />
        </Container>
        <FormContainer onSubmit={() => {}}>
          <Container className={spacing.mt15} justifycontent="space-between">
            <Input
              name="FirstName"
              width="minimal"
              placeholder="Enter first name"
              label="First name"
              isRequired
            />
            <Input
              name="LastName"
              width="minimal"
              placeholder="Enter last name"
              label="Last name"
              isRequired
            />
          </Container>
          <Input
            className={spacing.mt15}
            width="full"
            type="email"
            name="Email"
            placeholder="Enter email address"
            label="Email address"
            isRequired
          />
          <Input
            className={spacing.mt15}
            width="full"
            type="password"
            name="Password"
            placeholder="Enter password"
            label="Create password"
            isRequired
          />
          <PasswordCheck
            valid={false}
            conditionText="At least 8 characters"
            className={spacing.mt15}
          />
          <PasswordCheck
            valid
            conditionText="Both uppercase and lowercase letters"
            className={spacing.mt10}
          />
          <PasswordCheck
            valid={false}
            conditionText="At least one number and one special character"
            className={spacing.mt10}
          />
          <Button submit className={spacing.mt30} width="full" onClick={() => {}}>
            Create account
          </Button>
        </FormContainer>
        <Container className={spacing.mt20} justifycontent="center">
          <Text type="body-regular-14" color="grey-100">
            Already have a SmartBooks account?&nbsp;
          </Text>
          <LinkButton onClick={() => {}}>Login</LinkButton>
        </Container>
      </Container>
    </Container>
    <ControlPanel onChangeSelect={() => {}} onClickPolicy={() => {}} onClickTerms={() => {}} />
  </Container>
);
