import { Meta, Story } from '@storybook/react';
import { ContentLoader } from 'Components/base/ContentLoader';
import { Container } from 'Components/base/grid/Container';
import { MainPageContentContainer } from 'Components/base/grid/MainPageContentContainer';
import { MenuTab, MenuTabWrapper } from 'Components/base/tabs/MenuTab';
import { Text } from 'Components/base/typography/Text';
import { Pagination } from 'Components/custom/Stories/Pagination/Pagination';
import PerPageSelect from 'Components/custom/Stories/Pagination/PerPageSelect/PerPageSelect';
import { DevInProgressTooltip } from 'Components/elements/DevInProgressTooltip';
import { rowsPerPageOptions } from 'Mocks/fakeOptions';
import ImportPageFilters from 'Pages/Import/components/ImportPageFilters';
import ImportPageHeader from 'Pages/Import/components/ImportPageHeader';
import ImportPageTable from 'Pages/Import/components/ImportPageTable';
import AppleSettingsModal from 'Pages/Import/modals/AppleSettingsModal/AppleSettingsModal';
import { tabs } from 'Pages/Integration/ExternalCabinet/modals/CreateIntegration/constants';
import React, { useState } from 'react';

import spacing from '../../assets/styles/spacing.module.scss';
import { useToggle } from '../../hooks/useToggle';

const ImportPage: Story = ({ storyState }) => {
  const isLoading = storyState === 'loading';
  const {
    isOpen: isOpenAppleTaxDictionaryModal,
    onClose: onCloseAppleTaxDictionaryModal,
    onOpen: onOpenAppleTaxDictionaryModal,
  } = useToggle();

  const [rowsPerPage, setRowsPerPage] = useState<string>(rowsPerPageOptions[0].value);

  return (
    <Container fullscreen flexwrap="nowrap" background={'grey-10'} flexdirection="column">
      <Container flexdirection="column" gap="16" className={spacing.pX24}>
        <ImportPageHeader
          isLoading={isLoading}
          onOpenAppleSettingsModal={onOpenAppleTaxDictionaryModal}
        />
        <ContentLoader isLoading={isLoading} height="28px" width="362px">
          <MenuTabWrapper>
            {tabs.map(({ id, icon, title, disabled }) => (
              <DevInProgressTooltip key={id} disabled={!disabled}>
                <MenuTab
                  id="id"
                  active={id === 'sales'}
                  icon={icon}
                  disabled={disabled}
                  onClick={() => {}}
                >
                  {title}
                </MenuTab>
              </DevInProgressTooltip>
            ))}
          </MenuTabWrapper>
        </ContentLoader>
      </Container>
      <MainPageContentContainer>
        <ImportPageFilters isLoading={isLoading} />
        <ImportPageTable isLoading={isLoading} />
        {!isLoading && (
          <Pagination
            padding="16"
            customPagination={
              <Container gap="16" alignitems="center">
                <Text type="subtext-regular" color="grey-100">
                  Rows per Page
                </Text>
                <PerPageSelect
                  selectedValue={rowsPerPage}
                  onOptionClick={(value) => setRowsPerPage(value)}
                />
              </Container>
            }
          />
        )}
      </MainPageContentContainer>

      {isOpenAppleTaxDictionaryModal && (
        <AppleSettingsModal isLoaded isLoading={false} onClose={onCloseAppleTaxDictionaryModal} />
      )}
    </Container>
  );
};

export default {
  title: 'Pages/Import page',
  component: ImportPage,
  parameters: {
    layout: 'fullscreen',
  },
  argTypes: {
    storyState: {
      options: ['empty', 'loading', 'loaded'],
      control: {
        type: 'radio',
        labels: {
          empty: 'Empty page',
          loading: 'Page is loading',
          loaded: 'Page loaded',
        },
      },
    },
  },
} as Meta;

export const Import = ImportPage.bind({});
Import.args = {
  storyState: 'loaded',
};
