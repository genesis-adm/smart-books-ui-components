import { TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { TableCellWidth } from 'Components/types/gridTypes';

export const integratorFilterOptions: { value: string; title: string }[] = [
  { value: 'selectAll', title: 'Select All' },
  { value: 'google', title: 'Google Play' },
  { value: 'apple', title: 'Apple' },
];
export const nonClassifiedSKUFilterOptions: { value: string; title: string }[] = [
  { value: 'selectAll', title: 'Select All' },
  { value: 'yes', title: 'Yes' },
  { value: 'no', title: 'No' },
];
export const statusFilterOptions: { value: string; title: string }[] = [
  { value: 'selectAll', title: 'Select All' },
  { value: 'active', title: 'Active' },
  { value: 'disabled', title: 'Disabled' },
  { value: 'error', title: 'Error' },
];
export const cabinetNameFilterOptions: { value: string; title: string }[] = [
  { value: 'selectAll', title: 'Select All' },
  { value: 'cabinetName-1', title: 'Cabinet name 1' },
  { value: 'cabinetName-2', title: 'Cabinet name 2' },
  { value: 'cabinetName-3', title: 'Cabinet name 3' },
  { value: 'cabinetName-4', title: 'Cabinet name 4' },
  { value: 'cabinetName-5', title: 'Cabinet name 5' },
  { value: 'cabinetName-6', title: 'Cabinet name 6' },
];
export const createdByFilterOptions: { value: string; title: string; secondaryTitle?: string }[] = [
  { value: 'userName-1', title: 'User name 1', secondaryTitle: 'user1email@gmail.com' },
  { value: 'userName-2', title: 'User name 2', secondaryTitle: 'user2email@gmail.com' },
  { value: 'userName-3', title: 'User name 3', secondaryTitle: 'user3email@gmail.com' },
  { value: 'userName-4', title: 'User name 4', secondaryTitle: 'user4email@gmail.com' },
  { value: 'userName-5', title: 'User name 5', secondaryTitle: 'user5email@gmail.com' },
  { value: 'userName-6', title: 'User name 6', secondaryTitle: 'user6email@gmail.com' },
];
const sorting = {
  sortType: null,
  tooltipText: 'Sorting tooltip text',
};

export const tableHead: {
  minWidth: TableCellWidth;
  title: string;
  align?: TableTitleNewProps['align'];
  sorting?: TableTitleNewProps['sorting'];
}[] = [
  {
    minWidth: '110',
    title: 'Integrator',
    sorting,
  },
  {
    minWidth: '180',
    title: 'Cabinet name',
    sorting,
  },
  {
    minWidth: '200',
    title: 'Legal Entity',
    sorting,
  },
  {
    minWidth: '180',
    title: 'Customer',
    sorting,
  },
  {
    minWidth: '200',
    title: 'Created by',
    sorting,
  },
  {
    minWidth: '160',
    title: 'Non-classified SKU',
    sorting,
  },
  {
    minWidth: '120',
    title: 'Last update',
    sorting,
  },
  {
    minWidth: '88',
    title: 'Status',
    sorting,
    align: 'center',
  },
  {
    minWidth: '105',
    title: 'Actions',
    align: 'center',
  },
];
