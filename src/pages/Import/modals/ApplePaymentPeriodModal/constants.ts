import { TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { ApplePaymentPeriodModalStepId } from 'Pages/Import/modals/ApplePaymentPeriodModal/ApplePaymentPeriodModal.types';
import { endOfMonth, startOfMonth } from 'date-fns';

export const APPLE_PAYMENT_PERIOD_MODAL_ID = 'APPLE_PAYMENT_PERIOD_MODAL';

export const applePaymentPeriodYearAlreadyUsedTooltipMessage =
  'You have already used this Fiscal year';

export const applePaymentPeriodYearTooltipMessage = 'Select Fiscal year';

export const applePaymentPeriodModalSteps: { id: ApplePaymentPeriodModalStepId; title: string }[] =
  [
    { id: 'fiscalYear', title: 'Please choose a Fiscal year' },
    { id: 'paymentPeriod', title: 'Create new Payment period' },
  ];

export const applePaymentPeriodTableColumns: TableTitleNewProps[] = [
  {
    minWidth: '145',
    title: 'Period',
  },
  {
    minWidth: '145',
    title: 'Start Date',
    fixedWidth: true,
  },
  {
    minWidth: '145',
    title: 'End Date',
    fixedWidth: true,
  },
];

export const applePaymentPeriodTableRows = [
  {
    period: 'Oct',
    startDate: startOfMonth(new Date(2024, 9, 1)),
    endDate: endOfMonth(new Date(2024, 9, 2)),
  },
  {
    period: 'Nov',
    startDate: startOfMonth(new Date(2024, 10, 2)),
    endDate: endOfMonth(new Date(2024, 10, 2)),
  },
  {
    period: 'Dec',
    startDate: startOfMonth(new Date(2024, 11, 1)),
    endDate: endOfMonth(new Date(2024, 11, 1)),
  },
  {
    period: 'Jan',
    startDate: startOfMonth(new Date(2025, 0, 1)),
    endDate: endOfMonth(new Date(2024, 0, 2)),
  },
  {
    period: 'Feb',
    startDate: startOfMonth(new Date(2025, 1, 1)),
    endDate: endOfMonth(new Date(2024, 1, 2)),
  },
  {
    period: 'Mar',
    startDate: startOfMonth(new Date(2025, 2, 1)),
    endDate: endOfMonth(new Date(2024, 2, 2)),
  },
  {
    period: 'Apr',
    startDate: startOfMonth(new Date(2025, 3, 1)),
    endDate: endOfMonth(new Date(2024, 3, 2)),
  },
  {
    period: 'May',
    startDate: startOfMonth(new Date(2025, 4, 1)),
    endDate: endOfMonth(new Date(2024, 4, 2)),
  },
  {
    period: 'Jun',
    startDate: startOfMonth(new Date(2025, 5, 1)),
    endDate: endOfMonth(new Date(2024, 5, 2)),
  },
  {
    period: 'Jul',
    startDate: startOfMonth(new Date(2025, 6, 1)),
    endDate: endOfMonth(new Date(2024, 6, 2)),
  },
  {
    period: 'Aug',
    startDate: startOfMonth(new Date(2025, 7, 1)),
    endDate: endOfMonth(new Date(2024, 7, 2)),
  },
  {
    period: 'Sep',
    startDate: startOfMonth(new Date(2025, 8, 1)),
    endDate: endOfMonth(new Date(2024, 8, 2)),
  },
];

export const applePaymentPeriodModalDataByMode = {
  create: {
    headerTitle: 'Create new Payment period',
  },
  view: {
    headerTitle: 'Payment period',
  },
};
