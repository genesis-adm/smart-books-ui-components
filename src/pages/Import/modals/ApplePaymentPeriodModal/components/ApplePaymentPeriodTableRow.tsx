import { InputDate } from 'Components/base/inputs/InputDate';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import {
  applePaymentPeriodTableColumns,
  applePaymentPeriodTableRows,
} from 'Pages/Import/modals/ApplePaymentPeriodModal/constants';
import React, { FC, ReactElement, useState } from 'react';

import { Nullable } from '../../../../../types/general';

type ApplePaymentPeriodTableRowProps = {
  index: number;
  isView?: boolean;

  year: Nullable<number>;
};

const ApplePaymentPeriodTableRow: FC<ApplePaymentPeriodTableRowProps> = ({
  index,
  isView,
  year,
}: ApplePaymentPeriodTableRowProps): ReactElement => {
  const [startDate, setStartDate] = useState<Nullable<Date>>(
    isView ? new Date(applePaymentPeriodTableRows[index].startDate) : null,
  );
  const [endDate, setEndDate] = useState<Nullable<Date>>(
    isView ? new Date(applePaymentPeriodTableRows[index].endDate) : null,
  );

  const period = applePaymentPeriodTableRows[index].period;

  return (
    <TableRowNew gap="8">
      <TableCellTextNew
        minWidth={applePaymentPeriodTableColumns[0]?.minWidth}
        value={`${index + 1}. ${period} ${year ? (index > 2 ? year + 1 : year) : ''}`}
      />
      <TableCellNew
        minWidth={applePaymentPeriodTableColumns[1]?.minWidth}
        alignitems="flex-start"
        padding="8"
      >
        <InputDate
          name="startDate"
          selected={startDate}
          onChange={setStartDate}
          placeholder="MM-DD-YYYY"
          dateFormat="MM-dd-yyyy"
          label="Start date"
          width="134"
          height="36"
          calendarStartDay={1}
          readOnly={isView}
          error={
            index === 3 && !isView && !startDate
              ? { messages: ['Kindly ensure all fields are completed before saving'] }
              : undefined
          }
        />
      </TableCellNew>
      <TableCellNew
        minWidth={applePaymentPeriodTableColumns[2]?.minWidth}
        alignitems="flex-start"
        padding="8"
      >
        <InputDate
          name="endDate"
          selected={endDate}
          onChange={setEndDate}
          placeholder="MM-DD-YYYY"
          dateFormat="MM-dd-yyyy"
          label="End date"
          width="134"
          height="36"
          calendarStartDay={1}
          readOnly={isView}
          error={
            index === 1 && !isView && !endDate
              ? {
                  messages: [
                    'The start date should be set as the day following the end date of the previous period.',
                  ],
                }
              : undefined
          }
        />
      </TableCellNew>
    </TableRowNew>
  );
};

export default ApplePaymentPeriodTableRow;
