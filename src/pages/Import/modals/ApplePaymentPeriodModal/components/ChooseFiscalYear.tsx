import { ContentLoader } from 'Components/base/ContentLoader';
import { Container } from 'Components/base/grid/Container';
import ApplePaymentPeriodYear from 'Pages/Import/modals/ApplePaymentPeriodModal/components/ApplePaymentPeriodYear';
import {
  applePaymentPeriodYearAlreadyUsedTooltipMessage,
  applePaymentPeriodYearTooltipMessage,
} from 'Pages/Import/modals/ApplePaymentPeriodModal/constants';
import React, { FC, ReactElement } from 'react';

import { Nullable } from '../../../../../types/general';

type ChooseFiscalYearProps = {
  value?: Nullable<number>;
  onChange?: (year: Nullable<number>) => void;
  isLoading?: boolean;
};

const ChooseFiscalYear: FC<ChooseFiscalYearProps> = ({
  value,
  onChange,
  isLoading = false,
}: ChooseFiscalYearProps): ReactElement => {
  return (
    <Container gap="16" flexwrap="wrap" justifycontent="center">
      {[
        2024, 2023, 2022, 2021, 2020, 2019, 2018, 2017, 2016, 2015, 2014, 2013, 2012, 2011, 2010,
        2009,
      ].map((year, index) => (
        <ContentLoader key={year} width="90px" height="62px" isLoading={isLoading}>
          <ApplePaymentPeriodYear
            value={year}
            disabled={!index}
            checked={value === year}
            onChange={onChange}
            tooltip={
              !index
                ? applePaymentPeriodYearAlreadyUsedTooltipMessage
                : applePaymentPeriodYearTooltipMessage
            }
          />
        </ContentLoader>
      ))}
    </Container>
  );
};

export default ChooseFiscalYear;
