import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import ModalHeaderPlaceholder from 'Components/custom/placeholders/ModalHeaderPlaceholder';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import {
  ApplePaymentPeriodModalMode,
  ApplePaymentPeriodModalStepId,
} from 'Pages/Import/modals/ApplePaymentPeriodModal/ApplePaymentPeriodModal.types';
import { applePaymentPeriodModalDataByMode } from 'Pages/Import/modals/ApplePaymentPeriodModal/constants';
import React, { FC, ReactElement, ReactNode } from 'react';

import { Nullable } from '../../../../../types/general';

type ApplePaymentPeriodModalHeaderProps = {
  year: Nullable<number>;
  isLoading?: boolean;

  onClose: () => void;
  mode: ApplePaymentPeriodModalMode;
  stepId: ApplePaymentPeriodModalStepId;
};

const ApplePaymentPeriodModalHeader: FC<ApplePaymentPeriodModalHeaderProps> = ({
  year,
  isLoading,
  onClose,
  mode,
  stepId,
}: ApplePaymentPeriodModalHeaderProps): ReactElement => {
  const dataByStep: Record<
    ApplePaymentPeriodModalStepId,
    {
      title: ReactNode;
      headerHeight: '60' | '80';
    }
  > = {
    fiscalYear: {
      title: <Text type="body-medium">Please choose a Fiscal year</Text>,
      headerHeight: '60',
    },
    paymentPeriod: {
      title: (
        <Container flexdirection="column" alignitems="center" gap="4">
          <Text type="body-medium">{applePaymentPeriodModalDataByMode[mode].headerTitle}</Text>
          <Text color="grey-100">{`Fiscal year ${year}`}</Text>
        </Container>
      ),
      headerHeight: '80',
    },
  };

  if (isLoading)
    return (
      <ModalHeaderPlaceholder
        titleAlignment="center"
        border
        height={dataByStep[stepId].headerHeight}
      />
    );

  return (
    <ModalHeaderNew
      title={dataByStep[stepId].title}
      titlePosition="center"
      height={dataByStep[stepId].headerHeight}
      border
      onClose={onClose}
    />
  );
};

export default ApplePaymentPeriodModalHeader;
