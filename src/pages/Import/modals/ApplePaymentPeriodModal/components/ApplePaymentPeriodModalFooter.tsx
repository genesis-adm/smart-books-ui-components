import { ContentLoader } from 'Components/base/ContentLoader';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ApplePaymentPeriodModalStepId } from 'Pages/Import/modals/ApplePaymentPeriodModal/ApplePaymentPeriodModal.types';
import { ReactComponent as ArrowLeftIcon } from 'Svg/v2/16/arrow-left.svg';
import React, { FC, ReactElement } from 'react';

import { UseSteps } from '../../../../../hooks/useSteps';

type ApplePaymentPeriodModalFooterProps = {
  stepsOptions: UseSteps<ApplePaymentPeriodModalStepId>;
  btnDisabled?: boolean;
  isLoading?: boolean;
  onSubmit?: () => void;
};

const ApplePaymentPeriodModalFooter: FC<ApplePaymentPeriodModalFooterProps> = ({
  btnDisabled,
  stepsOptions,
  isLoading = false,
  onSubmit,
}: ApplePaymentPeriodModalFooterProps): ReactElement => {
  const {
    stepNumber,
    stepsLength,
    isFirstStep,
    isLastStep,
    prevStep,
    nextStep,
    currentStepTitle,
    nextStepTitle,
  } = stepsOptions;

  const btnAction = () => {
    isLastStep ? submit() : nextStep();
  };

  const submit = async (): Promise<void> => onSubmit?.();

  return (
    <ModalFooterNew justifycontent="space-between" border>
      <Container alignitems="center" gap="16" flexgrow="1" justifycontent="space-between">
        {!isFirstStep && !isLoading && (
          <Button
            width="100"
            height="40"
            navigation
            iconLeft={<ArrowLeftIcon />}
            background={'white-100'}
            color="black-100"
            border="grey-20-grey-90"
            onClick={prevStep}
          >
            Back
          </Button>
        )}

        <Container flexgrow="1" alignitems="center" justifycontent="flex-end" gap="16">
          <Container flexdirection="column" gap={isLoading ? '4' : undefined}>
            <ContentLoader isLoading={isLoading} width="80px" height="22px">
              <Text type="body-regular-14" color="grey-100">
                {`Step: ${stepNumber} of ${stepsLength}`}
              </Text>
            </ContentLoader>
            <ContentLoader isLoading={isLoading} width="224px" height="22px">
              <Text type="body-regular-14" color="grey-100">
                {nextStepTitle ? `Next: ${nextStepTitle}` : currentStepTitle}
              </Text>
            </ContentLoader>
          </Container>
          <ContentLoader height="40px" width="100px" isLoading={isLoading}>
            <Button height="40" width="100" onClick={btnAction} disabled={btnDisabled}>
              {isLastStep ? 'Create' : 'Next'}
            </Button>
          </ContentLoader>
        </Container>
      </Container>
    </ModalFooterNew>
  );
};

export default ApplePaymentPeriodModalFooter;
