import { Icon } from 'Components/base/Icon';
import { Radio } from 'Components/base/Radio';
import { Tooltip } from 'Components/base/Tooltip';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as TickInFilledCircleIcon } from 'Svg/v2/16/tick-in-filled-circle.svg';
import classNames from 'classnames';
import React, { ChangeEvent, FC, ReactElement, useState } from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';
import { Nullable } from '../../../../../types/general';

type ApplePaymentPeriodYearProps = {
  value?: Nullable<number>;
  checked: boolean;
  disabled?: boolean;
  onChange?: (value: Nullable<number>) => void;
  tooltip?: string;
};

const ApplePaymentPeriodYear: FC<ApplePaymentPeriodYearProps> = ({
  value,
  disabled,
  checked,
  onChange,
  tooltip,
}: ApplePaymentPeriodYearProps): ReactElement => {
  const [isHovered, setIsHovered] = useState(false);

  const chooseYear = ({ target }: ChangeEvent<HTMLInputElement>) => onChange?.(+target.id);

  const hover = () => {
    setIsHovered(true);
  };

  const leave = () => {
    setIsHovered(false);
  };

  return (
    <Tooltip message={tooltip}>
      <Container
        flexdirection="column"
        border="violet-20"
        borderRadius="8"
        className={classNames(spacing.h62fixed, spacing.w90fixed)}
        overflow="hidden"
        onMouseEnter={hover}
        onMouseLeave={leave}
        background={
          !disabled ? (isHovered ? 'grey-20' : 'transparent') : isHovered ? 'grey-20' : undefined
        }
        style={{ cursor: 'pointer' }}
        onClick={() => onChange?.(value || null)}
      >
        <Container
          className={classNames(spacing.h14fixed)}
          alignitems="center"
          justifycontent="center"
          background={!disabled ? 'violet-90' : 'grey-90'}
          style={{ flexShrink: '0' }}
        >
          <Text
            type="filename"
            color={!disabled ? 'white-100' : 'grey-20'}
            style={{ fontSize: '8px' }}
          >
            *
          </Text>
          <Text
            type="filename"
            color={!disabled ? 'white-100' : 'grey-20'}
            style={{ fontSize: '8px' }}
          >
            Fiscal year
          </Text>
        </Container>
        <Container flexgrow="1" justifycontent="center" alignitems="center" gap="8">
          {disabled ? (
            <>
              <Icon icon={<TickInFilledCircleIcon />} path="grey-100" />
              <Text type="body-medium" color={!disabled ? 'grey-100' : 'grey-90'}>
                {value}
              </Text>
            </>
          ) : (
            <Radio
              id={String(value)}
              name="year"
              onChange={chooseYear}
              checked={checked}
              color={checked ? 'black-100' : 'grey-100'}
              type="body-medium"
              hovered={isHovered}
            >
              {value}
            </Radio>
          )}
        </Container>
      </Container>
    </Tooltip>
  );
};

export default ApplePaymentPeriodYear;
