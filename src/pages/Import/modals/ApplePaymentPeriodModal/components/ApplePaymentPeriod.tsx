import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableTitleNew } from 'Components/elements/Table/TableTitleNew';
import TableSkeleton from 'Components/loaders/TableSkeleton';
import ApplePaymentPeriodTableRow from 'Pages/Import/modals/ApplePaymentPeriodModal/components/ApplePaymentPeriodTableRow';
import {
  applePaymentPeriodTableColumns,
  applePaymentPeriodTableRows,
} from 'Pages/Import/modals/ApplePaymentPeriodModal/constants';
import React, { FC, ReactElement } from 'react';

import { Nullable } from '../../../../../types/general';

type ApplePaymentPeriodProps = {
  year: Nullable<number>;

  isView?: boolean;
  isLoading?: boolean;
};

const ApplePaymentPeriod: FC<ApplePaymentPeriodProps> = ({
  year,
  isView,
  isLoading = false,
}: ApplePaymentPeriodProps): ReactElement => {
  return (
    <TableSkeleton isLoading={isLoading} rowsNumber={5} rowPxHeight={60}>
      <TableNew>
        <TableHeadNew>
          {applePaymentPeriodTableColumns.map((props, index) => (
            <TableTitleNew key={index} {...props} />
          ))}
        </TableHeadNew>

        <TableBodyNew>
          {applePaymentPeriodTableRows.map((_, index) => (
            <ApplePaymentPeriodTableRow key={index} index={index} isView={isView} year={year} />
          ))}
        </TableBodyNew>
      </TableNew>
    </TableSkeleton>
  );
};

export default ApplePaymentPeriod;
