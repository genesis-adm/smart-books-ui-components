import { Modal } from 'Components/modules/Modal';
import {
  ApplePaymentPeriodModalMode,
  ApplePaymentPeriodModalStepId,
} from 'Pages/Import/modals/ApplePaymentPeriodModal/ApplePaymentPeriodModal.types';
import ApplePaymentPeriod from 'Pages/Import/modals/ApplePaymentPeriodModal/components/ApplePaymentPeriod';
import ApplePaymentPeriodModalFooter from 'Pages/Import/modals/ApplePaymentPeriodModal/components/ApplePaymentPeriodModalFooter';
import ApplePaymentPeriodModalHeader from 'Pages/Import/modals/ApplePaymentPeriodModal/components/ApplePaymentPeriodModalHeader';
import ChooseFiscalYear from 'Pages/Import/modals/ApplePaymentPeriodModal/components/ChooseFiscalYear';
import {
  APPLE_PAYMENT_PERIOD_MODAL_ID,
  applePaymentPeriodModalSteps,
} from 'Pages/Import/modals/ApplePaymentPeriodModal/constants';
import React, { FC, ReactNode, useEffect, useState } from 'react';

import useSteps from '../../../../hooks/useSteps';
import { Nullable } from '../../../../types/general';

type ApplePaymentPeriodModalProps = {
  mode: ApplePaymentPeriodModalMode;
  isLoading?: boolean;
  onClose?: () => void;

  containerId?: string;
};

const ApplePaymentPeriodModal: FC<ApplePaymentPeriodModalProps> = ({
  mode,
  isLoading = false,
  onClose = () => {},
  containerId,
}: ApplePaymentPeriodModalProps) => {
  const stepsOptions = useSteps(
    applePaymentPeriodModalSteps,
    mode === 'create' ? 'fiscalYear' : 'paymentPeriod',
  );
  const { setStepId, stepId } = stepsOptions;

  const [year, setYear] = useState<Nullable<number>>(null);

  const isView = mode === 'view';

  const steps: Record<ApplePaymentPeriodModalStepId, { render: () => ReactNode }> = {
    fiscalYear: {
      render: () => <ChooseFiscalYear value={year} onChange={setYear} isLoading={isLoading} />,
    },
    paymentPeriod: {
      render: () => <ApplePaymentPeriod isView={isView} isLoading={isLoading} year={year} />,
    },
  };

  useEffect(() => {
    setStepId(mode === 'create' ? 'fiscalYear' : 'paymentPeriod');
  }, [setStepId, mode]);

  useEffect(() => {
    if (mode === 'create') {
      setYear(null);
    }

    setYear(2024);
  }, [mode]);

  if (mode === 'view' && !year) return <></>;

  return (
    <Modal
      size="530"
      height="575"
      centered
      modalId={APPLE_PAYMENT_PERIOD_MODAL_ID}
      overlay={containerId ? 'white-100' : 'black-100'}
      padding="24"
      pluginScrollDisabled
      containerId={containerId}
      header={
        <ApplePaymentPeriodModalHeader
          year={year}
          onClose={onClose}
          isLoading={isLoading}
          stepId={stepId}
          mode={mode}
        />
      }
      footer={
        mode === 'create' ? (
          <ApplePaymentPeriodModalFooter
            stepsOptions={stepsOptions}
            btnDisabled={!year}
            isLoading={isLoading}
            onSubmit={onClose}
          />
        ) : undefined
      }
    >
      {steps[stepId].render()}
    </Modal>
  );
};

export default ApplePaymentPeriodModal;
