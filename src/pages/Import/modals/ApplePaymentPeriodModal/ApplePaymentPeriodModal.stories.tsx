import { Meta, Story } from '@storybook/react';
import ApplePaymentPeriodModal from 'Pages/Import/modals/ApplePaymentPeriodModal/ApplePaymentPeriodModal';
import React from 'react';

const ApplePaymentPeriodModalStory: Story = ({ storyState, mode }) => {
  const isLoading = storyState === 'loading';

  return <ApplePaymentPeriodModal mode={mode} isLoading={isLoading} />;
};

export default {
  title: 'Pages/Import page/modals/Apple Payment Period',
  component: ApplePaymentPeriodModalStory,
  argTypes: {
    mode: {
      options: ['create', 'view'],
      control: {
        type: 'radio',
        labels: {
          create: 'Create',
          view: 'View',
        },
      },
    },
    storyState: {
      options: ['loading', 'loaded'],
      control: {
        type: 'radio',
        labels: {
          loading: 'Page is loading',
          loaded: 'Page loaded',
        },
      },
    },
  },
} as Meta;

export const ApplePaymentPeriod = ApplePaymentPeriodModalStory.bind({});
ApplePaymentPeriod.args = {
  mode: 'create',
  storyState: 'loaded',
};
