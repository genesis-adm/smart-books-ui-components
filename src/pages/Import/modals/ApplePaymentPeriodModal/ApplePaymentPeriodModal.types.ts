export type ApplePaymentPeriodModalStepId = 'fiscalYear' | 'paymentPeriod';

export type ApplePaymentPeriodModalMode = 'create' | 'view';
