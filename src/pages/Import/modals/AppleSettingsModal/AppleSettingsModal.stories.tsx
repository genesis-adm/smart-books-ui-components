import { Meta, Story } from '@storybook/react';
import AppleSettingsModal from 'Pages/Import/modals/AppleSettingsModal/AppleSettingsModal';
import React from 'react';

const AppleSettingsStory: Story = ({ storyState }) => {
  const isLoaded = storyState === 'loaded';
  const isLoading = storyState === 'loading';

  return <AppleSettingsModal isLoaded={isLoaded} isLoading={isLoading} />;
};

export default {
  title: 'Pages/Import page/modals/Apple Settings',
  component: AppleSettingsStory,
  argTypes: {
    storyState: {
      options: ['empty', 'loading', 'loaded'],
      control: {
        type: 'radio',
        labels: {
          empty: 'Empty page',
          loading: 'Page is loading',
          loaded: 'Page loaded',
        },
      },
    },
  },
} as Meta;

export const AppleSettings = AppleSettingsStory.bind({});
AppleSettings.args = {
  storyState: 'loaded',
};
