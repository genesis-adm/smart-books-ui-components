import { TabKey } from 'Pages/Import/modals/AppleSettingsModal/AppleSettingsModal.types';

import { TabItem } from '../../../../types/general';

export const APPLE_SETTINGS_MODAL_ID = 'APPLE_SETTINGS_MODAL';

export const appleSettingsModalTabs: TabItem<TabKey>[] = [
  { id: 'taxes', label: 'Taxes' },
  { id: 'currencyRate', label: 'Currency rate' },
  { id: 'paymentPeriod', label: 'Payment period' },
];
