import { Tooltip } from 'Components/base/Tooltip';
import { Text } from 'Components/base/typography/Text';
import { TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import React from 'react';

export const appleSettingsPaymentPeriodsTableColumns: (TableTitleNewProps & { id: string })[] = [
  {
    id: 'appleFiscalYear',
    minWidth: '170',
    title: 'Apple Fiscal year',
    flexgrow: '1',
  },
  {
    id: 'createdBy',
    minWidth: '260',
    title: 'Created by',
  },
  {
    id: 'createdDate',
    minWidth: '240',
    title: 'Create date',
  },
  {
    id: 'actions',
    minWidth: '70',
    title: 'Actions',
    align: 'center',
  },
];

export const appleSettingsPaymentPeriodsTableData = [
  {
    appleFiscalYear: '2024: 01 Oct 2023 - 28 Sep 2024',
    createdBy: {
      fullName: 'Artem Holba',
      email: 'artem.holba@gen.tech',
    },
    createdDate: '08 May 2022',
  },
  {
    appleFiscalYear: '2024: 01 Oct 2023 - 28 Sep 2024',
    createdBy: {
      fullName: 'Max Medvedev',
      email: 'max.medvedev@gen.tech',
    },
    createdDate: '08 Jun 2021',
  },
  {
    appleFiscalYear: '2024: 01 Oct 2023 - 28 Sep 2024',
    createdBy: {
      fullName: 'Anna Leonova',
      email: 'anna.leonova@gen.tech',
    },
    createdDate: '08 Jul 2024',
  },
];

export const appleSettingsPaymentPeriodsHint = (
  <>
    <Tooltip cursor="pointer" message="Open Apple's Fiscal Year Accounting Calendar">
      <Text color="violet-90">Apple&apos;s fiscal year&nbsp;</Text>
    </Tooltip>
    outlines when app revenue is paid to developers after deducting App Store fees and taxes.
    Payments periods are used for Realised Forex calculations.
  </>
);
