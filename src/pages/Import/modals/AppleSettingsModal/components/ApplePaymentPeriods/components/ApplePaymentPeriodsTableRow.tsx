import { IconButton } from 'Components/base/buttons/IconButton';
import { Logo } from 'Components/elements/Logo';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import {
  appleSettingsPaymentPeriodsTableColumns,
  appleSettingsPaymentPeriodsTableData,
} from 'Pages/Import/modals/AppleSettingsModal/components/ApplePaymentPeriods/constants';
import { ReactComponent as Trash16Icon } from 'Svg/v2/16/trashbox.svg';
import React, { FC, ReactElement } from 'react';

type ApplePaymentPeriodsTableRowProps = {
  index: number;
  onClick?: (e: React.MouseEvent<HTMLDivElement>) => void;
};

const ApplePaymentPeriodsTableRow: FC<ApplePaymentPeriodsTableRowProps> = ({
  index,
  onClick,
}: ApplePaymentPeriodsTableRowProps): ReactElement => {
  const { appleFiscalYear, createdBy, createdDate } = appleSettingsPaymentPeriodsTableData[index];

  return (
    <TableRowNew key={index} background={'info'} size="40" cursor="pointer" onClick={onClick}>
      <TableCellTextNew
        minWidth={appleSettingsPaymentPeriodsTableColumns[0].minWidth}
        align={appleSettingsPaymentPeriodsTableColumns[0].align}
        tagText={appleFiscalYear}
        tagColor="violet"
        flexgrow={appleSettingsPaymentPeriodsTableColumns[0].flexgrow}
        value={''}
      />
      <TableCellTextNew
        minWidth={appleSettingsPaymentPeriodsTableColumns[1].minWidth}
        align={appleSettingsPaymentPeriodsTableColumns[1].align}
        flexgrow={appleSettingsPaymentPeriodsTableColumns[1].flexgrow}
        secondaryColor="grey-100"
        value={createdBy.fullName}
        secondaryValue={createdBy.email}
        iconLeft={<Logo content="user" radius="rounded" size="24" name={createdBy.fullName} />}
      />
      <TableCellTextNew
        minWidth={appleSettingsPaymentPeriodsTableColumns[2].minWidth}
        align={appleSettingsPaymentPeriodsTableColumns[2].align}
        flexgrow={appleSettingsPaymentPeriodsTableColumns[2].flexgrow}
        value={createdDate}
      />
      <TableCellNew
        minWidth={appleSettingsPaymentPeriodsTableColumns[3].minWidth}
        flexgrow={appleSettingsPaymentPeriodsTableColumns[3].flexgrow}
        alignitems="center"
        justifycontent="center"
      >
        <IconButton
          icon={<Trash16Icon />}
          tooltip="Delete"
          size="24"
          onClick={() => {}}
          background={'grey-10-red-10'}
          color="grey-100-red-90"
        />
      </TableCellNew>
    </TableRowNew>
  );
};

export default ApplePaymentPeriodsTableRow;
