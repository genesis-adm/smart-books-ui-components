import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableTitleNew } from 'Components/elements/Table/TableTitleNew';
import ApplePaymentPeriodModal from 'Pages/Import/modals/ApplePaymentPeriodModal/ApplePaymentPeriodModal';
import ApplePaymentPeriodsTableRow from 'Pages/Import/modals/AppleSettingsModal/components/ApplePaymentPeriods/components/ApplePaymentPeriodsTableRow';
import {
  appleSettingsPaymentPeriodsTableColumns,
  appleSettingsPaymentPeriodsTableData,
} from 'Pages/Import/modals/AppleSettingsModal/components/ApplePaymentPeriods/constants';
import { APPLE_SETTINGS_MODAL_ID } from 'Pages/Import/modals/AppleSettingsModal/constants';
import React, { FC, ReactElement } from 'react';

import spacing from '../../../../../../../assets/styles/spacing.module.scss';
import { useToggle } from '../../../../../../../hooks/useToggle';

const ApplePaymentPeriodsTable: FC = (): ReactElement => {
  const applePaymentPeriodModalToggle = useToggle();

  const openPaymentPeriodModal = (e: React.MouseEvent<HTMLDivElement>): void => {
    e.stopPropagation();

    applePaymentPeriodModalToggle.onOpen();
  };

  return (
    <TableNew
      noResults={false}
      tableHead={
        <TableHeadNew radius="10" background={'grey-10'} className={spacing.pY4}>
          {appleSettingsPaymentPeriodsTableColumns.map(({ id, ...props }) => (
            <TableTitleNew
              key={id}
              background={'grey-10'}
              radius="10"
              {...props}
              onClick={() => {}}
            />
          ))}
        </TableHeadNew>
      }
    >
      <TableBodyNew>
        {appleSettingsPaymentPeriodsTableData.map((_, index) => (
          <ApplePaymentPeriodsTableRow key={index} index={index} onClick={openPaymentPeriodModal} />
        ))}
      </TableBodyNew>

      {applePaymentPeriodModalToggle.isOpen && (
        <ApplePaymentPeriodModal
          mode="view"
          containerId={APPLE_SETTINGS_MODAL_ID}
          onClose={applePaymentPeriodModalToggle.onClose}
        />
      )}
    </TableNew>
  );
};

export default ApplePaymentPeriodsTable;
