import { Button } from 'Components/base/buttons/Button';
import { BackgroundColorNewTypes, TextColorNewTypes } from 'Components/types/colorTypes';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import React, { FC, ReactElement } from 'react';

type CreateApplePaymentPeriodButtonProps = {
  onCreate?: () => void;
  background?: BackgroundColorNewTypes;
  color?: TextColorNewTypes;
};

const CreateApplePaymentPeriodButton: FC<CreateApplePaymentPeriodButtonProps> = ({
  onCreate,
  background = 'blue-10-blue-20',
  color = 'violet-90',
}: CreateApplePaymentPeriodButtonProps): ReactElement => {
  return (
    <Button
      onClick={onCreate}
      background={background}
      color={color}
      width="120"
      height="40"
      iconLeft={<PlusIcon />}
    >
      Create
    </Button>
  );
};

export default CreateApplePaymentPeriodButton;
