import { ContentLoader } from 'Components/base/ContentLoader';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import TableSkeleton from 'Components/loaders/TableSkeleton';
import ApplePaymentPeriodsTable from 'Pages/Import/modals/AppleSettingsModal/components/ApplePaymentPeriods/components/ApplePaymentPeriodsTable';
import CreateApplePaymentPeriodButton from 'Pages/Import/modals/AppleSettingsModal/components/ApplePaymentPeriods/components/CreateApplePaymentPeriodButton';
import {
  appleSettingsPaymentPeriodsHint,
  appleSettingsPaymentPeriodsTableData,
} from 'Pages/Import/modals/AppleSettingsModal/components/ApplePaymentPeriods/constants';
import AppleSettingsTabBlank from 'Pages/Import/modals/AppleSettingsModal/components/AppleSettingsTabBlank';
import { ReactComponent as AppleLogo24Icon } from 'Svg/v2/24/logo-apple.svg';
import classNames from 'classnames';
import React, { FC, ReactElement } from 'react';

import spacing from '../../../../../../assets/styles/spacing.module.scss';

type ApplePaymentPeriodsProps = {
  items?: typeof appleSettingsPaymentPeriodsTableData;
  onCreate?: () => void;

  isLoading: boolean;
};

const ApplePaymentPeriods: FC<ApplePaymentPeriodsProps> = ({
  items,
  onCreate,
  isLoading,
}: ApplePaymentPeriodsProps): ReactElement => {
  const noItems = !items?.length;

  if (noItems && !isLoading)
    return (
      <AppleSettingsTabBlank
        hintText="Please create Payment period"
        actions={<CreateApplePaymentPeriodButton onCreate={onCreate} />}
      />
    );

  return (
    <Container
      flexdirection="column"
      background={'white-100'}
      borderRadius="20"
      gap="24"
      className={classNames(spacing.p24, spacing.h100p)}
    >
      <Container justifycontent="space-between" alignitems="center" gap="24">
        {isLoading ? (
          <Container flexdirection="column" gap="4">
            <ContentLoader isLoading={isLoading} width="215px" height="16px" />
            <ContentLoader isLoading={isLoading} width="124px" height="16px" />
          </Container>
        ) : (
          <Container gap="12" className={spacing.w540fixed}>
            <Container alignitems="center">
              <AppleLogo24Icon />
            </Container>
            <Text color="grey-100">{appleSettingsPaymentPeriodsHint}</Text>
          </Container>
        )}

        <ContentLoader isLoading={isLoading} width="115px" height="40px">
          <CreateApplePaymentPeriodButton
            onCreate={onCreate}
            background={'violet-90-violet-100'}
            color="white-100"
          />
        </ContentLoader>
      </Container>

      <Container flexgrow="1" overflow="y-auto">
        <TableSkeleton isLoading={isLoading}>
          <ApplePaymentPeriodsTable />
        </TableSkeleton>
      </Container>
    </Container>
  );
};

export default ApplePaymentPeriods;
