import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as ManWithDocs } from 'Svg/v2/colored/man-with-docs.svg';
import React, { FC, ReactElement, ReactNode } from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';

type AppleSettingsTabBlankProps = {
  hintText: string;
  actions: ReactNode;
};

const AppleSettingsTabBlank: FC<AppleSettingsTabBlankProps> = ({
  actions,
  hintText,
}: AppleSettingsTabBlankProps): ReactElement => {
  return (
    <Container
      background={'white-100'}
      borderRadius="20"
      justifycontent="center"
      alignitems="center"
      flexdirection="column"
      gap="20"
      className={spacing.pY90}
    >
      <ManWithDocs />
      <Text color="grey-100" type="body-regular-14">
        {hintText}
      </Text>

      {actions}
    </Container>
  );
};

export default AppleSettingsTabBlank;
