import { ContentLoader } from 'Components/base/ContentLoader';
import { Container } from 'Components/base/grid/Container';
import { DevInProgressTooltip } from 'Components/elements/DevInProgressTooltip';
import { ExportDropdown } from 'Components/elements/ExportDropdown';
import { FilterDropDown } from 'Components/elements/filters/FilterDropDown';
import { Search } from 'Components/inputs/Search';
import useSearchExample from 'Components/inputs/Search/hooks/useSearchExample';
import TableSkeleton from 'Components/loaders/TableSkeleton';
import AppleCurrencyRatesTable from 'Pages/Import/modals/AppleSettingsModal/components/AppleCurrencyRates/components/AppleCurrencyRatesTable';
import ImportCurrencyRatesButton from 'Pages/Import/modals/AppleSettingsModal/components/AppleCurrencyRates/components/ImportCurrencyRatesButton';
import { appleSettingsCurrencyRatesTableData } from 'Pages/Import/modals/AppleSettingsModal/components/AppleCurrencyRates/constants';
import AppleSettingsTabBlank from 'Pages/Import/modals/AppleSettingsModal/components/AppleSettingsTabBlank';
import classNames from 'classnames';
import React, { FC, ReactElement } from 'react';

import spacing from '../../../../../../assets/styles/spacing.module.scss';

type AppleCurrencyRatesProps = {
  items?: typeof appleSettingsCurrencyRatesTableData;
  onImport?: () => void;
  isLoading: boolean;
};

const AppleCurrencyRates: FC<AppleCurrencyRatesProps> = ({
  items,
  onImport,
  isLoading,
}: AppleCurrencyRatesProps): ReactElement => {
  const { searchValue, setSearchValue, searchValuesList, handleSearchValueAdd, handleClearAll } =
    useSearchExample();

  const noItems = !items?.length;

  const filters = [{ id: 'paymentPeriod', title: 'Payment period' }];

  if (noItems && !isLoading)
    return (
      <AppleSettingsTabBlank
        hintText="Please Import Currency rates"
        actions={<ImportCurrencyRatesButton onClick={onImport} />}
      />
    );

  return (
    <Container
      flexdirection="column"
      background={'white-100'}
      borderRadius="20"
      gap="24"
      className={classNames(spacing.p24, spacing.h100p)}
    >
      <Container justifycontent="space-between" gap="24">
        <ContentLoader width="320px" height="40px" isLoading={isLoading}>
          <Search
            width="320"
            height="40"
            name="search-cabinet-name"
            placeholder="Search by Cabinet name"
            value={searchValue}
            onChange={(e) => setSearchValue(e.target.value)}
            searchParams={searchValuesList}
            onSearch={handleSearchValueAdd}
            onClear={handleClearAll}
          />
        </ContentLoader>

        <Container gap="24" alignitems="center">
          <ContentLoader width="40px" height="40px" isLoading={isLoading}>
            <DevInProgressTooltip>
              <ExportDropdown onExportToXLS={() => {}} onExportToCSV={() => {}} disabled />
            </DevInProgressTooltip>
          </ContentLoader>

          <ContentLoader width="140px" height="40px" isLoading={isLoading}>
            <ImportCurrencyRatesButton onClick={onImport} />
          </ContentLoader>
        </Container>
      </Container>

      <Container gap="24">
        {filters.map((item) => (
          <ContentLoader key={item.id} width="165px" height="32px" isLoading={isLoading}>
            <FilterDropDown title={item.title} value="All" onClear={() => {}} selectedAmount={0} />
          </ContentLoader>
        ))}
      </Container>

      <Container flexgrow="1" overflow="y-auto">
        <TableSkeleton isLoading={isLoading} headerPxHeight={36}>
          <AppleCurrencyRatesTable items={items} />
        </TableSkeleton>
      </Container>
    </Container>
  );
};

export default AppleCurrencyRates;
