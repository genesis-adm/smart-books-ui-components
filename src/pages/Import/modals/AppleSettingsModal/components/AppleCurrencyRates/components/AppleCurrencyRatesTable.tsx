import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableTitleNew } from 'Components/elements/Table/TableTitleNew';
import AppleCurrencyRatesModal from 'Pages/Import/modals/AppleCurrencyRatesModal/AppleCurrencyRatesModal';
import AppleCurrencyRatesTableRow from 'Pages/Import/modals/AppleSettingsModal/components/AppleCurrencyRates/components/AppleCurrencyRatesTableRow';
import {
  appleSettingsCurrencyRatesTableColumns,
  appleSettingsCurrencyRatesTableData,
} from 'Pages/Import/modals/AppleSettingsModal/components/AppleCurrencyRates/constants';
import { APPLE_SETTINGS_MODAL_ID } from 'Pages/Import/modals/AppleSettingsModal/constants';
import React, { FC, ReactElement } from 'react';

import spacing from '../../../../../../../assets/styles/spacing.module.scss';
import { useToggle } from '../../../../../../../hooks/useToggle';

type AppleCurrencyRatesTableProps = {
  items?: typeof appleSettingsCurrencyRatesTableData;
};

const AppleCurrencyRatesTable: FC<AppleCurrencyRatesTableProps> = ({
  items,
}: AppleCurrencyRatesTableProps): ReactElement => {
  const appleCurrencyRatesModalToggle = useToggle();

  const openCurrencyRatesModal = (e: React.MouseEvent<HTMLButtonElement>): void => {
    e.stopPropagation();

    appleCurrencyRatesModalToggle.onOpen();
  };

  return (
    <TableNew
      noResults={false}
      tableHead={
        <TableHeadNew radius="10" background={'grey-10'} className={spacing.pY6}>
          {appleSettingsCurrencyRatesTableColumns.map(({ id, ...props }) => (
            <TableTitleNew
              key={id}
              background={'grey-10'}
              radius="10"
              {...props}
              onClick={() => {}}
            />
          ))}
        </TableHeadNew>
      }
    >
      <TableBodyNew>
        {items?.map((_, index) => (
          <AppleCurrencyRatesTableRow key={index} index={index} onClick={openCurrencyRatesModal} />
        ))}
      </TableBodyNew>

      {appleCurrencyRatesModalToggle.isOpen && (
        <AppleCurrencyRatesModal
          containerId={APPLE_SETTINGS_MODAL_ID}
          onClose={appleCurrencyRatesModalToggle.onClose}
        />
      )}
    </TableNew>
  );
};

export default AppleCurrencyRatesTable;
