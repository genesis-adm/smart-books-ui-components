import { Tooltip } from 'Components/base/Tooltip';
import { Button } from 'Components/base/buttons/Button';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Logo } from 'Components/elements/Logo';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import {
  appleSettingsCurrencyRatesTableColumns,
  appleSettingsCurrencyRatesTableData,
} from 'Pages/Import/modals/AppleSettingsModal/components/AppleCurrencyRates/constants';
import { ReactComponent as Trash16Icon } from 'Svg/v2/16/trashbox.svg';
import React, { FC, ReactElement } from 'react';

type AppleCurrencyRatesTableRowProps = {
  index: number;
  onClick?: (e: React.MouseEvent<HTMLButtonElement>) => void;
};

const AppleCurrencyRatesTableRow: FC<AppleCurrencyRatesTableRowProps> = ({
  index,
  onClick,
}: AppleCurrencyRatesTableRowProps): ReactElement => {
  const { cabinetName, paymentPeriod, records, createdBy } =
    appleSettingsCurrencyRatesTableData[index];

  return (
    <TableRowNew key={index} background={'info'} size="40" cursor="pointer">
      <TableCellTextNew
        minWidth={appleSettingsCurrencyRatesTableColumns[0].minWidth}
        align={appleSettingsCurrencyRatesTableColumns[0].align}
        flexgrow={appleSettingsCurrencyRatesTableColumns[0].flexgrow}
        value={cabinetName}
      />
      <TableCellTextNew
        minWidth={appleSettingsCurrencyRatesTableColumns[1].minWidth}
        align={appleSettingsCurrencyRatesTableColumns[1].align}
        flexgrow={appleSettingsCurrencyRatesTableColumns[1].flexgrow}
        value={`${paymentPeriod.year}: ${paymentPeriod.from} - ${paymentPeriod.to}`}
      />
      <TableCellNew
        minWidth={appleSettingsCurrencyRatesTableColumns[2].minWidth}
        flexgrow={appleSettingsCurrencyRatesTableColumns[2].flexgrow}
        alignitems="center"
        justifycontent="center"
      >
        <Tooltip message="Open">
          <Button
            width="40"
            height="24"
            background={'blue-10-blue-20'}
            color="violet-90"
            font="text-medium"
            onClick={onClick}
          >
            {records}
          </Button>
        </Tooltip>
      </TableCellNew>
      <TableCellTextNew
        minWidth={appleSettingsCurrencyRatesTableColumns[3].minWidth}
        align={appleSettingsCurrencyRatesTableColumns[3].align}
        flexgrow={appleSettingsCurrencyRatesTableColumns[3].flexgrow}
        secondaryColor="grey-100"
        value={createdBy.fullName}
        secondaryValue={createdBy.email}
        iconLeft={<Logo content="user" radius="rounded" size="24" name={createdBy.fullName} />}
      />
      <TableCellNew
        minWidth={appleSettingsCurrencyRatesTableColumns[4].minWidth}
        flexgrow={appleSettingsCurrencyRatesTableColumns[4].flexgrow}
        alignitems="center"
        justifycontent="center"
      >
        <IconButton
          icon={<Trash16Icon />}
          tooltip="Delete"
          size="24"
          onClick={() => {}}
          background={'grey-10-red-10'}
          color="grey-100-red-90"
        />
      </TableCellNew>
    </TableRowNew>
  );
};

export default AppleCurrencyRatesTableRow;
