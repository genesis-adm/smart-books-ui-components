import { Button } from 'Components/base/buttons/Button';
import { ReactComponent as ImportIcon } from 'Svg/v2/16/download.svg';
import React, { FC, ReactElement } from 'react';

type ImportCurrencyRatesButtonProps = {
  onClick?: (e: React.MouseEvent<HTMLButtonElement>) => void;
};

const ImportCurrencyRatesButton: FC<ImportCurrencyRatesButtonProps> = ({
  onClick,
}: ImportCurrencyRatesButtonProps): ReactElement => {
  return (
    <Button
      onClick={onClick}
      background={'grey-10-grey-20'}
      color="grey-100"
      width="120"
      height="40"
      iconLeft={<ImportIcon />}
    >
      Import
    </Button>
  );
};

export default ImportCurrencyRatesButton;
