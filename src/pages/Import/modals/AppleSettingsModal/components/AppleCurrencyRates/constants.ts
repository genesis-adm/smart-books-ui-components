import { TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';

export const appleSettingsCurrencyRatesTableColumns: (TableTitleNewProps & { id: string })[] = [
  {
    id: 'cabinetName',
    minWidth: '180',
    title: 'Cabinet name',
  },
  {
    id: 'paymentPeriod',
    minWidth: '300',
    title: 'Payment period',
    flexgrow: '1',
  },
  {
    id: 'records',
    minWidth: '88',
    title: 'Records',
    align: 'center',
  },
  {
    id: 'createdBy',
    minWidth: '220',
    title: 'Created by',
  },
  {
    id: 'actions',
    minWidth: '70',
    title: 'Actions',
    align: 'center',
  },
];

export const appleSettingsCurrencyRatesTableData = [
  {
    cabinetName: 'BetterMe Limited Apple',
    paymentPeriod: {
      year: 2024,
      from: '05 Feb 2024',
      to: '02 Mar 2024',
    },
    records: 10,
    createdBy: {
      fullName: 'Artem Holba',
      email: 'artem.holba@gen.tech',
    },
  },
  {
    cabinetName: 'BetterMe Limited Apple',
    paymentPeriod: {
      year: 2023,
      from: '05 Dec 2022',
      to: '02 Jan 2023',
    },
    records: 12,
    createdBy: {
      fullName: 'Anna Leonova',
      email: 'anna.leonova@gen.tech',
    },
  },
  {
    cabinetName: 'BetterMe Limited Apple',
    paymentPeriod: {
      year: 2022,
      from: '05 Nov 2021',
      to: '02 Dec 2021',
    },
    records: 15,
    createdBy: {
      fullName: 'Max Medvedev',
      email: 'max.medvedev@gen.tech',
    },
  },
];
