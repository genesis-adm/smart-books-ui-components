import { Container } from 'Components/base/grid/Container';
import { DevInProgressTooltip } from 'Components/elements/DevInProgressTooltip';
import { ExportDropdown } from 'Components/elements/ExportDropdown';
import { FilterDropDown } from 'Components/elements/filters/FilterDropDown';
import { Search } from 'Components/inputs/Search';
import useSearchExample from 'Components/inputs/Search/hooks/useSearchExample';
import TableSkeleton from 'Components/loaders/TableSkeleton';
import AppleSettingsTabBlank from 'Pages/Import/modals/AppleSettingsModal/components/AppleSettingsTabBlank';
import CreateAppleTaxButton from 'Pages/Import/modals/AppleSettingsModal/components/Taxes/CreateAppleTaxButton';
import TaxesTable from 'Pages/Import/modals/AppleSettingsModal/components/Taxes/TaxesTable';
import { appleSettingsTaxTableData } from 'Pages/Import/modals/AppleSettingsModal/components/Taxes/constants';
import { TaxType } from 'Pages/Import/modals/TaxModal/TaxModal.types';
import classNames from 'classnames';
import React, { FC, ReactElement } from 'react';

import spacing from '../../../../../../assets/styles/spacing.module.scss';

type TaxesTabProps = {
  items?: typeof appleSettingsTaxTableData;
  onCreateTax?: (taxType: TaxType) => void;

  isLoading: boolean;
};

const Taxes: FC<TaxesTabProps> = ({
  items,
  onCreateTax,
  isLoading,
}: TaxesTabProps): ReactElement => {
  const { searchValue, setSearchValue, searchValuesList, handleSearchValueAdd, handleClearAll } =
    useSearchExample();

  const noItems = !items?.length;

  if (noItems && !isLoading)
    return (
      <AppleSettingsTabBlank
        hintText="Please create Taxes"
        actions={<CreateAppleTaxButton onCreateTax={onCreateTax} />}
      />
    );

  const filters = [
    { id: 'effectiveDate', title: 'Effective date' },
    { id: 'taxType', title: 'Tax type' },
  ];

  return (
    <Container
      flexdirection="column"
      background={'white-100'}
      borderRadius="20"
      gap="24"
      className={classNames(spacing.p24, spacing.h100p)}
    >
      <Container justifycontent="space-between" gap="24">
        <Search
          width="320"
          height="40"
          name="search-country-name"
          placeholder="Search by Country name"
          value={searchValue}
          onChange={(e) => setSearchValue(e.target.value)}
          searchParams={searchValuesList}
          onSearch={handleSearchValueAdd}
          onClear={handleClearAll}
        />

        <Container gap="24" alignitems="center">
          <DevInProgressTooltip>
            <ExportDropdown onExportToXLS={() => {}} onExportToCSV={() => {}} disabled />
          </DevInProgressTooltip>

          <CreateAppleTaxButton
            onCreateTax={onCreateTax}
            background={'violet-90-violet-100'}
            color="white-100"
          />
        </Container>
      </Container>

      <Container gap="24">
        {filters.map((item) => (
          <FilterDropDown
            key={item.id}
            title={item.title}
            value="All"
            onClear={() => {}}
            selectedAmount={0}
          />
        ))}
      </Container>

      <Container flexgrow="1" overflow="y-auto">
        <TableSkeleton isLoading={isLoading}>
          <TaxesTable />
        </TableSkeleton>
      </Container>
    </Container>
  );
};

export default Taxes;
