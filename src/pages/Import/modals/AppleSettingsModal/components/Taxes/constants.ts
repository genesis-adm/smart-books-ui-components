import { TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';

const sorting = {
  sortType: null,
  tooltipText: 'Sorting tooltip text',
};

export const appleTaxDictionaryTaxTableColumns: (TableTitleNewProps & { id: string })[] = [
  {
    id: 'countryName',
    minWidth: '100',
    title: 'Country',
    sorting,
  },
  {
    id: 'effectiveDate',
    minWidth: '150',
    title: 'Effective date',
    sorting,
  },
  {
    id: 'taxType',
    minWidth: '140',
    title: 'Tax type',
    align: 'center',
  },
  {
    id: 'taxRate',
    minWidth: '110',
    title: 'Tax rate',
    align: 'center',
    sorting,
  },
  {
    id: 'createdBy',
    minWidth: '220',
    title: 'Created By',
    flexgrow: '1',
  },
  {
    id: 'actions',
    minWidth: '60',
    title: 'Actions',
    align: 'center',
  },
];

export const appleSettingsTaxTableData = [
  {
    countryName: 'Ukraine',
    effectiveDate: '2024-02-05',
    taxType: 'VAT',
    taxRate: '20.00 %',
    createdBy: {
      fullName: 'Artem Holba',
      email: 'artem.holba@gen.tech',
    },
  },
  {
    countryName: 'Poland',
    effectiveDate: '2021-02-06',
    taxType: 'WHT',
    taxRate: '15.00 %',
    createdBy: {
      fullName: 'Anna Leonova',
      email: 'anna.leonova@gen.tech',
    },
  },
  {
    countryName: 'Italy',
    effectiveDate: '2023-02-07',
    taxType: 'Other Taxes',
    taxRate: '34.00 %',
    createdBy: {
      fullName: 'Max Medvedev',
      email: 'max.medvedev@gen.tech',
    },
  },
];
