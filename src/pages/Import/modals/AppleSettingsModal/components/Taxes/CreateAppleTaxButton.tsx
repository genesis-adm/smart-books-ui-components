import { DropDown } from 'Components/DropDown';
import { Button } from 'Components/base/buttons/Button';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { BackgroundColorNewTypes, TextColorNewTypes } from 'Components/types/colorTypes';
import { TaxType } from 'Pages/Import/modals/TaxModal/TaxModal.types';
import { ReactComponent as ChevronDown } from 'Svg/v2/16/chevron-down.svg';
import { ReactComponent as ChevronUp } from 'Svg/v2/16/chevron-up.svg';
import React, { FC, ReactElement } from 'react';

type CreateAppleTaxButtonProps = {
  onCreateTax?: (taxType: TaxType) => void;
  background?: BackgroundColorNewTypes;
  color?: TextColorNewTypes;
};

const CreateAppleTaxButton: FC<CreateAppleTaxButtonProps> = ({
  onCreateTax,
  background = 'blue-10-blue-20',
  color = 'violet-90',
}: CreateAppleTaxButtonProps): ReactElement => {
  const createTax = (taxType: TaxType) => () => {
    onCreateTax?.(taxType);
  };

  return (
    <DropDown
      flexdirection="column"
      padding="8"
      control={({ handleOpen, isOpen }) => (
        <Button
          onClick={handleOpen}
          background={background}
          color={color}
          width="140"
          height="40"
          iconRight={isOpen ? <ChevronUp /> : <ChevronDown />}
        >
          Create
        </Button>
      )}
    >
      <DropDownButton size="204" onClick={createTax('vat')}>
        VAT
      </DropDownButton>
      <DropDownButton size="204" onClick={createTax('wht')}>
        WHT
      </DropDownButton>
      <DropDownButton size="204" onClick={createTax('other')}>
        Other Taxes
      </DropDownButton>
    </DropDown>
  );
};

export default CreateAppleTaxButton;
