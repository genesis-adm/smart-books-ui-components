import { DropDown } from 'Components/DropDown';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Logo } from 'Components/elements/Logo';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import {
  appleSettingsTaxTableData,
  appleTaxDictionaryTaxTableColumns,
} from 'Pages/Import/modals/AppleSettingsModal/components/Taxes/constants';
import { ReactComponent as DropdownIcon } from 'Svg/20/dropdownbutton.svg';
import { ReactComponent as Calculator16Icon } from 'Svg/v2/16/calculator.svg';
import { ReactComponent as Trash16Icon } from 'Svg/v2/16/trashbox.svg';
import React, { FC, ReactElement } from 'react';

type TaxesTableRowProps = {
  index: number;
};

const TaxesTableRow: FC<TaxesTableRowProps> = ({ index }: TaxesTableRowProps): ReactElement => {
  const { countryName, effectiveDate, taxType, taxRate, createdBy } =
    appleSettingsTaxTableData[index];

  return (
    <TableRowNew key={index} background={'info'} size="40" cursor="auto">
      <TableCellTextNew
        minWidth={appleTaxDictionaryTaxTableColumns[0].minWidth}
        align={appleTaxDictionaryTaxTableColumns[0].align}
        flexgrow={appleTaxDictionaryTaxTableColumns[0].flexgrow}
        value={countryName}
      />
      <TableCellTextNew
        minWidth={appleTaxDictionaryTaxTableColumns[1].minWidth}
        align={appleTaxDictionaryTaxTableColumns[1].align}
        flexgrow={appleTaxDictionaryTaxTableColumns[1].flexgrow}
        value={effectiveDate}
      />
      <TableCellTextNew
        minWidth={appleTaxDictionaryTaxTableColumns[2].minWidth}
        align={appleTaxDictionaryTaxTableColumns[2].align}
        flexgrow={appleTaxDictionaryTaxTableColumns[2].flexgrow}
        value={taxType}
      />
      <TableCellTextNew
        minWidth={appleTaxDictionaryTaxTableColumns[3].minWidth}
        align={appleTaxDictionaryTaxTableColumns[3].align}
        flexgrow={appleTaxDictionaryTaxTableColumns[3].flexgrow}
        value={taxRate}
      />
      <TableCellTextNew
        minWidth={appleTaxDictionaryTaxTableColumns[4].minWidth}
        align={appleTaxDictionaryTaxTableColumns[4].align}
        flexgrow={appleTaxDictionaryTaxTableColumns[4].flexgrow}
        secondaryColor="grey-100"
        value={createdBy.fullName}
        secondaryValue={createdBy.email}
        iconLeft={<Logo content="user" radius="rounded" size="24" name={createdBy.fullName} />}
      />
      <TableCellNew
        minWidth={appleTaxDictionaryTaxTableColumns[5].minWidth}
        flexgrow={appleTaxDictionaryTaxTableColumns[5].flexgrow}
        alignitems="center"
        justifycontent="center"
      >
        <DropDown
          flexdirection="column"
          padding="8"
          gap="4"
          control={({ handleOpen }) => (
            <IconButton
              icon={<DropdownIcon />}
              size="24"
              onClick={handleOpen}
              color="grey-100-violet-90"
            />
          )}
        >
          <DropDownButton size="160" icon={<Calculator16Icon />} onClick={() => {}}>
            Recalculate
          </DropDownButton>
          <DropDownButton size="160" icon={<Trash16Icon />} onClick={() => {}}>
            Delete
          </DropDownButton>
        </DropDown>
      </TableCellNew>
    </TableRowNew>
  );
};

export default TaxesTableRow;
