import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableTitleNew } from 'Components/elements/Table/TableTitleNew';
import TaxesTableRow from 'Pages/Import/modals/AppleSettingsModal/components/Taxes/TaxesTableRow';
import {
  appleSettingsTaxTableData,
  appleTaxDictionaryTaxTableColumns,
} from 'Pages/Import/modals/AppleSettingsModal/components/Taxes/constants';
import React, { FC, ReactElement } from 'react';

import spacing from '../../../../../../assets/styles/spacing.module.scss';

const TaxesTable: FC = (): ReactElement => {
  return (
    <TableNew
      noResults={false}
      tableHead={
        <TableHeadNew radius="10" background={'grey-10'} className={spacing.pY6}>
          {appleTaxDictionaryTaxTableColumns.map(({ id, ...props }) => (
            <TableTitleNew
              key={id}
              background={'grey-10'}
              radius="10"
              {...props}
              onClick={() => {}}
            />
          ))}
        </TableHeadNew>
      }
    >
      <TableBodyNew>
        {appleSettingsTaxTableData.map((_, index) => (
          <TaxesTableRow key={index} index={index} />
        ))}
      </TableBodyNew>
    </TableNew>
  );
};

export default TaxesTable;
