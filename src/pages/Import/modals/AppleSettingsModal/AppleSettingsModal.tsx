import { Container } from 'Components/base/grid/Container';
import { MenuTab, MenuTabWrapper } from 'Components/base/tabs/MenuTab';
import { Modal } from 'Components/modules/Modal';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import AppleCurrencyRatesImportModal from 'Pages/Import/modals/AppleCurrencyRatesImportModal/AppleCurrencyRatesImportModal';
import ApplePaymentPeriodModal from 'Pages/Import/modals/ApplePaymentPeriodModal/ApplePaymentPeriodModal';
import { TabKey } from 'Pages/Import/modals/AppleSettingsModal/AppleSettingsModal.types';
import AppleCurrencyRates from 'Pages/Import/modals/AppleSettingsModal/components/AppleCurrencyRates/AppleCurrencyRates';
import { appleSettingsCurrencyRatesTableData } from 'Pages/Import/modals/AppleSettingsModal/components/AppleCurrencyRates/constants';
import ApplePaymentPeriods from 'Pages/Import/modals/AppleSettingsModal/components/ApplePaymentPeriods/ApplePaymentPeriods';
import { appleSettingsPaymentPeriodsTableData } from 'Pages/Import/modals/AppleSettingsModal/components/ApplePaymentPeriods/constants';
import Taxes from 'Pages/Import/modals/AppleSettingsModal/components/Taxes/Taxes';
import { appleSettingsTaxTableData } from 'Pages/Import/modals/AppleSettingsModal/components/Taxes/constants';
import {
  APPLE_SETTINGS_MODAL_ID,
  appleSettingsModalTabs,
} from 'Pages/Import/modals/AppleSettingsModal/constants';
import TaxModal from 'Pages/Import/modals/TaxModal/TaxModal';
import { TaxType } from 'Pages/Import/modals/TaxModal/TaxModal.types';
import classNames from 'classnames';
import React, { FC, ReactNode, useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';
import { useToggle } from '../../../../hooks/useToggle';
import { Nullable } from '../../../../types/general';

type AppleTaxDictionaryModalProps = {
  isLoaded: boolean;
  isLoading: boolean;
  onClose?: () => void;
};

const AppleSettingsModal: FC<AppleTaxDictionaryModalProps> = ({
  isLoaded,
  isLoading,
  onClose = () => {},
}: AppleTaxDictionaryModalProps) => {
  const [taxType, setTaxType] = useState<Nullable<TaxType>>(null);
  const appleCurrencyRatesImportModalToggle = useToggle();
  const applePaymentPeriodModalToggle = useToggle();

  const closeTaxModal = () => setTaxType(null);

  const [activeTabId, setActiveTabId] = useState<TabKey>('taxes');

  const taxItems = isLoaded ? appleSettingsTaxTableData : undefined;
  const paymentPeriodsItems = isLoaded ? appleSettingsPaymentPeriodsTableData : undefined;
  const currencyRatesItems = isLoaded ? appleSettingsCurrencyRatesTableData : undefined;

  const tabsById: Record<TabKey, { render: (isLoading: boolean) => ReactNode }> = {
    taxes: {
      render: (isLoading) => (
        <Taxes items={taxItems} onCreateTax={setTaxType} isLoading={isLoading} />
      ),
    },
    currencyRate: {
      render: (isLoading) => (
        <AppleCurrencyRates
          isLoading={isLoading}
          items={currencyRatesItems}
          onImport={appleCurrencyRatesImportModalToggle.onOpen}
        />
      ),
    },
    paymentPeriod: {
      render: (isLoading) => (
        <ApplePaymentPeriods
          items={paymentPeriodsItems}
          isLoading={isLoading}
          onCreate={applePaymentPeriodModalToggle.onOpen}
        />
      ),
    },
  };

  return (
    <Modal
      modalId={APPLE_SETTINGS_MODAL_ID}
      size="960"
      type="new"
      background={'grey-10'}
      flexdirection="column"
      classNameModalBody={spacing.h100p}
      pluginScrollDisabled
      header={
        <ModalHeaderNew
          height="72"
          border
          closeActionBackgroundColor="grey-10"
          title="Apple settings"
          background={'grey-10'}
          onClose={onClose}
        />
      }
    >
      <MenuTabWrapper>
        {appleSettingsModalTabs.map(({ id, label }) => {
          return (
            <MenuTab
              key={id}
              id={id}
              active={activeTabId === id}
              onClick={setActiveTabId as (id: TabKey) => void}
            >
              {label}
            </MenuTab>
          );
        })}
      </MenuTabWrapper>

      <Container
        flexgrow="1"
        overflow="y-auto"
        flexdirection="column"
        className={classNames(spacing.pY16)}
      >
        {tabsById[activeTabId].render(isLoading)}
      </Container>

      {!!taxType && (
        <TaxModal type={taxType} containerId={APPLE_SETTINGS_MODAL_ID} onClose={closeTaxModal} />
      )}

      {applePaymentPeriodModalToggle.isOpen && (
        <ApplePaymentPeriodModal
          mode="create"
          containerId={APPLE_SETTINGS_MODAL_ID}
          isLoading={isLoading}
          onClose={applePaymentPeriodModalToggle.onClose}
        />
      )}

      {appleCurrencyRatesImportModalToggle.isOpen && (
        <AppleCurrencyRatesImportModal
          containerId={APPLE_SETTINGS_MODAL_ID}
          isLoading={isLoading}
          onClose={appleCurrencyRatesImportModalToggle.onClose}
        />
      )}
    </Modal>
  );
};

export default AppleSettingsModal;
