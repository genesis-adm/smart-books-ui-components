import {
  AppleCurrencyRatesImportModalContext,
  AppleCurrencyRatesImportModalContextValue,
} from 'Pages/Import/modals/AppleCurrencyRatesImportModal/components/AppleCurrencyRatesImportModalContextProvider';
import { useContext } from 'react';

const useAppleCurrencyRatesImportModalContext = () =>
  useContext<AppleCurrencyRatesImportModalContextValue>(AppleCurrencyRatesImportModalContext);

export default useAppleCurrencyRatesImportModalContext;
