import { ContentLoader } from 'Components/base/ContentLoader';
import { Button, ButtonProps } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { AppleCurrencyRatesImportModalStepId } from 'Pages/Import/modals/AppleCurrencyRatesImportModal/AppleCurrencyRatesImportModal.types';
import { appleCurrencyRatesImportTableData } from 'Pages/Import/modals/AppleCurrencyRatesImportModal/constants';
import useAppleCurrencyRatesImportModalContext from 'Pages/Import/modals/AppleCurrencyRatesImportModal/hooks/useAppleCurrencyRatesImportModalContext';
import { ReactComponent as ArrowLeftIcon } from 'Svg/v2/16/arrow-left.svg';
import { ReactComponent as DownloadIcon } from 'Svg/v2/16/download.svg';
import React, { FC, ReactElement, ReactNode } from 'react';

import { UseSteps } from '../../../../../hooks/useSteps';

type AppleCurrencyRatesImportModalFooterProps = {
  isParsedData: boolean;
  stepsOptions: UseSteps<AppleCurrencyRatesImportModalStepId>;

  isLoading: boolean;
  onAction?: () => void;
};

const AppleCurrencyRatesImportModalFooter: FC<AppleCurrencyRatesImportModalFooterProps> = ({
  isParsedData,
  stepsOptions,
  isLoading,
  onAction,
}: AppleCurrencyRatesImportModalFooterProps): ReactElement => {
  const { contextData } = useAppleCurrencyRatesImportModalContext();
  const { externalCabinet, paymentPeriodMonth, file } = contextData;

  const tableItems = isParsedData ? appleCurrencyRatesImportTableData : [];

  const rowListLength = tableItems?.length;

  const {
    stepId,
    stepNumber,
    stepsLength,
    isFirstStep,
    isLastStep,
    prevStep,
    nextStep,
    nextStepTitle,
    currentStepTitle,
  } = stepsOptions;

  const actionButtonByStepId: Record<
    AppleCurrencyRatesImportModalStepId,
    { width: ButtonProps['width']; disabled: boolean; text: string; iconLeft?: ReactNode }
  > = {
    main: {
      width: '108',
      disabled: !externalCabinet || !paymentPeriodMonth || !file,
      text: 'Next',
    },
    preview: {
      width: '156',
      disabled: !rowListLength,
      iconLeft: <DownloadIcon />,
      text: rowListLength
        ? `Import ${rowListLength} ${rowListLength === 1 ? 'row' : 'rows'}`
        : 'Import',
    },
  };

  const btnAction = () => {
    isLastStep ? submit() : nextStep();
  };

  const submit = async (): Promise<void> => onAction?.();

  return (
    <ModalFooterNew border>
      <Container alignitems="center" gap="16" flexgrow="1" justifycontent="space-between">
        {!isFirstStep && !isLoading && (
          <Button
            width="100"
            height="40"
            navigation
            iconLeft={<ArrowLeftIcon />}
            background={'white-100'}
            color="black-100"
            border="grey-20-grey-90"
            onClick={prevStep}
          >
            Back
          </Button>
        )}

        <Container flexgrow="1" alignitems="center" justifycontent="flex-end" gap="16">
          <Container flexdirection="column" gap={isLoading ? '4' : undefined}>
            <ContentLoader isLoading={isLoading} width="80px" height="22px">
              <Text type="body-regular-14" color="grey-100">
                {`Step: ${stepNumber} of ${stepsLength}`}
              </Text>
            </ContentLoader>
            <ContentLoader isLoading={isLoading} width="224px" height="22px">
              <Text type="body-regular-14" color="grey-100">
                {nextStepTitle ? `Next: ${nextStepTitle}` : currentStepTitle}
              </Text>
            </ContentLoader>
          </Container>
          <ContentLoader height="40px" width="100px" isLoading={isLoading}>
            <Button height="40" onClick={btnAction} {...actionButtonByStepId[stepId]}>
              {actionButtonByStepId[stepId].text}
            </Button>
          </ContentLoader>
        </Container>
      </Container>
    </ModalFooterNew>
  );
};

export default AppleCurrencyRatesImportModalFooter;
