import { Checkbox } from 'Components/base/Checkbox';
import { Divider } from 'Components/base/Divider';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { NumericInput } from 'Components/base/inputs/NumericInput';
import { Text } from 'Components/base/typography/Text';
import CurrencySelect from 'Components/custom/Stories/selects/CurrencySelect';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableTitleNew } from 'Components/elements/Table/TableTitleNew';
import { TextColorNewTypes } from 'Components/types/colorTypes';
import { TableCellWidth } from 'Components/types/gridTypes';
import {
  appleCurrencyRatesImportTableColumns,
  appleCurrencyRatesImportTableData,
} from 'Pages/Import/modals/AppleCurrencyRatesImportModal/constants';
import useAppleCurrencyRatesImportModalContext from 'Pages/Import/modals/AppleCurrencyRatesImportModal/hooks/useAppleCurrencyRatesImportModalContext';
import { ReactComponent as CheckIcon } from 'Svg/v2/16/tick-in-filled-circle.svg';
import classNames from 'classnames';
import React, { FC, ReactElement } from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';

type AppleCurrencyRatesImportModalPreviewProps = {
  isParsedData: boolean;
};

const AppleCurrencyRatesImportModalPreview: FC<AppleCurrencyRatesImportModalPreviewProps> = ({
  isParsedData,
}: AppleCurrencyRatesImportModalPreviewProps): ReactElement => {
  const { contextData } = useAppleCurrencyRatesImportModalContext();
  const { externalCabinet } = contextData;

  const tableItems = isParsedData ? appleCurrencyRatesImportTableData : [];

  const validRows = tableItems?.length;
  const nonValidRows = 0;

  const dashboardItems: { label: string; value: number; color?: TextColorNewTypes }[] = [
    { label: 'Total rows:', value: tableItems?.length },
    {
      label: 'Number of valid rows:',
      value: validRows,
      color: validRows ? 'green-100' : 'red-100',
    },
    {
      label: 'Number of non-valid rows:',
      value: nonValidRows,
      color: nonValidRows ? 'green-100' : 'red-100',
    },
  ];

  return (
    <Container
      flexdirection="column"
      alignitems="center"
      className={classNames(spacing.w100p, spacing.pX24, spacing.pt8, spacing.pb48)}
      gap="16"
    >
      <Container justifycontent="center" gap="12">
        <Text type="text-medium" color="grey-100">
          Currency rates available for import:
        </Text>

        <Container gap="16">
          {dashboardItems.map(({ label, value, color }, index) => (
            <>
              {!!index && <Divider type="vertical" height="full" />}
              <Container key={label} alignitems="center" gap="8">
                <Text type="text-medium" color="grey-100">
                  {label}
                </Text>
                <Text type="caption-semibold" color={color}>
                  {value}
                </Text>
              </Container>
            </>
          ))}
        </Container>
      </Container>

      <Container
        className={classNames(spacing.w100p, spacing.h100p, spacing.p24)}
        borderRadius="20"
        background={'grey-10'}
      >
        <TableNew
          noResults={false}
          tableHead={
            <TableHeadNew radius="10" className={spacing.pY4}>
              {appleCurrencyRatesImportTableColumns.map(({ id, ...props }) => (
                <TableTitleNew key={id} radius="10" {...props} onClick={() => {}} />
              ))}
            </TableHeadNew>
          }
        >
          <TableBodyNew>
            {tableItems?.map(({ currencyCode, currencyRate }, index) => (
              <TableRowNew key={index} background={'white'} size="52">
                <TableCellNew
                  minWidth={appleCurrencyRatesImportTableColumns[0].minWidth as TableCellWidth}
                  justifycontent="center"
                  alignitems="center"
                  className={spacing.pY22}
                  fixedWidth
                >
                  <Checkbox name={`line-id`} checked onChange={() => {}} />
                </TableCellNew>
                <TableCellNew
                  minWidth={appleCurrencyRatesImportTableColumns[1].minWidth as TableCellWidth}
                  alignitems="center"
                  padding="0"
                  flexgrow={appleCurrencyRatesImportTableColumns[1].flexgrow}
                >
                  <CurrencySelect
                    height="36"
                    width="248"
                    defaultValue={{ value: currencyCode, label: currencyCode }}
                  />
                </TableCellNew>
                <TableCellNew
                  minWidth={appleCurrencyRatesImportTableColumns[2].minWidth as TableCellWidth}
                  alignitems="center"
                  gap="12"
                  fixedWidth
                >
                  <Text color="grey-100">{`${currencyCode} to ${externalCabinet?.ccyCode}`}</Text>
                  <NumericInput
                    height="36"
                    width="96"
                    label=""
                    name="currencyRate"
                    value={currencyRate}
                  />
                </TableCellNew>
                <TableCellNew
                  minWidth={appleCurrencyRatesImportTableColumns[3].minWidth as TableCellWidth}
                  alignitems="center"
                  justifycontent="center"
                  fixedWidth
                >
                  <IconButton
                    icon={<CheckIcon />}
                    size="24"
                    background={'green-10'}
                    color="green-100"
                  />
                </TableCellNew>
              </TableRowNew>
            ))}
          </TableBodyNew>
        </TableNew>
      </Container>
    </Container>
  );
};

export default AppleCurrencyRatesImportModalPreview;
