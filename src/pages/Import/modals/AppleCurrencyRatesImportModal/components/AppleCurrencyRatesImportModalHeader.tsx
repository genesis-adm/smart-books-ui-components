import { Tag } from 'Components/base/Tag';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { AppleCurrencyRatesImportModalStepId } from 'Pages/Import/modals/AppleCurrencyRatesImportModal/AppleCurrencyRatesImportModal.types';
import useAppleCurrencyRatesImportModalContext from 'Pages/Import/modals/AppleCurrencyRatesImportModal/hooks/useAppleCurrencyRatesImportModalContext';
import { formatApiDate } from 'Utils/date';
import classNames from 'classnames';
import { format } from 'date-fns';
import React, { FC, ReactElement, ReactNode } from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';

type AppleCurrencyRatesImportModalHeaderProps = {
  title: ReactNode;
  step: AppleCurrencyRatesImportModalStepId;
  onClose: () => void;
};

const AppleCurrencyRatesImportModalHeader: FC<AppleCurrencyRatesImportModalHeaderProps> = ({
  title,
  step,
  onClose,
}: AppleCurrencyRatesImportModalHeaderProps): ReactElement => {
  const { contextData } = useAppleCurrencyRatesImportModalContext();
  const { externalCabinet, paymentPeriodMonth, file, paymentDate = '' } = contextData;

  const tagsListData: ReactNode[] = [
    !!externalCabinet?.name && (
      <Tag className={spacing.w100} padding="4" text={externalCabinet.name} noWrap />
    ),
    !!paymentPeriodMonth && (
      <Container gap="4" className={classNames(spacing.pX8, spacing.pY4)}>
        <Text>{paymentPeriodMonth.name}:</Text>
        <Container className={spacing.w150}>
          <Text color="grey-100" noWrap>{`${formatApiDate(
            paymentPeriodMonth.from,
          )} - ${formatApiDate(paymentPeriodMonth.to)}`}</Text>
        </Container>
      </Container>
    ),
    file?.name && (
      <Tag className={spacing.w160} padding="4" text={`File name: ${file.name}`} noWrap />
    ),
    externalCabinet && (
      <Container gap="4" className={classNames(spacing.pX8, spacing.pY4)}>
        <Text color="grey-100">Cabinet Currency:</Text>
        <Container className={spacing.w30}>
          <Text noWrap>{externalCabinet.ccyCode}</Text>
        </Container>
      </Container>
    ),
    paymentDate && (
      <Container gap="4" className={classNames(spacing.pX8, spacing.pY4)}>
        <Text color="grey-100">Payment date:</Text>
        <Container>
          <Text noWrap>{format(paymentDate, 'dd MMM yyyy')}</Text>
        </Container>
      </Container>
    ),
  ].filter((item) => !!item) as ReactNode[];

  const normalizedStepTitleByStepId: Record<
    AppleCurrencyRatesImportModalStepId,
    { title: ReactNode; height: '60' | '128' }
  > = {
    main: {
      title,
      height: '60',
    },
    preview: {
      title: (
        <Container
          flexdirection="column"
          alignitems="center"
          className={spacing.w740min}
          overflow="hidden"
          gap="16"
        >
          <Text type="body-medium">{title}</Text>
          <Container gap="12" className={spacing.w100p} flexwrap="wrap">
            {tagsListData.map((text, index) => (
              <Container
                key={index}
                justifycontent="center"
                alignitems="center"
                background={'grey-20'}
                borderRadius="8"
                overflow="hidden"
              >
                {text}
              </Container>
            ))}
          </Container>
        </Container>
      ),
      height: '128',
    },
  };

  return (
    <ModalHeaderNew
      height={normalizedStepTitleByStepId[step].height}
      title={normalizedStepTitleByStepId[step].title}
      fontType="body-medium"
      titlePosition="center"
      border
      onClose={onClose}
    />
  );
};

export default AppleCurrencyRatesImportModalHeader;
