import { ContentLoader } from 'Components/base/ContentLoader';
import { Icon } from 'Components/base/Icon';
import { IconButton } from 'Components/base/buttons/IconButton';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import style from 'Components/custom/Sales/Products/TaxInformationCard/TaxInformationCard.module.scss';
import UploadFile from 'Components/custom/Stories/Upload/UploadFile';
import useAppleCurrencyRatesImportModalContext from 'Pages/Import/modals/AppleCurrencyRatesImportModal/hooks/useAppleCurrencyRatesImportModalContext';
import ChooseApplePaymentPeriodMonthModal from 'Pages/Import/modals/ChooseApplePaymentPeriodMonthModal/ChooseApplePaymentPeriodMonthModal';
import { ApplePaymentPeriodMonth } from 'Pages/Import/modals/ChooseApplePaymentPeriodMonthModal/ChooseApplePaymentPeriodMonthModal.types';
import { ExternalCabinet } from 'Pages/Integration/ExternalCabinet/ExternalCabinet.types';
import ChooseExternalCabinetModal from 'Pages/Integration/ExternalCabinet/modals/ChooseExternalCabinetModal/ChooseExternalCabinetModal';
import { ReactComponent as Pencil16Icon } from 'Svg/v2/16/pencil.svg';
import { ReactComponent as Plus16Icon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as TickInFilledCircle16Icon } from 'Svg/v2/16/tick-in-filled-circle.svg';
import { formatApiDate } from 'Utils/date';
import classNames from 'classnames';
import { format } from 'date-fns';
import React, { FC, ReactElement, useCallback, useEffect } from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';
import useHoverContainer from '../../../../../hooks/useHoverContainer';
import { useToggle } from '../../../../../hooks/useToggle';
import ChooseDateModal from '../../../../../modules/general/date/modals/ChooseDateModal';
import { Nullable } from '../../../../../types/general';
import { AppleCurrencyRatesImportModalStepData } from '../AppleCurrencyRatesImportModal.types';

type AppleCurrencyRatesImportModalMainProps = {
  isLoading: boolean;
  resetForm: boolean;
};

const AppleCurrencyRatesImportModalMain: FC<AppleCurrencyRatesImportModalMainProps> = ({
  isLoading,
  resetForm,
}: AppleCurrencyRatesImportModalMainProps): ReactElement => {
  const {
    isHovered: externalCabinetIsHovered,
    containerHoverProps: externalCabinetContainerHoverProps,
  } = useHoverContainer();
  const {
    isHovered: paymentPeriodMonthIsHovered,
    containerHoverProps: paymentPeriodMonthContainerHoverProps,
  } = useHoverContainer();
  const { isHovered: fileIsHovered, containerHoverProps: fileContainerHoverProps } =
    useHoverContainer();
  const { isHovered: paymentDateIsHovered, containerHoverProps: paymentDateContainerHoverProps } =
    useHoverContainer();

  const { contextData, setContextData } = useAppleCurrencyRatesImportModalContext();
  const { externalCabinet, paymentPeriodMonth, file, paymentDate } = contextData;
  const chooseExternalCabinetModalToggle = useToggle();

  const chooseApplePaymentPeriodMonthModalToggle = useToggle();

  const chooseDateModalToggle = useToggle();

  const resetFormValues = useCallback(() => {
    setContextData({ externalCabinet: null, paymentPeriodMonth: null, file: null });
  }, [setContextData]);

  const changeExternalCabinet = (externalCabinet: Nullable<ExternalCabinet>): void => {
    setContextData({ externalCabinet });
  };

  const changePaymentPeriodMonth = (
    paymentPeriodMonth: Nullable<ApplePaymentPeriodMonth>,
  ): void => {
    setContextData({ paymentPeriodMonth });
  };

  const changePaymentDate = (paymentDate: Nullable<Date>) => {
    setContextData({ paymentDate });
  };

  const changeFile = (file: Nullable<File>) => {
    setContextData({ file });
  };

  const steps = [
    {
      title: 'Step 1: please add Cabinet Name',
      button: {
        text: 'Add Cabinet',
        tooltip: 'Add Cabinet',
        action: chooseExternalCabinetModalToggle.onOpen,
      },
      editTooltip: 'Edit cabinet name',
      hovered: externalCabinetIsHovered,
      hoverProps: externalCabinetContainerHoverProps,
      value: externalCabinet?.name || '',
    },
    {
      title: 'Step 2: please add Payment period',
      button: {
        text: 'Add Date',
        tooltip: 'Add Date',
        action: chooseApplePaymentPeriodMonthModalToggle.onOpen,
      },
      editTooltip: 'Add Date',
      hovered: paymentPeriodMonthIsHovered,
      hoverProps: paymentPeriodMonthContainerHoverProps,
      value: !!paymentPeriodMonth && (
        <Container alignitems="flex-end" gap="4">
          <Text type="body-regular-16" style={{ lineHeight: 'initial' }}>
            {paymentPeriodMonth.name}
          </Text>
          <Text color="grey-100">{`${formatApiDate(paymentPeriodMonth.from)} - ${formatApiDate(
            paymentPeriodMonth.to,
          )}`}</Text>
        </Container>
      ),
    },
    {
      title: 'Step 3: Please Upload file',
      hovered: fileIsHovered,
      hoverProps: fileContainerHoverProps,
      value: !!file && file.name,
      button: {
        text: '',
        tooltip: '',
        action: () => {},
      },
    },
    {
      title: 'Step 4: Please Add Payment date',
      hovered: paymentDateIsHovered,
      hoverProps: paymentDateContainerHoverProps,
      value: !!paymentDate && format(paymentDate, 'dd MMM yyyy'),
      editTooltip: 'Edit Payment Date',
      button: {
        text: 'Add Date',
        tooltip: 'Add Payment Date',
        action: chooseDateModalToggle.onOpen,
      },
    },
  ];

  const paymentDateStepData = steps[3];

  useEffect(() => {
    if (resetForm) {
      resetFormValues();
    }
  }, [resetForm, resetFormValues]);

  return (
    <Container flexdirection="column" gap="24" className={spacing.w100p}>
      <Container justifycontent="space-between">
        {steps?.slice(0, 2).map((step, index) => (
          <Component
            key={step.title}
            isLoading={isLoading}
            disabled={!!index && !steps?.slice(0, 2)?.[index - 1]?.value}
            {...step}
          />
        ))}
      </Container>

      <ContentLoader height="176px" isLoading={isLoading}>
        <Container
          flexdirection="column"
          background={steps[2]?.hovered ? 'grey-20' : 'grey-10'}
          {...steps[2]?.hoverProps}
          border="grey-20"
          borderRadius="20"
          gap="16"
          className={classNames(spacing.pY16, spacing.pX24)}
        >
          <Container alignitems="center" gap="4">
            <Icon
              path={steps[2]?.value ? 'green-90' : 'grey-90'}
              icon={<TickInFilledCircle16Icon />}
            />
            <Text type="caption-regular" color="grey-90" transform="uppercase">
              {steps[2]?.title}
            </Text>
          </Container>

          <Container justifycontent="space-between">
            <Container style={{ flexBasis: '50%', maxWidth: '50%' }}>
              <UploadFile file={file} setFile={changeFile} style={{ flexWrap: 'nowrap' }} />
            </Container>

            <Container flexdirection="column" gap="12" style={{ flexBasis: '45%' }}>
              <Text type="caption-regular" color="grey-100">
                Instruction for upload file:
              </Text>

              <Container flexdirection="column" gap="16">
                <Text type="caption-regular" color="grey-100">
                  - All your information must be in one file
                </Text>
                <Text type="caption-regular" color="grey-100">
                  - The top row of your file must contain a header title for each column of
                  information
                </Text>
              </Container>
            </Container>
          </Container>
        </Container>
      </ContentLoader>

      <Component isLoading={isLoading} disabled={!steps?.[2]?.value} {...paymentDateStepData} />

      {chooseExternalCabinetModalToggle.isOpen && (
        <ChooseExternalCabinetModal
          type="apple"
          isLoading={isLoading}
          onSubmit={changeExternalCabinet}
          onClose={chooseExternalCabinetModalToggle.onClose}
          externalCabinetId={externalCabinet?.id}
        />
      )}

      {chooseApplePaymentPeriodMonthModalToggle.isOpen && (
        <ChooseApplePaymentPeriodMonthModal
          applePaymentPeriodMonth={paymentPeriodMonth || undefined}
          isLoading={isLoading}
          onSubmit={changePaymentPeriodMonth}
          onClose={chooseApplePaymentPeriodMonthModalToggle.onClose}
        />
      )}

      {chooseDateModalToggle.isOpen && (
        <ChooseDateModal
          title="Please Add Payment date"
          value={paymentDate}
          onSubmit={changePaymentDate}
          onClose={chooseDateModalToggle.onClose}
        />
      )}
    </Container>
  );
};

export default AppleCurrencyRatesImportModalMain;

type ComponentProps = {
  isLoading: boolean;
  disabled?: boolean;
} & AppleCurrencyRatesImportModalStepData;

const Component: FC<ComponentProps> = ({
  isLoading,
  title,
  button,
  editTooltip,
  hovered,
  hoverProps,
  value,
  disabled,
}: ComponentProps): ReactElement => {
  return (
    <ContentLoader height="86px" width="360px" isLoading={isLoading}>
      <Container
        justifycontent="space-between"
        background={hovered ? 'grey-20' : 'grey-10'}
        border="grey-20"
        borderRadius="20"
        {...hoverProps}
        className={classNames(spacing.w360fixed, spacing.h86fixed, spacing.pY16, spacing.pX24)}
      >
        <Container flexdirection="column" justifycontent="space-between">
          <Container alignitems="center" gap="4">
            <Icon path={value ? 'green-90' : 'grey-90'} icon={<TickInFilledCircle16Icon />} />
            <Text type="caption-regular" color="grey-90" transform="uppercase">
              {title}
            </Text>
          </Container>

          {value ? (
            <Text type="body-regular-16">{value}</Text>
          ) : (
            <Container display="inline-block">
              <LinkButton
                icon={<Plus16Icon />}
                tooltip={button?.tooltip}
                type="body-regular-16"
                color="violet-90"
                onClick={button?.action}
                disabled={disabled}
              >
                {button?.text}
              </LinkButton>
            </Container>
          )}
        </Container>

        {!!value && (
          <IconButton
            tooltip={editTooltip}
            icon={<Pencil16Icon />}
            size="24"
            color="grey-100-black-100"
            background={'transparent-grey-20'}
            onClick={button?.action}
          />
        )}
      </Container>
    </ContentLoader>
  );
};
