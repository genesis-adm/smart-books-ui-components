import ContextProvider, { ContextValue } from 'Components/ContextProvider/ContextProvider';
import { ApplePaymentPeriodMonth } from 'Pages/Import/modals/ChooseApplePaymentPeriodMonthModal/ChooseApplePaymentPeriodMonthModal.types';
import { ExternalCabinet } from 'Pages/Integration/ExternalCabinet/ExternalCabinet.types';
import React, { ReactElement, ReactNode, createContext } from 'react';

import { Nullable } from '../../../../../types/general';

type AppleCurrencyRatesImportModalContextData = {
  externalCabinet: Nullable<ExternalCabinet>;
  paymentPeriodMonth: Nullable<ApplePaymentPeriodMonth>;
  file: Nullable<File>;
  paymentDate: Nullable<Date>;
};

export type AppleCurrencyRatesImportModalContextValue =
  ContextValue<AppleCurrencyRatesImportModalContextData>;

export const AppleCurrencyRatesImportModalContext = createContext(
  {} as AppleCurrencyRatesImportModalContextValue,
);

const AppleCurrencyRatesImportModalContextProvider = ({
  children,
}: {
  children: ReactNode;
}): ReactElement => (
  <ContextProvider Context={AppleCurrencyRatesImportModalContext}>{children}</ContextProvider>
);

export default AppleCurrencyRatesImportModalContextProvider;
