import ModalHeaderSkeleton from 'Components/loaders/ModalHeaderSkeleton/ModalHeaderSkeleton';
import { Modal } from 'Components/modules/Modal';
import { AppleCurrencyRatesImportModalStepId } from 'Pages/Import/modals/AppleCurrencyRatesImportModal/AppleCurrencyRatesImportModal.types';
import AppleCurrencyRatesImportModalContextProvider from 'Pages/Import/modals/AppleCurrencyRatesImportModal/components/AppleCurrencyRatesImportModalContextProvider';
import AppleCurrencyRatesImportModalFooter from 'Pages/Import/modals/AppleCurrencyRatesImportModal/components/AppleCurrencyRatesImportModalFooter';
import AppleCurrencyRatesImportModalHeader from 'Pages/Import/modals/AppleCurrencyRatesImportModal/components/AppleCurrencyRatesImportModalHeader';
import AppleCurrencyRatesImportModalMain from 'Pages/Import/modals/AppleCurrencyRatesImportModal/components/AppleCurrencyRatesImportModalMain';
import AppleCurrencyRatesImportModalPreview from 'Pages/Import/modals/AppleCurrencyRatesImportModal/components/AppleCurrencyRatesImportModalPreview';
import { appleCurrencyRatesImportModalSteps } from 'Pages/Import/modals/AppleCurrencyRatesImportModal/constants';
import React, { FC, ReactElement, ReactNode, useEffect, useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';
import useSteps from '../../../../hooks/useSteps';

type AppleCurrencyRatesImportModalProps = {
  isLoading?: boolean;
  onClose?: () => void;
  isParsedData?: boolean;

  containerId?: string;
};

const AppleCurrencyRatesImportModal: FC<AppleCurrencyRatesImportModalProps> = ({
  isLoading = false,
  isParsedData = true,
  onClose,
  containerId,
}: AppleCurrencyRatesImportModalProps): ReactElement => {
  const [resetForm, setResetForm] = useState(false);
  const stepsOptions = useSteps(appleCurrencyRatesImportModalSteps, 'main');
  const { stepId, currentStepTitle } = stepsOptions;

  const steps: Record<AppleCurrencyRatesImportModalStepId, { render: () => ReactNode }> = {
    main: {
      render: () => (
        <AppleCurrencyRatesImportModalMain resetForm={resetForm} isLoading={isLoading} />
      ),
    },
    preview: { render: () => <AppleCurrencyRatesImportModalPreview isParsedData={isParsedData} /> },
  };

  const closeModal = () => {
    if (!onClose) {
      setResetForm(true);
      return;
    }

    onClose();
  };

  const clickFooterAction = () => {};

  useEffect(() => {
    resetForm && setResetForm(false);
  }, [resetForm]);

  return (
    <AppleCurrencyRatesImportModalContextProvider>
      <Modal
        size="800"
        type="old"
        containerId={containerId}
        padding="24"
        pluginScrollDisabled
        flexdirection="row"
        classNameModalBody={spacing.h100p}
        overlay={containerId ? 'white-100' : 'black-100'}
        header={
          <ModalHeaderSkeleton heightPx={60} titleWidthPx={160} border isLoading={isLoading}>
            <AppleCurrencyRatesImportModalHeader
              step={stepId}
              title={currentStepTitle}
              onClose={closeModal}
            />
          </ModalHeaderSkeleton>
        }
        footer={
          <AppleCurrencyRatesImportModalFooter
            isParsedData={isParsedData}
            stepsOptions={stepsOptions}
            isLoading={isLoading}
            onAction={clickFooterAction}
          />
        }
      >
        {steps?.[stepId]?.render()}
      </Modal>
    </AppleCurrencyRatesImportModalContextProvider>
  );
};

export default AppleCurrencyRatesImportModal;
