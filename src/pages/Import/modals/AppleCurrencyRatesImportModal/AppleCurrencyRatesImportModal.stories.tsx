import { Meta, Story } from '@storybook/react';
import AppleCurrencyRatesImportModal from 'Pages/Import/modals/AppleCurrencyRatesImportModal/AppleCurrencyRatesImportModal';
import React from 'react';

const AppleCurrencyRatesImportModalComponent: Story = ({ storyState, parsedData }) => {
  const isLoading = storyState === 'loading';
  const isParsedData = parsedData === 'items';

  return <AppleCurrencyRatesImportModal isLoading={isLoading} isParsedData={isParsedData} />;
};

export default {
  title: 'Pages/Import page/modals/Apple Currency Rates Import',
  component: AppleCurrencyRatesImportModalComponent,
  argTypes: {
    storyState: {
      options: ['loading', 'loaded'],
      control: {
        type: 'radio',
        labels: {
          loading: 'Data is loading',
          loaded: 'Data loaded',
        },
      },
    },
    parsedData: {
      options: ['items', 'noItems'],
      control: {
        type: 'radio',
        labels: {
          items: 'Items',
          noItems: 'No items',
        },
      },
    },
  },
} as Meta;

export const AppleCurrencyRatesImport = AppleCurrencyRatesImportModalComponent.bind({});
AppleCurrencyRatesImport.args = {
  storyState: 'loaded',
  parsedData: 'items',
};
