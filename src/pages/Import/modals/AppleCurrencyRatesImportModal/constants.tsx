import { Checkbox } from 'Components/base/Checkbox';
import { TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { AppleCurrencyRatesImportModalStepId } from 'Pages/Import/modals/AppleCurrencyRatesImportModal/AppleCurrencyRatesImportModal.types';
import React from 'react';

export const appleCurrencyRatesImportModalSteps: {
  id: AppleCurrencyRatesImportModalStepId;
  title: string;
}[] = [
  { id: 'main', title: 'Import Currency rate' },
  { id: 'preview', title: 'Preview mode' },
];

export const sorting = {
  sortType: null,
  tooltipText: 'Sorting tooltip text',
};

export const appleCurrencyRatesImportTableColumns: (TableTitleNewProps & { id: string })[] = [
  {
    id: 'checkAll',
    minWidth: '48',
    title: <Checkbox name="tickAll" checked onChange={() => {}} tick="unselect" />,
    padding: 'none',
  },
  {
    id: 'currencyCode',
    minWidth: '260',
    title: 'Currency',
    flexgrow: '1',
    sorting,
  },
  {
    id: 'currencyRate',
    minWidth: '210',
    title: 'Currency rate',
    sorting,
  },
  {
    id: 'status',
    minWidth: '90',
    title: 'Status',
    align: 'center',
    sorting,
  },
];

export const appleCurrencyRatesImportTableData = [
  {
    currencyCode: 'UAH',
    currencyRate: 0.89,
    status: 'active',
  },
  {
    currencyCode: 'EUR',
    currencyRate: 38,
    status: 'active',
  },
  {
    currencyCode: 'BRL',
    currencyRate: 11.58,
    status: 'active',
  },
];
