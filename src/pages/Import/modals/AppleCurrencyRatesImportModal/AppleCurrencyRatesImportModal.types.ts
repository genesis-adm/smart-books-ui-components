import { ReactNode } from 'react';

export type AppleCurrencyRatesImportModalStepId = 'main' | 'preview';

export type AppleCurrencyRatesImportModalStepData = {
  title: string;
  button: {
    text: string;
    tooltip: string;
    action: () => void;
  };
  editTooltip?: string;
  hovered: boolean;
  hoverProps: { onMouseEnter: () => void; onMouseLeave: () => void };
  value: ReactNode;
};
