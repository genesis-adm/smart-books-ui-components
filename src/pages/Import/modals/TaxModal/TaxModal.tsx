import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { InputDate } from 'Components/base/inputs/InputDate';
import { NumericInput } from 'Components/base/inputs/NumericInput';
import { OwnNumberFormatValues } from 'Components/base/inputs/NumericInput/NumericInput.types';
import CountriesSelect from 'Components/custom/Stories/selects/CountriesSelect';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { TaxType } from 'Pages/Import/modals/TaxModal/TaxModal.types';
import { MAX_TAX_RATE, TAX_MODAL_ID } from 'Pages/Import/modals/TaxModal/constants';
import React, { FC, useCallback, useState } from 'react';
import { NumberFormatValues } from 'react-number-format/types/types';

import { Nullable } from '../../../../types/general';

type TaxModalProps = {
  type: TaxType;
  onClose?: () => void;
  containerId?: string;
};

const TaxModal: FC<TaxModalProps> = ({ type, onClose = () => {}, containerId }: TaxModalProps) => {
  const [rateInputValue, setRateInputValue] = useState<number>();
  const [dateValue, setDateValue] = useState<Nullable<Date>>(null);

  const changeRateValue = ({ floatValue }: OwnNumberFormatValues) => {
    setRateInputValue(floatValue);
  };

  const isAllowedRateInput = useCallback(
    ({ floatValue = 0 }: NumberFormatValues): boolean => Math.abs(floatValue) <= MAX_TAX_RATE,
    [],
  );

  const dataByType: Record<TaxType, { title: string }> = {
    vat: {
      title: 'Create new VAT',
    },
    wht: {
      title: 'Create new WHT',
    },
    other: {
      title: 'Create new Other Taxes',
    },
  };

  return (
    <Modal
      modalId={TAX_MODAL_ID}
      size="580"
      height="540"
      containerId={containerId}
      padding="24"
      overlay={containerId ? 'white-100' : 'black-100'}
      flexdirection="column"
      pluginScrollDisabled
      header={
        <ModalHeaderNew
          height="60"
          border
          title={dataByType[type].title}
          titlePosition="center"
          fontType="body-medium"
          onClose={onClose}
        />
      }
      footer={
        <ModalFooterNew border>
          <Button background={'violet-90-violet-100'} color="white-100" width="108" height="40">
            Create
          </Button>
        </ModalFooterNew>
      }
    >
      <Container flexdirection="column" gap="24">
        <CountriesSelect required />

        <Container justifycontent="space-between">
          <InputDate
            name="inputdate"
            placeholder="dd.mm.yyyy"
            label="Effective date"
            dateFormat="dd MMM yyyy"
            width="248"
            selected={dateValue}
            onChange={setDateValue}
            calendarStartDay={1}
            required
          />

          <NumericInput
            name="rate"
            label="Rate"
            width="248"
            height="48"
            suffix="%"
            required
            isAllowed={isAllowedRateInput}
            value={rateInputValue}
            onValueChange={changeRateValue}
          />
        </Container>
      </Container>
    </Modal>
  );
};

export default TaxModal;
