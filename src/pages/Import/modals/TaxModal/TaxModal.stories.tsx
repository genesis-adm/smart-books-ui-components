import { Meta, Story } from '@storybook/react';
import TaxModal from 'Pages/Import/modals/TaxModal/TaxModal';
import React from 'react';

const TaxModalStory: Story = ({ type }) => {
  return <TaxModal type={type} />;
};

export default {
  title: 'Pages/Import page/modals/Tax',
  component: TaxModalStory,
  argTypes: {
    type: {
      options: ['vat', 'wht', 'other'],
      control: {
        type: 'radio',
        labels: {
          vat: 'VAT',
          wht: 'WHT',
          other: 'Other Taxes',
        },
      },
    },
  },
} as Meta;

export const Tax = TaxModalStory.bind({});
Tax.args = {
  type: 'vat',
};
