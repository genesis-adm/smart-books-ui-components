import { Meta, Story } from '@storybook/react';
import ChooseApplePaymentPeriodMonthModal from 'Pages/Import/modals/ChooseApplePaymentPeriodMonthModal/ChooseApplePaymentPeriodMonthModal';
import React from 'react';

const ChooseApplePaymentPeriodMonthComponent: Story = ({ state }) => {
  const isLoading = state === 'loading';

  return <ChooseApplePaymentPeriodMonthModal isLoading={isLoading} />;
};

export default {
  title: 'Pages/Import page/modals/ChooseApplePaymentPeriodMonth',
  component: ChooseApplePaymentPeriodMonthComponent,
  argTypes: {
    state: {
      name: 'State',
      options: ['loading', 'loaded'],
      control: {
        type: 'radio',
        labels: {
          loading: 'Data is loading',
          loaded: 'Data was loaded',
        },
      },
    },
  },
} as Meta;

export const ChooseApplePaymentPeriodMonth = ChooseApplePaymentPeriodMonthComponent.bind({});
ChooseApplePaymentPeriodMonth.args = {
  state: 'loaded',
};
