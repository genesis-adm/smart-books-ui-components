import { Divider } from 'Components/base/Divider';
import { Container } from 'Components/base/grid/Container';
import {
  ApplePaymentPeriod,
  ApplePaymentPeriodMonth,
} from 'Pages/Import/modals/ChooseApplePaymentPeriodMonthModal/ChooseApplePaymentPeriodMonthModal.types';
import ChooseApplePaymentPeriodMonthModalRowYear from 'Pages/Import/modals/ChooseApplePaymentPeriodMonthModal/components/ChooseApplePaymentPeriodMonthModalRowYear';
import ChooseApplePaymentPeriodMonthModalRowYearPeriod from 'Pages/Import/modals/ChooseApplePaymentPeriodMonthModal/components/ChooseApplePaymentPeriodMonthModalRowYearPeriod';
import React, { FC, ReactElement } from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';

type ChooseApplePaymentPeriodMonthModalRowProps = {
  applePaymentPeriod: ApplePaymentPeriod;
  chosenYearPeriodIndex?: number;
  onChange?: (paymentPeriodMonth: ApplePaymentPeriodMonth) => void;
};

const ChooseApplePaymentPeriodMonthModalRow: FC<ChooseApplePaymentPeriodMonthModalRowProps> = ({
  applePaymentPeriod: { id, year, periods },
  chosenYearPeriodIndex,
  onChange,
}: ChooseApplePaymentPeriodMonthModalRowProps): ReactElement => {
  return (
    <Container background={'grey-10'} borderRadius="10" gap="24" className={spacing.p24}>
      <Container justifycontent="center" alignitems="center">
        <ChooseApplePaymentPeriodMonthModalRowYear year={year} />
      </Container>

      <Container gap="4">
        {renderOptions.map((renderOptions, index) => (
          <>
            {!!index && <Divider type="vertical" height="full" />}
            <Container key={renderOptions.join('')} flexdirection="column" gap="4">
              {periods.slice(...renderOptions).map((period) => (
                <ChooseApplePaymentPeriodMonthModalRowYearPeriod
                  key={period.index}
                  paymentPeriodId={id}
                  year={year}
                  period={period}
                  onChange={onChange}
                  checked={chosenYearPeriodIndex === period.index}
                />
              ))}
            </Container>
          </>
        ))}
      </Container>
    </Container>
  );
};

export default ChooseApplePaymentPeriodMonthModalRow;

const renderOptions: [number, number][] = [
  [0, 3],
  [3, 6],
  [6, 9],
  [9, 12],
];
