import { Icon } from 'Components/base/Icon';
import { Radio } from 'Components/base/Radio';
import { Tooltip } from 'Components/base/Tooltip';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import {
  ApplePaymentPeriodMonth,
  ApplePaymentPeriodYearPeriod,
} from 'Pages/Import/modals/ChooseApplePaymentPeriodMonthModal/ChooseApplePaymentPeriodMonthModal.types';
import { ReactComponent as TickInFilledCircle16Icon } from 'Svg/v2/16/tick-in-filled-circle.svg';
import { formatApiDate } from 'Utils/date';
import React, { FC, ReactElement, useState } from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';

type ChooseApplePaymentPeriodMonthModalRowYearPeriod = {
  paymentPeriodId: number;
  year: number;
  period: ApplePaymentPeriodYearPeriod;
  onChange?: (value: ApplePaymentPeriodMonth) => void;
  checked: boolean;
};

const ChooseApplePaymentPeriodMonthModalRowYearPeriod: FC<
  ChooseApplePaymentPeriodMonthModalRowYearPeriod
> = ({
  paymentPeriodId,
  year,
  period,
  onChange,
  checked,
}: ChooseApplePaymentPeriodMonthModalRowYearPeriod): ReactElement => {
  const { name, from, to, index, isUsed } = period;
  const [isHovered, setIsHovered] = useState(false);

  const hover = () => {
    setIsHovered(true);
  };

  const leave = () => {
    setIsHovered(false);
  };

  const changeYearPeriod = () => {
    onChange?.({ paymentPeriodId, index, year, from, to, name });
  };

  const tooltipMessage = isUsed
    ? 'Currency rates have been already imported fot this period'
    : isHovered && !checked
    ? 'Select period'
    : '';

  return (
    <Tooltip message={tooltipMessage}>
      <Container
        onMouseEnter={hover}
        onMouseLeave={leave}
        background={isHovered || checked ? 'grey-20' : 'transparent'}
        borderRadius="10"
        className={spacing.p8}
      >
        {isUsed ? (
          <Container alignitems="center" gap="8">
            <Icon path="grey-90" icon={<TickInFilledCircle16Icon transform="scale(1.25)" />} />
            <Container flexdirection="column" gap="4">
              <Text color={isUsed ? 'grey-90' : 'black-100'}>October 2023</Text>
              <Text color={isUsed ? 'grey-90' : 'grey-100'}>05 Nov 2024 - 02 Dec 2024</Text>
            </Container>
          </Container>
        ) : (
          <Radio
            id={String(year) + period.index}
            name="paymentPeriod"
            onChange={changeYearPeriod}
            checked={checked}
            color={checked ? 'black-100' : 'grey-100'}
            type="body-medium"
            hovered={isHovered}
          >
            <Container flexdirection="column" gap="4">
              <Text color={isUsed ? 'grey-90' : 'black-100'}>{name}</Text>
              <Text color={isUsed ? 'grey-90' : 'grey-100'}>{`${formatApiDate(
                from,
              )} - ${formatApiDate(to)}`}</Text>
            </Container>
          </Radio>
        )}
      </Container>
    </Tooltip>
  );
};

export default ChooseApplePaymentPeriodMonthModalRowYearPeriod;
