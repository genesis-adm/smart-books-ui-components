import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React, { FC, ReactElement } from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';

type ChooseApplePaymentPeriodMonthModalRowYearProps = {
  year: number;
};

const ChooseApplePaymentPeriodMonthModalRowYear: FC<
  ChooseApplePaymentPeriodMonthModalRowYearProps
> = ({ year }: ChooseApplePaymentPeriodMonthModalRowYearProps): ReactElement => {
  return (
    <Container
      flexdirection="column"
      border="violet-20"
      borderRadius="8"
      className={classNames(spacing.h62fixed, spacing.w68fixed)}
      overflow="hidden"
      background={'transparent'}
    >
      <Container
        className={spacing.h14fixed}
        alignitems="center"
        justifycontent="center"
        background={'violet-90'}
        style={{ flexShrink: '0' }}
      >
        <Text type="filename" color="white-100" style={{ fontSize: '8px' }}>
          *
        </Text>
        <Text type="filename" color="white-100" style={{ fontSize: '8px' }}>
          Fiscal year
        </Text>
      </Container>
      <Container flexgrow="1" justifycontent="center" alignitems="center" gap="8">
        <Text type="body-medium" color="black-100">
          {year}
        </Text>
      </Container>
    </Container>
  );
};

export default ChooseApplePaymentPeriodMonthModalRowYear;
