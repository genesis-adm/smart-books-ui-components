export type ApplePaymentPeriod = {
  id: number;
  year: number;
  periods: ApplePaymentPeriodYearPeriod[];
};

export type ApplePaymentPeriodYearPeriod = {
  index: number;
  name: string;
  from: string;
  to: string;
  isUsed?: boolean;
};

export type ApplePaymentPeriodMonth = {
  index: number;
  name: string;
  from: string;
  to: string;
  year: number;
  paymentPeriodId: number;
};
