import { ContentLoader } from 'Components/base/ContentLoader';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import ModalHeaderSkeleton from 'Components/loaders/ModalHeaderSkeleton/ModalHeaderSkeleton';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { ApplePaymentPeriodMonth } from 'Pages/Import/modals/ChooseApplePaymentPeriodMonthModal/ChooseApplePaymentPeriodMonthModal.types';
import ChooseApplePaymentPeriodMonthModalRow from 'Pages/Import/modals/ChooseApplePaymentPeriodMonthModal/components/ChooseApplePaymentPeriodMonthModalRow';
import { applePaymentPeriods } from 'Pages/Import/modals/ChooseApplePaymentPeriodMonthModal/constants';
import classNames from 'classnames';
import React, { FC, ReactElement, useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';
import { Nullable } from '../../../../types/general';

type ChooseApplePaymentPeriodMonthModalProps = {
  applePaymentPeriodMonth?: ApplePaymentPeriodMonth;
  isLoading?: boolean;
  containerId?: string;
  onClose?: () => void;
  onSubmit?: (value: ApplePaymentPeriodMonth) => void;
};

const ChooseApplePaymentPeriodMonthModal: FC<ChooseApplePaymentPeriodMonthModalProps> = ({
  applePaymentPeriodMonth,
  isLoading = false,
  onClose,
  onSubmit,
  containerId,
}: ChooseApplePaymentPeriodMonthModalProps): ReactElement => {
  const [value, setValue] = useState<Nullable<ApplePaymentPeriodMonth>>(
    applePaymentPeriodMonth || null,
  );

  const resetValue = () => setValue(null);

  const closeModal = (): void => {
    if (!onClose) return;

    onClose();
  };

  const submit = () => {
    if (!onSubmit) return resetValue();

    if (value) {
      onSubmit(value);
      onClose?.();
    }
  };

  return (
    <Modal
      size="1040"
      height="690"
      containerId={containerId}
      flexdirection="column"
      padding="none"
      classNameModalBody={spacing.h100p}
      overlay={containerId ? 'white-100' : 'black-100'}
      pluginScrollDisabled
      header={
        <ModalHeaderSkeleton titleWidthPx={249} isLoading={isLoading} border>
          <ModalHeaderNew
            height="60"
            fontType="body-regular-16"
            title="Please choose a Payment period"
            titlePosition="center"
            border
            onClose={closeModal}
          />
        </ModalHeaderSkeleton>
      }
      footer={
        <ModalFooterNew border>
          <ContentLoader width="100px" isLoading={isLoading}>
            <Button
              background={'violet-90-violet-100'}
              color="white-100"
              width="100"
              height="40"
              disabled={!value}
              onClick={submit}
            >
              Save
            </Button>
          </ContentLoader>
        </ModalFooterNew>
      }
    >
      <Container
        flexdirection="column"
        gap="12"
        overflow="y-auto"
        customScroll
        className={classNames(spacing.pl24, spacing.pb12, spacing.mt24, spacing.mr16)}
      >
        {isLoading
          ? Array.from(Array(3)).map((_, index) => (
              <ContentLoader
                key={index}
                height="180px"
                isLoading={isLoading}
                style={{ flexShrink: 0 }}
              />
            ))
          : applePaymentPeriods.map((period) => (
              <ChooseApplePaymentPeriodMonthModalRow
                key={period.id}
                applePaymentPeriod={period}
                onChange={setValue}
                chosenYearPeriodIndex={
                  period.id === value?.paymentPeriodId ? value.index : undefined
                }
              />
            ))}
      </Container>
    </Modal>
  );
};

export default ChooseApplePaymentPeriodMonthModal;
