import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { Modal } from 'Components/modules/Modal';
import AppleCurrencyRatesModalHeader from 'Pages/Import/modals/AppleCurrencyRatesModal/components/AppleCurrencyRatesModalHeader';
import AppleCurrencyRatesModalTable from 'Pages/Import/modals/AppleCurrencyRatesModal/components/AppleCurrencyRatesModalTable';
import React, { FC, ReactElement, ReactNode } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

type AppleCurrencyRatesModalProps = {
  isLoading?: boolean;
  onClose?: () => void;

  containerId?: string;
};

const AppleCurrencyRatesModal: FC<AppleCurrencyRatesModalProps> = ({
  isLoading,
  onClose = () => {},
  containerId,
}: AppleCurrencyRatesModalProps): ReactElement => {
  const period = {
    year: 2023,
    from: '05 Nov 2024',
    to: '02 Dec 2024',
  };

  const ccyCode = 'USD';

  const fields: { label: string; value: ReactNode }[] = [
    { label: 'Cabinet Name:', value: <Text type="button">{'BetterMe Limited'}</Text> },
    {
      label: 'Payment period:',
      value: (
        <Container gap="4" alignitems="flex-end">
          <Text type="button">{period?.year}:</Text>
          <Text type="caption-regular" color="grey-100" style={{ lineHeight: 'initial' }}>
            {`${period?.from} - ${period?.to}`}
          </Text>
        </Container>
      ),
    },
    {
      label: 'Payment date:',
      value: <Text type="button">10 Jul 2025</Text>,
    },
  ];
  return (
    <Modal
      size="820"
      type="old"
      containerId={containerId}
      padding="24"
      pluginScrollDisabled
      flexdirection="row"
      classNameModalBody={spacing.h100p}
      overlay={containerId ? 'white-100' : 'black-100'}
      header={<AppleCurrencyRatesModalHeader onClose={onClose} />}
    >
      <Container
        flexdirection="column"
        justifycontent="space-between"
        className={spacing.h100p}
        style={{ flexBasis: '40%' }}
      >
        <Container flexdirection="column" gap="20">
          {fields?.map(({ label, value }) => (
            <Container key={label} flexdirection="column" gap="8">
              <Text type="caption-regular" color="grey-100">
                {label}
              </Text>
              {value}
            </Container>
          ))}
        </Container>

        <Container gap="4">
          <Text type="caption-regular" color="grey-100">
            Cabinet Currency:
          </Text>
          <Text type="caption-regular">{ccyCode}</Text>
        </Container>
      </Container>

      <Container flexdirection="column" style={{ flexBasis: '60%' }}>
        <AppleCurrencyRatesModalTable />
      </Container>
    </Modal>
  );
};

export default AppleCurrencyRatesModal;
