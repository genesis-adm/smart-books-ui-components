import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import React, { FC } from 'react';

type AppleCurrencyRatesModalHeaderProps = {
  onClose: () => void;
};

const AppleCurrencyRatesModalHeader: FC<AppleCurrencyRatesModalHeaderProps> = ({
  onClose,
}: AppleCurrencyRatesModalHeaderProps) => {
  return (
    <ModalHeaderNew title="View mode" titlePosition="center" height="80" onClose={onClose} border />
  );
};

export default AppleCurrencyRatesModalHeader;
