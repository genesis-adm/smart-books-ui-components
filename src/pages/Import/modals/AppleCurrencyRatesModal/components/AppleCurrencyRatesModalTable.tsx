import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableTitleNew } from 'Components/elements/Table/TableTitleNew';
import AppleCurrencyRatesModalTableRow from 'Pages/Import/modals/AppleCurrencyRatesModal/components/AppleCurrencyRatesModalTableRow';
import {
  currencyRatesModalTableColumns,
  currencyRatesModalTableData,
} from 'Pages/Import/modals/AppleCurrencyRatesModal/constants';
import React, { FC, ReactElement } from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';

const AppleCurrencyRatesModalTable: FC = (): ReactElement => {
  return (
    <TableNew>
      <TableHeadNew radius="10" background={'grey-10'} className={spacing.pY6}>
        {currencyRatesModalTableColumns.map(({ id, ...props }) => (
          <TableTitleNew key={id} background={'grey-10'} radius="10" {...props} />
        ))}
      </TableHeadNew>
      <TableBodyNew>
        {currencyRatesModalTableData.map(({ ccyCode, ccyRate }, index) => (
          <AppleCurrencyRatesModalTableRow key={index} ccyCode={ccyCode} ccyRate={ccyRate} />
        ))}
      </TableBodyNew>
    </TableNew>
  );
};

export default AppleCurrencyRatesModalTable;
