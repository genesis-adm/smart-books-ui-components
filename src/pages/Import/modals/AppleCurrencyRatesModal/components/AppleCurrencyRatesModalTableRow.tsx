import { Tag } from 'Components/base/Tag';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { currencyRatesModalTableColumns } from 'Pages/Import/modals/AppleCurrencyRatesModal/constants';
import React, { FC, ReactElement } from 'react';

type AppleCurrencyRatesModalTableRowProps = {
  ccyCode: string;
  ccyRate: string;
};

const AppleCurrencyRatesModalTableRow: FC<AppleCurrencyRatesModalTableRowProps> = ({
  ccyCode,
  ccyRate,
}: AppleCurrencyRatesModalTableRowProps): ReactElement => {
  return (
    <TableRowNew background={'transparent-with-border'} size="40">
      <TableCellTextNew minWidth={currencyRatesModalTableColumns?.[0]?.minWidth} value={ccyCode} />
      <TableCellNew
        minWidth={currencyRatesModalTableColumns?.[1]?.minWidth}
        flexgrow={currencyRatesModalTableColumns?.[1]?.flexgrow}
      >
        <Tag padding="2-8" text={ccyRate} color="grey-10" />
      </TableCellNew>
    </TableRowNew>
  );
};

export default AppleCurrencyRatesModalTableRow;
