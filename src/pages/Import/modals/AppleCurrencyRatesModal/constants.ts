import { TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';

export const currencyRatesModalTableColumns: (TableTitleNewProps & { id: string })[] = [
  {
    id: 'currencyCode',
    minWidth: '220',
    title: 'Currency',
  },
  {
    id: 'currencyRate',
    minWidth: '194',
    title: 'Currency rate',
    flexgrow: '1',
  },
];

export const currencyRatesModalTableData = [
  { ccyCode: 'USD', ccyRate: '1 EUR  = 2.034324 USD' },
  { ccyCode: 'USD', ccyRate: '1 EUR  = 2.034324 USD' },
  { ccyCode: 'USD', ccyRate: '1 EUR  = 2.034324 USD' },
  { ccyCode: 'USD', ccyRate: '1 EUR  = 2.034324 USD' },
  { ccyCode: 'USD', ccyRate: '1 EUR  = 2.034324 USD' },
  { ccyCode: 'USD', ccyRate: '1 EUR  = 2.034324 USD' },
  { ccyCode: 'USD', ccyRate: '1 EUR  = 2.034324 USD' },
  { ccyCode: 'USD', ccyRate: '1 EUR  = 2.034324 USD' },
  { ccyCode: 'USD', ccyRate: '1 EUR  = 2.034324 USD' },
  { ccyCode: 'USD', ccyRate: '1 EUR  = 2.034324 USD' },
  { ccyCode: 'USD', ccyRate: '1 EUR  = 2.034324 USD' },
  { ccyCode: 'USD', ccyRate: '1 EUR  = 2.034324 USD' },
  { ccyCode: 'USD', ccyRate: '1 EUR  = 2.034324 USD' },
];
