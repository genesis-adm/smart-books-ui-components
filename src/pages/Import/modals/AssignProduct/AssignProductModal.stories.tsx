import { Meta, Story } from '@storybook/react';
import AssignProductModal from 'Pages/Import/modals/AssignProduct/AssignProductModal';
import React from 'react';

const AssignProductModalStory: Story = ({ storyState }) => {
  const isLoading = storyState === 'loading';

  return <AssignProductModal isLoading={isLoading} />;
};

export default {
  title: 'Pages/Import page/modals/ Assign Product',
  component: AssignProductModalStory,
  argTypes: {
    storyState: {
      options: ['empty', 'loading', 'loaded'],
      control: {
        type: 'radio',
        labels: {
          empty: 'Empty page',
          loading: 'Page is loading',
          loaded: 'Page loaded',
        },
      },
    },
  },
} as Meta;

export const AssignProduct = AssignProductModalStory.bind({});
AssignProduct.args = {
  storyState: 'loaded',
};
