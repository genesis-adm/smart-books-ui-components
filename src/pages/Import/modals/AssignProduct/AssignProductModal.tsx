import TablePlaceholder from 'Components/custom/placeholders/TablePlaceholder';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { SelectOption } from 'Components/inputs/Select/Select.types';
import { Modal } from 'Components/modules/Modal';
import type { TableCellWidth } from 'Components/types/gridTypes';
import AssignProductModalFooter from 'Pages/Import/modals/AssignProduct/components/AssignProductModalFooter';
import AssignProductModalHeader from 'Pages/Import/modals/AssignProduct/components/AssignProductModalHeader';
import AssignProductTableRow from 'Pages/Import/modals/AssignProduct/components/AssignProductTableRow';
import {
  TableRowData,
  assignProductsTableHeader,
  tableRowData,
} from 'Pages/Import/modals/AssignProduct/constants';
import React, { useState } from 'react';
import { ReactElement } from 'react';

import { Nullable } from '../../../../types/general';

type AssignProductModalProps = {
  isLoading: boolean;
  handleClose?(): void;
};

const AssignProductModal = ({ isLoading, handleClose }: AssignProductModalProps): ReactElement => {
  const [tableRowList, setTableRowList] = useState<TableRowData[]>(tableRowData);

  const handleChangeProduct = (product: { value: Nullable<SelectOption>; id: string }) => {
    const newData = tableRowList.map((item) => {
      return item.id === product.id ? { ...item, product: product.value } : item;
    });

    setTableRowList(newData);
  };

  const disableSave = tableRowList.every((item) => !item.product?.value);

  return (
    <Modal
      size="820"
      header={<AssignProductModalHeader isLoading={isLoading} handleClose={handleClose} />}
      footer={<AssignProductModalFooter isLoading={isLoading} disableSave={disableSave} />}
    >
      {isLoading ? (
        <TablePlaceholder paddingX="none" paddingY="none" />
      ) : (
        <TableNew
          noResults={false}
          tableHead={
            <TableHeadNew>
              {assignProductsTableHeader.map((item, index) => (
                <TableTitleNew
                  key={index}
                  minWidth={item.minWidth as TableCellWidth}
                  align={item.align as TableTitleNewProps['align']}
                  title={item.title}
                  onClick={() => {}}
                />
              ))}
            </TableHeadNew>
          }
        >
          <TableBodyNew>
            {tableRowList.map((item) => (
              <AssignProductTableRow item={item} handleChangeProduct={handleChangeProduct} />
            ))}
          </TableBodyNew>
        </TableNew>
      )}
    </Modal>
  );
};

export default AssignProductModal;
