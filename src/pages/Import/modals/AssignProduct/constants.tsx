import { TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { SelectOption } from 'Components/inputs/Select/Select.types';
import { TableCellWidth } from 'Components/types/gridTypes';

import { Nullable } from '../../../../types/general';

export const assignProductsTableHeader: {
  minWidth: TableCellWidth;
  title: string;
  align?: TableTitleNewProps['align'];
}[] = [
  { minWidth: '48', title: '#' },
  { minWidth: '280', title: 'SKU' },
  { minWidth: '340', title: 'Assign Product' },
  { minWidth: '93', title: 'Status', align: 'center' },
];

export type TableRowData = { id: string; sku: string; product: Nullable<SelectOption> };

export const tableRowData: TableRowData[] = [
  { id: '1', sku: 'com.liti.android', product: { value: '', label: '' } },
  { id: '2', sku: 'com.liti.android', product: { value: '', label: '' } },
  { id: '3', sku: 'com.liti.android', product: { value: '', label: '' } },
];
