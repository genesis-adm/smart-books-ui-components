import { ContentLoader } from 'Components/base/ContentLoader';
import { Icon } from 'Components/base/Icon';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ReactComponent as AlertIcon } from 'Svg/v2/16/alert.svg';
import React, { ReactElement } from 'react';

type AssignProductModalFooterProps = {
  isLoading: boolean;
  disableSave: boolean;
};

const AssignProductModalFooter = ({
  isLoading,
  disableSave,
}: AssignProductModalFooterProps): ReactElement => (
  <ModalFooterNew border direction="normal">
    <ContentLoader isLoading={isLoading} height="24px" width="363px">
      <Container gap="4">
        <Icon icon={<AlertIcon />} path="grey-100" />
        <Text color="grey-100" type="caption-regular">
          New SKUs found. Assign products to import transactions
        </Text>
      </Container>
    </ContentLoader>
    <ContentLoader isLoading={isLoading} height="40px" width="97px">
      <Button width="md" height="40" disabled={disableSave} onClick={() => {}}>
        Save
      </Button>
    </ContentLoader>
  </ModalFooterNew>
);

export default AssignProductModalFooter;
