import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { IconButton } from 'Components/base/buttons/IconButton';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { Search } from 'Components/inputs/Search';
import { Select } from 'Components/inputs/Select';
import { SelectOption } from 'Components/inputs/Select/Select.types';
import { OptionWithFourLabels } from 'Components/inputs/Select/options/OptionWithFourLabels';
import { productOptions } from 'Mocks/fakeOptions';
import {
  TableRowData,
  assignProductsTableHeader,
} from 'Pages/Import/modals/AssignProduct/constants';
import { ReactComponent as CheckIcon } from 'Svg/v2/16/tick-in-filled-circle.svg';
import { ReactComponent as WarningIcon } from 'Svg/v2/16/warning.svg';
import classNames from 'classnames';
import React, { ReactElement, useState } from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';
import { Nullable } from '../../../../../types/general';

type AssignProductTableRowProps = {
  item: TableRowData;
  handleChangeProduct(product: { value: Nullable<SelectOption>; id: string }): void;
};

const AssignProductTableRow = ({
  item,
  handleChangeProduct,
}: AssignProductTableRowProps): ReactElement => {
  const [searchValue, setSearchValue] = useState<string>('');

  return (
    <TableRowNew key={item.id} gap="8">
      <TableCellTextNew
        minWidth={assignProductsTableHeader[0].minWidth}
        value={item.id}
        fixedWidth
      />
      <TableCellTextNew
        minWidth={assignProductsTableHeader[1].minWidth}
        value={item.sku}
        fixedWidth
      />
      <TableCellNew minWidth={assignProductsTableHeader[2].minWidth} fixedWidth>
        <Select
          label="Choose"
          placeholder="Choose"
          width="311"
          height="36"
          type="column"
          onChange={(value) => handleChangeProduct({ value, id: item.id })}
          value={item.product}
        >
          {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
            <SelectDropdown selectElement={selectElement} width="338">
              <SelectDropdownBody>
                <Search
                  className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                  width="full"
                  height="40"
                  name="search"
                  placeholder="Search Product"
                  value={searchValue}
                  onChange={(e) => setSearchValue(e.target.value)}
                  searchParams={[]}
                  onSearch={() => {}}
                  onClear={() => {}}
                />
                <SelectDropdownItemList>
                  {productOptions.map((value, indexOption) => (
                    <OptionWithFourLabels
                      id={value.id}
                      key={indexOption}
                      label={value?.name}
                      secondaryLabel={value.type}
                      tertiaryLabel={value.businessUnit}
                      selected={!Array.isArray(selectValue) && selectValue?.value === value.id}
                      onClick={() => {
                        onOptionClick({
                          value: value?.id,
                          label: value?.name,
                          subLabel: value?.businessUnit,
                        });
                        closeDropdown();
                        handleChangeProduct({
                          value: {
                            value: value?.id,
                            label: value?.name,
                            subLabel: value?.businessUnit,
                          },
                          id: item.id,
                        });
                      }}
                    />
                  ))}
                </SelectDropdownItemList>
              </SelectDropdownBody>
            </SelectDropdown>
          )}
        </Select>
      </TableCellNew>
      <TableCellNew
        minWidth={assignProductsTableHeader[3].minWidth}
        alignitems="center"
        justifycontent="center"
        fixedWidth
      >
        {item.product?.value ? (
          <IconButton
            icon={<CheckIcon />}
            size="24"
            background={'green-10-green-20'}
            color="green-100"
            tooltip="Assigned"
          />
        ) : (
          <IconButton
            icon={<WarningIcon />}
            size="24"
            background={'yellow-10-yellow-20'}
            color="yellow-100"
            tooltip="Not assigned"
          />
        )}
      </TableCellNew>
    </TableRowNew>
  );
};

export default AssignProductTableRow;
