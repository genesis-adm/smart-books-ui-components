import { ContentLoader } from 'Components/base/ContentLoader';
import { Divider } from 'Components/base/Divider';
import { Tag } from 'Components/base/Tag';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as CrossIcon } from 'Svg/v2/16/cross.svg';
import { ReactComponent as GooglePlayLogoIcon } from 'Svg/v2/24/google-play-logo-colored.svg';
import React, { ReactElement } from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';

type AssignProductModalHeaderProps = {
  isLoading: boolean;
  handleClose?(): void;
};

const AssignProductModalHeader = ({
  isLoading,
  handleClose,
}: AssignProductModalHeaderProps): ReactElement => {
  return (
    <Container background={'grey-10'} justifycontent="space-between" className={spacing.p24}>
      <Container gap="24" alignitems="center" justifycontent={'space-between'}>
        <Container flexdirection="column" gap="8">
          <ContentLoader isLoading={isLoading} height="14px" width="134px">
            <Text type="button">Assign Product</Text>
          </ContentLoader>
          <ContentLoader isLoading={isLoading} height="24px" width="134px">
            <Tag
              text="Google Play"
              icon={<GooglePlayLogoIcon transform="scale(0.7)" />}
              staticIconColor
            />
          </ContentLoader>
        </Container>
        <Divider type="vertical" height="26" />
        <ContentLoader isLoading={isLoading} height="24px" width="134px">
          <Container flexdirection="column" gap="8">
            <Text type="caption-regular" color="grey-100">
              Cabinet name:
            </Text>
            <Text type="caption-regular">BetterMe International</Text>
          </Container>
        </ContentLoader>
        <Divider height="26" type="vertical" />
        <ContentLoader isLoading={isLoading} height="24px" width="134px">
          <Container flexdirection="column" gap="8">
            <Text type="caption-regular" color="grey-100">
              Legal entity:
            </Text>
            <Text type="caption-regular">BetterMe Limited International</Text>
          </Container>
        </ContentLoader>
        <Divider height="26" type="vertical" />
        <ContentLoader isLoading={isLoading} height="24px" width="134px">
          <Container flexdirection="column" gap="8">
            <Text type="caption-regular" color="grey-100">
              Customer:
            </Text>
            <Text type="caption-regular">Google Irelend limited</Text>
          </Container>
        </ContentLoader>
      </Container>
      <ContentLoader isLoading={isLoading} height="24px" width="24px">
        <IconButton
          icon={<CrossIcon />}
          size="24"
          background={'transparent-grey-20'}
          tooltip="Close"
          onClick={handleClose}
        />
      </ContentLoader>
    </Container>
  );
};

export default AssignProductModalHeader;
