import { DropDown } from 'Components/DropDown';
import { ContentLoader } from 'Components/base/ContentLoader';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as AppleLogoIcon } from 'Svg/v2/16/apple_logo_black.svg';
import { ReactComponent as CrossIcon } from 'Svg/v2/16/cross.svg';
import { ReactComponent as SettingsIcon } from 'Svg/v2/16/gear.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import classNames from 'classnames';
import React, { ReactElement } from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

type ImportPageHeaderProps = {
  isLoading: boolean;
  onOpenAppleSettingsModal?: () => void;
};

const ImportPageHeader = ({
  isLoading,
  onOpenAppleSettingsModal,
}: ImportPageHeaderProps): ReactElement => {
  return (
    <Container
      justifycontent="space-between"
      alignitems="center"
      className={classNames(spacing.pY16)}
    >
      <Container justifycontent="center" flexgrow="2">
        <ContentLoader isLoading={isLoading} height="28px" width="251px">
          <Text type="title-semibold" align="center">
            Import page
          </Text>
        </ContentLoader>
      </Container>
      <Container alignitems="center" gap="16">
        <ContentLoader isLoading={isLoading} height="24px" width="24px">
          <IconButton
            tooltip="Create new integration"
            icon={<PlusIcon />}
            size="24"
            color="white-100"
            background={'violet-90-violet-100'}
          />
        </ContentLoader>

        <ContentLoader isLoading={isLoading} height="24px" width="24px">
          <DropDown
            padding="8"
            control={({ handleOpen }) => (
              <IconButton
                tooltip="Settings"
                icon={<SettingsIcon />}
                size="24"
                color="grey-100"
                background={'transparent-grey-20'}
                onClick={handleOpen}
              />
            )}
          >
            <DropDownButton size="224" icon={<AppleLogoIcon />} onClick={onOpenAppleSettingsModal}>
              Apple settings
            </DropDownButton>
          </DropDown>
        </ContentLoader>

        <ContentLoader isLoading={isLoading} height="24px" width="24px">
          <IconButton
            tooltip="Close"
            icon={<CrossIcon />}
            size="24"
            color="grey-100"
            background={'transparent-grey-20'}
          />
        </ContentLoader>
      </Container>
    </Container>
  );
};

export default ImportPageHeader;
