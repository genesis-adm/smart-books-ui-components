import { DropDown } from 'Components/DropDown';
import { Divider } from 'Components/base/Divider';
import { Tag } from 'Components/base/Tag';
import { Button } from 'Components/base/buttons/Button';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Text } from 'Components/base/typography/Text';
import TablePlaceholder from 'Components/custom/placeholders/TablePlaceholder';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { UserInfo } from 'Components/elements/UserInfo';
import type { TableCellWidth } from 'Components/types/gridTypes';
import ChooseDateModal, { ModalType } from 'Pages/Import/components/ChooseDateModal';
import { tableHead } from 'Pages/Import/constants';
import AssignProductModal from 'Pages/Import/modals/AssignProduct/AssignProductModal';
import { ReactComponent as BellIcon } from 'Svg/8/bell.svg';
import { ReactComponent as EyeIcon } from 'Svg/v2/16/eye.svg';
import { ReactComponent as DropdownIcon } from 'Svg/v2/16/three-dots.svg';
import { ReactComponent as CheckIcon } from 'Svg/v2/16/tick-in-filled-circle.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import { ReactComponent as WarningIcon } from 'Svg/v2/16/warning.svg';
import { ReactComponent as GooglePlayLogoIcon } from 'Svg/v2/24/google-play-logo-colored.svg';
import classNames from 'classnames';
import React, { MouseEvent, ReactElement, useState } from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

type ImportPageTableProps = {
  isLoading: boolean;
};

const ImportPageTable = ({ isLoading }: ImportPageTableProps): ReactElement => {
  const [isOpenedChooseDateModal, setIsOpenedChooseDateModal] = useState<boolean>(false);

  const [modalType, setModalType] = useState<ModalType>('activate');

  const handleOpenChooseDateModal = (type: ModalType) => {
    setModalType(type);
    setIsOpenedChooseDateModal(true);
  };

  const handleClose = () => {
    setIsOpenedChooseDateModal(false);
  };

  const [isOpenedAssignProductModal, setIsOpenedAssignProductModal] = useState<boolean>(false);

  const handleOpenAssignProductModal = () => setIsOpenedAssignProductModal(true);
  const handleCloseAssignProductModal = () => setIsOpenedAssignProductModal(false);

  if (isLoading) return <TablePlaceholder />;

  return (
    <>
      {isOpenedChooseDateModal && <ChooseDateModal type={modalType} handleClose={handleClose} />}
      {isOpenedAssignProductModal && (
        <AssignProductModal handleClose={handleCloseAssignProductModal} isLoading={false} />
      )}
      <TableNew
        noResults={false}
        className={classNames(spacing.mt24, spacing.ml24, spacing.pr24)}
        tableHead={
          <TableHeadNew>
            {tableHead.map((item, index) => (
              <TableTitleNew
                key={index}
                minWidth={item.minWidth as TableCellWidth}
                align={item.align as TableTitleNewProps['align']}
                title={item.title}
                sorting={item.sorting}
                onClick={() => {}}
              />
            ))}
          </TableHeadNew>
        }
      >
        <TableBodyNew>
          {Array.from(Array(39)).map((_, index) => (
            <TableRowNew key={index} gap="8">
              <TableCellNew minWidth={tableHead[0].minWidth} fixedWidth>
                <Tag
                  text="Google Play"
                  staticIconColor
                  icon={<GooglePlayLogoIcon transform="scale(0.7)" />}
                />
              </TableCellNew>
              <TableCellTextNew
                minWidth={tableHead[1].minWidth}
                value="BetterMe Limited Apple"
                fixedWidth
              />
              <TableCellTextNew
                minWidth={tableHead[2].minWidth}
                value="BetterMe Limited"
                fixedWidth
              />
              <TableCellTextNew minWidth={tableHead[3].minWidth} value="Customer name" fixedWidth />
              <TableCellNew minWidth={tableHead[4].minWidth} fixedWidth>
                <UserInfo name="VitaliyKvasha" isLogo secondaryText="VitalyKV@gmail.com" />
              </TableCellNew>
              <TableCellNew minWidth={tableHead[5].minWidth} fixedWidth justifycontent="center">
                {index % 2 === 1 ? (
                  <Button
                    font="caption-regular"
                    background={'red-10-red-20'}
                    height="24"
                    onClick={handleOpenAssignProductModal}
                  >
                    <Text color="red-90" type="caption-regular">
                      {'Add Product: '}
                    </Text>
                    <Text color="red-90" type="caption-bold">
                      20
                    </Text>
                  </Button>
                ) : (
                  <Tag text="None" />
                )}
              </TableCellNew>
              <TableCellTextNew minWidth={tableHead[6].minWidth} value="08 July 2021" fixedWidth />
              <TableCellNew minWidth={tableHead[7].minWidth} justifycontent="center" fixedWidth>
                {index % 2 === 1 ? (
                  <IconButton
                    size="24"
                    background={'green-10-green-20'}
                    color="green-90-green-100"
                    icon={<CheckIcon />}
                    tooltip="Activated"
                  />
                ) : (
                  <IconButton
                    size="24"
                    background={'yellow-10-yellow-20'}
                    color="yellow-90-yellow-100"
                    icon={<WarningIcon />}
                    additionalIcon={<BellIcon />}
                    tooltip="Disabled: cabinet will be activated starting from 30 jun 2024"
                  />
                )}
              </TableCellNew>
              <TableCellNew minWidth={tableHead[8].minWidth} justifycontent="center" fixedWidth>
                <DropDown
                  padding="8"
                  flexdirection="column"
                  control={({ handleOpen }) => (
                    <IconButton
                      icon={<DropdownIcon />}
                      shape="square"
                      size="24"
                      onClick={(e: MouseEvent<HTMLDivElement>) => {
                        e.stopPropagation();
                        handleOpen();
                      }}
                      background={'transparent-grey-20'}
                      color="grey-100-violet-90"
                      tooltip="Show Actions"
                    />
                  )}
                >
                  <DropDownButton size="224" icon={<EyeIcon />}>
                    Open
                  </DropDownButton>
                  {index % 2 === 1 ? (
                    <DropDownButton
                      size="224"
                      icon={<WarningIcon />}
                      onClick={() => handleOpenChooseDateModal('deactivate')}
                    >
                      Disable
                    </DropDownButton>
                  ) : (
                    <DropDownButton
                      size="224"
                      icon={<CheckIcon />}
                      onClick={() => handleOpenChooseDateModal('activate')}
                    >
                      Activate
                    </DropDownButton>
                  )}
                  <Divider fullHorizontalWidth className={spacing.mY4} />
                  <DropDownButton size="224" type="danger" icon={<TrashIcon />}>
                    Delete
                  </DropDownButton>
                </DropDown>
              </TableCellNew>
            </TableRowNew>
          ))}
        </TableBodyNew>
      </TableNew>
    </>
  );
};

export default ImportPageTable;
