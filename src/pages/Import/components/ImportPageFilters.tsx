import { ContentLoader } from 'Components/base/ContentLoader';
import { Divider } from 'Components/base/Divider';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { FilterDropDown } from 'Components/elements/filters/FilterDropDown';
import { FilterDropDownItem } from 'Components/elements/filters/FilterDropDownItem';
import { FilterItemsContainer } from 'Components/elements/filters/FilterItemsContainer';
import { FilterSearch } from 'Components/elements/filters/FilterSearch';
import { FiltersWrapper } from 'Components/elements/filters/FiltersWrapper';
import { options } from 'Mocks/fakeOptions';
import {
  cabinetNameFilterOptions,
  createdByFilterOptions,
  integratorFilterOptions,
  nonClassifiedSKUFilterOptions,
  statusFilterOptions,
} from 'Pages/Import/constants';
import classNames from 'classnames';
import React, { ReactElement, ReactNode, useState } from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

type Filters =
  | 'integrator'
  | 'cabinetName'
  | 'legalEntity'
  | 'customer'
  | 'createdBy'
  | 'nonClassifiedSKU'
  | 'status';

type ImportPageFiltersProps = {
  isLoading: boolean;
};

const ImportPageFilters = ({ isLoading }: ImportPageFiltersProps): ReactElement => {
  const [filterSearchValue, setFilterSearchValue] = useState<string>();

  const handleChange = (value: string) => {
    setFilterSearchValue(value);
  };

  const filtersValue: Record<Filters, { title: string; component: ReactNode }> = {
    integrator: {
      title: 'Integrator',
      component: (
        <FilterItemsContainer className={spacing.mt8}>
          {integratorFilterOptions.map((item) => (
            <FilterDropDownItem
              key={item.value}
              id={item.value}
              type="item"
              checked={false}
              blurred={false}
              value={item.title}
              onChange={() => {}}
            />
          ))}
        </FilterItemsContainer>
      ),
    },
    cabinetName: {
      title: 'Cabinet name',
      component: (
        <FilterItemsContainer className={spacing.mt8}>
          {cabinetNameFilterOptions.map((item) => (
            <FilterDropDownItem
              key={item.value}
              id={item.value}
              type="item"
              checked={false}
              blurred={false}
              value={item.title}
              onChange={() => {}}
            />
          ))}
        </FilterItemsContainer>
      ),
    },
    legalEntity: {
      title: 'Legal Entity',
      component: (
        <FilterItemsContainer className={spacing.mt8}>
          {options.map((item) => (
            <FilterDropDownItem
              key={item.value}
              id={item.value}
              type="item"
              checked={false}
              blurred={false}
              value={item.label}
              onChange={() => {}}
            />
          ))}
        </FilterItemsContainer>
      ),
    },
    customer: {
      title: 'Customer',
      component: (
        <FilterItemsContainer className={spacing.mt8}>
          {options.map((item) => (
            <FilterDropDownItem
              key={item.value}
              id={item.value}
              type="item"
              checked={false}
              blurred={false}
              value={item.label}
              onChange={() => {}}
            />
          ))}
        </FilterItemsContainer>
      ),
    },
    createdBy: {
      title: 'Created by',
      component: (
        <FilterItemsContainer className={spacing.mt8}>
          {createdByFilterOptions.map((item) => (
            <FilterDropDownItem
              key={item.value}
              id={item.value}
              type="item"
              checked={false}
              blurred={false}
              value={
                <Container justifycontent="center" flexdirection="column" flexgrow="1">
                  <Text>{item.title}</Text>
                  <Text color="grey-100">{item.secondaryTitle}</Text>
                </Container>
              }
              onChange={() => {}}
            />
          ))}
        </FilterItemsContainer>
      ),
    },
    nonClassifiedSKU: {
      title: 'Non-classified SKU',
      component: (
        <FilterItemsContainer className={spacing.mt8}>
          {nonClassifiedSKUFilterOptions.map((item) => (
            <FilterDropDownItem
              key={item.value}
              id={item.value}
              type="item"
              checked={false}
              blurred={false}
              value={item.title}
              onChange={() => {}}
            />
          ))}
        </FilterItemsContainer>
      ),
    },
    status: {
      title: 'Status',
      component: (
        <FilterItemsContainer className={spacing.mt8}>
          {statusFilterOptions.map((item) => (
            <FilterDropDownItem
              key={item.value}
              id={item.value}
              type="item"
              checked={false}
              blurred={false}
              value={item.title}
              onChange={() => {}}
            />
          ))}
        </FilterItemsContainer>
      ),
    },
  };

  const filters = Object.keys(filtersValue) as Filters[];

  return (
    <FiltersWrapper active>
      <ContentLoader isLoading={isLoading} height="32px" width="915px">
        {filters.map((item) => (
          <FilterDropDown
            key={item}
            title={filtersValue[item].title}
            value="All"
            onClear={() => {}}
            selectedAmount={0}
          >
            <Container
              className={classNames(spacing.m16, spacing.w288fixed)}
              flexdirection="column"
            >
              <FilterSearch
                title={filtersValue[item].title + ':'}
                value={filterSearchValue || ''}
                onChange={handleChange}
              />
              {filtersValue[item].component}
            </Container>
            <Divider fullHorizontalWidth />
            <Container
              className={classNames(spacing.m16, spacing.w288fixed)}
              justifycontent="space-between"
              alignitems="center"
              flexdirection="row-reverse"
            >
              <Button
                width="auto"
                height="32"
                padding="8"
                font="text-medium"
                onClick={() => {}}
                background={'violet-90-violet-100'}
                color="white-100"
              >
                Apply
              </Button>
              <Button
                width="auto"
                height="32"
                padding="8"
                font="text-medium"
                onClick={() => {}}
                background={'blue-10-red-10'}
                color="violet-90-red-90"
              >
                Clear all
              </Button>
            </Container>
          </FilterDropDown>
        ))}
      </ContentLoader>
    </FiltersWrapper>
  );
};

export default ImportPageFilters;
