import { DatePicker } from 'Components/base/DatePicker';
import { Icon } from 'Components/base/Icon';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { ReactComponent as InfoIcon } from 'Svg/v2/16/info.svg';
import classNames from 'classnames';
import React, { ReactElement, useState } from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';
import { Nullable } from '../../../types/general';

export type ModalType = 'activate' | 'deactivate';
type ChooseDateModalProps = {
  type: ModalType;
  handleClose(): void;
};

const ChooseDateModal = ({ type, handleClose }: ChooseDateModalProps): ReactElement => {
  const [dateValue, setDateValue] = useState<Nullable<Date>>(null);

  const disableSave = !dateValue;

  const handleSave = () => {
    handleClose();
  };

  const modalValueByType: Record<ModalType, { title: string }> = {
    activate: {
      title: 'Please choose date for activation of transactions import',
    },
    deactivate: {
      title: 'Please choose date for deactivation of transactions import',
    },
  };

  return (
    <Modal
      size={'352'}
      type="small"
      classNameModal={classNames(spacing.m16)}
      classNameModalBody={classNames(spacing.pY0, spacing.p16)}
      pluginScrollDisabled
      background={'grey-10'}
      padding="none"
      overlay="none"
      header={
        <ModalHeaderNew
          background={'grey-10'}
          title={
            <Container alignitems="flex-start" gap="4">
              <Icon icon={<InfoIcon />} path={'grey-100'} className={spacing.pt24} />
              <Text type="body-regular-14" color={'grey-100'} className={spacing.pt20}>
                {modalValueByType[type].title}
              </Text>
            </Container>
          }
          closeActionBackgroundColor="inherit"
          onClose={handleClose}
        />
      }
      footer={
        <ModalFooterNew background={'grey-10'}>
          <Button height="40" width="md" onClick={handleSave} disabled={disableSave}>
            Save
          </Button>
        </ModalFooterNew>
      }
    >
      <Container
        flexdirection="column"
        border="grey-20"
        background={'white-100'}
        radius
        borderRadius="10"
        style={{ boxShadow: '0 10px 30px 0 rgba(192, 196, 205, 0.30)' }}
      >
        <DatePicker
          name="activate-google-cabinet"
          selected={dateValue}
          onChange={(dates) => {
            setDateValue(dates);
          }}
        />
      </Container>
    </Modal>
  );
};

export default ChooseDateModal;
