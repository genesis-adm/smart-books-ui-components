import { Checkbox } from 'Components/base/Checkbox';
import { DatePicker } from 'Components/base/DatePicker';
import { Divider } from 'Components/base/Divider';
import { Radio } from 'Components/base/Radio';
import { Button } from 'Components/base/buttons/Button';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { FilterTab, FilterTabWrapper } from 'Components/base/tabs/FilterTab';
import { Text } from 'Components/base/typography/Text';
import { ExportStatus } from 'Components/custom/Export/ExportStatus';
import { FilterDropDown } from 'Components/elements/filters/FilterDropDown';
import { FilterDropDownItem } from 'Components/elements/filters/FilterDropDownItem';
import { FilterItemsContainer } from 'Components/elements/filters/FilterItemsContainer';
import { FolderIcon } from 'Components/graphics/FolderIcon';
import { Modal } from 'Components/modules/Modal';
import ModalHeader from 'Components/modules/Modal/ModalHeader';
import { Table } from 'Components/old/Table';
import { TableBody } from 'Components/old/Table/TableBody';
import { TableCell } from 'Components/old/Table/TableCell';
import { TableHead } from 'Components/old/Table/TableHead';
import { TableRow } from 'Components/old/Table/TableRow';
import { TableTitle } from 'Components/old/Table/TableTitle';
import { ReactComponent as DownloadIcon } from 'Svg/v2/16/download.svg';
import { ReactComponent as EraseIcon } from 'Svg/v2/16/erase.svg';
import { ReactComponent as TrashboxIcon } from 'Svg/v2/16/trashbox.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Export',
};

interface Information {
  id: string;
  date: string;
  user: string;
  filename: string;
  status: 'new' | 'failed' | 'success';
}

const data: Information[] = [
  {
    id: '1000000',
    date: '12/02/2021',
    user: 'Karine Mnatsakanian',
    filename: 'pl_request_project_export_2021-07-27_08-13',
    status: 'new',
  },
  {
    id: '1000001',
    date: '12/02/2021',
    user: 'Karine Mnatsakanian',
    filename: 'pl_request_project_export_2021-07-27_08-13',
    status: 'success',
  },
  {
    id: '1000002',
    date: '12/02/2021',
    user: 'Karine Mnatsakanian',
    filename: 'pl_request_project_export_2021-07-27_08-13',
    status: 'failed',
  },
  {
    id: '1000003',
    date: '12/02/2021',
    user: 'Karine Mnatsakanian',
    filename: 'pl_request_project_export_2021-07-27_08-13',
    status: 'success',
  },
  {
    id: '1000004',
    date: '12/02/2021',
    user: 'Karine Mnatsakanian',
    filename: 'pl_request_project_export_2021-07-27_08-13',
    status: 'success',
  },
  {
    id: '1000005',
    date: '12/02/2021',
    user: 'Karine Mnatsakanian',
    filename: 'pl_request_project_export_2021-07-27_08-13',
    status: 'failed',
  },
  {
    id: '1000006',
    date: '12/02/2021',
    user: 'Karine Mnatsakanian',
    filename: 'pl_request_project_export_2021-07-27_08-13',
    status: 'failed',
  },
  {
    id: '1000007',
    date: '12/02/2021',
    user: 'Karine Mnatsakanian',
    filename: 'pl_request_project_export_2021-07-27_08-13',
    status: 'success',
  },
  {
    id: '1000008',
    date: '12/02/2021',
    user: 'Karine Mnatsakanian',
    filename: 'pl_request_project_export_2021-07-27_08-13',
    status: 'success',
  },
  {
    id: '1000009',
    date: '12/02/2021',
    user: 'Karine Mnatsakanian',
    filename: 'pl_request_project_export_2021-07-27_08-13',
    status: 'failed',
  },
  {
    id: '1000010',
    date: '12/02/2021',
    user: 'Karine Mnatsakanian',
    filename: 'pl_request_project_export_2021-07-27_08-13',
    status: 'failed',
  },
  {
    id: '1000011',
    date: '12/02/2021',
    user: 'Karine Mnatsakanian',
    filename: 'pl_request_project_export_2021-07-27_08-13',
    status: 'failed',
  },
  {
    id: '1000012',
    date: '12/02/2021',
    user: 'Karine Mnatsakanian',
    filename: 'pl_request_project_export_2021-07-27_08-13',
    status: 'failed',
  },
  {
    id: '1000013',
    date: '12/02/2021',
    user: 'Karine Mnatsakanian',
    filename: 'pl_request_project_export_2021-07-27_08-13',
    status: 'success',
  },
  {
    id: '1000014',
    date: '12/02/2021',
    user: 'Karine Mnatsakanian',
    filename: 'pl_request_project_export_2021-07-27_08-13',
    status: 'success',
  },
  {
    id: '1000015',
    date: '12/02/2021',
    user: 'Karine Mnatsakanian',
    filename: 'pl_request_project_export_2021-07-27_08-13',
    status: 'success',
  },
  {
    id: '1000016',
    date: '12/02/2021',
    user: 'Karine Mnatsakanian',
    filename: 'pl_request_project_export_2021-07-27_08-13',
    status: 'success',
  },
  {
    id: '1000017',
    date: '12/02/2021',
    user: 'Karine Mnatsakanian',
    filename: 'pl_request_project_export_2021-07-27_08-13',
    status: 'success',
  },
  {
    id: '1000018',
    date: '12/02/2021',
    user: 'Karine Mnatsakanian',
    filename: 'pl_request_project_export_2021-07-27_08-13',
    status: 'success',
  },
  {
    id: '1000019',
    date: '12/02/2021',
    user: 'Karine Mnatsakanian',
    filename: 'pl_request_project_export_2021-07-27_08-13',
    status: 'success',
  },
];

export const ExportPage: React.FC = () => {
  const [active, setActive] = useState<string>('presets');
  const [startDay, setStartDay] = useState<Date | null>(new Date());
  const [endDay, setEndDay] = useState<Date | null>(null);
  const changeDayHandler = (dates: [Date | null, Date | null]) => {
    const [start, end] = dates;

    setStartDay(start);
    setEndDay(end);
  };
  return (
    <Modal
      size="full"
      overlay="grey-10"
      background="grey-10"
      header={<ModalHeader type="fullwidth" backgroundColor="white-100" title="Export page" />}
    >
      <Container
        flexdirection="column"
        className={classNames(spacing.m20, spacing.p24)}
        background="white-100"
        radius
        flexgrow="1"
      >
        <Container justifycontent="space-between" alignitems="center">
          <Container alignitems="center" className={spacing.mX10}>
            <FilterDropDown title="Date" value="All" onClear={() => {}} selectedAmount={0}>
              <DatePicker
                name="calendar"
                withPresets
                withRange
                startDate={startDay}
                endDate={endDay}
                onChange={changeDayHandler}
                className={spacing.mt16}
              />
              <Divider fullHorizontalWidth />
              <Container
                className={classNames(spacing.m16, spacing.w288fixed)}
                alignitems="center"
                flexdirection="row-reverse"
              >
                <Button
                  width="auto"
                  height="32"
                  padding="8"
                  onClick={() => {}}
                  background="violet-90-violet-100"
                  color="white-100"
                >
                  Apply
                </Button>
              </Container>
            </FilterDropDown>
            <FilterDropDown title="User" value="All" onClear={() => {}} selectedAmount={0}>
              <FilterItemsContainer className={classNames(spacing.mX8, spacing.mY16)}>
                {Array.from(Array(10)).map((_, index) => (
                  <FilterDropDownItem
                    key={index + 1}
                    id={String(index + 1)}
                    type="item"
                    checked={false}
                    blurred={false}
                    value="Karine Mnatsakanian"
                    onChange={() => {}}
                  />
                ))}
              </FilterItemsContainer>
            </FilterDropDown>
            <FilterDropDown title="Status" value="All" onClear={() => {}} selectedAmount={0}>
              <FilterItemsContainer className={classNames(spacing.mX8, spacing.mY16)}>
                <FilterDropDownItem
                  id="1"
                  type="item"
                  checked={false}
                  blurred={false}
                  value="Pending"
                  onChange={() => {}}
                />
                <FilterDropDownItem
                  id="2"
                  type="item"
                  checked={false}
                  blurred={false}
                  value="Failed"
                  onChange={() => {}}
                />
                <FilterDropDownItem
                  id="3"
                  type="item"
                  checked={false}
                  blurred={false}
                  value="Done"
                  onChange={() => {}}
                />
              </FilterItemsContainer>
            </FilterDropDown>
            <IconButton
              size="32"
              icon={<EraseIcon />}
              background="blue-10"
              color="violet-90-violet-100"
              tooltip="Clear all filters"
              onClick={() => {}}
            />
          </Container>
          <Text type="text-medium">
            Result:&nbsp;
            {data.length}
          </Text>
        </Container>
        <Table
          className={spacing.mY16}
          tableHead={
            <TableHead margin="none">
              <TableTitle minWidth="50" onClick={() => {}}>
                <Checkbox name="tickAll" checked onChange={() => {}} />
              </TableTitle>
              <TableTitle minWidth="170" title="ID" noSortIcon onClick={() => {}} />
              <TableTitle minWidth="170" title="Date" sortIcon onClick={() => {}} />
              <TableTitle minWidth="240" title="User" sortIcon onClick={() => {}} />
              <TableTitle
                minWidth="240"
                title="File name"
                flexgrow="1"
                noSortIcon
                onClick={() => {}}
              />
              <TableTitle minWidth="220" title="Status" sortIcon onClick={() => {}} />
              <TableTitle minWidth="100" title="Action" onClick={() => {}} />
            </TableHead>
          }
        >
          <TableBody margin="none" display="block">
            {data.map(({ id, ...item }) => (
              <TableRow size="extrasmall" key={id} background="mixed" gap="0">
                <TableCell minWidth="50" justifycontent="center" alignitems="center">
                  <Checkbox name={`line-${id}`} checked onChange={() => {}} />
                </TableCell>
                <TableCell minWidth="170">
                  <Text>{id}</Text>
                </TableCell>
                <TableCell minWidth="170">
                  <Text>{item.date}</Text>
                </TableCell>
                <TableCell minWidth="240">
                  <Text>{item.user}</Text>
                </TableCell>
                <TableCell minWidth="240" flexgrow="1">
                  <Text>{item.filename}</Text>
                </TableCell>
                <TableCell minWidth="220">
                  <ExportStatus status={item.status} />
                </TableCell>
                <TableCell minWidth="100" justifycontent="center">
                  <Container>
                    <IconButton
                      icon={<DownloadIcon />}
                      background="transparent"
                      color="black-100-violet-90"
                      tooltip="Download"
                      onClick={() => {}}
                    />
                    <IconButton
                      icon={<TrashboxIcon />}
                      background="transparent"
                      color="black-100-red-90"
                      tooltip="Delete"
                      onClick={() => {}}
                    />
                  </Container>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Container>
    </Modal>
  );
};

export const ExportPageEmpty: React.FC = () => (
  <Modal
    size="full"
    overlay="grey-10"
    background="grey-10"
    header={<ModalHeader type="fullwidth" backgroundColor="white-100" title="Export page" />}
  >
    <Container
      flexdirection="column"
      className={classNames(spacing.m20, spacing.p24)}
      background="white-100"
      radius
      flexgrow="1"
    >
      {/* Start empty block */}
      <Container flexgrow="1" flexdirection="column" justifycontent="center" alignitems="center">
        <Container
          className={classNames(spacing.w460)}
          flexdirection="column"
          justifycontent="center"
          alignitems="center"
        >
          <FolderIcon />
          <Text className={spacing.mt32} align="center" type="title-bold">
            Export page
          </Text>
          <Text className={spacing.mt16} align="center" type="body-regular-16" color="grey-100">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur viverra, orci id
            semper dapibus, elit eros ullamcorper diam, vel ultrices metus erat quis quam.
          </Text>
        </Container>
      </Container>
      {/* End empty block */}
    </Container>
  </Modal>
);
