import { TooltipIcon } from 'Components/base/TooltipIcon';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { SettingsContainer } from 'Components/base/grid/SettingsContainer';
import { Text } from 'Components/base/typography/Text';
import { ModalSettings } from 'Components/custom/Settings/ModalSettings';
import { Settings2StepVerification } from 'Components/custom/Settings/Settings2StepVerification';
import { SettingsInfoAction } from 'Components/custom/Settings/SettingsInfoAction';
import { SettingsNavigation } from 'Components/custom/Settings/SettingsNavigation';
import { SettingsNavigationButton } from 'Components/custom/Settings/SettingsNavigationButton';
import { SettingsTitle } from 'Components/custom/Settings/SettingsTitle';
import { Logo } from 'Components/elements/Logo';
import { OrganisationLaunch } from 'Components/elements/OrganisationLaunch';
import { Input } from 'Components/old/Input';
import { ReactComponent as LogoutIcon } from 'Svg/16/logout.svg';
import { ReactComponent as PhoneIcon } from 'Svg/16/phone.svg';
import { ReactComponent as QuestionIcon } from 'Svg/24/question.svg';
import { ReactComponent as SuccessIcon } from 'Svg/24/success.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/My Account Settings',
};

export const MyAccountPersonalInfo: React.FC = () => (
  <ModalSettings title="My account settings" className={spacing.p20}>
    <Container className={classNames(spacing.mt20)} flexgrow="1">
      <SettingsNavigation className={spacing.mr20}>
        <Container
          className={classNames(spacing.w100p, spacing.h100p)}
          flexdirection="column"
          alignitems="center"
          flexgrow="1"
        >
          <Container
            className={spacing.w100p}
            flexdirection="column"
            alignitems="center"
            flexgrow="1"
          >
            <Logo content="user" size="lg" name="Karine Mnatsakanian" />
            <Text className={spacing.mY20} type="body-medium" align="center">
              Karine Mnatsakanian
            </Text>
            <SettingsNavigationButton title="Personal info" selected onClick={() => {}} />
            <SettingsNavigationButton title="Sign in" selected={false} onClick={() => {}} />
            <SettingsNavigationButton title="Security" selected={false} onClick={() => {}} />
          </Container>
          <SettingsNavigationButton
            type="action"
            icon={<LogoutIcon />}
            title="Sign out"
            onClick={() => {}}
          />
        </Container>
      </SettingsNavigation>
      <SettingsContainer>
        <SettingsTitle title="Personal information:" />
        <Container flexwrap="wrap" justifycontent="space-between">
          <Input
            className={spacing.mt20}
            width="large"
            name="username"
            placeholder="Name"
            label="Name"
            isRequired
            readOnly
          />
          <Input
            className={spacing.mt20}
            width="large"
            name="usersurname"
            placeholder="Surname"
            label="Surname"
            isRequired
            readOnly
          />
          <Input
            className={spacing.mt20}
            width="large"
            name="userphone"
            icon={<PhoneIcon />}
            iconPosition="left"
            placeholder="+ 1 099 000 00 00"
            label="Phone number"
            isRequired
            readOnly
          />
        </Container>
        <Container className={spacing.mt50} justifycontent="space-between" alignitems="center">
          <Container alignitems="center">
            <Text type="text-medium" color="grey-100" className={spacing.mr16}>
              You have access to:
            </Text>
            <TooltipIcon icon={<QuestionIcon />} message="Some text" />
          </Container>
          <LinkButton icon={<PlusIcon />} onClick={() => {}}>
            Create organization account
          </LinkButton>
        </Container>
        <Container className={spacing.mt30} flexdirection="column" overflow="y-auto">
          <OrganisationLaunch
            name="Genesis"
            type="Organisation"
            background="grey-10"
            logo="https://picsum.photos/60"
            onLaunch={() => {}}
          />
          <OrganisationLaunch
            name="JiJi"
            type="Organisation"
            background="grey-10"
            logo="https://picsum.photos/70"
            onLaunch={() => {}}
          />
          <OrganisationLaunch
            name="Solid"
            type="Organisation"
            background="grey-10"
            logo="https://picsum.photos/80"
            onLaunch={() => {}}
          />
        </Container>
      </SettingsContainer>
    </Container>
  </ModalSettings>
);

export const MyAccountSignIn: React.FC = () => (
  <ModalSettings title="My account settings" className={spacing.p20}>
    <Container className={classNames(spacing.mt20)} flexgrow="1">
      <SettingsNavigation className={spacing.mr20}>
        <Container
          className={classNames(spacing.w100p, spacing.h100p)}
          flexdirection="column"
          alignitems="center"
          flexgrow="1"
        >
          <Container
            className={spacing.w100p}
            flexdirection="column"
            alignitems="center"
            flexgrow="1"
          >
            <Logo content="user" size="lg" name="Karine Mnatsakanian" />
            <Text className={spacing.mY20} type="body-medium" align="center">
              Karine Mnatsakanian
            </Text>
            <SettingsNavigationButton title="Personal info" selected={false} onClick={() => {}} />
            <SettingsNavigationButton title="Sign in" selected onClick={() => {}} />
            <SettingsNavigationButton title="Security" selected={false} onClick={() => {}} />
          </Container>
          <SettingsNavigationButton
            type="action"
            icon={<LogoutIcon />}
            title="Sign out"
            onClick={() => {}}
          />
        </Container>
      </SettingsNavigation>
      <SettingsContainer>
        <SettingsTitle className={spacing.mb20} title="Sign in information:" />
        <SettingsInfoAction title="User ID" value="01222938475" />
        <SettingsInfoAction
          title="Email address"
          value="karine.mnatsakanian@mail.com"
          actionText="Change"
          onClick={() => {}}
        />
        <SettingsInfoAction
          title="Password"
          value="• • • • • • • • • • • • • • • •"
          actionText="Change"
          onClick={() => {}}
        />
      </SettingsContainer>
    </Container>
  </ModalSettings>
);

export const MyAccountSecurity: React.FC = () => (
  <ModalSettings title="My account settings" className={spacing.p20}>
    <Container className={classNames(spacing.mt20)} flexgrow="1">
      <SettingsNavigation className={spacing.mr20}>
        <Container
          className={classNames(spacing.w100p, spacing.h100p)}
          flexdirection="column"
          alignitems="center"
          flexgrow="1"
        >
          <Container
            className={spacing.w100p}
            flexdirection="column"
            alignitems="center"
            flexgrow="1"
          >
            <Logo content="user" size="lg" name="Karine Mnatsakanian" />
            <Text className={spacing.mY20} type="body-medium" align="center">
              Karine Mnatsakanian
            </Text>
            <SettingsNavigationButton title="Personal info" selected={false} onClick={() => {}} />
            <SettingsNavigationButton title="Sign in" selected={false} onClick={() => {}} />
            <SettingsNavigationButton title="Security" selected onClick={() => {}} />
          </Container>
          <SettingsNavigationButton
            type="action"
            icon={<LogoutIcon />}
            title="Sign out"
            onClick={() => {}}
          />
        </Container>
      </SettingsNavigation>
      <SettingsContainer>
        <SettingsTitle className={spacing.mb20} title="Security:" />
        <SettingsInfoAction
          title="Linked Identities"
          value="Signed in with Google"
          actionText="Remove"
          onClick={() => {}}
        />
        <Settings2StepVerification onChange={() => {}} />
      </SettingsContainer>
    </Container>
  </ModalSettings>
);
