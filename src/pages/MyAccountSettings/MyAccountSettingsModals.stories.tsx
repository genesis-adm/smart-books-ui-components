import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { InputCodeGroup } from 'Components/custom/Login/InputCodeGroup';
import { SettingsModal } from 'Components/custom/Settings/SettingsModal';
import { PasswordCheck } from 'Components/elements/PasswordCheck';
import { Input } from 'Components/old/Input';
import { ReactComponent as InfoIcon } from 'Svg/24/info.svg';
import { ReactComponent as LoadingIcon } from 'Svg/24/loading.svg';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/My Account Settings/Modals',
};

export const RequestEmailChange: React.FC = () => (
  <SettingsModal
    textButtonLeft="Cancel"
    textButtonRight="Send request"
    onClickLeft={() => {}}
    onClickRight={() => {}}
    onClose={() => {}}
  >
    <Text className={spacing.mt50} type="title-bold" align="center">
      Request email change
    </Text>
    <Text className={spacing.mt20} type="body-regular-16" color="grey-100" align="center">
      We will send message with a confirmation code to your current email. If you don&apos;t have
      access to current email please contact to admin
    </Text>
    <Input
      className={spacing.mt30}
      width="full"
      name="InputDefault"
      type="email"
      placeholder="Enter current email address"
    />
  </SettingsModal>
);

export const ConfirmEmailChange: React.FC = () => (
  <SettingsModal
    textButtonLeft="Cancel"
    textButtonRight="Confirm"
    onClickLeft={() => {}}
    onClickRight={() => {}}
    onClose={() => {}}
  >
    <Text className={spacing.mt50} type="title-bold" align="center">
      Confirm email change
    </Text>
    <Text className={spacing.mt20} type="body-regular-16" color="grey-100" align="center">
      A message with with confirmation code has been sent to the your email
      karine.mnatsakanian@mail.com
    </Text>
    <InputCodeGroup className={spacing.mt30} onChange={() => {}} />
    <Container flexdirection="column" alignitems="center" className={spacing.mb40}>
      <Text className={spacing.mt30} type="text-regular">
        If you don&apos;t receive a code please
      </Text>
      <Text className={spacing.mt10} type="text-medium">
        Resend email after 04:54
      </Text>
      <LinkButton className={spacing.mt10} type="text-regular" onClick={() => {}}>
        Resend email
      </LinkButton>
    </Container>
  </SettingsModal>
);

export const AddNewEmailStep: React.FC = () => (
  <SettingsModal
    textButtonLeft="Cancel"
    textButtonRight="Change email"
    onClickLeft={() => {}}
    onClickRight={() => {}}
    onClose={() => {}}
  >
    <Text className={spacing.mt50} type="title-bold" align="center">
      Add new email
    </Text>
    <Text className={spacing.mt20} type="body-regular-16" color="grey-100" align="center">
      You should have access for new email, because we will ask you confirm it. If you don&apos;t
      have access to new email please cancel this operation
    </Text>
    <Input
      className={spacing.mt30}
      width="full"
      name="InputDefault"
      type="email"
      statusType="error"
      placeholder="Enter new email address"
    />
  </SettingsModal>
);
export const CreateNewPassword: React.FC = () => (
  <SettingsModal
    textButtonLeft="Cancel"
    textButtonRight="Next"
    onClickLeft={() => {}}
    onClickRight={() => {}}
    onClose={() => {}}
  >
    <Text className={spacing.mt40} type="title-bold" align="center">
      Create new password
    </Text>
    <Input
      className={spacing.mt30}
      width="full"
      type="password"
      name="Password"
      placeholder="Password"
      label="Enter password"
      isRequired
    />
    <Input
      className={spacing.mt10}
      width="full"
      type="password"
      name="PasswordConfirm"
      placeholder="Confirm password"
      label="Confirm password"
      isRequired
    />
    <PasswordCheck valid={false} conditionText="At least 8 characters" className={spacing.mt25} />
    <PasswordCheck
      valid
      conditionText="Both uppercase and lowercase letters"
      className={spacing.mt10}
    />
    <PasswordCheck
      valid={false}
      conditionText="At least one number and one special character"
      className={classNames(spacing.mt10, spacing.mb40)}
    />
  </SettingsModal>
);
