export type ExternalCabinet = {
  id: number;
  name: string;
  ccyCode: string;
};
