import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { ImportProcessor } from 'Components/custom/Sales/AllSales/ImportProcessorType/ImportProcessorType.types';
import { TextArea } from 'Components/inputs/TextArea';
import { TextInput } from 'Components/inputs/TextInput';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { clientSecretExample } from 'Pages/Integration/ExternalCabinet/modals/CreateIntegration/components/dialogTooltipExamples/ClientSecretDialogInfo';
import { VIEW_INTEGRATION_MODAL_ID } from 'Pages/Integration/ExternalCabinet/modals/EditIntegration/EditGeneralInformation';
import { ReactComponent as InfoIcon } from 'Svg/v2/16/info.svg';
import React, { useState } from 'react';
import { ReactElement } from 'react';

type EditIntegrationSetupProps = {
  importProcessorType: ImportProcessor;
  handleClose(): void;
};

const EditIntegrationSetup = ({
  importProcessorType,
  handleClose,
}: EditIntegrationSetupProps): ReactElement => {
  const [clientID, setClientID] = useState<string>(
    'K9ZZKS0KW9Q|9a9bf291-f561-4c6f-a274-4bfc02175344',
  );
  const [accountIdentifier, setAccountIdentifier] = useState<string>(
    'pubsite_prod_rev_12340056780009000006',
  );
  const [clientSecret, setClientSecret] = useState<string>(clientSecretExample);

  const isApple = importProcessorType === 'apple';

  return (
    <Modal
      size="630"
      height="739"
      containerId={VIEW_INTEGRATION_MODAL_ID}
      centered
      overlay="white-100"
      padding="24"
      header={
        <ModalHeaderNew
          title="Edit Intergration setup"
          fontType="body-medium"
          titlePosition="center"
          border
          closeActionBackgroundColor="inherit"
          onClose={handleClose}
        />
      }
      footer={
        <ModalFooterNew border>
          <Button width="100" height="40" onClick={handleClose}>
            Update
          </Button>
        </ModalFooterNew>
      }
    >
      <Container gap="24" flexdirection="column">
        {isApple && (
          <TextInput
            name="clientID"
            label="Client ID"
            value={clientID}
            onChange={(e) => {
              setClientID(e.target.value);
            }}
            required
            width="full"
            icon={<InfoIcon />}
          />
        )}
        <TextInput
          name="accountIdentifier"
          label="Account Identifier"
          value={accountIdentifier}
          onChange={(e) => {
            setAccountIdentifier(e.target.value);
          }}
          required
          width="full"
          icon={<InfoIcon />}
        />
        <TextArea
          name="clientSecret"
          label="Client Secret"
          value={clientSecret}
          minHeight="48"
          onChange={(e) => {
            setClientSecret(e.target.value);
          }}
          maxLength={'none'}
          required
          width="full"
          icon={<InfoIcon />}
        />
      </Container>
    </Modal>
  );
};

export default EditIntegrationSetup;
