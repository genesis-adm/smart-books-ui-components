import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { ImportProcessor } from 'Components/custom/Sales/AllSales/ImportProcessorType/ImportProcessorType.types';
import CurrencySelect from 'Components/custom/Stories/selects/CurrencySelect';
import { TextInput } from 'Components/inputs/TextInput';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import React, { useState } from 'react';
import { ReactElement } from 'react';

export const VIEW_INTEGRATION_MODAL_ID = 'view_integration_modal';

type EditCabinetNameProps = {
  importProcessorType: ImportProcessor;
  handleClose(): void;
};

const EditGeneralInformation = ({
  importProcessorType,
  handleClose,
}: EditCabinetNameProps): ReactElement => {
  const [cabinetName, setCabinetName] = useState<string>('BetterMe Limited GP');

  const isApple = importProcessorType === 'apple';

  return (
    <Modal
      size="630"
      height="336"
      centered
      overlay="white-100"
      padding="24"
      pluginScrollDisabled
      containerId={VIEW_INTEGRATION_MODAL_ID}
      header={
        <ModalHeaderNew
          title="Edit Cabinet Name"
          fontType="body-medium"
          titlePosition="center"
          border
          closeActionBackgroundColor="inherit"
          onClose={handleClose}
        />
      }
      footer={
        <ModalFooterNew border>
          <Button width="100" height="40" onClick={handleClose}>
            Update
          </Button>
        </ModalFooterNew>
      }
    >
      <Container gap="24">
        <TextInput
          name="cabinetName"
          label="Cabinet Name"
          value={cabinetName}
          onChange={(e) => {
            setCabinetName(e.target.value);
          }}
          required
          width="full"
        />

        {isApple && <CurrencySelect />}
      </Container>
    </Modal>
  );
};

export default EditGeneralInformation;
