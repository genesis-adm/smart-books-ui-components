import { Meta, Story } from '@storybook/react';
import { DropDown } from 'Components/DropDown';
import { Divider } from 'Components/base/Divider';
import { Icon } from 'Components/base/Icon';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ImportProcessor } from 'Components/custom/Sales/AllSales/ImportProcessorType/ImportProcessorType.types';
import ModalHeaderSkeleton from 'Components/loaders/ModalHeaderSkeleton/ModalHeaderSkeleton';
import { Modal } from 'Components/modules/Modal';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import EditGeneralInformation, {
  VIEW_INTEGRATION_MODAL_ID,
} from 'Pages/Integration/ExternalCabinet/modals/EditIntegration/EditGeneralInformation';
import EditIntegrationSetup from 'Pages/Integration/ExternalCabinet/modals/EditIntegration/EditIntegrationSetup';
import ReportsInformationTable from 'Pages/Integration/ExternalCabinet/modals/ViewIntegration/components/ReportsInformationTable';
import ViewIntegrationMainSkeleton from 'Pages/Integration/ExternalCabinet/modals/ViewIntegration/components/ViewIntegrationMainSkeleton';
import { dataByCabinetType } from 'Pages/Integration/ExternalCabinet/modals/ViewIntegration/constants';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import { ReactComponent as DropdownIcon } from 'Svg/v2/16/three-dots.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';

const ViewIntegrationModal: Story<{
  storyState: string;
  importProcessorType: Extract<ImportProcessor, 'apple' | 'googlePlay'>;
}> = ({ storyState, importProcessorType }) => {
  const isApple = importProcessorType === 'apple';
  const isLoading = storyState === 'loading';
  const [isOpenedEditGeneralInformationModal, setIsOpenedEditGeneralInformationModal] =
    useState<boolean>(false);
  const [isOpenedEditIntegrationSetupModal, setIsOpenedEditIntegrationSetupModal] =
    useState<boolean>(false);

  const handleOpenGeneralInformationModal = () => {
    setIsOpenedEditGeneralInformationModal(true);
  };

  const handleOpenIntegrationSetupModal = () => {
    setIsOpenedEditIntegrationSetupModal(true);
  };

  const handleCloseCabinetNameModal = () => {
    setIsOpenedEditGeneralInformationModal(false);
  };

  const handleCloseIntegrationSetupModal = () => {
    setIsOpenedEditIntegrationSetupModal(false);
  };

  return (
    <Modal
      modalId={VIEW_INTEGRATION_MODAL_ID}
      size="720"
      type="new"
      background={'grey-10'}
      header={
        <ModalHeaderSkeleton isLoading={isLoading} optionsBtn>
          <ModalHeaderNew
            title="View Integration"
            border
            closeActionBackgroundColor="inherit"
            background={'grey-10'}
            dropdownBtn={
              <DropDown
                flexdirection="column"
                padding="8"
                control={({ handleOpen }) => (
                  <IconButton
                    size="24"
                    icon={<DropdownIcon />}
                    onClick={handleOpen}
                    background={'transparent-grey-20'}
                    color="grey-100"
                    tooltip="Show more"
                  />
                )}
              >
                <DropDownButton
                  size="224"
                  icon={<EditIcon />}
                  onClick={handleOpenGeneralInformationModal}
                >
                  Edit General information
                </DropDownButton>
                <DropDownButton
                  size="224"
                  icon={<EditIcon />}
                  onClick={handleOpenIntegrationSetupModal}
                >
                  Edit Integration setup
                </DropDownButton>
              </DropDown>
            }
            onClose={() => {}}
          />
        </ModalHeaderSkeleton>
      }
    >
      {isOpenedEditGeneralInformationModal && (
        <EditGeneralInformation
          importProcessorType={importProcessorType}
          handleClose={handleCloseCabinetNameModal}
        />
      )}
      {isOpenedEditIntegrationSetupModal && (
        <EditIntegrationSetup
          importProcessorType={importProcessorType}
          handleClose={handleCloseIntegrationSetupModal}
        />
      )}
      <Container
        background={'white-100'}
        flexdirection="column"
        borderRadius="20"
        className={classNames(spacing.p24, spacing.mt12, spacing.w100p)}
        gap="24"
      >
        <ViewIntegrationMainSkeleton isLoading={isLoading}>
          <Container justifycontent="space-between" alignitems="center">
            <Container flexdirection="column" gap="16">
              <Text type="body-medium" className={spacing.pb4}>
                BetterMe Limited GP
              </Text>
              {isApple && (
                <Container gap="4">
                  <Text type="caption-regular" color="grey-100">
                    Cabinet Currency:
                  </Text>
                  <Text type="caption-regular">USD</Text>
                </Container>
              )}
              <Container gap="4">
                <Text type="caption-regular" color="grey-100">
                  Customer:
                </Text>
                <Text type="caption-regular">Customer name</Text>
              </Container>
              <Container gap="4">
                <Text type="caption-regular" color="grey-100">
                  Legal entity:
                </Text>
                <Text type="caption-regular">BetterMe Limited International</Text>
              </Container>
              <Container gap="4">
                <Text type="caption-regular" color="grey-100">
                  Initial import start date:
                </Text>
                <Text type="caption-regular">17 Jul 2024</Text>
              </Container>
            </Container>
            <Container flexdirection="column" gap="8" alignitems="center">
              <Icon icon={dataByCabinetType?.[importProcessorType]?.icon} />
              <Text type="caption-regular" color="grey-100">
                {dataByCabinetType?.[importProcessorType]?.iconLabel}
              </Text>
            </Container>
          </Container>
          <Divider fullHorizontalWidth />
          <Container flexdirection="column" gap="16">
            <Text type="body-medium" className={spacing.pb4}>
              Reports
            </Text>
            <ReportsInformationTable importProcessorType={importProcessorType} />
          </Container>
        </ViewIntegrationMainSkeleton>
      </Container>
    </Modal>
  );
};

export default {
  title: 'Pages/Integration/External Cabinet/View integration',
  component: ViewIntegrationModal,
  argTypes: {
    storyState: {
      options: ['loading', 'loaded'],
      control: {
        type: 'radio',
        labels: {
          loading: 'Modal is loading',
          loaded: 'Modal loaded',
        },
      },
    },
    importProcessorType: {
      options: ['apple', 'googlePlay'],
      control: {
        type: 'radio',
        labels: {
          apple: 'Apple cabinet',
          googlePlay: 'Google Play cabinet',
        },
      },
    },
  },
} as Meta;

export const ViewIntegration = ViewIntegrationModal.bind({});
ViewIntegration.args = {
  storyState: 'loaded',
  importProcessorType: 'googlePlay',
};
