import { Tag } from 'Components/base/Tag';
import { Button } from 'Components/base/buttons/Button';
import { ImportProcessor } from 'Components/custom/Sales/AllSales/ImportProcessorType/ImportProcessorType.types';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { TableCellWidth } from 'Components/types/gridTypes';
import {
  dataByCabinetType,
  reportsTableHeaderData,
} from 'Pages/Integration/ExternalCabinet/modals/ViewIntegration/constants';
import { ReactComponent as CheckIcon } from 'Svg/v2/16/tick-in-filled-circle.svg';
import { ReactComponent as WarningIcon } from 'Svg/v2/16/warning.svg';
import React from 'react';

type ReportsInformationTableProps = {
  importProcessorType: Extract<ImportProcessor, 'apple' | 'googlePlay'>;
};

const ReportsInformationTable = ({ importProcessorType }: ReportsInformationTableProps) => {
  return (
    <TableNew
      noResults={false}
      tableHead={
        <TableHeadNew>
          {reportsTableHeaderData.map((item, index) => (
            <TableTitleNew
              key={index}
              minWidth={item.minWidth as TableCellWidth}
              title={item.title}
              align={item.align as TableTitleNewProps['align']}
              onClick={() => {}}
            />
          ))}
        </TableHeadNew>
      }
    >
      <TableBodyNew>
        {Array.from(Array(dataByCabinetType?.[importProcessorType]?.reportsRowsNumber)).map(
          (_, index) => (
            <TableRowNew key={index} gap="8">
              <TableCellTextNew
                minWidth={reportsTableHeaderData[0].minWidth as TableCellWidth}
                value={
                  dataByCabinetType?.[importProcessorType]?.reportsData[index]?.reportTypeLabel
                }
                fixedWidth
                type="caption-regular"
              />
              <TableCellNew
                minWidth={reportsTableHeaderData[1].minWidth as TableCellWidth}
                justifycontent="center"
              >
                <Tag size="24" text="200" align="center" />
              </TableCellNew>
              <TableCellTextNew
                minWidth={reportsTableHeaderData[2].minWidth as TableCellWidth}
                value="08 July 2021"
                fixedWidth
              />
              <TableCellNew
                minWidth={reportsTableHeaderData[3].minWidth as TableCellWidth}
                justifycontent="center"
                fixedWidth
              >
                {index === 1 ? (
                  <Tag
                    size="24"
                    color="green"
                    icon={<CheckIcon />}
                    align="center"
                    iconTooltip="Active"
                  />
                ) : (
                  <Tag
                    size="24"
                    color="yellow"
                    icon={<WarningIcon />}
                    align="center"
                    iconTooltip="Disabled"
                  />
                )}
              </TableCellNew>
              <TableCellNew
                minWidth={reportsTableHeaderData[4].minWidth as TableCellWidth}
                justifycontent="center"
                fixedWidth
              >
                <Button
                  height="24"
                  width="64"
                  color="violet-90-violet-100"
                  font="text-medium"
                  background={'blue-10-blue-20'}
                >
                  {index === 1 ? 'Disable' : 'Activate'}
                </Button>
              </TableCellNew>
            </TableRowNew>
          ),
        )}
      </TableBodyNew>
    </TableNew>
  );
};

export default ReportsInformationTable;
