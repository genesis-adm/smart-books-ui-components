import { ContentLoader } from 'Components/base/ContentLoader';
import { Divider } from 'Components/base/Divider';
import { Container } from 'Components/base/grid/Container';
import classNames from 'classnames';
import React, { FC, ReactElement, ReactNode } from 'react';

import spacing from '../../../../../../assets/styles/spacing.module.scss';

type ViewIntegrationMainSkeletonProps = {
  isLoading: boolean;
  children: ReactNode;
};

const ViewIntegrationMainSkeleton: FC<ViewIntegrationMainSkeletonProps> = ({
  isLoading,
  children,
}: ViewIntegrationMainSkeletonProps): ReactElement => {
  if (isLoading)
    return (
      <Container flexdirection="column">
        <Container justifycontent="space-between">
          <Container flexdirection="column" gap="20">
            <ContentLoader height="20px" width="173px" isLoading />

            <Container flexdirection="column" gap="12">
              <ContentLoader height="16px" width="173px" isLoading />
              <ContentLoader height="16px" width="173px" isLoading />
              <ContentLoader height="16px" width="173px" isLoading />
            </Container>
          </Container>

          <Container
            flexdirection="column"
            gap="4"
            className={classNames(spacing.pt16, spacing.pr10)}
          >
            <ContentLoader height="52px" width="52px" isLoading />
            <ContentLoader height="12px" width="52px" isLoading />
          </Container>
        </Container>

        <Divider fullHorizontalWidth className={spacing.mY24} />

        <Container flexdirection="column" gap="20" className={spacing.pt5}>
          <Container flexdirection="column" gap="16">
            <ContentLoader height="20px" width="63px" isLoading />
            <ContentLoader height="20px" width="624px" isLoading />
          </Container>

          <Container flexdirection="column" gap="4">
            <ContentLoader height="60px" width="624px" isLoading />
            <ContentLoader height="60px" width="624px" isLoading />
          </Container>
        </Container>
      </Container>
    );

  return <>{children}</>;
};

export default ViewIntegrationMainSkeleton;
