import { ReactComponent as AppleLogoIcon52 } from 'Svg/v2/52/apple_logo_black_52.svg';
import { ReactComponent as GooglePlayLogoIcon52 } from 'Svg/v2/52/google_play_logo_52.svg';
import React from 'react';

export const reportsTableHeaderData = [
  {
    minWidth: '144',
    title: 'Report type',
  },
  {
    minWidth: '115',
    title: 'Transactions',
    align: 'center',
  },
  {
    minWidth: '166',
    title: 'Last successfull import',
    // align: 'center',
  },
  {
    minWidth: '88',
    title: 'Status',
    align: 'center',
  },
  {
    minWidth: '88',
    title: 'Actions',
    align: 'center',
  },
];

export const dataByCabinetType = {
  apple: {
    icon: <AppleLogoIcon52 />,
    iconLabel: 'Apple',
    reportsRowsNumber: 1,
    reportsData: [{ reportTypeLabel: 'Daily Sales report' }],
  },
  googlePlay: {
    icon: <GooglePlayLogoIcon52 />,
    iconLabel: 'Google Play',
    reportsRowsNumber: 2,
    reportsData: [
      { reportTypeLabel: 'Daily Estimate report' },
      { reportTypeLabel: 'Monthly Earnings report' },
    ],
  },
};
