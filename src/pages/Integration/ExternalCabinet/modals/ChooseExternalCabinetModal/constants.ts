import { ExternalCabinet } from 'Pages/Integration/ExternalCabinet/ExternalCabinet.types';

export const externalCabinetList: ExternalCabinet[] = [
  { id: 1, name: 'Cabinet 1', ccyCode: 'USD' },
  { id: 2, name: 'Cabinet 2', ccyCode: 'UAH' },
  { id: 3, name: 'Cabinet 3', ccyCode: 'EUR' },
  { id: 4, name: 'Cabinet 4 tooooooooooo looooooooooooooooong name', ccyCode: 'UAH' },
  { id: 5, name: 'Cabinet 5', ccyCode: 'ZLT' },
  { id: 6, name: 'Cabinet 6', ccyCode: 'UAH' },
  { id: 7, name: 'Cabinet 7', ccyCode: 'EUR' },
  { id: 8, name: 'Cabinet 8', ccyCode: 'USD' },
  { id: 9, name: 'Cabinet 9', ccyCode: 'UAH' },
  { id: 10, name: 'Cabinet 10', ccyCode: 'USD' },
];
