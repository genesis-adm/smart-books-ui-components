import { Meta, Story } from '@storybook/react';
import ChooseExternalCabinetModal from 'Pages/Integration/ExternalCabinet/modals/ChooseExternalCabinetModal/ChooseExternalCabinetModal';
import React from 'react';

const ChooseExternalCabinetComponent: Story = ({ state, integrationType }) => {
  const isLoading = state === 'loading';

  return <ChooseExternalCabinetModal isLoading={isLoading} type={integrationType} />;
};

export default {
  title: 'Pages/Integration/External Cabinet/Choose External Cabinet',
  component: ChooseExternalCabinetComponent,
  argTypes: {
    state: {
      name: 'State',
      options: ['loading', 'loaded'],
      control: {
        type: 'radio',
        labels: {
          loading: 'Data is loading',
          loaded: 'Data was loaded',
        },
      },
    },
    integrationType: {
      name: 'Integration type',
      options: ['googlePlay', 'apple'],
      control: {
        type: 'radio',
        labels: {
          googlePlay: 'Google play',
          apple: 'Apple',
        },
      },
    },
  },
} as Meta;

export const ChooseExternalCabinet = ChooseExternalCabinetComponent.bind({});
ChooseExternalCabinet.args = {
  state: 'loaded',
  integrationType: 'googlePlay',
};
