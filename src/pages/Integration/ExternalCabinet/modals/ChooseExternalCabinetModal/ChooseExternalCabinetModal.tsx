import { ContentLoader } from 'Components/base/ContentLoader';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { OptionSingleNew } from 'Components/base/inputs/SelectNew/options/OptionSingleNew';
import { Search } from 'Components/inputs/Search';
import useSearchExample from 'Components/inputs/Search/hooks/useSearchExample';
import ModalHeaderSkeleton from 'Components/loaders/ModalHeaderSkeleton/ModalHeaderSkeleton';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { ExternalCabinet } from 'Pages/Integration/ExternalCabinet/ExternalCabinet.types';
import { externalCabinetList } from 'Pages/Integration/ExternalCabinet/modals/ChooseExternalCabinetModal/constants';
import { IntegrationType } from 'Pages/Integration/ExternalCabinet/modals/CreateIntegration/components/CreateIntegrationMain';
import React, { FC, ReactElement, useState } from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';

type ChooseExternalCabinetModalProps = {
  externalCabinetId?: number;
  isLoading?: boolean;
  type: IntegrationType;
  containerId?: string;
  onClose?: () => void;
  onSubmit?: (value: ExternalCabinet) => void;
};

const ChooseExternalCabinetModal: FC<ChooseExternalCabinetModalProps> = ({
  externalCabinetId,
  isLoading = false,
  type,
  containerId,
  onClose,
  onSubmit,
}: ChooseExternalCabinetModalProps): ReactElement => {
  const [value, setValue] = useState<number | undefined>(externalCabinetId);

  const { searchValue, setSearchValue, searchValuesList, handleSearchValueAdd, handleClearAll } =
    useSearchExample();

  const resetValue = () => setValue(undefined);

  const closeModal = (): void => {
    onClose ? onClose() : resetValue();
  };

  const submit = () => {
    const submittedValue = externalCabinetList?.find(({ id }) => id === value);

    if (!onSubmit) return resetValue();

    if (submittedValue) {
      onSubmit(submittedValue);
      onClose?.();
    }
  };

  return (
    <Modal
      size="530"
      height="575"
      containerId={containerId}
      padding="24"
      overlay={containerId ? 'white-100' : 'black-100'}
      classNameModalBody={spacing.h100p}
      flexdirection="column"
      pluginScrollDisabled
      header={
        <ModalHeaderSkeleton titleWidthPx={348} isLoading={isLoading} border>
          <ModalHeaderNew
            height="60"
            border
            title="Please choose a Cabinet Name"
            titlePosition="center"
            fontType="body-medium"
            onClose={closeModal}
          />
        </ModalHeaderSkeleton>
      }
      footer={
        <ModalFooterNew border>
          <ContentLoader height="40px" width="108px" isLoading={isLoading}>
            <Button
              background={'violet-90-violet-100'}
              color="white-100"
              width="108"
              height="40"
              disabled={!value}
              onClick={submit}
            >
              Save
            </Button>
          </ContentLoader>
        </ModalFooterNew>
      }
    >
      <Container flexdirection="column" gap="16" className={spacing.h100p}>
        <Container style={{ flexShrink: 0 }}>
          <ContentLoader isLoading={isLoading}>
            <Search
              height="40"
              name="search-cabinet-name"
              placeholder="Search by Cabinet name"
              value={searchValue}
              onChange={(e) => setSearchValue(e.target.value)}
              searchParams={searchValuesList}
              onSearch={handleSearchValueAdd}
              onClear={handleClearAll}
            />
          </ContentLoader>
        </Container>

        <Container flexgrow="1" overflow="y-auto" flexdirection="column">
          <Container flexdirection="column" gap="8" customScroll={!isLoading}>
            {isLoading ? (
              <Skeleton />
            ) : (
              externalCabinetList.map(({ id, name }) => (
                <OptionSingleNew
                  key={id}
                  label={name}
                  selected={value === id}
                  type="radio"
                  onClick={() => setValue(id)}
                  style={{ flexShrink: 0 }}
                />
              ))
            )}
          </Container>
        </Container>
      </Container>
    </Modal>
  );
};

export default ChooseExternalCabinetModal;

const Skeleton: FC = (): ReactElement => {
  return (
    <>
      {Array.from(Array(6)).map((_, i) => (
        <ContentLoader key={i} isLoading />
      ))}
    </>
  );
};
