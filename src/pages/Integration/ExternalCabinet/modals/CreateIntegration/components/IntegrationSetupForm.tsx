import DialogTooltip from 'Components/base/DialogTooltip/DialogTooltip';
import Icon from 'Components/base/Icon/Icon';
import { Container } from 'Components/base/grid/Container';
import { InputDate } from 'Components/base/inputs/InputDate';
import { Text } from 'Components/base/typography/Text';
import { TextArea } from 'Components/inputs/TextArea';
import { TextInput } from 'Components/inputs/TextInput';
import { IntegrationType } from 'Pages/Integration/ExternalCabinet/modals/CreateIntegration/components/CreateIntegrationMain';
import AccountIdentifierDialogInfo from 'Pages/Integration/ExternalCabinet/modals/CreateIntegration/components/dialogTooltipExamples/AccountIdentifierDialogInfo';
import ClientSecretDialogInfo from 'Pages/Integration/ExternalCabinet/modals/CreateIntegration/components/dialogTooltipExamples/ClientSecretDialogInfo';
import IntegrationSetupFormPlaceholder from 'Pages/Integration/ExternalCabinet/modals/CreateIntegration/components/placeholders/IntegrationSetupFormPlaceholder';
import { IntegrationTypeContext } from 'Pages/Integration/ExternalCabinet/modals/CreateIntegration/context/IntegrationTypeContext';
import { ReactComponent as InfoIcon } from 'Svg/v2/16/info.svg';
import classNames from 'classnames';
import React, { ReactElement, useContext, useState } from 'react';

import spacing from '../../../../../../assets/styles/spacing.module.scss';

type IntegrationSetupFormProps = {
  isLoading?: boolean;
};

const IntegrationSetupForm = ({ isLoading }: IntegrationSetupFormProps): ReactElement => {
  const { integrationType } = useContext(IntegrationTypeContext);

  const [clientId, setClientId] = useState<string>('');
  const [accountIdentifier, setAccountIdentifier] = useState<string>('');
  const [clientSecret, setClientSecret] = useState<string>('');
  const [dateValue, setDateValue] = useState<Date>();

  const dialogTooltipBasedOnType: Record<
    IntegrationType,
    { accountIdentifier: ReactElement; clientSecret: ReactElement; clientId?: ReactElement }
  > = {
    googlePlay: {
      accountIdentifier: (
        <DialogTooltip
          title="How to find?"
          classNameContent={classNames(spacing.mY12)}
          control={({ handleOpen }) => (
            <Icon
              icon={<InfoIcon />}
              path="grey-100"
              cursor="pointer"
              onClick={(e) => {
                e.stopPropagation();
                handleOpen();
              }}
            />
          )}
        >
          <AccountIdentifierDialogInfo />
        </DialogTooltip>
      ),
      clientSecret: (
        <DialogTooltip
          title="How to find?"
          classNameContent={classNames(spacing.mY12)}
          control={({ handleOpen }) => (
            <Icon
              icon={<InfoIcon />}
              path="grey-100"
              cursor="pointer"
              onClick={(e) => {
                e.stopPropagation();
                handleOpen();
              }}
            />
          )}
        >
          <ClientSecretDialogInfo />
        </DialogTooltip>
      ),
    },
    apple: {
      clientId: <Icon icon={<InfoIcon />} path="grey-100" />,
      accountIdentifier: <Icon icon={<InfoIcon />} path="grey-100" />,
      clientSecret: <Icon icon={<InfoIcon />} path="grey-100" />,
    },
  };

  if (isLoading) return <IntegrationSetupFormPlaceholder />;

  return (
    <Container
      background={'white-100'}
      flexdirection="column"
      justifycontent="center"
      border="grey-20"
      borderRadius="20"
      className={classNames(spacing.p24, spacing.mt12, spacing.w100p)}
      gap="24"
    >
      <Container flexdirection="column" gap="8">
        <Text type="body-medium">Integration setup</Text>
        <Text type="body-regular-14" color="grey-100">
          Please set up your cabinet connection following the instructions.
        </Text>
      </Container>
      {integrationType === 'apple' && (
        <TextInput
          name="clientId"
          label="Client ID"
          value={clientId}
          onChange={(e) => {
            setClientId(e.target.value);
          }}
          required
          width="full"
          icon={dialogTooltipBasedOnType[integrationType].clientId}
        />
      )}
      <TextInput
        name="accountIdentifier"
        label="Account Identifier"
        value={accountIdentifier}
        onChange={(e) => {
          setAccountIdentifier(e.target.value);
        }}
        required
        width="full"
        icon={dialogTooltipBasedOnType[integrationType].accountIdentifier}
      />
      <TextArea
        name="clientSecret"
        label="Client Secret"
        value={clientSecret}
        minHeight="48"
        onChange={(e) => {
          setClientSecret(e.target.value);
        }}
        maxLength={'none'}
        required
        width="full"
        icon={dialogTooltipBasedOnType[integrationType].clientSecret}
      />
      <InputDate
        label="Import start date"
        name="calendar"
        selected={dateValue || null}
        calendarStartDay={1}
        popperDirection="bottom"
        width="full"
        onChange={(date) => {
          setDateValue(date);
        }}
        required
      />
    </Container>
  );
};

export default IntegrationSetupForm;
