import { Divider } from 'Components/base/Divider';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { IntegrationType } from 'Pages/Integration/ExternalCabinet/modals/CreateIntegration/components/CreateIntegrationMain';
import ReportInformationCard from 'Pages/Integration/ExternalCabinet/modals/CreateIntegration/components/ReportInformationCard';
import ReportsFormPlaceholder from 'Pages/Integration/ExternalCabinet/modals/CreateIntegration/components/placeholders/ReportsFormPlaceholder';
import { IntegrationTypeContext } from 'Pages/Integration/ExternalCabinet/modals/CreateIntegration/context/IntegrationTypeContext';
import classNames from 'classnames';
import React, { ReactElement, ReactNode, useContext, useState } from 'react';

import spacing from '../../../../../../assets/styles/spacing.module.scss';

export type ReportsFormProps = {
  isLoading?: boolean;
};
const ReportsForm = ({ isLoading }: ReportsFormProps): ReactElement => {
  const { integrationType } = useContext(IntegrationTypeContext);

  const [isActiveOptionalReport, setIsActiveOptionalReport] = useState<boolean>();

  const toggleReport = () => {
    setIsActiveOptionalReport((prev) => !prev);
  };

  const reportsByIntegrationType: Record<IntegrationType, ReactNode> = {
    googlePlay: (
      <>
        <ReportInformationCard type="monthlyReport" />
        <Divider type="vertical" height="38" />
        <ReportInformationCard
          type="estimatedDailyReport"
          isActive={isActiveOptionalReport}
          toggleReportActive={toggleReport}
        />
      </>
    ),
    apple: <ReportInformationCard type="dailySalesReport" />,
  };

  if (isLoading) return <ReportsFormPlaceholder />;

  return (
    <Container
      background={'white-100'}
      flexdirection="column"
      justifycontent="center"
      border="grey-20"
      borderRadius="20"
      className={classNames(spacing.pY24, spacing.pX8, spacing.mt12, spacing.w100p)}
      gap="8"
    >
      {integrationType !== 'apple' && (
        <Text type="body-medium" className={spacing.pX16}>
          Reports
        </Text>
      )}
      <Container gap="8">{reportsByIntegrationType[integrationType]}</Container>
    </Container>
  );
};

export default ReportsForm;
