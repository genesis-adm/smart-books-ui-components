import { Switch } from 'Components/base/Switch';
import { Tag } from 'Components/base/Tag';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as SleepingStickerIcon } from 'Svg/v2/16/sleeping-sticker.svg';
import { ReactComponent as CheckIcon } from 'Svg/v2/16/tick-in-filled-circle.svg';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../../../../assets/styles/spacing.module.scss';

type ReportType = 'monthlyReport' | 'estimatedDailyReport' | 'dailySalesReport';
type ReportData = {
  title: string;
  description: string;
  isDefault: boolean;
};
type ReportInformationCardProps = {
  type: ReportType;
  isActive?: boolean;
  toggleReportActive?(): void;
};

const ReportInformationCard = ({
  type,
  isActive,
  toggleReportActive,
}: ReportInformationCardProps) => {
  const reportData: Record<ReportType, ReportData> = {
    monthlyReport: {
      title: 'Monthly Earnings report',
      description:
        'This financial report matches the monthly payment you receive. Earnings are reported on the 4th of each month.',
      isDefault: true,
    },
    estimatedDailyReport: {
      title: 'Estimated Daily Sales report',
      description:
        'This report shows all transactions charged or refunded. The Estimated Sales Report is imported daily.',
      isDefault: false,
    },
    dailySalesReport: {
      title: 'Daily Sales Report',
      description:
        'This report shows all transactions charged or refunded. Sales Report is imported daily.',
      isDefault: true,
    },
  };

  return (
    <Container
      background={'transparent-grey-10'}
      flexdirection="column"
      gap="8"
      borderRadius="14"
      className={classNames(spacing.pX16, spacing.pY8, spacing.w100p)}
    >
      <Container justifycontent="space-between">
        <Text type="body-regular-14" color="grey-100">
          {reportData[type].title}
        </Text>
        {!reportData[type].isDefault && (
          <Switch
            id="enableReport"
            checked={isActive}
            onChange={() => {
              if (toggleReportActive) {
                toggleReportActive();
              }
            }}
          />
        )}
      </Container>
      <Container gap="4" alignitems="center">
        {reportData[type].isDefault || isActive ? (
          <Tag color="violet" text="Connected" icon={<CheckIcon />} cursor="default" />
        ) : (
          <Tag color="grey-10-grey-90" text="Off" icon={<SleepingStickerIcon />} cursor="default" />
        )}
        <Text type="caption-regular" color="grey-90">
          {reportData[type].isDefault ? 'Default' : 'Optional'}
        </Text>
      </Container>
      <Text color="grey-100" type="caption-regular-height-20">
        {reportData[type].description}
      </Text>
    </Container>
  );
};

export default ReportInformationCard;
