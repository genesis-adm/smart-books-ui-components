import { Divider } from 'Components/base/Divider';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../../../../../assets/styles/spacing.module.scss';

export const clientSecretExample = `{
  type: 'service_account',
  project_id: 'test-acc',
  private_key_id: '7e9a00c2a1db73b951e393730f0ee617dc61b',
  private_key: '-----BEGIN PRIVATE KEY----- MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDA3NNrgFMiBM6A5sGYeJuFgTKeJNjm+4P854syQBuQfBN7z741tUFDhi3tcoiwjyaN30KDwzXV4yGA3py5cHHmqVxj4OmcTzn8HjYtNx25KkN8ER2g45O+gjk\n8N+1NNunO1iOYlRJ6QEizxGbeuLYp87cNDqNkqOjam1RGLg35CdGPKYgz6CPxcYjT0F4suT90/khgerdSFB3cjcOf3/9bOBUwcPgBU9Alw+vdivrT6C5e+i6kJde9sxWwE0+gfdrrfNmjK0axQUQFb6M0J/jweS8MBlfo3y5vA==\n-----END PRIVATE KEY-----',
  client_email: 'example-reports@test-acc.iam.gserviceaccount.com',
  client_id: '106191899108888826480',
  auth_uri: 'https://accounts.google.com/o/oauth2/auth',
  token_uri: 'https://oauth2.googleapis.com/token',
  auth_provider_x509_cert_url: 'https://www.googleapis.com/oauth2/v1/certs',
  client_x509_cert_url: 'https://www.googleapis.com/robot/v1/metadata/x509/example-reports%40test-acc.iam.gserviceaccount.com',
}`;

const ClientSecretDialogInfo = () => (
  <>
    <Container gap="4">
      <Text type="caption-regular-height-24" color="grey-90" style={{ fontStyle: 'italic' }}>
        Note:
      </Text>
      <Text type="caption-regular-height-24" color="grey-100" style={{ fontStyle: 'italic' }}>
        You can find Client Secret using Google Service
      </Text>
    </Container>
    <Text type="caption-regular-height-24" color="grey-100" style={{ fontStyle: 'italic' }}>
      Account key in valid JSON format.
    </Text>
    <Divider fullHorizontalWidth />
    <Container flexdirection="column" gap="8">
      <Text type="caption-regular-height-24" color="grey-100">
        1. Create Service Account (technical account used for integration) using Owner’s Google Play
        Account
      </Text>
      <Text type="caption-regular-height-24" color="grey-100">
        2. Give permission "Financial data” to the Service Account
      </Text>
      <Text type="caption-regular-height-24" color="grey-100">
        3. Service Account can get access to the report more than 24 hours after adding permissions
      </Text>
      <Text type="caption-regular-height-24" color="grey-100">
        4. You can create one Service Account and add it to the several Accounts.
      </Text>
      <Text type="caption-regular-height-24" color="grey-100">
        5. After creating Service Account generate account "KEY" in JSON format. Enter the KEY JSON
        file content to the “Credentials” field.
      </Text>
    </Container>
    <Container
      background={'grey-10'}
      borderRadius="10"
      flexdirection="column"
      className={classNames(spacing.pX16, spacing.pY8, spacing.w100p)}
    >
      <Text type="caption-regular-height-24" color="grey-90">
        Example:
      </Text>
      <Text
        type="caption-regular-height-24"
        color="grey-100"
        className={spacing.w100p}
        style={{ whiteSpace: 'break-spaces', wordWrap: 'break-word' }}
      >
        {clientSecretExample}
      </Text>
    </Container>
    <Text type="caption-regular-height-24" color="grey-90" style={{ fontStyle: 'italic' }}>
      More information:
    </Text>
    <LinkButton type="caption-regular" onClick={() => {}}>
      Download and export reports - Play Console Help
    </LinkButton>
  </>
);

export default ClientSecretDialogInfo;
