import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as ArrowRightIcon } from 'Svg/v2/16/arrow-right.svg';
import React from 'react';

import Icon from '../../../../../../../components/base/Icon/Icon';

const AccountIdentifierDialogInfo = () => (
  <>
    <Container gap="4">
      <Text color="grey-100" type="caption-regular-height-20">
        1. Go to the
      </Text>
      <LinkButton type="caption-regular" onClick={() => {}}>
        Google Play Console
      </LinkButton>
    </Container>
    <Container gap="4" alignitems="center">
      <Text color="grey-100" type="caption-regular-height-24">
        2. Choose Account and select page from left menu Download reports
      </Text>
      <Icon icon={<ArrowRightIcon transform="scale(0.8)" />} path="grey-100" display="inline" />
      <Text color="grey-100" type="caption-regular-height-24">
        Financial
      </Text>
    </Container>
    <Text color="grey-100" type="caption-regular-height-24">
      3. Find button Copy Cloud Storage Uri and click on it. Now you have in buffer full link for
      Google Cloud Reports (Example: gs://pubsite_prod_rev_12340056780009000006/earnings/). Account
      ID in this case is the middle part of this link like “pubsite_prod_rev_12340056780009000006”
    </Text>
  </>
);

export default AccountIdentifierDialogInfo;
