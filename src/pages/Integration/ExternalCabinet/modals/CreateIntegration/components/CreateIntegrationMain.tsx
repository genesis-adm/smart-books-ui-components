import GeneralInformationForm from 'Pages/Integration/ExternalCabinet/modals/CreateIntegration/components/GeneralInformationForm';
import IntegrationSetupForm from 'Pages/Integration/ExternalCabinet/modals/CreateIntegration/components/IntegrationSetupForm';
import ReportsForm from 'Pages/Integration/ExternalCabinet/modals/CreateIntegration/components/ReportsForm';
import React, { ReactElement } from 'react';

export type IntegrationType = 'googlePlay' | 'apple';
export type CreateIntegrationMainProps = {
  isLoading?: boolean;
};
const CreateIntegrationMain = ({ isLoading }: CreateIntegrationMainProps): ReactElement => {
  return (
    <>
      <GeneralInformationForm isLoading={isLoading} />
      <IntegrationSetupForm isLoading={isLoading} />
      <ReportsForm isLoading={isLoading} />
    </>
  );
};

export default CreateIntegrationMain;
