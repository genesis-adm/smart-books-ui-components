import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { GeneralTab, GeneralTabWrapper } from 'Components/base/tabs/GeneralTab';
import { Text } from 'Components/base/typography/Text';
import { DevInProgressTooltip } from 'Components/elements/DevInProgressTooltip';
import {
  importProcessors,
  tabs,
} from 'Pages/Integration/ExternalCabinet/modals/CreateIntegration/constants';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../../../../assets/styles/spacing.module.scss';

const SelectImportProcessor = () => {
  return (
    <Container
      flexdirection="column"
      justifycontent="center"
      alignitems="center"
      className={classNames(spacing.w100p, spacing.h100p)}
    >
      <Container
        background={'white-100'}
        flexdirection="column"
        justifycontent="center"
        alignitems="center"
        borderRadius="20"
        className={classNames(spacing.p24, spacing.mt12, spacing.h460fixed, spacing.w100p)}
        gap="24"
      >
        <Text type="body-regular-14">Select Import processor</Text>
        <GeneralTabWrapper>
          {tabs.map(({ id, title, disabled, icon }) => (
            <DevInProgressTooltip key={id} disabled={!disabled}>
              <GeneralTab
                id={id}
                disabled={disabled}
                active={id === 'sales'}
                icon={icon}
                onClick={() => {}}
              >
                {title}
              </GeneralTab>
            </DevInProgressTooltip>
          ))}
        </GeneralTabWrapper>
        <Container
          flexwrap="wrap"
          alignitems="center"
          justifycontent="space-around"
          className={spacing.w250fixed}
        >
          {importProcessors.map(({ id, tooltip, title, disabled, icon }) => (
            <Container key={id} flexdirection="column" gap="12">
              <IconButton
                icon={icon}
                disabled={disabled}
                tooltip={tooltip}
                size="64"
                border="grey-20"
                borderRadius="20"
                background={'grey-10-grey-20'}
                sourceIconColor
                color="grey-100"
              />
              <Text type="caption-regular" color="grey-100" align="center">
                {title}
              </Text>
            </Container>
          ))}
        </Container>
      </Container>
    </Container>
  );
};

export default SelectImportProcessor;
