import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import CurrencySelect from 'Components/custom/Stories/selects/CurrencySelect';
import { Search } from 'Components/inputs/Search';
import { Select } from 'Components/inputs/Select';
import { OptionWithFourLabels } from 'Components/inputs/Select/options/OptionWithFourLabels';
import { OptionWithSingleLabel } from 'Components/inputs/Select/options/OptionWithSingleLabel';
import { TextInput } from 'Components/inputs/TextInput';
import { customers, legalEntities } from 'Mocks/fakeOptions';
import GeneralInformationFormPlaceholder from 'Pages/Integration/ExternalCabinet/modals/CreateIntegration/components/placeholders/GeneralInformationFormPlaceholder';
import { IntegrationTypeContext } from 'Pages/Integration/ExternalCabinet/modals/CreateIntegration/context/IntegrationTypeContext';
import classNames from 'classnames';
import React, { ReactElement, useContext, useState } from 'react';

import spacing from '../../../../../../assets/styles/spacing.module.scss';

type GeneralInformationFormProps = {
  isLoading?: boolean;
};
const GeneralInformationForm = ({ isLoading }: GeneralInformationFormProps): ReactElement => {
  const { integrationType } = useContext(IntegrationTypeContext);

  const [cabinetName, setCabinetName] = useState<string>('');
  const [searchValue, setSearchValue] = useState<string>('');

  if (isLoading) return <GeneralInformationFormPlaceholder />;

  return (
    <Container
      background={'white-100'}
      flexdirection="column"
      justifycontent="center"
      border="grey-20"
      borderRadius="20"
      className={classNames(spacing.p24, spacing.mt12, spacing.w100p)}
      gap="16"
    >
      <Container flexdirection="column" gap="8">
        <Text type="body-medium">General information</Text>
        <Container gap="12">
          <Text type="body-regular-14" color="grey-100">
            <Text type="inherit" color="grey-90" className={spacing.pr4}>
              Note:
            </Text>
            All transactions imported by this integration will be booked under the selected Legal
            Entity and Customer. These cannot be changed after the cabinet is created.
          </Text>
        </Container>
      </Container>
      <Container gap="24" flexdirection="column">
        <Container gap="24">
          <TextInput
            name="cabinetName"
            label="Cabinet Name"
            value={cabinetName}
            onChange={(e) => {
              setCabinetName(e.target.value);
            }}
            required
            width="full"
          />
          {integrationType === 'apple' && <CurrencySelect label="Cabinet currency" />}
        </Container>
        <Container gap="24">
          <Select label="Legal entity" placeholder="Select option" width="300" required>
            {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
              <SelectDropdown selectElement={selectElement} width="300">
                <SelectDropdownBody>
                  <Search
                    className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                    width="full"
                    height="40"
                    name="search"
                    placeholder="Search Legal entity"
                    value={searchValue}
                    onChange={(e) => setSearchValue(e.target.value)}
                    searchParams={[]}
                    onSearch={() => {}}
                    onClear={() => {}}
                  />
                  <SelectDropdownItemList>
                    {legalEntities.map((value, index) => (
                      <OptionWithSingleLabel
                        id={value.id}
                        key={index}
                        label={value?.title}
                        selected={!Array.isArray(selectValue) && selectValue?.value === value.id}
                        onClick={() => {
                          onOptionClick({
                            value: value?.id,
                            label: value?.title,
                          });
                          closeDropdown();
                        }}
                      />
                    ))}
                  </SelectDropdownItemList>
                </SelectDropdownBody>
              </SelectDropdown>
            )}
          </Select>
          <Select
            label="Customer"
            placeholder="Select option"
            type="baseWithCategory"
            width="300"
            required
          >
            {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
              <SelectDropdown selectElement={selectElement} width="300">
                <SelectDropdownBody>
                  <Search
                    className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                    width="full"
                    height="40"
                    name="search"
                    placeholder="Search Legal entity"
                    value={searchValue}
                    onChange={(e) => setSearchValue(e.target.value)}
                    searchParams={[]}
                    onSearch={() => {}}
                    onClear={() => {}}
                  />
                  <SelectDropdownItemList>
                    {customers.map((value, index) => {
                      const taxAgentLabel = value.isTaxAgent ? 'Tax agent' : 'Not tax agent';
                      return (
                        <OptionWithFourLabels
                          id={value.id}
                          key={index}
                          label={value?.name}
                          secondaryLabel={taxAgentLabel}
                          tertiaryLabel={value.type}
                          selected={!Array.isArray(selectValue) && selectValue?.value === value.id}
                          onClick={() => {
                            onOptionClick({
                              value: value?.id,
                              label: value?.name,
                              subLabel: taxAgentLabel,
                              tertiaryLabel: value.type,
                            });
                            closeDropdown();
                          }}
                        />
                      );
                    })}
                  </SelectDropdownItemList>
                </SelectDropdownBody>
              </SelectDropdown>
            )}
          </Select>
        </Container>
      </Container>
    </Container>
  );
};
export default GeneralInformationForm;
