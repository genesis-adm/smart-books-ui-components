import { ContentLoader } from 'Components/base/ContentLoader';
import { Container } from 'Components/base/grid/Container';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../../../../../assets/styles/spacing.module.scss';

const IntegrationSetupFormPlaceholder = () => (
  <Container
    gap="24"
    background={'white-100'}
    flexdirection="column"
    justifycontent="center"
    border="grey-20"
    borderRadius="20"
    className={classNames(spacing.p24, spacing.mt12, spacing.w100p)}
  >
    <Container gap="8" flexdirection="column">
      <ContentLoader isLoading height="28px" width="304px" />
      <ContentLoader isLoading height="28px" width="469px" />
    </Container>
    <Container flexdirection="column" gap="24">
      <ContentLoader isLoading height="48px" />
      <ContentLoader isLoading height="48px" />
      <ContentLoader isLoading height="48px" />
    </Container>
  </Container>
);

export default IntegrationSetupFormPlaceholder;
