import { IntegrationType } from 'Pages/Integration/ExternalCabinet/modals/CreateIntegration/components/CreateIntegrationMain';
import { createContext } from 'react';

export const IntegrationTypeContext = createContext<{ integrationType: IntegrationType }>({
  integrationType: 'googlePlay',
});
