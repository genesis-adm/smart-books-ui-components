import CreateIntegrationMain from 'Pages/Integration/ExternalCabinet/modals/CreateIntegration/components/CreateIntegrationMain';
import SelectImportProcessor from 'Pages/Integration/ExternalCabinet/modals/CreateIntegration/components/SelectImportProcessor';
import { ReactComponent as FolderIcon } from 'Svg/16/folder-light.svg';
import { ReactComponent as ChartsIcon } from 'Svg/v2/16/charts.svg';
import { ReactComponent as WalletIcon } from 'Svg/v2/16/wallet.svg';
import { ReactComponent as AppleLogoIcon } from 'Svg/v2/24/apple_logo_black.svg';
import { ReactComponent as GooglePlayLogoIcon } from 'Svg/v2/24/google-play-logo-colored.svg';
import { ReactNode } from 'react';
import React from 'react';

type ProcessorType = {
  id: string;
  title: string;
  icon: ReactNode;
  disabled?: boolean;
  tooltip?: string;
};

type TabsType = { id: string; title: string; icon: ReactNode; disabled?: boolean };

export const createIntegrationSteps: Record<
  'step1' | 'step2',
  { render: (isLoading?: boolean) => ReactNode }
> = {
  step1: { render: () => <SelectImportProcessor /> },
  step2: { render: (isLoading) => <CreateIntegrationMain isLoading={isLoading} /> },
};

export const tabs: TabsType[] = [
  { id: 'banking', title: 'Banking', icon: <WalletIcon />, disabled: true },
  { id: 'sales', title: 'Sales', icon: <ChartsIcon /> },
  { id: 'expenses', title: 'Expenses', icon: <FolderIcon />, disabled: true },
];

export const importProcessors: ProcessorType[] = [
  {
    id: 'apple',
    title: 'Apple',
    icon: <AppleLogoIcon />,
    tooltip: 'Create Integration for Apple',
  },
  {
    id: 'google',
    title: 'Google Play',
    icon: <GooglePlayLogoIcon />,
    tooltip: 'Create Integration for Google Play',
  },
];
