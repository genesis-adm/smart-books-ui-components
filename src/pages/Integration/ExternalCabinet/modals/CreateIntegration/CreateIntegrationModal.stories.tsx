import { Meta, Story } from '@storybook/react';
import { ContentLoader } from 'Components/base/ContentLoader';
import { Tag } from 'Components/base/Tag';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import ModalHeaderPlaceholder from 'Components/custom/placeholders/ModalHeaderPlaceholder';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { IntegrationType } from 'Pages/Integration/ExternalCabinet/modals/CreateIntegration/components/CreateIntegrationMain';
import { createIntegrationSteps } from 'Pages/Integration/ExternalCabinet/modals/CreateIntegration/constants';
import { IntegrationTypeContext } from 'Pages/Integration/ExternalCabinet/modals/CreateIntegration/context/IntegrationTypeContext';
import { ReactComponent as AppleLogoIcon } from 'Svg/v2/16/apple_logo_black.svg';
import { ReactComponent as ArrowLeftIcon } from 'Svg/v2/16/arrow-left.svg';
import { ReactComponent as GooglePlayLogoIcon } from 'Svg/v2/24/google-play-logo-colored.svg';
import classNames from 'classnames';
import React, { ReactNode } from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';

const CreateIntegrationModal: Story = ({ step, storyState, integrationType }) => {
  const tagByIntegrationType: Record<IntegrationType, { icon: ReactNode; text: string }> = {
    googlePlay: {
      icon: <GooglePlayLogoIcon transform="scale(0.7)" />,
      text: 'Google Play',
    },
    apple: { icon: <AppleLogoIcon />, text: 'Apple' },
  };

  return (
    <Modal
      size="720"
      type="new"
      background={'grey-10'}
      classNameModalBody={classNames(spacing.w100p, spacing.h100p)}
      header={
        storyState === 'loading' ? (
          <ModalHeaderPlaceholder />
        ) : (
          <ModalHeaderNew
            title={
              <Container gap="8" alignitems="center">
                <Text type="title-bold">Create Integration</Text>
                {step === 'step2' && (
                  <Tag
                    icon={tagByIntegrationType[integrationType as IntegrationType].icon}
                    cursor="default"
                    text={tagByIntegrationType[integrationType as IntegrationType].text}
                    color="grey"
                    size="24"
                    staticIconColor
                  />
                )}
              </Container>
            }
            border
            closeActionBackgroundColor="inherit"
            background={'grey-10'}
            onClose={() => {}}
          />
        )
      }
      footer={
        step === 'step2' && (
          <ModalFooterNew border background={'grey-10'}>
            <ContentLoader isLoading={storyState === 'loading'} width="134px">
              <Button
                height="48"
                width="134"
                background={'violet-90-violet-100'}
                onClick={() => {}}
              >
                Create
              </Button>
            </ContentLoader>
            <ContentLoader isLoading={storyState === 'loading'} width="134px">
              <Button
                height="48"
                navigation
                iconLeft={<ArrowLeftIcon />}
                width="134"
                background={'transparent'}
                border={'grey-40'}
                color="grey-100"
                onClick={() => {}}
              >
                Back
              </Button>
            </ContentLoader>
          </ModalFooterNew>
        )
      }
    >
      <IntegrationTypeContext.Provider value={{ integrationType }}>
        {createIntegrationSteps[step as 'step1' | 'step2'].render(storyState === 'loading')}
      </IntegrationTypeContext.Provider>
    </Modal>
  );
};

export default {
  title: 'Pages/Integration/External Cabinet/Create integration',
  component: CreateIntegrationModal,
  argTypes: {
    storyState: {
      options: ['empty', 'loading', 'loaded'],
      control: {
        type: 'radio',
        labels: {
          empty: 'Empty page',
          loading: 'Page is loading',
          loaded: 'Page loaded',
        },
      },
    },
    step: {
      options: ['step1', 'step2'],
      control: { type: 'radio', labels: { step1: 'Select processor', step2: 'Integration' } },
    },
    integrationType: {
      options: ['googlePlay', 'apple'],
      control: { type: 'radio', labels: { step1: 'Google play', step2: 'Apple' } },
    },
  },
} as Meta;

export const CreateIntegration = CreateIntegrationModal.bind({});
CreateIntegration.args = {
  storyState: 'loaded',
  step: 'step1',
  integrationType: 'googlePlay',
};
