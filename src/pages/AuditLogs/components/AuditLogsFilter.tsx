import { DatePicker } from 'Components/base/DatePicker';
import { Divider } from 'Components/base/Divider';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { FilterDropDown } from 'Components/elements/filters/FilterDropDown';
import { FilterDropDownItem } from 'Components/elements/filters/FilterDropDownItem';
import { FilterItemsContainer } from 'Components/elements/filters/FilterItemsContainer';
import { FilterSearch } from 'Components/elements/filters/FilterSearch';
import { FiltersWrapper } from 'Components/elements/filters/FiltersWrapper';
import { options } from 'Mocks/fakeOptions';
import { auditLogFilters } from 'Pages/AuditLogs/constants';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

type AuditLogsFilterProps = {
  type: 'fullScreen' | 'modal';
  showFilters: boolean;
};
const AuditLogsFilter: React.FC<AuditLogsFilterProps> = ({
  showFilters,
  type,
}: AuditLogsFilterProps) => {
  const [startDay, setStartDay] = useState<Date | null>(new Date());
  const [endDay, setEndDay] = useState<Date | null>(null);
  const changeDayHandler = (dates: [Date | null, Date | null]) => {
    const [start, end] = dates;

    setStartDay(start);
    setEndDay(end);
  };

  const [searchValue, setSearchValue] = useState<string>('');

  const handleChange = (value: string) => {
    setSearchValue(value);
  };
  return (
    <FiltersWrapper active={showFilters}>
      <FilterDropDown title="Date" value="All" onClear={() => {}} selectedAmount={0}>
        <DatePicker
          name="calendar"
          withRange
          withPresets
          startDate={startDay}
          endDate={endDay}
          onChange={changeDayHandler}
        />
        <Divider fullHorizontalWidth />
        <Container
          className={classNames(spacing.m16, spacing.w288fixed)}
          justifycontent="flex-end"
          alignitems="center"
        >
          <Button
            width="auto"
            height="32"
            padding="8"
            font="text-medium"
            onClick={() => {}}
            background="violet-90-violet-100"
            color="white-100"
          >
            Apply
          </Button>
        </Container>
      </FilterDropDown>
      {auditLogFilters.map(({ title, hideOnModalType }) => {
        if (hideOnModalType && type === 'modal') return null;
        return (
          <FilterDropDown
            key={title}
            title={title}
            value="All"
            selectedAmount={0}
            onClear={() => {}}
          >
            <Container
              className={classNames(spacing.m16, spacing.w288fixed)}
              flexdirection="column"
            >
              <FilterSearch title="Account type:" value={searchValue} onChange={handleChange} />
              <FilterItemsContainer className={spacing.mt8}>
                {options.map((item) => (
                  <FilterDropDownItem
                    key={item.value}
                    id={item.value}
                    type="item"
                    checked={false}
                    blurred={false}
                    value={item.label}
                    onChange={() => {}}
                  />
                ))}
              </FilterItemsContainer>
            </Container>
            <Divider fullHorizontalWidth />
            <Container
              className={classNames(spacing.m16, spacing.w288fixed)}
              justifycontent="flex-end"
              alignitems="center"
            >
              <Button
                width="auto"
                height="32"
                padding="8"
                font="text-medium"
                onClick={() => {}}
                background="violet-90-violet-100"
                color="white-100"
              >
                Apply
              </Button>
            </Container>
          </FilterDropDown>
        );
      })}
    </FiltersWrapper>
  );
};

export default AuditLogsFilter;
