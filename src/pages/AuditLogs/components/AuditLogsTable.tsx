import { Tag } from 'Components/base/Tag';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Text } from 'Components/base/typography/Text';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { UserInfo } from 'Components/elements/UserInfo';
import { TableCellWidth } from 'Components/types/gridTypes';
import { tableHead } from 'Pages/AuditLogs/constants';
import { ReactComponent as PlusIcon } from 'Svg/12/add.svg';
import { ReactComponent as PencilIcon } from 'Svg/v2/12/pencil.svg';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

type AuditLogTableProps = {
  type: 'fullScreen' | 'modal';
};
const AuditLogsTable: React.FC<AuditLogTableProps> = ({ type }: AuditLogTableProps) => {
  const eventDescriptionValueByType =
    type === 'fullScreen' ? 'Create Bank transaction' : '15,000.00 → 15,435.00';

  const entityOrFieldNameValueByType = type === 'modal' ? 'Amount ' : 'Bank transaction';

  return (
    <TableNew
      noResults={false}
      className={classNames(spacing.mt24, spacing.mX24, spacing.pr24)}
      tableHead={
        <TableHeadNew background="grey-10" radius="10">
          {tableHead.map((item, index) => {
            if (item.hideOnModalType && type === 'modal') return null;
            if (item.hideOnFullScreenType && type === 'fullScreen') return null;
            return (
              <TableTitleNew
                background="grey-10"
                key={index}
                minWidth={
                  type === 'modal' && item.minWidthOnModalType
                    ? (item.minWidthOnModalType as TableCellWidth)
                    : (item.minWidth as TableCellWidth)
                }
                align={item.align as TableTitleNewProps['align']}
                title={item.title}
                sorting={item.sorting}
                onClick={() => {}}
              />
            );
          })}
        </TableHeadNew>
      }
    >
      <TableBodyNew>
        {Array.from(Array(9)).map((_, index) => (
          <TableRowNew key={index} background="transparent-with-border">
            <TableCellNew minWidth={tableHead[0].minWidth as TableCellWidth} flexdirection="column">
              <Text type="caption-regular">Feb 22, 2023</Text>
              <Text color="grey-100" type="text-regular">
                16:45:11
              </Text>
            </TableCellNew>
            <TableCellNew
              minWidth={
                type === 'fullScreen'
                  ? (tableHead[1].minWidth as TableCellWidth)
                  : (tableHead[1].minWidthOnModalType as TableCellWidth)
              }
            >
              <UserInfo name="VitalyBigBoss" isLogo secondaryText="VitalyBigBoss@gmail.com" />
            </TableCellNew>
            {type === 'fullScreen' && (
              <TableCellTextNew
                minWidth={tableHead[2].minWidth as TableCellWidth}
                value="Transactions"
              />
            )}
            <TableCellNew
              minWidth={tableHead[3].minWidth as TableCellWidth}
              justifycontent="center"
            >
              {type === 'fullScreen' && (
                <Tag text="Create" icon={<PlusIcon />} font="text-regular" />
              )}
              {type === 'modal' && <Tag text="Edit" icon={<PencilIcon />} font="text-regular" />}
            </TableCellNew>
            <TableCellTextNew
              minWidth={
                type === 'fullScreen'
                  ? (tableHead[4].minWidth as TableCellWidth)
                  : (tableHead[5].minWidth as TableCellWidth)
              }
              value={entityOrFieldNameValueByType}
            />
            <TableCellNew minWidth={tableHead[6].minWidth as TableCellWidth} gap="4">
              <Text type="text-regular">{eventDescriptionValueByType}</Text>
              {type === 'fullScreen' && (
                <LinkButton onClick={() => {}} type="text-regular">
                  ID #231213
                </LinkButton>
              )}
            </TableCellNew>
          </TableRowNew>
        ))}
      </TableBodyNew>
    </TableNew>
  );
};

export default AuditLogsTable;
