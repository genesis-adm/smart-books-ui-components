import { Meta, Story } from '@storybook/react';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { Pagination } from 'Components/custom/Stories/Pagination/Pagination';
import PerPageSelect from 'Components/custom/Stories/Pagination/PerPageSelect/PerPageSelect';
import { FilterToggleButton } from 'Components/elements/filters/FilterToggleButton';
import { Search } from 'Components/inputs/Search';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { rowsPerPageOptions } from 'Mocks/fakeOptions';
import AuditLogsFilter from 'Pages/AuditLogs/components/AuditLogsFilter';
import AuditLogsTable from 'Pages/AuditLogs/components/AuditLogsTable';
import { ReactComponent as CSVIcon } from 'Svg/v2/16/file-csv.svg';
import { ReactComponent as XLSIcon } from 'Svg/v2/16/file-xls.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../assets/styles/spacing.module.scss';

const AuditLogsMainPageComponent: Story = () => {
  const [showFilters, setShowFilters] = useState<boolean>(false);
  const [rowsPerPage, setRowsPerPage] = useState<string>(rowsPerPageOptions[0].value);
  return (
    <Modal
      size="full-with-scroll"
      type="new"
      padding="none"
      pluginScrollDisabled
      header={
        <ModalHeaderNew
          onClose={() => {}}
          background={'grey-10'}
          title="Audit log"
          titlePosition="center"
          closeActionBackgroundColor="inherit"
        />
      }
      footer={
        <ModalFooterNew border>
          <Pagination
            borderTop="none"
            padding="0"
            customPagination={
              <Container gap="16" alignitems="center">
                <Text type="subtext-regular" color="grey-100">
                  Rows per Page
                </Text>
                <PerPageSelect
                  selectedValue={rowsPerPage}
                  onOptionClick={(value) => setRowsPerPage(value)}
                />
              </Container>
            }
          />
          <Container>
            <Button
              background={'transparent-blue-10'}
              font="caption-semibold"
              height="40"
              color="grey-100-violet-90"
              iconLeft={<XLSIcon />}
              onClick={() => {}}
            >
              Export to XLS
            </Button>
            <Button
              background={'transparent-blue-10'}
              font="caption-semibold"
              height="40"
              color="grey-100-violet-90"
              iconLeft={<CSVIcon />}
              onClick={() => {}}
            >
              Export to CSV
            </Button>
          </Container>
        </ModalFooterNew>
      }
    >
      <Container
        flexdirection="column"
        borderRadius="10"
        flexgrow="1"
        className={classNames(spacing.w100p, spacing.h100p)}
      >
        <Container gap="24" alignitems="center" className={classNames(spacing.mX24, spacing.mt24)}>
          <FilterToggleButton
            showFilters={showFilters}
            filtersSelected={false}
            onClear={() => {}}
            onClick={() => {
              setShowFilters((prevState) => !prevState);
            }}
          />
          <Search
            width="320"
            height="40"
            name="search-audit-log"
            placeholder="Search by entity ID"
            value=""
            onChange={() => {}}
            searchParams={[]}
            onSearch={() => {}}
            onClear={() => {}}
          />
        </Container>
        <AuditLogsFilter showFilters={showFilters} type="fullScreen" />
        <AuditLogsTable type="fullScreen" />
      </Container>
    </Modal>
  );
};

export default {
  title: 'Pages/Audit logs/ Audit Logs Main Page/AuditLogsMainPage',
  component: AuditLogsMainPageComponent,
} as Meta;

export const AuditLogsMainPage = AuditLogsMainPageComponent.bind({});
