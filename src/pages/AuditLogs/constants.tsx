export const sorting = {
  sortType: null,
  tooltipText: 'Sorting tooltip text',
};
export const auditLogFilters: { title: string; hideOnModalType?: boolean }[] = [
  { title: 'User' },
  { title: 'Event type', hideOnModalType: true },
  { title: 'Event action' },
  { title: 'Entity name', hideOnModalType: true },
];

export const tableHead = [
  { minWidth: '160', title: 'Date & Time', sorting },
  { minWidth: '320', title: 'User', sorting, minWidthOnModalType: '290' },
  { minWidth: '200', title: 'Event type', hideOnModalType: true },
  { minWidth: '170', title: 'Event action', align: 'center' },
  { minWidth: '220', title: 'Entity name', hideOnModalType: true },
  { minWidth: '220', title: 'Field name', hideOnFullScreenType: true },
  { minWidth: '320', title: 'Event description' },
];
