import { Meta, Story } from '@storybook/react';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { Pagination } from 'Components/custom/Stories/Pagination/Pagination';
import PerPageSelect from 'Components/custom/Stories/Pagination/PerPageSelect/PerPageSelect';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { rowsPerPageOptions } from 'Mocks/fakeOptions';
import AuditLogsFilter from 'Pages/AuditLogs/components/AuditLogsFilter';
import AuditLogsTable from 'Pages/AuditLogs/components/AuditLogsTable';
import { ReactComponent as CSVIcon } from 'Svg/v2/16/file-csv.svg';
import { ReactComponent as XLSIcon } from 'Svg/v2/16/file-xls.svg';
import React, { useState } from 'react';

const AuditLogsModalComponent: Story = () => {
  const [rowsPerPage, setRowsPerPage] = useState<string>(rowsPerPageOptions[0].value);

  return (
    <Modal
      size="1288"
      centered
      overlay="black-100"
      padding="none"
      pluginScrollDisabled
      header={
        <ModalHeaderNew
          onClose={() => {}}
          background={'grey-10'}
          title="Logs for Bank transaction ID #231213"
          titlePosition="center"
          closeActionBackgroundColor="inherit"
        />
      }
      footer={
        <ModalFooterNew border>
          <Pagination
            borderTop="none"
            padding="0"
            customPagination={
              <Container gap="16" alignitems="center">
                <Text type="subtext-regular" color="grey-100">
                  Rows per Page
                </Text>
                <PerPageSelect
                  selectedValue={rowsPerPage}
                  onOptionClick={(value) => setRowsPerPage(value)}
                />
              </Container>
            }
          />
          <Container>
            <Button
              background={'transparent-blue-10'}
              font="caption-semibold"
              height="40"
              color="grey-100-violet-90"
              iconLeft={<XLSIcon />}
              onClick={() => {}}
            >
              Export to XLS
            </Button>
            <Button
              background={'transparent-blue-10'}
              font="caption-semibold"
              height="40"
              color="grey-100-violet-90"
              iconLeft={<CSVIcon />}
              onClick={() => {}}
            >
              Export to CSV
            </Button>
          </Container>
        </ModalFooterNew>
      }
    >
      <AuditLogsFilter showFilters type="modal" />
      <AuditLogsTable type="modal" />
    </Modal>
  );
};
export default {
  title: 'Pages/Audit logs/Modals/ AuditLogsModal',
  component: AuditLogsModalComponent,
} as Meta;

export const AuditLogsModal = AuditLogsModalComponent.bind({});
