import { Meta, Story } from '@storybook/react';
import { SelectDropdown } from 'Components/SelectDropdown';
import SelectDropdownBody from 'Components/SelectDropdown/components/SelectDropdownBody/SelectDropdownBody';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { BackdropWrapper } from 'Components/base/BackdropWrapper';
import { Divider } from 'Components/base/Divider';
import { Tag } from 'Components/base/Tag';
import { Button } from 'Components/base/buttons/Button';
import { IconButton } from 'Components/base/buttons/IconButton';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { OptionSingleNew } from 'Components/base/inputs/SelectNew/options/OptionSingleNew';
import { Text } from 'Components/base/typography/Text';
import { LegalEntityTitle } from 'Components/custom/Accounting/ChartOfAccounts/LegalEntityTitle';
import { SelectBD } from 'Components/custom/Accounting/ChartOfAccounts/SelectBD';
import { SelectBDItem } from 'Components/custom/Accounting/ChartOfAccounts/SelectBDItem';
import { SensitiveAccountToggle } from 'Components/custom/Accounting/ChartOfAccounts/SensitiveAccountToggle';
import { MainPageEmpty } from 'Components/custom/MainPage/MainPageEmpty';
import { Select } from 'Components/inputs/Select';
import { TextArea } from 'Components/inputs/TextArea';
import { TextInput } from 'Components/inputs/TextInput';
import { Modal } from 'Components/modules/Modal';
import { options } from 'Mocks/fakeOptions';
import ChartOfAccountsAccordion from 'Pages/Accounting/ChartOfAccountsModals/components/ChartOfAccountsAccordion';
import OpeningBalance from 'Pages/Accounting/ChartOfAccountsModals/components/OpeningBalance';
import { ReactComponent as ArrowLeftIcon } from 'Svg/v2/16/arrow-left.svg';
import { ReactComponent as ActiveIcon } from 'Svg/v2/16/clipboard-tick.svg';
import { ReactComponent as CloseIcon } from 'Svg/v2/16/cross.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as UploadIcon } from 'Svg/v2/16/upload.svg';
import { ReactComponent as ManWithPaperImage } from 'Svg/v2/colored/man-with-docs-2x.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

const ViewChartOfAccountComponent: Story = ({ storyState }) => {
  const [isSensitive, setIsSensitive] = useState<boolean>(true);

  const [addingOpeningBalance, setAddingOpeningBalance] = useState<boolean>(false);
  const [newOpeningBalance, setNewOpeningBalance] = useState<boolean>(false);
  const [editingOpeningBalance, setEditingOpeningBalance] = useState<boolean>(false);

  return (
    <Modal
      size="1040"
      background="grey-10"
      type="new"
      pluginScrollDisabled
      padding="none"
      flexdirection="row"
    >
      <Container
        flexdirection="column"
        background="white-100"
        className={classNames(spacing.w320fixed, spacing.h100p, spacing.p24)}
        gap="16"
      >
        <LegalEntityTitle name="BetterMe Limited" />
        <Divider fullHorizontalWidth />
        <Container flexdirection="column">
          <Text color="grey-100">Choose Business Division</Text>
          <SelectBD nameActiveDB="BetterApp">
            <SelectBDItem name="Business division 1" isSelected onClick={() => {}} />
            <SelectBDItem name="Business division 2" isSelected={false} onClick={() => {}} />
          </SelectBD>
        </Container>
        <Divider fullHorizontalWidth />
        <ChartOfAccountsAccordion />
      </Container>

      <Container gap="24" className={spacing.p24} flexdirection="column" flexgrow="1">
        <Container justifycontent="space-between">
          <Container gap="24" alignitems="center">
            <IconButton
              background="transparent"
              size="24"
              icon={<ArrowLeftIcon />}
              color="grey-100-black-100"
              onClick={() => {}}
            />
            <Text color="grey-100" type="body-medium">
              Chart of Accounts / Asset / ... / Test Name
            </Text>
          </Container>
          <Container gap="16" alignitems="center">
            {storyState === 'loaded' && (
              <Tag
                icon={<ActiveIcon />}
                padding="4-8"
                cursor="default"
                text="Active"
                color="violet"
              />
            )}
            <IconButton
              background="white-100-grey-20"
              size="24"
              icon={<CloseIcon />}
              color="black-100"
              onClick={() => {}}
            />
          </Container>
        </Container>
        {storyState === 'loaded' && (
          <>
            <Container flexdirection="column" justifycontent="space-between" flexgrow="1">
              <Container
                borderRadius="20"
                background="white-100"
                flexdirection="column"
                className={classNames(spacing.p24)}
                gap="24"
              >
                <BackdropWrapper isVisible={addingOpeningBalance || editingOpeningBalance}>
                  <Container flexdirection="column" gap="24" flexgrow="1">
                    <Container gap="24" flexdirection="column" flexgrow="1">
                      <Container justifycontent="space-between">
                        <Text type="body-regular-14">Chart of Account information</Text>
                        <SensitiveAccountToggle
                          isSensitive={isSensitive}
                          onClick={() => setIsSensitive((prev) => !prev)}
                          disabled
                        />
                      </Container>
                      <Container gap="24">
                        <TextInput
                          name="accountName"
                          label="Account name"
                          width="408"
                          value="Test name"
                          required
                          readOnly
                          onChange={() => {}}
                        />
                        <Select
                          width="192"
                          label="Currency"
                          placeholder="Currency"
                          defaultValue={{ value: 'USD', label: 'USD' }}
                          readOnly
                          onChange={() => {}}
                        >
                          {({ onOptionClick, selectValue, selectElement, closeDropdown }) => (
                            <SelectDropdown selectElement={selectElement}>
                              <SelectDropdownBody>
                                <SelectDropdownItemList>
                                  {options.map(({ label, value }) => (
                                    <OptionSingleNew
                                      key={value}
                                      label={label}
                                      selected={selectValue?.value === value}
                                      onClick={() => {
                                        onOptionClick({
                                          label,
                                          value,
                                        });
                                        closeDropdown();
                                      }}
                                    />
                                  ))}
                                </SelectDropdownItemList>
                              </SelectDropdownBody>
                            </SelectDropdown>
                          )}
                        </Select>
                      </Container>
                      <Container gap="24">
                        <Select
                          placeholder="Account type"
                          width="300"
                          label="Account type"
                          defaultValue={{
                            value: 'Cash and cash equivalens',
                            label: 'Cash and cash equivalens',
                          }}
                          onChange={() => {}}
                          required
                          readOnly
                        >
                          {({ onOptionClick, selectElement, selectValue }) => (
                            <SelectDropdown selectElement={selectElement}>
                              <SelectDropdownBody>
                                <SelectDropdownItemList>
                                  <Text
                                    type="caption-semibold"
                                    className={classNames(spacing.mb8, spacing.mt16, spacing.ml8)}
                                    color="grey-100"
                                    display="flex"
                                  >
                                    Current
                                  </Text>

                                  {options.slice(0, 4).map(({ label, value }) => (
                                    <OptionSingleNew
                                      key={value}
                                      label={label}
                                      selected={selectValue?.value === value}
                                      onClick={() =>
                                        onOptionClick({
                                          label,
                                          value,
                                        })
                                      }
                                    />
                                  ))}
                                  <Divider fullHorizontalWidth />
                                  <Text
                                    type="caption-semibold"
                                    className={classNames(spacing.mb8, spacing.mt16, spacing.ml8)}
                                    color="grey-100"
                                    display="flex"
                                  >
                                    Non-current
                                  </Text>

                                  {options.slice(4, 6).map(({ label, value }) => (
                                    <OptionSingleNew
                                      key={value}
                                      label={label}
                                      selected={selectValue?.value === value}
                                      onClick={() =>
                                        onOptionClick({
                                          label,
                                          value,
                                        })
                                      }
                                    />
                                  ))}
                                </SelectDropdownItemList>
                              </SelectDropdownBody>
                            </SelectDropdown>
                          )}
                        </Select>
                        <Select
                          placeholder="Detail type"
                          width="300"
                          label="Detail type"
                          defaultValue={{ value: 'Cash on hand', label: 'Cash on hand' }}
                          required
                          readOnly
                          onChange={() => {}}
                        >
                          {({ onOptionClick, selectValue, selectElement, closeDropdown }) => (
                            <SelectDropdown selectElement={selectElement}>
                              <SelectDropdownBody>
                                <SelectDropdownItemList>
                                  {options.map(({ label, value }) => (
                                    <OptionSingleNew
                                      key={value}
                                      label={label}
                                      selected={selectValue?.value === value}
                                      onClick={() => {
                                        onOptionClick({
                                          label,
                                          value,
                                        });
                                        closeDropdown();
                                      }}
                                    />
                                  ))}
                                </SelectDropdownItemList>
                              </SelectDropdownBody>
                            </SelectDropdown>
                          )}
                        </Select>
                      </Container>
                      <TextArea
                        label="Notes"
                        name="notes"
                        placeholder="Enter"
                        readOnly
                        value=""
                        onChange={() => {}}
                      />
                      {!addingOpeningBalance && !editingOpeningBalance && !newOpeningBalance && (
                        <Container justifycontent="flex-end">
                          <LinkButton
                            icon={<PlusIcon />}
                            onClick={() => {
                              setAddingOpeningBalance(true);
                            }}
                          >
                            Add opening balance
                          </LinkButton>
                        </Container>
                      )}
                      <OpeningBalance
                        addingOpeningBalance={addingOpeningBalance}
                        newOpeningBalance={newOpeningBalance}
                        editingOpeningBalance={editingOpeningBalance}
                        setEditingOpeningBalance={setEditingOpeningBalance}
                        setNewOpeningBalance={setNewOpeningBalance}
                        setAddingOpeningBalance={setAddingOpeningBalance}
                      />
                    </Container>
                  </Container>
                </BackdropWrapper>
              </Container>
            </Container>
            <Divider />
            <Container justifycontent="flex-end">
              <Button
                background="white-100"
                border="grey-20-grey-90"
                color="grey-100"
                width="156"
                height="48"
                onClick={() => {}}
              >
                Close
              </Button>
            </Container>
          </>
        )}
        {storyState === 'empty' && (
          <MainPageEmpty
            description="You have no chart of accounts created. You may create your chart of account list using template accounts"
            action={
              <Container gap="24">
                <Button
                  width="220"
                  height="40"
                  background="blue-10-blue-20"
                  color="violet-90-violet-100"
                  iconLeft={<PlusIcon />}
                  onClick={() => {}}
                >
                  Create Template accounts
                </Button>
                <Button
                  width="220"
                  height="40"
                  background="blue-10-blue-20"
                  color="violet-90-violet-100"
                  iconLeft={<UploadIcon />}
                  onClick={() => {}}
                >
                  Import template accounts
                </Button>
              </Container>
            }
          />
        )}
        {storyState === 'emptyAccountClass' && (
          <Container
            justifycontent="center"
            alignitems="center"
            flexdirection="column"
            gap="16"
            className={classNames(spacing.pY60, spacing.h100p)}
          >
            <ManWithPaperImage />
            <Text type="body-regular-16" color="grey-100" align="center" className={spacing.w230}>
              Please choose Account class to view chart of account
            </Text>
          </Container>
        )}
      </Container>
    </Modal>
  );
};

export default {
  title: 'Pages/Accounting/Chart Of Accounts Modals',
  component: ViewChartOfAccountComponent,
  argTypes: {
    storyState: {
      options: ['empty', 'emptyAccountClass', 'loading', 'loaded'],
      control: {
        type: 'radio',
        labels: {
          empty: 'Empty page',
          emptyAccountClass: 'Empty Account Class',
          loading: 'Page is loading',
          loaded: 'Page loaded',
        },
      },
    },
  },
} as Meta;
export const ViewChartOfAccount = ViewChartOfAccountComponent.bind({});
ViewChartOfAccount.args = {
  storyState: 'loaded',
};
