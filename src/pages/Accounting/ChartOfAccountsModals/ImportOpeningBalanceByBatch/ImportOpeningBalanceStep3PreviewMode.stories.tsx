import { Meta, Story } from '@storybook/react';
import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { Checkbox } from 'Components/base/Checkbox';
import { Divider } from 'Components/base/Divider';
import { Tag } from 'Components/base/Tag';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { InputDate } from 'Components/base/inputs/InputDate';
import { NumericInput } from 'Components/base/inputs/NumericInput';
import { OwnNumberFormatValues } from 'Components/base/inputs/NumericInput/NumericInput.types';
import { Text } from 'Components/base/typography/Text';
import ImportModalFooter from 'Components/custom/Stories/ImportSharedComponents/ImportModalFooter';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { Search } from 'Components/inputs/Search';
import { Select } from 'Components/inputs/Select';
import { OptionWithFourLabels } from 'Components/inputs/Select/options/OptionWithFourLabels';
import { OptionWithSingleLabel } from 'Components/inputs/Select/options/OptionWithSingleLabel';
import { Modal } from 'Components/modules/Modal';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { TableCellWidth } from 'Components/types/gridTypes';
import { options } from 'Mocks/fakeOptions';
import { importOpeningBalanceTableHead } from 'Pages/Accounting/ChartOfAccountsModals/constants';
import { ReactComponent as CrossIcon } from 'Svg/v2/16/cross-in-circle.svg';
import { ReactComponent as TickIcon } from 'Svg/v2/16/tick-in-circle.svg';
import { ReactComponent as UploadIcon } from 'Svg/v2/16/upload.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

const ImportOpeningBalanceStep3PreviewModeComponent: Story = (args) => {
  const containErrorFields = args.containErrorFields;
  const [startDate, setStartDate] = useState(new Date());
  const [openingBalanceAmount, setOpeningBalanceAmount] = useState(10000);
  const [searchValue, setSearchValue] = useState('');

  const inputDateError = containErrorFields
    ? { messages: ['Please add Opening balance date'] }
    : undefined;
  const openingBalanceAmountError = containErrorFields
    ? { messages: ['Please add Opening balance amount'] }
    : undefined;
  const balanceTypeError = containErrorFields
    ? { messages: ['Please choose balance type from list'] }
    : undefined;
  const chartOfAccountNameError = containErrorFields
    ? { messages: ['Check for duplicate accounts'] }
    : undefined;

  const handleOpeningBalanceAmountChange = ({ floatValue }: OwnNumberFormatValues) => {
    setOpeningBalanceAmount(floatValue);
  };
  return (
    <Modal
      overlay="grey-10"
      size="full"
      background={'grey-10'}
      pluginScrollDisabled
      header={
        <ModalHeaderNew
          background={'grey-10'}
          title="Import opening balance"
          icon={
            <Tag
              color="grey"
              text="File name: name file 09.10.20.xls"
              padding="4-8"
              cursor="default"
            />
          }
          border
          closeActionBackgroundColor="inherit"
          onClose={() => {}}
        />
      }
      footer={<ImportModalFooter modalType="previewMode" isActive />}
    >
      <Container flexdirection="column" radius flexgrow="1" overflow="hidden">
        <Container flexdirection="column">
          <Container className={spacing.mt16} alignitems="center" justifycontent="space-between">
            <Text type="body-medium">Opening balance for import:</Text>
            <Container alignitems="center">
              <Text type="text-medium" color="grey-100">
                Total rows:&nbsp;
              </Text>
              <Text type="caption-semibold">3</Text>
              <Divider
                type="vertical"
                height="20"
                background={'grey-90'}
                className={spacing.mX16}
              />
              <Text type="text-medium" color="grey-100">
                Number of valid rows:&nbsp;
              </Text>
              <Text type="caption-semibold" color="green-90">
                2
              </Text>
              <Divider
                type="vertical"
                height="20"
                background={'grey-90'}
                className={spacing.mX16}
              />
              <Text type="text-medium" color="grey-100">
                Number of non valid rows:&nbsp;
              </Text>
              <Text type="caption-semibold" color="red-90">
                0
              </Text>
            </Container>
          </Container>
        </Container>
        <Container flexdirection="column" flexgrow="1" overflow="hidden">
          <TableNew
            noResults={false}
            className={spacing.mt24}
            tableHead={
              <TableHeadNew>
                {importOpeningBalanceTableHead.map((item, index) => (
                  <TableTitleNew
                    key={index}
                    minWidth={item.minWidth as TableCellWidth}
                    padding={item.padding as TableTitleNewProps['padding']}
                    align={item.align as TableTitleNewProps['align']}
                    title={item.title}
                    sorting={item.sorting}
                    onClick={() => {}}
                  />
                ))}
              </TableHeadNew>
            }
          >
            <TableBodyNew>
              {Array.from(Array(39)).map((_, index) => (
                <TableRowNew key={index} background={'white'} size={'52'}>
                  <TableCellNew
                    minWidth={importOpeningBalanceTableHead[0].minWidth as TableCellWidth}
                    justifycontent="center"
                    alignitems="center"
                  >
                    <Checkbox name={`line-${index}`} checked={true} onChange={() => {}} />
                  </TableCellNew>
                  <TableCellTextNew
                    minWidth={importOpeningBalanceTableHead[1].minWidth as TableCellWidth}
                    value="ID 429"
                    type="text-regular"
                  />
                  {containErrorFields ? (
                    <TableCellNew
                      minWidth={importOpeningBalanceTableHead[2].minWidth as TableCellWidth}
                      fixedWidth
                    >
                      <Select
                        label="Chart of account name"
                        placeholder="Select option"
                        height="36"
                        width="200"
                        error={chartOfAccountNameError}
                      >
                        {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
                          <SelectDropdown selectElement={selectElement} width="408">
                            <SelectDropdownBody>
                              <Search
                                className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                                width="full"
                                height="40"
                                name="search"
                                placeholder="Search option"
                                value={searchValue}
                                onChange={(e) => setSearchValue(e.target.value)}
                                searchParams={[]}
                                onSearch={() => {}}
                                onClear={() => {}}
                              />
                              <SelectDropdownItemList>
                                {options.map((value, index) => (
                                  <OptionWithFourLabels
                                    key={index}
                                    label={value?.label}
                                    secondaryLabel={value.subLabel}
                                    tertiaryLabel={value.tertiaryLabel}
                                    selected={selectValue === value}
                                    onClick={() => {
                                      onOptionClick(value);
                                      closeDropdown();
                                    }}
                                  />
                                ))}
                              </SelectDropdownItemList>
                            </SelectDropdownBody>
                          </SelectDropdown>
                        )}
                      </Select>
                    </TableCellNew>
                  ) : (
                    <TableCellTextNew
                      minWidth={importOpeningBalanceTableHead[2].minWidth as TableCellWidth}
                      value="Chart of account namefhwuifhwiuhgwuhuhgighwuthrhtrht"
                      type="text-regular"
                      fixedWidth
                    />
                  )}
                  <TableCellTextNew
                    minWidth={importOpeningBalanceTableHead[3].minWidth as TableCellWidth}
                    value="USD"
                    type="text-regular"
                    alignitems="center"
                  />
                  <TableCellTextNew
                    minWidth={importOpeningBalanceTableHead[4].minWidth as TableCellWidth}
                    value="Business unit name fgwieugwuigfwiuirghweiu"
                    type="text-regular"
                    fixedWidth
                  />
                  <TableCellTextNew
                    minWidth={importOpeningBalanceTableHead[5].minWidth as TableCellWidth}
                    value="Legal entity name yggygyugugygkgkhighygyg"
                    type="text-regular"
                    fixedWidth
                  />
                  {!containErrorFields ? (
                    <TableCellTextNew
                      minWidth={importOpeningBalanceTableHead[6].minWidth as TableCellWidth}
                      value="Debit"
                      type="text-regular"
                    />
                  ) : (
                    <TableCellNew
                      minWidth={importOpeningBalanceTableHead[6].minWidth as TableCellWidth}
                      fixedWidth
                    >
                      <Select
                        label="Balance type"
                        placeholder="Select option"
                        height="36"
                        type="column"
                        width="170"
                        error={balanceTypeError}
                      >
                        {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
                          <SelectDropdown selectElement={selectElement}>
                            <SelectDropdownBody>
                              <Search
                                className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                                width="full"
                                height="40"
                                name="search"
                                placeholder="Search option"
                                value={searchValue}
                                onChange={(e) => setSearchValue(e.target.value)}
                                searchParams={[]}
                                onSearch={() => {}}
                                onClear={() => {}}
                              />
                              <SelectDropdownItemList>
                                {options.map((value, index) => (
                                  <OptionWithSingleLabel
                                    id={value.label}
                                    key={index}
                                    label={value?.label}
                                    selected={
                                      !Array.isArray(selectValue) &&
                                      selectValue?.value === value.value
                                    }
                                    onClick={() => {
                                      onOptionClick({
                                        value: value?.value,
                                        label: value?.label,
                                      });
                                      closeDropdown();
                                    }}
                                  />
                                ))}
                              </SelectDropdownItemList>
                            </SelectDropdownBody>
                          </SelectDropdown>
                        )}
                      </Select>
                    </TableCellNew>
                  )}
                  <TableCellNew
                    minWidth={importOpeningBalanceTableHead[7].minWidth as TableCellWidth}
                    alignitems="flex-end"
                    fixedWidth
                  >
                    <NumericInput
                      name="num-1"
                      label=""
                      height="36"
                      width="114"
                      value={!containErrorFields ? openingBalanceAmount : null}
                      onValueChange={handleOpeningBalanceAmountChange}
                      suffix="$"
                      background={'grey-10'}
                      error={openingBalanceAmountError}
                    />
                  </TableCellNew>
                  <TableCellNew
                    minWidth={importOpeningBalanceTableHead[8].minWidth as TableCellWidth}
                    fixedWidth
                  >
                    <InputDate
                      name="inputdate"
                      placeholder="dd.mm.yyyy"
                      label="Date"
                      width="120"
                      height="36"
                      selected={!containErrorFields ? startDate : null}
                      onChange={(date: Date) => setStartDate(date)}
                      calendarStartDay={1}
                      error={inputDateError}
                    />
                  </TableCellNew>
                  <TableCellNew
                    minWidth={importOpeningBalanceTableHead[9].minWidth as TableCellWidth}
                    justifycontent="center"
                    alignitems="center"
                  >
                    {!containErrorFields ? (
                      <Tag icon={<TickIcon />} color="green" />
                    ) : (
                      <Tag icon={<CrossIcon />} color="red" />
                    )}
                  </TableCellNew>
                  <TableCellNew
                    minWidth={importOpeningBalanceTableHead[10].minWidth as TableCellWidth}
                    alignitems="center"
                    justifycontent="center"
                  >
                    <Button
                      height="24"
                      font="text-medium"
                      onClick={() => {}}
                      iconLeft={<UploadIcon />}
                      background={'blue-10-blue-20'}
                      border="blue-10-blue-20"
                      color="violet-90-violet-100"
                    >
                      Import
                    </Button>
                  </TableCellNew>
                </TableRowNew>
              ))}
            </TableBodyNew>
          </TableNew>
        </Container>
      </Container>
    </Modal>
  );
};

export default {
  title: 'Pages/Accounting/Chart Of Accounts Modals/Import OpeningBalance by Batch',
  component: ImportOpeningBalanceStep3PreviewModeComponent,
  argTypes: {
    containErrorFields: { type: 'boolean' },
  },
  args: { containErrorFields: false },
} as Meta;
export const ImportOpeningBalanceStep3PreviewMode =
  ImportOpeningBalanceStep3PreviewModeComponent.bind({});
