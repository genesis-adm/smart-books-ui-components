import { Meta, Story } from '@storybook/react';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import ImportModalFooter from 'Components/custom/Stories/ImportSharedComponents/ImportModalFooter';
import UploadFile from 'Components/custom/Stories/Upload/UploadFile';
import UploadInstruction from 'Components/custom/Stories/Upload/UploadInstruction';
import { Modal } from 'Components/modules/Modal';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import React, { useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

const ImportOpeningBalanceStep1Component: Story = () => {
  const [file, setFile] = useState<File | null>(null);
  return (
    <Modal
      size="md-extrawide"
      padding="none"
      header={<ModalHeaderNew onClose={() => {}} closeActionBackgroundColor="grey-10" />}
      footer={<ImportModalFooter isActive={!!file} modalType="upload" />}
    >
      <Container flexdirection="column" alignitems="center">
        <Text type="h2-semibold" align="center">
          Import opening balance
        </Text>
        <UploadFile file={file} setFile={setFile} className={spacing.mt40} />
        <UploadInstruction />
      </Container>
    </Modal>
  );
};

export default {
  title: 'Pages/Accounting/Chart Of Accounts Modals/Import OpeningBalance by Batch',
  component: ImportOpeningBalanceStep1Component,
} as Meta;
export const ImportOpeningBalanceStep1 = ImportOpeningBalanceStep1Component.bind({});
