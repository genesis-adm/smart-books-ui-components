import { Checkbox } from 'Components/base/Checkbox';
import React from 'react';

const sorting = {
  sortType: null,
  tooltipText: 'Sorting tooltip text',
};
export const importOpeningBalanceTableHead = [
  {
    minWidth: '48',
    title: <Checkbox name="tickAll" checked tick="unselect" onChange={() => {}} />,
    padding: 'none',
  },
  {
    minWidth: '160',
    title: 'Chart of account ID',
    sorting,
  },
  {
    minWidth: '220',
    title: 'Chart of account name',
    sorting,
  },
  {
    minWidth: '100',
    title: 'Currency',
    sorting,
    align: 'center',
  },
  {
    minWidth: '200',
    title: 'Business unit name',
    sorting,
  },
  {
    minWidth: '180',
    title: 'Legal entity name',
    sorting,
  },
  {
    minWidth: '180',
    title: 'Balance type',
    sorting,
  },
  {
    minWidth: '155',
    title: 'Opening Balance',
    sorting,
    align: 'right',
  },
  {
    minWidth: '155',
    title: 'Date',
    sorting,
  },
  {
    minWidth: '110',
    title: 'Status',
    sorting,
    align: 'center',
  },
  {
    minWidth: '115',
    title: 'Actions',
    sorting,
    align: 'center',
  },
];
