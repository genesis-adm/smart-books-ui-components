import { Meta } from '@storybook/react';
import { DropDownItem } from 'Components/DropDownItem';
import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { Checkbox } from 'Components/base/Checkbox';
import { Divider } from 'Components/base/Divider';
import { Radio } from 'Components/base/Radio';
import { Tooltip } from 'Components/base/Tooltip';
import { TooltipIcon } from 'Components/base/TooltipIcon';
import { Button } from 'Components/base/buttons/Button';
import { ButtonGroup } from 'Components/base/buttons/ButtonGroup';
import { IconButton } from 'Components/base/buttons/IconButton';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { InputNew } from 'Components/base/inputs/InputNew';
import { OptionSingleNew } from 'Components/base/inputs/SelectNew/options/OptionSingleNew';
import { Text } from 'Components/base/typography/Text';
import { AccountClassType } from 'Components/custom/Accounting/ChartOfAccounts/AccountClassType';
import { LegalEntityTitle } from 'Components/custom/Accounting/ChartOfAccounts/LegalEntityTitle';
import { SelectBD } from 'Components/custom/Accounting/ChartOfAccounts/SelectBD';
import { SelectBDItem } from 'Components/custom/Accounting/ChartOfAccounts/SelectBDItem';
import { UploadContainer } from 'Components/custom/Upload/UploadContainer';
import { UploadInfo } from 'Components/custom/Upload/UploadInfo';
import { UploadItemField } from 'Components/custom/Upload/UploadItemField';
import { ProgressBar } from 'Components/elements/ProgressBar';
import { DescriptionTooltip } from 'Components/graphics/DescriptionTooltip';
import { Select } from 'Components/inputs/Select';
import { Modal } from 'Components/modules/Modal';
import ModalFooter from 'Components/modules/Modal/ModalFooter';
import ModalHeader from 'Components/modules/Modal/ModalHeader';
import { Input } from 'Components/old/Input';
import { Table } from 'Components/old/Table';
import { TableBody } from 'Components/old/Table/TableBody';
import { TableCell } from 'Components/old/Table/TableCell';
import { TableHead } from 'Components/old/Table/TableHead';
import { TableRow } from 'Components/old/Table/TableRow';
import { TableTitle } from 'Components/old/Table/TableTitle';
import { options } from 'Mocks/fakeOptions';
import ChartOfAccountsAccordion from 'Pages/Accounting/ChartOfAccountsModals/components/ChartOfAccountsAccordion';
import { ReactComponent as QuestionIcon } from 'Svg/16/question.svg';
import { ReactComponent as ArrowLeftIcon } from 'Svg/v2/16/arrow-left.svg';
import { ReactComponent as ArrowRightIcon } from 'Svg/v2/16/arrow-right.svg';
import { ReactComponent as CloseIcon } from 'Svg/v2/16/cross.svg';
import { checkFileType } from 'Utils/fileHandlers';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Accounting/Chart Of Accounts Modals',
} as Meta;

interface Information {
  id: string;
  division: string;
  legalEntity: string;
  expensesType: string;
  expensesDetail: string;
  currency: string;
  description: string;
  sensitive: boolean;
  openingBalance: string;
  date: string;
}

const data: Information[] = [
  {
    id: '1',
    division: 'Business division name',
    legalEntity: 'Legal entity name',
    expensesType: 'Expenses',
    expensesDetail: 'Supplies & supplies',
    currency: 'USD',
    description: 'This is a tooltip text',
    sensitive: true,
    openingBalance: '1000,000.00',
    date: '08 July 2021',
  },
  {
    id: '2',
    division: 'Business division name',
    legalEntity: 'Legal entity name',
    expensesType: 'Expenses',
    expensesDetail: 'Supplies & supplies',
    currency: 'USD',
    description: 'This is a tooltip text',
    sensitive: true,
    openingBalance: '1000,000.00',
    date: '08 July 2021',
  },
  {
    id: '3',
    division: 'Business division name',
    legalEntity: 'Legal entity name',
    expensesType: 'Expenses',
    expensesDetail: 'Supplies & supplies',
    currency: 'USD',
    description: 'This is a tooltip text',
    sensitive: true,
    openingBalance: '1000,000.00',
    date: '08 July 2021',
  },
  {
    id: '4',
    division: 'Business division name',
    legalEntity: 'Legal entity name',
    expensesType: 'Expenses',
    expensesDetail: 'Supplies & supplies',
    currency: 'USD',
    description: 'This is a tooltip text',
    sensitive: true,
    openingBalance: '1000,000.00',
    date: '08 July 2021',
  },
];

export const ImportChartOfAccountsStep1: React.FC = () => {
  const [file, setFile] = useState<File | null>(null);

  const handleFileUpload = (item: File) => {
    setFile(item);
  };
  const handleFileDelete = () => {
    setFile(null);
  };
  const handleFileDrop = (e: React.DragEvent<HTMLElement>) => {
    const dropFile = e.dataTransfer.files[0];
    if (checkFileType(dropFile, ['xls', 'xlsx', 'csv'])) {
      setFile(e.dataTransfer.files[0]);
    }
  };
  return (
    <Modal
      size="full"
      background="grey-10"
      header={
        <ModalHeader type="fullwidth" backgroundColor="white-100" title="Import bank accounts" />
      }
      justifycontent="center"
      alignitems="center"
      footer={
        <ModalFooter justifycontent="space-between">
          <ProgressBar steps={2} activeStep={1} />
          <ButtonGroup align="right">
            <Button
              width="sm"
              background="white-100"
              color="black-100"
              border="grey-20-grey-90"
              onClick={() => {}}
            >
              Cancel
            </Button>
            <Button navigation iconRight={<ArrowRightIcon />} width="md" onClick={() => {}}>
              Next
            </Button>
          </ButtonGroup>
        </ModalFooter>
      }
    >
      <Container
        justifycontent="center"
        alignitems="center"
        flexdirection="column"
        className={spacing.w400}
      >
        <Text type="title-bold" align="center">
          Upload a file with your chart of account
        </Text>
        <Text type="body-regular-16" color="grey-100" align="center" className={spacing.mt24}>
          First time importing your chart of account?
        </Text>
        <Text type="body-regular-16" color="grey-100" align="center">
          Please&nbsp;
          <LinkButton type="body-regular-16" onClick={() => {}}>
            download XLS template
          </LinkButton>
          , fill it with your data and upload file below
        </Text>
        <UploadContainer onDrop={handleFileDrop} className={spacing.mt16}>
          <UploadItemField
            name="uploader"
            files={file}
            acceptFilesType="csv"
            onChange={handleFileUpload}
            onDelete={handleFileDelete}
          />
          <UploadInfo
            mainText={file ? file.name : 'Drag and drop or Choose file'}
            secondaryText="CSV; Maximum file size is 5MB"
          />
        </UploadContainer>
        <Text className={spacing.mt10} color="grey-100">
          Maximum file size is 5MB
        </Text>
      </Container>
    </Modal>
  );
};

export const ImportChartOfAccountsStep2: React.FC = () => (
  <Modal
    size="full"
    overlay="grey-10"
    background="grey-10"
    header={
      <ModalHeader type="fullwidth" backgroundColor="white-100" title="Import bank accounts" />
    }
    footer={
      <ModalFooter justifycontent="space-between">
        <ProgressBar steps={2} activeStep={2} />
        <ButtonGroup align="right">
          <Button
            width="sm"
            navigation
            iconLeft={<ArrowLeftIcon />}
            background="white-100"
            color="black-100"
            border="grey-20-grey-90"
            onClick={() => {}}
          >
            Back
          </Button>
          <Button width="md" onClick={() => {}}>
            Save and close
          </Button>
        </ButtonGroup>
      </ModalFooter>
    }
  >
    <Container flexdirection="column" className={spacing.mX30}>
      <Text type="body-medium" className={spacing.mt32}>
        Please add account class to sem vel porttitor tempor
      </Text>
      <Container className={spacing.mt6}>
        <Text type="text-regular">File name:&nbsp;</Text>
        <LinkButton type="text-regular" onClick={() => {}}>
          Accounts for import.xls
        </LinkButton>
      </Container>
      <Container
        className={classNames(spacing.h50min, spacing.mt16)}
        justifycontent="space-between"
      >
        <Container alignitems="center" gap="24">
          <Container>
            <Text type="text-medium">Accounts chosen:&nbsp;</Text>
            <Text type="caption-semibold">145</Text>
          </Container>
          <Select
            height="36"
            background="transparent"
            width="215"
            onChange={() => {}}
            placeholder="Choose class"
            label="Choose class"
          >
            {({ onOptionClick, selectValue, selectElement }) => (
              <SelectDropdown selectElement={selectElement}>
                <SelectDropdownBody>
                  <SelectDropdownItemList>
                    {options.map(({ label, value }) => (
                      <OptionSingleNew
                        key={value}
                        label={label}
                        selected={selectValue?.value === value}
                        onClick={() =>
                          onOptionClick({
                            label,
                            value,
                          })
                        }
                      />
                    ))}
                  </SelectDropdownItemList>
                </SelectDropdownBody>
              </SelectDropdown>
            )}
          </Select>
        </Container>
        <Container alignitems="center">
          <Text type="text-medium">Number of accounts:&nbsp;</Text>
          <Text type="caption-semibold">145</Text>
          <Divider type="vertical" className={spacing.mX16} />
          <Text type="text-medium" color="red-90">
            Not valid:&nbsp;
          </Text>
          <Text type="caption-semibold" color="red-90">
            144
          </Text>
        </Container>
      </Container>
    </Container>
    <Container flexgrow="1" className={spacing.pX30}>
      <Container className={classNames(spacing.w100p, spacing.mt24)}>
        <Table
          tableHead={
            <TableHead margin="none">
              <TableTitle minWidth="60" onClick={() => {}}>
                <Checkbox name="tickAll" checked onChange={() => {}} />
              </TableTitle>
              <TableTitle minWidth="160" title="Account class" sortIcon onClick={() => {}} />
              <TableTitle minWidth="190" title="Account name" sortIcon onClick={() => {}} />
              <TableTitle minWidth="150" title="Account type" sortIcon onClick={() => {}} />
              <TableTitle minWidth="190" title="Account detail type" sortIcon onClick={() => {}} />
              <TableTitle minWidth="90" title="Currency" onClick={() => {}} />
              <TableTitle minWidth="90" title="Description" onClick={() => {}} />
              <TableTitle
                minWidth="220"
                title="Business division & Legal entity"
                sortIcon
                onClick={() => {}}
              />
              <TableTitle minWidth="120" title="Opening balance" onClick={() => {}} />
              <TableTitle minWidth="120" title="as of date" sortIcon onClick={() => {}} />
            </TableHead>
          }
        >
          <TableBody margin="none">
            {data.map(({ id, ...item }) => (
              <TableRow key={id}>
                <TableCell minWidth="60" justifycontent="center" alignitems="center">
                  <Checkbox name={`line-${id}`} checked onChange={() => {}} />
                </TableCell>
                <TableCell minWidth="160" justifycontent="center">
                  <Select
                    height="36"
                    background="transparent"
                    error={{ messages: ['Error'] }}
                    onChange={() => {}}
                    width="full"
                    placeholder="Choose class"
                    label="Choose class"
                  >
                    {({ onOptionClick, selectValue, selectElement }) => (
                      <SelectDropdown selectElement={selectElement}>
                        <SelectDropdownBody>
                          <SelectDropdownItemList>
                            {options.map(({ label, value }) => (
                              <OptionSingleNew
                                key={value}
                                label={label}
                                selected={selectValue?.value === value}
                                onClick={() =>
                                  onOptionClick({
                                    label,
                                    value,
                                  })
                                }
                              />
                            ))}
                          </SelectDropdownItemList>
                        </SelectDropdownBody>
                      </SelectDropdown>
                    )}
                  </Select>
                </TableCell>
                <TableCell minWidth="190" justifycontent="center">
                  <Text type="caption-regular" display="block">
                    {item.legalEntity}
                  </Text>
                </TableCell>
                <TableCell minWidth="150" justifycontent="center">
                  <Text type="caption-regular" display="block">
                    {item.legalEntity}
                  </Text>
                </TableCell>
                <TableCell minWidth="190" justifycontent="center">
                  <Text type="caption-regular" display="block">
                    {item.legalEntity}
                  </Text>
                </TableCell>
                <TableCell minWidth="90" justifycontent="center">
                  <Text type="caption-regular" align="center">
                    {item.currency}
                  </Text>
                </TableCell>
                <TableCell minWidth="90" alignitems="center" justifycontent="center">
                  <Tooltip message={item.description}>
                    <DescriptionTooltip />
                  </Tooltip>
                </TableCell>
                <TableCell minWidth="220" justifycontent="center">
                  <Text type="caption-regular" display="block">
                    {item.division}
                  </Text>
                  <Text type="text-regular" display="block" color="grey-100">
                    {item.legalEntity}
                  </Text>
                </TableCell>
                <TableCell minWidth="120" justifycontent="center">
                  <Text align="right">{item.currency}</Text>
                </TableCell>
                <TableCell minWidth="120" justifycontent="center">
                  <Text color="grey-100">{item.currency}</Text>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Container>
    </Container>
  </Modal>
);

export const ImportCreateAccount: React.FC = () => (
  <Modal
    size="md-narrow"
    centered
    overlay="black-100"
    header={
      <ModalHeader
        backgroundColor="grey-10"
        headerHeight="medium"
        title="Create account"
        titlePosition="center"
        onClick={() => {}}
      />
    }
    footer={
      <ModalFooter justifycontent="center">
        <ButtonGroup align="center">
          <Button
            width="md"
            height="large"
            background="grey-20"
            color="black-100"
            border="grey-20-grey-90"
            onClick={() => {}}
          >
            Cancel
          </Button>
          <Button
            width="md"
            height="large"
            onClick={() => {}}
            dropdownBorder="white-100"
            dropdown={
              <>
                <DropDownItem type="button" onClick={() => {}}>
                  Save and add new
                </DropDownItem>
              </>
            }
          >
            Save and close
          </Button>
        </ButtonGroup>
      </ModalFooter>
    }
  >
    <Text className={spacing.mt32} type="body-medium" align="center">
      Please setup general info
    </Text>
    <Input
      className={spacing.mt32}
      width="full"
      type="text"
      name="accountname"
      placeholder="Enter name"
      label="Account name"
      isRequired
    />
    <Checkbox className={spacing.mt16} name="sensitiveaccount" checked={false} onChange={() => {}}>
      This account is sensitive
    </Checkbox>
    <Container className={spacing.mY16} justifycontent="space-between">
      <Select
        onChange={() => {}}
        placeholder="Choose option"
        width="230"
        label="Account type"
        required
      >
        {({ onOptionClick, selectValue, selectElement }) => (
          <SelectDropdown selectElement={selectElement}>
            <SelectDropdownBody>
              <SelectDropdownItemList>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={selectValue?.value === value}
                    onClick={() =>
                      onOptionClick({
                        label,
                        value,
                      })
                    }
                  />
                ))}
              </SelectDropdownItemList>
            </SelectDropdownBody>
          </SelectDropdown>
        )}
      </Select>
      <Select
        onChange={() => {}}
        placeholder="Choose option"
        width="230"
        label="Detail type"
        required
      >
        {({ onOptionClick, selectElement, selectValue }) => (
          <SelectDropdown selectElement={selectElement}>
            <SelectDropdownBody>
              <SelectDropdownItemList>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={selectValue?.value === value}
                    onClick={() =>
                      onOptionClick({
                        label,
                        value,
                      })
                    }
                  />
                ))}
              </SelectDropdownItemList>
            </SelectDropdownBody>
          </SelectDropdown>
        )}
      </Select>
    </Container>
    <Select
      onChange={() => {}}
      placeholder="Choose option"
      width="full"
      label="Choose currency"
      required
    >
      {({ onOptionClick, selectValue, selectElement }) => (
        <SelectDropdown selectElement={selectElement}>
          <SelectDropdownBody>
            <SelectDropdownItemList>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={selectValue?.value === value}
                  onClick={() =>
                    onOptionClick({
                      label,
                      value,
                    })
                  }
                />
              ))}
            </SelectDropdownItemList>
          </SelectDropdownBody>
        </SelectDropdown>
      )}
    </Select>
    <Checkbox className={spacing.mY16} name="subaccount" checked={false} onChange={() => {}}>
      This account is sub-account
    </Checkbox>
    <Container gap="16" flexdirection="column">
      <Select
        onChange={() => {}}
        placeholder="Choose option"
        width="full"
        label="Choose business division"
        required
      >
        {({ onOptionClick, selectValue, selectElement }) => (
          <SelectDropdown selectElement={selectElement}>
            <SelectDropdownBody>
              <SelectDropdownItemList>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={selectValue?.value === value}
                    onClick={() =>
                      onOptionClick({
                        label,
                        value,
                      })
                    }
                  />
                ))}
              </SelectDropdownItemList>
            </SelectDropdownBody>
          </SelectDropdown>
        )}
      </Select>
      <Select
        onChange={() => {}}
        placeholder="Choose option"
        width="full"
        label="Choose legal entity"
        required
      >
        {({ onOptionClick, selectValue, selectElement }) => (
          <SelectDropdown selectElement={selectElement}>
            <SelectDropdownBody>
              <SelectDropdownItemList>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={selectValue?.value === value}
                    onClick={() =>
                      onOptionClick({
                        label,
                        value,
                      })
                    }
                  />
                ))}
              </SelectDropdownItemList>
            </SelectDropdownBody>
          </SelectDropdown>
        )}
      </Select>
    </Container>
    <Input
      className={spacing.mt16}
      width="full"
      type="text"
      name="description"
      placeholder="Enter text"
      label="Description"
      isRequired
    />
    <Container className={spacing.mt16} justifycontent="space-between">
      <Input name="balance" width="modal-sm" placeholder="Enter balance" label="Balance" />
      <Input
        name="asofdate"
        width="modal-sm"
        placeholder="Enter date YYYY-MM-DD"
        label="as of"
        isRequired
      />
    </Container>
    <Checkbox
      className={classNames(spacing.mt16, spacing.mb32)}
      name="bankaccount"
      checked={false}
      onChange={() => {}}
    >
      This is a bank account
    </Checkbox>
  </Modal>
);

export const ImportCreateAccountBankAccount: React.FC = () => (
  <Modal
    size="md-narrow"
    centered
    overlay="black-100"
    header={
      <ModalHeader
        backgroundColor="grey-10"
        headerHeight="medium"
        title="Create account"
        titlePosition="center"
        onClick={() => {}}
      />
    }
    footer={
      <ModalFooter justifycontent="center">
        <ButtonGroup align="center">
          <Button
            width="md"
            height="large"
            background="grey-20"
            color="black-100"
            border="grey-20-grey-90"
            onClick={() => {}}
          >
            Cancel
          </Button>
          <Button
            width="md"
            navigation
            height="large"
            iconRight={<ArrowRightIcon />}
            onClick={() => {}}
          >
            Next
          </Button>
        </ButtonGroup>
      </ModalFooter>
    }
  >
    <Text className={spacing.mt32} type="body-medium" align="center">
      Please setup general info
    </Text>
    <InputNew
      className={spacing.mt32}
      width="full"
      name="accountname"
      placeholder="Enter name"
      label="Account name"
      required
    />
    <Checkbox className={spacing.mt16} name="sensitiveaccount" checked={false} onChange={() => {}}>
      This account is sensitive
    </Checkbox>
    <Container className={spacing.mY16} justifycontent="space-between">
      <Select
        onChange={() => {}}
        placeholder="Choose option"
        width="230"
        label="Account type"
        required
      >
        {({ onOptionClick, selectValue, selectElement }) => (
          <SelectDropdown selectElement={selectElement}>
            <SelectDropdownBody>
              <SelectDropdownItemList>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={selectValue?.value === value}
                    onClick={() =>
                      onOptionClick({
                        label,
                        value,
                      })
                    }
                  />
                ))}
              </SelectDropdownItemList>
            </SelectDropdownBody>
          </SelectDropdown>
        )}
      </Select>
      <Select
        onChange={() => {}}
        placeholder="Choose option"
        width="230"
        label="Detail type"
        required
      >
        {({ onOptionClick, selectValue, selectElement }) => (
          <SelectDropdown selectElement={selectElement}>
            <SelectDropdownBody>
              <SelectDropdownItemList>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={selectValue?.value === value}
                    onClick={() =>
                      onOptionClick({
                        label,
                        value,
                      })
                    }
                  />
                ))}
              </SelectDropdownItemList>
            </SelectDropdownBody>
          </SelectDropdown>
        )}
      </Select>
    </Container>
    <Select
      onChange={() => {}}
      placeholder="Choose option"
      width="full"
      label="Choose currency"
      required
    >
      {({ onOptionClick, selectValue, selectElement }) => (
        <SelectDropdown selectElement={selectElement}>
          <SelectDropdownBody>
            <SelectDropdownItemList>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={selectValue?.value === value}
                  onClick={() =>
                    onOptionClick({
                      label,
                      value,
                    })
                  }
                />
              ))}
            </SelectDropdownItemList>
          </SelectDropdownBody>
        </SelectDropdown>
      )}
    </Select>
    <Checkbox className={spacing.mY16} name="subaccount" checked onChange={() => {}}>
      This account is sub-account
    </Checkbox>
    <Container flexdirection="column" gap="16">
      <Select
        onChange={() => {}}
        placeholder="Choose option"
        width="full"
        label="Choose parent account"
        required
      >
        {({ onOptionClick, selectValue, selectElement }) => (
          <SelectDropdown selectElement={selectElement}>
            <SelectDropdownBody>
              <SelectDropdownItemList>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={selectValue?.value === value}
                    onClick={() =>
                      onOptionClick({
                        label,
                        value,
                      })
                    }
                  />
                ))}
              </SelectDropdownItemList>
            </SelectDropdownBody>
          </SelectDropdown>
        )}
      </Select>
      <Select
        onChange={() => {}}
        placeholder="Choose option"
        width="full"
        label="Choose business division"
        required
      >
        {({ onOptionClick, selectValue, selectElement }) => (
          <SelectDropdown selectElement={selectElement}>
            <SelectDropdownBody>
              <SelectDropdownItemList>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={selectValue?.value === value}
                    onClick={() =>
                      onOptionClick({
                        label,
                        value,
                      })
                    }
                  />
                ))}
              </SelectDropdownItemList>
            </SelectDropdownBody>
          </SelectDropdown>
        )}
      </Select>
      <Select
        onChange={() => {}}
        placeholder="Choose option"
        width="full"
        label="Choose legal entity"
        required
      >
        {({ onOptionClick, selectValue, selectElement }) => (
          <SelectDropdown selectElement={selectElement}>
            <SelectDropdownBody>
              <SelectDropdownItemList>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={selectValue?.value === value}
                    onClick={() =>
                      onOptionClick({
                        label,
                        value,
                      })
                    }
                  />
                ))}
              </SelectDropdownItemList>
            </SelectDropdownBody>
          </SelectDropdown>
        )}
      </Select>
    </Container>
    <Input
      className={spacing.mt16}
      width="full"
      type="text"
      name="description"
      placeholder="Enter text"
      label="Description"
      isRequired
    />
    <Container className={spacing.mt16} justifycontent="space-between">
      <Input name="balance" width="modal-sm" placeholder="Enter balance" label="Balance" />
      <Input
        name="asofdate"
        width="modal-sm"
        placeholder="Enter date YYYY-MM-DD"
        label="as of"
        isRequired
      />
    </Container>
    <Checkbox
      className={classNames(spacing.mt16, spacing.mb32)}
      name="bankaccount"
      checked
      onChange={() => {}}
    >
      This is a bank account
    </Checkbox>
  </Modal>
);

export const ImportCreateAccountAddBankInformationBankType: React.FC = () => (
  <Modal
    size="md-narrow"
    centered
    overlay="black-100"
    header={
      <ModalHeader
        backgroundColor="grey-10"
        headerHeight="medium"
        title="Create account"
        titlePosition="center"
        onClick={() => {}}
      />
    }
    footer={
      <ModalFooter justifycontent="center">
        <ButtonGroup align="center">
          <Button
            width="md"
            navigation
            iconLeft={<ArrowLeftIcon />}
            height="large"
            background="grey-20"
            color="black-100"
            border="grey-20-grey-90"
            onClick={() => {}}
          >
            Back
          </Button>
          <Button
            width="md"
            height="large"
            onClick={() => {}}
            dropdownBorder="white-100"
            dropdown={
              <>
                <DropDownItem type="button" onClick={() => {}}>
                  Save and add new
                </DropDownItem>
              </>
            }
          >
            Save and close
          </Button>
        </ButtonGroup>
      </ModalFooter>
    }
  >
    <Container flexdirection="column" className={classNames(spacing.mb32)}>
      <Text className={spacing.mt32} type="body-medium" align="center">
        Add bank information
      </Text>
      <Container className={spacing.mt16} alignitems="center">
        <Text type="caption-regular" className={spacing.mr10}>
          Type account
          <Text type="caption-regular" color="red-90">
            *
          </Text>
        </Text>
        <TooltipIcon
          icon={<QuestionIcon />}
          size="16"
          message={
            <Text display="block" color="white-100">
              Tooltip text
            </Text>
          }
        />
      </Container>
      <Container className={spacing.mt10}>
        <Radio id="bank" onChange={() => {}} name="accountType" checked>
          Bank
        </Radio>
        <Radio
          className={spacing.ml30}
          id="cash"
          onChange={() => {}}
          name="accountType"
          checked={false}
        >
          Cash
        </Radio>
        <Radio
          className={spacing.ml30}
          id="card"
          onChange={() => {}}
          name="accountType"
          checked={false}
        >
          Card
        </Radio>
      </Container>
      <Input
        className={spacing.mt16}
        width="full"
        type="text"
        name="bankname"
        placeholder="Enter name"
        label="Bank name"
        isRequired
      />
      <Input
        className={spacing.mt16}
        width="full"
        type="text"
        name="swiftcode"
        placeholder="Enter SWIFT code"
        label="SWIFT code / ABA / SEPA"
        isRequired
      />
      <Input
        className={spacing.mt16}
        width="full"
        type="text"
        name="bankaddress"
        placeholder="Enter bank address"
        label="Bank address"
        isRequired
      />
      <Text className={spacing.mt24} type="body-regular-14" color="grey-100">
        Account information:
      </Text>
      <Input
        className={spacing.mt16}
        width="full"
        type="text"
        name="iban"
        placeholder="Enter IBAN number"
        label="IBAN number"
        isRequired
      />
      <Input
        className={spacing.mt16}
        width="full"
        type="text"
        name="accountnumber"
        placeholder="Enter account number"
        label="Account number"
        isRequired
      />
      <Input
        className={spacing.mt16}
        width="full"
        type="text"
        name="holdername"
        placeholder="Enter holder name"
        label="Account holder name"
      />
      <Input
        className={spacing.mt16}
        width="full"
        type="text"
        name="holderaddress"
        placeholder="Enter holder address"
        label="Account holder address"
      />
      <Input
        className={spacing.mt16}
        width="full"
        type="text"
        name="correspondentbank"
        placeholder="Enter correspondent bank"
        label="Correspondent bank"
      />
      <Input
        className={spacing.mt16}
        width="full"
        type="text"
        name="correspondentbankaddress"
        placeholder="Enter correspondent bank address"
        label="Correspondent bank address"
      />
      <Container className={spacing.mt16} alignitems="center">
        <Text type="caption-regular" className={spacing.mr10}>
          Processing type
          <Text type="caption-regular" color="red-90">
            *
          </Text>
        </Text>
        <TooltipIcon
          icon={<QuestionIcon />}
          size="16"
          message={
            <Text display="block" color="white-100">
              Tooltip text
            </Text>
          }
        />
      </Container>
      <Container className={classNames(spacing.mt10, spacing.mb16)}>
        <Radio id="manual" onChange={() => {}} name="processingType" checked>
          Manual
        </Radio>
        <Radio
          className={spacing.ml30}
          id="auto"
          onChange={() => {}}
          name="processingType"
          checked={false}
        >
          Auto
        </Radio>
      </Container>

      <Select
        onChange={() => {}}
        placeholder="Choose import processor"
        width="full"
        label="Import processor"
        required
      >
        {({ onOptionClick, selectElement, selectValue }) => (
          <SelectDropdown selectElement={selectElement}>
            <SelectDropdownBody>
              <SelectDropdownItemList>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={selectValue?.value === value}
                    onClick={() =>
                      onOptionClick({
                        label,
                        value,
                      })
                    }
                  />
                ))}
              </SelectDropdownItemList>
            </SelectDropdownBody>
          </SelectDropdown>
        )}
      </Select>
    </Container>
  </Modal>
);

export const ImportCreateAccountAddBankInformationCardType: React.FC = () => (
  <Modal
    size="md-narrow"
    centered
    overlay="black-100"
    header={
      <ModalHeader
        backgroundColor="grey-10"
        headerHeight="medium"
        title="Create account"
        titlePosition="center"
        onClick={() => {}}
      />
    }
    footer={
      <ModalFooter justifycontent="center">
        <ButtonGroup align="center">
          <Button
            width="md"
            navigation
            iconLeft={<ArrowLeftIcon />}
            height="large"
            background="grey-20"
            color="black-100"
            border="grey-20-grey-90"
            onClick={() => {}}
          >
            Back
          </Button>
          <Button
            width="md"
            height="large"
            onClick={() => {}}
            dropdownBorder="white-100"
            dropdown={
              <DropDownItem type="button" onClick={() => {}}>
                Save and add new
              </DropDownItem>
            }
          >
            Save and close
          </Button>
        </ButtonGroup>
      </ModalFooter>
    }
  >
    <Container flexdirection="column" className={classNames(spacing.mb32)}>
      <Text className={spacing.mt32} type="body-medium" align="center">
        Add bank information
      </Text>
      <Container className={spacing.mt16} alignitems="center">
        <Text type="caption-regular" className={spacing.mr10}>
          Type account
          <Text type="caption-regular" color="red-90">
            *
          </Text>
        </Text>
        <TooltipIcon
          icon={<QuestionIcon />}
          size="16"
          message={
            <Text display="block" color="white-100">
              Tooltip text
            </Text>
          }
        />
      </Container>
      <Container className={spacing.mt10}>
        <Radio id="bank" onChange={() => {}} name="accountType" checked={false}>
          Bank
        </Radio>
        <Radio className={spacing.ml30} id="cash" onChange={() => {}} name="accountType" checked>
          Cash
        </Radio>
        <Radio
          className={spacing.ml30}
          id="card"
          onChange={() => {}}
          name="accountType"
          checked={false}
        >
          Card
        </Radio>
      </Container>
      <Input
        className={spacing.mt16}
        width="full"
        type="text"
        name="bankname"
        placeholder="Enter name"
        label="Bank name"
        isRequired
      />
      <Input
        className={spacing.mt16}
        width="full"
        type="text"
        name="swiftcode"
        placeholder="Enter SWIFT code"
        label="SWIFT code / ABA / SEPA"
        isRequired
      />
      <Input
        className={spacing.mt16}
        width="full"
        type="text"
        name="bankaddress"
        placeholder="Enter bank address"
        label="Bank address"
        isRequired
      />
      <Text className={spacing.mt24} type="body-regular-14" color="grey-100">
        Account information:
      </Text>
      <Input
        className={spacing.mt16}
        width="full"
        type="text"
        name="iban"
        placeholder="Enter IBAN number"
        label="IBAN number"
        isRequired
      />
      <Input
        className={spacing.mt16}
        width="full"
        type="text"
        name="cardnumber"
        placeholder="Enter card number"
        label="Card number"
        isRequired
      />
      <Input
        className={spacing.mt16}
        width="full"
        type="text"
        name="cardholdername"
        placeholder="Enter card holder name"
        label="Card holder name"
      />
      <Container className={spacing.mt16} alignitems="center">
        <Text type="caption-regular" className={spacing.mr10}>
          Processing type
          <Text type="caption-regular" color="red-90">
            *
          </Text>
        </Text>
        <TooltipIcon
          icon={<QuestionIcon />}
          size="16"
          message={
            <Text display="block" color="white-100">
              Tooltip text
            </Text>
          }
        />
      </Container>
      <Container className={classNames(spacing.mt10, spacing.mb16)}>
        <Radio id="manual" onChange={() => {}} name="processingType" checked>
          Manual
        </Radio>
        <Radio
          className={spacing.ml30}
          id="auto"
          onChange={() => {}}
          name="processingType"
          checked={false}
        >
          Auto
        </Radio>
      </Container>
      <Select
        onChange={() => {}}
        placeholder="Choose import processor"
        width="full"
        label="Import processor"
        required
      >
        {({ onOptionClick, selectValue, selectElement }) => (
          <SelectDropdown selectElement={selectElement}>
            <SelectDropdownBody>
              <SelectDropdownItemList>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={selectValue?.value === value}
                    onClick={() =>
                      onOptionClick({
                        label,
                        value,
                      })
                    }
                  />
                ))}
              </SelectDropdownItemList>
            </SelectDropdownBody>
          </SelectDropdown>
        )}
      </Select>
    </Container>
  </Modal>
);

export const ImportCreateAccountAddBankInformationCashType: React.FC = () => (
  <Modal
    size="md-narrow"
    centered
    overlay="black-100"
    header={
      <ModalHeader
        backgroundColor="grey-10"
        headerHeight="medium"
        title="Create account"
        titlePosition="center"
        onClick={() => {}}
      />
    }
    footer={
      <ModalFooter justifycontent="center">
        <ButtonGroup align="center">
          <Button
            width="md"
            navigation
            iconLeft={<ArrowLeftIcon />}
            height="large"
            background="grey-20"
            color="black-100"
            border="grey-20-grey-90"
            onClick={() => {}}
          >
            Back
          </Button>
          <Button
            width="md"
            height="large"
            onClick={() => {}}
            dropdownBorder="white-100"
            dropdown={
              <>
                <DropDownItem type="button" onClick={() => {}}>
                  Save and add new
                </DropDownItem>
              </>
            }
          >
            Save and close
          </Button>
        </ButtonGroup>
      </ModalFooter>
    }
  >
    <Text className={spacing.mt32} type="body-medium" align="center">
      Add bank information
    </Text>
    <Container className={spacing.mt16} alignitems="center">
      <Text type="caption-regular" className={spacing.mr10}>
        Type account
        <Text type="caption-regular" color="red-90">
          *
        </Text>
      </Text>
      <TooltipIcon
        icon={<QuestionIcon />}
        size="16"
        message={
          <Text display="block" color="white-100">
            Tooltip text
          </Text>
        }
      />
    </Container>
    <Container className={spacing.mt10}>
      <Radio id="bank" onChange={() => {}} name="accountType" checked={false}>
        Bank
      </Radio>
      <Radio
        className={spacing.ml30}
        id="cash"
        onChange={() => {}}
        name="accountType"
        checked={false}
      >
        Cash
      </Radio>
      <Radio className={spacing.ml30} id="card" onChange={() => {}} name="accountType" checked>
        Card
      </Radio>
    </Container>
    <Input
      className={spacing.mt16}
      width="full"
      type="text"
      name="bankname"
      placeholder="Enter name"
      label="Bank name"
      isRequired
    />
    <Input
      className={spacing.mt16}
      width="full"
      type="text"
      name="swiftcode"
      placeholder="Enter SWIFT code"
      label="SWIFT code / ABA / SEPA"
      isRequired
    />
    <Input
      className={spacing.mt16}
      width="full"
      type="text"
      name="bankaddress"
      placeholder="Enter bank address"
      label="Bank address"
      isRequired
    />
    <Text className={spacing.mt24} type="body-regular-14" color="grey-100">
      Account information:
    </Text>
    <Input
      className={spacing.mt16}
      width="full"
      type="text"
      name="safeboxnumber"
      placeholder="Enter safebox number"
      label="Safebox number"
    />
    <Input
      className={classNames(spacing.mt16, spacing.mb32)}
      width="full"
      type="text"
      name="accountholdername"
      placeholder="Enter holder number"
      label="Account number name"
      isRequired
    />
  </Modal>
);

export const SelectAccountClass: React.FC = () => (
  <Modal
    size="1040"
    background="grey-10"
    type="new"
    pluginScrollDisabled
    padding="none"
    flexdirection="row"
  >
    <Container
      flexdirection="column"
      background="white-100"
      className={classNames(spacing.w320fixed, spacing.h100p, spacing.p24)}
      gap="16"
    >
      <LegalEntityTitle name="Matar Wnd Wrade company SUPER LTD manage" />
      <Divider fullHorizontalWidth />
      <Container flexdirection="column">
        <Text color="grey-100">Choose Business Division</Text>
        <SelectBD nameActiveDB="BetterApp">
          <SelectBDItem name="Business division 1" isSelected onClick={() => {}} />
          <SelectBDItem name="Business division 2" isSelected={false} onClick={() => {}} />
        </SelectBD>
      </Container>
      <Divider fullHorizontalWidth />
      <ChartOfAccountsAccordion />
    </Container>
    <Container gap="24" className={spacing.p24} flexdirection="column" flexgrow="1">
      <Container justifycontent="space-between">
        <Container gap="24" alignitems="center">
          <IconButton
            background="transparent"
            size="24"
            icon={<ArrowLeftIcon />}
            color="grey-100-black-100"
            onClick={() => {}}
          />
          <Text color="grey-100" type="body-medium">
            Chart of Accounts
          </Text>
        </Container>
        <Container gap="16" alignitems="center">
          <IconButton
            background="white-100"
            size="24"
            icon={<CloseIcon />}
            color="black-100"
            onClick={() => {}}
          />
        </Container>
      </Container>
      <Container customScroll flexdirection="column" flexgrow="1">
        <Container
          flexdirection="column"
          justifycontent="center"
          alignitems="center"
          flexgrow="1"
          gap="40"
        >
          <Text type="body-medium" align="center" className={spacing.w240}>
            Please choose Account class to create chart of account
          </Text>
          <Container justifycontent="center">
            <AccountClassType type="Assets" onClick={() => {}} />
            <AccountClassType type="Liabilities" onClick={() => {}} />
            <AccountClassType type="Equity" onClick={() => {}} />
          </Container>
          <Container justifycontent="center">
            <AccountClassType type="Income" onClick={() => {}} />
            <AccountClassType type="Expenses" onClick={() => {}} />
          </Container>
        </Container>
      </Container>
    </Container>
  </Modal>
);
