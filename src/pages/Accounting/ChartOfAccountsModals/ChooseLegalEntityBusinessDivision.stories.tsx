import { Meta } from '@storybook/react';
import { Icon } from 'Components/base/Icon';
import { Button } from 'Components/base/buttons/Button';
import { ButtonGroup } from 'Components/base/buttons/ButtonGroup';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { EntityPicker } from 'Components/custom/Accounting/ChartOfAccounts/EntityPicker';
import { EntityPickerDivider } from 'Components/custom/Accounting/ChartOfAccounts/EntityPickerDivider';
import { Search } from 'Components/inputs/Search';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { businessUnits, legalEntities } from 'Mocks/fakeOptions';
import { ReactComponent as EmptyBDIcon } from 'Svg/v2/180/table.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Accounting/Chart Of Accounts Modals',
} as Meta;

export const ChooseLegalEntityBusinessDivision: React.FC = () => {
  const [legalEntityActive, setLegalEntityActive] = useState<string>();
  const [businessUnitActive, setBusinessUnitActive] = useState<string>();
  return (
    <Modal
      size="990"
      header={<ModalHeaderNew height="60" onClose={() => {}} />}
      pluginScrollDisabled
      footer={
        <ModalFooterNew justifycontent="space-between" border>
          <ButtonGroup align="right">
            <Button
              height="48"
              width="156"
              background={legalEntityActive ? 'violet-90' : 'grey-90'}
              onClick={() => {}}
            >
              Next
            </Button>
          </ButtonGroup>
        </ModalFooterNew>
      }
    >
      <Container
        flexdirection="column"
        className={classNames(spacing.w880, spacing.mX_auto)}
        alignitems="center"
      >
        <Text type="title-semibold" align="center">
          Please choose business structure of Chart of accounts list
        </Text>
        <Container className={classNames(spacing.w100p, spacing.mt40)} gap="40">
          <Container flexdirection="column" className={spacing.w380} gap="16" alignitems="center">
            <Text type="body-medium" align="center">
              Choose Legal entity
            </Text>
            <Search
              width="360"
              height="40"
              name="search-LE"
              placeholder="Search Legal entity"
              value=""
              onChange={() => {}}
              searchParams={[]}
              onSearch={() => {}}
              onClear={() => {}}
            />
            <Container
              flexdirection="column"
              gap="16"
              customScroll
              alignitems="center"
              className={classNames(spacing.h320, spacing.pr6, spacing.w360max)}
            >
              {legalEntities.map(({ id, title }) => (
                <EntityPicker
                  key={id}
                  name={title}
                  isActive={legalEntityActive === id}
                  onClick={() => setLegalEntityActive(id)}
                />
              ))}
              {/* <Icon className={spacing.mt40} icon={<EmptyIcon />} />
              <Text type="body-regular-16" color="grey-100">
                Nothing found for your request
              </Text> */}
            </Container>
          </Container>
          <EntityPickerDivider isActive={!!legalEntityActive} />
          <Container
            flexdirection="column"
            className={spacing.w380}
            gap="16"
            justifycontent={legalEntityActive ? 'flex-start' : 'center'}
            alignitems="center"
          >
            {!!legalEntityActive && (
              <>
                <Text type="body-medium" align="center">
                  Choose Business Division
                </Text>
                <Search
                  width="360"
                  height="40"
                  name="search-LE"
                  placeholder="Search Business Division"
                  value=""
                  onChange={() => {}}
                  searchParams={[]}
                  onSearch={() => {}}
                  onClear={() => {}}
                />
                <Container
                  flexdirection="column"
                  gap="16"
                  customScroll
                  alignitems="center"
                  className={classNames(spacing.h320, spacing.pr6)}
                >
                  {businessUnits.map(({ id, title }) => (
                    <EntityPicker
                      key={id}
                      name={title}
                      isActive={businessUnitActive === id}
                      onClick={() => setBusinessUnitActive(id)}
                    />
                  ))}
                </Container>
              </>
            )}
            {!legalEntityActive && (
              <Container
                justifycontent="center"
                flexdirection="column"
                alignitems="center"
                gap="20"
                className={spacing.w380fixed}
              >
                <Icon icon={<EmptyBDIcon />} />
                <Text
                  className={spacing.w210}
                  align="center"
                  type="body-regular-16"
                  color="grey-100"
                >
                  First choose Legal entity and then Business Division
                </Text>
              </Container>
            )}
          </Container>
        </Container>
      </Container>
    </Modal>
  );
};
