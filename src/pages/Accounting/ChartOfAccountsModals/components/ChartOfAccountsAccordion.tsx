import { Divider } from 'Components/base/Divider';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { AccordionAccountItem } from 'Components/custom/Accounting/ChartOfAccounts/AccordionAccountItem';
import { AccordionAccountSubItem } from 'Components/custom/Accounting/ChartOfAccounts/AccordionAccountSubItem';
import { ReactComponent as LiabliltiesIcon } from 'Svg/v2/16/bank-cheque.svg';
import { ReactComponent as LockOpened } from 'Svg/v2/16/lock-opened.svg';
import { ReactComponent as IncomeIcon } from 'Svg/v2/16/money-in.svg';
import { ReactComponent as ExpensesIcon } from 'Svg/v2/16/money-out.svg';
import { ReactComponent as AssetIcon } from 'Svg/v2/16/money-stack.svg';
import { ReactComponent as EquityIcon } from 'Svg/v2/16/scales.svg';
import { ReactComponent as EmptyChartOfAccountIcon } from 'Svg/v2/124/empty.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

const ChartOfAccountsAccordion = () => {
  const accordionItems = [
    { id: 'asset', title: 'Asset', isActive: true, counter: 3, icon: <AssetIcon /> },
    {
      id: 'liabilities',
      title: 'Liabilities',
      isActive: false,
      counter: 12,
      icon: <LiabliltiesIcon />,
    },
    { id: 'equity', title: 'Equity', isActive: true, counter: 0, icon: <EquityIcon /> },
    { id: 'income', title: 'Income', isActive: false, counter: 2, icon: <IncomeIcon /> },
    { id: 'expenses', title: 'Expenses', isActive: false, counter: 2, icon: <ExpensesIcon /> },
  ];

  const [itemActive, setItemActive] = useState<boolean>(true);

  return (
    <Container className={spacing.pr6} flexdirection="column" gap="16" customScroll flexgrow="1">
      {accordionItems.map(({ id, title, isActive, counter, icon }, index) => (
        <>
          <AccordionAccountItem
            key={id}
            name={title}
            counter={counter}
            icon={icon}
            hideActions
            addButtonShown={false}
            isActive={isActive && index !== 0}
            isOpen={isActive}
            onClick={() => {}}
            onAddClick={() => {}}
            onEdit={() => {}}
            onDelete={() => {}}
          >
            {index === 0 && (
              <>
                <AccordionAccountSubItem
                  name="Level 1 veeeeeeryyyy looooooooooong"
                  nestingLevel={1}
                  icon={<LockOpened />}
                  iconTooltip="Non - sensitive account"
                  isActive={itemActive}
                  isOpen={itemActive}
                  hideActions
                  addButtonShown={false}
                  onClick={() => {}}
                  onIconClick={() => setItemActive((prev) => !prev)}
                  onAddClick={() => {}}
                  onEdit={() => {}}
                  onDelete={() => {}}
                >
                  <AccordionAccountSubItem
                    name="Level 2"
                    nestingLevel={2}
                    isActive={false}
                    isOpen={itemActive}
                    hideActions
                    addButtonShown={false}
                    onClick={() => {}}
                    onIconClick={() => setItemActive((prev) => !prev)}
                    onAddClick={() => {}}
                    onEdit={() => {}}
                    onDelete={() => {}}
                  >
                    <AccordionAccountSubItem
                      name="Level 3"
                      nestingLevel={3}
                      icon={<LockOpened />}
                      iconTooltip="Non - sensitive account"
                      isActive={false}
                      isOpen={false}
                      addButtonShown={false}
                      hideActions
                      onClick={() => {}}
                      onAddClick={() => {}}
                      onEdit={() => {}}
                      onDelete={() => {}}
                    />
                  </AccordionAccountSubItem>
                </AccordionAccountSubItem>
              </>
            )}
            {index === 2 && (
              <Container
                flexdirection="column"
                gap="12"
                justifycontent="center"
                alignitems="center"
                alignself="center"
                className={classNames(spacing.w220, spacing.p32)}
              >
                <EmptyChartOfAccountIcon />
                <Text color="grey-100" type="body-regular-14" align="center">
                  You do not have any chart of account
                </Text>
              </Container>
            )}
          </AccordionAccountItem>
          {index !== accordionItems.length - 1 && <Divider fullHorizontalWidth />}
        </>
      ))}
    </Container>
  );
};

export default ChartOfAccountsAccordion;
