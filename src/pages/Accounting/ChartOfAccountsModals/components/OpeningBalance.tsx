import { Divider } from 'Components/base/Divider';
import { Radio } from 'Components/base/Radio';
import { Button } from 'Components/base/buttons/Button';
import { ButtonGroup } from 'Components/base/buttons/ButtonGroup';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { InputDate } from 'Components/base/inputs/InputDate';
import { NumericInput } from 'Components/base/inputs/NumericInput';
import { Text } from 'Components/base/typography/Text';
import { Currencies } from 'Components/elements/Currencies';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

type OpeningBalanceProps = {
  bankAccountType?: boolean;
  addingOpeningBalance: boolean;
  newOpeningBalance: boolean;
  editingOpeningBalance: boolean;
  setEditingOpeningBalance(editingOpeningBalance: boolean): void;
  setNewOpeningBalance(newOpeningBalance: boolean): void;
  setAddingOpeningBalance(addingOpeningBalance: boolean): void;
};

const OpeningBalance = ({
  bankAccountType,
  addingOpeningBalance,
  newOpeningBalance,
  editingOpeningBalance,
  setEditingOpeningBalance,
  setNewOpeningBalance,
  setAddingOpeningBalance,
}: OpeningBalanceProps) => {
  const [inputValue, setInputValue] = useState<number>();
  const [startDate, setStartDate] = useState<Date | null>(new Date());

  const handleChange = ({ target: { value } }: React.ChangeEvent<HTMLInputElement>) => {
    setInputValue(+value);
  };

  return (
    <>
      {(addingOpeningBalance || newOpeningBalance || editingOpeningBalance) && (
        <Container
          isFocusedElm={(addingOpeningBalance || editingOpeningBalance) && !bankAccountType}
          flexdirection="column"
          gap="12"
          border="grey-20"
          borderRadius="20"
          className={classNames(spacing.pX24, spacing.pt20, spacing.pb24)}
        >
          <Container justifycontent="space-between" alignitems="center">
            <Text type="body-regular-14">
              {addingOpeningBalance ? 'Add Opening Balance' : 'Opening Balance'}
            </Text>
            <Container alignitems="center" gap="12">
              {!bankAccountType && (
                <Currencies
                  background={'grey-10'}
                  selectedCurrency="eur"
                  selectedCurrencyValue="1"
                  baseCurrency="usd"
                  baseCurrencyValue="1.031"
                />
              )}
              {newOpeningBalance && (
                <>
                  <Divider type="vertical" height="26" />
                  <IconButton
                    size="32"
                    background={'grey-10-grey-20'}
                    color="grey-100"
                    icon={<EditIcon />}
                    onClick={() => {
                      setEditingOpeningBalance(true);
                      setNewOpeningBalance(false);
                    }}
                  />
                </>
              )}
              {(bankAccountType && addingOpeningBalance) ||
                (newOpeningBalance && (
                  <IconButton
                    size="32"
                    icon={<TrashIcon />}
                    background={'grey-10-grey-20'}
                    color="grey-100"
                    onClick={() => {
                      setNewOpeningBalance(false);
                      setInputValue(undefined);
                      setStartDate(new Date());
                    }}
                  />
                ))}
            </Container>
          </Container>
          <Container gap="16">
            <Radio
              id="debit"
              color="violet-90"
              type="caption-regular"
              onChange={() => {}}
              name="openingBalanceRadio"
              checked
            >
              Debit
            </Radio>
            <Radio
              id="credit"
              color="grey-90"
              type="caption-regular"
              onChange={() => {}}
              name="openingBalanceRadio"
              checked={false}
            >
              Credit
            </Radio>
          </Container>
          <Container className={spacing.mt4} justifycontent="space-between">
            <NumericInput
              name="num-2"
              width="280"
              height="48"
              label="Amount"
              value={inputValue}
              suffix="$"
              onChange={handleChange}
              readOnly={newOpeningBalance}
            />
            <InputDate
              name="inputdate"
              placeholder="dd.mm.yyyy"
              label="Date"
              width="280"
              selected={startDate}
              onChange={(date: Date) => setStartDate(date)}
              calendarStartDay={1}
              readOnly={newOpeningBalance}
            />
          </Container>
          {(addingOpeningBalance || editingOpeningBalance) && (
            <Container justifycontent="flex-end">
              <ButtonGroup align="right">
                <Button
                  background={'white-100'}
                  color="grey-100-black-100"
                  border="grey-20"
                  width="88"
                  height="40"
                  onClick={() => {
                    setAddingOpeningBalance(false);
                  }}
                >
                  Cancel
                </Button>
                <Button
                  width="88"
                  height="40"
                  disabled={!inputValue}
                  background={inputValue ? 'violet-90-violet-100' : 'grey-90'}
                  onClick={() => {
                    setEditingOpeningBalance(false);
                    setAddingOpeningBalance(false);
                    setNewOpeningBalance(true);
                  }}
                >
                  Save
                </Button>
              </ButtonGroup>
            </Container>
          )}
        </Container>
      )}
    </>
  );
};

export default OpeningBalance;
