import { Icon } from 'Components/base/Icon';
import { Tag } from 'Components/base/Tag';
import { AdditionalBlock } from 'Components/base/Tag/Tag';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import CurrenciesCustomRate from 'Components/elements/Currencies/CurrenciesCustomRate';
import { ReactComponent as EURIcon } from 'Svg/colored/eur.svg';
import { ReactComponent as UAHIcon } from 'Svg/colored/uah.svg';
import { ReactComponent as EyeIcon } from 'Svg/v2/16/eye.svg';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import React from 'react';

const CustomRateTag = ({ viewMode }: { viewMode: boolean }) => {
  const additionalDrop: AdditionalBlock = {
    icon: <EditIcon />,
    tooltip: 'Edit',
    padding: '16',
    dropdown: (
      <CurrenciesCustomRate
        selectedCurrencyValue="1"
        selectedCurrency="eur"
        baseCurrencyValue="1.01"
        baseCurrency="usd"
      />
    ),
  };
  return (
    <Tag
      icon={<EyeIcon />}
      text="FX"
      color="grey"
      font="caption-semibold"
      cursor="pointer"
      tooltip={
        <Container alignitems="center" gap="4">
          <Text color="white-100">1 UAH</Text>
          <Icon icon={<UAHIcon />} />
          <Text color="white-100">= 0.034 EUR</Text>
          <Icon icon={<EURIcon />} />
        </Container>
      }
      additionalDropdownBlock={!viewMode ? additionalDrop : undefined}
    />
  );
};

export default CustomRateTag;
