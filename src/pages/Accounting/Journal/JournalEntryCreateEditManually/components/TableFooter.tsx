import { Divider } from 'Components/base/Divider';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { TransactionPageSwitch } from 'Components/elements/TransactionPageSwitch';
import { ReactComponent as CSVIcon } from 'Svg/v2/16/file-csv.svg';
import { ReactComponent as XLSIcon } from 'Svg/v2/16/file-xls.svg';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';

const TableFooter = ({ viewMode }: { viewMode: boolean }) => (
  <>
    <Divider className={spacing.mt16} fullHorizontalWidth />
    <Container
      className={classNames(spacing.p16, spacing.w100p)}
      flexdirection="row-reverse"
      justifycontent="space-between"
    >
      <Container gap="8">
        <Container flexdirection="column" className={spacing.w150fixed}>
          <Text type="subtext-regular" align="right" color="grey-90">
            Total Debit
          </Text>
          <Text type="body-regular-14" align="right">
            0.00
          </Text>
        </Container>
        <Container flexdirection="column" className={classNames(spacing.w150fixed, spacing.mr16)}>
          <Text type="subtext-regular" align="right" color="grey-90">
            Total Credit
          </Text>
          <Text type="body-regular-14" align="right">
            0.00
          </Text>
        </Container>
      </Container>
      {viewMode && (
        <Container>
          <Button
            background="transparent-blue-10"
            font="caption-semibold"
            height="40"
            color="grey-100-violet-90"
            iconLeft={<XLSIcon />}
            onClick={() => {}}
          >
            Export to XLS
          </Button>
          <Button
            background="transparent-blue-10"
            font="caption-semibold"
            height="40"
            color="grey-100-violet-90"
            iconLeft={<CSVIcon />}
            onClick={() => {}}
          >
            Export to CSV
          </Button>
        </Container>
      )}
    </Container>
    {viewMode && (
      <TransactionPageSwitch page={1} totalPages={28} onPrevPage={() => {}} onNextPage={() => {}} />
    )}
  </>
);

export default TableFooter;
