import { Divider } from 'Components/base/Divider';
import { Container } from 'Components/base/grid/Container';
import { EntryInfoTitle } from 'Components/custom/Accounting/Journal/EntryInfoTitle';
import { Currencies } from 'Components/elements/Currencies';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';

const MainBlockHeader = ({ viewMode }: { viewMode: boolean }) => (
  <>
    <Container
      justifycontent="space-between"
      alignitems="center"
      className={classNames(spacing.mY28, spacing.ml32, spacing.mr24)}
    >
      <Container gap="32">
        <EntryInfoTitle category="Created by" name="Vitaliy Kvasha" showAvatar />
        <EntryInfoTitle category="Legal entity" name="BetterMe Limited" />
      </Container>
      <Currencies
        selectedCurrencyValue="1"
        selectedCurrency="eur"
        baseCurrencyValue="1.01"
        baseCurrency="usd"
        showCustomRate={!viewMode}
      />
    </Container>
    <Divider fullHorizontalWidth />
  </>
);

export default MainBlockHeader;
