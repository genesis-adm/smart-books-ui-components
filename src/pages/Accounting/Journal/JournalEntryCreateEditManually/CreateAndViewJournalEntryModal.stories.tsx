import { Meta } from '@storybook/react';
import { DropDown } from 'Components/DropDown';
import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { Divider } from 'Components/base/Divider';
import { Tag } from 'Components/base/Tag';
import { Button } from 'Components/base/buttons/Button';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { InputDate } from 'Components/base/inputs/InputDate';
import { NumericInput } from 'Components/base/inputs/NumericInput';
import { OwnNumberFormatValues } from 'Components/base/inputs/NumericInput/NumericInput.types';
import OptionDoubleHorizontalNew from 'Components/base/inputs/SelectNew/options/OptionDoubleHorizontalNew/OptionDoubleHorizontalNew';
import { OptionDoubleVerticalNew } from 'Components/base/inputs/SelectNew/options/OptionDoubleVerticalNew';
import { OptionSingleNew } from 'Components/base/inputs/SelectNew/options/OptionSingleNew';
import CurrenciesCustomRate from 'Components/elements/Currencies/CurrenciesCustomRate';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { Select } from 'Components/inputs/Select';
import { OptionWithTwoLabels } from 'Components/inputs/Select/options/OptionWithTwoLabels';
import { TextInput } from 'Components/inputs/TextInput';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderJournal } from 'Components/modules/Modal/elements/ModalHeaderJournal';
import { TableCellWidth } from 'Components/types/gridTypes';
import { options } from 'Mocks/fakeOptions';
import CustomRateTag from 'Pages/Accounting/Journal/JournalEntryCreateEditManually/components/CustomRateTag';
import MainBlockHeader from 'Pages/Accounting/Journal/JournalEntryCreateEditManually/components/MainBlockHeader';
import TableFooter from 'Pages/Accounting/Journal/JournalEntryCreateEditManually/components/TableFooter';
import { journalTableHead } from 'Pages/Accounting/Journal/constants';
import { ReactComponent as CurrencyIcon } from 'Svg/v2/16/banknote.svg';
import { ReactComponent as PostedIcon } from 'Svg/v2/16/clipboard-tick.svg';
import { ReactComponent as EyeIcon } from 'Svg/v2/16/eye.svg';
import { ReactComponent as DocumentIcon } from 'Svg/v2/16/file-blank.svg';
import { ReactComponent as DocumentLinesIcon } from 'Svg/v2/16/file-text-filled.svg';
import { ReactComponent as FileTextIcon } from 'Svg/v2/16/file-text.svg';
import { ReactComponent as InfoIcon } from 'Svg/v2/16/info.svg';
import { ReactComponent as LockClosedIcon } from 'Svg/v2/16/lock-closed.svg';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as DropdownIcon } from 'Svg/v2/16/three-dots.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Accounting/Journal/Modals/CreateAndViewJournalEntryModal',
  argTypes: {
    viewMode: {
      control: { type: 'boolean' },
    },
  },
  args: {
    viewMode: false,
  },
} as Meta;

export const CreateAndViewJournalEntryModal = ({ viewMode }: { viewMode: boolean }) => {
  const [inputValue, setInputValue] = useState<number>();
  const [descriptionValue, setDescriptionValue] = useState<string>('');
  const [isAddedFXRate, setIsAddedFXRate] = useState<boolean>(false);
  const handleChange = ({ floatValue }: OwnNumberFormatValues) => {
    setInputValue(floatValue);
  };

  const handleAddingFXRate = () => {
    setIsAddedFXRate(true);
  };

  return (
    <Modal
      type="new-bottom"
      size="full"
      pluginScrollDisabled
      padding="none"
      header={
        <ModalHeaderJournal
          title={viewMode ? 'Journal entry # 000001' : 'Create Journal entry'}
          actionTag={
            viewMode ? (
              <Tag
                size="32"
                padding="8"
                color="violet"
                text="Posted"
                icon={<PostedIcon />}
                cursor="default"
              />
            ) : (
              <Tag
                size="32"
                padding="8"
                color="green"
                text="New"
                icon={<DocumentIcon />}
                cursor="default"
              />
            )
          }
          action={
            viewMode ? (
              <DropDown
                flexdirection="column"
                padding="8"
                gap="4"
                control={({ handleOpen }) => (
                  <IconButton
                    icon={<DropdownIcon />}
                    onClick={handleOpen}
                    background={'white-100'}
                    color="grey-100"
                  />
                )}
              >
                <DropDownButton size="160" icon={<EditIcon />} onClick={() => {}}>
                  Edit
                </DropDownButton>
                <DropDownButton size="160" icon={<EyeIcon />} onClick={() => {}}>
                  View source document
                </DropDownButton>
                <DropDownButton size="160" icon={<DocumentLinesIcon />} onClick={() => {}}>
                  Bank transaction view
                </DropDownButton>
                <Divider type="horizontal" fullHorizontalWidth className={spacing.mY4} />
                <DropDownButton size="204" type="danger" icon={<TrashIcon />} onClick={() => {}}>
                  Delete
                </DropDownButton>
              </DropDown>
            ) : null
          }
          onClose={() => {}}
        />
      }
      footer={
        <ModalFooterNew justifycontent="flex-start" gap="24" background={'grey-10'}>
          <Button width="220" onClick={() => {}}>
            Create
          </Button>
          <Button
            width="220"
            background={'white-100'}
            color="grey-100-black-100"
            border="grey-20"
            onClick={() => {}}
            navigation
          >
            Close
          </Button>
        </ModalFooterNew>
      }
      background={'grey-10'}
    >
      <Container
        background={'white-100'}
        flexgrow="1"
        className={spacing.mX24}
        radius
        flexdirection="column"
        overflow="hidden"
      >
        <MainBlockHeader viewMode={viewMode} />
        <Container gap="24" className={classNames(spacing.mt24, spacing.mX24)}>
          <InputDate
            name="inputdate"
            placeholder="Choose date*"
            label="Date"
            required
            width="180"
            height="36"
            selected={new Date()}
            onChange={() => {}}
            calendarStartDay={1}
          />
          <Select
            height="36"
            required
            onChange={() => {}}
            width="180"
            placeholder="Currency"
            label="Currency"
            icon={<CurrencyIcon />}
          >
            {({ onOptionClick, selectValue, selectElement, closeDropdown }) => (
              <SelectDropdown selectElement={selectElement} width="200">
                <SelectDropdownBody>
                  <SelectDropdownItemList>
                    {options.map(({ label, value }) => (
                      <OptionSingleNew
                        key={value}
                        label={label}
                        selected={selectValue?.value === value}
                        onClick={() => {
                          onOptionClick({
                            label,
                            value,
                          });
                          closeDropdown();
                        }}
                      />
                    ))}
                  </SelectDropdownItemList>
                </SelectDropdownBody>
              </SelectDropdown>
            )}
          </Select>
          <TextInput
            width="300"
            height="36"
            maxLength={250}
            label="Add Description"
            placeholder="Add Description"
            icon={<FileTextIcon />}
            autoResizable
            counter
            required
            name="description"
            value={descriptionValue}
            onChange={(e) => setDescriptionValue(e.target.value)}
            replaceContentComponent={
              viewMode ? (
                <Tag
                  text="Sensitive content"
                  icon={<LockClosedIcon />}
                  tooltip="You don't have access to sensitive chart of accounts"
                />
              ) : undefined
            }
          />
        </Container>
        <TableNew
          noResults={false}
          className={classNames(spacing.mt24, spacing.ml24, spacing.pr24)}
          tableHead={
            <TableHeadNew>
              {journalTableHead.map((item, index) => (
                <TableTitleNew
                  key={index}
                  minWidth={item.minWidth as TableCellWidth}
                  required={item.required as TableTitleNewProps['required']}
                  align={item.align as TableTitleNewProps['align']}
                  title={item.title}
                  onClick={() => {}}
                />
              ))}
            </TableHeadNew>
          }
        >
          <TableBodyNew>
            {Array.from(Array(4)).map((_, index) => (
              <TableRowNew key={index} gap="8">
                {!viewMode ? (
                  <TableCellNew
                    minWidth={journalTableHead[0].minWidth as TableCellWidth}
                    alignitems="center"
                    padding="8"
                    fixedWidth
                  >
                    <Select placeholder="Choose" label="Choose" height="36" background={'grey-10'}>
                      {({ onOptionClick, selectValue, selectElement, closeDropdown }) => (
                        <SelectDropdown selectElement={selectElement} width="300">
                          <SelectDropdownBody>
                            <SelectDropdownItemList>
                              {options.map(({ label, secondaryLabel, value }) => (
                                <OptionDoubleVerticalNew
                                  key={value}
                                  label={label}
                                  secondaryLabel={secondaryLabel}
                                  selected={selectValue?.value === value}
                                  onClick={() => {
                                    onOptionClick({
                                      label,
                                      value,
                                    });
                                    closeDropdown();
                                  }}
                                />
                              ))}
                            </SelectDropdownItemList>
                          </SelectDropdownBody>
                        </SelectDropdown>
                      )}
                    </Select>
                  </TableCellNew>
                ) : (
                  <TableCellTextNew
                    minWidth={journalTableHead[0].minWidth as TableCellWidth}
                    value="SmartBooks veeeeeerrrrrry looooongggggg"
                    noWrap
                    lineClamp="none"
                    fixedWidth
                  />
                )}
                {!viewMode ? (
                  <TableCellNew
                    minWidth={journalTableHead[1].minWidth as TableCellWidth}
                    alignitems="center"
                    padding="8"
                    flexgrow="1"
                    fixedWidth
                  >
                    <Select
                      height="36"
                      placeholder="Choose"
                      label="Choose"
                      icon={<InfoIcon />}
                      type="baseWithCategory"
                      background={'grey-10'}
                    >
                      {({ onOptionClick, selectValue, selectElement, closeDropdown }) => (
                        <SelectDropdown selectElement={selectElement} width="624">
                          <SelectDropdownBody>
                            <SelectDropdownItemList>
                              {options.map(({ label, secondaryLabel, value }) => (
                                <OptionWithTwoLabels
                                  id={label}
                                  key={value}
                                  label={label}
                                  labelDirection="horizontal"
                                  secondaryLabel={secondaryLabel}
                                  selected={selectValue?.value === value}
                                  onClick={() => {
                                    onOptionClick({
                                      label,
                                      subLabel: secondaryLabel,
                                      value,
                                    });
                                    closeDropdown();
                                  }}
                                />
                              ))}
                            </SelectDropdownItemList>
                          </SelectDropdownBody>
                        </SelectDropdown>
                      )}
                    </Select>
                  </TableCellNew>
                ) : (
                  <TableCellTextNew
                    minWidth={journalTableHead[1].minWidth as TableCellWidth}
                    value="Receivables - UAH"
                    secondaryValue="Assets"
                    iconLeft={<InfoIcon />}
                    iconColor="grey-100"
                    secondaryColor="grey-100"
                    type="text-regular"
                    flexdirection="row"
                    justifycontent="space-between"
                    fixedWidth
                  />
                )}
                <TableCellNew
                  minWidth={journalTableHead[2].minWidth as TableCellWidth}
                  justifycontent="center"
                >
                  {!isAddedFXRate && !viewMode ? (
                    <DropDown
                      flexdirection="column"
                      padding="16"
                      gap="16"
                      control={({ handleOpen }) => (
                        <Button
                          font="caption-semibold"
                          height="24"
                          color="violet-90"
                          background={'blue-10'}
                          onClick={handleOpen}
                        >
                          Add
                        </Button>
                      )}
                    >
                      <CurrenciesCustomRate
                        selectedCurrencyValue="1"
                        selectedCurrency="eur"
                        baseCurrencyValue="1.01"
                        baseCurrency="usd"
                        onApply={handleAddingFXRate}
                      />
                    </DropDown>
                  ) : (
                    <CustomRateTag viewMode={viewMode} />
                  )}
                </TableCellNew>
                {!viewMode ? (
                  <TableCellNew
                    minWidth={journalTableHead[3].minWidth as TableCellWidth}
                    alignitems="center"
                    padding="8"
                    fixedWidth
                  >
                    <Select
                      height="36"
                      placeholder="Choose"
                      label="Choose"
                      type="baseWithCategory"
                      background={'grey-10'}
                    >
                      {({ onOptionClick, selectValue, selectElement, closeDropdown }) => (
                        <SelectDropdown selectElement={selectElement} width="320">
                          <SelectDropdownBody>
                            <SelectDropdownItemList>
                              {options.map(({ label, secondaryLabel, value }) => (
                                <OptionDoubleHorizontalNew
                                  key={value}
                                  label={label}
                                  secondaryLabel={secondaryLabel}
                                  selected={selectValue?.value === value}
                                  onClick={() => {
                                    onOptionClick({
                                      label,
                                      subLabel: secondaryLabel,
                                      value,
                                    });
                                    closeDropdown();
                                  }}
                                />
                              ))}
                            </SelectDropdownItemList>
                          </SelectDropdownBody>
                        </SelectDropdown>
                      )}
                    </Select>
                  </TableCellNew>
                ) : (
                  <TableCellTextNew
                    minWidth={journalTableHead[3].minWidth as TableCellWidth}
                    value="Client 1"
                    secondaryValue="Customer"
                    secondaryColor="grey-100"
                    type="text-regular"
                    flexdirection="row"
                    justifycontent="space-between"
                    fixedWidth
                  />
                )}
                {!viewMode ? (
                  <TableCellNew
                    minWidth={journalTableHead[4].minWidth as TableCellWidth}
                    alignitems="center"
                    padding="8"
                    fixedWidth
                  >
                    <Select
                      height="36"
                      label="Choose"
                      placeholder="Choose"
                      icon={<InfoIcon />}
                      background={'grey-10'}
                    >
                      {({ onOptionClick, selectValue, selectElement, closeDropdown }) => (
                        <SelectDropdown selectElement={selectElement}>
                          <SelectDropdownBody>
                            <SelectDropdownItemList>
                              {options.map(({ label, value }) => (
                                <OptionSingleNew
                                  key={value}
                                  label={label}
                                  selected={selectValue?.value === value}
                                  onClick={() => {
                                    onOptionClick({
                                      label,
                                      value,
                                    });
                                    closeDropdown();
                                  }}
                                />
                              ))}
                            </SelectDropdownItemList>
                          </SelectDropdownBody>
                        </SelectDropdown>
                      )}
                    </Select>
                  </TableCellNew>
                ) : (
                  <TableCellTextNew
                    minWidth={journalTableHead[4].minWidth as TableCellWidth}
                    value="BetterME | Healthy veeeeeerrrrrry looooongggggg"
                    noWrap
                    lineClamp="none"
                    fixedWidth
                  />
                )}
                <TableCellNew
                  minWidth={journalTableHead[5].minWidth as TableCellWidth}
                  alignitems="center"
                  justifycontent="flex-end"
                  padding="0"
                  fixedWidth
                >
                  <NumericInput
                    background={viewMode ? 'transparent' : 'grey-10'}
                    name="num-1"
                    label=""
                    width="134"
                    height="36"
                    value={inputValue}
                    onValueChange={handleChange}
                    suffix="$"
                  />
                </TableCellNew>
                <TableCellNew
                  minWidth={journalTableHead[6].minWidth as TableCellWidth}
                  alignitems="center"
                  justifycontent="flex-end"
                  padding="0"
                  fixedWidth
                >
                  <NumericInput
                    background={viewMode ? 'transparent' : 'grey-10'}
                    name="num-2"
                    label=""
                    width="134"
                    height="36"
                    value={inputValue}
                    onValueChange={handleChange}
                  />
                </TableCellNew>
                <TableCellNew
                  minWidth={journalTableHead[7].minWidth as TableCellWidth}
                  padding="0"
                  fixedWidth
                >
                  <IconButton
                    icon={<TrashIcon />}
                    background={'transparent'}
                    color="grey-100-red-90"
                    onClick={() => {}}
                  />
                </TableCellNew>
              </TableRowNew>
            ))}
            <Container className={classNames(spacing.mt16, spacing.mr16)} justifycontent="flex-end">
              <LinkButton icon={<PlusIcon />} onClick={() => {}}>
                Add new line
              </LinkButton>
            </Container>
          </TableBodyNew>
        </TableNew>
        <TableFooter viewMode={viewMode} />
      </Container>
    </Modal>
  );
};
