import { Meta } from '@storybook/react';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { AccountingCardLE } from 'Components/custom/Accounting/Journal/AccountingCardLE';
import { Pagination } from 'Components/custom/Stories/Pagination/Pagination';
import { Modal } from 'Components/modules/Modal';
import { ReactComponent as SearchIcon } from 'Svg/v2/16/search.svg';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Accounting/Journal/Modals/JournalEntryCreateEditManuallyCreateJournalEntry',
} as Meta;

export const JournalEntryCreateEditManuallyChooseLegalEntity: React.FC = () => (
  <Modal size="1130" pluginScrollDisabled background={'white-100'} padding="none">
    <Container
      className={classNames(spacing.pt24, spacing.pX16)}
      flexdirection="column"
      flexgrow="1"
    >
      <Container justifycontent="space-between" alignitems="center">
        <Text type="title-semibold" color="black-100">
          Please choose the legal entity
        </Text>
        <IconButton
          size="32"
          icon={<SearchIcon />}
          onClick={() => {}}
          background={'grey-10-grey-20'}
          color="grey-100"
        />
      </Container>
      <Container
        className={classNames(spacing.mY32, spacing.h310)}
        customScroll
        flexgrow="1"
        gap="16"
        flexwrap="wrap"
        aligncontent="flex-start"
      >
        {Array.from(Array(9)).map((_, index) => (
          <AccountingCardLE key={index} name="BetterMe Business Holdings Corp" onClick={() => {}} />
        ))}
      </Container>
      <Pagination borderTop="default" padding="16" />
    </Container>
  </Modal>
);
