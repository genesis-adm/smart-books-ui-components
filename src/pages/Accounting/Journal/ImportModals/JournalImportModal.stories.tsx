import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { Checkbox } from 'Components/base/Checkbox';
import { Divider } from 'Components/base/Divider';
import { Tag } from 'Components/base/Tag';
import { Button } from 'Components/base/buttons/Button';
import { IconButton } from 'Components/base/buttons/IconButton';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { InputDate } from 'Components/base/inputs/InputDate';
import { NumericInput } from 'Components/base/inputs/NumericInput';
import { SelectNew } from 'Components/base/inputs/SelectNew';
import { OptionDoubleHorizontalNew } from 'Components/base/inputs/SelectNew/options/OptionDoubleHorizontalNew';
import { OptionSingleNew } from 'Components/base/inputs/SelectNew/options/OptionSingleNew';
import { Text } from 'Components/base/typography/Text';
import { AccountingCardLE } from 'Components/custom/Accounting/Journal/AccountingCardLE';
import { RulesSelectLE } from 'Components/custom/Rules/RulesSelectLE';
import ImportModalFooter from 'Components/custom/Stories/ImportSharedComponents/ImportModalFooter';
import { Pagination } from 'Components/custom/Stories/Pagination/Pagination';
import { UploadContainer } from 'Components/custom/Upload/UploadContainer';
import { UploadInfo } from 'Components/custom/Upload/UploadInfo';
import { UploadItemField } from 'Components/custom/Upload/UploadItemField';
import { Logo } from 'Components/elements/Logo';
import { SelectImport } from 'Components/elements/SelectImport';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableSelect } from 'Components/elements/Table/TableSelect';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { FilterDropDown } from 'Components/elements/filters/FilterDropDown';
import { FilterDropDownItem } from 'Components/elements/filters/FilterDropDownItem';
import { FilterItemsContainer } from 'Components/elements/filters/FilterItemsContainer';
import { CircledIcon } from 'Components/graphics/CircledIcon';
import { OptionWithTwoLabels } from 'Components/inputs/Select/options/OptionWithTwoLabels';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { TableCellWidth } from 'Components/types/gridTypes';
import { options } from 'Mocks/fakeOptions';
import { ReactComponent as BriefcaseIcon } from 'Svg/v2/12/briefcase.svg';
import { ReactComponent as ArrowLeftIcon } from 'Svg/v2/16/arrow-left.svg';
import { ReactComponent as ArrowRightIcon } from 'Svg/v2/16/arrow-right.svg';
import { ReactComponent as ClearIcon } from 'Svg/v2/16/cross.svg';
import { ReactComponent as SearchIcon } from 'Svg/v2/16/search.svg';
import { ReactComponent as TickIcon } from 'Svg/v2/16/tick-in-circle.svg';
import { ReactComponent as UploadIcon } from 'Svg/v2/16/upload.svg';
import { checkFileType } from 'Utils/fileHandlers';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';
import { journalTableItems } from '../constants';

export default {
  title: 'Pages/Accounting/Journal/Modals/Journal Import Modals',
};

export const Step1ImportFileCSV: React.FC = () => {
  const [file, setFile] = useState<File | null>(null);
  const handleFileUpload = (item: File) => {
    setFile(item);
  };
  const handleFileDelete = () => {
    setFile(null);
  };
  const handleFileDrop = (e: React.DragEvent<HTMLElement>) => {
    const dropFile = e.dataTransfer.files[0];
    if (checkFileType(dropFile, ['csv'])) {
      setFile(e.dataTransfer.files[0]);
    }
  };
  return (
    <Modal
      size="md-extrawide"
      header={<ModalHeaderNew onClose={() => {}} />}
      pluginScrollDisabled
      footer={<ImportModalFooter modalType="upload" />}
    >
      <Container flexdirection="column" alignitems="center">
        <Text className={spacing.mt32} type="h2-semibold" align="center">
          Import Journal
        </Text>
        <UploadContainer className={spacing.mt40} onDrop={handleFileDrop}>
          <UploadItemField
            name="uploader"
            files={file}
            acceptFilesType="csv"
            onChange={handleFileUpload}
            onDelete={handleFileDelete}
            onReload={() => {}}
          />
          <UploadInfo
            mainText={file ? file.name : 'Drag and drop or Choose file'}
            secondaryText="CSV; Maximum file size is 5MB"
          />
        </UploadContainer>
        <Container
          className={classNames(spacing.mt40, spacing.w440)}
          flexdirection="column"
          gap="4"
        >
          <Text type="body-regular-14">Instruction for upload file:</Text>
          <Text type="body-regular-14">• All your information must be in one file</Text>
          <Text type="body-regular-14">
            • The top row of your file must contain a header title for each column of information
          </Text>
          <Container flexdirection="column">
            <Text type="body-regular-14">• To make sure your layout matches SmartBooks,</Text>
            <Container>
              <LinkButton type="body-regular-14" onClick={() => {}}>
                download this sample CSV
              </LinkButton>
            </Container>
          </Container>
        </Container>
      </Container>
    </Modal>
  );
};

export const Step2SelectLegalEntity: React.FC = () => (
  <Modal size="1130" pluginScrollDisabled background={'white-100'} padding="none">
    <Container
      className={classNames(spacing.pt24, spacing.pX16)}
      flexdirection="column"
      flexgrow="1"
    >
      <Container justifycontent="space-between" alignitems="center">
        <Text type="title-semibold" color="black-100">
          Please choose the legal entity
        </Text>
        <IconButton
          size="32"
          icon={<SearchIcon />}
          onClick={() => {}}
          background={'grey-10-grey-20'}
          color="grey-100"
        />
        {/* <Container alignitems="center" gap="16">
          <SearchNew
            height="40"
            width="320"
            name="search"
            value=""
            placeholder="Search"
            onClear={() => {}}
            onChange={() => {}}
            searchParams={[]}
            onSearch={() => {}}
          />
          <IconButton
            size="32"
            icon={<ClearIcon />}
            onClick={() => {}}
            background="grey-10-grey-20"
            color="grey-100"
          />
        </Container> */}
      </Container>
      <Container
        className={classNames(spacing.mY32, spacing.h310)}
        customScroll
        flexgrow="1"
        gap="16"
        flexwrap="wrap"
        aligncontent="flex-start"
      >
        {Array.from(Array(9)).map((_, index) => (
          <AccountingCardLE key={index} name="BetterMe Business Holdings Corp" onClick={() => {}} />
        ))}
      </Container>
      <Pagination padding="16" />
    </Container>
  </Modal>
);

export const Step3MatchColumns: React.FC = () => (
  <Modal
    overlay="grey-10"
    size="full"
    background={'grey-10'}
    pluginScrollDisabled
    header={
      <ModalHeaderNew
        type="fullwidth"
        background={'grey-10'}
        title="Import Journal"
        icon={<Tag color="grey" text="File name: CSV" padding="4-8" cursor="default" />}
        border
        onClose={() => {}}
      />
    }
    footer={
      <ModalFooterNew justifycontent="space-between" background={'grey-10'} border>
        <Container alignitems="center" gap="16" flexgrow="1" justifycontent="flex-end">
          <Container flexdirection="column">
            <Text type="body-regular-14" color="grey-100">
              Step: 2 of 3
            </Text>
            <Text type="body-regular-14" color="grey-100">
              Next: Preview mode
            </Text>
          </Container>
          <Button
            height="48"
            navigation
            iconRight={<ArrowRightIcon />}
            width="md"
            onClick={() => {}}
          >
            Next
          </Button>
        </Container>
        <Button
          height="48"
          width="sm"
          navigation
          iconLeft={<ArrowLeftIcon />}
          background={'white-100'}
          color="black-100"
          border="grey-20-grey-90"
          onClick={() => {}}
        >
          Back
        </Button>
      </ModalFooterNew>
    }
  >
    <Container
      background={'white-100'}
      flexdirection="column"
      radius
      flexgrow="1"
      className={classNames(spacing.p24, spacing.mX20, spacing.mY16)}
      customScroll
      gap="24"
    >
      <RulesSelectLE name="BetterMe Business">
        <Container
          flexdirection="column"
          className={classNames(spacing.w1130fixed, spacing.pt24, spacing.pX24)}
        >
          <Container className={spacing.w100p} justifycontent="space-between" alignitems="center">
            <Text type="title-semibold">Please choose the legal entity</Text>
            <IconButton
              icon={<SearchIcon />}
              size="40"
              color="grey-100"
              background={'grey-10-grey-20'}
              onClick={() => {}}
            />
            {/* <SearchNew
              width="320"
              height="40"
              name="search-banking-rules-mainPage"
              placeholder="Search by If conditions"
              value=""
              onChange={() => {}}
              searchParams={[]}
              onSearch={() => {}}
              onClear={() => {}}
            /> */}
          </Container>

          <Container className={spacing.mY24} gap="16" flexwrap="wrap">
            {Array.from(Array(9)).map((_, index) => (
              <AccountingCardLE
                key={index}
                name="BetterMe Business Holdings Corp"
                onClick={() => {}}
              />
            ))}
          </Container>
          <Pagination padding="16" />
        </Container>
      </RulesSelectLE>
      <Text type="body-regular-16">
        Map columns from&nbsp;
        <Text type="body-medium" display="inline-flex">
          namefile 09.10.20.xls
        </Text>
        &nbsp;for each Smart Books field
      </Text>
      <Container gap="16" flexwrap="wrap" className={spacing.w1400}>
        <SelectImport
          name="Journal number"
          tooltip="Tooltip text"
          label="Choose column from your file"
          placeholder="Choose option"
          background={'grey-10'}
          textWidth="160"
          required
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() =>
                    onClick({
                      label,
                      value,
                    })
                  }
                />
              ))}
            </>
          )}
        </SelectImport>
        <Container gap="16">
          <SelectImport
            name="Journal date"
            tooltip="Tooltip text"
            label="Choose column from your file"
            placeholder="Choose option"
            background={'grey-10'}
            textWidth="160"
            required
          >
            {({ onClick, state }) => (
              <>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={state?.value === value}
                    onClick={() =>
                      onClick({
                        label,
                        value,
                      })
                    }
                  />
                ))}
              </>
            )}
          </SelectImport>

          <SelectNew
            name="dateFormat"
            onChange={() => {}}
            // placeholder="Choose option"
            width="248"
            // height="36"
            label="Choose date format"
            required
          >
            {({ onClick, state, selectElement }) => (
              <SelectDropdown selectElement={selectElement}>
                <SelectDropdownBody>
                  <SelectDropdownItemList>
                    {options.slice(0, 2).map(({ label, value }) => (
                      <OptionSingleNew
                        key={value}
                        label={label}
                        selected={state?.value === value}
                        onClick={() =>
                          onClick({
                            label,
                            value,
                          })
                        }
                      />
                    ))}
                  </SelectDropdownItemList>
                </SelectDropdownBody>
              </SelectDropdown>
            )}
          </SelectNew>
        </Container>
        <SelectImport
          name="Journal currency"
          tooltip="Tooltip text"
          label="Choose column from your file"
          placeholder="Choose option"
          background={'grey-10'}
          textWidth="160"
          required
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() =>
                    onClick({
                      label,
                      value,
                    })
                  }
                />
              ))}
            </>
          )}
        </SelectImport>
        <SelectImport
          name="Chart of account ID"
          tooltip="Tooltip text"
          label="Choose column from your file"
          placeholder="Choose option"
          background={'grey-10'}
          textWidth="160"
          required
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() =>
                    onClick({
                      label,
                      value,
                    })
                  }
                />
              ))}
            </>
          )}
        </SelectImport>
        <SelectImport
          name="Descriptions"
          tooltip="Tooltip text"
          label="Choose column from your file"
          placeholder="Choose option"
          background={'grey-10'}
          textWidth="160"
          required
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() =>
                    onClick({
                      label,
                      value,
                    })
                  }
                />
              ))}
            </>
          )}
        </SelectImport>
        <SelectImport
          name="Debit, amount"
          tooltip="Tooltip text"
          label="Choose column from your file"
          placeholder="Choose option"
          background={'grey-10'}
          textWidth="160"
          required
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() =>
                    onClick({
                      label,
                      value,
                    })
                  }
                />
              ))}
            </>
          )}
        </SelectImport>
        <SelectImport
          name="Counterparty"
          tooltip="Tooltip text"
          label="Choose column from your file"
          placeholder="Choose option"
          background={'grey-10'}
          textWidth="160"
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() =>
                    onClick({
                      label,
                      value,
                    })
                  }
                />
              ))}
            </>
          )}
        </SelectImport>
        <SelectImport
          name="Credit, amount"
          tooltip="Tooltip text"
          label="Choose column from your file"
          placeholder="Choose option"
          background={'grey-10'}
          textWidth="160"
          required
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() =>
                    onClick({
                      label,
                      value,
                    })
                  }
                />
              ))}
            </>
          )}
        </SelectImport>
      </Container>
    </Container>
  </Modal>
);

export const Step4PreviewMode: React.FC = () => {
  const sorting = {
    sortType: null,
    tooltipText: 'Sorting tooltip text',
  };
  const tableHead = [
    // {
    //   minWidth: '60',
    //   title: (
    //     <IconButton
    //       size="24"
    //       background="white-100"
    //       color="grey-100-black-100"
    //       border="grey-10-grey-20"
    //       icon={<CaretIcon />}
    //       onClick={() => {}}
    //     />
    //   ),
    //   padding: '0',
    // },
    {
      minWidth: '48',
      title: <Checkbox name="tickAll" checked onChange={() => {}} />,
      padding: 'none',
    },
    {
      minWidth: '160',
      title: 'Journal number',
      sorting,
    },
    {
      minWidth: '150',
      title: 'Date',
      sorting,
    },
    {
      minWidth: '100',
      title: 'Currency',
      sorting,
    },
    {
      minWidth: '180',
      title: 'Descriptions',
      sorting,
      flexgrow: '1',
    },
    {
      minWidth: '190',
      title: 'Business Division',
      sorting,
    },
    {
      minWidth: '280',
      title: 'Chart fo account name',
      sorting,
    },
    {
      minWidth: '180',
      title: 'Counterparty',
      sorting,
    },
    {
      minWidth: '150',
      title: 'Debit, amount',
      sorting,
    },
    {
      minWidth: '150',
      title: 'Credit, amount',
      sorting,
    },
    {
      minWidth: '110',
      title: 'Status',
      sorting,
      align: 'center',
    },
    {
      minWidth: '130',
      title: 'Action',
      align: 'center',
    },
  ];

  return (
    <Modal
      overlay="grey-10"
      size="full"
      background={'grey-10'}
      pluginScrollDisabled
      header={
        // <ModalHeaderNew
        //   type="fullwidth"
        //   background="grey-10"
        //   title="Import Template account"
        //   icon={<Tag color="grey" text="File name: CSV" padding="4-8" cursor="default" />}
        //   radius
        //   onClose={() => {}}
        // />
        <Container
          className={classNames(spacing.w100p, spacing.p24)}
          alignitems="center"
          justifycontent="space-between"
          flexwrap="wrap"
          style={{ borderBottom: '1px solid #EDEFF1' }}
        >
          <Container gap="24" alignitems="center">
            <Container gap="8">
              <Text type="title-bold">Import Journal</Text>
              <Tag size="24" padding="4-8" color="grey" text="File name: CSV" cursor="default" />
            </Container>
            <Divider type="vertical" height="20" background={'grey-90'} />
            <Container alignitems="center" gap="32">
              <Container gap="12" alignitems="center">
                <CircledIcon size="24" background={'grey-20'} icon={<BriefcaseIcon />} />
                <Container flexdirection="column">
                  <Text type="caption-regular" color="grey-100">
                    Legal entity
                  </Text>
                  <Text type="body-medium">BetterMe Limited</Text>
                </Container>
              </Container>
              <Container gap="12" alignitems="center">
                <Logo content="user" radius="rounded" size="24" name="Vitalii Kvasha" />
                <Container flexdirection="column">
                  <Text type="caption-regular" color="grey-100">
                    Created by
                  </Text>
                  <Text type="body-medium">Vitalii Kvasha</Text>
                </Container>
              </Container>
            </Container>
            {/* <Divider type="vertical" height="20" background="grey-90" />
            <Tag
              size="32"
              icon={<DocumentIcon />}
              // icon={<SavedSentStatusIcon />}
              padding="4-8"
              cursor="default"
              text="New"
              // text="Saved & Sent"
              color="green"
              // color="purple"
            /> */}
          </Container>

          <IconButton
            size="32"
            icon={<ClearIcon />}
            onClick={() => {}}
            background={'white-100'}
            color="grey-100"
          />
        </Container>
      }
      footer={
        <ModalFooterNew justifycontent="space-between" background={'grey-10'} border>
          <Container alignitems="center" gap="16" flexgrow="1" justifycontent="flex-end">
            <Container flexdirection="column">
              <Text type="body-regular-14" color="grey-100">
                Step: 3 of 3
              </Text>
              <Text type="body-regular-14" color="grey-100">
                Preview mode
              </Text>
            </Container>
            <Button height="48" width="md" onClick={() => {}}>
              Import 3 rows
            </Button>
          </Container>
          <Button
            height="48"
            width="sm"
            navigation
            iconLeft={<ArrowLeftIcon />}
            background={'white-100'}
            color="black-100"
            border="grey-20-grey-90"
            onClick={() => {}}
          >
            Back
          </Button>
        </ModalFooterNew>
      }
    >
      <Container flexdirection="column" radius flexgrow="1" overflow="hidden">
        <Container flexdirection="column" className={spacing.mX32}>
          <Container className={spacing.mt32} alignitems="center" justifycontent="space-between">
            <Container alignitems="center" gap="16">
              <Text type="body-medium">Journal entries for importing:</Text>
              <FilterDropDown
                title="Status"
                background={'white-100'}
                value="All"
                onClear={() => {}}
                selectedAmount={0}
              >
                <Container
                  className={classNames(spacing.mY16, spacing.ml16, spacing.mr8, spacing.w296fixed)}
                  flexdirection="column"
                >
                  <FilterItemsContainer maxHeight="344" gap="0" className={spacing.mt8}>
                    <FilterDropDownItem
                      id="all"
                      type="item"
                      element="radio"
                      checked
                      blurred={false}
                      value="All"
                      onChange={() => {}}
                    />
                    <FilterDropDownItem
                      id="valid"
                      type="item"
                      element="radio"
                      checked={false}
                      blurred={false}
                      value="Valid"
                      onChange={() => {}}
                    />
                    <FilterDropDownItem
                      id="nonValid"
                      type="item"
                      element="radio"
                      checked={false}
                      blurred={false}
                      value="Non valid"
                      onChange={() => {}}
                    />
                  </FilterItemsContainer>
                </Container>
                <Divider fullHorizontalWidth />
                <Container
                  className={classNames(spacing.m16)}
                  justifycontent="space-between"
                  alignitems="center"
                  flexdirection="row-reverse"
                >
                  <Button
                    width="auto"
                    height="32"
                    padding="8"
                    onClick={() => {}}
                    background={'violet-90-violet-100'}
                    color="white-100"
                  >
                    Apply
                  </Button>
                  {/* {isCheckedElements && (
                    <Button
                      width="auto"
                      height="32"
                      padding="8"
                      onClick={onClear}
                      background="blue-10-grey-10"
                      color="violet-90-red-90"
                    >
                      Clear all
                    </Button>
                  )} */}
                </Container>
              </FilterDropDown>
            </Container>
            <Container alignitems="center">
              <Text type="text-medium" color="grey-100">
                Total rows:&nbsp;
              </Text>
              <Text type="caption-semibold">3</Text>
              <Divider
                type="vertical"
                height="20"
                background={'grey-90'}
                className={spacing.mX16}
              />
              <Text type="text-medium" color="grey-100">
                Number of valid rows:&nbsp;
              </Text>
              <Text type="caption-semibold" color="green-90">
                2
              </Text>
              <Divider
                type="vertical"
                height="20"
                background={'grey-90'}
                className={spacing.mX16}
              />
              <Text type="text-medium" color="grey-100">
                Number of non valid rows:&nbsp;
              </Text>
              <Text type="caption-semibold" color="red-90">
                0
              </Text>
            </Container>
          </Container>
        </Container>
        <Container flexdirection="column" flexgrow="1" className={spacing.mX32} overflow="hidden">
          <TableNew
            noResults={false}
            className={spacing.mt24}
            tableHead={
              <TableHeadNew radius="10">
                {tableHead.map((item, index) => (
                  <TableTitleNew
                    key={index}
                    minWidth={item.minWidth as TableCellWidth}
                    flexgrow={item.flexgrow as TableTitleNewProps['flexgrow']}
                    padding={item.padding as TableTitleNewProps['padding']}
                    align={item.align as TableTitleNewProps['align']}
                    title={item.title}
                    sorting={item.sorting}
                    onClick={() => {}}
                  />
                ))}
              </TableHeadNew>
            }
          >
            <TableBodyNew>
              {journalTableItems.map((item) => (
                <TableRowNew
                  key={item.id}
                  background={'white'}
                  // toggleStatus={isOpen}
                  size="auto-104"
                  showCollapseBtn={item.record.length > 2}
                  // onClick={onToggle}
                >
                  {/* <TableCellNew
                    minWidth="60"
                    justifycontent="center"
                    alignitems="center"
                    className={spacing.pY18}
                  >
                    <IconButton
                      size="24"
                      background="white-100"
                      color="grey-100-black-100"
                      border="grey-10-grey-20"
                      icon={<CaretIcon />}
                      onClick={() => {}}
                    />
                  </TableCellNew> */}
                  <TableCellNew
                    minWidth="48"
                    justifycontent="center"
                    alignitems="center"
                    className={spacing.pY22}
                  >
                    <Checkbox name={`line-${item.id}`} checked onChange={() => {}} />
                  </TableCellNew>
                  <TableCellTextNew
                    minWidth="160"
                    value="ID 429"
                    type="text-regular"
                    className={spacing.pY22}
                  />
                  <TableCellNew minWidth="150" className={spacing.pY12} padding="8-0">
                    <InputDate
                      name="inputdate"
                      placeholder="Date"
                      label="Date"
                      required
                      width="138"
                      height="36"
                      selected={new Date()}
                      onChange={() => {}}
                      calendarStartDay={1}
                      error={{ messages: ['Error'] }}
                      // readOnly
                    />
                  </TableCellNew>

                  <TableCellNew minWidth="100" className={spacing.pY12} padding="8">
                    <SelectNew
                      name="currency"
                      onChange={() => {}}
                      background={'transparent'}
                      border="transparent"
                      width="84"
                      height="36"
                      label=""
                      error
                      errorMessage="Currency not recognized"
                    >
                      {({ onClick, state, selectElement }) => (
                        <SelectDropdown selectElement={selectElement}>
                          <SelectDropdownBody>
                            <SelectDropdownItemList>
                              {options.map(({ label, value }) => (
                                <OptionSingleNew
                                  key={value}
                                  label={label}
                                  height="32"
                                  // type="radio"
                                  selected={state?.value === value}
                                  onClick={() =>
                                    onClick({
                                      label,
                                      value,
                                    })
                                  }
                                />
                              ))}
                            </SelectDropdownItemList>
                          </SelectDropdownBody>
                        </SelectDropdown>
                      )}
                    </SelectNew>
                  </TableCellNew>

                  <TableCellTextNew
                    minWidth="180"
                    value="Descriptions Text"
                    type="text-regular"
                    className={spacing.pY22}
                    flexgrow="1"
                  />

                  <TableCellNew
                    minWidth="190"
                    flexdirection="column"
                    justifycontent="center"
                    className={spacing.pY12}
                    padding="8"
                    gap="12"
                  >
                    <SelectNew
                      name="businessDivision1"
                      onChange={() => {}}
                      background={'transparent'}
                      border="transparent"
                      width="full"
                      height="36"
                      label=""
                      error
                      errorMessage="Business division #1"
                    >
                      {({ onClick, state, selectElement }) => (
                        <SelectDropdown selectElement={selectElement}>
                          <SelectDropdownBody>
                            <SelectDropdownItemList>
                              {options.map(({ label, value }) => (
                                <OptionSingleNew
                                  key={value}
                                  label={label}
                                  height="32"
                                  // type="radio"
                                  selected={state?.value === value}
                                  onClick={() =>
                                    onClick({
                                      label,
                                      value,
                                    })
                                  }
                                />
                              ))}
                            </SelectDropdownItemList>
                          </SelectDropdownBody>
                        </SelectDropdown>
                      )}
                    </SelectNew>
                    <SelectNew
                      name="businessDivision2"
                      onChange={() => {}}
                      background={'transparent'}
                      border="transparent"
                      width="full"
                      height="36"
                      label=""
                      error
                      errorMessage="Business division #2"
                    >
                      {({ onClick, state, selectElement }) => (
                        <SelectDropdown selectElement={selectElement}>
                          <SelectDropdownBody>
                            <SelectDropdownItemList>
                              {options.map(({ label, value }) => (
                                <OptionSingleNew
                                  key={value}
                                  label={label}
                                  height="32"
                                  // type="radio"
                                  selected={state?.value === value}
                                  onClick={() =>
                                    onClick({
                                      label,
                                      value,
                                    })
                                  }
                                />
                              ))}
                            </SelectDropdownItemList>
                          </SelectDropdownBody>
                        </SelectDropdown>
                      )}
                    </SelectNew>
                  </TableCellNew>

                  <TableCellNew
                    minWidth="280"
                    flexdirection="column"
                    justifycontent="center"
                    className={spacing.pY12}
                    padding="8"
                    gap="12"
                    fixedWidth
                  >
                    <TableSelect
                      name="account"
                      onChange={() => {}}
                      placeholder="Choose"
                      // icon={<InfoIcon />}
                      // tooltip="Please choose account"
                      // isRowActive={index === 2}
                      type="doubleHorizontal"
                      // disabled
                      error
                      errorMessage="Account error 1"
                    >
                      {({ onClick, state, selectElement }) => (
                        <SelectDropdown selectElement={selectElement} width="624">
                          <SelectDropdownBody>
                            <SelectDropdownItemList>
                              {options.map(({ label, secondaryLabel, value }) => (
                                <OptionWithTwoLabels
                                  id={label}
                                  key={value}
                                  label={label}
                                  labelDirection="horizontal"
                                  secondaryLabel={secondaryLabel}
                                  selected={state?.value === value}
                                  onClick={() =>
                                    onClick({
                                      label,
                                      secondaryLabel,
                                      value,
                                    })
                                  }
                                />
                              ))}
                            </SelectDropdownItemList>
                          </SelectDropdownBody>
                        </SelectDropdown>
                      )}
                    </TableSelect>
                    <TableSelect
                      name="account"
                      onChange={() => {}}
                      placeholder="Choose"
                      // icon={<InfoIcon />}
                      // tooltip="Please choose account"
                      // isRowActive={index === 2}
                      type="doubleHorizontal"
                      // disabled
                      error
                      errorMessage="Account error 2"
                    >
                      {({ onClick, state, selectElement }) => (
                        <SelectDropdown selectElement={selectElement} width="624">
                          <SelectDropdownBody>
                            <SelectDropdownItemList>
                              {options.map(({ label, secondaryLabel, value }) => (
                                <OptionDoubleHorizontalNew
                                  key={value}
                                  label={label}
                                  secondaryLabel={secondaryLabel}
                                  selected={state?.value === value}
                                  onClick={() =>
                                    onClick({
                                      label,
                                      secondaryLabel,
                                      value,
                                    })
                                  }
                                />
                              ))}
                            </SelectDropdownItemList>
                          </SelectDropdownBody>
                        </SelectDropdown>
                      )}
                    </TableSelect>
                  </TableCellNew>

                  <TableCellNew
                    minWidth="180"
                    flexdirection="column"
                    justifycontent="center"
                    className={spacing.pY12}
                    padding="8"
                    gap="12"
                    fixedWidth
                  >
                    <TableSelect
                      name="counterparty1"
                      onChange={() => {}}
                      placeholder="Choose"
                      // icon={<InfoIcon />}
                      // tooltip="Please choose account"
                      // isRowActive={index === 2}
                      // disabled
                      error
                      errorMessage="Counterparty 1 error"
                    >
                      {({ onClick, state, selectElement }) => (
                        <SelectDropdown selectElement={selectElement} width="624">
                          <SelectDropdownBody>
                            <SelectDropdownItemList>
                              {options.map(({ label, secondaryLabel, value }) => (
                                <OptionDoubleHorizontalNew
                                  key={value}
                                  label={label}
                                  secondaryLabel={secondaryLabel}
                                  selected={state?.value === value}
                                  onClick={() =>
                                    onClick({
                                      label,
                                      value,
                                    })
                                  }
                                />
                              ))}
                            </SelectDropdownItemList>
                          </SelectDropdownBody>
                        </SelectDropdown>
                      )}
                    </TableSelect>
                    <TableSelect
                      name="counterparty2"
                      onChange={() => {}}
                      placeholder="Choose"
                      // icon={<InfoIcon />}
                      // tooltip="Please choose account"
                      // isRowActive={index === 2}
                      // disabled
                      error
                      errorMessage="Counterparty 2 error"
                    >
                      {({ onClick, state, selectElement }) => (
                        <SelectDropdown selectElement={selectElement} width="624">
                          <SelectDropdownBody>
                            <SelectDropdownItemList>
                              {options.map(({ label, secondaryLabel, value }) => (
                                <OptionDoubleHorizontalNew
                                  key={value}
                                  label={label}
                                  secondaryLabel={secondaryLabel}
                                  selected={state?.value === value}
                                  onClick={() =>
                                    onClick({
                                      label,
                                      value,
                                    })
                                  }
                                />
                              ))}
                            </SelectDropdownItemList>
                          </SelectDropdownBody>
                        </SelectDropdown>
                      )}
                    </TableSelect>
                  </TableCellNew>

                  <TableCellNew
                    className={spacing.pY12}
                    minWidth="150"
                    flexdirection="column"
                    gap="12"
                    // justifycontent="center"
                    // alignitems="center"
                  >
                    {/* <NumericInput height="36" value="2000.00" suffix="$" error="Debit error" /> */}
                    <NumericInput
                      name="num-1"
                      label=""
                      className={spacing.mt48}
                      height="36"
                      width="114"
                      value={2000}
                      suffix="$"
                      error={{ messages: ['Debit error'] }}
                    />
                    {/* <Tag icon={<TickIcon />} color="green" /> */}
                    {/* <Tag icon={<CrossIcon />} color="red" /> */}
                  </TableCellNew>
                  <TableCellNew
                    className={spacing.pY12}
                    minWidth="150"
                    flexdirection="column"
                    gap="12"
                  >
                    <NumericInput
                      name="num-2"
                      label=""
                      height="36"
                      width="114"
                      value={2000}
                      suffix="$"
                      error={{ messages: ['Credit error'] }}
                    />
                  </TableCellNew>

                  <TableCellNew
                    className={spacing.pY16}
                    minWidth="110"
                    justifycontent="center"
                    alignitems="center"
                  >
                    <Tag icon={<TickIcon />} color="green" />
                    {/* <Tag icon={<CrossIcon />} color="red" /> */}
                  </TableCellNew>

                  <TableCellNew
                    className={spacing.pY16}
                    minWidth="130"
                    alignitems="center"
                    justifycontent="center"
                  >
                    <Button
                      width="80"
                      height="24"
                      font="text-medium"
                      onClick={() => {}}
                      iconLeft={<UploadIcon />}
                      background={'blue-10-blue-20'}
                      border="blue-10-blue-20"
                      color="violet-90-violet-100"
                    >
                      Import
                    </Button>
                  </TableCellNew>

                  {/* <TableCellTextNew
                    minWidth="110"
                    value="08 July 2021"
                    type="text-regular"
                    secondaryValue="14:37 PM"
                    className={spacing.pY16}
                  /> */}
                  {/* <TableCellTextNew
                    minWidth="110"
                    value="Frentinol"
                    type="text-regular"
                    className={spacing.pY16}
                  /> */}
                  {/* {activeCustomTableColumns.find((columnId) => columnId === 'businessDivision') && (
                    <TableCellTextNew
                      minWidth={tableHead['bd'].minWidth}
                      value="Business division name"
                      type="text-regular"
                      className={spacing.pY16}
                    />
                  )} */}
                  {/* {activeCustomTableColumns.find((columnId) => columnId === 'counterparty') && (
                    <TableCellTextNew
                      minWidth={tableHead['counterparty'].minWidth}
                      value="Client 1"
                      type="text-regular"
                      secondaryValue="Customer"
                      flexdirection="row"
                      justifycontent="space-between"
                      className={spacing.pY16}
                    />
                  )} */}
                  {/* {activeCustomTableColumns.find((columnId) => columnId === 'product') && (
                    <TableCellTextNew
                      minWidth={tableHead['product'].minWidth}
                      value="Product name"
                      type="text-regular"
                      className={spacing.pY16}
                    />
                  )} */}
                  {/* <TableCellNew minWidth="240" flexdirection="column" justifycontent="center">
                    <Container flexdirection="column" className={spacing.pY8}>
                      {item.record.map(() => (
                        <Container justifycontent="space-between" className={spacing.pY8}>
                          <Text lineClamp="1">Rental payments</Text>
                          <Text color="grey-100">Assets</Text>
                        </Container>
                      ))}
                    </Container>
                  </TableCellNew> */}
                  {/* {activeCustomTableColumns.find((columnId) => columnId === 'intercompany') && (
                    <TableCellTextNew
                      minWidth={tableHead['intercompany'].minWidth}
                      value="BetterME | Healthyandfit Inc"
                      type="text-regular"
                      className={spacing.pY16}
                    />
                  )} */}
                  {/* <TableCellNew minWidth="110" justifycontent="flex-end">
                    <Container flexdirection="column" className={spacing.pY8}>
                      {item.record.map((record) => (
                        <Container
                          flexdirection="column"
                          key={record.id}
                          className={classNames(spacing.h32fixed, spacing.pY8)}
                        >
                          {record.type === 'debit' && <Text noWrap>5,000,000.00 $</Text>}
                        </Container>
                      ))}
                    </Container>
                  </TableCellNew> */}
                  {/* <TableCellNew minWidth="110" justifycontent="flex-end">
                    <Container flexdirection="column" className={spacing.pY8}>
                      {item.record.map((record) => (
                        <Container
                          flexdirection="column"
                          key={record.id}
                          className={classNames(spacing.h32fixed, spacing.pY8)}
                        >
                          {record.type === 'credit' && <Text noWrap>1,000,000.00 $</Text>}
                        </Container>
                      ))}
                    </Container>
                  </TableCellNew> */}
                </TableRowNew>
              ))}
            </TableBodyNew>
          </TableNew>
        </Container>
      </Container>
    </Modal>
  );
};
