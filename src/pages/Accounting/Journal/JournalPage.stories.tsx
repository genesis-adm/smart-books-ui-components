import { Story } from '@storybook/react';
import { DropDown } from 'Components/DropDown';
import { Checkbox } from 'Components/base/Checkbox';
import { Divider } from 'Components/base/Divider';
import { LoadingLine } from 'Components/base/LoadingLine';
import { Button } from 'Components/base/buttons/Button';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { ContainerMain } from 'Components/base/grid/ContainerMain';
import { MainPageContentContainer } from 'Components/base/grid/MainPageContentContainer';
import { MenuTab, MenuTabWrapper } from 'Components/base/tabs/MenuTab';
import { Text } from 'Components/base/typography/Text';
import { MainPageEmpty } from 'Components/custom/MainPage/MainPageEmpty';
import { HeaderSB } from 'Components/custom/Stories/HeaderSB';
import { NavigationSB } from 'Components/custom/Stories/NavigationSB';
import { Pagination } from 'Components/custom/Stories/Pagination/Pagination';
import { ExportDropdown } from 'Components/elements/ExportDropdown';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { UserInfo } from 'Components/elements/UserInfo';
import { FilterDropDown } from 'Components/elements/filters/FilterDropDown';
import { FilterToggleButton } from 'Components/elements/filters/FilterToggleButton';
import { FiltersWrapper } from 'Components/elements/filters/FiltersWrapper';
import { Search } from 'Components/inputs/Search';
import { TableCellWidth } from 'Components/types/gridTypes';
import { journalTableItems } from 'Pages/Accounting/Journal/constants';
import { ReactComponent as IdIcon } from 'Svg/v2/12/clipboard-list.svg';
import { ReactComponent as HandIcon } from 'Svg/v2/12/hand.svg';
import { ReactComponent as PiechartIcon } from 'Svg/v2/16/piechart.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as SettingsIcon } from 'Svg/v2/16/settings.svg';
import { ReactComponent as DropdownIcon } from 'Svg/v2/16/three-dots.svg';
import { ReactComponent as UploadIcon } from 'Svg/v2/16/upload.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';
import { useToggle } from '../../../hooks/useToggle';

const filters = [
  { title: 'Account type' },
  { title: 'Currency' },
  { title: 'Business division' },
  { title: 'Legal entity' },
];

export type CustomJournalTableColumnId =
  | 'businessDivision'
  | 'intercompany'
  | 'product'
  | 'counterparty';

interface TableColumnOptions extends TableTitleNewProps {
  active: boolean;
}

type JournalTableColumnId =
  | 'checkbox'
  | 'id'
  | 'date'
  | 'createdBy'
  | 'le'
  | 'bd'
  | 'counterparty'
  | 'product'
  | 'account'
  | 'intercompany'
  | 'debit'
  | 'credit'
  | 'action';

const JournalPageStories: Story = ({ storyState }) => {
  const { isOpen: isOpenNavigationSB, onToggle: onToggleNavigationSB } = useToggle();
  const { isOpen: isOpenFilters, onToggle: onToggleFilters } = useToggle();
  const [activeCustomTableColumns, setActiveCustomTableColumns] = useState<
    CustomJournalTableColumnId[]
  >(['businessDivision', 'intercompany', 'product', 'counterparty']);

  const sorting = {
    sortType: null,
    tooltipText: 'Sorting tooltip text',
  };

  const tableHead: Record<JournalTableColumnId, TableColumnOptions> = {
    checkbox: {
      minWidth: '48',
      title: <Checkbox name="tickAll" checked onChange={() => {}} />,
      padding: 'none',
      active: true,
    },
    id: {
      minWidth: '120',
      title: 'ID',
      sorting,
      active: true,
    },
    date: {
      minWidth: '110',
      title: 'Date',
      sorting,
      active: true,
    },
    createdBy: {
      minWidth: '200',
      title: 'Created by',
      sorting,
      active: true,
    },
    le: {
      minWidth: '140',
      title: 'Legal Entity',
      sorting,
      active: true,
    },
    bd: {
      minWidth: '180',
      title: 'Business division',
      sorting,
      active: !!activeCustomTableColumns.find((columnId) => columnId === 'businessDivision'),
    },
    counterparty: {
      minWidth: '160',
      title: 'Counterparty',
      active: !!activeCustomTableColumns.find((columnId) => columnId === 'counterparty'),
    },
    product: {
      minWidth: '140',
      title: 'Product',
      sorting,
      active: !!activeCustomTableColumns.find((columnId) => columnId === 'product'),
    },
    account: {
      minWidth: '240',
      title: 'Account',
      sorting,
      active: true,
    },
    intercompany: {
      minWidth: '220',
      title: 'Intercompany',
      sorting,
      active: !!activeCustomTableColumns.find((columnId) => columnId === 'intercompany'),
    },
    debit: {
      minWidth: '160',
      title: 'Debit',
      align: 'right',
      sorting,
      active: true,
    },
    credit: {
      minWidth: '160',
      title: 'Credit',
      align: 'right',
      sorting,
      active: true,
    },
    action: {
      minWidth: '80',
      title: 'Action',
      align: 'center',
      active: true,
    },
  };

  const accountItemComponent = () => (
    <Container justifycontent="space-between" className={spacing.pY8}>
      <Text lineClamp="1">Rental payments</Text>
      <Text color="grey-100">Assets</Text>
    </Container>
  );

  return (
    <Container fullscreen flexwrap="nowrap" background={'grey-10'}>
      <NavigationSB opened={isOpenNavigationSB} onToggleOpen={onToggleNavigationSB} />
      <ContainerMain navOpened={isOpenNavigationSB}>
        <HeaderSB />
        <Container flexdirection="column" className={spacing.mX32}>
          <Text type="h1-semibold">Journal</Text>
          <MenuTabWrapper className={spacing.mt24}>
            <MenuTab id="chartOfAccounts" onClick={() => {}}>
              Chart of accounts
            </MenuTab>
            <MenuTab id="journal" active onClick={() => {}}>
              Journal
            </MenuTab>
            <MenuTab id="rules" onClick={() => {}}>
              Rules
            </MenuTab>
          </MenuTabWrapper>
        </Container>

        <MainPageContentContainer>
          {storyState === 'loaded' && (
            <>
              <Container
                className={classNames(spacing.mX24, spacing.mt24)}
                justifycontent="space-between"
                gap="24"
              >
                <Container gap="24" alignitems="center">
                  <FilterToggleButton
                    showFilters={isOpenFilters}
                    filtersSelected={false}
                    onClear={() => {}}
                    onClick={onToggleFilters}
                  />
                  <Search
                    width="320"
                    height="40"
                    name="search-journal-page"
                    placeholder="Search by ID and Account"
                    value=""
                    onChange={() => {}}
                    searchParams={[]}
                    onSearch={() => {}}
                    onClear={() => {}}
                  />
                </Container>

                <Container gap="24" alignitems="center">
                  <Button
                    background={'blue-10-blue-20'}
                    color="violet-90-violet-100"
                    width="140"
                    height="40"
                    iconLeft={<PiechartIcon />}
                  >
                    Run report
                  </Button>
                  <IconButton
                    size="40"
                    background={'grey-10-grey-20'}
                    color="grey-100"
                    icon={<SettingsIcon />}
                    onClick={() => {}}
                  />
                  <Divider type="vertical" background={'grey-90'} height="20" />
                  <ExportDropdown onExportToXLS={() => {}} onExportToCSV={() => {}} />
                  <IconButton
                    size="40"
                    background={'grey-10-grey-20'}
                    color="grey-100"
                    icon={<UploadIcon />}
                    onClick={() => {}}
                  />
                  <Button
                    background={'violet-90-violet-100'}
                    color="white-100"
                    width="140"
                    height="40"
                    iconLeft={<PlusIcon />}
                  >
                    Create
                  </Button>
                </Container>
              </Container>

              <FiltersWrapper active={isOpenFilters}>
                {filters.map((item) => (
                  <FilterDropDown
                    key={item.title}
                    title={item.title}
                    value="All"
                    onClear={() => {}}
                    selectedAmount={0}
                  />
                ))}
              </FiltersWrapper>

              <TableNew
                noResults={false}
                className={classNames(spacing.mt24, spacing.ml24, spacing.pr24)}
                tableHead={
                  <TableHeadNew>
                    {(Object.keys(tableHead) as JournalTableColumnId[]).map((item) => {
                      if (tableHead[item].active) {
                        return (
                          <TableTitleNew
                            key={item}
                            minWidth={tableHead[item].minWidth as TableCellWidth}
                            padding={tableHead[item].padding as TableTitleNewProps['padding']}
                            align={tableHead[item].align as TableTitleNewProps['align']}
                            title={tableHead[item].title}
                            sorting={tableHead[item].sorting}
                            onClick={() => {}}
                          />
                        );
                      }
                    })}
                  </TableHeadNew>
                }
              >
                <TableBodyNew>
                  {journalTableItems.map((item) => (
                    <TableRowNew
                      key={item.id}
                      size="auto"
                      showCollapseBtn={item.record.length > 4}
                      gap="8"
                    >
                      <TableCellNew
                        minWidth={tableHead['checkbox'].minWidth}
                        justifycontent="center"
                        alignitems="center"
                        className={spacing.pY16}
                      >
                        <Checkbox name={`line-${item.id}`} checked onChange={() => {}} />
                      </TableCellNew>
                      <TableCellTextNew
                        minWidth={tableHead['id'].minWidth}
                        value="ID 000001"
                        type="text-regular"
                        tagIcon={item.source === 'auto' ? <IdIcon /> : <HandIcon />}
                        secondaryTagText={item.is_new ? 'New' : ''}
                        secondaryTagColor="green"
                        className={spacing.pY16}
                      />
                      <TableCellTextNew
                        minWidth={tableHead['date'].minWidth}
                        value="08 July 2021"
                        type="text-regular"
                        secondaryValue="14:37 PM"
                        className={spacing.pY16}
                      />
                      <TableCellNew
                        minWidth={tableHead['createdBy'].minWidth}
                        className={spacing.pY12}
                      >
                        <UserInfo name="Liliya Sardaryan" isLogo />
                      </TableCellNew>
                      <TableCellTextNew
                        minWidth={tableHead['le'].minWidth}
                        value="Frentinol"
                        type="text-regular"
                        className={spacing.pY16}
                      />
                      {activeCustomTableColumns.find(
                        (columnId) => columnId === 'businessDivision',
                      ) && (
                        <TableCellTextNew
                          minWidth={tableHead['bd'].minWidth}
                          value="Business division name"
                          type="text-regular"
                          className={spacing.pY16}
                        />
                      )}
                      {activeCustomTableColumns.find((columnId) => columnId === 'counterparty') && (
                        <TableCellTextNew
                          minWidth={tableHead['counterparty'].minWidth}
                          value="Client 1"
                          type="text-regular"
                          secondaryValue="Customer"
                          flexdirection="row"
                          justifycontent="space-between"
                          className={spacing.pY16}
                        />
                      )}
                      {activeCustomTableColumns.find((columnId) => columnId === 'product') && (
                        <TableCellTextNew
                          minWidth={tableHead['product'].minWidth}
                          value="Product name"
                          type="text-regular"
                          className={spacing.pY16}
                        />
                      )}
                      <TableCellNew
                        minWidth={tableHead['account'].minWidth}
                        flexdirection="column"
                        justifycontent="center"
                      >
                        <Container flexdirection="column" className={spacing.pY8}>
                          {item.record.map(() => accountItemComponent())}
                        </Container>
                      </TableCellNew>
                      {activeCustomTableColumns.find((columnId) => columnId === 'intercompany') && (
                        <TableCellTextNew
                          minWidth={tableHead['intercompany'].minWidth}
                          value="BetterME | Healthyandfit Inc"
                          type="text-regular"
                          className={spacing.pY16}
                        />
                      )}
                      <TableCellNew
                        minWidth={tableHead['debit'].minWidth}
                        justifycontent="flex-end"
                      >
                        <Container flexdirection="column" className={spacing.pY8}>
                          {item.record.map((record) => (
                            <Container
                              flexdirection="column"
                              key={record.id}
                              className={classNames(spacing.h32fixed, spacing.pY8)}
                            >
                              {record.type === 'debit' && <Text noWrap>5,000,000.00 $</Text>}
                            </Container>
                          ))}
                        </Container>
                      </TableCellNew>
                      <TableCellNew
                        minWidth={tableHead['credit'].minWidth}
                        justifycontent="flex-end"
                      >
                        <Container flexdirection="column" className={spacing.pY8}>
                          {item.record.map((record) => (
                            <Container
                              flexdirection="column"
                              key={record.id}
                              className={classNames(spacing.h32fixed, spacing.pY8)}
                            >
                              {record.type === 'credit' && <Text noWrap>1,000,000.00 $</Text>}
                            </Container>
                          ))}
                        </Container>
                      </TableCellNew>

                      <TableCellNew
                        minWidth={tableHead['action'].minWidth}
                        alignitems="center"
                        justifycontent="center"
                        className={spacing.pY4}
                      >
                        <DropDown
                          flexdirection="column"
                          padding="8"
                          control={({ handleOpen }) => (
                            <IconButton
                              icon={<DropdownIcon />}
                              onClick={handleOpen}
                              background={'transparent'}
                              color="grey-100-violet-90"
                            />
                          )}
                        >
                          <DropDownButton size="160" icon={<PlusIcon />} onClick={() => {}}>
                            Edit
                          </DropDownButton>
                          <DropDownButton size="160" icon={<PlusIcon />} onClick={() => {}}>
                            Edit
                          </DropDownButton>
                          <DropDownButton size="160" icon={<PlusIcon />} onClick={() => {}}>
                            Edit
                          </DropDownButton>
                          <DropDownButton size="160" icon={<PlusIcon />} onClick={() => {}}>
                            Edit
                          </DropDownButton>
                          <DropDownButton size="160" icon={<PlusIcon />} onClick={() => {}}>
                            Edit
                          </DropDownButton>
                        </DropDown>
                      </TableCellNew>
                    </TableRowNew>
                  ))}
                </TableBodyNew>
              </TableNew>
              <Pagination padding="16" />
            </>
          )}
          {storyState === 'empty' && (
            <MainPageEmpty
              title="Create New Journal"
              description="You have no journal created. Your journal will be displayed here."
              action={
                <>
                  <Button
                    width="220"
                    height="40"
                    background={'blue-10-blue-20'}
                    color="violet-90-violet-100"
                    iconLeft={<PlusIcon />}
                    onClick={() => {}}
                  >
                    Create Journal
                  </Button>
                  <Button
                    width="220"
                    height="40"
                    background={'blue-10-blue-20'}
                    color="violet-90-violet-100"
                    iconLeft={<UploadIcon />}
                    onClick={() => {}}
                  >
                    Import from CSV
                  </Button>
                </>
              }
            />
          )}
        </MainPageContentContainer>
        <LoadingLine loadingCompleted={storyState !== 'loading'} />
      </ContainerMain>
    </Container>
  );
};

export default {
  title: 'Pages/Accounting/Journal/JournalPage',
  component: JournalPageStories,
  argTypes: {
    storyState: {
      options: ['empty', 'loading', 'loaded'],
      control: {
        type: 'radio',
        labels: {
          empty: 'Empty page',
          loading: 'Page is loading',
          loaded: 'Page loaded',
        },
      },
    },
  },
};

export const JournalPage = JournalPageStories.bind({});

JournalPage.args = {
  storyState: 'loaded',
};
