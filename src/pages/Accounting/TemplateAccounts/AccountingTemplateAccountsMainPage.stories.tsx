import { Meta, Story } from '@storybook/react';
import { DropDown } from 'Components/DropDown';
import { Divider } from 'Components/base/Divider';
import { Icon } from 'Components/base/Icon';
import { LoadingLine } from 'Components/base/LoadingLine';
import { Button } from 'Components/base/buttons/Button';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { ContainerMain } from 'Components/base/grid/ContainerMain';
import { MainPageContentContainer } from 'Components/base/grid/MainPageContentContainer';
import { MenuTab, MenuTabWrapper } from 'Components/base/tabs/MenuTab';
import { Text } from 'Components/base/typography/Text';
import { ChartOfAccountsList } from 'Components/custom/Accounting/TemplateAccounts/ChartOfAccountsList';
import { CurrencyList } from 'Components/custom/Accounting/TemplateAccounts/CurrencyList';
import { MainPageEmpty } from 'Components/custom/MainPage/MainPageEmpty';
import { HeaderSB } from 'Components/custom/Stories/HeaderSB';
import { NavigationSB } from 'Components/custom/Stories/NavigationSB';
import { Pagination } from 'Components/custom/Stories/Pagination/Pagination';
import { ExportDropdown } from 'Components/elements/ExportDropdown';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import {
  TableRowExpandable,
  TableRowExpandableWrapper,
} from 'Components/elements/Table/TableRowExpandable';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { FilterDropDown } from 'Components/elements/filters/FilterDropDown';
import { FilterToggleButton } from 'Components/elements/filters/FilterToggleButton';
import { FiltersWrapper } from 'Components/elements/filters/FiltersWrapper';
import { Search } from 'Components/inputs/Search';
import { ReactComponent as ChevronIcon } from 'Svg/v2/16/chevron-up.svg';
import { ReactComponent as LockClosedIcon } from 'Svg/v2/16/lock-closed.svg';
import { ReactComponent as LockOpenedIcon } from 'Svg/v2/16/lock-opened.svg';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import { ReactComponent as PiechartIcon } from 'Svg/v2/16/piechart.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as GearIcon } from 'Svg/v2/16/settings.svg';
import { ReactComponent as DropdownIcon } from 'Svg/v2/16/three-dots.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import { ReactComponent as UploadIcon } from 'Svg/v2/16/upload.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';
import type { TableCellWidth } from '../../../components/types/gridTypes';

const mainRow = {
  name: 'Account name',
  // currencies: ['EUR'],
  currencies: ['EUR', 'USD', 'UAH'],
  isSensitive: true,
  accountClass1: 'Asset',
  accountClass2: 'Current',
  type: 'Cash and cash equivalents',
  detailType: 'Cash in banks',
  description: 'Description text',
  onEdit: () => {},
  onDelete: () => {},
};

const RowExample = () => (
  <>
    <TableCellTextNew
      minWidth="220"
      value={mainRow.name}
      secondaryColor="grey-100"
      iconLeft={
        <Icon icon={mainRow.isSensitive ? <LockOpenedIcon /> : <LockClosedIcon />} path="grey-90" />
      }
    />
    <TableCellNew minWidth="160">
      <CurrencyList currencies={mainRow.currencies} />
    </TableCellNew>
    <TableCellTextNew
      minWidth="160"
      value={mainRow.accountClass1}
      secondaryValue={mainRow.accountClass2}
      secondaryColor="grey-100"
    />
    <TableCellTextNew
      minWidth="240"
      value={mainRow.type}
      secondaryValue={mainRow.detailType}
      secondaryColor="grey-100"
    />
    <TableCellTextNew minWidth="170" value={mainRow.description} flexgrow="1" />
    <TableCellNew minWidth="140" justifycontent="center" alignitems="center">
      <ChartOfAccountsList chartOfAccountsQuantity={4}>
        <Text type="button" color="grey-100" className={spacing.pb8}>
          Chart of Account 4
        </Text>
        {Array.from(Array(4)).map((_, index) => (
          <Container key={index} flexdirection="column">
            <Text type="body-regular-14">Other receivable account | USD</Text>
            <Text type="body-regular-14" color="grey-100">
              Business Division 2 | Legal entity 1, Legal entity 2
            </Text>
          </Container>
        ))}
      </ChartOfAccountsList>
    </TableCellNew>
    <TableCellNew minWidth="100" justifycontent="center" alignitems="center">
      <DropDown
        flexdirection="column"
        padding="8"
        control={({ handleOpen }) => (
          <IconButton
            icon={<DropdownIcon />}
            onClick={handleOpen}
            background={'transparent'}
            color="grey-100-violet-90"
          />
        )}
      >
        <DropDownButton size="160" icon={<EditIcon />} onClick={mainRow.onEdit}>
          Edit
        </DropDownButton>
        <DropDownButton size="160" type="danger" icon={<TrashIcon />} onClick={mainRow.onDelete}>
          Delete
        </DropDownButton>
      </DropDown>
    </TableCellNew>
  </>
);

const AccountingTemplateAccountsMainPageComponent: Story = ({ storyState }) => {
  const [toggle, toggleMenu] = useState<boolean>(false);
  const [toggleAllRow, setToggleAllRow] = useState<boolean>(false);
  const [showFilters, setShowFilters] = useState<boolean>(false);
  const sorting = {
    sortType: null,
    tooltipText: 'Sorting tooltip text',
  };
  const tableHead = [
    {
      minWidth: '48',
      title: (
        <IconButton
          size="24"
          background={'transparent'}
          border="grey-20"
          color="grey-100"
          icon={<ChevronIcon />}
          onClick={() => {
            setToggleAllRow(true);
            setTimeout(() => setToggleAllRow(false), 4);
          }}
        />
      ),
      padding: 'none',
    },
    {
      minWidth: '220',
      title: 'Account name',
      sorting,
    },
    {
      minWidth: '160',
      title: 'Currency',
    },
    {
      minWidth: '160',
      title: 'Account class',
    },
    {
      minWidth: '240',
      title: 'Type & Detail type',
      sorting,
    },
    {
      minWidth: '170',
      title: 'Description',
      flexgrow: '1',
    },
    {
      minWidth: '140',
      title: 'Chart of Account',
      align: 'center',
    },
    {
      minWidth: '100',
      title: 'Action',
      align: 'center',
    },
  ];
  const filters = [
    { title: 'Account name' },
    { title: 'Currency' },
    { title: 'Account type' },
    { title: 'Account detail type' },
  ];
  return (
    <Container fullscreen flexwrap="nowrap" background={'grey-10'}>
      <NavigationSB opened={toggle} onToggleOpen={() => toggleMenu(!toggle)} />
      <ContainerMain navOpened={toggle}>
        <HeaderSB />
        <Container flexdirection="column" className={spacing.mX32}>
          <Text type="h1-semibold">Accounting</Text>
          <MenuTabWrapper className={spacing.mt24}>
            <MenuTab id="chartOfAccounts" active={false} onClick={() => {}}>
              Chart of accounts
            </MenuTab>
            <MenuTab id="journal" active={false} onClick={() => {}}>
              Journal
            </MenuTab>
            <MenuTab id="templateAccounts" active onClick={() => {}}>
              Template accounts
            </MenuTab>
          </MenuTabWrapper>
        </Container>
        <MainPageContentContainer>
          {storyState === 'loaded' && (
            <>
              <Container
                className={classNames(spacing.mX24, spacing.mt24)}
                justifycontent="space-between"
                gap="24"
              >
                <Container gap="24" alignitems="center">
                  <FilterToggleButton
                    showFilters={showFilters}
                    filtersSelected={false}
                    onClear={() => {}}
                    onClick={() => {
                      setShowFilters((prevState) => !prevState);
                    }}
                  />
                  <Search
                    width="320"
                    height="40"
                    name="search-banking-transactions-mainPage"
                    placeholder="Search by Account name"
                    value=""
                    onChange={() => {}}
                    searchParams={[]}
                    onSearch={() => {}}
                    onClear={() => {}}
                  />
                </Container>
                <Container gap="24" alignitems="center">
                  <Button
                    background={'blue-10-blue-20'}
                    color="violet-90-violet-100"
                    width="140"
                    height="40"
                    iconLeft={<PiechartIcon />}
                  >
                    Run report
                  </Button>
                  <IconButton
                    size="40"
                    background={'grey-10-grey-20'}
                    color="grey-100"
                    icon={<GearIcon />}
                    onClick={() => {}}
                  />
                  <Divider type="vertical" background={'grey-90'} height="20" />
                  <ExportDropdown onExportToXLS={() => {}} onExportToCSV={() => {}} />
                  <IconButton
                    size="40"
                    background={'grey-10-grey-20'}
                    color="grey-100"
                    icon={<UploadIcon />}
                    onClick={() => {}}
                  />
                  <Button
                    background={'violet-90-violet-100'}
                    color="white-100"
                    width="140"
                    height="40"
                    iconLeft={<PlusIcon />}
                  >
                    Create
                  </Button>
                </Container>
              </Container>
              <FiltersWrapper active={showFilters}>
                {filters.map((item) => (
                  <FilterDropDown
                    key={item.title}
                    title={item.title}
                    value="All"
                    onClear={() => {}}
                    selectedAmount={0}
                  />
                ))}
              </FiltersWrapper>
              <TableNew
                noResults={false}
                className={classNames(spacing.mt24, spacing.ml24, spacing.pr24)}
                tableHead={
                  <TableHeadNew>
                    {tableHead.map((item, index) => (
                      <TableTitleNew
                        key={index}
                        minWidth={item.minWidth as TableCellWidth}
                        padding={item.padding as TableTitleNewProps['padding']}
                        align={item.align as TableTitleNewProps['align']}
                        flexgrow={item.flexgrow as TableTitleNewProps['flexgrow']}
                        title={item.title}
                        sorting={item.sorting}
                        onClick={() => {}}
                      />
                    ))}
                  </TableHeadNew>
                }
              >
                <TableBodyNew>
                  <TableRowExpandableWrapper isActive={false}>
                    <TableRowExpandable
                      type="primary"
                      isRowVisible
                      isToggleActive={false}
                      onToggle={() => {}}
                    >
                      <RowExample />
                    </TableRowExpandable>
                  </TableRowExpandableWrapper>
                  {Array.from(Array(4)).map((_, index) => (
                    <TableRowExpandableWrapper isActive key={index}>
                      <TableRowExpandable
                        type="primary"
                        isRowVisible
                        isToggleActive
                        onToggle={() => {}}
                      >
                        <RowExample />
                      </TableRowExpandable>
                      <TableRowExpandable isRowVisible isToggleActive onToggle={() => {}}>
                        <RowExample />
                      </TableRowExpandable>
                      <TableRowExpandable isRowVisible type="tertiary">
                        <RowExample />
                      </TableRowExpandable>
                      <TableRowExpandable isRowVisible type="tertiary">
                        <RowExample />
                      </TableRowExpandable>
                      <TableRowExpandable isRowVisible isToggleActive onToggle={() => {}}>
                        <RowExample />
                      </TableRowExpandable>
                      <TableRowExpandable isRowVisible type="tertiary">
                        <RowExample />
                      </TableRowExpandable>
                      <TableRowExpandable isRowVisible type="tertiary">
                        <RowExample />
                      </TableRowExpandable>
                    </TableRowExpandableWrapper>
                  ))}
                </TableBodyNew>
              </TableNew>
              <Pagination padding="16" />
            </>
          )}
          {storyState === 'empty' && (
            <MainPageEmpty
              title="Template accounts"
              description="You have no template accounts created. Your template accounts will be displayed here."
              action={
                <Container gap="24">
                  <Button
                    width="220"
                    height="40"
                    background={'blue-10-blue-20'}
                    color="violet-90-violet-100"
                    iconLeft={<PlusIcon />}
                    onClick={() => {}}
                  >
                    Create template account
                  </Button>
                  <Button
                    width="220"
                    height="40"
                    background={'blue-10-blue-20'}
                    color="violet-90-violet-100"
                    iconLeft={<UploadIcon />}
                    onClick={() => {}}
                  >
                    Import from CSV
                  </Button>
                </Container>
              }
            />
          )}
        </MainPageContentContainer>
        <LoadingLine loadingCompleted={storyState !== 'loading'} />
      </ContainerMain>
    </Container>
  );
};

export default {
  title: 'Pages/Accounting/Template Accounts/Accounting Template Accounts Main Page',
  component: AccountingTemplateAccountsMainPageComponent,
  argTypes: {
    storyState: {
      options: ['empty', 'loading', 'loaded'],
      control: {
        type: 'radio',
        labels: {
          empty: 'Empty page',
          loading: 'Page is loading',
          loaded: 'Page loaded',
        },
      },
    },
  },
} as Meta;

export const AccountingTemplateAccountsMainPage = AccountingTemplateAccountsMainPageComponent.bind(
  {},
);
AccountingTemplateAccountsMainPage.args = {
  storyState: 'loaded',
};
