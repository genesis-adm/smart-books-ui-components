import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownFooter } from 'Components/SelectDropdown/components/SelectDropdownFooter';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { Divider } from 'Components/base/Divider';
import { Icon } from 'Components/base/Icon';
import { Tag } from 'Components/base/Tag';
import { Button } from 'Components/base/buttons/Button';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { InputNew } from 'Components/base/inputs/InputNew';
import { SelectNew } from 'Components/base/inputs/SelectNew';
import { OptionSingleNew } from 'Components/base/inputs/SelectNew/options/OptionSingleNew';
import { TextareaNew } from 'Components/base/inputs/TextareaNew';
import { Text } from 'Components/base/typography/Text';
import { AccordionAccountItem } from 'Components/custom/Accounting/ChartOfAccounts/AccordionAccountItem';
import { AccordionAccountSubItem } from 'Components/custom/Accounting/ChartOfAccounts/AccordionAccountSubItem';
import { AccountClassType } from 'Components/custom/Accounting/ChartOfAccounts/AccountClassType';
import { SensitiveAccountToggle } from 'Components/custom/Accounting/ChartOfAccounts/SensitiveAccountToggle';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableTitleNew } from 'Components/elements/Table/TableTitleNew';
import { Search } from 'Components/inputs/Search';
import { OptionWithSingleLabel } from 'Components/inputs/Select/options/OptionWithSingleLabel';
import { OptionsFooter } from 'Components/inputs/Select/options/OptionsFooter';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { options } from 'Mocks/fakeOptions';
import { ReactComponent as ArrowsSwitchIcon } from 'Svg/v2/12/arrows.svg';
import { ReactComponent as ArrowLeftIcon } from 'Svg/v2/16/arrow-left.svg';
import { ReactComponent as LiabliltiesIcon } from 'Svg/v2/16/bank-cheque.svg';
import { ReactComponent as ActiveIcon } from 'Svg/v2/16/clipboard-tick.svg';
import { ReactComponent as CloseIcon } from 'Svg/v2/16/cross.svg';
import { ReactComponent as DocumentIcon } from 'Svg/v2/16/file-blank.svg';
import { ReactComponent as IncomeIcon } from 'Svg/v2/16/money-in.svg';
import { ReactComponent as ExpensesIcon } from 'Svg/v2/16/money-out.svg';
import { ReactComponent as AssetIcon } from 'Svg/v2/16/money-stack.svg';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as EquityIcon } from 'Svg/v2/16/scales.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Accounting/Template Accounts/Template Accounts Modals',
};

const CHART_OF_ACCOUNTS_LENGTH = 20;

const SideMenu = () => {
  const [itemActive, setItemActive] = useState<boolean>(false);

  return (
    <Container
      flexdirection="column"
      background="white-100"
      className={classNames(spacing.w320fixed, spacing.h100p, spacing.p24)}
      gap="16"
    >
      <Container className={spacing.pr6} flexdirection="column" gap="16" customScroll flexgrow="1">
        <AccordionAccountItem
          name="Asset"
          counter={2}
          icon={<AssetIcon />}
          isActive={itemActive}
          onClick={() => setItemActive((prev) => !prev)}
          onAddClick={() => {}}
          onEdit={() => {}}
          onDelete={() => {}}
        >
          <AccordionAccountSubItem
            name="Level 1"
            nestingLevel={1}
            isActive={itemActive}
            isOpen={itemActive}
            onClick={() => {}}
            onIconClick={() => setItemActive((prev) => !prev)}
            onAddClick={() => {}}
            onEdit={() => {}}
            onDelete={() => {}}
          >
            <AccordionAccountSubItem
              name="Level 2"
              nestingLevel={2}
              isActive={itemActive}
              isOpen={itemActive}
              onClick={() => {}}
              onIconClick={() => setItemActive((prev) => !prev)}
              onAddClick={() => {}}
              onEdit={() => {}}
              onDelete={() => {}}
            >
              <AccordionAccountSubItem
                name="Level 3"
                nestingLevel={3}
                isActive={false}
                isOpen={false}
                addButtonShown={false}
                onClick={() => {}}
                onAddClick={() => {}}
                onEdit={() => {}}
                onDelete={() => {}}
              />
            </AccordionAccountSubItem>
          </AccordionAccountSubItem>
          <AccordionAccountSubItem
            name="Level 1"
            nestingLevel={1}
            isActive={itemActive}
            isOpen={itemActive}
            onClick={() => {}}
            onIconClick={() => setItemActive((prev) => !prev)}
            onAddClick={() => {}}
            onEdit={() => {}}
            onDelete={() => {}}
          >
            <AccordionAccountSubItem
              name="Level 2"
              nestingLevel={2}
              isActive={false}
              isOpen={false}
              onClick={() => {}}
              onAddClick={() => {}}
              onEdit={() => {}}
              onDelete={() => {}}
            />
          </AccordionAccountSubItem>
          <AccordionAccountSubItem
            name="Level 1"
            nestingLevel={1}
            isActive={false}
            isOpen={false}
            onClick={() => {}}
            onAddClick={() => {}}
            onEdit={() => {}}
            onDelete={() => {}}
          />
        </AccordionAccountItem>
        <Divider fullHorizontalWidth />
        <AccordionAccountItem
          name="Liabilities"
          counter={12}
          icon={<LiabliltiesIcon />}
          isActive={false}
          onClick={() => {}}
          onAddClick={() => {}}
          onEdit={() => {}}
          onDelete={() => {}}
        />
        <Divider fullHorizontalWidth />
        <AccordionAccountItem
          name="Equity"
          counter={18}
          icon={<EquityIcon />}
          isActive
          onClick={() => {}}
          onAddClick={() => {}}
          onEdit={() => {}}
          onDelete={() => {}}
        >
          {/* <Container
              flexdirection="column"
              gap="12"
              justifycontent="center"
              alignitems="center"
              alignself="center"
              className={classNames(spacing.w220, spacing.p32)}
            >
              <EmptyChartOfAccountIcon />
              <Text color="grey-100" type="body-regular-14" align="center">
                You do not have any chart of account
              </Text>
            </Container> */}
        </AccordionAccountItem>
        <Divider fullHorizontalWidth />
        <AccordionAccountItem
          name="Income"
          counter={2}
          icon={<IncomeIcon />}
          isActive={false}
          onClick={() => {}}
          onAddClick={() => {}}
          onEdit={() => {}}
          onDelete={() => {}}
        />
        <Divider fullHorizontalWidth />
        <AccordionAccountItem
          name="Expenses"
          counter={2}
          icon={<ExpensesIcon />}
          isActive={false}
          onClick={() => {}}
          onAddClick={() => {}}
          onEdit={() => {}}
          onDelete={() => {}}
        />
      </Container>
    </Container>
  );
};

export const SelectAccountClass: React.FC = () => {
  return (
    <Modal
      size="1040"
      background="grey-10"
      type="new"
      pluginScrollDisabled
      padding="none"
      flexdirection="row"
    >
      <SideMenu />
      <Container gap="24" className={spacing.p24} flexdirection="column" flexgrow="1">
        <Container justifycontent="space-between">
          <Container gap="24" alignitems="center">
            <IconButton
              background="transparent"
              size="24"
              icon={<ArrowLeftIcon />}
              color="grey-100-black-100"
              onClick={() => {}}
            />
            <Text color="grey-100" type="body-medium">
              Template accounts
            </Text>
          </Container>
          <Container gap="16" alignitems="center">
            <IconButton
              background="white-100"
              size="24"
              icon={<CloseIcon />}
              color="black-100"
              onClick={() => {}}
            />
          </Container>
        </Container>
        <Container customScroll flexdirection="column" flexgrow="1">
          <Container
            flexdirection="column"
            justifycontent="center"
            alignitems="center"
            flexgrow="1"
            gap="40"
          >
            <Text type="body-medium" align="center" className={spacing.w240}>
              Please choose Account class to create template account
            </Text>
            <Container justifycontent="center">
              <AccountClassType type="Assets" onClick={() => {}} />
              <AccountClassType type="Liabilities" onClick={() => {}} />
              <AccountClassType type="Equity" onClick={() => {}} />
            </Container>
            <Container justifycontent="center">
              <AccountClassType type="Income" onClick={() => {}} />
              <AccountClassType type="Expenses" onClick={() => {}} />
            </Container>
          </Container>
        </Container>
      </Container>
    </Modal>
  );
};

export const CreateNewTemplate: React.FC = () => {
  type CurrenciesType = {
    id: string;
    label: string;
    onDelete(): void;
  };
  const currencies = [
    {
      id: 'usd0',
      label: 'USD',
      onDelete: () => {},
    },
    {
      id: 'usd1',
      label: 'USD',
      onDelete: () => {},
    },
    {
      id: 'usd2',
      label: 'UAH',
      onDelete: () => {},
    },
    {
      id: 'usd3',
      label: 'EUR',
      onDelete: () => {},
    },
    {
      id: 'usd4',
      label: 'FGF',
      onDelete: () => {},
    },
    {
      id: 'usd5',
      label: 'FTY',
      onDelete: () => {},
    },
    {
      id: 'usd6',
      label: 'YTU',
      onDelete: () => {},
    },
    {
      id: 'usd7',
      label: 'TYU',
      onDelete: () => {},
    },
    {
      id: 'usd8',
      label: 'TY7',
      onDelete: () => {},
    },
    {
      id: 'usd9',
      label: 'RTY',
      onDelete: () => {},
    },
    {
      id: 'usd10',
      label: 'FDF',
      onDelete: () => {},
    },
  ];
  const [isSensitive, setIsSensitive] = useState<boolean>(false);
  const [currenciesTag, setCurrenciesTag] = useState<CurrenciesType[]>([]);
  const [searchValue, setSearchValue] = useState('');
  const handleToggleCurrency = (curr: CurrenciesType) => () => {
    setCurrenciesTag((prevState) => {
      return prevState.some((prev) => prev.id.includes(curr.id))
        ? prevState.filter((item) => item.id !== curr.id)
        : [...prevState, curr];
    });
  };
  return (
    <Modal
      size="1040"
      background="grey-10"
      type="new"
      pluginScrollDisabled
      padding="none"
      flexdirection="row"
    >
      <SideMenu />
      <Container gap="24" className={spacing.p24} flexdirection="column" flexgrow="1">
        <Container justifycontent="space-between">
          <Container gap="24" alignitems="center">
            <IconButton
              background="transparent"
              size="24"
              icon={<ArrowLeftIcon />}
              color="grey-100-black-100"
              onClick={() => {}}
            />
            <Text color="grey-100" type="body-medium">
              Template accounts / Asset / New
            </Text>
          </Container>
          <Container gap="16" alignitems="center">
            <Tag icon={<DocumentIcon />} padding="4-8" cursor="default" text="New" color="green" />
            <IconButton
              background="white-100"
              size="24"
              icon={<CloseIcon />}
              color="black-100"
              onClick={() => {}}
            />
          </Container>
        </Container>
        <Container flexdirection="column" flexgrow="1">
          <Container
            borderRadius="20"
            background="white-100"
            flexdirection="column"
            className={classNames(spacing.p24)}
            gap="24"
            flexgrow="1"
            style={{ boxShadow: '0px 5px 17px rgba(153, 152, 152, 0.18)' }}
          >
            <Container gap="24" flexdirection="column" flexgrow="1">
              <Container justifycontent="space-between">
                <Text type="body-regular-14">Create template account</Text>
                <SensitiveAccountToggle
                  isSensitive={isSensitive}
                  onClick={() => setIsSensitive((prev) => !prev)}
                />
              </Container>
              <InputNew
                name="accountName"
                label="Account name"
                width="full"
                value="Test name"
                onChange={() => {}}
              />
              <SelectNew
                name="currency"
                label="Currency"
                onChange={() => {}}
                width="624"
                required
                tags={currenciesTag}
                tagColor="grey"
                height="full"
                wrapTags
              >
                {({ onClick, state, selectElement }) => (
                  <SelectDropdown selectElement={selectElement}>
                    <Search
                      className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                      width="full"
                      height="40"
                      name="search-LE"
                      placeholder="Search currency"
                      value={searchValue}
                      onChange={(e) => setSearchValue(e.target.value)}
                      searchParams={[]}
                      onSearch={() => {}}
                      onClear={() => {}}
                    />
                    <SelectDropdownBody>
                      <SelectDropdownItemList>
                        {currencies.map((item) => (
                          <OptionWithSingleLabel
                            id={item.id}
                            key={item.id}
                            label={item.label}
                            height="32"
                            type="checkbox"
                            selected={currenciesTag.some((tag) => tag.id === item.id)}
                            onClick={handleToggleCurrency(item)}
                          />
                        ))}
                      </SelectDropdownItemList>
                    </SelectDropdownBody>
                    <SelectDropdownFooter>
                      <OptionsFooter
                        optionsLength={currencies.length}
                        selectedOptionsNumber={currenciesTag.length}
                        checked={currenciesTag.length === currencies.length}
                        onSelectChange={() =>
                          currenciesTag.length !== currencies.length
                            ? setCurrenciesTag(currencies)
                            : setCurrenciesTag([])
                        }
                        onClear={() => setCurrenciesTag([])}
                        onApply={() =>
                          onClick({
                            value: '',
                            label: '',
                          })
                        }
                      />
                    </SelectDropdownFooter>
                  </SelectDropdown>
                )}
              </SelectNew>
              <Container gap="24">
                <SelectNew
                  name="accountType"
                  onChange={() => {}}
                  width="300"
                  required
                  label="Account type"
                >
                  {({ onClick, state, selectElement }) => (
                    <SelectDropdown selectElement={selectElement}>
                      <Search
                        className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                        width="full"
                        height="40"
                        name="search-LE"
                        placeholder="Search currency"
                        value={searchValue}
                        onChange={(e) => setSearchValue(e.target.value)}
                        searchParams={[]}
                        onSearch={() => {}}
                        onClear={() => {}}
                      />
                      <SelectDropdownBody>
                        <SelectDropdownItemList>
                          {options.map(({ label, value }) => (
                            <OptionWithSingleLabel
                              id={label}
                              key={value}
                              label={label}
                              selected={state?.value === value}
                              onClick={() =>
                                onClick({
                                  label,
                                  value,
                                })
                              }
                            />
                          ))}
                        </SelectDropdownItemList>
                      </SelectDropdownBody>
                    </SelectDropdown>
                  )}
                </SelectNew>
                <SelectNew
                  name="detailType"
                  label="Detail type"
                  onChange={() => {}}
                  width="300"
                  required
                >
                  {({ onClick, state, selectElement }) => (
                    <SelectDropdown selectElement={selectElement}>
                      <SelectDropdownBody>
                        <SelectDropdownItemList>
                          {options.map(({ label, value }) => (
                            <OptionSingleNew
                              key={value}
                              label={label}
                              selected={state?.value === value}
                              onClick={() =>
                                onClick({
                                  label,
                                  value,
                                })
                              }
                            />
                          ))}
                        </SelectDropdownItemList>
                      </SelectDropdownBody>
                    </SelectDropdown>
                  )}
                </SelectNew>
              </Container>
              <TextareaNew
                label="Notes"
                name="notes"
                placeholder="Enter"
                value=""
                onChange={() => {}}
              />
              {/* <Container
                background="grey-10"
                border="grey-20"
                borderRadius="10"
                justifycontent="space-between"
                alignitems="center"
                className={classNames(spacing.w100p, spacing.p24)}
              >
                <Container flexdirection="column" gap="8" className={spacing.w240}>
                  <Text type="body-medium">Business Structure</Text>
                  <Text type="body-regular-14" color="grey-100">
                    Please add new Business Structure to invite new team members
                  </Text>
                  <Button
                    width="108"
                    height="24"
                    background="blue-10-blue-20"
                    color="violet-90-violet-100"
                    iconLeft={<PlusIcon />}
                    onClick={() => {}}
                  >
                    Add new
                  </Button>
                </Container>
                <Icon icon={<EmptyChartOfAccountIcon />} />
              </Container> */}
              <Container
                background="grey-10"
                border="grey-20"
                borderRadius="10"
                flexdirection="column"
                className={classNames(spacing.w100p, spacing.pX24, spacing.pY16, spacing.h200fixed)}
              >
                <Container justifycontent="space-between" alignitems="center">
                  <Text type="body-regular-14" color="grey-100">
                    Business Structure
                  </Text>
                  <IconButton
                    icon={<EditIcon />}
                    size="24"
                    background="transparent"
                    color="grey-100-yellow-90"
                    onClick={() => {}}
                  />
                </Container>
                <TableNew
                  noResults={false}
                  tableHead={
                    <TableHeadNew background="grey-10" margin="none">
                      <TableTitleNew background="grey-10" minWidth="32" padding="none" title="#" />
                      <TableTitleNew
                        background="grey-10"
                        minWidth="180"
                        title="Business Division"
                      />
                      <TableTitleNew
                        background="grey-10"
                        minWidth="180"
                        title="Legal entity"
                        flexgrow="1"
                      />
                    </TableHeadNew>
                  }
                >
                  <TableBodyNew>
                    {Array.from(Array(1)).map((_, index) => (
                      <TableRowNew
                        key={index}
                        background="transparent"
                        size="28"
                        cursor="auto"
                        gap="8"
                      >
                        <TableCellTextNew
                          minWidth="32"
                          padding="8"
                          align="center"
                          color="grey-100"
                          value={`${index + 1}`}
                        />
                        <TableCellNew
                          minWidth="180"
                          fixedWidth
                          alignitems="center"
                          justifycontent="space-between"
                          padding="12-0"
                          gap="8"
                        >
                          <Tag
                            color="grey-black"
                            font="body-regular-14"
                            padding="0-4"
                            text={`Some name of business # ${index + 1}`}
                            noWrap
                            flexShrink="1"
                            align="left"
                            // cursor="default"
                          />
                          <Icon icon={<ArrowsSwitchIcon />} />
                        </TableCellNew>
                        <TableCellNew
                          minWidth="180"
                          alignitems="center"
                          justifycontent="space-between"
                          padding="12-0"
                          gap="8"
                          flexgrow="1"
                        >
                          <Tag
                            color="grey-black"
                            font="body-regular-14"
                            padding="0-4"
                            text={`Legal Entity # 00${index + 1}`}
                            align="left"
                            additionalCounter={2}
                            tooltip={
                              <Container flexdirection="column">
                                <Text color="white-100">Legal Entity #001-1</Text>
                                <Text color="white-100">Legal Entity #001-2</Text>
                                <Text color="white-100">Legal Entity #001-3</Text>
                              </Container>
                            }
                          />
                        </TableCellNew>
                      </TableRowNew>
                    ))}
                  </TableBodyNew>
                </TableNew>
              </Container>
            </Container>
            <Divider fullHorizontalWidth />
            <Container justifycontent="flex-end">
              <Button height="40" onClick={() => {}}>
                Create
              </Button>
            </Container>
          </Container>
        </Container>
      </Container>
    </Modal>
  );
};

export const ViewTemplate: React.FC = () => {
  return (
    <Modal
      size="1040"
      background="grey-10"
      type="new"
      pluginScrollDisabled
      padding="none"
      flexdirection="row"
    >
      <SideMenu />
      <Container gap="24" className={spacing.p24} flexdirection="column" flexgrow="1">
        <Container justifycontent="space-between">
          <Container gap="24" alignitems="center">
            <IconButton
              background="transparent"
              size="24"
              icon={<ArrowLeftIcon />}
              color="grey-100-black-100"
              onClick={() => {}}
            />
            <Text color="grey-100" type="body-medium">
              Template accounts / Asset / Other receivable account
            </Text>
          </Container>
          <Container gap="16" alignitems="center">
            <Tag
              icon={<ActiveIcon />}
              padding="4-8"
              cursor="default"
              text="Active"
              color="violet"
            />
            <IconButton
              background="white-100"
              size="24"
              icon={<CloseIcon />}
              color="black-100"
              onClick={() => {}}
            />
          </Container>
        </Container>
        <Container flexdirection="column" flexgrow="1" gap="24" overflow="hidden">
          <Container
            borderRadius="20"
            background="white-100"
            flexdirection="column"
            className={classNames(spacing.p24)}
            gap="24"
          >
            <Container justifycontent="space-between" alignitems="center">
              <Text type="body-regular-14">Chart of Account information</Text>
              <IconButton
                icon={<EditIcon />}
                size="24"
                background="transparent"
                color="grey-100-yellow-90"
                onClick={() => {}}
              />
            </Container>
            <Container flexdirection="column" gap="12">
              <Container alignitems="center">
                <Text className={spacing.w120fixed} color="grey-100">
                  Account name
                </Text>
                <Text type="body-regular-14">Other receivable account</Text>
              </Container>
              <Container alignitems="center">
                <Text className={spacing.w120fixed} color="grey-100">
                  Account type*
                </Text>
                <Text type="body-regular-14">Receivables</Text>
              </Container>
              <Container alignitems="center">
                <Text className={spacing.w120fixed} color="grey-100">
                  Detail type*
                </Text>
                <Text type="body-regular-14">Other Receivables</Text>
              </Container>
              <Container alignitems="center">
                <Text className={spacing.w120fixed} color="grey-100">
                  Notes
                </Text>
                <Text type="body-regular-14">Other Receivables</Text>
              </Container>
              <Container alignitems="center">
                <Text className={spacing.w120fixed} color="grey-100">
                  Access
                </Text>
                <Text type="body-regular-14">Non - sensitive account</Text>
              </Container>
            </Container>
          </Container>
          <Container
            borderRadius="20"
            background="white-100"
            flexdirection="column"
            className={classNames(spacing.p24)}
            gap="16"
            flexgrow="1"
            overflow="hidden"
          >
            <Text type="body-regular-14">Chart of Account 4</Text>
            <Container flexdirection="column" customScroll gap="16">
              {Array.from(Array(CHART_OF_ACCOUNTS_LENGTH)).map((_, index) => (
                <>
                  <Container key={index} justifycontent="space-between" alignitems="center">
                    <Container flexdirection="column" gap="4">
                      <Text type="body-regular-14">Other receivable account | USD</Text>
                      <Container gap="16" alignitems="center">
                        <Tag
                          color="grey-black"
                          font="body-regular-14"
                          padding="0-4"
                          text="Business Division 2"
                          cursor="default"
                        />
                        <Icon icon={<ArrowsSwitchIcon />} />
                        <Tag
                          color="grey-black"
                          font="body-regular-14"
                          padding="0-4"
                          text="Legal entity 1"
                          cursor="default"
                        />
                      </Container>
                    </Container>
                    <IconButton
                      icon={<TrashIcon />}
                      background="transparent"
                      color="grey-100-red-90"
                      onClick={() => {}}
                    />
                  </Container>
                  {index < CHART_OF_ACCOUNTS_LENGTH - 1 && <Divider fullHorizontalWidth />}
                </>
              ))}
            </Container>
          </Container>
          <Divider fullHorizontalWidth />
          <Container justifycontent="flex-end">
            <Button
              background="white-100"
              border="grey-20-grey-90"
              color="grey-100"
              width="156"
              height="40"
              onClick={() => {}}
            >
              Close
            </Button>
          </Container>
        </Container>
      </Container>
    </Modal>
  );
};

export const AddBusinessStructure: React.FC = () => {
  return (
    <Modal
      size="960"
      background="white-100"
      header={
        <ModalHeaderNew
          title="Add business structure"
          border
          titlePosition="center"
          onClose={() => {}}
        />
      }
      pluginScrollDisabled
      footer={
        <ModalFooterNew justifycontent="space-between" border>
          <Button height="48" width="180" onClick={() => {}}>
            Save
          </Button>
          {/* <Container alignitems="center" gap="16" flexgrow="1" justifycontent="flex-end">
            <Container flexdirection="column">
              <Text type="body-regular-14" color="grey-100">
                Step: 1 of 2
              </Text>
              <Text type="body-regular-14" color="grey-100">
                Next: Set up access
              </Text>
            </Container>
            <Button height="48" iconRight={<ArrowRightIcon />} navigation width="180" onClick={() => {}}>
              Next
            </Button>
          </Container> */}
          <Container alignitems="center" gap="8" className={spacing.p8} radius border="grey-10">
            <Text color="grey-100" type="body-regular-14">
              Add all available business structures
              {/* Clear all business structures */}
            </Text>
            <Button
              height="32"
              width="80"
              background="blue-10-blue-20"
              color="violet-90"
              onClick={() => {}}
            >
              Add all
              {/* Clear all */}
            </Button>
          </Container>
        </ModalFooterNew>
      }
    >
      <TableNew
        noResults={false}
        className={classNames(spacing.pX20, spacing.mY24)}
        tableHead={
          <TableHeadNew background="white-100">
            <TableTitleNew background="white-100" minWidth="48" padding="none" title="#" />
            <TableTitleNew background="white-100" minWidth="380" title="Business Division" />
            <TableTitleNew background="white-100" minWidth="350" title="Legal entity" />
            <TableTitleNew background="white-100" minWidth="120" title="Actions" align="center" />
          </TableHeadNew>
        }
      >
        <TableBodyNew>
          {Array.from(Array(3)).map((_, index) => (
            <TableRowNew key={index} background="white" cursor="auto" gap="8">
              <TableCellTextNew
                minWidth="48"
                padding="8"
                align="center"
                color="grey-100"
                value={`${index + 1}`}
              />
              <TableCellNew
                minWidth="380"
                justifycontent="space-between"
                alignitems="center"
                padding="16-0"
              >
                <SelectNew
                  name="businessDivision"
                  onChange={() => {}}
                  width="320"
                  required
                  height="36"
                  label=""
                  placeholder="Select business divisions"
                >
                  {({ onClick, state, selectElement }) => (
                    <SelectDropdown selectElement={selectElement}>
                      <Search
                        className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                        width="full"
                        height="40"
                        name="search-BD"
                        placeholder="Search business divisions"
                        value=""
                        onChange={() => {}}
                        searchParams={[]}
                        onSearch={() => {}}
                        onClear={() => {}}
                      />
                      <SelectDropdownBody>
                        <SelectDropdownItemList>
                          {options.map(({ label, value }) => (
                            <OptionSingleNew
                              key={value}
                              label={label}
                              height="32"
                              type="radio"
                              selected={state?.value === value}
                              onClick={() =>
                                onClick({
                                  label,
                                  value,
                                })
                              }
                            />
                          ))}
                        </SelectDropdownItemList>
                      </SelectDropdownBody>
                    </SelectDropdown>
                  )}
                </SelectNew>
                <Icon icon={<ArrowsSwitchIcon />} />
              </TableCellNew>
              <TableCellNew minWidth="350">
                <SelectNew
                  name="legalEntity"
                  onChange={() => {}}
                  width="320"
                  required
                  height="36"
                  label=""
                  placeholder="Select legal entities"
                >
                  {({ onClick, state, selectElement }) => (
                    <SelectDropdown selectElement={selectElement}>
                      <Search
                        className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                        width="full"
                        height="40"
                        name="search-LE"
                        placeholder="Search legal entities"
                        value=""
                        onChange={() => {}}
                        searchParams={[]}
                        onSearch={() => {}}
                        onClear={() => {}}
                      />
                      <SelectDropdownBody>
                        <SelectDropdownItemList>
                          {options.map(({ label, value }) => (
                            <OptionSingleNew
                              key={value}
                              label={label}
                              height="32"
                              type="checkbox"
                              selected={state?.value === value}
                              onClick={() =>
                                onClick({
                                  label,
                                  value,
                                })
                              }
                            />
                          ))}
                        </SelectDropdownItemList>
                      </SelectDropdownBody>
                      <SelectDropdownFooter>
                        <Container
                          justifycontent="flex-end"
                          className={classNames(spacing.mX12, spacing.mY8)}
                        >
                          <Button width="108" height="32" onClick={() => {}}>
                            Apply
                          </Button>
                        </Container>
                      </SelectDropdownFooter>
                    </SelectDropdown>
                  )}
                </SelectNew>
              </TableCellNew>
              <TableCellNew minWidth="120" justifycontent="center" alignitems="center">
                <IconButton
                  icon={<TrashIcon />}
                  background="transparent"
                  color="grey-100-red-90"
                  onClick={() => {}}
                />
              </TableCellNew>
            </TableRowNew>
          ))}
          <Container justifycontent="flex-end">
            <Button
              width="120"
              height="24"
              background="blue-10-blue-20"
              color="violet-90-violet-100"
              font="text-medium"
              iconLeft={<PlusIcon />}
              onClick={() => {}}
            >
              Add new line
            </Button>
          </Container>
        </TableBodyNew>
      </TableNew>
    </Modal>
  );
};
