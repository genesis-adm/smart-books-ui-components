import { DropDown } from 'Components/DropDown';
import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownFooter } from 'Components/SelectDropdown/components/SelectDropdownFooter';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { Checkbox } from 'Components/base/Checkbox';
import { Divider } from 'Components/base/Divider';
import { Icon } from 'Components/base/Icon';
import { Radio } from 'Components/base/Radio';
import { Switch } from 'Components/base/Switch';
import { Tag } from 'Components/base/Tag';
import { Button } from 'Components/base/buttons/Button';
import { ButtonGroup } from 'Components/base/buttons/ButtonGroup';
import { IconButton } from 'Components/base/buttons/IconButton';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { TagType } from 'Components/base/inputs/InputElements/InputTag';
import { InputNew } from 'Components/base/inputs/InputNew';
import { SelectNew } from 'Components/base/inputs/SelectNew';
import { OptionSingleNew } from 'Components/base/inputs/SelectNew/options/OptionSingleNew';
import { TextareaNew } from 'Components/base/inputs/TextareaNew';
import { Text } from 'Components/base/typography/Text';
import { AccordionAccountItem } from 'Components/custom/Accounting/ChartOfAccounts/AccordionAccountItem';
import { AccordionAccountSubItem } from 'Components/custom/Accounting/ChartOfAccounts/AccordionAccountSubItem';
import { AccountClassType } from 'Components/custom/Accounting/ChartOfAccounts/AccountClassType';
import { SensitiveAccountToggle } from 'Components/custom/Accounting/ChartOfAccounts/SensitiveAccountToggle';
import { UploadContainer } from 'Components/custom/Upload/UploadContainer';
import { UploadInfo } from 'Components/custom/Upload/UploadInfo';
import { UploadItemField } from 'Components/custom/Upload/UploadItemField';
import { SelectImport } from 'Components/elements/SelectImport';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { TableCellWidth } from 'Components/types/gridTypes';
import { options } from 'Mocks/fakeOptions';
import { ReactComponent as ArrowsSwitchIcon } from 'Svg/v2/12/arrows.svg';
import { ReactComponent as ArrowLeftIcon } from 'Svg/v2/16/arrow-left.svg';
import { ReactComponent as ArrowRightIcon } from 'Svg/v2/16/arrow-right.svg';
import { ReactComponent as LiabliltiesIcon } from 'Svg/v2/16/bank-cheque.svg';
import { ReactComponent as ActiveIcon } from 'Svg/v2/16/clipboard-tick.svg';
import { ReactComponent as CrossIcon } from 'Svg/v2/16/cross-in-circle.svg';
import { ReactComponent as CloseIcon } from 'Svg/v2/16/cross.svg';
import { ReactComponent as DocumentIcon } from 'Svg/v2/16/file-blank.svg';
import { ReactComponent as IncomeIcon } from 'Svg/v2/16/money-in.svg';
import { ReactComponent as ExpensesIcon } from 'Svg/v2/16/money-out.svg';
import { ReactComponent as AssetIcon } from 'Svg/v2/16/money-stack.svg';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as EquityIcon } from 'Svg/v2/16/scales.svg';
import { ReactComponent as TickIcon } from 'Svg/v2/16/tick-in-circle.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import { ReactComponent as UploadIcon } from 'Svg/v2/16/upload.svg';
import { ReactComponent as EmptyChartOfAccountIcon } from 'Svg/v2/124/empty.svg';
import { checkFileType } from 'Utils/fileHandlers';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Accounting/Template Accounts/Template Accounts Modals',
};
export const Step1ImportFileCSV: React.FC = () => {
  const [file, setFile] = useState<File | null>(null);
  const handleFileUpload = (item: File) => {
    setFile(item);
  };
  const handleFileDelete = () => {
    setFile(null);
  };
  const handleFileDrop = (e: React.DragEvent<HTMLElement>) => {
    const dropFile = e.dataTransfer.files[0];
    if (checkFileType(dropFile, ['csv'])) {
      setFile(e.dataTransfer.files[0]);
    }
  };
  return (
    <Modal
      size="md-extrawide"
      header={<ModalHeaderNew onClose={() => {}} />}
      pluginScrollDisabled
      footer={
        <ModalFooterNew justifycontent="space-between" border>
          <Container alignitems="center" gap="16" flexgrow="1" justifycontent="flex-end">
            <Container flexdirection="column">
              <Text type="body-regular-14" color="grey-100">
                Step: 1 of 3
              </Text>
              <Text type="body-regular-14" color="grey-100">
                Next: Match columns
              </Text>
            </Container>
            <Button
              height="48"
              navigation
              iconRight={<ArrowRightIcon />}
              width="md"
              onClick={() => {}}
            >
              Next
            </Button>
          </Container>
          <Button
            height="48"
            width="sm"
            background="white-100"
            color="black-100"
            border="grey-20-grey-90"
            onClick={() => {}}
          >
            Cancel
          </Button>
        </ModalFooterNew>
      }
    >
      <Container flexdirection="column" alignitems="center">
        <Text className={spacing.mt32} type="h2-semibold" align="center">
          Import template accounts
        </Text>
        <UploadContainer className={spacing.mt40} onDrop={handleFileDrop}>
          <UploadItemField
            name="uploader"
            files={file}
            acceptFilesType="csv"
            onChange={handleFileUpload}
            onDelete={handleFileDelete}
            onReload={() => {}}
          />
          <UploadInfo
            mainText={file ? file.name : 'Drag and drop or Choose file'}
            secondaryText="CSV; Maximum file size is 5MB"
          />
        </UploadContainer>
        <Container
          className={classNames(spacing.mt40, spacing.w440)}
          flexdirection="column"
          gap="4"
        >
          <Text type="body-regular-14">Instruction for upload file:</Text>
          <Text type="body-regular-14">• All your information must be in one file</Text>
          <Text type="body-regular-14">
            • The top row of your file must contain a header title for each column of information
          </Text>
        </Container>
      </Container>
    </Modal>
  );
};

export const Step2MatchColumns: React.FC = () => (
  <Modal
    overlay="grey-10"
    size="full"
    background="grey-10"
    pluginScrollDisabled
    header={
      <ModalHeaderNew
        type="fullwidth"
        background="grey-10"
        title="Import Template account"
        icon={<Tag color="grey" text="File name: CSV" padding="4-8" cursor="default" />}
        border
        onClose={() => {}}
      />
    }
    footer={
      <ModalFooterNew justifycontent="space-between" border>
        <Container alignitems="center" gap="16" flexgrow="1" justifycontent="flex-end">
          <Container flexdirection="column">
            <Text type="body-regular-14" color="grey-100">
              Step: 2 of 3
            </Text>
            <Text type="body-regular-14" color="grey-100">
              Next: Preview mode
            </Text>
          </Container>
          <Button
            height="48"
            navigation
            iconRight={<ArrowRightIcon />}
            width="md"
            onClick={() => {}}
          >
            Next
          </Button>
        </Container>
        <Button
          height="48"
          width="sm"
          navigation
          iconLeft={<ArrowLeftIcon />}
          background="white-100"
          color="black-100"
          border="grey-20-grey-90"
          onClick={() => {}}
        >
          Back
        </Button>
      </ModalFooterNew>
    }
  >
    <Container
      background="white-100"
      flexdirection="column"
      radius
      flexgrow="1"
      className={classNames(spacing.p24, spacing.mX20, spacing.mY16)}
      customScroll
    >
      <Text type="body-regular-16">General</Text>
      <Container className={spacing.mt24} gap="16" flexwrap="wrap">
        <SelectImport
          name="Name"
          label="Choose column from your file"
          placeholder="Choose option"
          background="grey-10"
          textWidth="160"
          required
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() =>
                    onClick({
                      label,
                      value,
                    })
                  }
                />
              ))}
            </>
          )}
        </SelectImport>
        <SelectImport
          name="Account Detail type"
          label="Choose column from your file"
          placeholder="Choose option"
          background="grey-10"
          textWidth="160"
          required
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() =>
                    onClick({
                      label,
                      value,
                    })
                  }
                />
              ))}
            </>
          )}
        </SelectImport>
        <SelectImport
          name="Currency"
          label="Choose column from your file"
          placeholder="Choose option"
          background="grey-10"
          textWidth="160"
          required
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() =>
                    onClick({
                      label,
                      value,
                    })
                  }
                />
              ))}
            </>
          )}
        </SelectImport>
        <SelectImport
          name="Notes"
          label="Choose column from your file"
          placeholder="Choose option"
          background="grey-10"
          textWidth="160"
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() =>
                    onClick({
                      label,
                      value,
                    })
                  }
                />
              ))}
            </>
          )}
        </SelectImport>
        <SelectImport
          name="Account type"
          label="Choose column from your file"
          placeholder="Choose option"
          background="grey-10"
          textWidth="160"
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() =>
                    onClick({
                      label,
                      value,
                    })
                  }
                />
              ))}
            </>
          )}
        </SelectImport>
      </Container>
      <Divider className={spacing.mY24} fullHorizontalWidth />
      {/* <Container
        background="grey-10"
        border="grey-20"
        borderRadius="10"
        justifycontent="space-between"
        alignitems="center"
        className={classNames(spacing.w100p, spacing.w506, spacing.p24)}
      >
        <Container flexdirection="column" gap="8" className={spacing.w240}>
          <Text type="body-medium">Business Structure</Text>
          <Text type="body-regular-14" color="grey-100">
            Please add new Business Structure to invite new team members
          </Text>
          <Button
            width="108"
            height="24"
            background="blue-10-blue-20"
            color="violet-90-violet-100"
            iconLeft={<PlusIcon />}
            onClick={() => {}}
          >
            Add new
          </Button>
        </Container>
        <Icon icon={<EmptyChartOfAccountIcon />} />
      </Container> */}
      <Container
        background="grey-10"
        border="grey-20"
        borderRadius="10"
        flexdirection="column"
        className={classNames(spacing.pX24, spacing.w506, spacing.pY16, spacing.h200fixed)}
      >
        <Container justifycontent="space-between" alignitems="center">
          <Text type="body-regular-14" color="grey-100">
            Business Structure
          </Text>
          <IconButton
            icon={<EditIcon />}
            size="24"
            background="transparent"
            color="grey-100-yellow-90"
            onClick={() => {}}
          />
        </Container>
        <TableNew
          noResults={false}
          tableHead={
            <TableHeadNew background="grey-10" margin="none">
              <TableTitleNew background="grey-10" minWidth="32" padding="none" title="#" />
              <TableTitleNew background="grey-10" minWidth="180" title="Business Division" />
              <TableTitleNew
                background="grey-10"
                minWidth="180"
                title="Legal entity"
                flexgrow="1"
              />
            </TableHeadNew>
          }
        >
          <TableBodyNew>
            {Array.from(Array(1)).map((_, index) => (
              <TableRowNew key={index} background="transparent" size="28" cursor="auto" gap="8">
                <TableCellTextNew
                  minWidth="32"
                  padding="8"
                  align="center"
                  color="grey-100"
                  value={`${index + 1}`}
                />
                <TableCellNew
                  minWidth="180"
                  fixedWidth
                  alignitems="center"
                  justifycontent="space-between"
                  padding="12-0"
                  gap="8"
                >
                  <Tag
                    color="grey-black"
                    font="body-regular-14"
                    padding="0-4"
                    text={`Some name of business # ${index + 1}`}
                    noWrap
                    flexShrink="1"
                    align="left"
                    // cursor="default"
                  />
                  <Icon icon={<ArrowsSwitchIcon />} />
                </TableCellNew>
                <TableCellNew
                  minWidth="180"
                  alignitems="center"
                  justifycontent="space-between"
                  padding="12-0"
                  gap="8"
                  flexgrow="1"
                >
                  <Tag
                    color="grey-black"
                    font="body-regular-14"
                    padding="0-4"
                    text={`Legal Entity # 00${index + 1}`}
                    align="left"
                    additionalCounter={2}
                    tooltip={
                      <Container flexdirection="column">
                        <Text color="white-100">Legal Entity #001-1</Text>
                        <Text color="white-100">Legal Entity #001-2</Text>
                        <Text color="white-100">Legal Entity #001-3</Text>
                      </Container>
                    }
                  />
                </TableCellNew>
              </TableRowNew>
            ))}
          </TableBodyNew>
        </TableNew>
      </Container>
    </Container>
  </Modal>
);

export const Step3PreviewMode: React.FC = () => {
  const sorting = {
    sortType: null,
    tooltipText: 'Sorting tooltip text',
  };
  const tableHead = [
    {
      minWidth: '48',
      title: <Checkbox name="tickAll" checked onChange={() => {}} />,
      padding: 'none',
    },
    {
      minWidth: '240',
      title: 'Name',
      sorting,
    },
    {
      minWidth: '150',
      title: 'Currency',
      sorting,
    },
    {
      minWidth: '240',
      title: 'Account type',
      sorting,
    },
    {
      minWidth: '240',
      title: 'Account detail type',
      sorting,
    },
    {
      minWidth: '240',
      title: 'Notes',
      sorting,
      flexgrow: '1',
    },
    {
      minWidth: '110',
      title: 'Status',
      sorting,
      align: 'center',
    },
    {
      minWidth: '130',
      title: 'Action',
      align: 'center',
    },
  ];

  return (
    <Modal
      overlay="grey-10"
      size="full"
      background="grey-10"
      pluginScrollDisabled
      header={
        <ModalHeaderNew
          type="fullwidth"
          background="grey-10"
          title="Import Template account"
          icon={<Tag color="grey" text="File name: CSV" padding="4-8" cursor="default" />}
          radius
          onClose={() => {}}
        />
      }
      footer={
        <ModalFooterNew justifycontent="space-between" background="grey-10" border>
          <Container alignitems="center" gap="16" flexgrow="1" justifycontent="flex-end">
            <Container flexdirection="column">
              <Text type="body-regular-14" color="grey-100">
                Step: 3 of 3
              </Text>
              <Text type="body-regular-14" color="grey-100">
                Preview mode
              </Text>
            </Container>
            <Button height="48" width="md" onClick={() => {}}>
              Import 3 rows
            </Button>
          </Container>
          <Button
            height="48"
            width="sm"
            navigation
            iconLeft={<ArrowLeftIcon />}
            background="white-100"
            color="black-100"
            border="grey-20-grey-90"
            onClick={() => {}}
          >
            Back
          </Button>
        </ModalFooterNew>
      }
    >
      <Container flexdirection="column" radius flexgrow="1" overflow="hidden">
        <Container flexdirection="column" className={spacing.mX32}>
          <Text type="body-medium" className={spacing.mt32}>
            Template accounts for importing:
          </Text>
          <Container className={spacing.mt12} alignitems="center" justifycontent="space-between">
            <Container alignitems="center" gap="8">
              <Text color="grey-100">Business Structure:</Text>
              <Container
                alignitems="center"
                background="grey-20"
                radius
                gap="4"
                className={spacing.p4}
              >
                <Text type="text-regular" color="grey-100">
                  Business Division
                </Text>
                <Icon icon={<ArrowsSwitchIcon />} path="grey-100" />
                <Text type="text-regular" color="grey-100">
                  Legal entity 1 | Legal entity 2
                </Text>
              </Container>
              <DropDown
                flexdirection="column"
                padding="16"
                control={({ isOpen, handleOpen }) => (
                  <Tag text="+2" color={isOpen ? 'violet' : 'grey'} onClick={handleOpen} />
                )}
              >
                <Container flexdirection="column" gap="8" className={spacing.w300min}>
                  <Text type="button" color="grey-100">
                    Business structure: 3
                  </Text>
                  <Container flexdirection="column">
                    <Text type="body-regular-14">Business Division 2</Text>
                    <Text type="body-regular-14" color="grey-100">
                      Legal entity 1, Legal entity 2
                    </Text>
                  </Container>
                  <Container flexdirection="column" className={spacing.w300min}>
                    <Text type="body-regular-14">Business Division 2</Text>
                    <Text type="body-regular-14" color="grey-100">
                      Legal entity 1, Legal entity 2
                    </Text>
                  </Container>
                </Container>
              </DropDown>
            </Container>
            <Container alignitems="center">
              <Text type="text-medium" color="grey-100">
                Total rows:&nbsp;
              </Text>
              <Text type="caption-semibold">3</Text>
              <Divider type="vertical" height="20" background="grey-90" className={spacing.mX16} />
              <Text type="text-medium" color="grey-100">
                Number of valid rows:&nbsp;
              </Text>
              <Text type="caption-semibold" color="green-90">
                2
              </Text>
              <Divider type="vertical" height="20" background="grey-90" className={spacing.mX16} />
              <Text type="text-medium" color="grey-100">
                Number of non valid rows:&nbsp;
              </Text>
              <Text type="caption-semibold" color="red-90">
                0
              </Text>
            </Container>
          </Container>
        </Container>
        <Container flexdirection="column" flexgrow="1" className={spacing.mX32} overflow="hidden">
          <TableNew
            noResults={false}
            className={spacing.mt24}
            tableHead={
              <TableHeadNew>
                {tableHead.map((item, index) => (
                  <TableTitleNew
                    key={index}
                    minWidth={item.minWidth as TableCellWidth}
                    // flexgrow={item.flexgrow as TableTitleNewProps['flexgrow']}
                    padding={item.padding as TableTitleNewProps['padding']}
                    align={item.align as TableTitleNewProps['align']}
                    title={item.title}
                    sorting={item.sorting}
                    onClick={() => {}}
                  />
                ))}
              </TableHeadNew>
            }
          >
            <TableBodyNew>
              {Array.from(Array(39)).map((_, index) => (
                <TableRowNew size="52" background="white" key={index}>
                  <TableCellNew minWidth="48" justifycontent="center" alignitems="center">
                    <Checkbox name={`line-${index}`} checked onChange={() => {}} />
                  </TableCellNew>
                  <TableCellNew minWidth="240" alignitems="center" padding="8">
                    <InputNew
                      name="name"
                      width="full"
                      height="36"
                      label="Name"
                      placeholder=""
                      value="Name"
                      background="transparent"
                      border="transparent"
                      onChange={() => {}}
                      error
                      errorMessage="Name already exist. Please use other name"
                    />
                  </TableCellNew>
                  <TableCellNew minWidth="150" padding="8-0">
                    <SelectNew
                      name="currency"
                      onChange={() => {}}
                      width="full"
                      background="transparent"
                      border="transparent"
                      height="36"
                      label=""
                      // tags={businessDivisionTags}
                      error
                      errorMessage="Currency not recognized. Please choose from the list"
                    >
                      {({ onClick, state, selectElement }) => (
                        <SelectDropdown selectElement={selectElement}>
                          <SelectDropdownBody>
                            <SelectDropdownItemList>
                              {options.map(({ label, value }) => (
                                <OptionSingleNew
                                  key={value}
                                  label={label}
                                  height="32"
                                  // type="radio"
                                  selected={state?.value === value}
                                  onClick={() =>
                                    onClick({
                                      label: '',
                                      value,
                                    })
                                  }
                                />
                              ))}
                            </SelectDropdownItemList>
                          </SelectDropdownBody>
                        </SelectDropdown>
                      )}
                    </SelectNew>
                  </TableCellNew>
                  <TableCellNew minWidth="240" padding="8-0">
                    <SelectNew
                      name="accountType"
                      onChange={() => {}}
                      width="full"
                      background="transparent"
                      border="transparent"
                      height="36"
                      label=""
                      error
                      errorMessage="Account type not recognized. Please choose from the list"
                      // tags={businessDivisionTags}
                    >
                      {({ onClick, state, selectElement }) => (
                        <SelectDropdown selectElement={selectElement}>
                          <SelectDropdownBody>
                            <SelectDropdownItemList>
                              {options.map(({ label, value }) => (
                                <OptionSingleNew
                                  key={value}
                                  label={label}
                                  height="32"
                                  // type="radio"
                                  selected={state?.value === value}
                                  onClick={() =>
                                    onClick({
                                      label: '',
                                      value,
                                    })
                                  }
                                />
                              ))}
                            </SelectDropdownItemList>
                          </SelectDropdownBody>
                        </SelectDropdown>
                      )}
                    </SelectNew>
                  </TableCellNew>
                  <TableCellNew minWidth="240" padding="8-0">
                    <SelectNew
                      name="accountDetailType"
                      onChange={() => {}}
                      background="transparent"
                      border="transparent"
                      width="full"
                      height="36"
                      label=""
                      error
                      errorMessage="Account detail type not recognized. Please choose from the list"
                      // tags={businessDivisionTags}
                    >
                      {({ onClick, state, selectElement }) => (
                        <SelectDropdown selectElement={selectElement}>
                          <SelectDropdownBody>
                            <SelectDropdownItemList>
                              {options.map(({ label, value }) => (
                                <OptionSingleNew
                                  key={value}
                                  label={label}
                                  height="32"
                                  // type="radio"
                                  selected={state?.value === value}
                                  onClick={() =>
                                    onClick({
                                      label: '',
                                      value,
                                    })
                                  }
                                />
                              ))}
                            </SelectDropdownItemList>
                          </SelectDropdownBody>
                        </SelectDropdown>
                      )}
                    </SelectNew>
                  </TableCellNew>
                  <TableCellTextNew minWidth="240" value="Notes text" />
                  <TableCellNew minWidth="110" justifycontent="center" alignitems="center">
                    <Tag icon={<TickIcon />} color="green" />
                    {/* <Tag icon={<CrossIcon />} color="red" /> */}
                  </TableCellNew>
                  <TableCellNew minWidth="120" alignitems="center" justifycontent="center">
                    <Button
                      width="80"
                      height="24"
                      font="text-medium"
                      onClick={() => {}}
                      iconLeft={<UploadIcon />}
                      background="blue-10-blue-20"
                      border="blue-10-blue-20"
                      color="violet-90-violet-100"
                    >
                      Import
                    </Button>
                  </TableCellNew>
                </TableRowNew>
              ))}
            </TableBodyNew>
          </TableNew>
        </Container>
      </Container>
    </Modal>
  );
};
