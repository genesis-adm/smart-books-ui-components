import { Meta, Story } from '@storybook/react';
import { DropDown } from 'Components/DropDown';
import { Checkbox } from 'Components/base/Checkbox';
import { Divider } from 'Components/base/Divider';
import { LoadingLine } from 'Components/base/LoadingLine';
import { TagSegmented } from 'Components/base/TagSegmented';
import { TooltipIcon } from 'Components/base/TooltipIcon';
import { Button } from 'Components/base/buttons/Button';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { ContainerMain } from 'Components/base/grid/ContainerMain';
import { MainPageContentContainer } from 'Components/base/grid/MainPageContentContainer';
import { MenuTab, MenuTabWrapper } from 'Components/base/tabs/MenuTab';
import { Text } from 'Components/base/typography/Text';
import { MainPageEmpty } from 'Components/custom/MainPage/MainPageEmpty';
import { HeaderSB } from 'Components/custom/Stories/HeaderSB';
import { NavigationSB } from 'Components/custom/Stories/NavigationSB';
import { Pagination } from 'Components/custom/Stories/Pagination/Pagination';
import { ExportDropdown } from 'Components/elements/ExportDropdown';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { FilterDropDown } from 'Components/elements/filters/FilterDropDown';
import { FilterToggleButton } from 'Components/elements/filters/FilterToggleButton';
import { FiltersWrapper } from 'Components/elements/filters/FiltersWrapper';
import { Search } from 'Components/inputs/Search';
import { bankAccounts } from 'Mocks/purchasesFakeData';
import { ReactComponent as CopyIcon } from 'Svg/v2/16/copy.svg';
import { ReactComponent as RemoveIcon } from 'Svg/v2/16/cross-in-filled-circle.svg';
import { ReactComponent as EyeIcon } from 'Svg/v2/16/eye.svg';
import { ReactComponent as InvoiceIcon } from 'Svg/v2/16/invoice.svg';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as DropdownIcon } from 'Svg/v2/16/three-dots.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import { ReactComponent as UploadIcon } from 'Svg/v2/16/upload.svg';
import { ReactComponent as CompanyIcon } from 'Svg/v2/20/buildings.svg';
import { ReactComponent as IndividualIcon } from 'Svg/v2/20/user-id.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';
import type { TableCellWidth } from '../../../components/types/gridTypes';

const SalesCustomersMainPageComponent: Story = ({ storyState }) => {
  const [toggle, toggleMenu] = useState<boolean>(false);
  const [showFilters, setShowFilters] = useState<boolean>(false);
  const sorting = {
    sortType: null,
    tooltipText: 'Sorting tooltip text',
  };
  const tableHead = [
    {
      minWidth: '48',
      title: <Checkbox name="tickAll" checked onChange={() => {}} />,
      padding: 'none',
    },
    {
      minWidth: '170',
      title: 'Customer name',
      sorting,
    },
    {
      minWidth: '220',
      title: 'Contact information',
      sorting,
    },
    {
      minWidth: '170',
      title: 'Billing address',
      sorting,
    },
    {
      minWidth: '150',
      title: 'Notes',
    },
    {
      minWidth: '140',
      title: 'Invoice terms',
      sorting,
    },
    {
      minWidth: '340',
      title: 'Payment method',
      sorting,
    },
    // { minWidth: '210', title: 'Preferred payment method', sorting },
    // { minWidth: '100', title: 'Description', align: 'center' },
    {
      minWidth: '100',
      title: 'Action',
      align: 'center',
    },
  ];
  const filters = [{ title: 'Customer name' }, { title: 'Payment method' }];
  return (
    <Container fullscreen flexwrap="nowrap" background={'grey-10'}>
      <NavigationSB opened={toggle} onToggleOpen={() => toggleMenu(!toggle)} />
      <ContainerMain navOpened={toggle}>
        <HeaderSB />
        <Container flexdirection="column" className={spacing.mX32}>
          <Text type="h1-semibold">Sales</Text>
          <MenuTabWrapper className={spacing.mt24}>
            <MenuTab id="allSales" active={false} onClick={() => {}}>
              All sales
            </MenuTab>
            <MenuTab id="customers" active onClick={() => {}}>
              Customers
            </MenuTab>
            <MenuTab id="products" active={false} onClick={() => {}}>
              Products
            </MenuTab>
          </MenuTabWrapper>
        </Container>
        <MainPageContentContainer>
          {storyState === 'loaded' && (
            <>
              <Container
                className={classNames(spacing.mX24, spacing.mt24)}
                justifycontent="space-between"
                gap="24"
              >
                <Container gap="24" alignitems="center">
                  <FilterToggleButton
                    showFilters={showFilters}
                    filtersSelected={false}
                    onClear={() => {}}
                    onClick={() => {
                      setShowFilters((prevState) => !prevState);
                    }}
                  />
                  <Search
                    width="320"
                    height="40"
                    name="search-sales-customers-mainPage"
                    placeholder="Search by Customer & Notes"
                    value=""
                    onChange={() => {}}
                    searchParams={[]}
                    onSearch={() => {}}
                    onClear={() => {}}
                  />
                </Container>
                <Container gap="24" alignitems="center">
                  <Divider type="vertical" background={'grey-90'} height="20" />
                  <ExportDropdown onExportToXLS={() => {}} onExportToCSV={() => {}} />
                  <IconButton
                    size="40"
                    background={'grey-10-grey-20'}
                    color="grey-100"
                    icon={<UploadIcon />}
                    tooltip="Import files"
                    onClick={() => {}}
                  />
                  <Button
                    background={'violet-90-violet-100'}
                    color="white-100"
                    width="140"
                    height="40"
                    iconLeft={<PlusIcon />}
                  >
                    Create
                  </Button>
                </Container>
              </Container>
              <FiltersWrapper active={showFilters}>
                {filters.map((item) => (
                  <FilterDropDown
                    key={item.title}
                    title={item.title}
                    value="All"
                    onClear={() => {}}
                    selectedAmount={0}
                  />
                ))}
              </FiltersWrapper>
              <TableNew
                noResults={false}
                className={classNames(spacing.mt24, spacing.ml24, spacing.pr24)}
                tableHead={
                  <TableHeadNew>
                    {tableHead.map((item, index) => (
                      <TableTitleNew
                        key={index}
                        minWidth={item.minWidth as TableCellWidth}
                        // flexgrow={item.flexgrow as TableTitleNewProps['flexgrow']}
                        padding={item.padding as TableTitleNewProps['padding']}
                        align={item.align as TableTitleNewProps['align']}
                        title={item.title}
                        sorting={item.sorting}
                        onClick={() => {}}
                      />
                    ))}
                  </TableHeadNew>
                }
              >
                <TableBodyNew>
                  {Array.from(Array(39)).map((_, index) => (
                    <TableRowNew key={index} gap="8">
                      <TableCellNew minWidth="48" justifycontent="center" alignitems="center">
                        <Checkbox name={`line-${index}`} checked onChange={() => {}} />
                      </TableCellNew>

                      <TableCellTextNew
                        minWidth="170"
                        type="caption-semibold"
                        iconLeft={
                          <TooltipIcon
                            icon={index % 2 ? <IndividualIcon /> : <CompanyIcon />}
                            size="24"
                            color="grey-100-black-100"
                            background={'transparent'}
                            message={index % 2 ? 'Individual' : 'Company'}
                          />
                        }
                        value={
                          index % 2
                            ? 'Customer name,dnfjsdfbngkjdfhgkjhfdkshgkjhsd'
                            : 'Customer name'
                        }
                        secondaryValue="xxx0001"
                      />
                      <TableCellTextNew
                        minWidth="220"
                        value="customer.email@gmail.com"
                        secondaryValue="+38 097 000 00 00"
                        type="text-regular"
                        secondaryType="text-regular"
                      />
                      <TableCellTextNew
                        minWidth="170"
                        lineClamp="2"
                        type="text-regular"
                        color="grey-100"
                        value="Ukraine, Kyiv city, Bohdana Khmelnytskoho Street, 40/25"
                      />
                      <TableCellTextNew
                        minWidth="150"
                        lineClamp="2"
                        type="text-regular"
                        color="grey-100"
                        value="Regular vendor for any time"
                      />
                      <TableCellTextNew
                        minWidth="140"
                        type="text-regular"
                        color="grey-100"
                        value="Invoice terms"
                      />
                      <TableCellNew minWidth="340">
                        <TagSegmented
                          className={spacing.w300max}
                          {...bankAccounts}
                          iconStaticColor
                          tooltipMessage={
                            <Container flexdirection="column">
                              <Text color="white-100" style={{ wordBreak: 'break-all' }}>
                                {bankAccounts.mainLabel}
                              </Text>
                              <Text color="white-100" style={{ wordBreak: 'break-all' }}>
                                {bankAccounts.secondLabel} | {bankAccounts.thirdLabel}
                              </Text>
                            </Container>
                          }
                        />
                      </TableCellNew>
                      <TableCellNew minWidth="100" alignitems="center" justifycontent="center">
                        <DropDown
                          flexdirection="column"
                          padding="8"
                          gap="4"
                          control={({ handleOpen }) => (
                            <IconButton
                              icon={<DropdownIcon />}
                              onClick={handleOpen}
                              background={'transparent'}
                              color="grey-100-violet-90"
                            />
                          )}
                        >
                          <DropDownButton size="160" icon={<EyeIcon />} onClick={() => {}}>
                            Open
                          </DropDownButton>
                          <DropDownButton size="160" icon={<InvoiceIcon />} onClick={() => {}}>
                            Create invoice
                          </DropDownButton>
                          <DropDownButton size="160" icon={<CopyIcon />} onClick={() => {}}>
                            Duplicate
                          </DropDownButton>
                          <DropDownButton size="160" icon={<EditIcon />} onClick={() => {}}>
                            Edit
                          </DropDownButton>
                          <DropDownButton size="160" icon={<RemoveIcon />} onClick={() => {}}>
                            Make inactive
                          </DropDownButton>
                          <Divider type="horizontal" fullHorizontalWidth className={spacing.mY4} />
                          <DropDownButton
                            size="204"
                            type="danger"
                            icon={<TrashIcon />}
                            onClick={() => {}}
                          >
                            Delete
                          </DropDownButton>
                        </DropDown>
                      </TableCellNew>
                    </TableRowNew>
                  ))}
                </TableBodyNew>
              </TableNew>
              <Pagination padding="16" />
            </>
          )}
          {storyState === 'empty' && (
            <MainPageEmpty
              title="Create new Customers"
              description="You have no customers created. Your created customers will be displayed here."
              action={
                <>
                  <Button
                    width="220"
                    height="40"
                    background={'blue-10-blue-20'}
                    color="violet-90-violet-100"
                    iconLeft={<PlusIcon />}
                    onClick={() => {}}
                  >
                    Create Customers
                  </Button>
                  <Button
                    width="220"
                    height="40"
                    background={'blue-10-blue-20'}
                    color="violet-90-violet-100"
                    iconLeft={<UploadIcon />}
                    onClick={() => {}}
                  >
                    Import Customers
                  </Button>
                </>
              }
            />
          )}
        </MainPageContentContainer>
        <LoadingLine loadingCompleted={storyState !== 'loading'} />
      </ContainerMain>
    </Container>
  );
};

export default {
  title: 'Pages/Sales/Customers/Sales Customers Main Page',
  component: SalesCustomersMainPageComponent,
  argTypes: {
    storyState: {
      options: ['empty', 'loading', 'loaded'],
      control: {
        type: 'radio',
        labels: {
          empty: 'Empty page',
          loading: 'Page is loading',
          loaded: 'Page loaded',
        },
      },
    },
  },
} as Meta;

export const SalesCustomersMainPage = SalesCustomersMainPageComponent.bind({});
SalesCustomersMainPage.args = {
  storyState: 'loaded',
};
