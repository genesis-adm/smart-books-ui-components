import { Checkbox } from 'Components/base/Checkbox';
import { Divider } from 'Components/base/Divider';
import { Icon } from 'Components/base/Icon';
import { Radio } from 'Components/base/Radio';
import { Tag } from 'Components/base/Tag';
import { TooltipIcon } from 'Components/base/TooltipIcon';
import { Button } from 'Components/base/buttons/Button';
import { CheckboxButton } from 'Components/base/buttons/CheckboxButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { InputDate } from 'Components/base/inputs/InputDate';
import { InputNew } from 'Components/base/inputs/InputNew';
import { InputPhone } from 'Components/base/inputs/InputPhone';
import { SelectNew } from 'Components/base/inputs/SelectNew';
import { OptionDoubleVerticalNew } from 'Components/base/inputs/SelectNew/options/OptionDoubleVerticalNew';
import { OptionSingleNew } from 'Components/base/inputs/SelectNew/options/OptionSingleNew';
import { TextareaNew } from 'Components/base/inputs/TextareaNew';
import { GeneralTab, GeneralTabWrapper } from 'Components/base/tabs/GeneralTab';
import { MenuTab, MenuTabWrapper } from 'Components/base/tabs/MenuTab';
import { Text } from 'Components/base/typography/Text';
import { AddNewWrapper } from 'Components/elements/AddNewWrapper';
import { BillingCardItem } from 'Components/elements/BillingCardItem';
import { AssignContainer } from 'Components/modules/Assign/AssignContainer';
import { AssignLine } from 'Components/modules/Assign/AssignLine';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { options } from 'Mocks/fakeOptions';
import { ReactComponent as BankIcon } from 'Svg/v2/16/bank.svg';
import { ReactComponent as CreditCardIcon } from 'Svg/v2/16/credit-card.svg';
import { ReactComponent as InfoIcon } from 'Svg/v2/16/info-in-circle.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as QuestionAltIcon } from 'Svg/v2/16/question-transparent.svg';
import { ReactComponent as StarFilledIcon } from 'Svg/v2/16/star-filled.svg';
import { ReactComponent as StarIcon } from 'Svg/v2/16/star.svg';
import { ReactComponent as StatusSuccessIcon } from 'Svg/v2/16/tick-in-circle.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import { ReactComponent as WalletIcon } from 'Svg/v2/16/wallet.svg';
import { ReactComponent as MastercardIcon } from 'Svg/v2/24/logo-mastercard.svg';
import { ReactComponent as AfterpayIcon } from 'Svg/v2/32/logo-afterpay.svg';
import { ReactComponent as BICSwiftCodeTooltip } from 'Svg/v2/tooltips/bic-swift-code.svg';
import { ReactComponent as IBANNumberTooltip } from 'Svg/v2/tooltips/iban-number.svg';
import { ReactComponent as RoutingNumberTooltip } from 'Svg/v2/tooltips/routing-number.svg';
import handleAccountLength from 'Utils/handleAccountLength';
import { formatBIC, formatCardNumber, formatIban, formatRoutingNumber } from 'Utils/paymentFormat ';
import classNames from 'classnames';
import React, { ReactNode, useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Sales/Customers/Modals/Create Customer',
};

export const CreateNewCustomerGeneralTab: React.FC = () => {
  const [nameValue, setNameValue] = useState('');
  const [einValue, setEINValue] = useState('');
  const [notesValue, setNotesValue] = useState('');
  return (
    <Modal
      size="720"
      background="grey-10"
      type="new"
      header={
        <ModalHeaderNew
          background="grey-10"
          title="Create new customer"
          border
          onClose={() => {}}
        />
      }
      footer={
        <ModalFooterNew background="grey-10">
          <Button width="md" onClick={() => {}}>
            Save
          </Button>
        </ModalFooterNew>
      }
    >
      <MenuTabWrapper>
        <MenuTab id="general" active onClick={() => {}}>
          General
        </MenuTab>
        <MenuTab id="contacts" onClick={() => {}}>
          Contacts
        </MenuTab>
        <MenuTab id="billing" onClick={() => {}}>
          Billing
        </MenuTab>
        <MenuTab id="accounting" onClick={() => {}}>
          Accounting
        </MenuTab>
      </MenuTabWrapper>
      <Container
        flexdirection="column"
        borderRadius="20"
        background="white-100"
        className={classNames(spacing.mt24, spacing.p24)}
      >
        <Text type="body-regular-14">General</Text>
        <Text type="caption-regular" color="grey-100" className={spacing.mt16}>
          Choose customer type
        </Text>
        <Container className={spacing.mt12} gap="20">
          <Radio id="individual" onChange={() => {}} name="customerType" checked>
            Individual
          </Radio>
          <Radio id="company" onChange={() => {}} name="customerType" checked={false}>
            Company
          </Radio>
        </Container>
        <Container
          justifycontent="space-between"
          className={classNames(spacing.w100p, spacing.mt16)}
        >
          <InputNew
            name="firstName"
            label="First name"
            width="192"
            required
            onChange={(e) => {
              setNameValue(e.target.value);
            }}
            value={nameValue}
          />
          <InputNew
            name="lastName"
            label="Last name"
            width="192"
            required
            onChange={(e) => {
              setNameValue(e.target.value);
            }}
            value={nameValue}
          />
          <InputNew
            name="EIN"
            label="EIN"
            width="192"
            value={einValue}
            onChange={(e) => {
              setEINValue(e.target.value);
            }}
          />
        </Container>
        {/* customer type === 'company' */}
        {/* <Container
          justifycontent="space-between"
          className={classNames(spacing.w100p, spacing.mt16)}
        >
          <InputNew
            name="customerName"
            label="Customer name"
            width="300"
            required
            onChange={(e) => {
              setNameValue(e.target.value);
            }}
            value={nameValue}
          />
          <InputNew
            name="EIN"
            label="EIN"
            width="300"
            value={einValue}
            onChange={(e) => {
              setEINValue(e.target.value);
            }}
          />
        </Container> */}
        <TextareaNew
          className={spacing.mt24}
          name="notes"
          label="Notes"
          value={notesValue}
          onChange={(e) => setNotesValue(e.target.value)}
        />
        <Text className={spacing.mt24} type="body-regular-14">
          Other information
        </Text>
        <Container
          justifycontent="space-between"
          className={classNames(spacing.w100p, spacing.mt16)}
        >
          <SelectNew name="invoiceterms" onChange={() => {}} width="300" label="Invoice terms">
            {({ onClick, state }) => (
              <>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={state?.value === value}
                    onClick={() => onClick({ label, value })}
                  />
                ))}
              </>
            )}
          </SelectNew>
          <SelectNew name="currency" onChange={() => {}} width="300" label="Currency">
            {({ onClick, state }) => (
              <>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={state?.value === value}
                    onClick={() => onClick({ label, value })}
                  />
                ))}
              </>
            )}
          </SelectNew>
        </Container>
      </Container>
    </Modal>
  );
};

export const CreateNewCustomerContactsTab: React.FC = () => {
  const [contactNameValue, setContactNameValue] = useState('');
  const [emailValue, setEmailValue] = useState('');
  const [zipCodeValue, setZipCodeValue] = useState('');
  const [streetValue, setStreetValue] = useState('');
  const [sameAsBillingValue, setSameAsBillingValue] = useState(true);
  return (
    <Modal
      size="720"
      background="grey-10"
      type="new"
      header={
        <ModalHeaderNew
          background="grey-10"
          title="Create new customer"
          border
          onClose={() => {}}
        />
      }
      footer={
        <ModalFooterNew background="grey-10">
          <Button width="md" onClick={() => {}}>
            Save
          </Button>
        </ModalFooterNew>
      }
    >
      <MenuTabWrapper>
        <MenuTab id="general" onClick={() => {}}>
          General
        </MenuTab>
        <MenuTab id="contacts" active onClick={() => {}}>
          Contacts
        </MenuTab>
        <MenuTab id="billing" onClick={() => {}}>
          Billing
        </MenuTab>
        <MenuTab id="accounting" onClick={() => {}}>
          Accounting
        </MenuTab>
      </MenuTabWrapper>
      <Container
        flexdirection="column"
        borderRadius="20"
        background="white-100"
        className={classNames(spacing.mt24, spacing.p24)}
      >
        <Text type="body-regular-14">General</Text>
        <InputNew
          className={spacing.mt16}
          name="contactName"
          label="Contact person name"
          width="full"
          required
          onChange={(e) => {
            setContactNameValue(e.target.value);
          }}
          value={contactNameValue}
        />
        <Container
          justifycontent="space-between"
          className={classNames(spacing.w100p, spacing.mt24)}
        >
          <InputPhone
            width="300"
            id="nmbr-id"
            name="test-nmbr"
            value=""
            label="Phone number"
            onChange={() => {}}
            onBlur={() => {}}
          />
          <InputNew
            name="email"
            label="Email address"
            width="300"
            type="email"
            value={emailValue}
            onChange={(e) => {
              setEmailValue(e.target.value);
            }}
          />
        </Container>
        <Text className={spacing.mt24} type="body-regular-14">
          Billing address
        </Text>
        <Container
          justifycontent="space-between"
          className={classNames(spacing.w100p, spacing.mt16)}
        >
          <SelectNew name="country" onChange={() => {}} width="192" label="Country" required>
            {({ onClick, state }) => (
              <>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={state?.value === value}
                    onClick={() => onClick({ label, value })}
                  />
                ))}
              </>
            )}
          </SelectNew>
          <SelectNew name="state" onChange={() => {}} width="192" label="State" required>
            {({ onClick, state }) => (
              <>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={state?.value === value}
                    onClick={() => onClick({ label, value })}
                  />
                ))}
              </>
            )}
          </SelectNew>
          <SelectNew name="city" onChange={() => {}} width="192" label="City">
            {({ onClick, state }) => (
              <>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={state?.value === value}
                    onClick={() => onClick({ label, value })}
                  />
                ))}
              </>
            )}
          </SelectNew>
        </Container>
        <Container
          justifycontent="space-between"
          className={classNames(spacing.w100p, spacing.mt16)}
        >
          <InputNew
            name="zipCode"
            label="ZIP code"
            width="192"
            value={zipCodeValue}
            onChange={(e) => {
              setZipCodeValue(e.target.value);
            }}
          />
          <InputNew
            name="street"
            label="Street"
            width="408"
            value={streetValue}
            onChange={(e) => {
              setStreetValue(e.target.value);
            }}
          />
        </Container>
        <Text className={spacing.mt24} type="body-regular-14">
          Shipping address
        </Text>
        <Checkbox
          className={spacing.mt16}
          name="sameAsBilling"
          checked={sameAsBillingValue}
          onChange={() => {
            setSameAsBillingValue((prev) => !prev);
          }}
        >
          Same as billing address
        </Checkbox>
        {!sameAsBillingValue && (
          <>
            <Container
              justifycontent="space-between"
              className={classNames(spacing.w100p, spacing.mt16)}
            >
              <SelectNew name="country" onChange={() => {}} width="192" label="Country" required>
                {({ onClick, state }) => (
                  <>
                    {options.map(({ label, value }) => (
                      <OptionSingleNew
                        key={value}
                        label={label}
                        selected={state?.value === value}
                        onClick={() => onClick({ label, value })}
                      />
                    ))}
                  </>
                )}
              </SelectNew>
              <SelectNew name="state" onChange={() => {}} width="192" label="State" required>
                {({ onClick, state }) => (
                  <>
                    {options.map(({ label, value }) => (
                      <OptionSingleNew
                        key={value}
                        label={label}
                        selected={state?.value === value}
                        onClick={() => onClick({ label, value })}
                      />
                    ))}
                  </>
                )}
              </SelectNew>
              <SelectNew name="city" onChange={() => {}} width="192" label="City">
                {({ onClick, state }) => (
                  <>
                    {options.map(({ label, value }) => (
                      <OptionSingleNew
                        key={value}
                        label={label}
                        selected={state?.value === value}
                        onClick={() => onClick({ label, value })}
                      />
                    ))}
                  </>
                )}
              </SelectNew>
            </Container>
            <Container
              justifycontent="space-between"
              className={classNames(spacing.w100p, spacing.mt16)}
            >
              <InputNew
                name="zipCode"
                label="ZIP code"
                width="192"
                value={zipCodeValue}
                onChange={(e) => {
                  setZipCodeValue(e.target.value);
                }}
              />
              <InputNew
                name="street"
                label="Street"
                width="408"
                value={streetValue}
                onChange={(e) => {
                  setStreetValue(e.target.value);
                }}
              />
            </Container>
          </>
        )}
      </Container>
    </Modal>
  );
};

export const CreateNewCustomerBillingTab: React.FC = () => {
  const [billingTabActive, setBillingTabActive] = useState<BillingType>('bankAccount');
  type BillingType = 'creditCard' | 'bankAccount' | 'wallet';
  interface TabsType<T = string> {
    type: T;
    label: string;
    icon: ReactNode;
  }
  const billingTabs: TabsType<BillingType>[] = [
    {
      type: 'creditCard',
      label: 'Credit Card',
      icon: <CreditCardIcon />,
    },
    {
      type: 'bankAccount',
      label: 'Bank Account',
      icon: <BankIcon />,
    },
    {
      type: 'wallet',
      label: 'Wallet',
      icon: <WalletIcon />,
    },
  ];
  const [isDefaultState, setIsDefaultState] = useState<boolean>(false);
  const [isModalAddShow, setIsModalAddShow] = useState<boolean>(false);
  const [cardNumber, setCardNumber] = useState<string>('');
  const [iban, setIban] = useState<string>('');
  const [bic, setBic] = useState<string>('');
  const [routing, setRouting] = useState<string>('');
  const [bankType, setBankType] = useState<'routing' | 'bicAccount' | 'bicIBAN'>('routing');
  return (
    <Modal
      size="720"
      background="grey-10"
      type="new"
      pluginScrollDisabled
      header={
        <ModalHeaderNew
          background="grey-10"
          title="Create new customer"
          border
          onClose={() => {}}
        />
      }
      footer={
        <ModalFooterNew background="grey-10">
          <Button width="md" onClick={() => {}}>
            Save
          </Button>
        </ModalFooterNew>
      }
    >
      <MenuTabWrapper>
        <MenuTab id="general" onClick={() => {}}>
          General
        </MenuTab>
        <MenuTab id="contacts" onClick={() => {}}>
          Contacts
        </MenuTab>
        <MenuTab id="billing" active onClick={() => {}}>
          Billing
        </MenuTab>
        <MenuTab id="accounting" onClick={() => {}}>
          Accounting
        </MenuTab>
      </MenuTabWrapper>
      <Container
        flexdirection="column"
        borderRadius="20"
        background="white-100"
        className={classNames(spacing.mt24, spacing.p24)}
        gap="24"
        overflow="auto"
      >
        <Container justifycontent="space-between" alignitems="center">
          <Text type="body-regular-14">Billing</Text>
          <Container gap="8">
            <Container alignitems="center" gap="4">
              <Icon icon={<StarFilledIcon />} path="grey-90" />
              <Text color="grey-90">Default</Text>
            </Container>
            <Tag
              color="grey"
              icon={<CreditCardIcon />}
              // icon={<BankIcon />}
              // icon={<WalletIcon />}
              text={`Bank of America | ${handleAccountLength('4664482943308584')} | USD`}
              cursor="default"
            />
          </Container>
        </Container>
        <Divider fullHorizontalWidth />
        <Container justifycontent="space-between" alignitems="center">
          <GeneralTabWrapper>
            {billingTabs.map(({ type, label, icon }) => (
              <GeneralTab
                key={type}
                id={type}
                icon={icon}
                font="body-regular-14"
                onClick={() => setBillingTabActive(type)}
                active={billingTabActive === type}
              >
                {label}
              </GeneralTab>
            ))}
          </GeneralTabWrapper>
          <Button
            height="24"
            background="blue-10-blue-20"
            color="violet-90"
            font="text-medium"
            iconLeft={<PlusIcon />}
            onClick={() => setIsModalAddShow(true)}
          >
            Create new {billingTabs.find(({ type }) => type === billingTabActive)?.label}
          </Button>
        </Container>
        <Container
          gap="20"
          flexwrap="wrap"
          customScroll
          overflow={isModalAddShow ? 'hidden' : 'auto'}
        >
          {billingTabActive === 'creditCard' && (
            <>
              {isModalAddShow && (
                <AddNewWrapper>
                  <Container justifycontent="space-between">
                    <Text type="body-medium" color="grey-100">
                      Create new Card
                    </Text>
                    <Text type="body-medium">Bank Name</Text>
                  </Container>
                  <Container justifycontent="space-between" gap="24">
                    <InputNew
                      name="cardNumber"
                      label="Card Number"
                      width="full"
                      background="white-100"
                      placeholder="XXXX XXXX XXXX XXXX"
                      icon={<CreditCardIcon />}
                      maxLength={19}
                      value={formatCardNumber(cardNumber)}
                      onChange={({ target }) => setCardNumber(target.value)}
                      // icon={<StatusSuccessIcon />}
                      elementPosition="left"
                      action={
                        <Container
                          className={classNames(spacing.w24fixed, spacing.h100pfixed)}
                          justifycontent="center"
                          alignitems="center"
                        >
                          <Icon icon={<MastercardIcon />} clickThrough staticColor />
                        </Container>
                      }
                      // onChange={(e) => {
                      //   setNameValue(e.target.value);
                      // }}
                      // value={nameValue}
                    />

                    <SelectNew
                      name="currency"
                      onChange={() => {}}
                      placeholder="Choose option"
                      width="200"
                      label="Currency"
                      background="white-100"
                      required
                    >
                      {({ onClick, state }) => (
                        <>
                          {options.map(({ label, value }) => (
                            <OptionSingleNew
                              key={value}
                              label={label}
                              selected={state?.value === value}
                              onClick={() => onClick({ label, value })}
                            />
                          ))}
                        </>
                      )}
                    </SelectNew>
                  </Container>
                  <InputNew
                    name="cardHolder"
                    label="Card Holder"
                    width="full"
                    background="white-100"
                    // onChange={(e) => {
                    //   setNameValue(e.target.value);
                    // }}
                    // value={nameValue}
                  />
                  <Container justifycontent="flex-end" gap="24">
                    <Button
                      width="auto"
                      height="40"
                      onClick={() => setIsModalAddShow(false)}
                      background="white-100"
                      border="grey-20-grey-90"
                      color="grey-100"
                    >
                      Cancel
                    </Button>
                    <Button width="auto" height="40" onClick={() => setIsModalAddShow(false)}>
                      Create
                    </Button>
                  </Container>
                </AddNewWrapper>
              )}
              {Array.from(Array(10)).map((_, index) => (
                <BillingCardItem
                  key={index}
                  title="MonoBank"
                  secondaryTitle="Joe Biden"
                  account="****4567"
                  currency="USD"
                  paymentSystem={index % 2 === 0 ? 'mastercard' : 'visa'}
                  isDefault={isDefaultState}
                  isBlurred={isModalAddShow}
                  onToggleDefault={() => setIsDefaultState((prev) => !prev)}
                  onEdit={() => {}}
                  onDelete={() => {}}
                />
              ))}
            </>
          )}
          {billingTabActive === 'bankAccount' && (
            <>
              {isModalAddShow && (
                <AddNewWrapper>
                  <Container justifycontent="space-between">
                    <Text type="body-medium" color="grey-100">
                      Create new Card
                    </Text>
                    <Text type="body-medium">Bank Name</Text>
                  </Container>

                  <Container flexdirection="column" gap="12">
                    <Text type="caption-regular">
                      Please choose type of bank id and account details:
                    </Text>
                    <Container gap="20">
                      <Radio
                        id="routingAccount"
                        color="grey-100"
                        type="caption-regular"
                        onChange={() => setBankType('routing')}
                        name="bankType"
                        checked={bankType === 'routing'}
                      >
                        Routing number & Account number
                      </Radio>
                      <Radio
                        id="bicAccount"
                        color="grey-100"
                        type="caption-regular"
                        onChange={() => setBankType('bicAccount')}
                        name="bankType"
                        checked={bankType === 'bicAccount'}
                      >
                        BIC & Account number
                      </Radio>
                      <Radio
                        id="bicIBAN"
                        color="grey-100"
                        type="caption-regular"
                        onChange={() => setBankType('bicIBAN')}
                        name="bankType"
                        checked={bankType === 'bicIBAN'}
                      >
                        BIC & IBAN account
                      </Radio>
                    </Container>
                  </Container>
                  <Container justifycontent="space-between" gap="24">
                    {bankType === 'routing' && (
                      <InputNew
                        name="routingNumber"
                        label="Routing Number"
                        width="full"
                        background="white-100"
                        placeholder="XXXX XXXX X"
                        icon={<InfoIcon />}
                        // icon={<StatusSuccessIcon />}
                        maxLength={11}
                        elementPosition="left"
                        onChange={({ target }) => setRouting(target.value)}
                        value={formatRoutingNumber(routing)}
                        tooltip={<RoutingNumberTooltip />}
                        tooltipWidth="auto"
                      />
                    )}
                    {(bankType === 'bicIBAN' || bankType === 'bicAccount') && (
                      <InputNew
                        name="bicCode"
                        label="BIC code"
                        width="full"
                        background="white-100"
                        placeholder="XXXX XX XX XXX"
                        icon={<InfoIcon />}
                        // icon={<StatusSuccessIcon />}
                        maxLength={14}
                        elementPosition="left"
                        onChange={({ target }) => setBic(target.value)}
                        value={formatBIC(bic)}
                        tooltip={<BICSwiftCodeTooltip />}
                        tooltipWidth="auto"
                      />
                    )}
                    <SelectNew
                      name="currency"
                      onChange={() => {}}
                      placeholder="Choose option"
                      width="200"
                      label="Currency"
                      background="white-100"
                      required
                    >
                      {({ onClick, state }) => (
                        <>
                          {options.map(({ label, value }) => (
                            <OptionSingleNew
                              key={value}
                              label={label}
                              selected={state?.value === value}
                              onClick={() => onClick({ label, value })}
                            />
                          ))}
                        </>
                      )}
                    </SelectNew>
                  </Container>
                  {(bankType === 'routing' || bankType === 'bicAccount') && (
                    <InputNew
                      name="accountNumber"
                      label="Account Number"
                      width="full"
                      background="white-100"
                      // onChange={(e) => {
                      //   setNameValue(e.target.value);
                      // }}
                      // value={nameValue}
                    />
                  )}
                  {bankType === 'bicIBAN' && (
                    <InputNew
                      name="ibanAccount"
                      label="IBAN account"
                      width="full"
                      background="white-100"
                      placeholder="XX XX XXXXXXXXXXX"
                      icon={<InfoIcon />}
                      // icon={<StatusSuccessIcon />}
                      elementPosition="left"
                      // onChange={(e) => {
                      //   setNameValue(e.target.value);
                      // }}
                      value={iban}
                      onChange={({ target }) => setIban(formatIban(target.value))}
                      tooltip={<IBANNumberTooltip />}
                      tooltipWidth="auto"
                    />
                  )}
                  <Container justifycontent="flex-end" gap="24">
                    <Button
                      width="auto"
                      height="40"
                      onClick={() => setIsModalAddShow(false)}
                      background="white-100"
                      border="grey-20-grey-90"
                      color="grey-100"
                    >
                      Cancel
                    </Button>
                    <Button width="auto" height="40" onClick={() => setIsModalAddShow(false)}>
                      Create
                    </Button>
                  </Container>
                </AddNewWrapper>
              )}
              {Array.from(Array(10)).map((_, index) => (
                <BillingCardItem
                  key={index}
                  title="Chartered Bank of India, Australia and China | CHASUS33XXX"
                  tertiaryTitle="Floor 4, The Columbus Building, 7 Westferry Circus, London, United Kingdom"
                  account="3686******6015 | USD"
                  isDefault={isDefaultState}
                  isBlurred={isModalAddShow}
                  onToggleDefault={() => setIsDefaultState((prev) => !prev)}
                  onEdit={() => {}}
                  onDelete={() => {}}
                />
              ))}
            </>
          )}
          {billingTabActive === 'wallet' && (
            <>
              {isModalAddShow && (
                <AddNewWrapper>
                  <Text type="body-medium" color="grey-100">
                    Create new Wallet
                  </Text>
                  <Container justifycontent="space-between" gap="24">
                    <SelectNew
                      name="walletName"
                      label="Wallet Name"
                      onChange={() => {}}
                      placeholder="Choose option"
                      icon={<WalletIcon />}
                      width="full"
                      background="white-100"
                      required
                    >
                      {({ onClick, state }) => (
                        <>
                          {options.map(({ label, value }) => (
                            <OptionSingleNew
                              key={value}
                              label={label}
                              icon={<AfterpayIcon />}
                              selected={state?.value === value}
                              onClick={() => onClick({ label, value })}
                            />
                          ))}
                        </>
                      )}
                    </SelectNew>
                    <SelectNew
                      name="currency"
                      onChange={() => {}}
                      placeholder="Choose option"
                      width="200"
                      label="Currency"
                      background="white-100"
                      required
                    >
                      {({ onClick, state }) => (
                        <>
                          {options.map(({ label, value }) => (
                            <OptionSingleNew
                              key={value}
                              label={label}
                              selected={state?.value === value}
                              onClick={() => onClick({ label, value })}
                            />
                          ))}
                        </>
                      )}
                    </SelectNew>
                  </Container>
                  <InputNew
                    name="walletNumber"
                    label="Wallet Holder"
                    width="full"
                    background="white-100"
                    // onChange={(e) => {
                    //   setNameValue(e.target.value);
                    // }}
                    // value={nameValue}
                  />
                  <Container justifycontent="flex-end" gap="24">
                    <Button
                      width="auto"
                      height="40"
                      onClick={() => setIsModalAddShow(false)}
                      background="white-100"
                      border="grey-20-grey-90"
                      color="grey-100"
                    >
                      Cancel
                    </Button>
                    <Button width="auto" height="40" onClick={() => setIsModalAddShow(false)}>
                      Create
                    </Button>
                  </Container>
                </AddNewWrapper>
              )}
              {Array.from(Array(10)).map((_, index) => (
                <BillingCardItem
                  key={index}
                  title="Wallet name"
                  account="****2XXX"
                  currency="USD"
                  paymentSystem="afterpay"
                  isDefault={isDefaultState}
                  isBlurred={isModalAddShow}
                  onToggleDefault={() => setIsDefaultState((prev) => !prev)}
                  onEdit={() => {}}
                  onDelete={() => {}}
                />
              ))}
            </>
          )}
        </Container>
      </Container>
    </Modal>
  );
};

export const CreateNewCustomerAccountingTab: React.FC = () => {
  const [amountValue, setAmountValue] = useState('');
  return (
    <Modal
      size="720"
      background="grey-10"
      type="new"
      header={
        <ModalHeaderNew
          background="grey-10"
          title="Create new customer"
          border
          onClose={() => {}}
        />
      }
      footer={
        <ModalFooterNew background="grey-10">
          <Button width="md" onClick={() => {}}>
            Save
          </Button>
        </ModalFooterNew>
      }
    >
      <MenuTabWrapper>
        <MenuTab id="general" onClick={() => {}}>
          General
        </MenuTab>
        <MenuTab id="contacts" onClick={() => {}}>
          Contacts
        </MenuTab>
        <MenuTab id="billing" onClick={() => {}}>
          Billing
        </MenuTab>
        <MenuTab id="accounting" active onClick={() => {}}>
          Accounting
        </MenuTab>
      </MenuTabWrapper>
      <Container
        flexdirection="column"
        borderRadius="20"
        background="white-100"
        className={classNames(spacing.p24, spacing.mt24, spacing.h360)}
      >
        <AssignContainer
          titleSource="Legal entity / Business unit"
          titleTarget="Choose account"
          width="660"
        >
          <AssignLine
            order="1"
            sourceValue="BetterMe health & fit"
            sourceSecondaryValue="BetterMe"
            isAssigned={false}
          >
            <SelectNew
              onChange={() => {}}
              // defaultValue={{ label: '', value: '' }}
              label="Assign Product"
              placeholder="Assign Product"
              name="product"
              width="full"
            >
              {({ onClick, state }) => (
                <>
                  {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                    <OptionDoubleVerticalNew
                      key={value}
                      label={label}
                      secondaryLabel={secondaryLabel}
                      selected={state?.value === value}
                      onClick={() => onClick({ label, value })}
                    />
                  ))}
                </>
              )}
            </SelectNew>
          </AssignLine>
          <AssignLine
            order="1"
            sourceValue="BetterMe health & fit"
            sourceSecondaryValue="BetterMe"
            isAssigned={false}
          >
            <SelectNew
              onChange={() => {}}
              // defaultValue={{ label: '', value: '' }}
              label="Assign Product"
              placeholder="Assign Product"
              name="product"
              width="full"
            >
              {({ onClick, state }) => (
                <>
                  {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                    <OptionDoubleVerticalNew
                      key={value}
                      label={label}
                      secondaryLabel={secondaryLabel}
                      selected={state?.value === value}
                      onClick={() => onClick({ label, value })}
                    />
                  ))}
                </>
              )}
            </SelectNew>
          </AssignLine>
          <AssignLine
            order="1"
            sourceValue="BetterMe health & fit"
            sourceSecondaryValue="BetterMe"
            isAssigned={false}
          >
            <SelectNew
              onChange={() => {}}
              // defaultValue={{ label: '', value: '' }}
              label="Assign Product"
              placeholder="Assign Product"
              name="product"
              width="full"
            >
              {({ onClick, state }) => (
                <>
                  {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                    <OptionDoubleVerticalNew
                      key={value}
                      label={label}
                      secondaryLabel={secondaryLabel}
                      selected={state?.value === value}
                      onClick={() => onClick({ label, value })}
                    />
                  ))}
                </>
              )}
            </SelectNew>
          </AssignLine>
          <AssignLine
            order="1"
            sourceValue="BetterMe health & fit"
            sourceSecondaryValue="BetterMe"
            isAssigned={false}
          >
            <SelectNew
              onChange={() => {}}
              // defaultValue={{ label: '', value: '' }}
              label="Assign Product"
              placeholder="Assign Product"
              name="product"
              width="full"
            >
              {({ onClick, state }) => (
                <>
                  {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                    <OptionDoubleVerticalNew
                      key={value}
                      label={label}
                      secondaryLabel={secondaryLabel}
                      selected={state?.value === value}
                      onClick={() => onClick({ label, value })}
                    />
                  ))}
                </>
              )}
            </SelectNew>
          </AssignLine>
          <AssignLine
            order="1"
            sourceValue="BetterMe health & fit"
            sourceSecondaryValue="BetterMe"
            isAssigned={false}
          >
            <SelectNew
              onChange={() => {}}
              // defaultValue={{ label: '', value: '' }}
              label="Assign Product"
              placeholder="Assign Product"
              name="product"
              width="full"
            >
              {({ onClick, state }) => (
                <>
                  {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                    <OptionDoubleVerticalNew
                      key={value}
                      label={label}
                      secondaryLabel={secondaryLabel}
                      selected={state?.value === value}
                      onClick={() => onClick({ label, value })}
                    />
                  ))}
                </>
              )}
            </SelectNew>
          </AssignLine>
          <AssignLine
            order="1"
            sourceValue="BetterMe health & fit"
            sourceSecondaryValue="BetterMe"
            isAssigned={false}
          >
            <SelectNew
              onChange={() => {}}
              // defaultValue={{ label: '', value: '' }}
              label="Assign Product"
              placeholder="Assign Product"
              name="product"
              width="full"
            >
              {({ onClick, state }) => (
                <>
                  {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                    <OptionDoubleVerticalNew
                      key={value}
                      label={label}
                      secondaryLabel={secondaryLabel}
                      selected={state?.value === value}
                      onClick={() => onClick({ label, value })}
                    />
                  ))}
                </>
              )}
            </SelectNew>
          </AssignLine>
        </AssignContainer>
      </Container>
      <Container
        flexdirection="column"
        borderRadius="20"
        background="white-100"
        className={classNames(spacing.mt24, spacing.p24)}
      >
        <Text type="body-regular-14">Opening balance</Text>
        <Container
          justifycontent="space-between"
          className={classNames(spacing.w100p, spacing.mt24)}
        >
          <SelectNew
            name="businessDivision"
            onChange={() => {}}
            width="300"
            label="Business division"
          >
            {({ onClick, state }) => (
              <>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={state?.value === value}
                    onClick={() => onClick({ label, value })}
                  />
                ))}
              </>
            )}
          </SelectNew>
          <SelectNew name="legalEntity" onChange={() => {}} width="300" label="Legal entity">
            {({ onClick, state }) => (
              <>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={state?.value === value}
                    onClick={() => onClick({ label, value })}
                  />
                ))}
              </>
            )}
          </SelectNew>
        </Container>
        <SelectNew
          className={spacing.mt24}
          name="account"
          onChange={() => {}}
          width="full"
          label="Account"
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() => onClick({ label, value })}
                />
              ))}
            </>
          )}
        </SelectNew>
        <Container
          justifycontent="space-between"
          className={classNames(spacing.w100p, spacing.mt24)}
        >
          <InputNew
            name="amount"
            label="Amount"
            width="300"
            value={amountValue}
            onChange={(e) => {
              setAmountValue(e.target.value);
            }}
          />
          <InputDate
            label="Choose date"
            name="calendar"
            selected={new Date()}
            calendarStartDay={1}
            width="300"
            onChange={() => {}}
          />
        </Container>
      </Container>
      <Container className={spacing.mt24} justifycontent="flex-end">
        <LinkButton icon={<PlusIcon />} onClick={() => {}}>
          Add one more opening balance
        </LinkButton>
      </Container>
    </Modal>
  );
};
