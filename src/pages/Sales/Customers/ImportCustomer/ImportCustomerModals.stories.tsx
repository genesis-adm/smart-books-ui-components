import { Checkbox } from 'Components/base/Checkbox';
import { Divider } from 'Components/base/Divider';
import { Radio } from 'Components/base/Radio';
import { TooltipIcon } from 'Components/base/TooltipIcon';
import { Button } from 'Components/base/buttons/Button';
import { ButtonGroup } from 'Components/base/buttons/ButtonGroup';
import { CheckboxButton } from 'Components/base/buttons/CheckboxButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { InputDate } from 'Components/base/inputs/InputDate';
import { InputNew } from 'Components/base/inputs/InputNew';
import { InputPhone } from 'Components/base/inputs/InputPhone';
import { SelectNew } from 'Components/base/inputs/SelectNew';
import { OptionSingleNew } from 'Components/base/inputs/SelectNew/options/OptionSingleNew';
import { TextareaNew } from 'Components/base/inputs/TextareaNew';
import { MenuTab, MenuTabWrapper } from 'Components/base/tabs/MenuTab';
import { Text } from 'Components/base/typography/Text';
import { UploadContainer } from 'Components/custom/Upload/UploadContainer';
import { UploadInfo } from 'Components/custom/Upload/UploadInfo';
import { UploadItemField } from 'Components/custom/Upload/UploadItemField';
import { ProgressBar } from 'Components/elements/ProgressBar';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { TableCellWidth } from 'Components/types/gridTypes';
import { options } from 'Mocks/fakeOptions';
import { ReactComponent as ArrowLeftIcon } from 'Svg/v2/16/arrow-left.svg';
import { ReactComponent as ArrowRightIcon } from 'Svg/v2/16/arrow-right.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as QuestionIcon } from 'Svg/v2/16/question-transparent.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import { checkFileType } from 'Utils/fileHandlers';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';
import { Nullable } from '../../../../types/general';

export default {
  title: 'Pages/Sales/Customers/Modals/Import Customer',
};

export const ImportCustomerStep1: React.FC = () => {
  const [file, setFile] = useState<File | null>(null);
  const handleFileUpload = (item: Nullable<File>) => {
    setFile(item);
  };
  const handleFileDelete = () => {
    setFile(null);
  };
  const handleFileDrop = (e: React.DragEvent<HTMLElement>) => {
    const dropFile = e.dataTransfer.files[0];
    if (checkFileType(dropFile, ['xls', 'xlsx', 'csv'])) {
      setFile(e.dataTransfer.files[0]);
    }
  };
  return (
    <Modal
      size="md-extrawide"
      header={<ModalHeaderNew onClose={() => {}} />}
      pluginScrollDisabled
      footer={
        <ModalFooterNew justifycontent="space-between" border>
          <ButtonGroup align="right">
            <Button
              height="48"
              width="sm"
              background="white-100"
              color="black-100"
              border="grey-20-grey-90"
              onClick={() => {}}
            >
              Cancel
            </Button>
            <Button
              height="48"
              navigation
              iconRight={<ArrowRightIcon />}
              width="md"
              onClick={() => {}}
            >
              Next
            </Button>
          </ButtonGroup>
          <ProgressBar steps={3} activeStep={1} />
        </ModalFooterNew>
      }
    >
      <Container flexdirection="column" className={spacing.w624} alignitems="center">
        <Text className={spacing.mt32} type="h2-semibold" align="center">
          Import customers
        </Text>
        <UploadContainer className={spacing.mt48} onDrop={handleFileDrop}>
          <UploadItemField
            name="uploader"
            files={file}
            acceptFilesType="excel"
            onChange={handleFileUpload}
            onDelete={handleFileDelete}
            onReload={() => {}}
          />

          <UploadInfo
            mainText={file ? file.name : 'Drag and drop or Choose file'}
            secondaryText="XLS; XLSX; CSV; Maximum file size is 5MB"
          />
        </UploadContainer>
        <Container
          className={classNames(spacing.mt20, spacing.w440)}
          flexdirection="column"
          gap="4"
        >
          <Text type="body-regular-14">Instruction for upload file:</Text>
          <Text type="body-regular-14">• All your customer information must be in one file</Text>
          <Text type="body-regular-14">
            • The top row of your file must contain a header title for each column of information
          </Text>
        </Container>
      </Container>
    </Modal>
  );
};

export const ImportCustomerStep2: React.FC = () => (
  <Modal
    overlay="grey-10"
    size="full"
    background="grey-10"
    pluginScrollDisabled
    header={
      <ModalHeaderNew
        type="fullwidth"
        background="white-100"
        title="Import customers"
        radius
        onClose={() => {}}
      />
    }
    footer={
      <ModalFooterNew justifycontent="space-between">
        <ButtonGroup align="right">
          <Button
            height="48"
            width="sm"
            navigation
            iconLeft={<ArrowLeftIcon />}
            background="white-100"
            color="black-100"
            border="grey-20-grey-90"
            onClick={() => {}}
          >
            Back
          </Button>
          <Button
            height="48"
            width="md"
            navigation
            iconRight={<ArrowRightIcon />}
            onClick={() => {}}
          >
            Next
          </Button>
        </ButtonGroup>
        <ProgressBar steps={3} activeStep={2} />
      </ModalFooterNew>
    }
  >
    <Container
      background="white-100"
      flexdirection="column"
      radius
      flexgrow="1"
      customScroll
      className={classNames(spacing.p24, spacing.mX20, spacing.mY16)}
    >
      <Text type="body-regular-16">
        Map columns from&nbsp;
        <Text type="body-medium" display="inline-flex">
          namefile 09.10.20.xls
        </Text>
        &nbsp;for each Smart Books field
      </Text>
      <Container className={spacing.mt32} gap="32">
        <Container gap="16" flexdirection="column">
          <Container alignitems="center">
            <Container className={classNames(spacing.h50min, spacing.mr20)} alignitems="center">
              <Text type="body-regular-14" color="grey-100" className={spacing.w130fixed} noWrap>
                Customer name
                <Text type="caption-regular" color="red-90">
                  *
                </Text>
              </Text>
              <ArrowLeftIcon />
            </Container>
            <SelectNew
              name="option5"
              onChange={() => {}}
              background="transparent"
              placeholder="Choose option"
              label="Choose option"
            >
              {({ onClick, state }) => (
                <>
                  {options.map(({ label, value }) => (
                    <OptionSingleNew
                      key={value}
                      label={label}
                      selected={state?.value === value}
                      onClick={() => onClick({ label, value })}
                    />
                  ))}
                </>
              )}
            </SelectNew>
          </Container>
          <Container alignitems="center">
            <Container className={classNames(spacing.h50min, spacing.mr20)} alignitems="center">
              <Text type="body-regular-14" color="grey-100" className={spacing.w130fixed} noWrap>
                Notes
              </Text>
              <ArrowLeftIcon />
            </Container>
            <SelectNew
              name="option5"
              onChange={() => {}}
              background="transparent"
              placeholder="Choose option"
              label="Choose option"
            >
              {({ onClick, state }) => (
                <>
                  {options.map(({ label, value }) => (
                    <OptionSingleNew
                      key={value}
                      label={label}
                      selected={state?.value === value}
                      onClick={() => onClick({ label, value })}
                    />
                  ))}
                </>
              )}
            </SelectNew>
          </Container>
          <Container alignitems="center">
            <Container className={classNames(spacing.h50min, spacing.mr20)} alignitems="center">
              <Text type="body-regular-14" color="grey-100" className={spacing.w130fixed} noWrap>
                Contact person
              </Text>
              <ArrowLeftIcon />
            </Container>
            <SelectNew
              name="option5"
              onChange={() => {}}
              background="transparent"
              placeholder="Choose option"
              label="Choose option"
            >
              {({ onClick, state }) => (
                <>
                  {options.map(({ label, value }) => (
                    <OptionSingleNew
                      key={value}
                      label={label}
                      selected={state?.value === value}
                      onClick={() => onClick({ label, value })}
                    />
                  ))}
                </>
              )}
            </SelectNew>
          </Container>
        </Container>
        <Container gap="16" flexdirection="column">
          <Container alignitems="center">
            <Container className={classNames(spacing.h50min, spacing.mr20)} alignitems="center">
              <Text type="body-regular-14" color="grey-100" className={spacing.w130fixed} noWrap>
                Phone number
              </Text>
              <ArrowLeftIcon />
            </Container>
            <SelectNew
              name="option5"
              onChange={() => {}}
              background="transparent"
              placeholder="Choose option"
              label="Choose option"
            >
              {({ onClick, state }) => (
                <>
                  {options.map(({ label, value }) => (
                    <OptionSingleNew
                      key={value}
                      label={label}
                      selected={state?.value === value}
                      onClick={() => onClick({ label, value })}
                    />
                  ))}
                </>
              )}
            </SelectNew>
          </Container>
          <Container alignitems="center">
            <Container className={classNames(spacing.h50min, spacing.mr20)} alignitems="center">
              <Text type="body-regular-14" color="grey-100" className={spacing.w130fixed} noWrap>
                Email
              </Text>
              <ArrowLeftIcon />
            </Container>
            <SelectNew
              name="option5"
              onChange={() => {}}
              background="transparent"
              placeholder="Choose option"
              label="Choose option"
            >
              {({ onClick, state }) => (
                <>
                  {options.map(({ label, value }) => (
                    <OptionSingleNew
                      key={value}
                      label={label}
                      selected={state?.value === value}
                      onClick={() => onClick({ label, value })}
                    />
                  ))}
                </>
              )}
            </SelectNew>
          </Container>
        </Container>
      </Container>

      <Container className={spacing.mt32} gap="32">
        <Container gap="16" flexdirection="column">
          <Text type="body-medium">Billing address:</Text>
          <Container alignitems="center">
            <Container className={classNames(spacing.h50min, spacing.mr20)} alignitems="center">
              <Text type="body-regular-14" color="grey-100" className={spacing.w130fixed} noWrap>
                Country
              </Text>
              <ArrowLeftIcon />
            </Container>
            <SelectNew
              name="option5"
              onChange={() => {}}
              background="transparent"
              placeholder="Choose option"
              label="Choose option"
            >
              {({ onClick, state }) => (
                <>
                  {options.map(({ label, value }) => (
                    <OptionSingleNew
                      key={value}
                      label={label}
                      selected={state?.value === value}
                      onClick={() => onClick({ label, value })}
                    />
                  ))}
                </>
              )}
            </SelectNew>
          </Container>
          <Container alignitems="center">
            <Container className={classNames(spacing.h50min, spacing.mr20)} alignitems="center">
              <Text type="body-regular-14" color="grey-100" className={spacing.w130fixed} noWrap>
                State
              </Text>
              <ArrowLeftIcon />
            </Container>
            <SelectNew
              name="option5"
              onChange={() => {}}
              background="transparent"
              placeholder="Choose option"
              label="Choose option"
            >
              {({ onClick, state }) => (
                <>
                  {options.map(({ label, value }) => (
                    <OptionSingleNew
                      key={value}
                      label={label}
                      selected={state?.value === value}
                      onClick={() => onClick({ label, value })}
                    />
                  ))}
                </>
              )}
            </SelectNew>
          </Container>
          <Container alignitems="center">
            <Container className={classNames(spacing.h50min, spacing.mr20)} alignitems="center">
              <Text type="body-regular-14" color="grey-100" className={spacing.w130fixed} noWrap>
                City
              </Text>
              <ArrowLeftIcon />
            </Container>
            <SelectNew
              name="option5"
              onChange={() => {}}
              background="transparent"
              placeholder="Choose option"
              label="Choose option"
            >
              {({ onClick, state }) => (
                <>
                  {options.map(({ label, value }) => (
                    <OptionSingleNew
                      key={value}
                      label={label}
                      selected={state?.value === value}
                      onClick={() => onClick({ label, value })}
                    />
                  ))}
                </>
              )}
            </SelectNew>
          </Container>
          <Container alignitems="center">
            <Container className={classNames(spacing.h50min, spacing.mr20)} alignitems="center">
              <Text type="body-regular-14" color="grey-100" className={spacing.w130fixed} noWrap>
                ZIP code
              </Text>
              <ArrowLeftIcon />
            </Container>
            <SelectNew
              name="option5"
              onChange={() => {}}
              background="transparent"
              placeholder="Choose option"
              label="Choose option"
            >
              {({ onClick, state }) => (
                <>
                  {options.map(({ label, value }) => (
                    <OptionSingleNew
                      key={value}
                      label={label}
                      selected={state?.value === value}
                      onClick={() => onClick({ label, value })}
                    />
                  ))}
                </>
              )}
            </SelectNew>
          </Container>
          <Container alignitems="center">
            <Container className={classNames(spacing.h50min, spacing.mr20)} alignitems="center">
              <Text type="body-regular-14" color="grey-100" className={spacing.w130fixed} noWrap>
                Street name
              </Text>
              <ArrowLeftIcon />
            </Container>
            <SelectNew
              name="option5"
              onChange={() => {}}
              background="transparent"
              placeholder="Choose option"
              label="Choose option"
            >
              {({ onClick, state }) => (
                <>
                  {options.map(({ label, value }) => (
                    <OptionSingleNew
                      key={value}
                      label={label}
                      selected={state?.value === value}
                      onClick={() => onClick({ label, value })}
                    />
                  ))}
                </>
              )}
            </SelectNew>
          </Container>
        </Container>
        <Container gap="16" flexdirection="column">
          <Text type="body-medium">Shipping address:</Text>
          <Container alignitems="center">
            <Container className={classNames(spacing.h50min, spacing.mr20)} alignitems="center">
              <Text type="body-regular-14" color="grey-100" className={spacing.w130fixed} noWrap>
                Country
              </Text>
              <ArrowLeftIcon />
            </Container>
            <SelectNew
              name="option5"
              onChange={() => {}}
              background="transparent"
              placeholder="Choose option"
              label="Choose option"
            >
              {({ onClick, state }) => (
                <>
                  {options.map(({ label, value }) => (
                    <OptionSingleNew
                      key={value}
                      label={label}
                      selected={state?.value === value}
                      onClick={() => onClick({ label, value })}
                    />
                  ))}
                </>
              )}
            </SelectNew>
          </Container>
          <Container alignitems="center">
            <Container className={classNames(spacing.h50min, spacing.mr20)} alignitems="center">
              <Text type="body-regular-14" color="grey-100" className={spacing.w130fixed} noWrap>
                State
              </Text>
              <ArrowLeftIcon />
            </Container>
            <SelectNew
              name="option5"
              onChange={() => {}}
              background="transparent"
              placeholder="Choose option"
              label="Choose option"
            >
              {({ onClick, state }) => (
                <>
                  {options.map(({ label, value }) => (
                    <OptionSingleNew
                      key={value}
                      label={label}
                      selected={state?.value === value}
                      onClick={() => onClick({ label, value })}
                    />
                  ))}
                </>
              )}
            </SelectNew>
          </Container>
          <Container alignitems="center">
            <Container className={classNames(spacing.h50min, spacing.mr20)} alignitems="center">
              <Text type="body-regular-14" color="grey-100" className={spacing.w130fixed} noWrap>
                City
              </Text>
              <ArrowLeftIcon />
            </Container>
            <SelectNew
              name="option5"
              onChange={() => {}}
              background="transparent"
              placeholder="Choose option"
              label="Choose option"
            >
              {({ onClick, state }) => (
                <>
                  {options.map(({ label, value }) => (
                    <OptionSingleNew
                      key={value}
                      label={label}
                      selected={state?.value === value}
                      onClick={() => onClick({ label, value })}
                    />
                  ))}
                </>
              )}
            </SelectNew>
          </Container>
          <Container alignitems="center">
            <Container className={classNames(spacing.h50min, spacing.mr20)} alignitems="center">
              <Text type="body-regular-14" color="grey-100" className={spacing.w130fixed} noWrap>
                ZIP code
              </Text>
              <ArrowLeftIcon />
            </Container>
            <SelectNew
              name="option5"
              onChange={() => {}}
              background="transparent"
              placeholder="Choose option"
              label="Choose option"
            >
              {({ onClick, state }) => (
                <>
                  {options.map(({ label, value }) => (
                    <OptionSingleNew
                      key={value}
                      label={label}
                      selected={state?.value === value}
                      onClick={() => onClick({ label, value })}
                    />
                  ))}
                </>
              )}
            </SelectNew>
          </Container>
          <Container alignitems="center">
            <Container className={classNames(spacing.h50min, spacing.mr20)} alignitems="center">
              <Text type="body-regular-14" color="grey-100" className={spacing.w130fixed} noWrap>
                Street name
              </Text>
              <ArrowLeftIcon />
            </Container>
            <SelectNew
              name="option5"
              onChange={() => {}}
              background="transparent"
              placeholder="Choose option"
              label="Choose option"
            >
              {({ onClick, state }) => (
                <>
                  {options.map(({ label, value }) => (
                    <OptionSingleNew
                      key={value}
                      label={label}
                      selected={state?.value === value}
                      onClick={() => onClick({ label, value })}
                    />
                  ))}
                </>
              )}
            </SelectNew>
          </Container>
        </Container>
      </Container>

      <Container className={spacing.mt32} gap="32">
        <Container gap="16" flexdirection="column">
          <Text type="body-medium">Bank payments details</Text>
          <Container alignitems="center">
            <Container className={classNames(spacing.h50min, spacing.mr20)} alignitems="center">
              <Text type="body-regular-14" color="grey-100" className={spacing.w130fixed} noWrap>
                Customer bank
              </Text>
              <ArrowLeftIcon />
            </Container>
            <SelectNew
              name="option5"
              onChange={() => {}}
              background="transparent"
              placeholder="Choose option"
              label="Choose option"
            >
              {({ onClick, state }) => (
                <>
                  {options.map(({ label, value }) => (
                    <OptionSingleNew
                      key={value}
                      label={label}
                      selected={state?.value === value}
                      onClick={() => onClick({ label, value })}
                    />
                  ))}
                </>
              )}
            </SelectNew>
          </Container>
          <Container alignitems="center">
            <Container className={classNames(spacing.h50min, spacing.mr20)} alignitems="center">
              <Text type="body-regular-14" color="grey-100" className={spacing.w130fixed} noWrap>
                Customer bank ID
              </Text>
              <ArrowLeftIcon />
            </Container>
            <SelectNew
              name="option5"
              onChange={() => {}}
              background="transparent"
              placeholder="Choose option"
              label="Choose option"
            >
              {({ onClick, state }) => (
                <>
                  {options.map(({ label, value }) => (
                    <OptionSingleNew
                      key={value}
                      label={label}
                      selected={state?.value === value}
                      onClick={() => onClick({ label, value })}
                    />
                  ))}
                </>
              )}
            </SelectNew>
          </Container>
          <Container alignitems="center">
            <Container className={classNames(spacing.h50min, spacing.mr20)} alignitems="center">
              <Text type="body-regular-14" color="grey-100" className={spacing.w130fixed} noWrap>
                Customer bank account
              </Text>
              <ArrowLeftIcon />
            </Container>
            <SelectNew
              name="option5"
              onChange={() => {}}
              background="transparent"
              placeholder="Choose option"
              label="Choose option"
            >
              {({ onClick, state }) => (
                <>
                  {options.map(({ label, value }) => (
                    <OptionSingleNew
                      key={value}
                      label={label}
                      selected={state?.value === value}
                      onClick={() => onClick({ label, value })}
                    />
                  ))}
                </>
              )}
            </SelectNew>
          </Container>
        </Container>
      </Container>
    </Container>
  </Modal>
);

export const ImportCustomerStep3: React.FC = () => {
  const sorting = {
    sortType: null,
    tooltipText: 'Sorting tooltip text',
  };
  const tableHead = [
    {
      minWidth: '48',
      title: <Checkbox name="tickAll" checked onChange={() => {}} />,
      padding: 'none',
    },
    { minWidth: '160', title: 'Customer name', sorting },
    { minWidth: '160', title: 'Notes', sorting },
    { minWidth: '160', title: 'Contact person', sorting },
    { minWidth: '160', title: 'Phone number', sorting },
    { minWidth: '220', title: 'Email', sorting },
    { minWidth: '160', title: 'Country', secondaryTitle: 'Billing', sorting },
    { minWidth: '160', title: 'State', secondaryTitle: 'Billing', sorting },
    { minWidth: '160', title: 'City', secondaryTitle: 'Billing', sorting },
    { minWidth: '160', title: 'ZIP code', secondaryTitle: 'Billing', sorting },
    { minWidth: '180', title: 'Street name', secondaryTitle: 'Billing', sorting },
    { minWidth: '160', title: 'Country', secondaryTitle: 'Shipping', sorting },
    { minWidth: '160', title: 'State', secondaryTitle: 'Shipping', sorting },
    { minWidth: '160', title: 'City', secondaryTitle: 'Shipping', sorting },
    { minWidth: '160', title: 'ZIP code', secondaryTitle: 'Shipping', sorting },
    { minWidth: '180', title: 'Street name', secondaryTitle: 'Shipping', sorting },
    { minWidth: '160', title: 'Customer bank', sorting },
    { minWidth: '160', title: 'Customer bank ID', sorting },
    { minWidth: '200', title: 'Customer bank account', sorting },
  ];

  return (
    <Modal
      overlay="grey-10"
      size="full"
      background="grey-10"
      pluginScrollDisabled
      header={
        <ModalHeaderNew
          type="fullwidth"
          background="white-100"
          title="Import customers"
          radius
          onClose={() => {}}
        />
      }
      footer={
        <ModalFooterNew justifycontent="space-between">
          <ButtonGroup align="right">
            <Button
              height="48"
              width="sm"
              navigation
              iconLeft={<ArrowLeftIcon />}
              background="white-100"
              color="black-100"
              border="grey-20-grey-90"
              onClick={() => {}}
            >
              Back
            </Button>
            <Button height="48" width="md" onClick={() => {}}>
              Import customers
            </Button>
          </ButtonGroup>
          <ProgressBar steps={3} activeStep={3} />
        </ModalFooterNew>
      }
    >
      <Container flexdirection="column" radius flexgrow="1" overflow="hidden">
        <Container flexdirection="column" className={spacing.mX32}>
          <Text type="body-medium" className={classNames(spacing.mt32, spacing.ml16)}>
            Select customers to import:
          </Text>
          <Container
            className={classNames(spacing.mt12, spacing.ml16)}
            alignitems="center"
            justifycontent="space-between"
          >
            <Container alignitems="center">
              <Text type="text-regular" color="grey-100">
                Organization:&nbsp;
              </Text>
              <Text type="caption-regular">GENESIS</Text>
            </Container>
            <Container alignitems="center">
              <Text type="text-medium" color="grey-100">
                Number of customers:&nbsp;
              </Text>
              <Text type="caption-semibold">145</Text>
              <Divider type="vertical" height="20" background="grey-90" className={spacing.mX16} />
              <Text type="text-medium" color="grey-100">
                Selected:&nbsp;
              </Text>
              <Text type="caption-semibold">145</Text>
            </Container>
          </Container>
        </Container>
        <Container flexdirection="column" flexgrow="1" className={spacing.mX32} overflow="hidden">
          <TableNew
            noResults={false}
            className={spacing.mt24}
            tableHead={
              <TableHeadNew>
                {tableHead.map((item, index) => (
                  <TableTitleNew
                    key={index}
                    minWidth={item.minWidth as TableCellWidth}
                    // flexgrow={item.flexgrow as TableTitleNewProps['flexgrow']}
                    padding={item.padding as TableTitleNewProps['padding']}
                    // align={item.align as TableTitleNewProps['align']}
                    title={item.title}
                    secondaryTitle={item.secondaryTitle}
                    sorting={item.sorting}
                    onClick={() => {}}
                  />
                ))}
              </TableHeadNew>
            }
          >
            <TableBodyNew>
              {Array.from(Array(39)).map((_, index) => (
                <TableRowNew key={index} background="white">
                  <TableCellNew minWidth="48" justifycontent="center" alignitems="center">
                    <Checkbox name={`line-${index}`} checked onChange={() => {}} />
                  </TableCellNew>
                  <TableCellTextNew minWidth="160" value="Customer long name value" />
                  <TableCellTextNew minWidth="160" value="Notes long long name value" />
                  <TableCellTextNew minWidth="160" value="Contact person" />
                  <TableCellTextNew minWidth="160" value="+3809700000021" />
                  <TableCellTextNew minWidth="220" value="customeremail@gmail.com" />
                  <TableCellTextNew minWidth="160" value="United States" />
                  <TableCellTextNew minWidth="160" value="Delaware" />
                  <TableCellTextNew minWidth="160" value="Wilmington" />
                  <TableCellTextNew minWidth="160" value="021564" />
                  <TableCellTextNew minWidth="180" value="1, Long Long Long Street Name" />
                  <TableCellTextNew minWidth="160" value="United States" />
                  <TableCellTextNew minWidth="160" value="Delaware" />
                  <TableCellTextNew minWidth="160" value="Wilmington" />
                  <TableCellTextNew minWidth="160" value="021564" />
                  <TableCellTextNew minWidth="180" value="1, Long Long Long Street Name" />
                  <TableCellTextNew minWidth="160" value="Customer bank name" />
                  <TableCellTextNew minWidth="160" value="Bank ID" />
                  <TableCellTextNew minWidth="200" value="Bank account" />
                </TableRowNew>
              ))}
            </TableBodyNew>
          </TableNew>
        </Container>
      </Container>
    </Modal>
  );
};
