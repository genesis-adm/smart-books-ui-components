import { Meta, Story } from '@storybook/react';
import { DropDown } from 'Components/DropDown';
import { Checkbox } from 'Components/base/Checkbox';
import { Divider } from 'Components/base/Divider';
import { LoadingLine } from 'Components/base/LoadingLine';
import { Button } from 'Components/base/buttons/Button';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { ContainerMain } from 'Components/base/grid/ContainerMain';
import { MainPageContentContainer } from 'Components/base/grid/MainPageContentContainer';
import { MenuTab, MenuTabWrapper } from 'Components/base/tabs/MenuTab';
import { Text } from 'Components/base/typography/Text';
import { MainPageEmpty } from 'Components/custom/MainPage/MainPageEmpty';
import { HeaderSB } from 'Components/custom/Stories/HeaderSB';
import { NavigationSB } from 'Components/custom/Stories/NavigationSB';
import { Pagination } from 'Components/custom/Stories/Pagination/Pagination';
import { ExportDropdown } from 'Components/elements/ExportDropdown';
import { Logo } from 'Components/elements/Logo';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { FilterDropDown } from 'Components/elements/filters/FilterDropDown';
import { FilterToggleButton } from 'Components/elements/filters/FilterToggleButton';
import { FiltersWrapper } from 'Components/elements/filters/FiltersWrapper';
import { Search } from 'Components/inputs/Search';
import { ReactComponent as CopyIcon } from 'Svg/v2/16/copy.svg';
import { ReactComponent as RemoveIcon } from 'Svg/v2/16/cross-in-filled-circle.svg';
import { ReactComponent as EyeIcon } from 'Svg/v2/16/eye.svg';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as DropdownIcon } from 'Svg/v2/16/three-dots.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import { ReactComponent as UploadIcon } from 'Svg/v2/16/upload.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';
import type { TableCellWidth } from '../../../components/types/gridTypes';

const SalesProductsMainPageComponent: Story = ({ storyState }) => {
  const [toggle, toggleMenu] = useState(false);
  const [showFilters, setShowFilters] = useState(false);
  const sorting = {
    sortType: null,
    tooltipText: 'Sorting tooltip text',
  };
  const filters = [
    { title: 'Product name' },
    { title: 'Unit measures' },
    { title: 'Product type' },
    { title: 'Business division' },
  ];
  const tableHead = [
    {
      minWidth: '48',
      title: <Checkbox name="tickAll" checked onChange={() => {}} />,
      padding: 'none',
    },
    {
      minWidth: '110',
      title: 'Product logo',
    },
    {
      minWidth: '160',
      title: 'Product name & ID',
      sorting,
    },
    {
      minWidth: '110',
      title: 'Unit measure',
    },
    {
      minWidth: '120',
      title: 'SKU',
    },
    {
      minWidth: '120',
      title: 'Product type',
      sorting,
    },
    {
      minWidth: '230',
      title: 'Description',
      flexgrow: '1',
    },
    {
      minWidth: '240',
      title: 'Business division',
      sorting,
    },
    {
      minWidth: '100',
      title: 'Action',
      align: 'center',
    },
  ];
  return (
    <Container fullscreen flexwrap="nowrap" background={'grey-10'}>
      <NavigationSB opened={toggle} onToggleOpen={() => toggleMenu(!toggle)} />
      <ContainerMain navOpened={toggle}>
        <HeaderSB />
        <Container flexdirection="column" className={spacing.mX32}>
          <Text type="h1-semibold">Sales</Text>
          <MenuTabWrapper className={spacing.mt24}>
            <MenuTab id="allSales" active={false} onClick={() => {}}>
              All sales
            </MenuTab>
            <MenuTab id="customers" active={false} onClick={() => {}}>
              Customers
            </MenuTab>
            <MenuTab id="products" active onClick={() => {}}>
              Products
            </MenuTab>
          </MenuTabWrapper>
        </Container>
        <MainPageContentContainer>
          {storyState === 'loaded' && (
            <>
              <Container
                className={classNames(spacing.mX24, spacing.mt24)}
                justifycontent="space-between"
                gap="24"
              >
                <Container gap="24" alignitems="center">
                  <FilterToggleButton
                    showFilters={showFilters}
                    filtersSelected={false}
                    onClear={() => {}}
                    onClick={() => {
                      setShowFilters((prevState) => !prevState);
                    }}
                  />
                  <Search
                    width="320"
                    height="40"
                    name="search-sales-products-mainPage"
                    placeholder="Search by ID and description"
                    value=""
                    onChange={() => {}}
                    searchParams={[]}
                    onSearch={() => {}}
                    onClear={() => {}}
                  />
                </Container>
                <Container gap="24" alignitems="center">
                  <Divider type="vertical" background={'grey-90'} height="20" />
                  <ExportDropdown onExportToXLS={() => {}} onExportToCSV={() => {}} />
                  <IconButton
                    size="40"
                    background={'grey-10-grey-20'}
                    color="grey-100"
                    icon={<UploadIcon />}
                    tooltip="Import files"
                    onClick={() => {}}
                  />
                  <Button
                    background={'violet-90-violet-100'}
                    color="white-100"
                    width="140"
                    height="40"
                    iconLeft={<PlusIcon />}
                  >
                    Create
                  </Button>
                </Container>
              </Container>
              <FiltersWrapper active={showFilters}>
                {filters.map((item) => (
                  <FilterDropDown
                    key={item.title}
                    title={item.title}
                    value="All"
                    onClear={() => {}}
                    selectedAmount={0}
                  />
                ))}
              </FiltersWrapper>
              <TableNew
                noResults={false}
                className={classNames(spacing.mt24, spacing.ml24, spacing.pr24)}
                tableHead={
                  <TableHeadNew>
                    {tableHead.map((item, index) => (
                      <TableTitleNew
                        key={index}
                        minWidth={item.minWidth as TableCellWidth}
                        flexgrow={item.flexgrow as TableTitleNewProps['flexgrow']}
                        padding={item.padding as TableTitleNewProps['padding']}
                        align={item.align as TableTitleNewProps['align']}
                        title={item.title}
                        sorting={item.sorting}
                        onClick={() => {}}
                      />
                    ))}
                  </TableHeadNew>
                }
              >
                <TableBodyNew>
                  {Array.from(Array(39)).map((_, index) => (
                    <TableRowNew key={index} gap="8">
                      <TableCellNew minWidth="48" justifycontent="center" alignitems="center">
                        <Checkbox name={`line-${index}`} checked onChange={() => {}} />
                      </TableCellNew>
                      <TableCellNew minWidth="110" alignitems="center" justifycontent="center">
                        <Logo
                          size="44"
                          content="company"
                          name="Genesis"
                          img="https://picsum.photos/60"
                          color="#6367F6"
                        />
                      </TableCellNew>
                      <TableCellTextNew
                        minWidth="160"
                        value="Product name line"
                        secondaryValue="ID 000000000"
                        type="caption-semibold"
                        secondaryType="text-regular"
                        noWrap
                        lineClamp="none"
                      />
                      <TableCellTextNew minWidth="110" type="text-regular" value="Kg" />
                      <TableCellTextNew
                        minWidth="120"
                        type="text-regular"
                        value="1234fdsf"
                        counter="+2"
                        counterTooltip="More SKUs"
                      />
                      <TableCellTextNew minWidth="120" type="text-regular" value="Digital goods" />
                      <TableCellTextNew
                        minWidth="230"
                        flexgrow="1"
                        type="text-regular"
                        value="Description Text"
                      />
                      <TableCellTextNew
                        minWidth="240"
                        type="text-regular"
                        value="Genesis Media & Mobile Application"
                      />
                      <TableCellNew minWidth="100" alignitems="center" justifycontent="center">
                        <DropDown
                          flexdirection="column"
                          padding="8"
                          gap="4"
                          control={({ handleOpen }) => (
                            <IconButton
                              icon={<DropdownIcon />}
                              onClick={handleOpen}
                              background={'transparent'}
                              color="grey-100-violet-90"
                            />
                          )}
                        >
                          <DropDownButton size="160" icon={<EyeIcon />} onClick={() => {}}>
                            Open
                          </DropDownButton>
                          <DropDownButton size="160" icon={<CopyIcon />} onClick={() => {}}>
                            Duplicate
                          </DropDownButton>
                          <DropDownButton size="160" icon={<EditIcon />} onClick={() => {}}>
                            Edit
                          </DropDownButton>
                          <DropDownButton size="160" icon={<RemoveIcon />} onClick={() => {}}>
                            Make inactive
                          </DropDownButton>
                          <Divider type="horizontal" fullHorizontalWidth className={spacing.mY4} />
                          <DropDownButton
                            size="204"
                            type="danger"
                            icon={<TrashIcon />}
                            onClick={() => {}}
                          >
                            Delete
                          </DropDownButton>
                        </DropDown>
                      </TableCellNew>
                    </TableRowNew>
                  ))}
                </TableBodyNew>
              </TableNew>
              <Pagination padding="16" />
            </>
          )}
          {storyState === 'empty' && (
            <MainPageEmpty
              title="Create new Products"
              description="You have no products created. Your created products will be displayed here."
              action={
                <>
                  <Button
                    width="220"
                    height="40"
                    background={'blue-10-blue-20'}
                    color="violet-90-violet-100"
                    iconLeft={<PlusIcon />}
                    onClick={() => {}}
                  >
                    Create Products
                  </Button>
                  <Button
                    width="220"
                    height="40"
                    background={'blue-10-blue-20'}
                    color="violet-90-violet-100"
                    iconLeft={<UploadIcon />}
                    onClick={() => {}}
                  >
                    Import Products
                  </Button>
                </>
              }
            />
          )}
        </MainPageContentContainer>
        <LoadingLine loadingCompleted={storyState !== 'loading'} />
      </ContainerMain>
    </Container>
  );
};

export default {
  title: 'Pages/Sales/Products/Sales Products Main Page',
  component: SalesProductsMainPageComponent,
  argTypes: {
    storyState: {
      options: ['empty', 'loading', 'loaded'],
      control: {
        type: 'radio',
        labels: {
          empty: 'Empty page',
          loading: 'Page is loading',
          loaded: 'Page loaded',
        },
      },
    },
  },
} as Meta;

export const SalesProductsMainPage = SalesProductsMainPageComponent.bind({});
SalesProductsMainPage.args = {
  storyState: 'loaded',
};
