import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { Checkbox } from 'Components/base/Checkbox';
import { Divider } from 'Components/base/Divider';
import { Radio } from 'Components/base/Radio';
import { TooltipIcon } from 'Components/base/TooltipIcon';
import { Button } from 'Components/base/buttons/Button';
import { ButtonGroup } from 'Components/base/buttons/ButtonGroup';
import { CheckboxButton } from 'Components/base/buttons/CheckboxButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { InputDate } from 'Components/base/inputs/InputDate';
import { InputNew } from 'Components/base/inputs/InputNew';
import { InputPhone } from 'Components/base/inputs/InputPhone';
import { SelectNew } from 'Components/base/inputs/SelectNew';
import { OptionSingleNew } from 'Components/base/inputs/SelectNew/options/OptionSingleNew';
import { TextareaNew } from 'Components/base/inputs/TextareaNew';
import { MenuTab, MenuTabWrapper } from 'Components/base/tabs/MenuTab';
import { Text } from 'Components/base/typography/Text';
import { UploadContainer } from 'Components/custom/Upload/UploadContainer';
import { UploadInfo } from 'Components/custom/Upload/UploadInfo';
import { UploadItemField } from 'Components/custom/Upload/UploadItemField';
import { ProgressBar } from 'Components/elements/ProgressBar';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableSelect } from 'Components/elements/Table/TableSelect';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { TableCellWidth } from 'Components/types/gridTypes';
import { options } from 'Mocks/fakeOptions';
import { ReactComponent as ArrowLeftIcon } from 'Svg/v2/16/arrow-left.svg';
import { ReactComponent as ArrowRightIcon } from 'Svg/v2/16/arrow-right.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as QuestionIcon } from 'Svg/v2/16/question-transparent.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import { checkFileType } from 'Utils/fileHandlers';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';
import { Nullable } from '../../../../types/general';

export default {
  title: 'Pages/Sales/Products/Modals/Import Product',
};

export const ImportProductStep1: React.FC = () => {
  const [file, setFile] = useState<File | null>(null);
  const handleFileUpload = (item: Nullable<File>) => {
    setFile(item);
  };
  const handleFileDelete = () => {
    setFile(null);
  };
  const handleFileDrop = (e: React.DragEvent<HTMLElement>) => {
    const dropFile = e.dataTransfer.files[0];
    if (checkFileType(dropFile, ['xls', 'xlsx', 'csv'])) {
      setFile(e.dataTransfer.files[0]);
    }
  };
  return (
    <Modal
      size="md-extrawide"
      header={<ModalHeaderNew onClose={() => {}} />}
      pluginScrollDisabled
      footer={
        <ModalFooterNew justifycontent="space-between" border>
          <ButtonGroup align="right">
            <Button
              height="48"
              width="sm"
              background="white-100"
              color="black-100"
              border="grey-20-grey-90"
              onClick={() => {}}
            >
              Cancel
            </Button>
            <Button
              height="48"
              navigation
              iconRight={<ArrowRightIcon />}
              width="md"
              onClick={() => {}}
            >
              Next
            </Button>
          </ButtonGroup>
          <ProgressBar steps={3} activeStep={1} />
        </ModalFooterNew>
      }
    >
      <Container flexdirection="column" className={spacing.w624} alignitems="center">
        <Text className={spacing.mt32} type="h2-semibold" align="center">
          Import products
        </Text>
        <UploadContainer className={spacing.mt48} onDrop={handleFileDrop}>
          <UploadItemField
            name="uploader"
            files={file}
            acceptFilesType="excel"
            onChange={handleFileUpload}
            onDelete={handleFileDelete}
            onReload={() => {}}
          />

          <UploadInfo
            mainText={file ? file.name : 'Drag and drop or Choose file'}
            secondaryText="XLS; XLSX; CSV; Maximum file size is 5MB"
          />
        </UploadContainer>
        <Container
          className={classNames(spacing.mt20, spacing.w440)}
          flexdirection="column"
          gap="4"
        >
          <Text type="body-regular-14">Instruction for upload file:</Text>
          <Text type="body-regular-14">• All your product information must be in one file</Text>
          <Text type="body-regular-14">
            • The top row of your file must contain a header title for each column of information
          </Text>
          <Text type="body-regular-14">
            • Product name is the only required field and have to be unique
          </Text>
        </Container>
      </Container>
    </Modal>
  );
};

export const ImportProductStep2: React.FC = () => (
  <Modal
    overlay="grey-10"
    size="full"
    background="grey-10"
    pluginScrollDisabled
    header={
      <ModalHeaderNew
        type="fullwidth"
        background="white-100"
        title="Import products"
        radius
        onClose={() => {}}
      />
    }
    footer={
      <ModalFooterNew justifycontent="space-between">
        <ButtonGroup align="right">
          <Button
            height="48"
            width="sm"
            navigation
            iconLeft={<ArrowLeftIcon />}
            background="white-100"
            color="black-100"
            border="grey-20-grey-90"
            onClick={() => {}}
          >
            Back
          </Button>
          <Button
            height="48"
            width="md"
            navigation
            iconRight={<ArrowRightIcon />}
            onClick={() => {}}
          >
            Next
          </Button>
        </ButtonGroup>
        <ProgressBar steps={3} activeStep={2} />
      </ModalFooterNew>
    }
  >
    <Container
      background="white-100"
      flexdirection="column"
      radius
      flexgrow="1"
      customScroll
      className={classNames(spacing.p24, spacing.mX20, spacing.mY16)}
    >
      <Text type="body-regular-16">File name</Text>
      <Container className={spacing.mt32} gap="32">
        <Container gap="16" flexdirection="column">
          <Container alignitems="center">
            <Container className={classNames(spacing.h50min, spacing.mr20)} alignitems="center">
              <Text type="body-regular-14" color="grey-100" className={spacing.w130fixed} noWrap>
                Product name
                <Text type="caption-regular" color="red-90">
                  *
                </Text>
              </Text>
              <ArrowLeftIcon />
            </Container>
            <SelectNew
              name="option5"
              onChange={() => {}}
              background="transparent"
              placeholder="Choose option"
              label="Choose option"
            >
              {({ onClick, state, selectElement }) => (
                <SelectDropdown selectElement={selectElement}>
                  <SelectDropdownBody>
                    <SelectDropdownItemList>
                      {options.map(({ label, value }) => (
                        <OptionSingleNew
                          key={value}
                          label={label}
                          selected={state?.value === value}
                          onClick={() =>
                            onClick({
                              label,
                              value,
                            })
                          }
                        />
                      ))}
                    </SelectDropdownItemList>
                  </SelectDropdownBody>
                </SelectDropdown>
              )}
            </SelectNew>
          </Container>
          <Container alignitems="center">
            <Container className={classNames(spacing.h50min, spacing.mr20)} alignitems="center">
              <Text type="body-regular-14" color="grey-100" className={spacing.w130fixed} noWrap>
                SKU
              </Text>
              <ArrowLeftIcon />
            </Container>
            <SelectNew
              name="option5"
              onChange={() => {}}
              background="transparent"
              placeholder="Choose option"
              label="Choose option"
            >
              {({ onClick, state, selectElement }) => (
                <SelectDropdown selectElement={selectElement}>
                  <SelectDropdownBody>
                    <SelectDropdownItemList>
                      {options.map(({ label, value }) => (
                        <OptionSingleNew
                          key={value}
                          label={label}
                          selected={state?.value === value}
                          onClick={() =>
                            onClick({
                              label,
                              value,
                            })
                          }
                        />
                      ))}
                    </SelectDropdownItemList>
                  </SelectDropdownBody>
                </SelectDropdown>
              )}
            </SelectNew>
          </Container>
          <Container alignitems="center">
            <Container className={classNames(spacing.h50min, spacing.mr20)} alignitems="center">
              <Text type="body-regular-14" color="grey-100" className={spacing.w130fixed} noWrap>
                Notes
              </Text>
              <ArrowLeftIcon />
            </Container>
            <SelectNew
              name="option5"
              onChange={() => {}}
              background="transparent"
              placeholder="Choose option"
              label="Choose option"
            >
              {({ onClick, state, selectElement }) => (
                <SelectDropdown selectElement={selectElement}>
                  <SelectDropdownBody>
                    <SelectDropdownItemList>
                      {options.map(({ label, value }) => (
                        <OptionSingleNew
                          key={value}
                          label={label}
                          selected={state?.value === value}
                          onClick={() =>
                            onClick({
                              label,
                              value,
                            })
                          }
                        />
                      ))}
                    </SelectDropdownItemList>
                  </SelectDropdownBody>
                </SelectDropdown>
              )}
            </SelectNew>
          </Container>
        </Container>
      </Container>
    </Container>
  </Modal>
);

export const ImportProductStep3: React.FC = () => {
  const sorting = {
    sortType: null,
    tooltipText: 'Sorting tooltip text',
  };
  const tableHead = [
    {
      minWidth: '48',
      title: <Checkbox name="tickAll" checked onChange={() => {}} />,
      padding: 'none',
    },
    {
      minWidth: '220',
      title: 'Product name',
      sorting,
    },
    {
      minWidth: '270',
      title: 'Notes',
      sorting,
      flexgrow: '1',
    },
    {
      minWidth: '320',
      title: 'SKU',
      sorting,
    },
    {
      minWidth: '240',
      title: 'Business division',
      sorting,
      required: true,
    },
    {
      minWidth: '240',
      title: 'Product type',
      sorting,
      required: true,
    },
  ];

  return (
    <Modal
      overlay="grey-10"
      size="full"
      background="grey-10"
      pluginScrollDisabled
      header={
        <ModalHeaderNew
          type="fullwidth"
          background="white-100"
          title="Import products"
          radius
          onClose={() => {}}
        />
      }
      footer={
        <ModalFooterNew justifycontent="space-between">
          <ButtonGroup align="right">
            <Button
              height="48"
              width="sm"
              navigation
              iconLeft={<ArrowLeftIcon />}
              background="white-100"
              color="black-100"
              border="grey-20-grey-90"
              onClick={() => {}}
            >
              Back
            </Button>
            <Button height="48" width="md" onClick={() => {}}>
              Import products
            </Button>
          </ButtonGroup>
          <ProgressBar steps={3} activeStep={3} />
        </ModalFooterNew>
      }
    >
      <Container flexdirection="column" radius flexgrow="1" overflow="hidden">
        <Container flexdirection="column" className={spacing.mX32}>
          <Text type="body-medium" className={classNames(spacing.mt32, spacing.ml16)}>
            Select products to import:
          </Text>
          <Container
            className={classNames(spacing.mt12, spacing.ml16)}
            alignitems="center"
            justifycontent="space-between"
          >
            <Container alignitems="center">
              <Text type="text-regular" color="grey-100">
                Organization:&nbsp;
              </Text>
              <Text type="caption-regular">GENESIS</Text>
            </Container>
            <Container alignitems="center">
              <Text type="text-medium" color="grey-100">
                Number of products:&nbsp;
              </Text>
              <Text type="caption-semibold">145</Text>
              <Divider type="vertical" height="20" background="grey-90" className={spacing.mX16} />
              <Text type="text-medium" color="grey-100">
                Selected:&nbsp;
              </Text>
              <Text type="caption-semibold">145</Text>
            </Container>
          </Container>
        </Container>
        <Container flexdirection="column" flexgrow="1" className={spacing.mX32} overflow="hidden">
          <TableNew
            noResults={false}
            className={spacing.mt24}
            tableHead={
              <TableHeadNew>
                {tableHead.map((item, index) => (
                  <TableTitleNew
                    key={index}
                    minWidth={item.minWidth as TableCellWidth}
                    flexgrow={item.flexgrow as TableTitleNewProps['flexgrow']}
                    padding={item.padding as TableTitleNewProps['padding']}
                    required={item.required}
                    title={item.title}
                    sorting={item.sorting}
                    onClick={() => {}}
                  />
                ))}
              </TableHeadNew>
            }
          >
            <TableBodyNew>
              {Array.from(Array(39)).map((_, index) => (
                <TableRowNew key={index} background="white">
                  <TableCellNew minWidth="48" justifycontent="center" alignitems="center">
                    <Checkbox name={`line-${index}`} checked onChange={() => {}} />
                  </TableCellNew>
                  <TableCellTextNew minWidth="220" value="Product name value" />
                  <TableCellTextNew
                    minWidth="270"
                    value="Notes long long name value"
                    flexgrow="1"
                  />
                  <TableCellTextNew minWidth="320" value="SKU list" />
                  <TableCellNew minWidth="240" alignitems="center" padding="8">
                    <TableSelect
                      name="businessDivision"
                      onChange={() => {}}
                      placeholder="Select from the list"
                      // isRowActive={index === 2}
                      // listMinWidth={300}
                      // disabled
                      // error
                    >
                      {({ onClick, state, selectElement }) => (
                        <SelectDropdown selectElement={selectElement}>
                          <SelectDropdownBody>
                            <SelectDropdownItemList>
                              {options.map(({ label, value }) => (
                                <OptionSingleNew
                                  key={value}
                                  label={label}
                                  selected={state?.value === value}
                                  onClick={() =>
                                    onClick({
                                      label,
                                      value,
                                    })
                                  }
                                />
                              ))}
                            </SelectDropdownItemList>
                          </SelectDropdownBody>
                        </SelectDropdown>
                      )}
                    </TableSelect>
                  </TableCellNew>
                  <TableCellNew minWidth="240" alignitems="center" padding="8">
                    <TableSelect
                      name="productType"
                      onChange={() => {}}
                      placeholder="Select from the list"
                      // isRowActive={index === 2}
                      // listMinWidth={300}
                      // disabled
                      error
                    >
                      {({ onClick, state, selectElement }) => (
                        <SelectDropdown selectElement={selectElement}>
                          <SelectDropdownBody>
                            <SelectDropdownItemList>
                              {options.map(({ label, value }) => (
                                <OptionSingleNew
                                  key={value}
                                  label={label}
                                  selected={state?.value === value}
                                  onClick={() =>
                                    onClick({
                                      label,
                                      value,
                                    })
                                  }
                                />
                              ))}
                            </SelectDropdownItemList>
                          </SelectDropdownBody>
                        </SelectDropdown>
                      )}
                    </TableSelect>
                  </TableCellNew>
                </TableRowNew>
              ))}
            </TableBodyNew>
          </TableNew>
        </Container>
      </Container>
    </Modal>
  );
};
