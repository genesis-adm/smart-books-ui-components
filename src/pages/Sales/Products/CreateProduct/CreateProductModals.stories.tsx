import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { Divider } from 'Components/base/Divider';
import { Radio } from 'Components/base/Radio';
import { TooltipIcon } from 'Components/base/TooltipIcon';
import { Button } from 'Components/base/buttons/Button';
import { CheckboxButton } from 'Components/base/buttons/CheckboxButton';
import { Container } from 'Components/base/grid/Container';
import { InputNew } from 'Components/base/inputs/InputNew';
import { InputSKU } from 'Components/base/inputs/InputSKU';
import { SelectNew } from 'Components/base/inputs/SelectNew';
import { OptionDoubleVerticalNew } from 'Components/base/inputs/SelectNew/options/OptionDoubleVerticalNew';
import { OptionSingleNew } from 'Components/base/inputs/SelectNew/options/OptionSingleNew';
import { TextareaNew } from 'Components/base/inputs/TextareaNew';
import { MenuTab, MenuTabWrapper } from 'Components/base/tabs/MenuTab';
import { Text } from 'Components/base/typography/Text';
import { ProductCodeCard } from 'Components/custom/Sales/Products/ProductCodeCard';
import { ProductCodeCategoryCard } from 'Components/custom/Sales/Products/ProductCodeCategoryCard';
import { ProductCodeCategoryDropdown } from 'Components/custom/Sales/Products/ProductCodeCategoryDropdown';
import { ProductCodeSubcategory } from 'Components/custom/Sales/Products/ProductCodeSubcategory';
import { ProductType } from 'Components/custom/Sales/Products/ProductType';
import { TaxInformationCard } from 'Components/custom/Sales/Products/TaxInformationCard';
import { UploadContainer } from 'Components/custom/Upload/UploadContainer';
import { UploadInfo } from 'Components/custom/Upload/UploadInfo';
import { UploadItemField } from 'Components/custom/Upload/UploadItemField';
import { Accordion } from 'Components/elements/Accordion';
import { AssignContainer } from 'Components/modules/Assign/AssignContainer';
import { AssignLine } from 'Components/modules/Assign/AssignLine';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { options } from 'Mocks/fakeOptions';
import { ReactComponent as ArrowLeftIcon } from 'Svg/v2/16/arrow-left.svg';
import { ReactComponent as QuestionAltIcon } from 'Svg/v2/16/question-transparent.svg';
import { ReactComponent as ITIcon } from 'Svg/v2/24/analysis-chart.svg';
import { ReactComponent as ProductsIcon } from 'Svg/v2/24/book-opened.svg';
import { ReactComponent as BuildingIcon } from 'Svg/v2/24/building.svg';
import { ReactComponent as ServicesIcon } from 'Svg/v2/24/chef-hat.svg';
import { ReactComponent as MiscellaneousIcon } from 'Svg/v2/24/clipboard.svg';
import { ReactComponent as ClothingIcon } from 'Svg/v2/24/clothes.svg';
import { ReactComponent as FreightIcon } from 'Svg/v2/24/delivery.svg';
import { ReactComponent as DigitalProductsIcon } from 'Svg/v2/24/devices.svg';
import { ReactComponent as ConstructionIcon } from 'Svg/v2/24/dish-plate-cover.svg';
import { ReactComponent as FoodIcon } from 'Svg/v2/24/fastfood.svg';
import { ReactComponent as HealthServicesIcon } from 'Svg/v2/24/heart-rate.svg';
import { ReactComponent as MedicalCareIcon } from 'Svg/v2/24/hospital.svg';
import { ReactComponent as RentalIcon } from 'Svg/v2/24/key.svg';
import { ReactComponent as FeesIcon } from 'Svg/v2/24/receipt-discount.svg';
import { ReactComponent as ProfessionalServicesIcon } from 'Svg/v2/24/suitcase-s.svg';
import { checkFileType } from 'Utils/fileHandlers';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';
import { Nullable } from '../../../../types/general';

export default {
  title: 'Pages/Sales/Products/Modals/Create Product',
};

export const CreateProductChooseProductType: React.FC = () => (
  <Modal
    size="720"
    background="grey-10"
    type="new"
    header={
      <ModalHeaderNew background="grey-10" title="Create new product" border onClose={() => {}} />
    }
  >
    <Container
      flexdirection="column"
      justifycontent="center"
      className={classNames(spacing.h100p, spacing.w100p)}
    >
      <Container
        flexdirection="column"
        alignitems="center"
        borderRadius="20"
        background="white-100"
        className={spacing.p56}
      >
        <Text type="title-semibold">Select product type</Text>
        <Container justifycontent="center" className={spacing.mt40}>
          <ProductType type="digitalgoods" onClick={() => {}} />
          <ProductType type="service" onClick={() => {}} />
          <ProductType type="bundle" onClick={() => {}} />
        </Container>
        <Container justifycontent="center" className={spacing.mt40}>
          <ProductType type="inventory" onClick={() => {}} />
          <ProductType type="noninventory" onClick={() => {}} />
        </Container>
      </Container>
    </Container>
  </Modal>
);

export const CreateNewServiceGeneralTab: React.FC = () => {
  const [image, setImage] = useState<File | null>(null);
  const handleImageUpload = (item: Nullable<File>) => {
    setImage(item);
  };
  const handleImageDelete = () => {
    setImage(null);
  };
  const handleImageDrop = (e: React.DragEvent<HTMLElement>) => {
    const dropImage = e.dataTransfer.files[0];
    if (checkFileType(dropImage, ['jpg', 'jpeg', 'png', 'webp'])) {
      setImage(dropImage);
    }
  };
  const [currentValue, setCurrentValue] = useState('');
  return (
    <Modal
      size="720"
      background="grey-10"
      type="new"
      header={
        <ModalHeaderNew background="grey-10" title="Create new Service" border onClose={() => {}} />
      }
      footer={
        <ModalFooterNew background="grey-10">
          <Button width="md" onClick={() => {}}>
            Create
          </Button>
          <Button
            width="sm"
            background="white-100"
            color="grey-100-black-100"
            border="grey-90"
            onClick={() => {}}
            iconLeft={<ArrowLeftIcon />}
            navigation
          >
            Back
          </Button>
        </ModalFooterNew>
      }
    >
      <MenuTabWrapper>
        <MenuTab id="general" active onClick={() => {}}>
          General
        </MenuTab>
        <MenuTab id="accounting" onClick={() => {}}>
          Accounting
        </MenuTab>
      </MenuTabWrapper>
      <Container
        flexdirection="column"
        alignitems="center"
        borderRadius="20"
        background="white-100"
        className={classNames(spacing.mt24, spacing.p24)}
      >
        <Container justifycontent="space-between" className={spacing.w100p}>
          <SelectNew
            name="businessdivision"
            onChange={() => {}}
            width="192"
            label="Business division"
            required
          >
            {({ onClick, state, selectElement }) => (
              <SelectDropdown selectElement={selectElement}>
                <SelectDropdownBody>
                  <SelectDropdownItemList>
                    {options.map(({ label, value }) => (
                      <OptionSingleNew
                        key={value}
                        label={label}
                        selected={state?.value === value}
                        onClick={() =>
                          onClick({
                            label,
                            value,
                          })
                        }
                      />
                    ))}
                  </SelectDropdownItemList>
                </SelectDropdownBody>
              </SelectDropdown>
            )}
          </SelectNew>
          <InputNew
            name="productname"
            label="Product name"
            width="192"
            required
            onChange={() => {}}
          />
          <SelectNew
            name="unitmeasure"
            onChange={() => {}}
            width="192"
            label="Unit measure"
            // required
          >
            {({ onClick, state, selectElement }) => (
              <SelectDropdown selectElement={selectElement}>
                <SelectDropdownBody>
                  <SelectDropdownItemList>
                    {options.map(({ label, value }) => (
                      <OptionSingleNew
                        key={value}
                        label={label}
                        selected={state?.value === value}
                        onClick={() =>
                          onClick({
                            label,
                            value,
                          })
                        }
                      />
                    ))}
                  </SelectDropdownItemList>
                </SelectDropdownBody>
              </SelectDropdown>
            )}
          </SelectNew>
        </Container>
        <InputSKU
          className={spacing.mt24}
          name="sku"
          width="full"
          value=""
          SKU={[]}
          onSKUAdd={() => {}}
          onClearAllSKU={() => {}}
        />
        <UploadContainer className={spacing.mt24} onDrop={handleImageDrop}>
          <UploadItemField
            name="uploader"
            files={image}
            acceptFilesType="image"
            onChange={handleImageUpload}
            onDelete={handleImageDelete}
            onReload={() => {}}
          />

          <UploadInfo
            mainText={image ? image.name : 'Drag and drop or Choose file'}
            secondaryText="All image formats; Maximum file size is 5MB"
          />
        </UploadContainer>
        <TextareaNew
          className={spacing.mt24}
          name="notes"
          label="Notes"
          value={currentValue}
          onChange={(e) => setCurrentValue(e.target.value)}
        />
      </Container>
    </Modal>
  );
};

export const CreateNewDigitalGoodsGeneralTab: React.FC = () => {
  const [image, setImage] = useState<File | null>(null);
  const handleImageUpload = (item: Nullable<File>) => {
    setImage(item);
  };
  const handleImageDelete = () => {
    setImage(null);
  };
  const handleImageDrop = (e: React.DragEvent<HTMLElement>) => {
    const dropImage = e.dataTransfer.files[0];
    if (checkFileType(dropImage, ['jpg', 'jpeg', 'png', 'webp'])) {
      setImage(dropImage);
    }
  };
  const [currentValue, setCurrentValue] = useState('');
  return (
    <Modal
      size="720"
      background="grey-10"
      type="new"
      header={
        <ModalHeaderNew background="grey-10" title="Create new Service" border onClose={() => {}} />
      }
      footer={
        <ModalFooterNew background="grey-10">
          <Button width="md" onClick={() => {}}>
            Create
          </Button>
          <Button
            width="sm"
            background="white-100"
            color="grey-100-black-100"
            border="grey-90"
            onClick={() => {}}
            iconLeft={<ArrowLeftIcon />}
            navigation
          >
            Back
          </Button>
        </ModalFooterNew>
      }
    >
      <MenuTabWrapper>
        <MenuTab id="general" active onClick={() => {}}>
          General
        </MenuTab>
        <MenuTab id="accounting" onClick={() => {}}>
          Accounting
        </MenuTab>
      </MenuTabWrapper>
      <Container
        flexdirection="column"
        alignitems="center"
        borderRadius="20"
        background="white-100"
        className={classNames(spacing.mt24, spacing.p24)}
      >
        <Container justifycontent="space-between" className={spacing.w100p}>
          <SelectNew
            name="businessdivision"
            onChange={() => {}}
            width="300"
            label="Business division"
            required
          >
            {({ onClick, state, selectElement }) => (
              <SelectDropdown selectElement={selectElement}>
                <SelectDropdownBody>
                  <SelectDropdownItemList>
                    {options.map(({ label, value }) => (
                      <OptionSingleNew
                        key={value}
                        label={label}
                        selected={state?.value === value}
                        onClick={() =>
                          onClick({
                            label,
                            value,
                          })
                        }
                      />
                    ))}
                  </SelectDropdownItemList>
                </SelectDropdownBody>
              </SelectDropdown>
            )}
          </SelectNew>
          <InputNew
            name="productname"
            label="Product name"
            width="300"
            required
            onChange={() => {}}
          />
        </Container>
        <InputSKU
          className={spacing.mt24}
          name="sku"
          width="full"
          value=""
          SKU={[]}
          onSKUAdd={() => {}}
          onClearAllSKU={() => {}}
        />
        <UploadContainer className={spacing.mt24} onDrop={handleImageDrop}>
          <UploadItemField
            name="uploader"
            files={image}
            acceptFilesType="image"
            onChange={handleImageUpload}
            onDelete={handleImageDelete}
            onReload={() => {}}
          />
          <UploadInfo
            mainText={image ? image.name : 'Drag and drop or Choose file'}
            secondaryText="All image formats; Maximum file size is 5MB"
          />
        </UploadContainer>
        <TextareaNew
          className={spacing.mt24}
          name="notes"
          label="Notes"
          value={currentValue}
          onChange={(e) => setCurrentValue(e.target.value)}
        />
      </Container>
    </Modal>
  );
};

export const CreateNewServiceAccountingTab: React.FC = () => (
  <Modal
    size="720"
    background="grey-10"
    type="new"
    header={
      <ModalHeaderNew background="grey-10" title="Create new Service" border onClose={() => {}} />
    }
    footer={
      <ModalFooterNew background="grey-10">
        <Button width="md" onClick={() => {}}>
          Create
        </Button>
        <Button
          width="sm"
          background="white-100"
          color="grey-100-black-100"
          border="grey-90"
          onClick={() => {}}
          iconLeft={<ArrowLeftIcon />}
          navigation
        >
          Back
        </Button>
      </ModalFooterNew>
    }
  >
    <MenuTabWrapper>
      <MenuTab id="general" onClick={() => {}}>
        General
      </MenuTab>
      <MenuTab id="accounting" active onClick={() => {}}>
        Accounting
      </MenuTab>
    </MenuTabWrapper>
    <Container
      flexdirection="column"
      borderRadius="20"
      background="white-100"
      className={classNames(spacing.mt24, spacing.p24)}
    >
      <Text type="body-medium">Tax information</Text>
      <Button
        className={spacing.mt16}
        width="full"
        background="grey-10-blue-10"
        color="violet-90-violet-100"
        border="violet-90-violet-100"
        dashed="always"
        onClick={() => {}}
      >
        Add product code
      </Button>
      <TaxInformationCard
        className={spacing.mt16}
        taxCode="PC040405"
        taxCodeDescription="Clothing and related products (business-to-customer)-gym suits"
        category="Administrative and support services"
        subcategory="Accessories and supplies"
        onEdit={() => {}}
        onDelete={() => {}}
      />
      <Text type="caption-regular" className={spacing.mt24}>
        Tax type
      </Text>
      <Container className={spacing.mt12}>
        <Radio id="inclusive" onChange={() => {}} name="taxType" checked>
          Inclusive
        </Radio>
        <Radio
          className={spacing.ml16}
          id="exclusive"
          onChange={() => {}}
          name="taxType"
          checked={false}
        >
          Exclusive
        </Radio>
      </Container>
      {/* <SelectNew
        className={spacing.mt24}
        name="taxaccount"
        onChange={() => {}}
        width="full"
        label="Choose tax account"
      >
        {({ onClick, state }) => (
          <>
            {options.map(({ label, value }) => (
              <OptionSingleNew
                key={value}
                label={label}
                selected={state?.value === value}
                onClick={() => onClick({ label, value })}
              />
            ))}
          </>
        )}
      </SelectNew> */}
      {/* <Container className={spacing.mt16} justifycontent="space-between">
        <Container alignitems="center">
          <Text type="caption-regular" className={spacing.mr8}>
            Selected 2 out of 20 Legal entities
          </Text>
          <TooltipIcon
            icon={<QuestionAltIcon />}
            size="16"
            color="grey-100-black-100"
            background="transparent"
            message="Tooltip text"
          />
        </Container>
        <Button background="transparent-blue-10" color="violet-90" height="32" onClick={() => {}}>
          Show all (15)
        </Button>
      </Container> */}
      {/* <Container gap="12" className={spacing.mt8} flexwrap="wrap">
        <CheckboxButton
          name="le1"
          label="Legal entity 1"
          font="caption-semibold"
          checked
          onChange={() => {}}
        />
        <CheckboxButton
          name="le2"
          label="Legal entity 2"
          font="caption-semibold"
          checked={false}
          onChange={() => {}}
        />
      </Container> */}
      <Container
        flexdirection="column"
        borderRadius="20"
        background="white-100"
        className={classNames(spacing.h360, spacing.mt16)}
      >
        <AssignContainer
          titleSource="Legal entity / Business unit"
          titleTarget="Choose account"
          width="660"
        >
          <AssignLine
            order="1"
            sourceValue="BetterMe health & fit"
            sourceSecondaryValue="BetterMe"
            isAssigned={false}
          >
            <SelectNew
              onChange={() => {}}
              // defaultValue={{ label: '', value: '' }}
              label="Assign Product"
              placeholder="Assign Product"
              name="product"
              width="full"
            >
              {({ onClick, state, selectElement }) => (
                <SelectDropdown selectElement={selectElement}>
                  <SelectDropdownBody>
                    <SelectDropdownItemList>
                      {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                        <OptionDoubleVerticalNew
                          key={value}
                          label={label}
                          secondaryLabel={secondaryLabel}
                          selected={state?.value === value}
                          onClick={() =>
                            onClick({
                              label,
                              value,
                            })
                          }
                        />
                      ))}
                    </SelectDropdownItemList>
                  </SelectDropdownBody>
                </SelectDropdown>
              )}
            </SelectNew>
          </AssignLine>
          <AssignLine
            order="1"
            sourceValue="BetterMe health & fit"
            sourceSecondaryValue="BetterMe"
            isAssigned={false}
          >
            <SelectNew
              onChange={() => {}}
              // defaultValue={{ label: '', value: '' }}
              label="Assign Product"
              placeholder="Assign Product"
              name="product"
              width="full"
            >
              {({ onClick, state, selectElement }) => (
                <SelectDropdown selectElement={selectElement}>
                  <SelectDropdownBody>
                    <SelectDropdownItemList>
                      {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                        <OptionDoubleVerticalNew
                          key={value}
                          label={label}
                          secondaryLabel={secondaryLabel}
                          selected={state?.value === value}
                          onClick={() =>
                            onClick({
                              label,
                              value,
                            })
                          }
                        />
                      ))}
                    </SelectDropdownItemList>
                  </SelectDropdownBody>
                </SelectDropdown>
              )}
            </SelectNew>
          </AssignLine>
          <AssignLine
            order="1"
            sourceValue="BetterMe health & fit"
            sourceSecondaryValue="BetterMe"
            isAssigned={false}
          >
            <SelectNew
              onChange={() => {}}
              // defaultValue={{ label: '', value: '' }}
              label="Assign Product"
              placeholder="Assign Product"
              name="product"
              width="full"
            >
              {({ onClick, state, selectElement }) => (
                <SelectDropdown selectElement={selectElement}>
                  <SelectDropdownBody>
                    <SelectDropdownItemList>
                      {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                        <OptionDoubleVerticalNew
                          key={value}
                          label={label}
                          secondaryLabel={secondaryLabel}
                          selected={state?.value === value}
                          onClick={() =>
                            onClick({
                              label,
                              value,
                            })
                          }
                        />
                      ))}
                    </SelectDropdownItemList>
                  </SelectDropdownBody>
                </SelectDropdown>
              )}
            </SelectNew>
          </AssignLine>
          <Container flexdirection="row-reverse">
            <Button
              background="transparent-blue-10"
              color="violet-90"
              height="32"
              onClick={() => {}}
            >
              Show all (15)
            </Button>
          </Container>
        </AssignContainer>
      </Container>
    </Container>
    <Accordion className={spacing.mt24} title="Choose income account" onClick={() => {}}>
      <Container
        flexdirection="column"
        borderRadius="20"
        background="white-100"
        className={classNames(spacing.p24, spacing.mt16)}
      >
        <Container
          flexdirection="column"
          borderRadius="20"
          background="white-100"
          className={spacing.h360}
        >
          <AssignContainer
            titleSource="Legal entity / Business unit"
            titleTarget="Choose account"
            width="660"
          >
            <AssignLine
              order="1"
              sourceValue="BetterMe health & fit"
              sourceSecondaryValue="BetterMe"
              isAssigned={false}
            >
              <SelectNew
                onChange={() => {}}
                // defaultValue={{ label: '', value: '' }}
                label="Assign Product"
                placeholder="Assign Product"
                name="product"
                width="full"
              >
                {({ onClick, state, selectElement }) => (
                  <SelectDropdown selectElement={selectElement}>
                    <SelectDropdownBody>
                      <SelectDropdownItemList>
                        {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                          <OptionDoubleVerticalNew
                            key={value}
                            label={label}
                            secondaryLabel={secondaryLabel}
                            selected={state?.value === value}
                            onClick={() =>
                              onClick({
                                label,
                                value,
                              })
                            }
                          />
                        ))}
                      </SelectDropdownItemList>
                    </SelectDropdownBody>
                  </SelectDropdown>
                )}
              </SelectNew>
            </AssignLine>
            <AssignLine
              order="1"
              sourceValue="BetterMe health & fit"
              sourceSecondaryValue="BetterMe"
              isAssigned={false}
            >
              <SelectNew
                onChange={() => {}}
                // defaultValue={{ label: '', value: '' }}
                label="Assign Product"
                placeholder="Assign Product"
                name="product"
                width="full"
              >
                {({ onClick, state, selectElement }) => (
                  <SelectDropdown selectElement={selectElement}>
                    <SelectDropdownBody>
                      <SelectDropdownItemList>
                        {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                          <OptionDoubleVerticalNew
                            key={value}
                            label={label}
                            secondaryLabel={secondaryLabel}
                            selected={state?.value === value}
                            onClick={() =>
                              onClick({
                                label,
                                value,
                              })
                            }
                          />
                        ))}
                      </SelectDropdownItemList>
                    </SelectDropdownBody>
                  </SelectDropdown>
                )}
              </SelectNew>
            </AssignLine>
            <AssignLine
              order="1"
              sourceValue="BetterMe health & fit"
              sourceSecondaryValue="BetterMe"
              isAssigned={false}
            >
              <SelectNew
                onChange={() => {}}
                // defaultValue={{ label: '', value: '' }}
                label="Assign Product"
                placeholder="Assign Product"
                name="product"
                width="full"
              >
                {({ onClick, state, selectElement }) => (
                  <SelectDropdown selectElement={selectElement}>
                    <SelectDropdownBody>
                      <SelectDropdownItemList>
                        {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                          <OptionDoubleVerticalNew
                            key={value}
                            label={label}
                            secondaryLabel={secondaryLabel}
                            selected={state?.value === value}
                            onClick={() =>
                              onClick({
                                label,
                                value,
                              })
                            }
                          />
                        ))}
                      </SelectDropdownItemList>
                    </SelectDropdownBody>
                  </SelectDropdown>
                )}
              </SelectNew>
            </AssignLine>
            <AssignLine
              order="1"
              sourceValue="BetterMe health & fit"
              sourceSecondaryValue="BetterMe"
              isAssigned={false}
            >
              <SelectNew
                onChange={() => {}}
                // defaultValue={{ label: '', value: '' }}
                label="Assign Product"
                placeholder="Assign Product"
                name="product"
                width="full"
              >
                {({ onClick, state, selectElement }) => (
                  <SelectDropdown selectElement={selectElement}>
                    <SelectDropdownBody>
                      <SelectDropdownItemList>
                        {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                          <OptionDoubleVerticalNew
                            key={value}
                            label={label}
                            secondaryLabel={secondaryLabel}
                            selected={state?.value === value}
                            onClick={() =>
                              onClick({
                                label,
                                value,
                              })
                            }
                          />
                        ))}
                      </SelectDropdownItemList>
                    </SelectDropdownBody>
                  </SelectDropdown>
                )}
              </SelectNew>
            </AssignLine>
            <AssignLine
              order="1"
              sourceValue="BetterMe health & fit"
              sourceSecondaryValue="BetterMe"
              isAssigned={false}
            >
              <SelectNew
                onChange={() => {}}
                // defaultValue={{ label: '', value: '' }}
                label="Assign Product"
                placeholder="Assign Product"
                name="product"
                width="full"
              >
                {({ onClick, state, selectElement }) => (
                  <SelectDropdown selectElement={selectElement}>
                    <SelectDropdownBody>
                      <SelectDropdownItemList>
                        {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                          <OptionDoubleVerticalNew
                            key={value}
                            label={label}
                            secondaryLabel={secondaryLabel}
                            selected={state?.value === value}
                            onClick={() =>
                              onClick({
                                label,
                                value,
                              })
                            }
                          />
                        ))}
                      </SelectDropdownItemList>
                    </SelectDropdownBody>
                  </SelectDropdown>
                )}
              </SelectNew>
            </AssignLine>
            <AssignLine
              order="1"
              sourceValue="BetterMe health & fit"
              sourceSecondaryValue="BetterMe"
              isAssigned={false}
            >
              <SelectNew
                onChange={() => {}}
                // defaultValue={{ label: '', value: '' }}
                label="Assign Product"
                placeholder="Assign Product"
                name="product"
                width="full"
              >
                {({ onClick, state, selectElement }) => (
                  <SelectDropdown selectElement={selectElement}>
                    <SelectDropdownBody>
                      <SelectDropdownItemList>
                        {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                          <OptionDoubleVerticalNew
                            key={value}
                            label={label}
                            secondaryLabel={secondaryLabel}
                            selected={state?.value === value}
                            onClick={() =>
                              onClick({
                                label,
                                value,
                              })
                            }
                          />
                        ))}
                      </SelectDropdownItemList>
                    </SelectDropdownBody>
                  </SelectDropdown>
                )}
              </SelectNew>
            </AssignLine>
          </AssignContainer>
        </Container>
      </Container>
    </Accordion>
    <Accordion className={spacing.mt24} title="Choose discount account" onClick={() => {}}>
      <Container
        flexdirection="column"
        borderRadius="20"
        background="white-100"
        className={classNames(spacing.p24, spacing.mt16)}
      >
        <Container
          flexdirection="column"
          borderRadius="20"
          background="white-100"
          className={spacing.h360}
        >
          <AssignContainer
            titleSource="Legal entity / Business unit"
            titleTarget="Choose account"
            width="660"
          >
            <AssignLine
              order="1"
              sourceValue="BetterMe health & fit"
              sourceSecondaryValue="BetterMe"
              isAssigned={false}
            >
              <SelectNew
                onChange={() => {}}
                // defaultValue={{ label: '', value: '' }}
                label="Assign Product"
                placeholder="Assign Product"
                name="product"
                width="full"
              >
                {({ onClick, state, selectElement }) => (
                  <SelectDropdown selectElement={selectElement}>
                    <SelectDropdownBody>
                      <SelectDropdownItemList>
                        {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                          <OptionDoubleVerticalNew
                            key={value}
                            label={label}
                            secondaryLabel={secondaryLabel}
                            selected={state?.value === value}
                            onClick={() =>
                              onClick({
                                label,
                                value,
                              })
                            }
                          />
                        ))}
                      </SelectDropdownItemList>
                    </SelectDropdownBody>
                  </SelectDropdown>
                )}
              </SelectNew>
            </AssignLine>
            <AssignLine
              order="1"
              sourceValue="BetterMe health & fit"
              sourceSecondaryValue="BetterMe"
              isAssigned={false}
            >
              <SelectNew
                onChange={() => {}}
                // defaultValue={{ label: '', value: '' }}
                label="Assign Product"
                placeholder="Assign Product"
                name="product"
                width="full"
              >
                {({ onClick, state, selectElement }) => (
                  <SelectDropdown selectElement={selectElement}>
                    <SelectDropdownBody>
                      <SelectDropdownItemList>
                        {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                          <OptionDoubleVerticalNew
                            key={value}
                            label={label}
                            secondaryLabel={secondaryLabel}
                            selected={state?.value === value}
                            onClick={() =>
                              onClick({
                                label,
                                value,
                              })
                            }
                          />
                        ))}
                      </SelectDropdownItemList>
                    </SelectDropdownBody>
                  </SelectDropdown>
                )}
              </SelectNew>
            </AssignLine>
            <AssignLine
              order="1"
              sourceValue="BetterMe health & fit"
              sourceSecondaryValue="BetterMe"
              isAssigned={false}
            >
              <SelectNew
                onChange={() => {}}
                // defaultValue={{ label: '', value: '' }}
                label="Assign Product"
                placeholder="Assign Product"
                name="product"
                width="full"
              >
                {({ onClick, state, selectElement }) => (
                  <SelectDropdown selectElement={selectElement}>
                    <SelectDropdownBody>
                      <SelectDropdownItemList>
                        {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                          <OptionDoubleVerticalNew
                            key={value}
                            label={label}
                            secondaryLabel={secondaryLabel}
                            selected={state?.value === value}
                            onClick={() =>
                              onClick({
                                label,
                                value,
                              })
                            }
                          />
                        ))}
                      </SelectDropdownItemList>
                    </SelectDropdownBody>
                  </SelectDropdown>
                )}
              </SelectNew>
            </AssignLine>
            <AssignLine
              order="1"
              sourceValue="BetterMe health & fit"
              sourceSecondaryValue="BetterMe"
              isAssigned={false}
            >
              <SelectNew
                onChange={() => {}}
                // defaultValue={{ label: '', value: '' }}
                label="Assign Product"
                placeholder="Assign Product"
                name="product"
                width="full"
              >
                {({ onClick, state, selectElement }) => (
                  <SelectDropdown selectElement={selectElement}>
                    <SelectDropdownBody>
                      <SelectDropdownItemList>
                        {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                          <OptionDoubleVerticalNew
                            key={value}
                            label={label}
                            secondaryLabel={secondaryLabel}
                            selected={state?.value === value}
                            onClick={() =>
                              onClick({
                                label,
                                value,
                              })
                            }
                          />
                        ))}
                      </SelectDropdownItemList>
                    </SelectDropdownBody>
                  </SelectDropdown>
                )}
              </SelectNew>
            </AssignLine>
            <AssignLine
              order="1"
              sourceValue="BetterMe health & fit"
              sourceSecondaryValue="BetterMe"
              isAssigned={false}
            >
              <SelectNew
                onChange={() => {}}
                // defaultValue={{ label: '', value: '' }}
                label="Assign Product"
                placeholder="Assign Product"
                name="product"
                width="full"
              >
                {({ onClick, state, selectElement }) => (
                  <SelectDropdown selectElement={selectElement}>
                    <SelectDropdownBody>
                      <SelectDropdownItemList>
                        {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                          <OptionDoubleVerticalNew
                            key={value}
                            label={label}
                            secondaryLabel={secondaryLabel}
                            selected={state?.value === value}
                            onClick={() =>
                              onClick({
                                label,
                                value,
                              })
                            }
                          />
                        ))}
                      </SelectDropdownItemList>
                    </SelectDropdownBody>
                  </SelectDropdown>
                )}
              </SelectNew>
            </AssignLine>
            <AssignLine
              order="1"
              sourceValue="BetterMe health & fit"
              sourceSecondaryValue="BetterMe"
              isAssigned={false}
            >
              <SelectNew
                onChange={() => {}}
                // defaultValue={{ label: '', value: '' }}
                label="Assign Product"
                placeholder="Assign Product"
                name="product"
                width="full"
              >
                {({ onClick, state, selectElement }) => (
                  <SelectDropdown selectElement={selectElement}>
                    <SelectDropdownBody>
                      <SelectDropdownItemList>
                        {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                          <OptionDoubleVerticalNew
                            key={value}
                            label={label}
                            secondaryLabel={secondaryLabel}
                            selected={state?.value === value}
                            onClick={() =>
                              onClick({
                                label,
                                value,
                              })
                            }
                          />
                        ))}
                      </SelectDropdownItemList>
                    </SelectDropdownBody>
                  </SelectDropdown>
                )}
              </SelectNew>
            </AssignLine>
          </AssignContainer>
        </Container>
      </Container>
    </Accordion>
    <Accordion className={spacing.mt24} title="Choose fee account" onClick={() => {}}>
      <Container
        flexdirection="column"
        borderRadius="20"
        background="white-100"
        className={classNames(spacing.p24, spacing.mt16)}
      >
        <Container
          flexdirection="column"
          borderRadius="20"
          background="white-100"
          className={spacing.h360}
        >
          <AssignContainer
            titleSource="Legal entity / Business unit"
            titleTarget="Choose account"
            width="660"
          >
            <AssignLine
              order="1"
              sourceValue="BetterMe health & fit"
              sourceSecondaryValue="BetterMe"
              isAssigned={false}
            >
              <SelectNew
                onChange={() => {}}
                // defaultValue={{ label: '', value: '' }}
                label="Assign Product"
                placeholder="Assign Product"
                name="product"
                width="full"
              >
                {({ onClick, state, selectElement }) => (
                  <SelectDropdown selectElement={selectElement}>
                    <SelectDropdownBody>
                      <SelectDropdownItemList>
                        {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                          <OptionDoubleVerticalNew
                            key={value}
                            label={label}
                            secondaryLabel={secondaryLabel}
                            selected={state?.value === value}
                            onClick={() =>
                              onClick({
                                label,
                                value,
                              })
                            }
                          />
                        ))}
                      </SelectDropdownItemList>
                    </SelectDropdownBody>
                  </SelectDropdown>
                )}
              </SelectNew>
            </AssignLine>
            <AssignLine
              order="1"
              sourceValue="BetterMe health & fit"
              sourceSecondaryValue="BetterMe"
              isAssigned={false}
            >
              <SelectNew
                onChange={() => {}}
                // defaultValue={{ label: '', value: '' }}
                label="Assign Product"
                placeholder="Assign Product"
                name="product"
                width="full"
              >
                {({ onClick, state, selectElement }) => (
                  <SelectDropdown selectElement={selectElement}>
                    <SelectDropdownBody>
                      <SelectDropdownItemList>
                        {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                          <OptionDoubleVerticalNew
                            key={value}
                            label={label}
                            secondaryLabel={secondaryLabel}
                            selected={state?.value === value}
                            onClick={() =>
                              onClick({
                                label,
                                value,
                              })
                            }
                          />
                        ))}
                      </SelectDropdownItemList>
                    </SelectDropdownBody>
                  </SelectDropdown>
                )}
              </SelectNew>
            </AssignLine>
            <AssignLine
              order="1"
              sourceValue="BetterMe health & fit"
              sourceSecondaryValue="BetterMe"
              isAssigned={false}
            >
              <SelectNew
                onChange={() => {}}
                // defaultValue={{ label: '', value: '' }}
                label="Assign Product"
                placeholder="Assign Product"
                name="product"
                width="full"
              >
                {({ onClick, state, selectElement }) => (
                  <SelectDropdown selectElement={selectElement}>
                    <SelectDropdownBody>
                      <SelectDropdownItemList>
                        {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                          <OptionDoubleVerticalNew
                            key={value}
                            label={label}
                            secondaryLabel={secondaryLabel}
                            selected={state?.value === value}
                            onClick={() =>
                              onClick({
                                label,
                                value,
                              })
                            }
                          />
                        ))}
                      </SelectDropdownItemList>
                    </SelectDropdownBody>
                  </SelectDropdown>
                )}
              </SelectNew>
            </AssignLine>
            <AssignLine
              order="1"
              sourceValue="BetterMe health & fit"
              sourceSecondaryValue="BetterMe"
              isAssigned={false}
            >
              <SelectNew
                onChange={() => {}}
                // defaultValue={{ label: '', value: '' }}
                label="Assign Product"
                placeholder="Assign Product"
                name="product"
                width="full"
              >
                {({ onClick, state, selectElement }) => (
                  <SelectDropdown selectElement={selectElement}>
                    <SelectDropdownBody>
                      <SelectDropdownItemList>
                        {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                          <OptionDoubleVerticalNew
                            key={value}
                            label={label}
                            secondaryLabel={secondaryLabel}
                            selected={state?.value === value}
                            onClick={() =>
                              onClick({
                                label,
                                value,
                              })
                            }
                          />
                        ))}
                      </SelectDropdownItemList>
                    </SelectDropdownBody>
                  </SelectDropdown>
                )}
              </SelectNew>
            </AssignLine>
            <AssignLine
              order="1"
              sourceValue="BetterMe health & fit"
              sourceSecondaryValue="BetterMe"
              isAssigned={false}
            >
              <SelectNew
                onChange={() => {}}
                // defaultValue={{ label: '', value: '' }}
                label="Assign Product"
                placeholder="Assign Product"
                name="product"
                width="full"
              >
                {({ onClick, state, selectElement }) => (
                  <SelectDropdown selectElement={selectElement}>
                    <SelectDropdownBody>
                      <SelectDropdownItemList>
                        {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                          <OptionDoubleVerticalNew
                            key={value}
                            label={label}
                            secondaryLabel={secondaryLabel}
                            selected={state?.value === value}
                            onClick={() =>
                              onClick({
                                label,
                                value,
                              })
                            }
                          />
                        ))}
                      </SelectDropdownItemList>
                    </SelectDropdownBody>
                  </SelectDropdown>
                )}
              </SelectNew>
            </AssignLine>
            <AssignLine
              order="1"
              sourceValue="BetterMe health & fit"
              sourceSecondaryValue="BetterMe"
              isAssigned={false}
            >
              <SelectNew
                onChange={() => {}}
                // defaultValue={{ label: '', value: '' }}
                label="Assign Product"
                placeholder="Assign Product"
                name="product"
                width="full"
              >
                {({ onClick, state, selectElement }) => (
                  <SelectDropdown selectElement={selectElement}>
                    <SelectDropdownBody>
                      <SelectDropdownItemList>
                        {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                          <OptionDoubleVerticalNew
                            key={value}
                            label={label}
                            secondaryLabel={secondaryLabel}
                            selected={state?.value === value}
                            onClick={() =>
                              onClick({
                                label,
                                value,
                              })
                            }
                          />
                        ))}
                      </SelectDropdownItemList>
                    </SelectDropdownBody>
                  </SelectDropdown>
                )}
              </SelectNew>
            </AssignLine>
          </AssignContainer>
        </Container>
      </Container>
    </Accordion>
  </Modal>
);

export const AddProductCodeChooseCategory: React.FC = () => (
  <Modal
    size="720"
    background="grey-10"
    type="new"
    header={
      <ModalHeaderNew
        background="grey-10"
        action="back"
        title="Add product code"
        border
        onClose={() => {}}
      />
    }
    footer={
      <ModalFooterNew background="grey-10">
        <Button width="md" onClick={() => {}}>
          Add product code
        </Button>
      </ModalFooterNew>
    }
  >
    <Container
      flexdirection="column"
      borderRadius="20"
      background="white-100"
      className={classNames(spacing.pY24, spacing.pX16)}
    >
      <Text type="body-medium">Choose category</Text>
      <Container flexwrap="wrap" gap="16" className={spacing.mt16}>
        <ProductCodeCategoryCard
          title="Administrative and support services"
          icon={<BuildingIcon />}
          onClick={() => {}}
        />
        <ProductCodeCategoryCard title="Clothing" icon={<ClothingIcon />} onClick={() => {}} />
        <ProductCodeCategoryCard
          title="Construction and real property"
          icon={<ConstructionIcon />}
          onClick={() => {}}
        />
        <ProductCodeCategoryCard
          title="Digital products"
          icon={<DigitalProductsIcon />}
          onClick={() => {}}
        />
        <ProductCodeCategoryCard
          title="Fees, coupons, dues and charges"
          icon={<FeesIcon />}
          onClick={() => {}}
        />
        <ProductCodeCategoryCard title="Food and beverage" icon={<FoodIcon />} onClick={() => {}} />
        <ProductCodeCategoryCard title="Freight" icon={<FreightIcon />} onClick={() => {}} />
        <ProductCodeCategoryCard
          title="Health and wellness"
          icon={<HealthServicesIcon />}
          onClick={() => {}}
        />
        <ProductCodeCategoryCard
          title="Information technology"
          icon={<ITIcon />}
          onClick={() => {}}
        />
        <ProductCodeCategoryCard
          title="Medical care"
          icon={<MedicalCareIcon />}
          onClick={() => {}}
        />
        <ProductCodeCategoryCard
          title="Miscellaneous services"
          icon={<MiscellaneousIcon />}
          onClick={() => {}}
        />
        <ProductCodeCategoryCard title="Products" icon={<ProductsIcon />} onClick={() => {}} />
        <ProductCodeCategoryCard
          title="Professional services"
          icon={<ProfessionalServicesIcon />}
          onClick={() => {}}
        />
        <ProductCodeCategoryCard
          title="Rental and leasing"
          icon={<RentalIcon />}
          onClick={() => {}}
        />
        <ProductCodeCategoryCard
          title="Services and supplies by industry"
          icon={<ServicesIcon />}
          onClick={() => {}}
        />
      </Container>
    </Container>
  </Modal>
);

export const AddProductCodeChooseProductCode: React.FC = () => (
  <Modal
    size="720"
    background="grey-10"
    type="new"
    header={
      <ModalHeaderNew
        background="grey-10"
        action="back"
        title="Add product code"
        border
        onClose={() => {}}
      />
    }
    footer={
      <ModalFooterNew background="grey-10">
        <Button width="md" onClick={() => {}}>
          Add product code
        </Button>
      </ModalFooterNew>
    }
  >
    <Container
      flexdirection="column"
      borderRadius="20"
      background="white-100"
      className={classNames(spacing.pY24, spacing.pX16)}
    >
      <Text color="grey-100">Choose category</Text>
      <ProductCodeCategoryDropdown
        className={spacing.mt12}
        categoryIcon={<ITIcon />}
        categoryName="Information technology"
      >
        <ProductCodeCategoryCard
          title="Administrative and support services"
          icon={<BuildingIcon />}
          onClick={() => {}}
        />
        <ProductCodeCategoryCard title="Clothing" icon={<ClothingIcon />} onClick={() => {}} />
        <ProductCodeCategoryCard
          title="Construction and real property"
          icon={<ConstructionIcon />}
          onClick={() => {}}
        />
        <ProductCodeCategoryCard
          title="Digital products"
          icon={<DigitalProductsIcon />}
          onClick={() => {}}
        />
        <ProductCodeCategoryCard
          title="Fees, coupons, dues and charges"
          icon={<FeesIcon />}
          onClick={() => {}}
        />
        <ProductCodeCategoryCard title="Food and beverage" icon={<FoodIcon />} onClick={() => {}} />
        <ProductCodeCategoryCard title="Freight" icon={<FreightIcon />} onClick={() => {}} />
        <ProductCodeCategoryCard
          title="Health and wellness"
          icon={<HealthServicesIcon />}
          onClick={() => {}}
        />
        <ProductCodeCategoryCard
          title="Information technology"
          icon={<ITIcon />}
          onClick={() => {}}
        />
        <ProductCodeCategoryCard
          title="Medical care"
          icon={<MedicalCareIcon />}
          onClick={() => {}}
        />
        <ProductCodeCategoryCard
          title="Miscellaneous services"
          icon={<MiscellaneousIcon />}
          onClick={() => {}}
        />
        <ProductCodeCategoryCard title="Products" icon={<ProductsIcon />} onClick={() => {}} />
        <ProductCodeCategoryCard
          title="Professional services"
          icon={<ProfessionalServicesIcon />}
          onClick={() => {}}
        />
        <ProductCodeCategoryCard
          title="Rental and leasing"
          icon={<RentalIcon />}
          onClick={() => {}}
        />
        <ProductCodeCategoryCard
          title="Services and supplies by industry"
          icon={<ServicesIcon />}
          onClick={() => {}}
        />
      </ProductCodeCategoryDropdown>
      <Text className={spacing.mt16} color="grey-100">
        Choose subcategory
      </Text>
      <Container className={spacing.mt16} flexwrap="wrap" gap="16">
        <ProductCodeSubcategory
          id="id1"
          name="subcategories"
          label="Miscellaneous"
          checked={false}
          disabled={false}
          onChange={() => {}}
        />
        <ProductCodeSubcategory
          id="id2"
          name="subcategories"
          label="Fuels"
          checked={false}
          disabled={false}
          onChange={() => {}}
        />
        <ProductCodeSubcategory
          id="id3"
          name="subcategories"
          label="Firearms and accessories"
          checked={false}
          disabled={false}
          onChange={() => {}}
        />
        <ProductCodeSubcategory
          id="id4"
          name="subcategories"
          label="Household goods & essentials"
          checked={false}
          disabled={false}
          onChange={() => {}}
        />
        <ProductCodeSubcategory
          id="id5"
          name="subcategories"
          label="Gift and prepaid download cards"
          checked={false}
          disabled={false}
          onChange={() => {}}
        />
        <ProductCodeSubcategory
          id="id6"
          name="subcategories"
          label="Collectible coins & precious metals"
          checked={false}
          disabled={false}
          onChange={() => {}}
        />
        <ProductCodeSubcategory
          id="id7"
          name="subcategories"
          label="Personal electronics"
          checked={false}
          disabled={false}
          onChange={() => {}}
        />
        <ProductCodeSubcategory
          id="id8"
          name="subcategories"
          label="Emergency preparedness"
          checked={false}
          disabled={false}
          onChange={() => {}}
        />
        <ProductCodeSubcategory
          id="id9"
          name="subcategories"
          label="Books, magazines, and newspapers"
          checked={false}
          disabled={false}
          onChange={() => {}}
        />
        <ProductCodeSubcategory
          id="id10"
          name="subcategories"
          label="Batteries"
          checked={false}
          disabled={false}
          onChange={() => {}}
        />
        <ProductCodeSubcategory
          id="id11"
          name="subcategories"
          label="School supplies (B2C)"
          checked={false}
          disabled={false}
          onChange={() => {}}
        />
        <ProductCodeSubcategory
          id="id12"
          name="subcategories"
          label="School supplies (B2B)"
          checked={false}
          disabled={false}
          onChange={() => {}}
        />
        <ProductCodeSubcategory
          id="id13"
          name="subcategories"
          label="Infant and baby supplies"
          checked={false}
          disabled={false}
          onChange={() => {}}
        />
        <ProductCodeSubcategory
          id="id14"
          name="subcategories"
          label="Outdoor gear and supplies"
          checked={false}
          disabled={false}
          onChange={() => {}}
        />
        <ProductCodeSubcategory
          id="id15"
          name="subcategories"
          label="CBD products from industrial hemp"
          checked={false}
          disabled={false}
          onChange={() => {}}
        />
        <ProductCodeSubcategory
          id="id16"
          name="subcategories"
          label="Tobacco and vape"
          checked={false}
          disabled={false}
          onChange={() => {}}
        />
      </Container>
      <Divider className={spacing.mt16} fullHorizontalWidth />
      <Text className={spacing.mt16} color="grey-100">
        Showing results in Clothing
      </Text>
      <Container flexwrap="wrap" gap="16" className={spacing.mt16}>
        <ProductCodeCard
          active={false}
          productCode="PC040405"
          description="Clothing and related products (business-to-customer)-gym suits"
          details="Clothing and related products (business-to-customer)-gym suits"
          onClick={() => {}}
        />
        <ProductCodeCard
          active
          productCode="PC040405"
          description="Clothing and related products (business-to-customer)-gym suits"
          details="Clothing and related products (business-to-customer)-gym suits"
          onClick={() => {}}
        />
        <ProductCodeCard
          active={false}
          productCode="PC040405"
          description="Clothing and related products (business-to-customer)-gym suits"
          details="Clothing and related products (business-to-customer)-gym suits"
          onClick={() => {}}
        />
        <ProductCodeCard
          active={false}
          productCode="PC040405"
          description="Clothing and related products (business-to-customer)-gym suits"
          details="Clothing and related products (business-to-customer)-gym suits"
          onClick={() => {}}
        />
        <ProductCodeCard
          active={false}
          productCode="PC040405"
          description="Clothing and related products (business-to-customer)-gym suits"
          details="Clothing and related products (business-to-customer)-gym suits"
          onClick={() => {}}
        />
        <ProductCodeCard
          active={false}
          productCode="PC040405"
          description="Clothing and related products (business-to-customer)-gym suits"
          details="Clothing and related products (business-to-customer)-gym suits"
          onClick={() => {}}
        />
        <ProductCodeCard
          active={false}
          productCode="PC040405"
          description="Clothing and related products (business-to-customer)-gym suits"
          details="Clothing and related products (business-to-customer)-gym suits"
          onClick={() => {}}
        />
        <ProductCodeCard
          active={false}
          productCode="PC040405"
          description="Clothing and related products (business-to-customer)-gym suits"
          details="Clothing and related products (business-to-customer)-gym suits"
          onClick={() => {}}
        />
        <ProductCodeCard
          active={false}
          productCode="PC040405"
          description="Clothing and related products (business-to-customer)-gym suits"
          details="Clothing and related products (business-to-customer)-gym suits"
          onClick={() => {}}
        />
        <ProductCodeCard
          active={false}
          productCode="PC040405"
          description="Clothing and related products (business-to-customer)-gym suits"
          details="Clothing and related products (business-to-customer)-gym suits"
          onClick={() => {}}
        />
        <ProductCodeCard
          active={false}
          productCode="PC040405"
          description="Clothing and related products (business-to-customer)-gym suits"
          details="Clothing and related products (business-to-customer)-gym suits"
          onClick={() => {}}
        />
        <ProductCodeCard
          active={false}
          productCode="PC040405"
          description="Clothing and related products (business-to-customer)-gym suits"
          details="Clothing and related products (business-to-customer)-gym suits"
          onClick={() => {}}
        />
      </Container>
    </Container>
  </Modal>
);
