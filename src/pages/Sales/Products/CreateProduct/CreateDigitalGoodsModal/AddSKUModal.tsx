import { DropDown } from 'Components/DropDown';
import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { Tag } from 'Components/base/Tag';
import { Button } from 'Components/base/buttons/Button';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableTitleNew } from 'Components/elements/Table/TableTitleNew';
import { Search } from 'Components/inputs/Search';
import { Select } from 'Components/inputs/Select';
import { SelectOption } from 'Components/inputs/Select/Select.types';
import { OptionWithSingleLabel } from 'Components/inputs/Select/options/OptionWithSingleLabel';
import { TextInput } from 'Components/inputs/TextInput';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { options } from 'Mocks/fakeOptions';
import {
  Integrators,
  addSKUTableHeader,
  integratorsData,
  tagDataByStatus,
} from 'Pages/Sales/Products/CreateProduct/CreateDigitalGoodsModal/constants';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as DropdownIcon } from 'Svg/v2/16/three-dots.svg';
import { ReactComponent as CheckFilledIcon } from 'Svg/v2/16/tick-in-filled-circle.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import { ReactComponent as WarningIcon } from 'Svg/v2/16/warning.svg';
import classNames from 'classnames';
import React, { ReactElement, ReactNode, useState } from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';

export type StatusSku = 'waiting' | 'active' | 'disabled';

export type SkuData = {
  integratorId?: Integrators;
  skuName: string;
  status: StatusSku;
  cabinetName: SelectOption;
};

type AddSKUModalProps = {
  skuList: SkuData[];
  handleClose(): void;
  onSave(skuList: SkuData[]): void;
  editMode: boolean;
};

const AddSKUModal = ({
  handleClose,
  skuList,
  onSave,
  editMode,
}: AddSKUModalProps): ReactElement => {
  const [searchValue, setSearchValue] = useState<string>('');
  const [skuName, setSkuName] = useState<string>('');
  const [cabinetName, setCabinetName] = useState<SelectOption>({ value: '', label: '' });
  const [integratorId, setIntegratorId] = useState<Integrators | undefined>();

  const skuData: SkuData = {
    integratorId,
    skuName,
    cabinetName,
    status: 'waiting',
  };

  const disableSave = !integratorId || !skuName || !cabinetName?.value;

  const integrators = Object.keys(integratorsData) as Integrators[];

  const handleSave = () => {
    onSave([skuData]);
    handleClose();
  };

  const actionsBasedOnStatus: Record<StatusSku, ReactNode> = {
    waiting: (
      <DropDownButton
        type="danger"
        size="224"
        icon={<TrashIcon />}
        disabled={skuList.length === 1}
        tooltip={skuList.length === 1 ? 'Disable' : undefined}
      >
        Delete
      </DropDownButton>
    ),
    active: (
      <DropDownButton size="224" icon={<WarningIcon />}>
        Disable
      </DropDownButton>
    ),
    disabled: (
      <DropDownButton size="224" icon={<CheckFilledIcon />}>
        Activate
      </DropDownButton>
    ),
  };

  const modalTitle = editMode ? 'Edit SKU' : 'Add SKU';

  return (
    <Modal
      size="912"
      centered
      overlay="black-100"
      padding="none"
      pluginScrollDisabled
      header={
        <ModalHeaderNew
          title={modalTitle}
          fontType="body-medium"
          titlePosition="center"
          border
          closeActionBackgroundColor="inherit"
          onClose={() => {
            handleClose();
          }}
        />
      }
      footer={
        <ModalFooterNew border>
          <Button
            width="88"
            height="40"
            background={disableSave ? 'grey-90' : 'violet-90-violet-100'}
            disabled={disableSave}
            onClick={handleSave}
          >
            Save
          </Button>
        </ModalFooterNew>
      }
    >
      <TableNew
        noResults={false}
        className={classNames(spacing.mt24, spacing.mX24, spacing.pr24)}
        tableHead={
          <TableHeadNew>
            {addSKUTableHeader.map((item, index) => {
              return (
                <TableTitleNew
                  key={index}
                  minWidth={item.minWidth}
                  align={item.align}
                  title={item.title}
                  onClick={() => {}}
                />
              );
            })}
          </TableHeadNew>
        }
      >
        <TableBodyNew>
          {skuList.map((item, index) => (
            <TableRowNew key={index} background={'grey-10'} gap="8">
              <TableCellNew minWidth={addSKUTableHeader[0].minWidth} fixedWidth>
                <Select
                  label="Choose Integrator"
                  placeholder="Choose"
                  height="36"
                  width="215"
                  required
                  readOnly={item.status !== 'waiting'}
                  value={{
                    value: editMode && item.integratorId ? item.integratorId : integratorId || '',
                    label: item.integratorId
                      ? integratorsData[item.integratorId].title
                      : (integratorId && integratorsData[integratorId].title) || '',
                  }}
                >
                  {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
                    <SelectDropdown selectElement={selectElement} width="408">
                      <SelectDropdownBody>
                        <Search
                          className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                          width="full"
                          height="40"
                          name="search"
                          placeholder="Search Integrator"
                          value={searchValue}
                          onChange={(e) => setSearchValue(e.target.value)}
                          searchParams={[]}
                          onSearch={() => {}}
                          onClear={() => {}}
                        />
                        <SelectDropdownItemList>
                          {integrators.map((value, index) => (
                            <OptionWithSingleLabel
                              id={value}
                              key={index}
                              icon={integratorsData[value].icon}
                              iconStaticColor
                              label={integratorsData[value].title}
                              selected={!Array.isArray(selectValue) && selectValue?.value === value}
                              onClick={() => {
                                onOptionClick({
                                  value: value,
                                  label: integratorsData[value].title,
                                });
                                closeDropdown();
                                setIntegratorId(value);
                              }}
                            />
                          ))}
                        </SelectDropdownItemList>
                      </SelectDropdownBody>
                    </SelectDropdown>
                  )}
                </Select>
              </TableCellNew>
              <TableCellNew minWidth={addSKUTableHeader[1].minWidth} fixedWidth>
                <Select
                  label="Choose Cabinet name"
                  placeholder="Choose"
                  height="36"
                  width="215"
                  required
                  readOnly={item.status !== 'waiting'}
                  value={editMode ? item.cabinetName : cabinetName}
                >
                  {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
                    <SelectDropdown selectElement={selectElement} width="408">
                      <SelectDropdownBody>
                        <Search
                          className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                          width="full"
                          height="40"
                          name="search"
                          placeholder="Search Cabinet name"
                          value={searchValue}
                          onChange={(e) => setSearchValue(e.target.value)}
                          searchParams={[]}
                          onSearch={() => {}}
                          onClear={() => {}}
                        />
                        <SelectDropdownItemList>
                          {options.map((value, index) => (
                            <OptionWithSingleLabel
                              id={value.value}
                              key={index}
                              label={value?.label}
                              selected={
                                !Array.isArray(selectValue) && selectValue?.value === value.value
                              }
                              onClick={() => {
                                onOptionClick({
                                  value: value?.value,
                                  label: value?.label,
                                });
                                setCabinetName({
                                  value: value?.value,
                                  label: value?.label,
                                });
                                closeDropdown();
                              }}
                            />
                          ))}
                        </SelectDropdownItemList>
                      </SelectDropdownBody>
                    </SelectDropdown>
                  )}
                </Select>
              </TableCellNew>
              <TableCellNew minWidth={addSKUTableHeader[2].minWidth} fixedWidth>
                <TextInput
                  name="skuName"
                  label="SKU name"
                  readOnly={item.status !== 'waiting'}
                  value={editMode ? item.skuName : skuName}
                  onChange={(e) => {
                    setSkuName(e.target.value);
                  }}
                  required
                  width="192"
                  height="36"
                />
              </TableCellNew>
              <TableCellNew minWidth={addSKUTableHeader[3].minWidth} fixedWidth alignitems="center">
                <Tag
                  text={tagDataByStatus[item.status].text}
                  icon={tagDataByStatus[item.status].icon}
                  color={tagDataByStatus[item.status].color}
                />
              </TableCellNew>
              <TableCellNew minWidth={addSKUTableHeader[4].minWidth} fixedWidth alignitems="center">
                <DropDown
                  flexdirection="column"
                  padding="8"
                  gap="4"
                  control={({ handleOpen }) => (
                    <IconButton
                      size="24"
                      icon={<DropdownIcon />}
                      tooltip="Show actions"
                      onClick={handleOpen}
                      background={'transparent-grey-20'}
                      color="grey-100"
                    />
                  )}
                >
                  {actionsBasedOnStatus[item.status]}
                </DropDown>
              </TableCellNew>
            </TableRowNew>
          ))}
          <Container className={classNames(spacing.mt16, spacing.mr16)} justifycontent="flex-end">
            <LinkButton icon={<PlusIcon />} onClick={() => {}}>
              Add new line
            </LinkButton>
          </Container>
        </TableBodyNew>
      </TableNew>
    </Modal>
  );
};

export default AddSKUModal;
