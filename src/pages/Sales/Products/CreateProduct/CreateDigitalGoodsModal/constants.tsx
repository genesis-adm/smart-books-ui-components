import { TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { TableCellWidth } from 'Components/types/gridTypes';
import {
  SkuData,
  StatusSku,
} from 'Pages/Sales/Products/CreateProduct/CreateDigitalGoodsModal/AddSKUModal';
import { ReactComponent as AppleLogoBlackIcon } from 'Svg/v2/16/apple_logo_black.svg';
import { ReactComponent as ClockFilledIcon } from 'Svg/v2/16/clock-filled.svg';
import { ReactComponent as FacebookLogoBlackIcon } from 'Svg/v2/16/facebook-logo-black.svg';
import { ReactComponent as GoogleAdmobLogoBlackIcon } from 'Svg/v2/16/google-admob-logo-black.svg';
import { ReactComponent as GoogleAdSenseLogoBlackIcon } from 'Svg/v2/16/google-adsense-logo-black.svg';
import { ReactComponent as GoogleAdwordsLogoBlackIcon } from 'Svg/v2/16/google-adwords-logo-black.svg';
import { ReactComponent as GoogleAdXLogoBlackIcon } from 'Svg/v2/16/google-adx-logo-black.svg';
import { ReactComponent as GooglePlayLogoBlackIcon } from 'Svg/v2/16/google-play-logo-black.svg';
import { ReactComponent as CheckFilledIcon } from 'Svg/v2/16/tick-in-filled-circle.svg';
import { ReactComponent as WarningIcon } from 'Svg/v2/16/warning.svg';
import { ReactNode } from 'react';
import React from 'react';

export type Integrators =
  | 'apple'
  | 'googleAdx'
  | 'googleAdmob'
  | 'facebookAds'
  | 'googleAdwords'
  | 'googleAdSense'
  | 'googlePlay';

export const addSKUTableHeader: {
  minWidth: TableCellWidth;
  title: string;
  align?: TableTitleNewProps['align'];
}[] = [
  {
    minWidth: '230',
    title: 'Integrator',
  },
  {
    minWidth: '230',
    title: 'Cabinet name',
  },
  {
    minWidth: '210',
    title: 'SKU',
  },
  {
    minWidth: '93',
    title: 'Status',
    align: 'center',
  },
  {
    minWidth: '70',
    title: 'Actions',
    align: 'center',
  },
];

export const tagDataByStatus: Record<
  StatusSku,
  {
    text: string;
    icon: ReactNode;
    color: 'green-10-green-20' | 'yellow-10-yellow-20' | 'grey';
    tooltipMessage?: string;
  }
> = {
  waiting: {
    text: 'Waiting',
    icon: <ClockFilledIcon />,
    color: 'grey',
    tooltipMessage: 'Waiting',
  },
  active: {
    text: 'Active',
    icon: <CheckFilledIcon />,
    color: 'green-10-green-20',
    tooltipMessage: 'Processed 100 transactions Editing/deletion is not possible',
  },
  disabled: {
    text: 'Disabled',
    icon: <WarningIcon />,
    color: 'yellow-10-yellow-20',
    tooltipMessage: 'Disabled',
  },
};

export const integratorsData: Record<Integrators, { title: string; icon?: ReactNode }> = {
  apple: {
    title: 'Apple',
    icon: <AppleLogoBlackIcon />,
  },
  googleAdx: {
    title: 'Google AdX',
    icon: <GoogleAdXLogoBlackIcon />,
  },
  googleAdmob: {
    title: 'Google AdMob',
    icon: <GoogleAdmobLogoBlackIcon />,
  },
  facebookAds: {
    title: 'Facebook ads',
    icon: <FacebookLogoBlackIcon />,
  },
  googleAdwords: {
    title: 'Google Adwords',
    icon: <GoogleAdwordsLogoBlackIcon />,
  },
  googleAdSense: {
    title: 'Google AdSense',
    icon: <GoogleAdSenseLogoBlackIcon />,
  },
  googlePlay: {
    title: 'Google Play',
    icon: <GooglePlayLogoBlackIcon />,
  },
};

export const initialData: SkuData[] = [
  {
    integratorId: 'googlePlay',
    cabinetName: { value: 'value-1', label: 'Main label 1' },
    skuName: 'liti.app.subs.9.00_monthly',
    status: 'active',
  },
  {
    integratorId: 'apple',
    cabinetName: { value: 'value-1', label: 'Main label 1' },
    skuName: 'liti.app.subs.9.00_monthly',
    status: 'disabled',
  },
  {
    integratorId: 'googleAdwords',
    cabinetName: { value: 'value-1', label: 'Main label 1' },
    skuName: 'liti.app.subs.9.00_monthly',
    status: 'waiting',
  },
];
