import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as ManWithDocs } from 'Svg/v2/colored/man-with-docs.svg';
import React, { ReactElement } from 'react';

import spacing from '../../../../../../assets/styles/spacing.module.scss';

type EmptySkuProps = { handleSkuModalOpen(): void };

const EmptySku = ({ handleSkuModalOpen }: EmptySkuProps): ReactElement => (
  <Container
    justifycontent="center"
    alignitems="center"
    flexdirection="column"
    gap="20"
    className={spacing.h100p}
  >
    <ManWithDocs />
    <Text color="grey-100" type="body-regular-14">
      Please add SKU to create product
    </Text>
    <Button
      height="24"
      background={'blue-10-blue-20'}
      color="violet-90"
      font="text-medium"
      onClick={handleSkuModalOpen}
    >
      Add SKU
    </Button>
  </Container>
);

export default EmptySku;
