import { Container } from 'Components/base/grid/Container';
import BusinessUnitsSelect from 'Components/custom/Stories/selects/BusinessUnitsSelect';
import { TextArea } from 'Components/inputs/TextArea';
import { TextInput } from 'Components/inputs/TextInput';
import React, { ReactElement, useState } from 'react';

const DigitalGoodsMainForm = (): ReactElement => {
  const [productName, setProductName] = useState<string>('');
  const [notes, setNotes] = useState<string>('');

  return (
    <>
      <Container gap="24">
        <BusinessUnitsSelect />
        <TextInput
          name="productName"
          label="Product name"
          value={productName}
          onChange={(e) => {
            setProductName(e.target.value);
          }}
          required
          width="full"
        />
      </Container>
      <TextArea
        name="notes"
        label="Notes"
        value={notes}
        minHeight="88"
        onChange={(e) => {
          setNotes(e.target.value);
        }}
        width="full"
      />
    </>
  );
};

export default DigitalGoodsMainForm;
