import { Tag } from 'Components/base/Tag';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { SkuData } from 'Pages/Sales/Products/CreateProduct/CreateDigitalGoodsModal/AddSKUModal';
import {
  integratorsData,
  tagDataByStatus,
} from 'Pages/Sales/Products/CreateProduct/CreateDigitalGoodsModal/constants';
import { ReactComponent as PencilIcon } from 'Svg/v2/16/pencil.svg';
import classNames from 'classnames';
import React, { ReactElement } from 'react';

import spacing from '../../../../../../assets/styles/spacing.module.scss';

type SkuListViewProps = {
  skuList: SkuData[];
  onEdit(): void;
};

const SkuListView = ({ skuList, onEdit }: SkuListViewProps): ReactElement => {
  return (
    <Container
      className={classNames(spacing.h104min, spacing.p24)}
      border={'grey-20'}
      borderRadius="10"
      flexdirection="column"
      gap="4"
    >
      <Container justifycontent="space-between">
        <Text color="grey-100" type="body-regular-14">
          SKU
        </Text>
        <IconButton
          icon={<PencilIcon />}
          size="24"
          background={'grey-10-grey-20'}
          tooltip="Edit SKU"
          onClick={onEdit}
        />
      </Container>
      <Container gap="16" flexwrap="wrap">
        {skuList.map(({ integratorId, skuName, status }) =>
          !integratorId ? (
            <></>
          ) : (
            <Tag
              key={skuName}
              text={integratorId ? `${integratorsData[integratorId]?.title} | ${skuName}` : ''}
              icon={integratorId ? integratorsData[integratorId].icon : undefined}
              staticIconColor
              additionalIconRight={tagDataByStatus[status].icon}
              color={tagDataByStatus[status].color}
              additionalIconTooltip={tagDataByStatus[status].tooltipMessage}
              isNotClickable
            />
          ),
        )}
      </Container>
    </Container>
  );
};

export default SkuListView;
