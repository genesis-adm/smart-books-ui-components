import { Meta, Story } from '@storybook/react';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { MenuTab, MenuTabWrapper } from 'Components/base/tabs/MenuTab';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import AddSKUModal, {
  SkuData,
} from 'Pages/Sales/Products/CreateProduct/CreateDigitalGoodsModal/AddSKUModal';
import DigitalGoodsMainForm from 'Pages/Sales/Products/CreateProduct/CreateDigitalGoodsModal/components/DigitalGoodsMainForm';
import EmptySku from 'Pages/Sales/Products/CreateProduct/CreateDigitalGoodsModal/components/EmptySku';
import SkuListView from 'Pages/Sales/Products/CreateProduct/CreateDigitalGoodsModal/components/SkuListView';
import { initialData } from 'Pages/Sales/Products/CreateProduct/CreateDigitalGoodsModal/constants';
import { ReactComponent as ArrowLeftIcon } from 'Svg/v2/16/arrow-left.svg';
import classNames from 'classnames';
import React, { useEffect, useState } from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';

const CreateDigitalGoodsModal: Story = ({ mode }) => {
  const [isOpenedSKUModal, setSKUModalOpened] = useState<boolean>(false);

  const [skuList, setSkuList] = useState<SkuData[]>([]);

  const handleCloseSKUModal = () => {
    setSKUModalOpened(false);
  };

  const handleOpenSkuModal = () => {
    setSKUModalOpened(true);
  };

  const modalTitle = mode === 'edit' ? 'Edit Digital goods' : 'Create new Digital goods';

  useEffect(() => {
    console.log(mode);
    mode === 'edit'
      ? setSkuList(initialData)
      : setSkuList([
          {
            skuName: '',
            status: 'waiting',
            cabinetName: { value: '', label: '' },
          },
        ]);
  }, [mode]);

  return (
    <>
      {isOpenedSKUModal && (
        <AddSKUModal
          skuList={skuList}
          handleClose={handleCloseSKUModal}
          onSave={(skuList) => setSkuList(skuList)}
          editMode={mode === 'edit'}
        />
      )}
      <Modal
        size="720"
        background={'grey-10'}
        type="new"
        classNameModalBody={spacing.h100p}
        header={
          <ModalHeaderNew
            title={modalTitle}
            background={'grey-10'}
            border
            closeActionBackgroundColor="inherit"
            onClose={() => {}}
          />
        }
        footer={
          <ModalFooterNew background={'grey-10'} border>
            <Button width="128" onClick={() => {}}>
              Create
            </Button>
            <Button
              width="128"
              background={'transparent'}
              color="grey-100-black-100"
              border="grey-90"
              onClick={() => {}}
              iconLeft={<ArrowLeftIcon />}
              navigation
            >
              Back
            </Button>
          </ModalFooterNew>
        }
      >
        <MenuTabWrapper>
          <MenuTab id="general" active onClick={() => {}}>
            General
          </MenuTab>
          <MenuTab id="accounting" onClick={() => {}}>
            Accounting
          </MenuTab>
        </MenuTabWrapper>
        <Container
          flexdirection="column"
          borderRadius="20"
          border="grey-20"
          background={'white-100'}
          gap="24"
          className={classNames(spacing.mt4, spacing.p24, spacing.h100p)}
        >
          <DigitalGoodsMainForm />

          {skuList.length && !skuList.every((item) => !item.integratorId) ? (
            <SkuListView skuList={skuList} onEdit={handleOpenSkuModal} />
          ) : (
            <EmptySku handleSkuModalOpen={handleOpenSkuModal} />
          )}
        </Container>
      </Modal>
    </>
  );
};

export default {
  title: 'Pages/Sales/Products/Modals/DigitalGoods/Create Digital Goods',
  component: CreateDigitalGoodsModal,
  argTypes: {
    mode: {
      options: ['create', 'edit'],
      control: {
        type: 'radio',
        labels: {
          create: 'Create',
          edit: 'Edit',
        },
      },
    },
  },
} as Meta;

export const CreateDigitalGoods = CreateDigitalGoodsModal.bind({});
CreateDigitalGoods.args = {
  mode: 'create',
};
