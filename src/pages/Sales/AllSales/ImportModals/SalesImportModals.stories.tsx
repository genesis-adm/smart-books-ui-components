import { Meta } from '@storybook/react';
import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownFooter } from 'Components/SelectDropdown/components/SelectDropdownFooter';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { Checkbox } from 'Components/base/Checkbox';
import { Divider } from 'Components/base/Divider';
import { Radio } from 'Components/base/Radio';
import { Tag } from 'Components/base/Tag';
import { Button } from 'Components/base/buttons/Button';
import { IconButton } from 'Components/base/buttons/IconButton';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { InputDateNew } from 'Components/base/inputs/InputDateNew';
import { InputNew } from 'Components/base/inputs/InputNew';
import { NumericInput } from 'Components/base/inputs/NumericInput';
import { SelectNew } from 'Components/base/inputs/SelectNew';
import { OptionSingleNew } from 'Components/base/inputs/SelectNew/options/OptionSingleNew';
import { Text } from 'Components/base/typography/Text';
import { AccountingCardLE } from 'Components/custom/Accounting/Journal/AccountingCardLE';
import { RulesSelectLE } from 'Components/custom/Rules/RulesSelectLE';
import { Pagination } from 'Components/custom/Stories/Pagination/Pagination';
import { UploadContainer } from 'Components/custom/Upload/UploadContainer';
import { UploadInfo } from 'Components/custom/Upload/UploadInfo';
import { UploadItemField } from 'Components/custom/Upload/UploadItemField';
import { Logo } from 'Components/elements/Logo';
import { SelectImport } from 'Components/elements/SelectImport';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { FilterDropDown } from 'Components/elements/filters/FilterDropDown';
import { FilterDropDownItem } from 'Components/elements/filters/FilterDropDownItem';
import { FilterItemsContainer } from 'Components/elements/filters/FilterItemsContainer';
import { CircledIcon } from 'Components/graphics/CircledIcon';
import { Search } from 'Components/inputs/Search';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { TableCellWidth } from 'Components/types/gridTypes';
import { options } from 'Mocks/fakeOptions';
import {
  buyerAddressColumns,
  buyerInformationColumns,
  customerColumns,
  feeColumns,
  generalColumns,
  productColumns,
  tableHead,
  taxColumns,
} from 'Mocks/salesImportModalsData';
import { ReactComponent as BriefcaseIcon } from 'Svg/v2/12/briefcase.svg';
import { ReactComponent as ArrowLeftIcon } from 'Svg/v2/16/arrow-left.svg';
import { ReactComponent as ArrowRightIcon } from 'Svg/v2/16/arrow-right.svg';
import { ReactComponent as ClearIcon } from 'Svg/v2/16/cross.svg';
import { ReactComponent as CSVIcon } from 'Svg/v2/16/file-csv.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as TickIcon } from 'Svg/v2/16/tick-in-circle.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import { ReactComponent as UploadIcon } from 'Svg/v2/16/upload.svg';
import { checkFileType } from 'Utils/fileHandlers';
import classNames from 'classnames';
import React, { useEffect, useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Sales/All Sales/Import Modals/Sales Import Modals',
  argTypes: {
    taxAgent: {
      control: { type: 'boolean' },
    },
    marketplace: {
      control: { type: 'boolean' },
    },
  },
  args: {
    taxAgent: true,
    marketplace: true,
  },
} as Meta;

export const ImportSalesStep1 = () => {
  const [file, setFile] = useState<File | null>(null);
  const handleFileUpload = (item: File) => {
    setFile(item);
  };
  const handleFileDelete = () => {
    setFile(null);
  };
  const handleFileDrop = (e: React.DragEvent<HTMLElement>) => {
    const dropFile = e.dataTransfer.files[0];
    if (checkFileType(dropFile, ['csv'])) {
      setFile(e.dataTransfer.files[0]);
    }
  };
  return (
    <Modal
      size="md-extrawide"
      height="690"
      header={<ModalHeaderNew onClose={() => {}} />}
      pluginScrollDisabled
      footer={
        <ModalFooterNew justifycontent="space-between" border>
          <Container alignitems="center" gap="16" flexgrow="1" justifycontent="flex-end">
            <Container flexdirection="column">
              <Text type="body-regular-14" color="grey-100">
                Step: 1 of 3
              </Text>
              <Text type="body-regular-14" color="grey-100">
                Next: Match columns
              </Text>
            </Container>
            <Button
              height="48"
              navigation
              iconRight={<ArrowRightIcon />}
              width="230"
              onClick={() => {}}
              background={'grey-90'}
            >
              Next
            </Button>
          </Container>
          <Button
            height="40"
            background={'white-100-blue-10'}
            color="grey-100-violet-90"
            onClick={() => {}}
            iconLeft={<CSVIcon />}
          >
            Export Products list .CSV
          </Button>
          <Button
            height="40"
            background={'white-100-blue-10'}
            color="grey-100-violet-90"
            onClick={() => {}}
            iconLeft={<CSVIcon />}
          >
            Export Customers list .CSV
          </Button>
        </ModalFooterNew>
      }
    >
      <Container flexdirection="column" alignitems="center">
        <Text className={spacing.mt32} type="h2-semibold" align="center">
          Import sales
        </Text>
        <UploadContainer className={spacing.mt40} onDrop={handleFileDrop}>
          <UploadItemField
            name="uploader"
            files={file}
            acceptFilesType="csv"
            onChange={handleFileUpload}
            onDelete={handleFileDelete}
            onReload={() => {}}
          />
          <UploadInfo
            mainText={file ? file.name : 'Drag and drop or Choose file'}
            secondaryText="CSV; Maximum file size is 5MB"
          />
        </UploadContainer>
        <Container
          className={classNames(spacing.mt40, spacing.w440)}
          flexdirection="column"
          gap="4"
        >
          <Text type="body-regular-14">Instruction for upload file:</Text>
          <Text type="body-regular-14">• All your information must be in one file</Text>
          <Text type="body-regular-14">
            • The top row of your file must contain a header title for each column of information
          </Text>
          <Container flexdirection="column">
            <Text type="body-regular-14">• To make sure your layout matches SmartBooks,</Text>
            <Container>
              <LinkButton type="body-regular-14" onClick={() => {}}>
                download this sample CSV
              </LinkButton>
            </Container>
          </Container>
        </Container>
      </Container>
    </Modal>
  );
};

export const ChooseLegalEntityStep2: React.FC = () => (
  <Modal size="1130" pluginScrollDisabled background={'white-100'} padding="none">
    <Container
      className={classNames(spacing.pt24, spacing.pX16)}
      flexdirection="column"
      flexgrow="1"
    >
      <Container justifycontent="space-between" alignitems="center">
        <Container alignitems="center" gap="16">
          <Text type="title-semibold" color="black-100">
            Please choose the legal entity
          </Text>

          <Search
            height="40"
            width="320"
            name="search"
            value=""
            placeholder="Search by legal entity"
            onClear={() => {}}
            onChange={() => {}}
            searchParams={[]}
            onSearch={() => {}}
          />
        </Container>
        <IconButton
          size="32"
          icon={<ClearIcon />}
          onClick={() => {}}
          background={'grey-10-grey-20'}
          color="grey-100"
        />
      </Container>
      <Container
        className={classNames(spacing.mY32, spacing.h310)}
        customScroll
        flexgrow="1"
        gap="16"
        flexwrap="wrap"
        aligncontent="flex-start"
      >
        {Array.from(Array(9)).map((_, index) => (
          <AccountingCardLE key={index} name="BetterMe Business Holdings Corp" onClick={() => {}} />
        ))}
      </Container>
      <Pagination padding="16" />
    </Container>
  </Modal>
);

export const MatchColumnsStep3 = ({ marketplace }: { marketplace: boolean }) => {
  const [isOpenTaxes, setIsOpenTaxes] = useState(false);
  const [isOpenFees, setIsOpenFees] = useState(false);
  const [isOpenBuyerInformation, setIsOpenBuyerInformation] = useState(false);
  const [isTaxRateChecked, setIsTaxRateChecked] = useState(true);
  const [isFeeRateChecked, setIsFeeRateChecked] = useState(true);
  const [taxColumnsArr, setTaxColumnsArr] = useState(taxColumns);
  const [feeColumnsArr, setFeeColumnsArr] = useState(feeColumns);

  const typeTagText = marketplace ? 'Marketplace' : 'Direct sales';
  const handleOpenTaxes = () => setIsOpenTaxes(true);
  const handleOpenFees = () => setIsOpenFees(true);
  const handleOpenBuyerInformation = () => setIsOpenBuyerInformation(true);
  const handleTaxRateChecked = () => setIsTaxRateChecked(!isTaxRateChecked);
  const handleFeeRateChecked = () => setIsFeeRateChecked(!isFeeRateChecked);

  useEffect(() => {
    const columnType = isTaxRateChecked ? 'amount' : 'rate';

    const updatedTaxColumns = taxColumns.map((item) => ({
      ...item,
      unchecked: item.id === columnType,
    }));

    setTaxColumnsArr(updatedTaxColumns);
  }, [isTaxRateChecked]);

  useEffect(() => {
    const columnType = isFeeRateChecked ? 'feeAmount' : 'feeRate';

    const updatedFeeColumns = feeColumns.map((item) => ({
      ...item,
      unchecked: item.id === columnType,
    }));

    setFeeColumnsArr(updatedFeeColumns);
  }, [isFeeRateChecked]);

  return (
    <Modal
      size="full"
      overlay="grey-10"
      background={'grey-10'}
      pluginScrollDisabled
      header={
        <ModalHeaderNew
          type="fullwidth"
          background={'grey-10'}
          title="Import sales"
          icon={
            <Container gap="12">
              <Tag color="grey" text={typeTagText} padding="4-8" cursor="default" />
              <Tag color="grey" text="File name: CSV" padding="4-8" cursor="default" />
            </Container>
          }
          border
          onClose={() => {}}
        />
      }
      footer={
        <ModalFooterNew justifycontent="space-between" background={'grey-10'} border>
          <Container alignitems="center" gap="16" flexgrow="1" justifycontent="flex-end">
            <Container flexdirection="column">
              <Text type="body-regular-14" color="grey-100">
                Step: 2 of 3
              </Text>
              <Text type="body-regular-14" color="grey-100">
                Next: Preview mode
              </Text>
            </Container>
            <Button
              height="48"
              background={'grey-90'}
              navigation
              iconRight={<ArrowRightIcon />}
              width="230"
              onClick={() => {}}
            >
              Next
            </Button>
          </Container>
          <Button
            height="48"
            width="sm"
            navigation
            iconLeft={<ArrowLeftIcon />}
            background={'white-100'}
            color="black-100"
            border="grey-20-grey-90"
            onClick={() => {}}
          >
            Back
          </Button>
        </ModalFooterNew>
      }
    >
      <Container
        background={'white-100'}
        flexdirection="column"
        radius
        flexgrow="1"
        className={classNames(spacing.p24, spacing.mX20, spacing.mY16)}
        customScroll
        gap="40"
        borderRadius="20"
      >
        <RulesSelectLE name="BetterMe Business">
          <Container
            flexdirection="column"
            className={classNames(spacing.w1130fixed, spacing.pt24, spacing.pX24)}
          >
            <Container className={spacing.w100p} justifycontent="space-between" alignitems="center">
              <Container alignitems="center" gap="16">
                <Text type="title-semibold">Please choose the legal entity</Text>
                <Search
                  width="320"
                  height="40"
                  name="search-banking-rules-mainPage"
                  placeholder="Search by legal entity"
                  value=""
                  onChange={() => {}}
                  searchParams={[]}
                  onSearch={() => {}}
                  onClear={() => {}}
                />
              </Container>
              <IconButton
                size="32"
                icon={<ClearIcon />}
                onClick={() => {}}
                background={'grey-10-grey-20'}
                color="grey-100"
              />
            </Container>
            <Container className={spacing.mY24} gap="16" flexwrap="wrap">
              {Array.from(Array(9)).map((_, index) => (
                <AccountingCardLE
                  key={index}
                  name="BetterMe Business Holdings Corp"
                  onClick={() => {}}
                />
              ))}
            </Container>
            <Pagination padding="16" />
          </Container>
        </RulesSelectLE>
        <Text type="body-regular-16">
          Map columns from&nbsp;
          <Text type="body-medium" display="inline-flex">
            namefile 09.10.20.xls
          </Text>
          &nbsp;for each Smart Books field
        </Text>
        <Text type="body-medium">General</Text>
        <Container gap="16-100" flexwrap="wrap" className={spacing.w1400}>
          {generalColumns.map((item) => (
            <SelectImport
              key={item.id}
              name={item.name}
              tooltip="Tooltip text"
              label="Choose column from your file"
              placeholder="Choose option"
              background={'grey-10'}
              textWidth="260"
              required={item.required}
            >
              {({ onClick, state }) => (
                <>
                  {options.map(({ label, value }) => (
                    <OptionSingleNew
                      key={value}
                      label={label}
                      selected={state?.value === value}
                      onClick={() =>
                        onClick({
                          label,
                          value,
                        })
                      }
                    />
                  ))}
                </>
              )}
            </SelectImport>
          ))}
        </Container>
        <Text type="body-medium">Customer</Text>
        <Container gap="16-100" flexwrap="wrap" className={spacing.w1400}>
          {customerColumns.map((item) => (
            <SelectImport
              key={item.id}
              name={item.name}
              tooltip="Tooltip text"
              label="Choose column from your file"
              placeholder="Choose option"
              background={'grey-10'}
              textWidth="260"
              required={item.required}
            >
              {({ onClick, state }) => (
                <>
                  {options.map(({ label, value }) => (
                    <OptionSingleNew
                      key={value}
                      label={label}
                      selected={state?.value === value}
                      onClick={() =>
                        onClick({
                          label,
                          value,
                        })
                      }
                    />
                  ))}
                </>
              )}
            </SelectImport>
          ))}
        </Container>
        <Text type="body-medium">Product</Text>
        <Container gap="16-100" flexwrap="wrap" className={spacing.w1400}>
          {productColumns.map((item) => (
            <SelectImport
              key={item.id}
              name={item.name}
              tooltip="Tooltip text"
              label="Choose column from your file"
              placeholder="Choose option"
              background={'grey-10'}
              textWidth="260"
              required={item.required}
            >
              {({ onClick, state }) => (
                <>
                  {options.map(({ label, value }) => (
                    <OptionSingleNew
                      key={value}
                      label={label}
                      selected={state?.value === value}
                      onClick={() =>
                        onClick({
                          label,
                          value,
                        })
                      }
                    />
                  ))}
                </>
              )}
            </SelectImport>
          ))}
        </Container>
        <Container flexdirection="column" gap="24">
          {!isOpenTaxes && (
            <Button
              dashed="always"
              background={'grey-10'}
              width="full"
              color="violet-90"
              border="violet-90-opacity"
              onClick={handleOpenTaxes}
            >
              + Add Taxes
            </Button>
          )}
          {isOpenTaxes && (
            <Container flexdirection="column">
              <Divider fullHorizontalWidth />
              <Container justifycontent="center">
                <Container
                  gap="12"
                  alignitems="center"
                  background={'grey-10'}
                  className={classNames(spacing.w328, spacing.pX11, spacing.mY24)}
                  borderRadius="10"
                >
                  <Text type="body-regular-14">Please choose:</Text>
                  <Radio
                    id="rate"
                    color="black-100"
                    type="body-regular-14"
                    onChange={handleTaxRateChecked}
                    name="taxRate"
                    checked={isTaxRateChecked}
                  >
                    Tax rate
                  </Radio>
                  <Radio
                    id="amount"
                    color="grey-100"
                    type="body-regular-14"
                    onChange={handleTaxRateChecked}
                    name="taxAmount"
                    checked={!isTaxRateChecked}
                  >
                    Tax amount
                  </Radio>
                </Container>
              </Container>
              <SelectImport
                name="TAX calculations"
                tooltip="Tooltip text"
                label="Choose column from your file"
                placeholder="Choose option"
                background={'grey-10'}
                textWidth="260"
                required
              >
                {({ onClick, state }) => (
                  <>
                    {options.map(({ label, value }) => (
                      <OptionSingleNew
                        key={value}
                        label={label}
                        selected={state?.value === value}
                        onClick={() =>
                          onClick({
                            label,
                            value,
                          })
                        }
                      />
                    ))}
                  </>
                )}
              </SelectImport>
              <Container alignitems="center" justifycontent="space-between">
                <Text type="body-medium" className={spacing.pY28}>
                  Tax #1
                </Text>
                <IconButton
                  icon={<TrashIcon />}
                  onClick={() => {}}
                  size="24"
                  background={'grey-10-red-10'}
                  color="grey-100-red-90"
                />
              </Container>
              <Container gap="16-100" flexwrap="wrap" className={spacing.w1400}>
                {taxColumnsArr.map((item) => (
                  <SelectImport
                    key={item.id}
                    name={item.name}
                    tooltip="Tooltip text"
                    label="Choose column from your file"
                    placeholder="Choose option"
                    background={'grey-10'}
                    textWidth="260"
                    required={item.required}
                    disabled={item.unchecked}
                  >
                    {({ onClick, state }) => (
                      <>
                        {options.map(({ label, value }) => (
                          <OptionSingleNew
                            key={value}
                            label={label}
                            selected={state?.value === value}
                            onClick={() =>
                              onClick({
                                label,
                                value,
                              })
                            }
                          />
                        ))}
                      </>
                    )}
                  </SelectImport>
                ))}
              </Container>
              <Container alignitems="center" justifycontent="space-between">
                <Text type="body-medium" className={spacing.pY28}>
                  Tax #2
                </Text>
                <IconButton
                  icon={<TrashIcon />}
                  onClick={() => {}}
                  size="24"
                  background={'grey-10-red-10'}
                  color="grey-100-red-90"
                />
              </Container>
              <Container gap="16-100" flexwrap="wrap" className={spacing.w1400}>
                {taxColumnsArr.map((item) => (
                  <SelectImport
                    key={item.id}
                    name={item.name}
                    tooltip="Tooltip text"
                    label="Choose column from your file"
                    placeholder="Choose option"
                    background={'grey-10'}
                    textWidth="260"
                    required={item.required}
                    disabled={item.unchecked}
                  >
                    {({ onClick, state }) => (
                      <>
                        {options.map(({ label, value }) => (
                          <OptionSingleNew
                            key={value}
                            label={label}
                            selected={state?.value === value}
                            onClick={() =>
                              onClick({
                                label,
                                value,
                              })
                            }
                          />
                        ))}
                      </>
                    )}
                  </SelectImport>
                ))}
              </Container>
              <Container justifycontent="flex-end" className={spacing.pt23}>
                <LinkButton icon={<PlusIcon />} onClick={() => {}}>
                  Add Taxes
                </LinkButton>
              </Container>
            </Container>
          )}
          {!isOpenFees && marketplace && (
            <Button
              dashed="always"
              background={'grey-10'}
              width="full"
              color="violet-90"
              border="violet-90-opacity"
              onClick={handleOpenFees}
            >
              + Add Fees
            </Button>
          )}
          {isOpenFees && marketplace && (
            <Container flexdirection="column">
              <Divider fullHorizontalWidth />
              <Container justifycontent="center">
                <Container
                  gap="12"
                  alignitems="center"
                  background={'grey-10'}
                  className={classNames(spacing.w328, spacing.pX11, spacing.mY24)}
                  borderRadius="10"
                >
                  <Text type="body-regular-14">Please choose:</Text>
                  <Radio
                    id="feeRate"
                    color="black-100"
                    type="body-regular-14"
                    onChange={handleFeeRateChecked}
                    name="feeRate"
                    checked={isFeeRateChecked}
                  >
                    Fee rate
                  </Radio>
                  <Radio
                    id="feeAmount"
                    color="grey-100"
                    type="body-regular-14"
                    onChange={handleFeeRateChecked}
                    name="feeAmount"
                    checked={!isFeeRateChecked}
                  >
                    Fee amount
                  </Radio>
                </Container>
              </Container>
              <Container alignitems="center" justifycontent="space-between">
                <Text type="body-medium" className={spacing.pY28}>
                  Fee #1
                </Text>
                <IconButton
                  icon={<TrashIcon />}
                  onClick={() => {}}
                  size="24"
                  background={'grey-10-red-10'}
                  color="grey-100-red-90"
                />
              </Container>
              <Container gap="16-100" flexwrap="wrap" className={spacing.w1400}>
                {feeColumnsArr.map((item) => (
                  <SelectImport
                    key={item.id}
                    name={item.name}
                    tooltip="Tooltip text"
                    label="Choose column from your file"
                    placeholder="Choose option"
                    background={'grey-10'}
                    textWidth="260"
                    required={item.required}
                    disabled={item.unchecked}
                  >
                    {({ onClick, state }) => (
                      <>
                        {options.map(({ label, value }) => (
                          <OptionSingleNew
                            key={value}
                            label={label}
                            selected={state?.value === value}
                            onClick={() =>
                              onClick({
                                label,
                                value,
                              })
                            }
                          />
                        ))}
                      </>
                    )}
                  </SelectImport>
                ))}
              </Container>
              <Container justifycontent="flex-end" className={spacing.pt23}>
                <LinkButton icon={<PlusIcon />} onClick={() => {}}>
                  Add Fees
                </LinkButton>
              </Container>
            </Container>
          )}
          {!isOpenBuyerInformation && marketplace && (
            <Button
              dashed="always"
              background={'grey-10'}
              width="full"
              color="violet-90"
              border="violet-90-opacity"
              onClick={handleOpenBuyerInformation}
            >
              + Add Вuyer informations
            </Button>
          )}
          {isOpenBuyerInformation && marketplace && (
            <Container flexdirection="column">
              <Divider fullHorizontalWidth />
              <Container alignitems="center" justifycontent="space-between">
                <Text type="body-medium" className={spacing.pY28}>
                  Вuyer informations
                </Text>
                <IconButton
                  icon={<TrashIcon />}
                  onClick={() => {}}
                  size="24"
                  background={'grey-10-red-10'}
                  color="grey-100-red-90"
                />
              </Container>
              <Container gap="16-100" flexwrap="wrap" className={spacing.w1400}>
                {buyerInformationColumns.map((item) => (
                  <SelectImport
                    key={item.id}
                    name={item.name}
                    tooltip="Tooltip text"
                    label="Choose column from your file"
                    placeholder="Choose option"
                    background={'grey-10'}
                    textWidth="260"
                    required={item.required}
                  >
                    {({ onClick, state }) => (
                      <>
                        {options.map(({ label, value }) => (
                          <OptionSingleNew
                            key={value}
                            label={label}
                            selected={state?.value === value}
                            onClick={() =>
                              onClick({
                                label,
                                value,
                              })
                            }
                          />
                        ))}
                      </>
                    )}
                  </SelectImport>
                ))}
              </Container>
              <Container gap="100">
                <Container flexdirection="column">
                  <Text type="body-medium" className={spacing.pY28}>
                    Buyer Billing address
                  </Text>
                  <Container gap="16" flexdirection="column">
                    {buyerAddressColumns.map((item) => (
                      <SelectImport
                        key={item.id}
                        name={item.name}
                        tooltip="Tooltip text"
                        label="Choose column from your file"
                        placeholder="Choose option"
                        background={'grey-10'}
                        textWidth="260"
                        required={item.required}
                      >
                        {({ onClick, state }) => (
                          <>
                            {options.map(({ label, value }) => (
                              <OptionSingleNew
                                key={value}
                                label={label}
                                selected={state?.value === value}
                                onClick={() =>
                                  onClick({
                                    label,
                                    value,
                                  })
                                }
                              />
                            ))}
                          </>
                        )}
                      </SelectImport>
                    ))}
                  </Container>
                </Container>
                <Container flexdirection="column">
                  <Text type="body-medium" className={spacing.pY28}>
                    Buyer Shipping address
                  </Text>
                  <Container gap="16" flexdirection="column">
                    {buyerAddressColumns.map((item) => (
                      <SelectImport
                        key={item.id}
                        name={item.name}
                        tooltip="Tooltip text"
                        label="Choose column from your file"
                        placeholder="Choose option"
                        background={'grey-10'}
                        textWidth="260"
                        required={item.required}
                      >
                        {({ onClick, state }) => (
                          <>
                            {options.map(({ label, value }) => (
                              <OptionSingleNew
                                key={value}
                                label={label}
                                selected={state?.value === value}
                                onClick={() =>
                                  onClick({
                                    label,
                                    value,
                                  })
                                }
                              />
                            ))}
                          </>
                        )}
                      </SelectImport>
                    ))}
                  </Container>
                </Container>
              </Container>
            </Container>
          )}
        </Container>
      </Container>
    </Modal>
  );
};

export const PreviewModeStep4 = ({ marketplace }: { marketplace: boolean }) => {
  const [startDay, setStartDay] = useState(new Date());
  const typeTagText = marketplace ? 'Marketplace' : 'Direct sales';

  return (
    <Modal
      size="full"
      overlay="grey-10"
      background={'grey-10'}
      pluginScrollDisabled
      header={
        <Container
          className={classNames(spacing.w100p, spacing.p24)}
          alignitems="center"
          justifycontent="space-between"
          flexwrap="wrap"
          style={{ borderBottom: '1px solid #EDEFF1' }}
        >
          <Container gap="24" alignitems="center">
            <Container gap="8">
              <Text type="title-bold">Import sales</Text>
              <Tag color="grey" text={typeTagText} padding="4-8" cursor="default" />
              <Tag size="24" padding="4-8" color="grey" text="File name: CSV" cursor="default" />
            </Container>
            <Container alignitems="center" gap="32">
              <Container gap="12" alignitems="center">
                <CircledIcon size="24" background={'grey-20'} icon={<BriefcaseIcon />} />
                <Container flexdirection="column">
                  <Text type="caption-regular" color="grey-100">
                    Legal entity
                  </Text>
                  <Text type="body-medium">BetterMe Limited</Text>
                </Container>
              </Container>
              <Container gap="12" alignitems="center">
                <Logo content="user" radius="rounded" size="24" name="Vitalii Kvasha" />
                <Container flexdirection="column">
                  <Text type="caption-regular" color="grey-100">
                    Created by
                  </Text>
                  <Text type="body-medium">Vitalii Kvasha</Text>
                </Container>
              </Container>
            </Container>
          </Container>
          <IconButton
            size="32"
            icon={<ClearIcon />}
            onClick={() => {}}
            background={'inherit'}
            color="grey-100"
          />
        </Container>
      }
      footer={
        <ModalFooterNew justifycontent="space-between" background={'grey-10'} border>
          <Container alignitems="center" gap="16" flexgrow="1" justifycontent="flex-end">
            <Container flexdirection="column">
              <Text type="body-regular-14" color="grey-100">
                Step: 3 of 3
              </Text>
              <Text type="body-regular-14" color="grey-100">
                Preview mode
              </Text>
            </Container>
            <Button height="48" width="230" onClick={() => {}}>
              Import 3 rows
            </Button>
          </Container>
          <Button
            height="48"
            width="sm"
            navigation
            iconLeft={<ArrowLeftIcon />}
            background={'white-100'}
            color="black-100"
            border="grey-20-grey-90"
            onClick={() => {}}
          >
            Back
          </Button>
        </ModalFooterNew>
      }
    >
      <Container flexdirection="column" radius flexgrow="1" overflow="hidden">
        <Container flexdirection="column" className={spacing.mX32}>
          <Container className={spacing.mt32} alignitems="center" justifycontent="space-between">
            <Container alignitems="center" gap="16">
              <Text type="body-medium">Sales for importing:</Text>
              <FilterDropDown
                title="Status"
                background={'white-100'}
                value="All"
                onClear={() => {}}
                selectedAmount={0}
              >
                <Container
                  className={classNames(spacing.mY16, spacing.ml16, spacing.mr8, spacing.w296fixed)}
                  flexdirection="column"
                >
                  <FilterItemsContainer maxHeight="344" gap="0" className={spacing.mt8}>
                    <FilterDropDownItem
                      id="all"
                      type="item"
                      element="radio"
                      checked
                      blurred={false}
                      value="All"
                      onChange={() => {}}
                    />
                    <FilterDropDownItem
                      id="valid"
                      type="item"
                      element="radio"
                      checked={false}
                      blurred={false}
                      value="Valid"
                      onChange={() => {}}
                    />
                    <FilterDropDownItem
                      id="nonValid"
                      type="item"
                      element="radio"
                      checked={false}
                      blurred={false}
                      value="Non valid"
                      onChange={() => {}}
                    />
                  </FilterItemsContainer>
                </Container>
                <Divider fullHorizontalWidth />
                <Container
                  className={classNames(spacing.m16)}
                  justifycontent="flex-end"
                  alignitems="center"
                >
                  <Button
                    width="auto"
                    height="32"
                    padding="8"
                    onClick={() => {}}
                    background={'violet-90-violet-100'}
                    color="white-100"
                  >
                    Apply
                  </Button>
                </Container>
              </FilterDropDown>
            </Container>
            <Container alignitems="center">
              <Text type="text-medium" color="grey-100">
                Total rows:&nbsp;
              </Text>
              <Text type="caption-semibold">3</Text>
              <Divider
                type="vertical"
                height="20"
                background={'grey-90'}
                className={spacing.mX16}
              />
              <Text type="text-medium" color="grey-100">
                Number of valid rows:&nbsp;
              </Text>
              <Text type="caption-semibold" color="green-90">
                2
              </Text>
              <Divider
                type="vertical"
                height="20"
                background={'grey-90'}
                className={spacing.mX16}
              />
              <Text type="text-medium" color="grey-100">
                Number of non valid rows:&nbsp;
              </Text>
              <Text type="caption-semibold" color="red-90">
                0
              </Text>
            </Container>
          </Container>
        </Container>
        <Container flexdirection="column" flexgrow="1" className={spacing.mX32} overflow="hidden">
          <TableNew
            noResults={false}
            className={spacing.mt24}
            tableHead={
              <TableHeadNew radius="10">
                {tableHead.map((item, index) => {
                  if (item.marketplace && !marketplace) return null;
                  return (
                    <TableTitleNew
                      key={index}
                      minWidth={item.minWidth as TableCellWidth}
                      padding={item.padding as TableTitleNewProps['padding']}
                      align={item.align as TableTitleNewProps['align']}
                      title={item.title}
                      sorting={item.sorting}
                      onClick={() => {}}
                    />
                  );
                })}
              </TableHeadNew>
            }
          >
            <TableBodyNew>
              {Array.from(Array(3)).map((_, index) => (
                <TableRowNew background={'white'} size="auto-104" key={index} showCollapseBtn>
                  <TableCellNew
                    minWidth={tableHead[0].minWidth as TableCellWidth}
                    justifycontent="center"
                    alignitems="center"
                    className={spacing.pY22}
                    fixedWidth
                  >
                    <Checkbox name={`line-id`} checked onChange={() => {}} />
                  </TableCellNew>
                  <TableCellTextNew
                    minWidth={tableHead[1].minWidth as TableCellWidth}
                    value="ID 429"
                    type="text-regular"
                    className={spacing.pY22}
                    fixedWidth
                  />
                  <TableCellNew
                    minWidth={tableHead[2].minWidth as TableCellWidth}
                    className={spacing.pY12}
                    padding="8-0"
                    fixedWidth
                  >
                    <InputDateNew
                      name="inputdate"
                      placeholder="Date"
                      label="Date"
                      required
                      width="138"
                      height="36"
                      selected={startDay}
                      onChange={(date: Date) => setStartDay(date)}
                      calendarStartDay={1}
                      popperDirection="bottom-end"
                    />
                  </TableCellNew>
                  <TableCellNew
                    minWidth={tableHead[3].minWidth as TableCellWidth}
                    className={spacing.pY12}
                    padding="8"
                    fixedWidth
                  >
                    <SelectNew
                      name="calculations"
                      onChange={() => {}}
                      background={'grey-10'}
                      width="120"
                      height="36"
                      value={{
                        label: 'Tax inclusive',
                        name: 'taxInclusive',
                        value: 'Tax inclusive',
                      }}
                      label=""
                    >
                      {({ onClick, state, selectElement }) => (
                        <SelectDropdown selectElement={selectElement} width="320">
                          <SelectDropdownBody>
                            <SelectDropdownItemList>
                              {options.map(({ label, value }) => (
                                <OptionSingleNew
                                  key={value}
                                  label={label}
                                  height="32"
                                  selected={state?.value === value}
                                  onClick={() =>
                                    onClick({
                                      label,
                                      value,
                                    })
                                  }
                                />
                              ))}
                            </SelectDropdownItemList>
                          </SelectDropdownBody>
                        </SelectDropdown>
                      )}
                    </SelectNew>
                  </TableCellNew>
                  <TableCellNew
                    minWidth={tableHead[4].minWidth as TableCellWidth}
                    className={spacing.pY12}
                    padding="8"
                    fixedWidth
                  >
                    <SelectNew
                      name="customer"
                      onChange={() => {}}
                      background={'grey-10'}
                      width="170"
                      height="36"
                      placeholder="Choose"
                      label=""
                      error
                      errorMessage="Customer is not recognized"
                    >
                      {({ onClick, state, selectElement }) => (
                        <SelectDropdown selectElement={selectElement} width="320">
                          <SelectDropdownBody>
                            <SelectDropdownItemList>
                              {options.map(({ label, value }) => (
                                <OptionSingleNew
                                  key={value}
                                  label={label}
                                  height="32"
                                  selected={state?.value === value}
                                  onClick={() =>
                                    onClick({
                                      label,
                                      value,
                                    })
                                  }
                                />
                              ))}
                            </SelectDropdownItemList>
                          </SelectDropdownBody>
                          <SelectDropdownFooter>
                            <Container justifycontent="flex-end" className={spacing.m12}>
                              <LinkButton
                                icon={<PlusIcon />}
                                onClick={() => {}}
                                type="text-regular"
                              >
                                Create new Customer
                              </LinkButton>
                            </Container>
                          </SelectDropdownFooter>
                        </SelectDropdown>
                      )}
                    </SelectNew>
                  </TableCellNew>
                  <TableCellNew
                    minWidth={tableHead[5].minWidth as TableCellWidth}
                    className={spacing.pY12}
                    padding="8"
                    fixedWidth
                    gap="12"
                  >
                    <SelectNew
                      name="taxAgent"
                      onChange={() => {}}
                      background={'grey-10'}
                      width="110"
                      height="36"
                      label=""
                      value={{
                        label: 'No',
                        value: 'No',
                      }}
                    >
                      {({ onClick, state, selectElement }) => (
                        <SelectDropdown selectElement={selectElement} width="300">
                          <SelectDropdownBody>
                            <SelectDropdownItemList>
                              {options.map(({ label, value }) => (
                                <OptionSingleNew
                                  key={value}
                                  label={label}
                                  height="32"
                                  selected={state?.value === value}
                                  onClick={() =>
                                    onClick({
                                      label,
                                      value,
                                    })
                                  }
                                />
                              ))}
                            </SelectDropdownItemList>
                          </SelectDropdownBody>
                        </SelectDropdown>
                      )}
                    </SelectNew>
                  </TableCellNew>
                  <TableCellNew
                    minWidth={tableHead[6].minWidth as TableCellWidth}
                    className={spacing.pY12}
                    padding="8"
                    fixedWidth
                  >
                    <SelectNew
                      name="currency"
                      onChange={() => {}}
                      background={'grey-10'}
                      width="90"
                      height="36"
                      label=""
                      placeholder="Choose"
                      error
                      errorMessage="Currency is not recognized"
                    >
                      {({ onClick, state, selectElement }) => (
                        <SelectDropdown selectElement={selectElement} width="192">
                          <SelectDropdownBody>
                            <SelectDropdownItemList>
                              {options.map(({ label, value }) => (
                                <OptionSingleNew
                                  key={value}
                                  label={label}
                                  height="32"
                                  selected={state?.value === value}
                                  onClick={() =>
                                    onClick({
                                      label,
                                      value,
                                    })
                                  }
                                />
                              ))}
                            </SelectDropdownItemList>
                          </SelectDropdownBody>
                        </SelectDropdown>
                      )}
                    </SelectNew>
                  </TableCellNew>
                  <TableCellNew
                    minWidth={tableHead[7].minWidth as TableCellWidth}
                    className={spacing.pY12}
                    padding="8"
                    fixedWidth
                  >
                    <InputNew name="customerEmail" label="" width="260" height="36" />
                  </TableCellNew>
                  <TableCellNew
                    minWidth={tableHead[8].minWidth as TableCellWidth}
                    className={spacing.pY12}
                    padding="8"
                    fixedWidth
                  >
                    <InputNew name="paymentTerms" label="" width="260" height="36" />
                  </TableCellNew>
                  <TableCellNew
                    minWidth={tableHead[9].minWidth as TableCellWidth}
                    className={spacing.pY12}
                    padding="8"
                    fixedWidth
                  >
                    <InputNew name="notes" label="" width="260" height="36" />
                  </TableCellNew>
                  <TableCellNew
                    minWidth={tableHead[10].minWidth as TableCellWidth}
                    className={spacing.pY12}
                    padding="8"
                    gap="12"
                    fixedWidth
                    flexdirection="column"
                  >
                    <SelectNew
                      name="product"
                      onChange={() => {}}
                      background={'grey-10'}
                      height="36"
                      width="170"
                      label=""
                      placeholder="Choose"
                      error
                      errorMessage="Product is not recognized"
                    >
                      {({ onClick, state, selectElement }) => (
                        <SelectDropdown selectElement={selectElement} width="300">
                          <SelectDropdownBody>
                            <SelectDropdownItemList>
                              {options.map(({ label, value }) => (
                                <OptionSingleNew
                                  key={value}
                                  label={label}
                                  height="32"
                                  selected={state?.value === value}
                                  onClick={() =>
                                    onClick({
                                      label,
                                      value,
                                    })
                                  }
                                />
                              ))}
                            </SelectDropdownItemList>
                          </SelectDropdownBody>
                          <SelectDropdownFooter>
                            <Container justifycontent="flex-end" className={spacing.m12}>
                              <LinkButton
                                icon={<PlusIcon />}
                                onClick={() => {}}
                                type="text-regular"
                              >
                                Create new Product
                              </LinkButton>
                            </Container>
                          </SelectDropdownFooter>
                        </SelectDropdown>
                      )}
                    </SelectNew>
                    <SelectNew
                      name="product"
                      onChange={() => {}}
                      background={'grey-10'}
                      height="36"
                      width="170"
                      label=""
                      error
                      errorMessage="Product is not recognized"
                    >
                      {({ onClick, state, selectElement }) => (
                        <SelectDropdown selectElement={selectElement} width="300">
                          <SelectDropdownBody>
                            <SelectDropdownItemList>
                              {options.map(({ label, value }) => (
                                <OptionSingleNew
                                  key={value}
                                  label={label}
                                  height="32"
                                  selected={state?.value === value}
                                  onClick={() =>
                                    onClick({
                                      label,
                                      value,
                                    })
                                  }
                                />
                              ))}
                            </SelectDropdownItemList>
                          </SelectDropdownBody>
                        </SelectDropdown>
                      )}
                    </SelectNew>
                    <SelectNew
                      name="product"
                      onChange={() => {}}
                      background={'grey-10'}
                      height="36"
                      width="170"
                      label=""
                      error
                      errorMessage="Product is not recognized"
                    >
                      {({ onClick, state, selectElement }) => (
                        <SelectDropdown selectElement={selectElement} width="300">
                          <SelectDropdownBody>
                            <SelectDropdownItemList>
                              {options.map(({ label, value }) => (
                                <OptionSingleNew
                                  key={value}
                                  label={label}
                                  height="32"
                                  selected={state?.value === value}
                                  onClick={() =>
                                    onClick({
                                      label,
                                      value,
                                    })
                                  }
                                />
                              ))}
                            </SelectDropdownItemList>
                          </SelectDropdownBody>
                        </SelectDropdown>
                      )}
                    </SelectNew>
                  </TableCellNew>
                  <TableCellNew
                    className={spacing.pY12}
                    minWidth={tableHead[11].minWidth as TableCellWidth}
                    flexdirection="column"
                    gap="12"
                    fixedWidth
                    padding="8"
                  >
                    <NumericInput name="num-1" label="" height="36" width="80" value={12} />
                    <NumericInput name="num-2" label="" height="36" width="80" value={12} />
                    <NumericInput name="num-3" label="" height="36" width="80" value={12} />
                  </TableCellNew>
                  <TableCellNew
                    className={spacing.pY12}
                    minWidth={tableHead[12].minWidth as TableCellWidth}
                    flexdirection="column"
                    gap="12"
                    padding="8"
                    fixedWidth
                  >
                    <NumericInput
                      name="num-1"
                      label=""
                      height="36"
                      width="144"
                      value={1200}
                      suffix="$"
                    />
                    <NumericInput
                      name="num-2"
                      label=""
                      height="36"
                      width="144"
                      value={1200}
                      suffix="$"
                    />
                    <NumericInput
                      name="num-3"
                      label=""
                      height="36"
                      width="144"
                      value={1200}
                      suffix="$"
                    />
                  </TableCellNew>
                  <TableCellNew
                    className={spacing.pY12}
                    minWidth={tableHead[13].minWidth as TableCellWidth}
                    flexdirection="column"
                    padding="8"
                    gap="12"
                    fixedWidth
                  >
                    <NumericInput
                      name="num-1"
                      label=""
                      height="36"
                      width="144"
                      value={-200}
                      suffix="$"
                    />
                    <NumericInput
                      name="num-2"
                      label=""
                      height="36"
                      width="144"
                      value={-200}
                      suffix="$"
                    />
                    <NumericInput
                      name="num-3"
                      label=""
                      height="36"
                      width="144"
                      value={-200}
                      suffix="$"
                    />
                  </TableCellNew>
                  <TableCellNew
                    minWidth={tableHead[14].minWidth as TableCellWidth}
                    className={spacing.pY12}
                    padding="8"
                    flexdirection="column"
                    fixedWidth
                    gap="12"
                  >
                    <InputNew name="notes" label="Tax name" width="170" height="36" />
                    <InputNew name="notes" label="Tax name" width="170" height="36" />
                    <InputNew name="notes" label="Tax name" width="170" height="36" />
                  </TableCellNew>
                  <TableCellNew
                    minWidth={tableHead[15].minWidth as TableCellWidth}
                    className={spacing.pY12}
                    flexdirection="column"
                    padding="8"
                    gap="12"
                    fixedWidth
                  >
                    <SelectNew
                      name="taxType"
                      onChange={() => {}}
                      background={'grey-10'}
                      height="36"
                      width="150"
                      label=""
                      placeholder="Choose"
                      error
                      errorMessage="Tax type is not recognized"
                    >
                      {({ onClick, state, selectElement }) => (
                        <SelectDropdown selectElement={selectElement} width="300">
                          <SelectDropdownBody>
                            <SelectDropdownItemList>
                              {options.map(({ label, value }) => (
                                <OptionSingleNew
                                  key={value}
                                  label={label}
                                  height="32"
                                  selected={state?.value === value}
                                  onClick={() =>
                                    onClick({
                                      label,
                                      value,
                                    })
                                  }
                                />
                              ))}
                            </SelectDropdownItemList>
                          </SelectDropdownBody>
                        </SelectDropdown>
                      )}
                    </SelectNew>
                    <SelectNew
                      name="taxType"
                      onChange={() => {}}
                      background={'grey-10'}
                      height="36"
                      width="150"
                      label=""
                      placeholder="Choose"
                      error
                      errorMessage="Tax type is not recognized"
                    >
                      {({ onClick, state, selectElement }) => (
                        <SelectDropdown selectElement={selectElement} width="300">
                          <SelectDropdownBody>
                            <SelectDropdownItemList>
                              {options.map(({ label, value }) => (
                                <OptionSingleNew
                                  key={value}
                                  label={label}
                                  height="32"
                                  selected={state?.value === value}
                                  onClick={() =>
                                    onClick({
                                      label,
                                      value,
                                    })
                                  }
                                />
                              ))}
                            </SelectDropdownItemList>
                          </SelectDropdownBody>
                        </SelectDropdown>
                      )}
                    </SelectNew>
                    <SelectNew
                      name="taxType"
                      onChange={() => {}}
                      background={'grey-10'}
                      height="36"
                      width="150"
                      label=""
                      placeholder="Choose"
                      error
                      errorMessage="Tax type is not recognized"
                    >
                      {({ onClick, state, selectElement }) => (
                        <SelectDropdown selectElement={selectElement} width="300">
                          <SelectDropdownBody>
                            <SelectDropdownItemList>
                              {options.map(({ label, value }) => (
                                <OptionSingleNew
                                  key={value}
                                  label={label}
                                  height="32"
                                  selected={state?.value === value}
                                  onClick={() =>
                                    onClick({
                                      label,
                                      value,
                                    })
                                  }
                                />
                              ))}
                            </SelectDropdownItemList>
                          </SelectDropdownBody>
                        </SelectDropdown>
                      )}
                    </SelectNew>
                  </TableCellNew>
                  <TableCellNew
                    className={spacing.pY12}
                    minWidth={tableHead[16].minWidth as TableCellWidth}
                    gap="12"
                    padding="8"
                    flexdirection="column"
                    fixedWidth
                  >
                    <NumericInput
                      name="num-1"
                      label=""
                      height="36"
                      width="144"
                      value={1}
                      suffix="%"
                    />
                    <NumericInput
                      name="num-2"
                      label=""
                      height="36"
                      width="144"
                      value={1}
                      suffix="%"
                    />
                    <NumericInput
                      name="num-3"
                      label=""
                      height="36"
                      width="144"
                      value={1}
                      suffix="%"
                    />
                  </TableCellNew>
                  <TableCellNew
                    className={spacing.pY12}
                    minWidth={tableHead[17].minWidth as TableCellWidth}
                    gap="12"
                    padding="8"
                    flexdirection="column"
                    fixedWidth
                  >
                    <NumericInput
                      name="num-1"
                      label=""
                      height="36"
                      width="144"
                      value={200}
                      suffix="$"
                    />
                    <NumericInput
                      name="num-2"
                      label=""
                      height="36"
                      width="144"
                      value={200}
                      suffix="$"
                    />
                    <NumericInput
                      name="num-3"
                      label=""
                      height="36"
                      width="144"
                      value={200}
                      suffix="$"
                    />
                  </TableCellNew>
                  {marketplace && (
                    <TableCellNew
                      minWidth={tableHead[18].minWidth as TableCellWidth}
                      className={spacing.pY12}
                      padding="8"
                      gap="12"
                      fixedWidth
                      flexdirection="column"
                    >
                      <InputNew name="feeName" label="Fee Name" width="170" height="36" />
                      <InputNew name="feeName" label="Fee Name" width="170" height="36" />
                      <InputNew name="feeName" label="Fee Name" width="170" height="36" />
                    </TableCellNew>
                  )}
                  {marketplace && (
                    <TableCellNew
                      minWidth={tableHead[19].minWidth as TableCellWidth}
                      className={spacing.pY12}
                      padding="8"
                      gap="12"
                      fixedWidth
                      flexdirection="column"
                    >
                      <InputNew name="feeTypes" label="Fee Type" width="150" height="36" />
                      <InputNew name="feeTypes" label="Fee Type" width="150" height="36" />
                      <InputNew name="feeTypes" label="Fee Type" width="150" height="36" />
                    </TableCellNew>
                  )}
                  {marketplace && (
                    <TableCellNew
                      className={spacing.pY12}
                      minWidth={tableHead[20].minWidth as TableCellWidth}
                      gap="12"
                      padding="8"
                      fixedWidth
                      flexdirection="column"
                    >
                      <NumericInput
                        name="numeric-1"
                        label=""
                        height="36"
                        width="144"
                        value={1}
                        suffix="%"
                      />
                      <NumericInput
                        name="numeric-2"
                        label=""
                        height="36"
                        width="144"
                        value={1}
                        suffix="%"
                      />
                      <NumericInput
                        name="numeric-3"
                        label=""
                        height="36"
                        width="144"
                        value={1}
                        suffix="%"
                      />
                    </TableCellNew>
                  )}
                  {marketplace && (
                    <TableCellNew
                      className={spacing.pY12}
                      minWidth={tableHead[21].minWidth as TableCellWidth}
                      gap="12"
                      padding="8"
                      fixedWidth
                      flexdirection="column"
                    >
                      <NumericInput
                        name="numeric-1"
                        label=""
                        height="36"
                        width="144"
                        value={200}
                        suffix="$"
                      />
                      <NumericInput
                        name="numeric-2"
                        label=""
                        height="36"
                        width="144"
                        value={200}
                        suffix="$"
                      />
                      <NumericInput
                        name="numeric-3"
                        label=""
                        height="36"
                        width="144"
                        value={200}
                        suffix="$"
                      />
                    </TableCellNew>
                  )}
                  {marketplace && (
                    <TableCellNew
                      minWidth={tableHead[22].minWidth as TableCellWidth}
                      className={spacing.pY12}
                      padding="8"
                      gap="12"
                      fixedWidth
                    >
                      <InputNew name="buyerName" label="Вuyer name*" width="170" height="36" />
                    </TableCellNew>
                  )}
                  {marketplace && (
                    <TableCellNew
                      minWidth={tableHead[23].minWidth as TableCellWidth}
                      className={spacing.pY12}
                      padding="8"
                      gap="12"
                      fixedWidth
                    >
                      <SelectNew
                        name="buyerCurrency"
                        onChange={() => {}}
                        background={'grey-10'}
                        width="110"
                        height="36"
                        label=""
                        placeholder="Choose"
                        error
                        errorMessage="Currency is not recognized"
                      >
                        {({ onClick, state, selectElement }) => (
                          <SelectDropdown selectElement={selectElement} width="192">
                            <SelectDropdownBody>
                              <SelectDropdownItemList>
                                {options.map(({ label, value }) => (
                                  <OptionSingleNew
                                    key={value}
                                    label={label}
                                    height="32"
                                    selected={state?.value === value}
                                    onClick={() =>
                                      onClick({
                                        label,
                                        value,
                                      })
                                    }
                                  />
                                ))}
                              </SelectDropdownItemList>
                            </SelectDropdownBody>
                          </SelectDropdown>
                        )}
                      </SelectNew>
                    </TableCellNew>
                  )}
                  {marketplace && (
                    <TableCellNew
                      minWidth={tableHead[24].minWidth as TableCellWidth}
                      className={spacing.pY12}
                      padding="8"
                      gap="12"
                      fixedWidth
                    >
                      <InputNew name="buyerEmail" label="" width="270" height="36" />
                    </TableCellNew>
                  )}
                  {marketplace && (
                    <TableCellNew
                      minWidth={tableHead[25].minWidth as TableCellWidth}
                      className={spacing.pY12}
                      padding="8"
                      gap="12"
                      fixedWidth
                    >
                      <InputNew name="buyerPhone" label="" width="270" height="36" />
                    </TableCellNew>
                  )}
                  {marketplace && (
                    <TableCellNew
                      minWidth={tableHead[26].minWidth as TableCellWidth}
                      className={spacing.pY12}
                      padding="8"
                      gap="12"
                      fixedWidth
                    >
                      <InputNew
                        name="buyerBillingAddressCountry"
                        label="Text"
                        width="230"
                        height="36"
                      />
                    </TableCellNew>
                  )}
                  {marketplace && (
                    <TableCellNew
                      minWidth={tableHead[27].minWidth as TableCellWidth}
                      className={spacing.pY12}
                      padding="8"
                      gap="12"
                      fixedWidth
                    >
                      <InputNew
                        name="buyerBillingAddressState"
                        label="Text"
                        width="230"
                        height="36"
                      />
                    </TableCellNew>
                  )}
                  {marketplace && (
                    <TableCellNew
                      minWidth={tableHead[28].minWidth as TableCellWidth}
                      className={spacing.pY12}
                      padding="8"
                      gap="12"
                      fixedWidth
                    >
                      <InputNew
                        name="buyerBillingAddressCity"
                        label="Text"
                        width="230"
                        height="36"
                      />
                    </TableCellNew>
                  )}
                  {marketplace && (
                    <TableCellNew
                      minWidth={tableHead[29].minWidth as TableCellWidth}
                      className={spacing.pY12}
                      padding="8"
                      gap="12"
                      fixedWidth
                    >
                      <InputNew
                        name="buyerBillingAddressZip"
                        label="Text"
                        width="230"
                        height="36"
                      />
                    </TableCellNew>
                  )}
                  {marketplace && (
                    <TableCellNew
                      minWidth={tableHead[30].minWidth as TableCellWidth}
                      className={spacing.pY12}
                      padding="8"
                      gap="12"
                      fixedWidth
                    >
                      <InputNew
                        name="buyerShippingAddressCountry"
                        label="Text"
                        width="230"
                        height="36"
                      />
                    </TableCellNew>
                  )}
                  {marketplace && (
                    <TableCellNew
                      minWidth={tableHead[31].minWidth as TableCellWidth}
                      className={spacing.pY12}
                      padding="8"
                      gap="12"
                      fixedWidth
                    >
                      <InputNew
                        name="buyerShippingAddressState"
                        label="Text"
                        width="230"
                        height="36"
                      />
                    </TableCellNew>
                  )}
                  {marketplace && (
                    <TableCellNew
                      minWidth={tableHead[32].minWidth as TableCellWidth}
                      className={spacing.pY12}
                      padding="8"
                      gap="12"
                      fixedWidth
                    >
                      <InputNew
                        name="buyerShippingAddressCity"
                        label="Text"
                        width="230"
                        height="36"
                      />
                    </TableCellNew>
                  )}
                  {marketplace && (
                    <TableCellNew
                      minWidth={tableHead[33].minWidth as TableCellWidth}
                      className={spacing.pY12}
                      padding="8"
                      gap="12"
                      fixedWidth
                    >
                      <InputNew
                        name="buyerShippingAddressZip"
                        label="Text"
                        width="230"
                        height="36"
                      />
                    </TableCellNew>
                  )}
                  <TableCellNew
                    className={spacing.pY16}
                    minWidth={tableHead[34].minWidth as TableCellWidth}
                    justifycontent="center"
                    alignitems="center"
                    gap="12"
                    padding="8"
                    fixedWidth
                  >
                    <Tag icon={<TickIcon />} color="green" />
                  </TableCellNew>

                  <TableCellNew
                    className={spacing.pY16}
                    minWidth={tableHead[35].minWidth as TableCellWidth}
                    alignitems="center"
                    justifycontent="center"
                    gap="12"
                    padding="8"
                    fixedWidth
                  >
                    <Button
                      width="80"
                      height="24"
                      font="text-medium"
                      onClick={() => {}}
                      iconLeft={<UploadIcon />}
                      background={'blue-10-blue-20'}
                      border="blue-10-blue-20"
                      color="violet-90-violet-100"
                    >
                      Import
                    </Button>
                  </TableCellNew>
                </TableRowNew>
              ))}
            </TableBodyNew>
          </TableNew>
        </Container>
      </Container>
    </Modal>
  );
};
