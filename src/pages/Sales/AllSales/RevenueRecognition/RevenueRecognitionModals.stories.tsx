import { DropDown } from 'Components/DropDown';
import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { Checkbox } from 'Components/base/Checkbox';
import { DatePicker } from 'Components/base/DatePicker';
import { Divider } from 'Components/base/Divider';
import { Icon } from 'Components/base/Icon';
import { Radio } from 'Components/base/Radio';
import { Tag } from 'Components/base/Tag';
import { TooltipIcon } from 'Components/base/TooltipIcon';
import { Button } from 'Components/base/buttons/Button';
import { ButtonGroup } from 'Components/base/buttons/ButtonGroup';
import { CheckboxButton } from 'Components/base/buttons/CheckboxButton';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { InputDate } from 'Components/base/inputs/InputDate';
import { InputDateRange } from 'Components/base/inputs/InputDateRange';
import { InputNew } from 'Components/base/inputs/InputNew';
import { InputPasswordNew } from 'Components/base/inputs/InputPasswordNew';
import { InputPhone } from 'Components/base/inputs/InputPhone';
import { InputSKU } from 'Components/base/inputs/InputSKU';
import { InputTime } from 'Components/base/inputs/InputTime';
import { NumericInput } from 'Components/base/inputs/NumericInput';
import { OwnNumberFormatValues } from 'Components/base/inputs/NumericInput/NumericInput.types';
import { SelectNew } from 'Components/base/inputs/SelectNew';
import { OptionDoubleVerticalNew } from 'Components/base/inputs/SelectNew/options/OptionDoubleVerticalNew';
import { OptionQuadroNew } from 'Components/base/inputs/SelectNew/options/OptionQuadroNew';
import { OptionSingleNew } from 'Components/base/inputs/SelectNew/options/OptionSingleNew';
import { TextareaNew } from 'Components/base/inputs/TextareaNew';
import { CurrencyTab, CurrencyTabWrapper } from 'Components/base/tabs/CurrencyTab';
import { GeneralTab, GeneralTabWrapper } from 'Components/base/tabs/GeneralTab';
import { MenuTab, MenuTabWrapper } from 'Components/base/tabs/MenuTab';
import { Text } from 'Components/base/typography/Text';
import { AccountingCardLE } from 'Components/custom/Accounting/Journal/AccountingCardLE';
import { EntryInfoTitle } from 'Components/custom/Accounting/Journal/EntryInfoTitle';
import { UploadContainer } from 'Components/custom/Upload/UploadContainer';
import { UploadInfo } from 'Components/custom/Upload/UploadInfo';
import { UploadInput } from 'Components/custom/Upload/UploadInput';
import { UploadItem } from 'Components/custom/Upload/UploadItem';
import { Accordion } from 'Components/elements/Accordion';
import { Currencies } from 'Components/elements/Currencies';
import { PaginationButtonNew } from 'Components/elements/Pagination/PaginationButtonNew';
import { PaginationContainerNew } from 'Components/elements/Pagination/PaginationContainerNew';
import { PaginationDirectionButton } from 'Components/elements/Pagination/PaginationDirectionButton';
import { ProgressBar } from 'Components/elements/ProgressBar';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableSelect } from 'Components/elements/Table/TableSelect';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { TransactionPageSwitch } from 'Components/elements/TransactionPageSwitch';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderJournal } from 'Components/modules/Modal/elements/ModalHeaderJournal';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { ModalHeaderSales } from 'Components/modules/Modal/elements/ModalHeaderSales';
import { TableCellWidth } from 'Components/types/gridTypes';
import { options } from 'Mocks/fakeOptions';
import { ReactComponent as ArrowLeftIcon } from 'Svg/v2/16/arrow-left.svg';
import { ReactComponent as ArrowRightIcon } from 'Svg/v2/16/arrow-right.svg';
import { ReactComponent as CurrencyIcon } from 'Svg/v2/16/banknote.svg';
import { ReactComponent as CalendarIcon } from 'Svg/v2/16/calendar.svg';
import { ReactComponent as PostedIcon } from 'Svg/v2/16/clipboard-tick.svg';
import { ReactComponent as ClockIcon } from 'Svg/v2/16/clock.svg';
import { ReactComponent as ClearIcon } from 'Svg/v2/16/cross.svg';
import { ReactComponent as EyeIcon } from 'Svg/v2/16/eye.svg';
import { ReactComponent as DocumentIcon } from 'Svg/v2/16/file-blank.svg';
import { ReactComponent as CSVIcon } from 'Svg/v2/16/file-csv.svg';
import { ReactComponent as FileDownload } from 'Svg/v2/16/file-download.svg';
import { ReactComponent as DocumentLinesIcon } from 'Svg/v2/16/file-text-filled.svg';
import { ReactComponent as FileTextIcon } from 'Svg/v2/16/file-text.svg';
import { ReactComponent as XLSIcon } from 'Svg/v2/16/file-xls.svg';
import { ReactComponent as InfoIcon } from 'Svg/v2/16/info.svg';
import { ReactComponent as LinkIcon } from 'Svg/v2/16/link.svg';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as QuestionIcon } from 'Svg/v2/16/question-transparent.svg';
import { ReactComponent as SearchIcon } from 'Svg/v2/16/search.svg';
import { ReactComponent as DropdownIcon } from 'Svg/v2/16/three-dots.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import { ReactComponent as SalesAccrualTitleIcon } from 'Svg/v2/56/clipboard-currency.svg';
import { ReactComponent as CreateInvoiceTitleIcon } from 'Svg/v2/56/clipboard-list.svg';
import { checkFileType } from 'Utils/fileHandlers';
import classNames from 'classnames';
import React, { ChangeEvent, useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Sales/All Sales/Modals/Revenue Recognition',
};

export const RevenueRecognition: React.FC = () => {
  const tableHead = [
    {
      minWidth: '48',
      title: <Checkbox name="tickAll" checked={false} onChange={() => {}} />,
      padding: 'none',
    },
    {
      minWidth: '110',
      title: 'ID',
    },
    {
      minWidth: '210',
      title: 'Product',
    },
    {
      minWidth: '100',
      title: 'Occurence',
    },
    {
      minWidth: '200',
      title: 'Recognition Date',
    },
    {
      minWidth: '250',
      title: 'Income account',
    },
    {
      minWidth: '150',
      title: 'Amortization schedule, €',
      align: 'right',
      font: 'caption-bold',
      fixedWidth: true,
    },
    {
      minWidth: '150',
      title: `Amortized amount, €`,
      align: 'right',
      font: 'caption-bold',
      fixedWidth: true,
    },
    {
      minWidth: '150',
      title: 'Unamortized amount, €',
      align: 'right',
      font: 'caption-bold',
      fixedWidth: true,
    },
    {
      minWidth: '40',
    },
  ];
  const [inputValue, setInputValue] = useState<number>();
  const [startDate, setStartDate] = useState<Date | null>(null);

  const handleChange = ({ floatValue }: OwnNumberFormatValues) => {
    setInputValue(floatValue);
  };

  return (
    <Modal
      type="new-bottom"
      size="full"
      pluginScrollDisabled
      header={
        <ModalHeaderJournal
          title="Revenue Recognition"
          // infoTag={
          //   <Tag
          //     size="32"
          //     padding="8"
          //     color="grey"
          //     text="View mode"
          //     icon={<EyeIcon />}
          //     cursor="default"
          //   />
          // }
          actionTag={
            <Tag
              size="32"
              padding="8"
              color="yellow"
              text="Draft"
              icon={<DocumentIcon />}
              cursor="default"
            />
          }
          // actionTag={<Tag size="32" padding="8" color="violet" text="Posted" icon={<PostedIcon />} />}
          // action={
          //   <DropDown
          //     flexdirection="column"
          //     padding="8"
          //     gap="4"
          //     control={({ handleOpen }) => (
          //       <IconButton
          //         icon={<DropdownIcon />}
          //         onClick={handleOpen}
          //         background="white-100"
          //         color="grey-100"
          //       />
          //     )}
          //   >
          //     <DropDownButton size="160" icon={<EditIcon />} onClick={() => {}}>
          //       Edit
          //     </DropDownButton>
          //     <DropDownButton size="160" icon={<EyeIcon />} onClick={() => {}}>
          //       View source document
          //     </DropDownButton>
          //     <DropDownButton size="160" icon={<DocumentLinesIcon />} onClick={() => {}}>
          //       Bank transaction view
          //     </DropDownButton>
          //     <Divider type="horizontal" fullHorizontalWidth className={spacing.mY4} />
          //     <DropDownButton size="204" type="danger" icon={<TrashIcon />} onClick={() => {}}>
          //       Delete
          //     </DropDownButton>
          //   </DropDown>
          // }
          onClose={() => {}}
        />
      }
      footer={
        <ModalFooterNew justifycontent="flex-start" gap="24" background="grey-10">
          <Button width="220" onClick={() => {}}>
            Create
          </Button>
          <Button
            width="220"
            background="white-100"
            color="grey-100-black-100"
            border="grey-20"
            onClick={() => {}}
            navigation
          >
            Close
          </Button>
        </ModalFooterNew>
      }
      background="grey-10"
    >
      <Container
        background="white-100"
        flexgrow="1"
        className={spacing.mX24}
        radius
        flexdirection="column"
        overflow="hidden"
      >
        <GeneralTabWrapper className={classNames(spacing.mt24, spacing.ml32, spacing.mr24)}>
          <GeneralTab id="betterme" onClick={() => {}} active>
            BetterMe
          </GeneralTab>
          <GeneralTab id="headway" onClick={() => {}} active={false}>
            Headway
          </GeneralTab>
        </GeneralTabWrapper>
        <Container
          justifycontent="space-between"
          alignitems="center"
          className={classNames(spacing.mY28, spacing.ml32, spacing.mr24)}
        >
          <Container gap="32">
            <Container flexdirection="column" gap="4">
              <Text className={spacing.ml16} color="grey-100">
                Revenue schedule
              </Text>
              <SelectNew
                name="currency"
                height="36"
                // error
                onChange={() => {}}
                width="192"
                placeholder="Select"
                label="Revenue schedule"
              >
                {({ onClick, state }) => (
                  <>
                    {options.slice(0, 3).map(({ label, value }) => (
                      <OptionSingleNew
                        key={value}
                        label={label}
                        selected={state?.value === value}
                        onClick={() =>
                          onClick({
                            label,
                            value,
                          })
                        }
                      />
                    ))}
                    <Divider type="horizontal" fullHorizontalWidth className={spacing.mY4} />
                    <OptionSingleNew
                      label="Custom date"
                      icon={<CalendarIcon />}
                      selected={false}
                      onClick={() => {}}
                    />
                  </>
                )}
              </SelectNew>
            </Container>
            <Container flexdirection="column" gap="4">
              <Container alignitems="center" gap="4">
                <Icon icon={<InfoIcon />} path="grey-100" />
                <Text color="grey-100">Recognition type</Text>
              </Container>
              <Container gap="24" className={spacing.h36fixed} alignitems="center">
                <Radio id="daily" onChange={() => {}} name="recognitionType" checked>
                  Daily
                </Radio>
                <Radio id="monthly" onChange={() => {}} name="recognitionType" checked={false}>
                  Monthly
                </Radio>
              </Container>
            </Container>
          </Container>
          <Container gap="24">
            <Currencies
              selectedCurrencyValue="1"
              selectedCurrency="eur"
              baseCurrencyValue="1.01"
              baseCurrency="usd"
            />
            <CurrencyTabWrapper>
              <CurrencyTab id="usd" onClick={() => {}} active>
                USD
              </CurrencyTab>
              <CurrencyTab id="eur" onClick={() => {}} active={false}>
                EUR
              </CurrencyTab>
            </CurrencyTabWrapper>
          </Container>
        </Container>
        <Divider fullHorizontalWidth />

        <TableNew
          noResults={false}
          className={classNames(spacing.mt24, spacing.ml24, spacing.pr24)}
          tableHead={
            <TableHeadNew>
              {tableHead.map((item, index) => (
                <TableTitleNew
                  key={index}
                  minWidth={item.minWidth as TableCellWidth}
                  // flexgrow={item.flexgrow as TableTitleNewProps['flexgrow']}
                  padding={item.padding as TableTitleNewProps['padding']}
                  font={item.font as TableTitleNewProps['font']}
                  // required={item.required as TableTitleNewProps['required']}
                  fixedWidth={item.fixedWidth}
                  align={item.align as TableTitleNewProps['align']}
                  title={item.title}
                  onClick={() => {}}
                />
              ))}
            </TableHeadNew>
          }
        >
          <TableBodyNew>
            {Array.from(Array(4)).map((_, index) => (
              <TableRowNew key={index} gap="8">
                <TableCellNew minWidth="48" justifycontent="center" alignitems="center">
                  <Checkbox name={`line-${index}`} checked={false} onChange={() => {}} />
                  {/* <TooltipIcon
                    background="white-100"
                    color="grey-100"
                    icon={<InfoIcon />}
                    size="16"
                    message="Tooltip"
                  /> */}
                </TableCellNew>
                <TableCellNew minWidth="110" alignitems="center" padding="8">
                  <Container gap="8" alignitems="center">
                    <Tag padding="4" color="grey" icon={<ClockIcon />} />
                    <Text color="grey-100">Scheduled</Text>
                    {/* <Tag padding="4" color="violet" icon={<LinkIcon />} />
                    <Text color="violet-90">092341</Text> */}
                  </Container>
                </TableCellNew>
                <TableCellNew minWidth="210" alignitems="center" padding="8">
                  <TableSelect
                    name="product"
                    onChange={() => {}}
                    placeholder="Choose"
                    isRowActive={index === 2}
                    type="doubleVertical"
                    // disabled
                    // error
                  >
                    {({ onClick, state, selectElement }) => (
                      <SelectDropdown selectElement={selectElement}>
                        <SelectDropdownBody>
                          <SelectDropdownItemList>
                            {options.map(({ label, secondaryLabel, value }) => (
                              <OptionDoubleVerticalNew
                                key={value}
                                label={label}
                                secondaryLabel={secondaryLabel}
                                selected={state?.value === value}
                                onClick={() =>
                                  onClick({
                                    label,
                                    secondaryLabel,
                                    value,
                                  })
                                }
                              />
                            ))}
                          </SelectDropdownItemList>
                        </SelectDropdownBody>
                      </SelectDropdown>
                    )}
                  </TableSelect>
                </TableCellNew>
                {/* <TableCellTextNew
                  minWidth="210"
                  value="BetterMe health & fit"
                  secondaryValue="BetterMe"
                  secondaryColor="grey-100"
                  type="caption-bold"
                  secondaryType="caption-regular"
                /> */}
                <TableCellTextNew minWidth="100" value="1" type="text-regular" />
                <TableCellNew minWidth="200" padding="8">
                  {/* <InputDate
                    name="inputdate"
                    placeholder="Choose date*"
                    label="Date"
                    required
                    width="150"
                    height="36"
                    selected={null}
                    onChange={() => {}}
                    calendarStartDay={1}
                  /> */}
                  <DropDown
                    flexdirection="column"
                    padding="8"
                    control={({ handleOpen }) => (
                      <InputNew
                        width="full"
                        icon={<CalendarIcon />}
                        elementPosition="left"
                        name="date"
                        label="date"
                        placeholder="Choose date*"
                        height="36"
                        onClick={handleOpen}
                        value={startDate?.toLocaleDateString('en-US')}
                      />
                    )}
                  >
                    <Container flexdirection="column" alignitems="center" gap="16">
                      <DatePicker
                        name="calendar"
                        selected={startDate}
                        onChange={(date: Date | null) => setStartDate(date)}
                      />
                      <Divider fullHorizontalWidth />
                      <Container className={spacing.w100p} flexdirection="row-reverse">
                        <Button
                          width="auto"
                          height="32"
                          padding="8"
                          onClick={() => {}}
                          background="violet-90-violet-100"
                          color="white-100"
                        >
                          Apply
                        </Button>
                      </Container>
                    </Container>
                  </DropDown>
                </TableCellNew>
                {/* <TableCellTextNew minWidth="200" value="2021.09.25" type="text-regular" /> */}
                <TableCellNew minWidth="250" alignitems="center" padding="8">
                  <TableSelect
                    name="incomeAccount"
                    onChange={() => {}}
                    placeholder="Choose"
                    isRowActive={index === 2}
                    type="doubleHorizontal"
                    // disabled
                    // error
                  >
                    {({ onClick, state, selectElement }) => (
                      <SelectDropdown selectElement={selectElement} width="624">
                        <SelectDropdownBody>
                          <SelectDropdownItemList>
                            {options.map(
                              ({ label, secondaryLabel, tertiaryLabel, quadroLabel, value }) => (
                                <OptionQuadroNew
                                  key={value}
                                  label={label}
                                  secondaryLabel={secondaryLabel}
                                  tertiaryLabel={tertiaryLabel}
                                  quadroLabel={quadroLabel}
                                  selected={state?.value === value}
                                  onClick={() =>
                                    onClick({
                                      label,
                                      secondaryLabel,
                                      value,
                                    })
                                  }
                                />
                              ),
                            )}
                          </SelectDropdownItemList>
                        </SelectDropdownBody>
                      </SelectDropdown>
                    )}
                  </TableSelect>
                </TableCellNew>
                {/* <TableCellTextNew
                  minWidth="250"
                  value="Rental payments payable"
                  secondaryValue="Assets"
                  type="caption-regular"
                  secondaryType="caption-regular"
                  flexDirection="row"
                /> */}
                <TableCellNew
                  minWidth="150"
                  alignitems="center"
                  justifycontent="flex-end"
                  padding="0"
                >
                  <NumericInput
                    name="numeric"
                    label=""
                    width="134"
                    height="36"
                    value={inputValue}
                    onValueChange={handleChange}
                  />
                </TableCellNew>
                {/* <TableCellTextNew minWidth="150" value="1,000,000.00 €" type="caption-bold" /> */}
                <TableCellNew
                  minWidth="150"
                  alignitems="center"
                  justifycontent="flex-end"
                  padding="0"
                >
                  <NumericInput
                    name="numeric"
                    label=""
                    width="134"
                    height="36"
                    value={inputValue}
                    onValueChange={handleChange}
                  />
                </TableCellNew>
                {/* <TableCellTextNew minWidth="150" value="1,000,000.00 €" type="caption-bold" /> */}
                <TableCellNew
                  minWidth="150"
                  alignitems="center"
                  justifycontent="flex-end"
                  padding="0"
                >
                  <NumericInput
                    name="numeric"
                    label=""
                    width="134"
                    height="36"
                    value={inputValue}
                    onValueChange={handleChange}
                  />
                </TableCellNew>
                {/* <TableCellTextNew minWidth="150" value="1,000,000.00 €" type="caption-bold" /> */}
                <TableCellNew minWidth="40" padding="0">
                  <IconButton
                    icon={<TrashIcon />}
                    background="transparent"
                    color="grey-100-red-90"
                    onClick={() => {}}
                  />
                </TableCellNew>
              </TableRowNew>
            ))}

            <Container className={classNames(spacing.mt16, spacing.mr16)} justifycontent="flex-end">
              <LinkButton icon={<PlusIcon />} onClick={() => {}}>
                Add new line
              </LinkButton>
            </Container>
            <TableRowNew background="blue-10" size="28" className={spacing.mt16}>
              <TableCellNew minWidth="48" padding="0" />
              <TableCellNew minWidth="110" padding="0" />
              <TableCellNew minWidth="210" padding="0" />
              <TableCellNew minWidth="100" padding="0" />
              <TableCellNew minWidth="170" padding="0" />
              <TableCellTextNew
                minWidth="250"
                value="Subtotal"
                align="center"
                type="caption-bold"
                padding="8"
              />
              <TableCellTextNew
                minWidth="150"
                value="2,000,000.00 €"
                align="right"
                type="caption-bold"
                padding="8"
              />
              <TableCellTextNew
                minWidth="150"
                value="0.00 €"
                align="right"
                type="caption-bold"
                padding="8"
              />
              <TableCellTextNew
                minWidth="150"
                value="2,000,000.00 €"
                align="right"
                type="caption-bold"
                padding="8"
              />
              <TableCellNew minWidth="40" padding="0" />
            </TableRowNew>
          </TableBodyNew>
        </TableNew>
        {/* <ActionBar background="transparent">
          <Checkbox name="unselect" tick="unselect" checked onChange={() => {}}>
            <Text type="body-regular-14">
              Selected accounts:&nbsp;
              {1}
            </Text>
          </Checkbox>
          <ButtonGroup align="right">
            <Button
              background="transparent-grey-10"
              color="grey-100-red-90"
              border="white-100-grey-10"
              width="auto"
              height="small"
              iconLeft={<TrashIcon />}
              onClick={() => {}}
            >
              Delete
            </Button>
          </ButtonGroup>
        </ActionBar> */}
        <Divider className={spacing.mt16} fullHorizontalWidth />
        <Container
          className={classNames(spacing.p16, spacing.w100p)}
          flexdirection="row-reverse"
          justifycontent="space-between"
        >
          <Container>
            <Container
              flexdirection="column"
              className={classNames(spacing.w150fixed, spacing.pX8)}
            >
              <Text type="subtext-bold" align="right" color="grey-90">
                Total
              </Text>
              <Text type="subtext-bold" align="right" color="grey-90">
                Amortization schedule, €
              </Text>
              <Text type="body-bold-14" align="right">
                0.00
              </Text>
            </Container>
            <Container
              flexdirection="column"
              className={classNames(spacing.w150fixed, spacing.pX8)}
            >
              <Text type="subtext-bold" align="right" color="grey-90">
                Total
              </Text>
              <Text type="subtext-bold" align="right" color="grey-90">
                Amortized schedule, €
              </Text>
              <Text type="body-bold-14" align="right">
                0.00
              </Text>
            </Container>
            <Container
              flexdirection="column"
              className={classNames(spacing.w150fixed, spacing.pX8)}
            >
              <Text type="subtext-bold" align="right" color="grey-90">
                Total
              </Text>
              <Text type="subtext-bold" align="right" color="grey-90">
                Unamortized schedule, €
              </Text>
              <Text type="body-bold-14" align="right">
                0.00
              </Text>
            </Container>
          </Container>
          <Container>
            <Button
              background="transparent-blue-10"
              font="caption-semibold"
              height="40"
              color="grey-100-violet-90"
              iconLeft={<XLSIcon />}
              onClick={() => {}}
            >
              Export to XLS
            </Button>
            <Button
              background="transparent-blue-10"
              font="caption-semibold"
              height="40"
              color="grey-100-violet-90"
              iconLeft={<CSVIcon />}
              onClick={() => {}}
            >
              Export to CSV
            </Button>
          </Container>
        </Container>
        <TransactionPageSwitch
          page={1}
          totalPages={28}
          onPrevPage={() => {}}
          onNextPage={() => {}}
        />
      </Container>
    </Modal>
  );
};
