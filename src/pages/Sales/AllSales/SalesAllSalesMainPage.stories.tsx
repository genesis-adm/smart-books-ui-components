import { Meta, Story } from '@storybook/react';
import { DropDown, DropDownContext } from 'Components/DropDown';
import { Checkbox } from 'Components/base/Checkbox';
import { Divider } from 'Components/base/Divider';
import { Icon } from 'Components/base/Icon';
import { LoadingLine } from 'Components/base/LoadingLine';
import { Tag } from 'Components/base/Tag';
import { Button } from 'Components/base/buttons/Button';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { ContainerMain } from 'Components/base/grid/ContainerMain';
import { MainPageContentContainer } from 'Components/base/grid/MainPageContentContainer';
import { MenuTab, MenuTabWrapper } from 'Components/base/tabs/MenuTab';
import { Text } from 'Components/base/typography/Text';
import { MainPageEmpty } from 'Components/custom/MainPage/MainPageEmpty';
import { HeaderSB } from 'Components/custom/Stories/HeaderSB';
import { NavigationSB } from 'Components/custom/Stories/NavigationSB';
import { Pagination } from 'Components/custom/Stories/Pagination/Pagination';
import { ExportDropdown } from 'Components/elements/ExportDropdown';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { FilterDropDown } from 'Components/elements/filters/FilterDropDown';
import { FilterToggleButton } from 'Components/elements/filters/FilterToggleButton';
import { FiltersWrapper } from 'Components/elements/filters/FiltersWrapper';
import { Search } from 'Components/inputs/Search';
import type { TableCellWidth } from 'Components/types/gridTypes';
import { ReactComponent as FlashIcon } from 'Svg/v2/10/flash.svg';
import { ReactComponent as HandIcon } from 'Svg/v2/10/hand.svg';
import { ReactComponent as CaretDownIcon } from 'Svg/v2/16/caret-down.svg';
import { ReactComponent as ClipboardCurrencyIcon } from 'Svg/v2/16/clipboard-currency.svg';
import { ReactComponent as ClipboardListIcon } from 'Svg/v2/16/clipboard-list.svg';
import { ReactComponent as EyeIcon } from 'Svg/v2/16/eye.svg';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import { ReactComponent as PiechartIcon } from 'Svg/v2/16/piechart.svg';
import { ReactComponent as PlusIcon16 } from 'Svg/v2/16/plus.svg';
import { ReactComponent as SettingsIcon } from 'Svg/v2/16/settings.svg';
import { ReactComponent as DropdownIcon } from 'Svg/v2/16/three-dots.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import { ReactComponent as UndoRightRoundIcon16 } from 'Svg/v2/16/undoLeftRoundSquare.svg';
import { ReactComponent as UploadIcon } from 'Svg/v2/16/upload.svg';
import classNames from 'classnames';
import React, { FC, ReactElement, useContext, useState } from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

const SalesAllSalesMainPageComponent: Story = ({ storyState }) => {
  const [toggle, toggleMenu] = useState<boolean>(false);
  const [showFilters, setShowFilters] = useState<boolean>(false);
  const sorting = {
    sortType: null,
    tooltipText: 'Sorting tooltip text',
  };
  const tableHead = [
    {
      minWidth: '48',
      title: <Checkbox name="tickAll" checked onChange={() => {}} />,
      padding: 'none',
    },
    {
      minWidth: '150',
      title: 'ID / Date',
      sorting,
    },
    {
      minWidth: '80',
      title: 'Type',
      sorting,
      align: 'center',
    },
    {
      minWidth: '180',
      title: 'Customer',
      sorting,
    },
    {
      minWidth: '160',
      title: 'Agreement type',
      sorting,
    },
    {
      minWidth: '160',
      title: 'Legal entity',
      sorting,
    },
    { minWidth: '230', title: 'Notes' },
    {
      minWidth: '100',
      title: 'Currency',
      sorting,
      align: 'center',
    },
    {
      minWidth: '160',
      title: 'Sales CCY',
      sorting,
      align: 'right',
    },
    {
      minWidth: '166',
      title: 'Receivables CCY',
      sorting,
      align: 'right',
    },
    {
      minWidth: '100',
      title: 'Action',
      align: 'center',
    },
  ];
  const filters = [
    { title: 'Date' },
    { title: 'Type' },
    { title: 'Customer' },
    { title: 'Product' },
    { title: 'Business division' },
    { title: 'Legal entity' },
    { title: 'Status' },
    { title: 'Rules' },
    { title: 'Sales CCY' },
    { title: 'Receivables CCY' },
  ];
  return (
    <Container fullscreen flexwrap="nowrap" background={'grey-10'}>
      <NavigationSB opened={toggle} onToggleOpen={() => toggleMenu(!toggle)} />
      <ContainerMain navOpened={toggle}>
        <HeaderSB />
        <Container flexdirection="column" className={spacing.mX32}>
          <Text type="h1-semibold">Sales</Text>
          <MenuTabWrapper className={spacing.mt24}>
            <MenuTab id="allSales" active onClick={() => {}}>
              All sales
            </MenuTab>
            {/* <MenuTab id="receiveables" active={false} onClick={() => {}}>
              Receivables
            </MenuTab>
            <MenuTab id="integrations" active={false} onClick={() => {}}>
              Integrations
            </MenuTab> */}
            <MenuTab id="customers" active={false} onClick={() => {}}>
              Customers
            </MenuTab>
            <MenuTab id="products" active={false} onClick={() => {}}>
              Products
            </MenuTab>
          </MenuTabWrapper>
        </Container>
        <MainPageContentContainer>
          {storyState === 'loaded' && (
            <>
              <Container
                className={classNames(spacing.mX24, spacing.mt24)}
                justifycontent="space-between"
                gap="24"
              >
                <Container gap="24" alignitems="center">
                  <FilterToggleButton
                    showFilters={showFilters}
                    filtersSelected={false}
                    onClear={() => {}}
                    onClick={() => {
                      setShowFilters((prevState) => !prevState);
                    }}
                  />
                  <Search
                    width="320"
                    height="40"
                    name="search-banking-transactions-mainPage"
                    placeholder="Search by ID, Customer or Product"
                    value=""
                    onChange={() => {}}
                    searchParams={[]}
                    onSearch={() => {}}
                    onClear={() => {}}
                  />
                </Container>
                <Container gap="24" alignitems="center">
                  <Button
                    background={'blue-10-blue-20'}
                    color="violet-90-violet-100"
                    width="140"
                    height="40"
                    iconLeft={<PiechartIcon />}
                  >
                    Run report
                  </Button>
                  <IconButton
                    size="40"
                    background={'grey-10-grey-20'}
                    color="grey-100"
                    icon={<SettingsIcon />}
                    onClick={() => {}}
                  />
                  <Divider type="vertical" background={'grey-90'} height="20" />
                  <ExportDropdown onExportToXLS={() => {}} onExportToCSV={() => {}} />
                  <DropDown
                    flexdirection="column"
                    padding="8"
                    control={({ handleOpen }) => (
                      <>
                        <IconButton
                          mode="dropdown"
                          size="dropdown"
                          background={'grey-10-grey-20'}
                          color="grey-100"
                          icon={<UploadIcon />}
                          onClick={handleOpen}
                        />
                      </>
                    )}
                  >
                    <DropDownButton size="160" onClick={() => {}}>
                      Direct sales
                    </DropDownButton>
                    <DropDownButton size="160" onClick={() => {}}>
                      Marketplace
                    </DropDownButton>
                  </DropDown>
                  <DropDown
                    flexdirection="column"
                    padding="8"
                    control={({ handleOpen: toggleCreateButtonDropdown }) => (
                      <Button
                        onClick={toggleCreateButtonDropdown}
                        background={'violet-90-violet-100'}
                        color="white-100"
                        width="140"
                        height="40"
                        iconRight={<CaretDownIcon />}
                      >
                        Create
                      </Button>
                    )}
                  >
                    <Container
                      gap="4"
                      className={classNames(spacing.h32fixed, spacing.pl4)}
                      alignitems="center"
                    >
                      <Icon icon={<ClipboardListIcon />} path="grey-100" />
                      <Text type="caption-semibold" color="grey-100">
                        Sales accrual
                      </Text>
                    </Container>
                    <DropDownButton size="204" onClick={() => {}}>
                      Direct sales
                    </DropDownButton>
                    <DropDownButton size="204" onClick={() => {}}>
                      Marketplace
                    </DropDownButton>

                    <Divider fullHorizontalWidth className={spacing.mY8} />

                    <Container
                      gap="4"
                      className={classNames(spacing.h32fixed, spacing.pl4)}
                      alignitems="center"
                    >
                      <Icon icon={<ClipboardCurrencyIcon />} path="grey-100" />
                      <Text type="caption-semibold" color="grey-100">
                        Invoice
                      </Text>
                    </Container>
                    <DropDownButton size="204" onClick={() => {}}>
                      Direct sales
                    </DropDownButton>

                    <Divider fullHorizontalWidth className={spacing.mY8} />

                    <Container
                      gap="4"
                      className={classNames(spacing.h32fixed, spacing.pl4)}
                      alignitems="center"
                    >
                      <Icon icon={<UndoRightRoundIcon16 />} path="grey-100" />
                      <Text type="caption-semibold" color="grey-100">
                        Reversal
                      </Text>
                    </Container>

                    <ReversalDropDown />
                  </DropDown>
                </Container>
              </Container>
              <FiltersWrapper active={showFilters}>
                {filters.map((item) => (
                  <FilterDropDown
                    key={item.title}
                    title={item.title}
                    value="All"
                    onClear={() => {}}
                    selectedAmount={0}
                  />
                ))}
              </FiltersWrapper>
              <TableNew
                noResults={false}
                className={classNames(spacing.mt24, spacing.ml24, spacing.pr24)}
                tableHead={
                  <TableHeadNew>
                    {tableHead.map((item, index) => (
                      <TableTitleNew
                        key={index}
                        minWidth={item.minWidth as TableCellWidth}
                        padding={item.padding as TableTitleNewProps['padding']}
                        align={item.align as TableTitleNewProps['align']}
                        title={item.title}
                        sorting={item.sorting}
                        onClick={() => {}}
                      />
                    ))}
                  </TableHeadNew>
                }
              >
                <TableBodyNew>
                  {Array.from(Array(39)).map((_, index) => (
                    <TableRowNew key={index} gap="8">
                      <TableCellNew minWidth="48" justifycontent="center" alignitems="center">
                        <Checkbox name={`line-${index}`} checked onChange={() => {}} />
                      </TableCellNew>
                      <TableCellTextNew
                        minWidth="150"
                        value="ID 000001"
                        secondaryValue="08 September 2021"
                        secondaryColor="grey-100"
                        tagIcon={index % 2 === 0 ? <FlashIcon /> : <HandIcon />}
                        tagColor={index % 2 === 0 ? 'green' : 'grey'}
                      />
                      <TableCellNew minWidth="80" justifycontent="center" alignitems="center">
                        <Tag
                          icon={
                            index % 3 !== 0 ? (
                              index % 2 === 0 ? (
                                <ClipboardListIcon />
                              ) : (
                                <ClipboardCurrencyIcon />
                              )
                            ) : (
                              <UndoRightRoundIcon16 />
                            )
                          }
                          iconTooltip={
                            index % 3 !== 0
                              ? index % 2 === 0
                                ? 'Invoice'
                                : 'Sales Accrual'
                              : 'Reversal'
                          }
                        />
                      </TableCellNew>
                      <TableCellTextNew
                        minWidth="180"
                        value="Google Ireland Lim"
                        type="text-regular"
                      />
                      <TableCellNew minWidth="160">
                        <Tag padding="4-8" text={index % 2 ? 'Marketplace' : 'Direct sales'} />
                      </TableCellNew>
                      <TableCellTextNew
                        minWidth="160"
                        value="BetterMe Limited"
                        type="text-regular"
                      />
                      <TableCellTextNew minWidth="230" value={''} fixedWidth />
                      <TableCellTextNew
                        minWidth="100"
                        value="USD"
                        color="grey-100"
                        type="text-regular"
                        align="center"
                      />
                      <TableCellTextNew
                        minWidth="160"
                        value="$ 1,000,000.00"
                        type="caption-bold"
                        align="right"
                      />
                      <TableCellTextNew
                        minWidth="166"
                        value="$ 1,000,000.00"
                        type="caption-bold"
                        align="right"
                      />
                      <TableCellNew minWidth="100" alignitems="center" justifycontent="center">
                        <DropDown
                          flexdirection="column"
                          padding="8"
                          control={({ handleOpen }) => (
                            <IconButton
                              icon={<DropdownIcon />}
                              onClick={handleOpen}
                              background={'transparent'}
                              color="grey-100-violet-90"
                            />
                          )}
                        >
                          <DropDownButton size="204" icon={<EyeIcon />} onClick={() => {}}>
                            Open
                          </DropDownButton>
                          <DropDownButton size="204" icon={<EditIcon />} onClick={() => {}}>
                            Edit
                          </DropDownButton>
                          <DropDownButton size="204" icon={<EyeIcon />} onClick={() => {}}>
                            Open Journal entries
                          </DropDownButton>

                          {index % 3 !== 0 && (
                            <>
                              <Divider className={spacing.mY4} />

                              <DropDownButton size="204" icon={<PlusIcon16 />} onClick={() => {}}>
                                Create Chargeback
                              </DropDownButton>
                              <DropDownButton size="204" icon={<PlusIcon16 />} onClick={() => {}}>
                                Create Refund
                              </DropDownButton>
                              <DropDownButton size="204" icon={<PlusIcon16 />} onClick={() => {}}>
                                Create Reversal
                              </DropDownButton>
                            </>
                          )}

                          <Divider className={spacing.mY4} />

                          <DropDownButton
                            size="204"
                            type="danger"
                            icon={<TrashIcon />}
                            onClick={() => {}}
                          >
                            Delete
                          </DropDownButton>
                        </DropDown>
                      </TableCellNew>
                    </TableRowNew>
                  ))}
                </TableBodyNew>
              </TableNew>
              <Pagination padding="16" />
            </>
          )}
          {storyState === 'empty' && (
            <MainPageEmpty
              title="Create new sales"
              description="You have no sales created. Your sales will be displayed here."
              action={
                <>
                  <DropDown
                    flexdirection="column"
                    padding="8"
                    control={({ handleOpen }) => (
                      <Button
                        width="180"
                        height="40"
                        background={'blue-10-blue-20'}
                        color="violet-90-violet-100"
                        iconRight={<CaretDownIcon />}
                        onClick={handleOpen}
                      >
                        Create Sales
                      </Button>
                    )}
                  >
                    <DropDownButton size="204" onClick={() => {}}>
                      Direct sales
                    </DropDownButton>
                    <DropDownButton size="204" onClick={() => {}}>
                      Marketplace
                    </DropDownButton>
                  </DropDown>
                  <DropDown
                    flexdirection="column"
                    padding="8"
                    control={({ handleOpen }) => (
                      <Button
                        width="180"
                        height="40"
                        background={'blue-10-blue-20'}
                        color="violet-90-violet-100"
                        iconRight={<CaretDownIcon />}
                        onClick={handleOpen}
                      >
                        Import Sales
                      </Button>
                    )}
                  >
                    <DropDownButton size="204" onClick={() => {}}>
                      Direct sales
                    </DropDownButton>
                    <DropDownButton size="204" onClick={() => {}}>
                      Marketplace
                    </DropDownButton>
                  </DropDown>
                </>
              }
            />
          )}
        </MainPageContentContainer>
        <LoadingLine loadingCompleted={storyState !== 'loading'} />
      </ContainerMain>
    </Container>
  );
};

const ReversalDropDown: FC = (): ReactElement => {
  const { handleOpen } = useContext(DropDownContext);

  return (
    <DropDown
      flexdirection="column"
      padding="8"
      position="left-top"
      control={({ handleOpen }) => (
        <DropDownButton size="204" onClick={handleOpen} drop>
          Marketplace
        </DropDownButton>
      )}
    >
      <Text type="caption-semibold" color="grey-100" className={spacing.p8}>
        Please choose Reversal type
      </Text>
      <DropDownButton size="204" onClick={handleOpen}>
        Chargeback
      </DropDownButton>
      <DropDownButton size="204" onClick={handleOpen}>
        Refund
      </DropDownButton>
    </DropDown>
  );
};

export default {
  title: 'Pages/Sales/All Sales/Sales All Sales Main Page',
  component: SalesAllSalesMainPageComponent,
  argTypes: {
    storyState: {
      options: ['empty', 'loading', 'loaded'],
      control: {
        type: 'radio',
        labels: {
          empty: 'Empty page',
          loading: 'Page is loading',
          loaded: 'Page loaded',
        },
      },
    },
  },
} as Meta;

export const SalesAllSalesMainPage = SalesAllSalesMainPageComponent.bind({});
SalesAllSalesMainPage.args = {
  storyState: 'loaded',
};
