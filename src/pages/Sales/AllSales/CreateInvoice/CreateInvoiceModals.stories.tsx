import { Meta, Story } from '@storybook/react';
import { Radio } from 'Components/base/Radio';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { AccountingCardLE } from 'Components/custom/Accounting/Journal/AccountingCardLE';
import {
  AddFeesModal,
  AddTaxModal,
} from 'Components/custom/Sales/SalesAddRateModal/SalesAddRateModal.stories';
import AddWhtModal from 'Components/custom/Sales/SalesAddRateModal/components/AddWhtModal';
import { Pagination } from 'Components/custom/Stories/Pagination/Pagination';
import { TransactionPageSwitch } from 'Components/elements/TransactionPageSwitch';
import { Modal } from 'Components/modules/Modal';
import SalesTransactionModalBillingInfo from 'Pages/Sales/AllSales/CreateInvoice/components/SalesTransactionModalBillingInfo';
import SalesTransactionModalBuyerInfo from 'Pages/Sales/AllSales/CreateInvoice/components/SalesTransactionModalBuyerInfo';
import SalesTransactionModalCustomerInfo from 'Pages/Sales/AllSales/CreateInvoice/components/SalesTransactionModalCustomerInfo';
import SalesTransactionModalFooter from 'Pages/Sales/AllSales/CreateInvoice/components/SalesTransactionModalFooter';
import SalesTransactionModalHeader from 'Pages/Sales/AllSales/CreateInvoice/components/SalesTransactionModalHeader';
import SalesTransactionModalProductInfo from 'Pages/Sales/AllSales/CreateInvoice/components/SalesTransactionModalProductInfo';
import SalesTransactionModalShippingInfo from 'Pages/Sales/AllSales/CreateInvoice/components/SalesTransactionModalShippingInfo';
import SalesTransactionModalSummary from 'Pages/Sales/AllSales/CreateInvoice/components/SalesTransactionModalSummary';
import {
  dataByDocTypeOperation,
  reversalOperations,
} from 'Pages/Sales/AllSales/CreateInvoice/constants';
import {
  SalesTransactionAgreementType,
  SalesTransactionDocType,
  SalesTransactionDocTypeOperation,
} from 'Pages/Sales/AllSales/CreateInvoice/types';
import { ReactComponent as SearchIcon } from 'Svg/v2/16/search.svg';
import classNames from 'classnames';
import React, { useEffect, useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

type ArgsType = {
  agreementType: SalesTransactionAgreementType;
  taxAgent: boolean;
  docType: SalesTransactionDocType;
};

export default {
  title: 'Pages/Sales/All Sales/Modals/Create Invoice',
  argTypes: {
    docType: {
      name: 'Document type',
      control: 'select',
      options: ['sale', 'reversal'],
    },
    agreementType: {
      name: 'Agreement type',
      control: 'select',
      if: { arg: 'docType', neq: 'reversal' },
      options: ['direct', 'marketplace'],
    },
    taxAgent: {
      name: 'Is tax agent?',
      control: { type: 'boolean' },
    },
  },
  args: {
    docType: 'sale',
    taxAgent: true,
    agreementType: 'direct',
  },
} as Meta<ArgsType>;

export const CreateInvoiceSelectLE: React.FC = () => (
  <Modal size="1130" pluginScrollDisabled background={'white-100'}>
    <Container
      className={classNames(spacing.pt24, spacing.pX16)}
      flexdirection="column"
      flexgrow="1"
    >
      <Container justifycontent="space-between" alignitems="center">
        <Text type="title-semibold" color="black-100">
          Please choose the legal entity
        </Text>
        <IconButton
          size="32"
          icon={<SearchIcon />}
          onClick={() => {}}
          background={'grey-10-grey-20'}
          color="grey-100"
        />
      </Container>
      <Container
        className={classNames(spacing.mY32, spacing.h310)}
        customScroll
        flexgrow="1"
        gap="16"
        flexwrap="wrap"
        aligncontent="flex-start"
      >
        {Array.from(Array(9)).map((_, index) => (
          <AccountingCardLE key={index} name="BetterMe Business Holdings Corp" onClick={() => {}} />
        ))}
      </Container>
      <Pagination borderTop="default" padding="16" />
    </Container>
  </Modal>
);

export const CreateInvoice: Story<ArgsType> = ({
  agreementType: argAgreementType,
  taxAgent,
  docType,
}) => {
  const agreementType: SalesTransactionAgreementType =
    docType === 'reversal' ? 'marketplace' : argAgreementType;

  const [docTypeOperation, setDocTypeOperation] =
    useState<SalesTransactionDocTypeOperation>('chargeback');

  const [isOpenTaxesModal, setIsOpenTaxesModal] = useState<boolean>(false);
  const [isOpenFeesModal, setIsOpenFeesModal] = useState<boolean>(false);
  const [isOpenWhtModal, setIsOpenWhtModal] = useState<boolean>(false);

  const openTaxesModal = () => setIsOpenTaxesModal(true);
  const openFeesModal = () => setIsOpenFeesModal(true);
  const openWhtModal = () => setIsOpenWhtModal(true);

  const { docTitle } = dataByDocTypeOperation[docTypeOperation];

  const isReversalDocType = docType === 'reversal';
  const isMarketplace = agreementType === 'marketplace';

  useEffect(() => {
    if (isReversalDocType) {
      setDocTypeOperation('chargeback');
      return;
    }

    setDocTypeOperation('accrual');
  }, [isReversalDocType]);

  return (
    <Modal
      overlay="grey-10"
      size="full"
      background={'grey-10'}
      pluginScrollDisabled
      header={<SalesTransactionModalHeader title={docTitle} agreementType={agreementType} />}
      footer={<SalesTransactionModalFooter />}
    >
      {isOpenTaxesModal && <AddTaxModal onClose={() => setIsOpenTaxesModal(false)} />}
      {isOpenFeesModal && <AddFeesModal onClose={() => setIsOpenFeesModal(false)} />}
      {isOpenWhtModal && <AddWhtModal onClose={() => setIsOpenWhtModal(false)} />}

      {isReversalDocType && (
        <Container alignitems="center" gap="12" className={classNames(spacing.pX24, spacing.pb12)}>
          <Text type="body-regular-14" color="grey-100">
            Please choose Reversal type
          </Text>
          {reversalOperations?.map(({ id, title }) => (
            <Radio
              key={id}
              id={id}
              name={id}
              color="grey-100"
              checked={id === docTypeOperation}
              onChange={() => setDocTypeOperation(id)}
            >
              {title}
            </Radio>
          ))}
        </Container>
      )}

      <Container
        className={classNames(spacing.m24, spacing.pr12)}
        flexdirection="column"
        gap="24"
        customScroll
      >
        <Container gap="24">
          <Container flexdirection="column" flexgrow="1" gap="24">
            <SalesTransactionModalCustomerInfo />
          </Container>

          <SalesTransactionModalSummary
            docType={docType}
            isTaxAgent={taxAgent}
            docTypeOperation={docTypeOperation}
          />
        </Container>

        {isMarketplace && <SalesTransactionModalBuyerInfo />}

        <SalesTransactionModalProductInfo
          isReversal={isReversalDocType}
          isMarketplace={isMarketplace}
          onOpenTaxModal={openTaxesModal}
          onOpenFeeModal={openFeesModal}
          onOpenWHTModal={openWhtModal}
        />

        <SalesTransactionModalBillingInfo />

        <SalesTransactionModalShippingInfo />
      </Container>

      <TransactionPageSwitch
        page={1}
        totalPages={30}
        isViewMode
        onPrevPage={() => {}}
        onNextPage={() => {}}
      />
    </Modal>
  );
};
