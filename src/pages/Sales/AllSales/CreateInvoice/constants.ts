import { SalesTransactionDocTypeOperation } from 'Pages/Sales/AllSales/CreateInvoice/types';

export const taxes = [
  {
    value: 'tax inclusive',
    label: 'Tax inclusive',
  },
  {
    value: 'tax exclusive',
    label: 'Tax exclusive',
  },
];

export const reversalOnlyColumns = ['WHT'];

export const tableHeadProducts = [
  {
    minWidth: '48',
    title: '#',
  },
  {
    minWidth: '194',
    title: 'Product',
    font: 'caption-bold',
    required: true,
  },
  {
    minWidth: '160',
    title: 'QTY',
    required: true,
    align: 'center',
    padding: 'none',
  },
  {
    minWidth: '240',
    title: 'Unit Price, $',
    align: 'right',
    required: true,
  },
  {
    minWidth: '124',
    title: 'Amount, $',
    align: 'right',
  },
  {
    minWidth: '220',
    title: 'Discount, $',
    align: 'right',
  },
  {
    minWidth: '150',
    title: 'Tax rate, %',
    align: 'center',
  },
  {
    id: 'WHT',
    minWidth: '150',
    title: 'WHT, %',
    align: 'center',
  },
  {
    id: 'feeRate',
    minWidth: '150',
    title: 'Fee rate, %',
    align: 'center',
  },
  {
    minWidth: '124',
    title: 'Total, $',
    align: 'right',
    font: 'caption-bold',
  },
  {
    minWidth: '40',
  },
];

export const dataByDocTypeOperation: Record<
  SalesTransactionDocTypeOperation,
  { docTitle: string }
> = {
  accrual: {
    docTitle: 'Create Sales accrual',
  },
  chargeback: {
    docTitle: 'Create Chargeback',
  },
  refund: {
    docTitle: 'Create Refund',
  },
};

export const reversalOperations: { id: SalesTransactionDocTypeOperation; title: string }[] = [
  {
    id: 'chargeback',
    title: 'Chargeback',
  },
  {
    id: 'refund',
    title: 'Refund',
  },
];
