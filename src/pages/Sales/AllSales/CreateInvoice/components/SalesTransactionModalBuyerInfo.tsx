import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { Container } from 'Components/base/grid/Container';
import { InputNew } from 'Components/base/inputs/InputNew';
import { InputPhone } from 'Components/base/inputs/InputPhone';
import { SelectNew } from 'Components/base/inputs/SelectNew';
import { OptionSingleNew } from 'Components/base/inputs/SelectNew/options/OptionSingleNew';
import { GeneralTab, GeneralTabWrapper } from 'Components/base/tabs/GeneralTab';
import { Text } from 'Components/base/typography/Text';
import { options } from 'Mocks/fakeOptions';
import classNames from 'classnames';
import React, { FC, ReactElement } from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';

const SalesTransactionModalBuyerInfo: FC = (): ReactElement => {
  return (
    <Container
      className={classNames(spacing.p24, spacing.w100p)}
      flexdirection="column"
      background={'white-100'}
      radius
      gap="24"
      borderRadius="20"
    >
      <Container justifycontent="space-between">
        <Text type="body-medium">Buyer information</Text>
      </Container>
      <Container gap="24" flexwrap="wrap">
        <InputNew width="320" name="buyer" label="Buyer" onChange={() => {}} />
        <SelectNew name="currency" onChange={() => {}} width="192" label="Currency">
          {({ onClick, state, selectElement }) => (
            <SelectDropdown selectElement={selectElement}>
              <SelectDropdownBody>
                <SelectDropdownItemList>
                  {options.map(({ label, value }) => (
                    <OptionSingleNew
                      key={value}
                      label={label}
                      selected={state?.value === value}
                      onClick={() =>
                        onClick({
                          label,
                          value,
                        })
                      }
                    />
                  ))}
                </SelectDropdownItemList>
              </SelectDropdownBody>
            </SelectDropdown>
          )}
        </SelectNew>
        <InputNew width="320" name="email" label="Email" onChange={() => {}} />
        <InputPhone
          width="320"
          id="phone"
          value=""
          name="phone"
          label="Phone"
          onChange={() => {}}
          onBlur={() => {}}
        />
      </Container>
      <GeneralTabWrapper>
        <GeneralTab font="body-regular-14" id="billing" onClick={() => {}} active>
          Billing
        </GeneralTab>
        <GeneralTab font="body-regular-14" id="shipping" onClick={() => {}} active={false}>
          Shipping
        </GeneralTab>
      </GeneralTabWrapper>
      <Container gap="24" flexwrap="wrap">
        <SelectNew name="country" onChange={() => {}} width="192" label="Country">
          {({ onClick, state, selectElement }) => (
            <SelectDropdown selectElement={selectElement}>
              <SelectDropdownBody>
                <SelectDropdownItemList>
                  {options.map(({ label, value }) => (
                    <OptionSingleNew
                      key={value}
                      label={label}
                      selected={state?.value === value}
                      onClick={() =>
                        onClick({
                          label,
                          value,
                        })
                      }
                    />
                  ))}
                </SelectDropdownItemList>
              </SelectDropdownBody>
            </SelectDropdown>
          )}
        </SelectNew>
        <SelectNew name="state" onChange={() => {}} width="192" label="State">
          {({ onClick, state, selectElement }) => (
            <SelectDropdown selectElement={selectElement}>
              <SelectDropdownBody>
                <SelectDropdownItemList>
                  {options.map(({ label, value }) => (
                    <OptionSingleNew
                      key={value}
                      label={label}
                      selected={state?.value === value}
                      onClick={() =>
                        onClick({
                          label,
                          value,
                        })
                      }
                    />
                  ))}
                </SelectDropdownItemList>
              </SelectDropdownBody>
            </SelectDropdown>
          )}
        </SelectNew>
        <SelectNew name="city" onChange={() => {}} width="192" label="City">
          {({ onClick, state, selectElement }) => (
            <SelectDropdown selectElement={selectElement}>
              <SelectDropdownBody>
                <SelectDropdownItemList>
                  {options.map(({ label, value }) => (
                    <OptionSingleNew
                      key={value}
                      label={label}
                      selected={state?.value === value}
                      onClick={() =>
                        onClick({
                          label,
                          value,
                        })
                      }
                    />
                  ))}
                </SelectDropdownItemList>
              </SelectDropdownBody>
            </SelectDropdown>
          )}
        </SelectNew>
        <InputNew width="320" name="street" label="Street" onChange={() => {}} />
        <InputNew width="192" name="zip" label="ZIP code" onChange={() => {}} />
      </Container>
    </Container>
  );
};

export default SalesTransactionModalBuyerInfo;
