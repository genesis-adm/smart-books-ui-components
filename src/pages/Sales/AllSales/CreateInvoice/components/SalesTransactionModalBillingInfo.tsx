import { SelectDropdown } from 'Components/SelectDropdown';
import { Divider } from 'Components/base/Divider';
import { Container } from 'Components/base/grid/Container';
import { SelectNew } from 'Components/base/inputs/SelectNew';
import { OptionDoubleVerticalNew } from 'Components/base/inputs/SelectNew/options/OptionDoubleVerticalNew';
import { Text } from 'Components/base/typography/Text';
import { Accordion } from 'Components/elements/Accordion';
import { options } from 'Mocks/fakeOptions';
import { ReactComponent as Mastercard16Icon } from 'Svg/v2/16/logo-mastercard.svg';
import { ReactComponent as Mastercard24Icon } from 'Svg/v2/24/logo-mastercard.svg';
import classNames from 'classnames';
import React, { FC, ReactElement } from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';

const SalesTransactionModalBillingInfo: FC = (): ReactElement => {
  return (
    <Accordion title="Billing information" onClick={() => {}}>
      <Container
        className={classNames(spacing.p24, spacing.w100p)}
        background={'white-100'}
        radius
        gap="24"
      >
        <SelectNew
          name="depositTo"
          onChange={() => {}}
          icon={<Mastercard16Icon />}
          iconStaticColor
          width="552"
          label="Deposit to"
        >
          {({ onClick, state, selectElement }) => (
            <SelectDropdown selectElement={selectElement}>
              <Container flexdirection="column">
                <Text type="button" color="grey-100" className={spacing.mY16}>
                  Credit Card
                </Text>
                {options.slice(0, 3).map(({ label, secondaryLabel, value }) => (
                  <OptionDoubleVerticalNew
                    key={value}
                    label={label}
                    secondaryLabel={secondaryLabel}
                    icon={<Mastercard24Icon />}
                    iconStaticColor
                    selected={state?.value === value}
                    onClick={() =>
                      onClick({
                        label,
                        value,
                      })
                    }
                  />
                ))}
                <Divider type="horizontal" fullHorizontalWidth className={spacing.mt4} />
                <Text type="button" color="grey-100" className={spacing.mY16}>
                  Bank account
                </Text>
                {options.slice(0, 3).map(({ label, secondaryLabel, value }) => (
                  <OptionDoubleVerticalNew
                    key={value}
                    label={label}
                    secondaryLabel={secondaryLabel}
                    icon={<Mastercard24Icon />}
                    iconStaticColor
                    selected={state?.value === value}
                    onClick={() =>
                      onClick({
                        label,
                        value,
                      })
                    }
                  />
                ))}
                <Divider type="horizontal" fullHorizontalWidth className={spacing.mt4} />
                <Text type="button" color="grey-100" className={spacing.mY16}>
                  Wallet
                </Text>
                {options.slice(0, 3).map(({ label, secondaryLabel, value }) => (
                  <OptionDoubleVerticalNew
                    key={value}
                    label={label}
                    secondaryLabel={secondaryLabel}
                    icon={<Mastercard24Icon />}
                    iconStaticColor
                    selected={state?.value === value}
                    onClick={() =>
                      onClick({
                        label,
                        value,
                      })
                    }
                  />
                ))}
              </Container>
            </SelectDropdown>
          )}
        </SelectNew>
      </Container>
    </Accordion>
  );
};

export default SalesTransactionModalBillingInfo;
