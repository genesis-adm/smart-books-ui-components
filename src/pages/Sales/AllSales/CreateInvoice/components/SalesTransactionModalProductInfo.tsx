import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownFooter } from 'Components/SelectDropdown/components/SelectDropdownFooter';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { Button } from 'Components/base/buttons/Button';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { NumericInput } from 'Components/base/inputs/NumericInput';
import { OptionDoubleVerticalNew } from 'Components/base/inputs/SelectNew/options/OptionDoubleVerticalNew';
import { SelectPickable } from 'Components/base/inputs/SelectPickable';
import { OptionsPickable } from 'Components/base/inputs/SelectPickable/OptionsPickable';
import { Text } from 'Components/base/typography/Text';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableSelect } from 'Components/elements/Table/TableSelect';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { TableCellWidth } from 'Components/types/gridTypes';
import { options } from 'Mocks/fakeOptions';
import {
  reversalOnlyColumns,
  tableHeadProducts,
  taxes,
} from 'Pages/Sales/AllSales/CreateInvoice/constants';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import classNames from 'classnames';
import React, { FC, ReactElement, useState } from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';

type SalesTransactionModalProductInfoProps = {
  isReversal: boolean;
  isMarketplace: boolean;
  onOpenTaxModal?: () => void;
  onOpenWHTModal?: () => void;
  onOpenFeeModal?: () => void;
};

const SalesTransactionModalProductInfo: FC<SalesTransactionModalProductInfoProps> = ({
  isReversal,
  isMarketplace,
  onOpenTaxModal,
  onOpenWHTModal,
  onOpenFeeModal,
}: SalesTransactionModalProductInfoProps): ReactElement => {
  const [inputValue, setInputValue] = useState<number>(0);

  const handleChange = ({ target: { value } }: React.ChangeEvent<HTMLInputElement>) => {
    setInputValue(+value);
  };

  const filteredTableHeadProducts = tableHeadProducts?.filter(
    ({ id }) =>
      !(
        (!isMarketplace && id === 'feeRate') ||
        (!isReversal && id && reversalOnlyColumns.includes(id))
      ),
  );

  return (
    <Container
      className={classNames(spacing.p24, spacing.w100p)}
      flexdirection="column"
      background={'white-100'}
      borderRadius="20"
      gap="24"
    >
      <Container justifycontent="space-between">
        <Text type="body-medium">Product information</Text>
        <Container gap="32" alignitems="center" className={spacing.pX12}>
          <Text type="text-regular" noWrap color="grey-100">
            Amounts are
          </Text>
          <SelectPickable height="36" width="150" background={'grey-10'} defaultValue={taxes[0]}>
            {({ onClick, state }) => (
              <>
                {taxes.map(({ value, label }) => {
                  return (
                    <OptionsPickable
                      key={value}
                      label={label}
                      selected={state?.value === value}
                      onClick={() => {
                        onClick({
                          label,
                          value,
                        });
                      }}
                    />
                  );
                })}
              </>
            )}
          </SelectPickable>
        </Container>
      </Container>
      <TableNew
        noResults={false}
        className={spacing.h400fixed}
        tableHead={
          <TableHeadNew>
            {filteredTableHeadProducts.map((item, index) => (
              <TableTitleNew
                key={index}
                minWidth={item.minWidth as TableCellWidth}
                padding={item.padding as TableTitleNewProps['padding']}
                font={item.font as TableTitleNewProps['font']}
                required={item.required as TableTitleNewProps['required']}
                align={item.align as TableTitleNewProps['align']}
                title={item.title}
                onClick={() => {}}
              />
            ))}
          </TableHeadNew>
        }
      >
        <TableBodyNew>
          {Array.from(Array(4)).map((_, index) => (
            <TableRowNew key={index} active={index === 2} gap="8" className={spacing.mt8}>
              <TableCellTextNew
                minWidth={tableHeadProducts[0].minWidth as TableCellWidth}
                padding="12"
                value={`${index + 1}`}
                color="grey-100"
                type="text-regular"
                align="center"
                fixedWidth
              />
              <TableCellNew
                minWidth={tableHeadProducts[1].minWidth as TableCellWidth}
                alignitems="center"
                padding="8"
                fixedWidth
              >
                <TableSelect
                  name="product"
                  onChange={() => {}}
                  placeholder="Choose"
                  isRowActive={index === 2}
                  type="doubleVertical"
                  border="grey-20"
                >
                  {({ onClick, state, selectElement }) => (
                    <SelectDropdown selectElement={selectElement} width="300">
                      <SelectDropdownBody>
                        <SelectDropdownItemList>
                          {options.map(({ label, secondaryLabel, value }) => (
                            <OptionDoubleVerticalNew
                              key={value}
                              label={label}
                              secondaryLabel={secondaryLabel}
                              selected={state?.value === value}
                              onClick={() =>
                                onClick({
                                  label,
                                  secondaryLabel,
                                  value,
                                })
                              }
                            />
                          ))}
                        </SelectDropdownItemList>
                      </SelectDropdownBody>

                      <SelectDropdownFooter>
                        <Container justifycontent="flex-end" className={spacing.mr12}>
                          <LinkButton icon={<PlusIcon />} onClick={() => {}}>
                            Create new product
                          </LinkButton>
                        </Container>
                      </SelectDropdownFooter>
                    </SelectDropdown>
                  )}
                </TableSelect>
              </TableCellNew>
              <TableCellNew
                id="quantity"
                minWidth={tableHeadProducts[2].minWidth as TableCellWidth}
                alignitems="center"
                justifycontent="center"
                padding="8"
                flexgrow="1"
                fixedWidth
              >
                <NumericInput
                  name="num-3"
                  label=""
                  width="124"
                  height="36"
                  value={inputValue}
                  limit={isReversal ? 'negativeOnly' : 'positiveOnly'}
                  onChange={handleChange}
                />
              </TableCellNew>
              <TableCellNew
                minWidth={tableHeadProducts[3].minWidth as TableCellWidth}
                alignitems="center"
                justifycontent="flex-end"
                flexgrow="1"
                padding="8"
                fixedWidth
              >
                <NumericInput
                  name="num"
                  label=""
                  width="144"
                  height="36"
                  value={inputValue}
                  onChange={handleChange}
                />
              </TableCellNew>

              <TableCellTextNew
                id="amount"
                minWidth={tableHeadProducts[4].minWidth as TableCellWidth}
                value={isReversal ? '-0.00 $' : '0.00 $'}
                align="right"
                flexgrow="1"
                padding="8"
                alignitems="flex-end"
                justifycontent="flex-end"
                fixedWidth
              />
              <TableCellNew
                id="discount"
                minWidth={tableHeadProducts[5].minWidth as TableCellWidth}
                alignitems="flex-end"
                justifycontent="flex-end"
                padding="8"
                fixedWidth
              >
                <NumericInput
                  name="num-1"
                  label=""
                  width="144"
                  height="36"
                  value={inputValue}
                  onChange={handleChange}
                  limit={isReversal ? 'positiveOnly' : 'negativeOnly'}
                />
              </TableCellNew>
              <TableCellNew
                id="taxRate"
                minWidth={tableHeadProducts[6].minWidth as TableCellWidth}
                alignitems="center"
                justifycontent="center"
                flexgrow="1"
                padding="12"
                fixedWidth
              >
                <AddButton onClick={onOpenTaxModal} />
              </TableCellNew>
              {isReversal && (
                <TableCellNew
                  id="WHT"
                  minWidth={tableHeadProducts[7].minWidth as TableCellWidth}
                  alignitems="center"
                  justifycontent="center"
                  flexgrow="1"
                  padding="12"
                  fixedWidth
                >
                  <AddButton onClick={onOpenWHTModal} />
                </TableCellNew>
              )}
              {isMarketplace && (
                <TableCellNew
                  id="feeRate"
                  minWidth={tableHeadProducts[7].minWidth as TableCellWidth}
                  alignitems="center"
                  justifycontent="center"
                  flexgrow="1"
                  padding="8"
                  fixedWidth
                >
                  <AddButton onClick={onOpenFeeModal} />
                </TableCellNew>
              )}
              <TableCellTextNew
                id="total"
                minWidth={tableHeadProducts[8].minWidth as TableCellWidth}
                type="caption-bold"
                value={isReversal ? '-0.00 $' : '0.00 $'}
                align="right"
                flexgrow="1"
                padding="12"
                fixedWidth
              />
              <TableCellNew
                minWidth={tableHeadProducts[9].minWidth as TableCellWidth}
                padding="8"
              />
            </TableRowNew>
          ))}
          <Container
            className={classNames(spacing.mt16, spacing.mr16, spacing.mb32)}
            justifycontent="flex-end"
            flexdirection="column"
            alignitems="flex-end"
            gap="39"
          >
            <LinkButton icon={<PlusIcon />} onClick={() => {}}>
              Add new line
            </LinkButton>
            <Container justifycontent="flex-end" gap="165">
              <Text type="body-bold-14" className={spacing.mr165}>
                Subtotal, $
              </Text>
              <Text type="body-bold-14">0.00 $</Text>
            </Container>
          </Container>
        </TableBodyNew>
      </TableNew>
    </Container>
  );
};

export default SalesTransactionModalProductInfo;

type AddButtonProps = {
  onClick?: (e: React.MouseEvent<HTMLButtonElement>) => void;
};

const AddButton: FC<AddButtonProps> = ({ onClick }: AddButtonProps): ReactElement => {
  return (
    <Button
      width="50"
      background={'blue-10-transparent'}
      color="violet-90"
      iconLeft={<PlusIcon />}
      height="24"
      font="text-regular"
      onClick={onClick}
    >
      Add
    </Button>
  );
};
