import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownFooter } from 'Components/SelectDropdown/components/SelectDropdownFooter';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { Checkbox } from 'Components/base/Checkbox';
import { Divider } from 'Components/base/Divider';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { InputDate } from 'Components/base/inputs/InputDate';
import { InputNew } from 'Components/base/inputs/InputNew';
import { SelectNew } from 'Components/base/inputs/SelectNew';
import { OptionDoubleVerticalNew } from 'Components/base/inputs/SelectNew/options/OptionDoubleVerticalNew';
import { OptionSingleNew } from 'Components/base/inputs/SelectNew/options/OptionSingleNew';
import { TextareaNew } from 'Components/base/inputs/TextareaNew';
import { Text } from 'Components/base/typography/Text';
import { UploadContainer } from 'Components/custom/Upload/UploadContainer';
import { UploadInfo } from 'Components/custom/Upload/UploadInfo';
import { UploadItemField } from 'Components/custom/Upload/UploadItemField';
import { options } from 'Mocks/fakeOptions';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { checkFileType, isFileInArray } from 'Utils/fileHandlers';
import classNames from 'classnames';
import React, { FC, ReactElement, useState } from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';

const SalesTransactionModalCustomerInfo: FC = (): ReactElement => {
  const [currentValue, setCurrentValue] = useState('');

  const [checked, setChecked] = useState<boolean>(false);

  const handleCheck = () => setChecked(!checked);

  const [files, setFiles] = useState<File[]>([]);
  const handleFileUpload = (file: File) => {
    if (!isFileInArray(file, files)) {
      setFiles((prevState) => [...prevState, file]);
    }
  };
  const handleFileDelete = ({ name, size }: { name: string; size: number }) => {
    setFiles((prev) => prev.filter((file) => !(file.name === name && file.size === size)));
  };
  const handleFileDrop = (e: React.DragEvent<HTMLElement>) => {
    const fileArray = e.dataTransfer.files;
    Array.from(fileArray).forEach((file) => {
      if (checkFileType(file, ['xls', 'xlsx', 'csv']) && !isFileInArray(file, files)) {
        setFiles((prevState) => [...prevState, file]);
      }
    });
  };

  return (
    <Container
      className={spacing.p24}
      background={'white-100'}
      radius
      gap="24"
      flexdirection="column"
      borderRadius="20"
    >
      <Text type="body-medium">Customer information</Text>
      <Container gap="24" flexwrap="wrap">
        <Container gap="16">
          <SelectNew
            name="customerName"
            onChange={() => {}}
            width="672"
            label="Customer name"
            required
          >
            {({ onClick, state, selectElement }) => (
              <SelectDropdown selectElement={selectElement}>
                <SelectDropdownBody>
                  <SelectDropdownItemList>
                    {options.map(({ label, secondaryLabel, value }) => (
                      <OptionDoubleVerticalNew
                        key={value}
                        label={label}
                        secondaryLabel={secondaryLabel}
                        selected={state?.value === value}
                        onClick={() =>
                          onClick({
                            label,
                            value,
                            secondaryLabel,
                          })
                        }
                      />
                    ))}
                  </SelectDropdownItemList>
                </SelectDropdownBody>

                <SelectDropdownFooter>
                  <Container
                    justifycontent="flex-end"
                    className={classNames(spacing.mr12, spacing.pY16)}
                  >
                    <LinkButton icon={<PlusIcon />} onClick={() => {}}>
                      Create new Customer
                    </LinkButton>
                  </Container>
                </SelectDropdownFooter>
              </SelectDropdown>
            )}
          </SelectNew>
          <Checkbox
            className={spacing.w172min}
            name="taxAgent"
            checked={checked}
            onChange={handleCheck}
          >
            <Text type="body-regular-14" color="grey-100">
              Customer is tax agent
            </Text>
          </Checkbox>
        </Container>
        <Container gap="24" flexwrap="wrap">
          <SelectNew name="currency" onChange={() => {}} width="192" label="Currency" required>
            {({ onClick, state, selectElement }) => (
              <SelectDropdown selectElement={selectElement}>
                <SelectDropdownBody>
                  <SelectDropdownItemList>
                    {options.map(({ label, value }) => (
                      <OptionSingleNew
                        key={value}
                        label={label}
                        selected={state?.value === value}
                        onClick={() =>
                          onClick({
                            label,
                            value,
                          })
                        }
                      />
                    ))}
                  </SelectDropdownItemList>
                </SelectDropdownBody>
              </SelectDropdown>
            )}
          </SelectNew>
          <InputDate
            label="Invoice date"
            name="calendar"
            selected={null}
            calendarStartDay={1}
            width="192"
            onChange={() => {}}
            required
            popperDirection="bottom"
          />

          <InputNew width="320" name="email" label="Email" required onChange={() => {}} />
          <SelectNew
            name="paymentTerms"
            onChange={() => {}}
            width="192"
            label="Payment Terms"
            required
          >
            {({ onClick, state, selectElement }) => (
              <SelectDropdown selectElement={selectElement}>
                <SelectDropdownBody>
                  <SelectDropdownItemList>
                    {options.map(({ label, value }) => (
                      <OptionSingleNew
                        key={value}
                        label={label}
                        selected={state?.value === value}
                        onClick={() =>
                          onClick({
                            label,
                            value,
                          })
                        }
                      />
                    ))}
                  </SelectDropdownItemList>
                </SelectDropdownBody>
              </SelectDropdown>
            )}
          </SelectNew>
        </Container>
      </Container>
      <Divider fullHorizontalWidth />
      <Container gap="24">
        <UploadContainer onDrop={handleFileDrop}>
          <UploadItemField
            name="uploader"
            files={files}
            acceptFilesType="excel"
            limit={10}
            onChange={handleFileUpload}
            onDelete={handleFileDelete}
            onDownload={() => console.log('downloaded')}
          />
          {!files.length && (
            <UploadInfo
              mainText="Drag and drop or Choose file"
              secondaryText="XLS; XLSX; CSV; Maximum file size is 5MB"
            />
          )}
        </UploadContainer>
        <TextareaNew
          minHeight="134"
          name="notes"
          label="Notes"
          value={currentValue}
          onChange={(e) => setCurrentValue(e.target.value)}
        />
      </Container>
    </Container>
  );
};

export default SalesTransactionModalCustomerInfo;
