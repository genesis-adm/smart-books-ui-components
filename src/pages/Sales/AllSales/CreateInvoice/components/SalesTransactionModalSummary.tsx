import { Divider } from 'Components/base/Divider';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import {
  SalesTransactionDocType,
  SalesTransactionDocTypeOperation,
} from 'Pages/Sales/AllSales/CreateInvoice/types';
import classNames from 'classnames';
import React, { FC, ReactElement, ReactNode } from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';

type SalesSummaryProps = {
  docType: SalesTransactionDocType;
  isTaxAgent: boolean;
  docTypeOperation: SalesTransactionDocTypeOperation;
};

const SalesTransactionModalSummary: FC<SalesSummaryProps> = ({
  docType,
  isTaxAgent,
  docTypeOperation,
}: SalesSummaryProps): ReactElement => {
  const summaryBorderBottomStyle =
    'conic-gradient(from -67.5deg at bottom,#0000,#000 1deg 134deg,#0000 135deg) 50%/16.28px 100%';

  const isReversal = docType === 'reversal';
  const isRefund = isReversal && docTypeOperation === 'refund';

  const dataByType: Record<SalesTransactionDocType, () => ReactNode> = {
    sale: () => (
      <Container flexdirection="column" gap="8">
        <Container justifycontent="space-between">
          <Text type="body-regular-14" color="grey-100">
            Subtotal
          </Text>
          <Text type="body-regular-14" color="grey-100">
            0.00 $
          </Text>
        </Container>

        {isTaxAgent && (
          <Container justifycontent="space-between">
            <Text type="body-regular-14" color="grey-100">
              Taxes
            </Text>
            <Text type="body-regular-14" color="grey-100">
              0.00 $
            </Text>
          </Container>
        )}

        <Container justifycontent="space-between">
          <Text type="body-regular-14" color="grey-100">
            Fees
          </Text>
          <Text type="body-regular-14" color="grey-100">
            0.00 $
          </Text>
        </Container>
      </Container>
    ),

    reversal: () => (
      <Container flexdirection="column" gap="8">
        {isTaxAgent && (
          <Container justifycontent="space-between">
            <Text type="body-regular-14" color="grey-100">
              Subtotal
            </Text>
            <Text type="body-regular-14" color="grey-100">
              -0.00 $
            </Text>
          </Container>
        )}

        {isTaxAgent && (
          <Container justifycontent="space-between">
            <Text type="body-regular-14" color="grey-100">
              Taxes
            </Text>
            <Text type="body-regular-14" color="grey-100">
              0.00 $
            </Text>
          </Container>
        )}

        <Container justifycontent="space-between">
          <Text type="body-regular-14" color="grey-100">
            Chargeback
          </Text>
          <Text type="body-regular-14" color="grey-100">
            -0.00 $
          </Text>
        </Container>

        <Container justifycontent="space-between">
          <Text type="body-regular-14" color="grey-100">
            WHT Refund
          </Text>
          <Text type="body-regular-14" color="grey-100">
            0.00 $
          </Text>
        </Container>

        <Container justifycontent="space-between">
          <Text type="body-regular-14" color="grey-100">
            Fee refund
          </Text>
          <Text type="body-regular-14" color="grey-100">
            0.00 $
          </Text>
        </Container>
      </Container>
    ),
  };

  return (
    <Container
      className={classNames(spacing.w440min, spacing.p24)}
      flexdirection="column"
      background={'white-100'}
      radius
      gap="16"
      alignself="flex-start"
      style={{
        WebkitMask: summaryBorderBottomStyle,
        mask: summaryBorderBottomStyle,
      }}
    >
      <Container justifycontent="space-between">
        <Text type="title-semibold">Summary</Text>
      </Container>

      <Divider fullHorizontalWidth />

      {dataByType[docType]()}

      <Divider fullHorizontalWidth />

      <Container justifycontent="space-between">
        <Text type="title-semibold" color="grey-100">
          Total
        </Text>
        <Text type="title-semibold" color="grey-100">
          {isReversal ? '-0.00 $' : '0.00 $'}
        </Text>
      </Container>
      {!isTaxAgent && (
        <Container justifycontent="space-between">
          <Text type="body-regular-14" color="grey-100">
            incl. Taxes
          </Text>
          <Text type="body-regular-14" color="grey-100">
            {isRefund ? '-0.00 $' : '0.00 $'}
          </Text>
        </Container>
      )}
    </Container>
  );
};

export default SalesTransactionModalSummary;
