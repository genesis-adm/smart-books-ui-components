import { DropDown } from 'Components/DropDown';
import { Divider } from 'Components/base/Divider';
import { Tag } from 'Components/base/Tag';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { CurrencyTab, CurrencyTabWrapper } from 'Components/base/tabs/CurrencyTab';
import { Text } from 'Components/base/typography/Text';
import { Currencies } from 'Components/elements/Currencies';
import { Logo } from 'Components/elements/Logo';
import { CircledIcon } from 'Components/graphics/CircledIcon';
import { SalesTransactionAgreementType } from 'Pages/Sales/AllSales/CreateInvoice/types';
import { ReactComponent as BriefcaseIcon } from 'Svg/v2/12/briefcase.svg';
import { ReactComponent as CalendarFilledIcon } from 'Svg/v2/16/calendar-filled.svg';
import { ReactComponent as ClearIcon } from 'Svg/v2/16/cross.svg';
import { ReactComponent as FileAddIcon } from 'Svg/v2/16/file-add.svg';
import { ReactComponent as DocumentIcon } from 'Svg/v2/16/file-blank.svg';
import { ReactComponent as GearIcon } from 'Svg/v2/16/settings.svg';
import { ReactComponent as DropdownIcon } from 'Svg/v2/16/three-dots.svg';
import classNames from 'classnames';
import React, { FC, ReactElement, useState } from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';

type SalesTransactionModalHeaderProps = {
  agreementType: SalesTransactionAgreementType;
  title: string;
};

const SalesTransactionModalHeader: FC<SalesTransactionModalHeaderProps> = ({
  title,
  agreementType,
}: SalesTransactionModalHeaderProps): ReactElement => {
  const [customCurrencyValue, setCustomCurrencyValue] = useState<string>('1.0012');
  const [inputCurrencyValue, setInputCurrencyValue] = useState<string>(customCurrencyValue);

  const typeTagTextByDocType: Record<SalesTransactionAgreementType, string> = {
    direct: 'Direct sales',
    marketplace: 'Marketplace',
  };

  const typeTagText = typeTagTextByDocType[agreementType];

  return (
    <Container
      className={classNames(spacing.w100p, spacing.p24)}
      alignitems="center"
      justifycontent="space-between"
      flexwrap="wrap"
      style={{ borderBottom: '1px solid #EDEFF1' }}
    >
      <Container gap="24" alignitems="center">
        <Container flexdirection="column" alignitems="flex-start" gap="8">
          <Text type="title-bold">{title}</Text>
          <Tag size="24" padding="4-8" color="grey" text={typeTagText} cursor="default" />
        </Container>
        <Divider type="vertical" height="20" background={'grey-90'} />
        <Container alignitems="center" gap="32">
          <Container gap="12" alignitems="center">
            <CircledIcon size="24" background={'grey-20'} icon={<BriefcaseIcon />} />
            <Container flexdirection="column">
              <Text type="caption-regular" color="grey-100">
                Legal entity
              </Text>
              <Text type="body-medium">BetterMe Limited</Text>
            </Container>
          </Container>
          <Container gap="12" alignitems="center">
            <Logo content="user" size="24" name="Vitalii Kvasha" />
            <Container flexdirection="column">
              <Text type="caption-regular" color="grey-100">
                Created by
              </Text>
              <Text type="body-medium">Vitalii Kvasha</Text>
            </Container>
          </Container>
        </Container>
        <Divider type="vertical" height="20" background={'grey-90'} />
        <Tag
          size="32"
          icon={<DocumentIcon />}
          padding="4-8"
          cursor="default"
          text="New"
          color="green"
        />
      </Container>
      <Container gap="24" alignitems="center">
        <Currencies
          background={'white-100'}
          selectedCurrency="eur"
          selectedCurrencyValue="1"
          baseCurrency="usd"
          showCustomRate
          baseCurrencyValue={customCurrencyValue}
          baseCurrencyInputValue={inputCurrencyValue}
          onChange={(e) => {
            setInputCurrencyValue(e.target.value);
          }}
          onApply={() => setCustomCurrencyValue(inputCurrencyValue)}
        />
        <CurrencyTabWrapper background={'white-100'}>
          <CurrencyTab id="usd" onClick={() => {}} active>
            USD
          </CurrencyTab>
          <CurrencyTab id="eur" onClick={() => {}} active={false}>
            EUR
          </CurrencyTab>
        </CurrencyTabWrapper>
        <DropDown
          flexdirection="column"
          padding="8"
          gap="4"
          control={({ handleOpen }) => (
            <IconButton
              size="32"
              icon={<DropdownIcon />}
              onClick={handleOpen}
              background={'white-100'}
              color="grey-100"
            />
          )}
        >
          <DropDownButton size="204" icon={<FileAddIcon />} onClick={() => {}}>
            Save as Template
          </DropDownButton>
          <DropDownButton size="204" icon={<CalendarFilledIcon />} onClick={() => {}}>
            Recurring invoice
          </DropDownButton>
          <Divider type="horizontal" fullHorizontalWidth className={spacing.mY4} />
          <DropDownButton size="204" icon={<GearIcon />} onClick={() => {}}>
            Revenue recognition
          </DropDownButton>
        </DropDown>
        <IconButton
          size="32"
          icon={<ClearIcon />}
          onClick={() => {}}
          background={'white-100'}
          color="grey-100"
        />
      </Container>
    </Container>
  );
};

export default SalesTransactionModalHeader;
