import { Container } from 'Components/base/grid/Container';
import { InputDate } from 'Components/base/inputs/InputDate';
import { InputNew } from 'Components/base/inputs/InputNew';
import { Accordion } from 'Components/elements/Accordion';
import classNames from 'classnames';
import React, { FC, ReactElement } from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';

const SalesTransactionModalShippingInfo: FC = (): ReactElement => {
  return (
    <Accordion title="Shipping information" onClick={() => {}}>
      <Container
        className={classNames(spacing.p24, spacing.w100p)}
        background={'white-100'}
        radius
        gap="24"
      >
        <InputNew width="320" name="shippingFrom" label="Shipping from" onChange={() => {}} />
        <InputDate
          label="Shipping date"
          name="shippingDateCalendar"
          selected={null}
          calendarStartDay={1}
          width="320"
          onChange={() => {}}
          popperDirection="top"
        />
        <InputNew width="320" name="shippingAgent" label="Shipping Agent" onChange={() => {}} />
        <InputNew width="320" name="trackingNumber" label="Tracking number" onChange={() => {}} />
      </Container>
    </Accordion>
  );
};

export default SalesTransactionModalShippingInfo;
