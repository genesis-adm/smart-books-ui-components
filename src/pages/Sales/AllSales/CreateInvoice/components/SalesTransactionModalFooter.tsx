import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { ProgressBar } from 'Components/elements/ProgressBar';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ReactComponent as ArrowRightIcon } from 'Svg/v2/16/arrow-right.svg';
import { ReactComponent as CSVIcon } from 'Svg/v2/16/file-csv.svg';
import { ReactComponent as XLSIcon } from 'Svg/v2/16/file-xls.svg';
import React, { FC, ReactElement } from 'react';

const SalesTransactionModalFooter: FC = (): ReactElement => {
  return (
    <ModalFooterNew justifycontent="space-between">
      <Button height="48" width="md" navigation iconRight={<ArrowRightIcon />} onClick={() => {}}>
        Next
      </Button>
      <Container alignitems="center" gap="24">
        <ProgressBar steps={2} activeStep={1} />
        <Button
          background={'transparent-blue-10'}
          height="40"
          color="grey-100-violet-90"
          iconLeft={<XLSIcon />}
          onClick={() => {}}
        >
          Export to XLS
        </Button>
        <Button
          background={'transparent-blue-10'}
          height="40"
          color="grey-100-violet-90"
          iconLeft={<CSVIcon />}
          onClick={() => {}}
        >
          Export to CSV
        </Button>
      </Container>
    </ModalFooterNew>
  );
};

export default SalesTransactionModalFooter;
