export type SalesTransactionDocTypes = {
  sale: 'accrual';
  reversal: 'chargeback' | 'refund';
};

export type SalesTransactionDocType = keyof SalesTransactionDocTypes;

export type SalesTransactionDocTypeOperation = SalesTransactionDocTypes[SalesTransactionDocType];

export type SalesTransactionAgreementType = 'direct' | 'marketplace';
