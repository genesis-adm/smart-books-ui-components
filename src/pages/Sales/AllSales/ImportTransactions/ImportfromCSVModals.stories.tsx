import { Checkbox } from 'Components/base/Checkbox';
import { Divider } from 'Components/base/Divider';
import { Radio } from 'Components/base/Radio';
import { Button } from 'Components/base/buttons/Button';
import { ButtonGroup } from 'Components/base/buttons/ButtonGroup';
import { IconButton } from 'Components/base/buttons/IconButton';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { SelectNew } from 'Components/base/inputs/SelectNew';
import { OptionSingleNew } from 'Components/base/inputs/SelectNew/options/OptionSingleNew';
import { Text } from 'Components/base/typography/Text';
import { AccountingCardLE } from 'Components/custom/Accounting/Journal/AccountingCardLE';
import { Pagination } from 'Components/custom/Stories/Pagination/Pagination';
import { UploadContainer } from 'Components/custom/Upload/UploadContainer';
import { UploadInfo } from 'Components/custom/Upload/UploadInfo';
import { UploadItemField } from 'Components/custom/Upload/UploadItemField';
import { ProgressBar } from 'Components/elements/ProgressBar';
import { SelectImport } from 'Components/elements/SelectImport';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { options } from 'Mocks/fakeOptions';
import { ReactComponent as ArrowLeftIcon } from 'Svg/v2/16/arrow-left.svg';
import { ReactComponent as ArrowRightIcon } from 'Svg/v2/16/arrow-right.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as SearchIcon } from 'Svg/v2/16/search.svg';
import { checkFileType } from 'Utils/fileHandlers';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';
import type { TableCellWidth } from '../../../../components/types/gridTypes';

export default {
  title: 'Pages/Sales/All Sales/Modals/Import Transactions/Import from CSV',
};

export const Step0ChooseLegalEntity: React.FC = () => (
  <Modal size="1130" pluginScrollDisabled background={'white-100'} padding="none">
    <Container
      className={classNames(spacing.pt24, spacing.pX16)}
      flexdirection="column"
      flexgrow="1"
    >
      <Container justifycontent="space-between" alignitems="center">
        <Text type="title-semibold" color="black-100">
          Please choose the legal entity
        </Text>
        <IconButton
          size="32"
          icon={<SearchIcon />}
          onClick={() => {}}
          background={'grey-10-grey-20'}
          color="grey-100"
        />
        {/* <Container alignitems="center" gap="16">
          <SearchNew
            height="40"
            width="320"
            name="search"
            value=""
            placeholder="Search"
            onClear={() => {}}
            onChange={() => {}}
            searchParams={[]}
            onSearch={() => {}}
          />
          <IconButton
            size="32"
            icon={<ClearIcon />}
            onClick={() => {}}
            background="grey-10-grey-20"
            color="grey-100"
          />
        </Container> */}
      </Container>
      <Container
        className={classNames(spacing.mY32, spacing.h310)}
        customScroll
        flexgrow="1"
        gap="16"
        flexwrap="wrap"
        aligncontent="flex-start"
      >
        {Array.from(Array(9)).map((_, index) => (
          <AccountingCardLE key={index} name="BetterMe Business Holdings Corp" onClick={() => {}} />
        ))}
      </Container>
      <Pagination padding="16" />
    </Container>
  </Modal>
);

export const Step1ImportFileCSV: React.FC = () => {
  const [file, setFile] = useState<File | null>(null);
  const handleFileUpload = (item: File) => {
    setFile(item);
  };
  const handleFileDelete = () => {
    setFile(null);
  };
  const handleFileDrop = (e: React.DragEvent<HTMLElement>) => {
    const dropFile = e.dataTransfer.files[0];
    if (checkFileType(dropFile, ['csv'])) {
      setFile(e.dataTransfer.files[0]);
    }
  };
  return (
    <Modal
      size="md-extrawide"
      header={<ModalHeaderNew onClose={() => {}} />}
      pluginScrollDisabled
      footer={
        <ModalFooterNew justifycontent="space-between" border>
          <ButtonGroup align="right">
            <Button
              height="48"
              width="sm"
              background={'white-100'}
              color="black-100"
              border="grey-20-grey-90"
              onClick={() => {}}
            >
              Cancel
            </Button>
            <Button
              height="48"
              navigation
              iconRight={<ArrowRightIcon />}
              width="md"
              onClick={() => {}}
            >
              Next
            </Button>
          </ButtonGroup>
          <ProgressBar steps={3} activeStep={1} />
        </ModalFooterNew>
      }
    >
      <Container flexdirection="column" className={spacing.w624} alignitems="center">
        <Text className={spacing.mt32} type="h2-semibold" align="center">
          Import Transactions
        </Text>
        <UploadContainer className={spacing.mt48} onDrop={handleFileDrop}>
          <UploadItemField
            name="uploader"
            files={file}
            acceptFilesType="csv"
            onChange={handleFileUpload}
            onDelete={handleFileDelete}
            onReload={() => {}}
          />
          <UploadInfo
            mainText={file ? file.name : 'Drag and drop or Choose file'}
            secondaryText="CSV; Maximum file size is 5MB"
          />
        </UploadContainer>
        <Container
          className={classNames(spacing.mt20, spacing.w440)}
          flexdirection="column"
          gap="4"
        >
          <Text type="body-regular-14">Instruction for upload file:</Text>
          <Text type="body-regular-14">• All your customer information must be in one file</Text>
          <Text type="body-regular-14">
            • The top row of your file must contain a header title for each column of information
          </Text>
          <Text type="body-regular-14">
            • Product name is the only required field and have to be unique
          </Text>
        </Container>
      </Container>
    </Modal>
  );
};

export const Step2ImportTransactions: React.FC = () => (
  <Modal
    overlay="grey-10"
    size="full"
    background={'grey-10'}
    pluginScrollDisabled
    header={
      <ModalHeaderNew
        type="fullwidth"
        background={'white-100'}
        title="Import transactions"
        radius
        onClose={() => {}}
      />
    }
    footer={
      <ModalFooterNew justifycontent="space-between">
        <ButtonGroup align="right">
          <Button
            height="48"
            width="sm"
            navigation
            iconLeft={<ArrowLeftIcon />}
            background={'white-100'}
            color="black-100"
            border="grey-20-grey-90"
            onClick={() => {}}
          >
            Back
          </Button>
          <Button
            height="48"
            width="md"
            navigation
            iconRight={<ArrowRightIcon />}
            onClick={() => {}}
          >
            Next
          </Button>
        </ButtonGroup>
        <ProgressBar steps={3} activeStep={2} />
      </ModalFooterNew>
    }
  >
    <Container
      background={'white-100'}
      flexdirection="column"
      radius
      flexgrow="1"
      className={classNames(spacing.p24, spacing.mX20, spacing.mY16)}
      customScroll
    >
      <Text type="body-regular-16">General</Text>
      <Container className={spacing.mt24} gap="16" flexwrap="wrap">
        <SelectImport
          name="Customer name"
          label="Choose option"
          placeholder="Choose column from your file"
          required
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() => onClick({ label, value })}
                />
              ))}
            </>
          )}
        </SelectImport>
        <SelectImport
          name="Quantity"
          label="Choose option"
          placeholder="Choose column from your file"
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() => onClick({ label, value })}
                />
              ))}
            </>
          )}
        </SelectImport>
        <SelectImport
          name="Currency"
          label="Choose option"
          placeholder="Choose column from your file"
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() => onClick({ label, value })}
                />
              ))}
            </>
          )}
        </SelectImport>
        <SelectImport
          name="Unit price"
          label="Choose option"
          placeholder="Choose column from your file"
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() => onClick({ label, value })}
                />
              ))}
            </>
          )}
        </SelectImport>
        <SelectImport
          name="Transaction date"
          label="Choose option"
          placeholder="Choose column from your file"
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() => onClick({ label, value })}
                />
              ))}
            </>
          )}
        </SelectImport>
        <SelectImport
          name="Discount"
          label="Choose option"
          placeholder="Choose column from your file"
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() => onClick({ label, value })}
                />
              ))}
            </>
          )}
        </SelectImport>
        <SelectImport
          name="Product name"
          label="Choose option"
          placeholder="Choose column from your file"
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() => onClick({ label, value })}
                />
              ))}
            </>
          )}
        </SelectImport>
      </Container>
      <Text type="body-regular-16" className={spacing.mt32}>
        Taxes
      </Text>
      <Container className={spacing.mt24} gap="16" flexwrap="wrap">
        <Container flexdirection="column" gap="16">
          <SelectImport
            name="Taxes"
            label="Choose option"
            placeholder="Choose column from your file"
          >
            {({ onClick, state }) => (
              <>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={state?.value === value}
                    onClick={() => onClick({ label, value })}
                  />
                ))}
              </>
            )}
          </SelectImport>
        </Container>
        <Container gap="24" className={spacing.h48fixed}>
          <Radio id="taxInclusive" onChange={() => {}} name="taxesRadio" color="grey-100" checked>
            Tax inclusive
          </Radio>
          <Radio
            id="taxExclusive"
            onChange={() => {}}
            name="taxesRadio"
            color="grey-100"
            checked={false}
          >
            Tax exclusive
          </Radio>
        </Container>
      </Container>
      <Text type="body-regular-16" className={spacing.mt32}>
        Fee 1
      </Text>
      <Container className={spacing.mt24} gap="16" flexwrap="wrap">
        <Container flexdirection="column" gap="16">
          <SelectImport
            name="Fee name"
            label="Choose option"
            placeholder="Choose column from your file"
          >
            {({ onClick, state }) => (
              <>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={state?.value === value}
                    onClick={() => onClick({ label, value })}
                  />
                ))}
              </>
            )}
          </SelectImport>
          <SelectImport
            name="Fee amount"
            label="Choose option"
            placeholder="Choose column from your file"
          >
            {({ onClick, state }) => (
              <>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={state?.value === value}
                    onClick={() => onClick({ label, value })}
                  />
                ))}
              </>
            )}
          </SelectImport>
          <SelectImport
            name="Revenue schedule"
            label="Choose option"
            placeholder="Choose column from your file"
          >
            {({ onClick, state }) => (
              <>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={state?.value === value}
                    onClick={() => onClick({ label, value })}
                  />
                ))}
              </>
            )}
          </SelectImport>
        </Container>
        <Container gap="24" className={spacing.h48fixed}>
          <Radio id="feeInclusive" onChange={() => {}} name="fee1Radio" color="grey-100" checked>
            Fee inclusive
          </Radio>
          <Radio
            id="feeExclusive"
            onChange={() => {}}
            name="fee1Radio"
            color="grey-100"
            checked={false}
          >
            Fee exclusive
          </Radio>
        </Container>
      </Container>
      <Text type="body-regular-16" className={spacing.mt32}>
        Fee 2
      </Text>
      <Container className={spacing.mt24} gap="16" flexwrap="wrap">
        <Container flexdirection="column" gap="16">
          <SelectImport
            name="Fee name"
            label="Choose option"
            placeholder="Choose column from your file"
          >
            {({ onClick, state }) => (
              <>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={state?.value === value}
                    onClick={() => onClick({ label, value })}
                  />
                ))}
              </>
            )}
          </SelectImport>
          <SelectImport
            name="Fee amount"
            label="Choose option"
            placeholder="Choose column from your file"
          >
            {({ onClick, state }) => (
              <>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={state?.value === value}
                    onClick={() => onClick({ label, value })}
                  />
                ))}
              </>
            )}
          </SelectImport>
          <SelectImport
            name="Revenue schedule"
            label="Choose option"
            placeholder="Choose column from your file"
          >
            {({ onClick, state }) => (
              <>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={state?.value === value}
                    onClick={() => onClick({ label, value })}
                  />
                ))}
              </>
            )}
          </SelectImport>
        </Container>
        <Container gap="24" className={spacing.h48fixed}>
          <Radio id="feeInclusive" onChange={() => {}} name="fee2Radio" color="grey-100" checked>
            Fee inclusive
          </Radio>
          <Radio
            id="feeExclusive"
            onChange={() => {}}
            name="fee2Radio"
            color="grey-100"
            checked={false}
          >
            Fee exclusive
          </Radio>
        </Container>
      </Container>
      <Container className={spacing.mt16}>
        <LinkButton icon={<PlusIcon />} onClick={() => {}}>
          Add other fee
        </LinkButton>
      </Container>
      <Text type="body-regular-16" className={spacing.mt32}>
        Buyer information
      </Text>
      <Container className={spacing.mt24} gap="16" flexwrap="wrap">
        <SelectImport
          name="Name"
          label="Choose option"
          placeholder="Choose column from your file"
          required
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() => onClick({ label, value })}
                />
              ))}
            </>
          )}
        </SelectImport>
        <SelectImport name="Email" label="Choose option" placeholder="Choose column from your file">
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() => onClick({ label, value })}
                />
              ))}
            </>
          )}
        </SelectImport>
        <SelectImport
          name="Currency"
          label="Choose option"
          placeholder="Choose column from your file"
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() => onClick({ label, value })}
                />
              ))}
            </>
          )}
        </SelectImport>
        <SelectImport name="Phone" label="Choose option" placeholder="Choose column from your file">
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() => onClick({ label, value })}
                />
              ))}
            </>
          )}
        </SelectImport>
      </Container>
      <Container className={spacing.mt32} gap="16">
        <Container flexdirection="column" gap="16">
          <Text type="body-regular-16" className={spacing.mb8}>
            Buyer Billing address
          </Text>
          <SelectImport
            name="Country"
            label="Choose option"
            placeholder="Choose column from your file"
            required
          >
            {({ onClick, state }) => (
              <>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={state?.value === value}
                    onClick={() => onClick({ label, value })}
                  />
                ))}
              </>
            )}
          </SelectImport>
          <SelectImport
            name="State"
            label="Choose option"
            placeholder="Choose column from your file"
            required
          >
            {({ onClick, state }) => (
              <>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={state?.value === value}
                    onClick={() => onClick({ label, value })}
                  />
                ))}
              </>
            )}
          </SelectImport>
          <SelectImport
            name="City"
            label="Choose option"
            placeholder="Choose column from your file"
          >
            {({ onClick, state }) => (
              <>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={state?.value === value}
                    onClick={() => onClick({ label, value })}
                  />
                ))}
              </>
            )}
          </SelectImport>
          <SelectImport
            name="Street"
            label="Choose option"
            placeholder="Choose column from your file"
          >
            {({ onClick, state }) => (
              <>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={state?.value === value}
                    onClick={() => onClick({ label, value })}
                  />
                ))}
              </>
            )}
          </SelectImport>
          <SelectImport
            name="Zip code"
            label="Choose option"
            placeholder="Choose column from your file"
          >
            {({ onClick, state }) => (
              <>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={state?.value === value}
                    onClick={() => onClick({ label, value })}
                  />
                ))}
              </>
            )}
          </SelectImport>
        </Container>
        <Container flexdirection="column" gap="16">
          <Text type="body-regular-16" className={spacing.mb8}>
            Buyer Shipping address
          </Text>
          <SelectImport
            name="Country"
            label="Choose option"
            placeholder="Choose column from your file"
            required
          >
            {({ onClick, state }) => (
              <>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={state?.value === value}
                    onClick={() => onClick({ label, value })}
                  />
                ))}
              </>
            )}
          </SelectImport>
          <SelectImport
            name="State"
            label="Choose option"
            placeholder="Choose column from your file"
            required
          >
            {({ onClick, state }) => (
              <>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={state?.value === value}
                    onClick={() => onClick({ label, value })}
                  />
                ))}
              </>
            )}
          </SelectImport>
          <SelectImport
            name="City"
            label="Choose option"
            placeholder="Choose column from your file"
          >
            {({ onClick, state }) => (
              <>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={state?.value === value}
                    onClick={() => onClick({ label, value })}
                  />
                ))}
              </>
            )}
          </SelectImport>
          <SelectImport
            name="Street"
            label="Choose option"
            placeholder="Choose column from your file"
          >
            {({ onClick, state }) => (
              <>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={state?.value === value}
                    onClick={() => onClick({ label, value })}
                  />
                ))}
              </>
            )}
          </SelectImport>
          <SelectImport
            name="Zip code"
            label="Choose option"
            placeholder="Choose column from your file"
          >
            {({ onClick, state }) => (
              <>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={state?.value === value}
                    onClick={() => onClick({ label, value })}
                  />
                ))}
              </>
            )}
          </SelectImport>
        </Container>
      </Container>
    </Container>
  </Modal>
);

export const Step3ImportTransactions: React.FC = () => {
  const sorting = {
    sortType: null,
    tooltipText: 'Sorting tooltip text',
  };
  const tableHead = [
    {
      minWidth: '48',
      title: <Checkbox name="tickAll" checked onChange={() => {}} />,
      padding: 'none',
    },
    { minWidth: '220', title: 'Customer', sorting },
    { minWidth: '100', title: 'Currency', sorting, align: 'center' },
    { minWidth: '150', title: 'Transaction date', sorting },
    { minWidth: '220', title: 'Product name', sorting },
    { minWidth: '80', title: 'Q-ty', sorting },
    { minWidth: '130', title: 'Unit price', sorting, align: 'right' },
    { minWidth: '110', title: 'Discount', sorting, align: 'right' },
    { minWidth: '130', title: 'Taxes', sorting, align: 'right' },
    { minWidth: '130', title: 'Tax type', sorting },
    { minWidth: '140', title: 'Fee name', sorting },
    { minWidth: '150', title: 'Fee amount', sorting, align: 'right' },
    { minWidth: '130', title: 'Fee type', sorting },
    { minWidth: '170', title: 'Revenue schedule', sorting, align: 'right' },
    { minWidth: '180', title: 'Name (Buyer)', sorting },
    { minWidth: '180', title: 'Currency (Buyer)', sorting },
    { minWidth: '180', title: 'Email (Buyer)', sorting },
    { minWidth: '180', title: 'Phone (Buyer)', sorting },
    { minWidth: '230', title: 'Country (Buyer Billing address)', sorting },
    { minWidth: '230', title: 'State (Buyer Billing address)', sorting },
    { minWidth: '230', title: 'City (Buyer Billing address)', sorting },
    { minWidth: '230', title: 'Street (Buyer Billing address)', sorting },
    { minWidth: '230', title: 'Zip Code (Buyer Billing address)', sorting },
    { minWidth: '240', title: 'Country (Buyer Shipping address)', sorting },
    { minWidth: '230', title: 'State (Buyer Shipping address)', sorting },
    { minWidth: '230', title: 'City (Buyer Shipping address)', sorting },
    { minWidth: '230', title: 'Street (Buyer Shipping address)', sorting },
    { minWidth: '250', title: 'Zip Code (Buyer Shipping address)', sorting },
  ];

  return (
    <Modal
      overlay="grey-10"
      size="full"
      background={'grey-10'}
      pluginScrollDisabled
      header={
        <ModalHeaderNew
          type="fullwidth"
          background={'white-100'}
          title="Import transactions"
          radius
          onClose={() => {}}
        />
      }
      footer={
        <ModalFooterNew justifycontent="space-between">
          <ButtonGroup align="right">
            <Button
              height="48"
              width="sm"
              navigation
              iconLeft={<ArrowLeftIcon />}
              background={'white-100'}
              color="black-100"
              border="grey-20-grey-90"
              onClick={() => {}}
            >
              Back
            </Button>
            <Button height="48" width="md" onClick={() => {}}>
              Import Transactions
            </Button>
          </ButtonGroup>
          <ProgressBar steps={3} activeStep={3} />
        </ModalFooterNew>
      }
    >
      <Container flexdirection="column" radius flexgrow="1" overflow="hidden">
        <Container flexdirection="column" className={spacing.mX32}>
          <Text type="body-medium" className={spacing.mt32}>
            Select customers to import:
          </Text>
          <Container className={spacing.mt12} alignitems="center" justifycontent="space-between">
            <Container alignitems="center">
              <Text type="text-regular" color="grey-100">
                Organization:&nbsp;
              </Text>
              <Text type="caption-regular">GENESIS</Text>
              <Divider
                type="vertical"
                height="20"
                background={'grey-90'}
                className={spacing.mX16}
              />
              <Text type="text-regular" color="grey-100">
                Legan entity:&nbsp;
              </Text>
              <Text type="caption-regular">GENESIS</Text>
            </Container>
            <Container alignitems="center">
              <Text type="text-medium" color="grey-100">
                Number of customers:&nbsp;
              </Text>
              <Text type="caption-semibold">145</Text>
              <Divider
                type="vertical"
                height="20"
                background={'grey-90'}
                className={spacing.mX16}
              />
              <Text type="text-medium" color="grey-100">
                Selected:&nbsp;
              </Text>
              <Text type="caption-semibold">145</Text>
            </Container>
          </Container>
        </Container>
        <Container flexdirection="column" flexgrow="1" className={spacing.mX32} overflow="hidden">
          <TableNew
            noResults={false}
            className={spacing.mt24}
            tableHead={
              <TableHeadNew>
                {tableHead.map((item, index) => (
                  <TableTitleNew
                    key={index}
                    minWidth={item.minWidth as TableCellWidth}
                    // flexgrow={item.flexgrow as TableTitleNewProps['flexgrow']}
                    padding={item.padding as TableTitleNewProps['padding']}
                    align={item.align as TableTitleNewProps['align']}
                    title={item.title}
                    sorting={item.sorting}
                    onClick={() => {}}
                  />
                ))}
              </TableHeadNew>
            }
          >
            <TableBodyNew>
              {Array.from(Array(39)).map((_, index) => (
                <TableRowNew key={index}>
                  <TableCellNew minWidth="48" justifycontent="center" alignitems="center">
                    <Checkbox name={`line-${index}`} checked onChange={() => {}} />
                  </TableCellNew>
                  {/* <TableCellTextNew minWidth="220" value="Customer value" /> */}
                  <TableCellNew minWidth="220" padding="0">
                    <SelectNew
                      name="customer"
                      onChange={() => {}}
                      placeholder="Choose Customer"
                      label="Choose Customer"
                      width="220"
                      height="36"
                      error
                    >
                      {({ onClick, state }) => (
                        <>
                          {options.map(({ label, value }) => (
                            <OptionSingleNew
                              key={value}
                              label={label}
                              selected={state?.value === value}
                              onClick={() => onClick({ label, value })}
                            />
                          ))}
                        </>
                      )}
                    </SelectNew>
                  </TableCellNew>
                  <TableCellTextNew minWidth="100" value="USD" align="center" />
                  <TableCellTextNew minWidth="150" value="08 July 2021" />
                  {/* <TableCellTextNew minWidth="220" value="Product name value" /> */}
                  <TableCellNew minWidth="220" padding="0">
                    <SelectNew
                      name="product"
                      onChange={() => {}}
                      placeholder="Choose Product"
                      label="Choose Product"
                      width="220"
                      height="36"
                      error
                    >
                      {({ onClick, state }) => (
                        <>
                          {options.map(({ label, value }) => (
                            <OptionSingleNew
                              key={value}
                              label={label}
                              selected={state?.value === value}
                              onClick={() => onClick({ label, value })}
                            />
                          ))}
                        </>
                      )}
                    </SelectNew>
                  </TableCellNew>
                  <TableCellTextNew minWidth="80" value="12" />
                  <TableCellTextNew minWidth="130" value="121$" align="right" />
                  <TableCellTextNew minWidth="110" value="122$" align="right" />
                  <TableCellTextNew minWidth="130" value="123$" align="right" />
                  <TableCellTextNew minWidth="130" value="Tax exclusive" />
                  <TableCellTextNew minWidth="140" value="Fee name" />
                  <TableCellTextNew minWidth="150" value="124$" align="right" />
                  <TableCellTextNew minWidth="130" value="Tax exclusive" />
                  <TableCellTextNew minWidth="170" value="125$" align="right" />
                  <TableCellTextNew minWidth="180" value="Name (Buyer) value" />
                  <TableCellTextNew minWidth="180" value="Currency (Buyer) value" />
                  <TableCellTextNew minWidth="180" value="Email (Buyer) value" />
                  <TableCellTextNew minWidth="180" value="Phone (Buyer) value" />
                  <TableCellTextNew minWidth="230" value="Country (Buyer Billing address)" />
                  <TableCellTextNew minWidth="230" value="State (Buyer Billing address)" />
                  <TableCellTextNew minWidth="230" value="City (Buyer Billing address)" />
                  <TableCellTextNew minWidth="230" value="Street (Buyer Billing address)" />
                  <TableCellTextNew minWidth="230" value="Zip Code (Buyer Billing address)" />
                  <TableCellTextNew minWidth="240" value="Country (Buyer Shipping address)" />
                  <TableCellTextNew minWidth="230" value="State (Buyer Shipping address)" />
                  <TableCellTextNew minWidth="230" value="City (Buyer Shipping address)" />
                  <TableCellTextNew minWidth="230" value="Street (Buyer Shipping address)" />
                  <TableCellTextNew minWidth="250" value="Zip Code (Buyer Shipping address)" />
                </TableRowNew>
              ))}
            </TableBodyNew>
          </TableNew>
        </Container>
      </Container>
    </Modal>
  );
};
