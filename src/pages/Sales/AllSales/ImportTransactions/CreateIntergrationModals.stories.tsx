import { DropDown } from 'Components/DropDown';
import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownFooter } from 'Components/SelectDropdown/components/SelectDropdownFooter';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { Checkbox } from 'Components/base/Checkbox';
import { Icon } from 'Components/base/Icon';
import { Tag } from 'Components/base/Tag';
import { Tooltip } from 'Components/base/Tooltip';
import { Button } from 'Components/base/buttons/Button';
import { IconButton } from 'Components/base/buttons/IconButton';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { InputNew } from 'Components/base/inputs/InputNew';
import { SelectNew } from 'Components/base/inputs/SelectNew';
import { OptionDoubleVerticalNew } from 'Components/base/inputs/SelectNew/options/OptionDoubleVerticalNew';
import { OptionSingleNew } from 'Components/base/inputs/SelectNew/options/OptionSingleNew';
import { GeneralTab, GeneralTabWrapper } from 'Components/base/tabs/GeneralTab';
import { Text } from 'Components/base/typography/Text';
import { AssignProductTitle } from 'Components/custom/Sales/AllSales/AssignProductTitle';
import { ImportProcessorType } from 'Components/custom/Sales/AllSales/ImportProcessorType';
import { PrivateKeyExample } from 'Components/custom/Sales/AllSales/PrivateKeyExample';
import { Pagination } from 'Components/custom/Stories/Pagination/Pagination';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { FilterDropDown } from 'Components/elements/filters/FilterDropDown';
import { FiltersWrapper } from 'Components/elements/filters/FiltersWrapper';
import { AssignContainer } from 'Components/modules/Assign/AssignContainer';
import { AssignLine } from 'Components/modules/Assign/AssignLine';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { TableTransactionQuantity } from 'Components/old/Table/TableTransactionQuantity';
import { options } from 'Mocks/fakeOptions';
import { ReactComponent as ArrowLeftIcon } from 'Svg/v2/16/arrow-left.svg';
import { ReactComponent as ChartsIcon } from 'Svg/v2/16/charts.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as DropdownIcon } from 'Svg/v2/16/three-dots.svg';
import { ReactComponent as StatusSuccessIcon } from 'Svg/v2/16/tick-in-circle.svg';
import { ReactComponent as WalletIcon } from 'Svg/v2/16/wallet.svg';
import { ReactComponent as GooglePlayLogo } from 'Svg/v2/24/logo-googleplay.svg';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';
import type { TableCellWidth } from '../../../../components/types/gridTypes';

export default {
  title: 'Pages/Sales/All Sales/Modals/Import Transactions/Create Integration',
};

export const SelectImportProcessor: React.FC = () => (
  <Modal
    size="720"
    background={'grey-10'}
    type="new"
    header={
      <ModalHeaderNew background={'grey-10'} title="Create new product" border onClose={() => {}} />
    }
  >
    <Container
      flexdirection="column"
      justifycontent="center"
      className={classNames(spacing.h100p, spacing.w100p)}
    >
      <Container
        flexdirection="column"
        alignitems="center"
        borderRadius="20"
        background={'white-100'}
        className={spacing.p56}
      >
        <Text type="title-semibold">Select product type</Text>
        <Container justifycontent="center" className={spacing.mt40}>
          <ImportProcessorType type="apple" onClick={() => {}} />
          <ImportProcessorType type="adX" onClick={() => {}} />
          <ImportProcessorType type="adMob" onClick={() => {}} />
          <ImportProcessorType type="facebook" onClick={() => {}} />
        </Container>
        <Container justifycontent="center" className={spacing.mt40}>
          <ImportProcessorType type="adwords" onClick={() => {}} />
          <ImportProcessorType type="adsense" onClick={() => {}} />
          <ImportProcessorType type="googlePlay" onClick={() => {}} />
        </Container>
      </Container>
    </Container>
  </Modal>
);

export const CreateIntegration: React.FC = () => {
  // const [nameValue, setNameValue] = useState('');
  // const [einValue, setEINValue] = useState('');
  // const [notesValue, setNotesValue] = useState('');
  // const [bankNameValue, setBankNameValue] = useState('');
  // const [swiftCodeValue, setSwiftCodeValue] = useState('');
  // const [accountNumberValue, setAccountNumberValue] = useState('');
  return (
    <Modal
      size="720"
      background={'grey-10'}
      type="new"
      header={
        <ModalHeaderNew
          background={'grey-10'}
          title="Create Integration"
          border
          onClose={() => {}}
        />
      }
      footer={
        <ModalFooterNew background={'grey-10'}>
          <Button width="220" onClick={() => {}}>
            Create
          </Button>
          <Button
            navigation
            background={'white-100'}
            color="black-100"
            border="grey-20-grey-90"
            iconLeft={<ArrowLeftIcon />}
            width="140"
            onClick={() => {}}
          >
            Back
          </Button>
        </ModalFooterNew>
      }
    >
      <Container
        flexdirection="column"
        borderRadius="20"
        background={'white-100'}
        className={classNames(spacing.p24)}
      >
        <Text type="title-semibold">Select Legal Entity & Customer</Text>
        <Text type="body-regular-14" color="grey-100" className={spacing.mt4}>
          <Text type="inherit" color="grey-90">
            Instruction:&nbsp;
          </Text>
          ALL imported transaction by this integration will be booked under selected Legal entity
          and Customer.
        </Text>
        <Container gap="24" justifycontent="space-between" className={spacing.mt24}>
          <SelectNew name="leganEntity" onChange={() => {}} width="300" label="Legal entity">
            {({ onClick, state, selectElement }) => (
              <SelectDropdown selectElement={selectElement}>
                <SelectDropdownBody>
                  <SelectDropdownItemList>
                    {options.map(({ label, value }) => (
                      <OptionSingleNew
                        key={value}
                        label={label}
                        selected={state?.value === value}
                        onClick={() =>
                          onClick({
                            label,
                            value,
                          })
                        }
                      />
                    ))}
                  </SelectDropdownItemList>
                </SelectDropdownBody>
              </SelectDropdown>
            )}
          </SelectNew>
          <SelectNew name="customer" onChange={() => {}} width="300" label="Customer">
            {({ onClick, state, selectElement }) => (
              <SelectDropdown selectElement={selectElement}>
                <SelectDropdownBody>
                  <SelectDropdownItemList>
                    {options.map(({ label, value }) => (
                      <OptionSingleNew
                        key={value}
                        label={label}
                        selected={state?.value === value}
                        onClick={() =>
                          onClick({
                            label,
                            value,
                          })
                        }
                      />
                    ))}
                  </SelectDropdownItemList>
                </SelectDropdownBody>
              </SelectDropdown>
            )}
          </SelectNew>
        </Container>
      </Container>
      <Container
        flexdirection="column"
        borderRadius="20"
        background={'white-100'}
        className={classNames(spacing.p24, spacing.mt24)}
      >
        <Text type="title-semibold">Intergration setup</Text>
        <Text type="body-regular-14" color="grey-100" className={spacing.mt4}>
          Please setup connection to your cabinet folowing the instructions
        </Text>
        <Container gap="24" justifycontent="space-between" className={spacing.mt24}>
          <InputNew
            name="cabinetName"
            label="Cabinet Name"
            width="300"
            // onChange={(e) => {
            //   setNameValue(e.target.value);
            // }}
            // value={nameValue}
          />
          <Container flexdirection="column">
            <InputNew
              name="keyID"
              label="KeyID"
              width="300"
              // onChange={(e) => {
              //   setNameValue(e.target.value);
              // }}
              // value={nameValue}
            />
            <Text
              type="body-regular-14"
              color="grey-100"
              className={classNames(spacing.mt4, spacing.ml16)}
            >
              <Text type="inherit" color="grey-90">
                Example:&nbsp;
              </Text>
              H7TPZV39ER
            </Text>
          </Container>
        </Container>
        <Container flexdirection="column" className={spacing.mt24}>
          <InputNew
            name="issuer"
            label="Issuer"
            width="full"
            // onChange={(e) => {
            //   setNameValue(e.target.value);
            // }}
            // value={nameValue}
          />
          <Text
            type="body-regular-14"
            color="grey-100"
            className={classNames(spacing.mt4, spacing.ml16)}
          >
            <Text type="inherit" color="grey-90">
              Example:&nbsp;
            </Text>
            us7917ad-1306-8807-b326-2m12133c8439
          </Text>
        </Container>
        <Container flexdirection="column" className={spacing.mt24}>
          <InputNew
            name="privateKey"
            label="Private Key"
            width="full"
            // onChange={(e) => {
            //   setNameValue(e.target.value);
            // }}
            // value={nameValue}
          />
          <PrivateKeyExample className={spacing.mt4} />
        </Container>
        <Container flexdirection="column" className={spacing.mt24}>
          <InputNew
            name="accountIdentifier"
            label="Account Identifier"
            width="full"
            // onChange={(e) => {
            //   setNameValue(e.target.value);
            // }}
            // value={nameValue}
          />
          <Text
            type="body-regular-14"
            color="grey-100"
            className={classNames(spacing.mt4, spacing.ml16)}
          >
            <Text type="inherit" color="grey-90">
              Example:&nbsp;
            </Text>
            pubsite_prod_3951360228355269957
          </Text>
        </Container>
      </Container>
    </Modal>
  );
};

export const ImportTransactionsPage: React.FC = () => {
  const filters = [
    { title: 'Integrator' },
    { title: 'Cabinet Name' },
    { title: 'Legal entity' },
    { title: 'Customer' },
    { title: 'Create by' },
    { title: 'Transactions' },
    { title: 'Non-classified transactions' },
    { title: 'Status' },
  ];
  const sorting = {
    sortType: null,
    tooltipText: 'Sorting tooltip text',
  };
  const tableHead = [
    {
      minWidth: '48',
      title: <Checkbox name="tickAll" checked={false} onChange={() => {}} />,
      padding: 'none',
    },
    {
      minWidth: '110',
      title: 'Integrator',
      sorting,
    },
    {
      minWidth: '170',
      title: 'Cabinet name',
      sorting,
    },
    {
      minWidth: '170',
      title: 'Legal entity',
      sorting,
    },
    {
      minWidth: '170',
      title: 'Customer',
      sorting,
    },
    {
      minWidth: '140',
      title: 'Created by',
      sorting,
    },
    {
      minWidth: '120',
      title: 'Transactions',
      sorting,
      align: 'center',
    },
    {
      minWidth: '140',
      title: 'Non-classified transactions',
      sorting,
      fixedWidth: true,
      align: 'center',
    },
    {
      minWidth: '130',
      title: 'Last update',
      sorting,
    },
    {
      minWidth: '80',
      title: 'Status',
      sorting,
      align: 'center',
    },
    {
      minWidth: '80',
      title: 'Action',
      align: 'center',
    },
  ];
  return (
    <Modal
      overlay="grey-10"
      size="full"
      background={'grey-10'}
      pluginScrollDisabled
      header={
        <ModalHeaderNew
          type="fullwidth"
          background={'white-100'}
          title="Import page"
          radius
          onClose={() => {}}
        />
      }
      footer={
        <ModalFooterNew justifycontent="space-between">
          <Pagination borderTop="none" padding="16" />
          <Button
            width="auto"
            height="40"
            background={'grey-10-grey-20'}
            color="grey-100"
            onClick={() => {}}
          >
            Open Sales
          </Button>
        </ModalFooterNew>
      }
    >
      <Container
        background={'white-100'}
        flexdirection="column"
        radius
        flexgrow="1"
        className={classNames(spacing.p24, spacing.mX20, spacing.mY16)}
        overflow="hidden"
      >
        <GeneralTabWrapper>
          <GeneralTab
            font="body-regular-14"
            icon={<ChartsIcon />}
            id="sales"
            onClick={() => {}}
            active
          >
            Sales
          </GeneralTab>
          <GeneralTab
            font="body-regular-14"
            id="banking"
            icon={<WalletIcon />}
            onClick={() => {}}
            active={false}
          >
            Banking
          </GeneralTab>
        </GeneralTabWrapper>
        <FiltersWrapper active margins="none">
          {filters.map((item) => (
            <FilterDropDown
              key={item.title}
              title={item.title}
              value="All"
              onClear={() => {}}
              selectedAmount={0}
            />
          ))}
        </FiltersWrapper>

        <TableNew
          noResults={false}
          className={spacing.mt24}
          tableHead={
            <TableHeadNew>
              {tableHead.map((item, index) => (
                <TableTitleNew
                  key={index}
                  minWidth={item.minWidth as TableCellWidth}
                  // flexgrow={item.flexgrow as TableTitleNewProps['flexgrow']}
                  padding={item.padding as TableTitleNewProps['padding']}
                  // font={item.font as TableTitleNewProps['font']}
                  // required={item.required as TableTitleNewProps['required']}
                  align={item.align as TableTitleNewProps['align']}
                  fixedWidth={item.fixedWidth}
                  sorting={item.sorting}
                  title={item.title}
                  onClick={() => {}}
                />
              ))}
            </TableHeadNew>
          }
        >
          <TableBodyNew>
            {Array.from(Array(4)).map((_, index) => (
              <TableRowNew key={index} gap="8">
                <TableCellNew minWidth="48" justifycontent="center" alignitems="center">
                  <Checkbox name={`line-${index}`} checked={false} onChange={() => {}} />
                </TableCellNew>
                <TableCellNew minWidth="110" alignitems="center" justifycontent="center">
                  <Tooltip message="Google Play">
                    <Icon icon={<GooglePlayLogo />} />
                  </Tooltip>
                  {/* <Icon icon={<AdMobLogo />} /> */}
                  {/* <Icon icon={<AdSenseLogo />} /> */}
                  {/* <Icon icon={<AdWordsLogo />} /> */}
                  {/* <Icon icon={<AdXLogo />} /> */}
                  {/* <Icon icon={<AppleLogo />} /> */}
                  {/* <Icon icon={<FacebookLogo />} /> */}
                </TableCellNew>
                <TableCellTextNew
                  minWidth="170"
                  value="BetterMe Limited Apple"
                  type="caption-regular"
                />
                <TableCellTextNew minWidth="170" value="Legal entity name" type="caption-regular" />
                <TableCellTextNew minWidth="170" value="Customer name" type="caption-regular" />
                <TableCellTextNew
                  minWidth="140"
                  value="Иванов ИИ"
                  secondaryValue="08 July 2021"
                  secondaryColor="grey-100"
                  type="caption-regular"
                  secondaryType="caption-regular"
                />
                <TableCellNew minWidth="120" justifycontent="center" alignitems="center">
                  <TableTransactionQuantity value={355} />
                </TableCellNew>
                <TableCellNew minWidth="140" justifycontent="center" alignitems="center">
                  <Tag color="red" padding="8" tooltip="Assign product" text="Add product: 20" />
                  {/* <Tag color="grey" padding="8" text="None" /> */}
                </TableCellNew>
                <TableCellTextNew
                  minWidth="130"
                  value="08 July 2021"
                  secondaryValue="10:45 AM"
                  secondaryColor="grey-100"
                  type="caption-regular"
                  secondaryType="caption-regular"
                />
                <TableCellNew minWidth="80" justifycontent="center" alignitems="center">
                  <Tag icon={<StatusSuccessIcon />} tooltip="Active" color="green" />
                  {/* <Tag icon={<StatusAlertIcon />} tooltip="Disable" color="grey" /> */}
                </TableCellNew>
                <TableCellNew minWidth="80" alignitems="center" justifycontent="center">
                  <DropDown
                    flexdirection="column"
                    padding="8"
                    control={({ handleOpen }) => (
                      <IconButton
                        icon={<DropdownIcon />}
                        onClick={handleOpen}
                        background={'transparent'}
                        color="grey-100-violet-90"
                      />
                    )}
                  >
                    {/* <DropDownButton size="160" icon={<EditIcon />} onClick={() => {}}>
                      Edit
                    </DropDownButton> */}
                    {/* <DropDownButton size="160" icon={<DisableIcon />} onClick={() => {}}>
                      Disable
                    </DropDownButton> */}
                    {/* <DropDownButton size="160" icon={<EnableIcon />} onClick={() => {}}>
                      Enable
                    </DropDownButton> */}
                    {/* <DropDownButton size="160" icon={<CopyIcon />} onClick={() => {}}>
                      Copy
                    </DropDownButton> */}
                    {/* <DropDownButton
                      size="160"
                      type="danger"
                      icon={<TrashIcon />}
                      onClick={() => {}}
                    >
                      Delete
                    </DropDownButton> */}
                  </DropDown>
                </TableCellNew>
              </TableRowNew>
            ))}
          </TableBodyNew>
        </TableNew>
      </Container>
    </Modal>
  );
};

export const AssignProduct: React.FC = () => (
  <Modal
    size="800"
    background={'white-100'}
    pluginScrollDisabled
    header={
      <ModalHeaderNew
        background={'grey-10'}
        title="Assign Product"
        titlePosition="center"
        radius
        onClose={() => {}}
      />
    }
    footer={
      <ModalFooterNew justifycontent="center" gap="24" direction="normal">
        <Button
          height="48"
          width="220"
          background={'white-100'}
          color="black-100"
          border="grey-20-grey-90"
          onClick={() => {}}
        >
          Close
        </Button>
        <Button height="48" width="220" onClick={() => {}}>
          Assign Product
        </Button>
      </ModalFooterNew>
    }
  >
    <Container className={spacing.w100p} background={'grey-10'}>
      <AssignProductTitle type="Cabinet name" name="BetterMe International" />
      <AssignProductTitle type="Legal entity" name="Frentinol limited" />
      <AssignProductTitle type="Customer" name="Google Irelend limited" />
    </Container>
    <Container justifycontent="center" className={spacing.pY16}>
      <Text color="grey-100" className={spacing.w280} align="center">
        We found new SKUs, please assign product in order to import transactions:
      </Text>
    </Container>
    <AssignContainer titleSource="SKU" titleTarget="Product" width="660">
      <AssignLine
        order="1"
        sourceValue="BetterMe health & fit"
        sourceSecondaryValue="BetterMe"
        isAssigned={false}
      >
        <SelectNew
          onChange={() => {}}
          // defaultValue={{ label: '', value: '' }}
          label="Assign Product"
          placeholder="Assign Product"
          name="product"
          width="full"
        >
          {({ onClick, state, selectElement }) => (
            <SelectDropdown selectElement={selectElement}>
              <SelectDropdownBody>
                <SelectDropdownItemList>
                  {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                    <OptionDoubleVerticalNew
                      key={value}
                      label={label}
                      secondaryLabel={secondaryLabel}
                      selected={state?.value === value}
                      onClick={() =>
                        onClick({
                          label,
                          value,
                        })
                      }
                    />
                  ))}
                </SelectDropdownItemList>
              </SelectDropdownBody>

              <SelectDropdownFooter>
                <Container justifycontent="flex-end" className={spacing.mr12}>
                  <LinkButton icon={<PlusIcon />} onClick={() => {}}>
                    Create new Product
                  </LinkButton>
                </Container>
              </SelectDropdownFooter>
            </SelectDropdown>
          )}
        </SelectNew>
      </AssignLine>
      <AssignLine
        order="2"
        sourceValue="BetterMe health & fit"
        sourceSecondaryValue="BetterMe"
        isAssigned
      >
        <SelectNew
          onChange={() => {}}
          // defaultValue={{ label: '', value: '' }}
          label="Assign Product"
          placeholder="Assign Product"
          name="product"
          width="full"
        >
          {({ onClick, state, selectElement }) => (
            <SelectDropdown selectElement={selectElement}>
              <SelectDropdownBody>
                <SelectDropdownItemList>
                  {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                    <OptionDoubleVerticalNew
                      key={value}
                      label={label}
                      secondaryLabel={secondaryLabel}
                      selected={state?.value === value}
                      onClick={() =>
                        onClick({
                          label,
                          value,
                        })
                      }
                    />
                  ))}
                </SelectDropdownItemList>
              </SelectDropdownBody>

              <SelectDropdownFooter>
                <Container justifycontent="flex-end" className={spacing.mr12}>
                  <LinkButton icon={<PlusIcon />} onClick={() => {}}>
                    Create new Product
                  </LinkButton>
                </Container>
              </SelectDropdownFooter>
            </SelectDropdown>
          )}
        </SelectNew>
      </AssignLine>
    </AssignContainer>
  </Modal>
);
