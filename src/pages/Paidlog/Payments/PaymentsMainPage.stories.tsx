import { Meta, Story } from '@storybook/react';
import { DropDown } from 'Components/DropDown';
import { Divider } from 'Components/base/Divider';
import { Icon } from 'Components/base/Icon';
import { Tag } from 'Components/base/Tag';
import { TagSegmented } from 'Components/base/TagSegmented';
import { Button } from 'Components/base/buttons/Button';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { ContainerMain } from 'Components/base/grid/ContainerMain';
import { MainPageContentContainer } from 'Components/base/grid/MainPageContentContainer';
import { GeneralTab, GeneralTabWrapper } from 'Components/base/tabs/GeneralTab';
import { MenuTab, MenuTabWrapper } from 'Components/base/tabs/MenuTab';
import { Text } from 'Components/base/typography/Text';
import { HeaderSB } from 'Components/custom/Stories/HeaderSB';
import { NavigationSB } from 'Components/custom/Stories/NavigationSB';
import { Pagination } from 'Components/custom/Stories/Pagination/Pagination';
import PerPageSelect from 'Components/custom/Stories/Pagination/PerPageSelect/PerPageSelect';
import { ExportDropdown } from 'Components/elements/ExportDropdown';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { UserInfo } from 'Components/elements/UserInfo';
import { FilterDropDown } from 'Components/elements/filters/FilterDropDown';
import { FilterDropDownItem } from 'Components/elements/filters/FilterDropDownItem';
import { FilterItemsContainer } from 'Components/elements/filters/FilterItemsContainer';
import { FilterSearch } from 'Components/elements/filters/FilterSearch';
import { FilterToggleButton } from 'Components/elements/filters/FilterToggleButton';
import { FiltersWrapper } from 'Components/elements/filters/FiltersWrapper';
import { Search } from 'Components/inputs/Search';
import { TableCellWidth } from 'Components/types/gridTypes';
import { rowsPerPageOptions } from 'Mocks/fakeOptions';
import {
  bankAccounts,
  filterOptions,
  paymentsFilters,
  paymentsTableHead,
  paymentsTabs,
  tableHead,
} from 'Mocks/purchasesFakeData';
import { ReactComponent as OpenIcon } from 'Svg/16/eye.svg';
import { ReactComponent as ArrowRightIcon } from 'Svg/v2/16/arrow-right.svg';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as DropdownIcon } from 'Svg/v2/16/three-dots.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

const PaymentsMainPageComponent: Story = ({ storyState }) => {
  const [toggle, toggleMenu] = useState<boolean>(false);
  const [showFilters, setShowFilters] = useState<boolean>(false);
  const [searchValue, setSearchValue] = useState<string>('');
  const [rowsPerPage, setRowsPerPage] = useState<string>(rowsPerPageOptions[0].value);

  const handleChange = (value: string) => {
    setSearchValue(value);
  };
  const filteredItems = filterOptions.filter((item) => item.option.includes(searchValue));

  return (
    <Container fullscreen flexwrap="nowrap" background={'grey-10'}>
      <NavigationSB opened={toggle} onToggleOpen={() => toggleMenu(!toggle)} />
      <ContainerMain navOpened={toggle}>
        <HeaderSB />
        <Container flexdirection="column" className={spacing.mX32}>
          <Text type="h1-semibold">Paidlog</Text>
          <MenuTabWrapper className={spacing.mt24}>
            <MenuTab id="paymentRequest" active={false} onClick={() => {}}>
              Bills
            </MenuTab>
            <MenuTab id="approvals" active={false} onClick={() => {}}>
              Approvals
            </MenuTab>
            <MenuTab id="payments" active onClick={() => {}}>
              Payments
            </MenuTab>
            <MenuTab id="dictionaries" active={false} onClick={() => {}}>
              Dictionaries
            </MenuTab>
          </MenuTabWrapper>
        </Container>
        <MainPageContentContainer>
          {storyState === 'loaded' && (
            <>
              <Container
                className={classNames(spacing.mX24, spacing.mt24)}
                justifycontent="space-between"
                gap="24"
              >
                <Container gap="24" alignitems="center">
                  <FilterToggleButton
                    showFilters={showFilters}
                    filtersSelected={false}
                    onClear={() => {}}
                    onClick={() => {
                      setShowFilters((prevState) => !prevState);
                    }}
                  />
                  <Search
                    width="320"
                    height="40"
                    name="search-bills-mainPage"
                    placeholder="Search by ID or Vendor"
                    value=""
                    onChange={() => {}}
                    searchParams={[]}
                    onSearch={() => {}}
                    onClear={() => {}}
                  />
                </Container>
                <Container gap="24" alignitems="center">
                  <ExportDropdown onExportToXLS={() => {}} onExportToCSV={() => {}} />
                  <Button
                    background={'violet-90-violet-100'}
                    color="white-100"
                    width="140"
                    height="40"
                    iconLeft={<PlusIcon />}
                  >
                    Create
                  </Button>
                </Container>
              </Container>
              <FiltersWrapper active={showFilters}>
                {paymentsFilters.map((item) => (
                  <FilterDropDown
                    key={item.title}
                    title={item.title}
                    value="All"
                    onClear={() => {}}
                    selectedAmount={0}
                  >
                    {item.component}
                    {!item.component && (
                      <>
                        <Container
                          className={classNames(spacing.m16, spacing.w288fixed)}
                          flexdirection="column"
                        >
                          <FilterSearch
                            title="Account type:"
                            value={searchValue}
                            onChange={handleChange}
                          />
                          <FilterItemsContainer className={spacing.mt8}>
                            {filteredItems.map((item) => (
                              <FilterDropDownItem
                                key={item.id}
                                id={item.id}
                                type="item"
                                checked={false}
                                blurred={false}
                                value={item.option}
                                onChange={() => {}}
                              />
                            ))}
                          </FilterItemsContainer>
                        </Container>
                        <Divider fullHorizontalWidth />
                        <Container
                          className={classNames(spacing.m16, spacing.w288fixed)}
                          justifycontent="flex-end"
                          alignitems="center"
                        >
                          <Button
                            width="auto"
                            height="32"
                            padding="8"
                            font="text-medium"
                            onClick={() => {}}
                            background={'violet-90-violet-100'}
                            color="white-100"
                          >
                            Apply
                          </Button>
                        </Container>
                      </>
                    )}
                  </FilterDropDown>
                ))}
              </FiltersWrapper>
              <GeneralTabWrapper className={classNames(spacing.mX24, spacing.mt24)}>
                {paymentsTabs.map(({ name, active, icon }) => (
                  <GeneralTab key={name} id={name} active={active} icon={icon} onClick={() => {}}>
                    {name}
                  </GeneralTab>
                ))}
              </GeneralTabWrapper>
              <TableNew
                noResults={false}
                className={classNames(spacing.mt24, spacing.ml24, spacing.pr24)}
                tableHead={
                  <TableHeadNew>
                    {paymentsTableHead.map((item, index) => {
                      return (
                        <TableTitleNew
                          key={index}
                          minWidth={item.minWidth as TableCellWidth}
                          align={item.align as TableTitleNewProps['align']}
                          padding={item.padding as TableTitleNewProps['padding']}
                          title={item.title}
                          sorting={item.sorting}
                          onClick={() => {}}
                        />
                      );
                    })}
                  </TableHeadNew>
                }
              >
                <TableBodyNew>
                  {Array.from(Array(9)).map((_, index) => (
                    <TableRowNew key={index} gap="8">
                      <TableCellTextNew
                        minWidth={paymentsTableHead[0].minWidth as TableCellWidth}
                        value="ID 000000"
                        secondaryValue="12 July 2023"
                      />
                      <TableCellTextNew
                        minWidth={paymentsTableHead[1].minWidth as TableCellWidth}
                        value="Rozetkadfkdkfjfjjgjgjjgjgjjgjgjjghhg"
                        fixedWidth
                        secondaryValue="Company"
                        noWrap
                        lineClamp="none"
                      />
                      <TableCellNew minWidth={paymentsTableHead[2].minWidth as TableCellWidth}>
                        <Tag text="ID434223" additionalCounter={2} />
                      </TableCellNew>
                      <TableCellNew
                        minWidth={paymentsTableHead[3].minWidth as TableCellWidth}
                        gap="8"
                      >
                        <Tag text="Furniture" color="violet" />
                        <Tag text="+4" color="violet" />
                      </TableCellNew>
                      <TableCellTextNew
                        minWidth={paymentsTableHead[4].minWidth as TableCellWidth}
                        value="EUR"
                        alignitems="center"
                      />
                      <TableCellTextNew
                        minWidth={paymentsTableHead[5].minWidth as TableCellWidth}
                        value="1,200.00 €"
                        type="caption-semibold"
                        align="right"
                      />
                      <TableCellTextNew
                        minWidth={paymentsTableHead[6].minWidth as TableCellWidth}
                        value="15 July 2023"
                        tagText="Paid"
                        tagColor="green"
                      />
                      <TableCellTextNew
                        minWidth={paymentsTableHead[7].minWidth as TableCellWidth}
                        value="BetterMe International Limited"
                        fixedWidth
                        noWrap
                        lineClamp="none"
                      />
                      <TableCellNew minWidth={paymentsTableHead[8].minWidth as TableCellWidth}>
                        <TagSegmented
                          {...bankAccounts}
                          tooltipMessage={
                            <Container flexdirection="column">
                              <Text color="white-100" style={{ wordBreak: 'break-all' }}>
                                {bankAccounts.mainLabel}
                              </Text>
                              <Text color="white-100" style={{ wordBreak: 'break-all' }}>
                                {bankAccounts.secondLabel} | {bankAccounts.thirdLabel}
                              </Text>
                            </Container>
                          }
                        />
                      </TableCellNew>
                      <TableCellNew
                        minWidth={paymentsTableHead[9].minWidth as TableCellWidth}
                        alignitems="center"
                      >
                        <Text color="grey-100">Card</Text>
                        <Icon icon={<ArrowRightIcon transform="scale(0.5)" />} path="inherit" />
                        <TagSegmented
                          {...bankAccounts}
                          tooltipMessage={
                            <Container flexdirection="column">
                              <Text color="white-100" style={{ wordBreak: 'break-all' }}>
                                {bankAccounts.mainLabel}
                              </Text>
                              <Text color="white-100" style={{ wordBreak: 'break-all' }}>
                                {bankAccounts.secondLabel} | {bankAccounts.thirdLabel}
                              </Text>
                            </Container>
                          }
                        />
                      </TableCellNew>
                      <TableCellNew
                        minWidth={paymentsTableHead[10].minWidth as TableCellWidth}
                        fixedWidth
                      >
                        <UserInfo name="Dinys Vlasov" isLogo secondaryText="Dinys@gmail.com" />
                      </TableCellNew>
                      <TableCellNew
                        minWidth={tableHead[12].minWidth as TableCellWidth}
                        justifycontent="center"
                        alignitems="center"
                        fixedWidth
                        padding="8"
                      >
                        <DropDown
                          flexdirection="column"
                          padding="8"
                          control={({ handleOpen }) => (
                            <IconButton
                              icon={<DropdownIcon />}
                              onClick={handleOpen}
                              background={'transparent'}
                              color="grey-100-violet-90"
                            />
                          )}
                        >
                          <DropDownButton size="160" icon={<OpenIcon />} onClick={() => {}}>
                            Open
                          </DropDownButton>
                          <DropDownButton size="160" icon={<EditIcon />} onClick={() => {}}>
                            Edit
                          </DropDownButton>
                          <Divider fullHorizontalWidth />
                          <DropDownButton
                            size="160"
                            type="danger"
                            icon={<TrashIcon />}
                            onClick={() => {}}
                          >
                            Delete
                          </DropDownButton>
                        </DropDown>
                      </TableCellNew>
                    </TableRowNew>
                  ))}
                </TableBodyNew>
              </TableNew>
              <Pagination
                customPagination={
                  <Container gap="16" alignitems="center">
                    <Text type="subtext-regular" color="grey-100">
                      Rows per Page
                    </Text>
                    <PerPageSelect
                      selectedValue={rowsPerPage}
                      onOptionClick={(value) => setRowsPerPage(value)}
                    />
                  </Container>
                }
              />
            </>
          )}
        </MainPageContentContainer>
      </ContainerMain>
    </Container>
  );
};
export default {
  title: 'Pages/Paidlog/Payments/PaymentsMainPage',
  component: PaymentsMainPageComponent,
  argTypes: {
    storyState: {
      options: ['empty', 'loading', 'loaded'],
      control: {
        type: 'radio',
        labels: {
          empty: 'Empty page',
          loading: 'Page is loading',
          loaded: 'Page loaded',
        },
      },
    },
  },
} as Meta;

export const PaymentsMainPage = PaymentsMainPageComponent.bind({});
PaymentsMainPage.args = {
  storyState: 'loaded',
};
