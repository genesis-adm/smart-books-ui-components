import { Meta, Story } from '@storybook/react';
import { DropDown } from 'Components/DropDown';
import { Divider } from 'Components/base/Divider';
import { Icon } from 'Components/base/Icon';
import { LoadingLine } from 'Components/base/LoadingLine';
import { Tag } from 'Components/base/Tag';
import { Button } from 'Components/base/buttons/Button';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { ContainerMain } from 'Components/base/grid/ContainerMain';
import { MainPageContentContainer } from 'Components/base/grid/MainPageContentContainer';
import { GeneralTab, GeneralTabWrapper } from 'Components/base/tabs/GeneralTab';
import { MenuTab, MenuTabWrapper } from 'Components/base/tabs/MenuTab';
import { Text } from 'Components/base/typography/Text';
import { MainPageEmpty } from 'Components/custom/MainPage/MainPageEmpty';
import HeaderSB from 'Components/custom/Stories/HeaderSB/HeaderSB';
import NavigationSB from 'Components/custom/Stories/NavigationSB/NavigationSB';
import { Pagination } from 'Components/custom/Stories/Pagination/Pagination';
import PerPageSelect from 'Components/custom/Stories/Pagination/PerPageSelect/PerPageSelect';
import { ExportDropdown } from 'Components/elements/ExportDropdown';
import LogoMultiple from 'Components/elements/LogoMultiple/LogoMultiple';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import {
  TableRowExpandable,
  TableRowExpandableWrapper,
} from 'Components/elements/Table/TableRowExpandable';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { FilterDropDown } from 'Components/elements/filters/FilterDropDown';
import { FilterDropDownItem } from 'Components/elements/filters/FilterDropDownItem';
import { FilterItemsContainer } from 'Components/elements/filters/FilterItemsContainer';
import { FilterSearch } from 'Components/elements/filters/FilterSearch';
import { FilterToggleButton } from 'Components/elements/filters/FilterToggleButton';
import { FiltersWrapper } from 'Components/elements/filters/FiltersWrapper';
import { Search } from 'Components/inputs/Search';
import { TableCellWidth } from 'Components/types/gridTypes';
import { rowsPerPageOptions } from 'Mocks/fakeOptions';
import { categoriesFilters, filterOptions, sorting } from 'Mocks/purchasesFakeData';
import { ReactComponent as DisabledIcon } from 'Svg/16/minus-circle.svg';
import { ReactComponent as ChevronIcon } from 'Svg/v2/16/chevron-up.svg';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as DropdownIcon } from 'Svg/v2/16/three-dots.svg';
import { ReactComponent as StatusSuccessIcon } from 'Svg/v2/16/tick-in-circle.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import { ReactComponent as UploadIcon } from 'Svg/v2/16/upload.svg';
import { ReactComponent as KeyIcon } from 'Svg/v2/24/key.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

const mainRow = {
  name: 'Office equipment',
  icon: <KeyIcon transform="scale(0.7)" />,
  notes: 'Notes is a notetaking app developed by Apple Inc.',
  owners: ['Karine Mnatsakanian', 'Karine Mnatsakanian', 'Karine Mnatsakanian'],
  status: 'Active',
  isSensitive: true,
  onEdit: () => {},
  onDelete: () => {},
  onDisable: () => {},
};
const dictionariesTabs: { name: string; active: boolean }[] = [
  {
    name: 'Vendors',
    active: false,
  },
  {
    name: 'Categories',
    active: true,
  },
  {
    name: 'Items',
    active: false,
  },
];

const DictionariesCategoriesMainPageComponent: Story = ({ storyState }) => {
  const [toggle, toggleMenu] = useState<boolean>(false);
  const [showFilters, setShowFilters] = useState<boolean>(false);
  const [searchValue, setSearchValue] = useState('');
  const filteredItems = filterOptions.filter((item) => item.option.includes(searchValue));
  const [toggleAllRow, setToggleAllRow] = useState<boolean>(false);
  const [rowsPerPage, setRowsPerPage] = useState<string>(rowsPerPageOptions[0].value);

  const handleChange = (value: string) => {
    setSearchValue(value);
  };
  const categoriesTableHead = [
    {
      minWidth: '48',
      title: (
        <IconButton
          size="24"
          background={'transparent'}
          border="grey-20"
          color="grey-100"
          icon={<ChevronIcon />}
          onClick={() => {
            setToggleAllRow(true);
            setTimeout(() => setToggleAllRow(false), 4);
          }}
        />
      ),
      padding: 'none',
    },
    {
      minWidth: '250',
      title: 'Name',
      sorting,
      flexgrow: '1',
    },
    {
      minWidth: '340',
      title: 'Notes',
      sorting,
      flexgrow: '1',
    },
    {
      minWidth: '300',
      title: 'Owner',
      sorting,
    },
    {
      minWidth: '180',
      title: 'Status',
      sorting,
      align: 'center',
    },
    {
      minWidth: '140',
      title: 'Action',
      align: 'center',
    },
  ];
  const RowExample = () => (
    <>
      <TableCellTextNew
        minWidth={categoriesTableHead[1].minWidth as TableCellWidth}
        value={mainRow.name}
        secondaryColor="grey-100"
        iconLeft={<Icon icon={mainRow.icon} path="grey-90" />}
        noWrap
        lineClamp="none"
        flexgrow="1"
      />
      <TableCellTextNew
        minWidth={categoriesTableHead[2].minWidth as TableCellWidth}
        value={mainRow.notes}
        flexgrow="1"
        color="grey-100"
        noWrap
        lineClamp="none"
      />
      <TableCellNew
        minWidth={categoriesTableHead[3].minWidth as TableCellWidth}
        alignitems="center"
        gap="8"
      >
        <LogoMultiple
          items={mainRow.owners}
          maxCount={2}
          marginLeft="5"
          content="user"
          radius="rounded"
          size="24"
        />
        {mainRow.owners.length === 1 && <Text>{mainRow.owners[0]}</Text>}
      </TableCellNew>
      <TableCellNew
        minWidth={categoriesTableHead[4].minWidth as TableCellWidth}
        justifycontent="center"
        alignitems="center"
      >
        <Tag icon={<StatusSuccessIcon />} tooltip="Active" color="green" text="Active" />
      </TableCellNew>
      <TableCellNew
        minWidth={categoriesTableHead[5].minWidth as TableCellWidth}
        justifycontent="center"
        alignitems="center"
      >
        <DropDown
          flexdirection="column"
          padding="8"
          control={({ handleOpen }) => (
            <IconButton
              icon={<DropdownIcon />}
              onClick={handleOpen}
              background={'transparent'}
              color="grey-100-violet-90"
            />
          )}
        >
          <DropDownButton size="160" icon={<EditIcon />} onClick={mainRow.onEdit}>
            Edit
          </DropDownButton>
          <DropDownButton size="160" icon={<DisabledIcon />} onClick={mainRow.onDisable}>
            Disable
          </DropDownButton>
          <Divider fullHorizontalWidth />
          <DropDownButton size="160" type="danger" icon={<TrashIcon />} onClick={mainRow.onDelete}>
            Delete
          </DropDownButton>
        </DropDown>
      </TableCellNew>
    </>
  );
  return (
    <Container fullscreen flexwrap="nowrap" background={'grey-10'}>
      <NavigationSB opened={toggle} onToggleOpen={() => toggleMenu(!toggle)} />
      <ContainerMain navOpened={toggle}>
        <HeaderSB />
        <Container flexdirection="column" className={spacing.mX32}>
          <Text type="h1-semibold">Paidlog</Text>
          <MenuTabWrapper className={spacing.mt24}>
            <MenuTab id="paymentRequest" active={false} onClick={() => {}}>
              Payment request
            </MenuTab>
            <MenuTab id="approvals" active={false} onClick={() => {}}>
              Approvals
            </MenuTab>
            <MenuTab id="payments" active={false} onClick={() => {}}>
              Payments
            </MenuTab>
            <MenuTab id="dictionaries" active onClick={() => {}}>
              Dictionaries
            </MenuTab>
          </MenuTabWrapper>
        </Container>
        <MainPageContentContainer>
          {storyState === 'loaded' && (
            <>
              <Container
                className={classNames(spacing.mX24, spacing.mt24)}
                justifycontent="space-between"
                gap="24"
              >
                <Container gap="24" alignitems="center">
                  <FilterToggleButton
                    showFilters={showFilters}
                    filtersSelected={false}
                    onClear={() => {}}
                    onClick={() => {
                      setShowFilters((prevState) => !prevState);
                    }}
                  />
                  <Search
                    width="320"
                    height="40"
                    name="search-paidlog-dictionaries-categories-mainPage"
                    placeholder="Search by Name"
                    value=""
                    onChange={() => {}}
                    searchParams={[]}
                    onSearch={() => {}}
                    onClear={() => {}}
                  />
                </Container>
                <Container gap="24" alignitems="center">
                  <Divider type="vertical" background={'grey-90'} height="20" />
                  <ExportDropdown onExportToXLS={() => {}} onExportToCSV={() => {}} />
                  <IconButton
                    size="40"
                    background={'grey-10-grey-20'}
                    color="grey-100"
                    icon={<UploadIcon />}
                    onClick={() => {}}
                  />
                  <Button
                    background={'violet-90-violet-100'}
                    color="white-100"
                    width="140"
                    height="40"
                    iconLeft={<PlusIcon />}
                  >
                    Create
                  </Button>
                </Container>
              </Container>
              <FiltersWrapper active={showFilters}>
                {categoriesFilters.map((item) => (
                  <FilterDropDown
                    key={item.title}
                    title={item.title}
                    value="All"
                    onClear={() => {}}
                    selectedAmount={0}
                  >
                    <Container
                      className={classNames(spacing.m16, spacing.w288fixed)}
                      flexdirection="column"
                    >
                      <FilterSearch
                        title="Account type:"
                        value={searchValue}
                        onChange={handleChange}
                      />
                      <FilterItemsContainer className={spacing.mt8}>
                        {filteredItems.map((item) => (
                          <FilterDropDownItem
                            key={item.id}
                            id={item.id}
                            type="item"
                            checked={false}
                            blurred={false}
                            value={item.option}
                            onChange={() => {}}
                          />
                        ))}
                      </FilterItemsContainer>
                    </Container>
                    <Divider fullHorizontalWidth />
                    <Container
                      className={classNames(spacing.m16, spacing.w288fixed)}
                      justifycontent="flex-end"
                      alignitems="center"
                    >
                      <Button
                        width="auto"
                        height="32"
                        padding="8"
                        font="text-medium"
                        onClick={() => {}}
                        background={'violet-90-violet-100'}
                        color="white-100"
                      >
                        Apply
                      </Button>
                    </Container>
                  </FilterDropDown>
                ))}
              </FiltersWrapper>
              <GeneralTabWrapper className={classNames(spacing.mX24, spacing.mt24)}>
                {dictionariesTabs.map(({ name, active }) => (
                  <GeneralTab key={name} id={name} active={active} onClick={() => {}}>
                    {name}
                  </GeneralTab>
                ))}
              </GeneralTabWrapper>
              <TableNew
                noResults={false}
                className={classNames(spacing.mt24, spacing.ml24, spacing.pr24)}
                tableHead={
                  <TableHeadNew>
                    {categoriesTableHead.map((item, index) => (
                      <TableTitleNew
                        key={index}
                        minWidth={item.minWidth as TableCellWidth}
                        padding={item.padding as TableTitleNewProps['padding']}
                        align={item.align as TableTitleNewProps['align']}
                        flexgrow={item.flexgrow as TableTitleNewProps['flexgrow']}
                        title={item.title}
                        sorting={item.sorting}
                        onClick={() => {}}
                      />
                    ))}
                  </TableHeadNew>
                }
              >
                <TableBodyNew>
                  <TableRowExpandableWrapper isActive={false}>
                    <TableRowExpandable
                      type="primary"
                      isRowVisible
                      isToggleActive={false}
                      onToggle={() => {}}
                    >
                      <>
                        <TableCellTextNew
                          minWidth={categoriesTableHead[1].minWidth as TableCellWidth}
                          value={mainRow.name}
                          secondaryColor="grey-100"
                          flexgrow="1"
                          iconLeft={<Icon icon={mainRow.icon} path="grey-90" />}
                          noWrap
                          lineClamp="none"
                          justifycontent="flex-start"
                        />
                        <TableCellTextNew
                          minWidth={categoriesTableHead[2].minWidth as TableCellWidth}
                          value={mainRow.notes}
                          flexgrow="1"
                          color="grey-100"
                          noWrap
                          lineClamp="none"
                          justifycontent="flex-start"
                        />
                        <TableCellNew
                          minWidth={categoriesTableHead[3].minWidth as TableCellWidth}
                          alignitems="center"
                          gap="8"
                          justifycontent="flex-start"
                        >
                          <LogoMultiple
                            items={['Karine Mnatsakanian']}
                            maxCount={2}
                            marginLeft="5"
                            content="user"
                            radius="rounded"
                            size="24"
                          />
                          <Text>{mainRow.owners[0]}</Text>
                        </TableCellNew>
                        <TableCellNew
                          minWidth={categoriesTableHead[4].minWidth as TableCellWidth}
                          justifycontent="center"
                          alignitems="center"
                        >
                          <Tag
                            icon={<StatusSuccessIcon />}
                            tooltip="Active"
                            color="green"
                            text="Active"
                          />
                        </TableCellNew>
                        <TableCellNew
                          minWidth={categoriesTableHead[5].minWidth as TableCellWidth}
                          justifycontent="center"
                          alignitems="center"
                        >
                          <DropDown
                            flexdirection="column"
                            padding="8"
                            control={({ handleOpen }) => (
                              <IconButton
                                icon={<DropdownIcon />}
                                onClick={handleOpen}
                                background={'transparent'}
                                color="grey-100-violet-90"
                              />
                            )}
                          >
                            <DropDownButton size="160" icon={<EditIcon />} onClick={mainRow.onEdit}>
                              Edit
                            </DropDownButton>
                            <DropDownButton
                              size="160"
                              icon={<DisabledIcon />}
                              onClick={mainRow.onDisable}
                            >
                              Disable
                            </DropDownButton>
                            <Divider fullHorizontalWidth />
                            <DropDownButton
                              size="160"
                              type="danger"
                              icon={<TrashIcon />}
                              onClick={mainRow.onDelete}
                            >
                              Delete
                            </DropDownButton>
                          </DropDown>
                        </TableCellNew>
                      </>
                    </TableRowExpandable>
                  </TableRowExpandableWrapper>
                  {Array.from(Array(4)).map((_, index) => (
                    <TableRowExpandableWrapper isActive key={index}>
                      <TableRowExpandable
                        type="primary"
                        isRowVisible
                        isToggleActive
                        onToggle={() => {}}
                      >
                        <RowExample />
                      </TableRowExpandable>
                      <TableRowExpandable isRowVisible isToggleActive onToggle={() => {}}>
                        <RowExample />
                      </TableRowExpandable>
                      <TableRowExpandable isRowVisible type="tertiary">
                        <RowExample />
                      </TableRowExpandable>
                      <TableRowExpandable isRowVisible type="tertiary">
                        <RowExample />
                      </TableRowExpandable>
                      <TableRowExpandable isRowVisible isToggleActive onToggle={() => {}}>
                        <RowExample />
                      </TableRowExpandable>
                      <TableRowExpandable isRowVisible type="tertiary">
                        <RowExample />
                      </TableRowExpandable>
                      <TableRowExpandable isRowVisible type="tertiary">
                        <RowExample />
                      </TableRowExpandable>
                    </TableRowExpandableWrapper>
                  ))}
                </TableBodyNew>
              </TableNew>
              <Pagination
                customPagination={
                  <Container gap="16" alignitems="center">
                    <Text type="subtext-regular" color="grey-100">
                      Rows per Page
                    </Text>
                    <PerPageSelect
                      selectedValue={rowsPerPage}
                      onOptionClick={(value) => setRowsPerPage(value)}
                    />
                  </Container>
                }
              />
            </>
          )}
          {storyState === 'empty' && (
            <>
              <GeneralTabWrapper className={classNames(spacing.mX24, spacing.mt24)}>
                {dictionariesTabs.map(({ name, active }) => (
                  <GeneralTab key={name} id={name} active={active} onClick={() => {}}>
                    {name}
                  </GeneralTab>
                ))}
              </GeneralTabWrapper>
              <MainPageEmpty
                title="Create new Categories"
                description="You have no categories created. Your categories will be displayed here."
                action={
                  <Button
                    width="220"
                    height="40"
                    background={'blue-10-blue-20'}
                    color="violet-90-violet-100"
                    iconLeft={<PlusIcon />}
                    onClick={() => {}}
                  >
                    Create new Categories
                  </Button>
                }
              />
            </>
          )}
        </MainPageContentContainer>
        <LoadingLine loadingCompleted={storyState !== 'loading'} />
      </ContainerMain>
    </Container>
  );
};

export default {
  title: 'Pages/Paidlog/Dictionaries/DictionariesCategoriesMainPage',
  component: DictionariesCategoriesMainPageComponent,
  argTypes: {
    storyState: {
      options: ['empty', 'loading', 'loaded'],
      control: {
        type: 'radio',
        labels: {
          empty: 'Empty page',
          loading: 'Page is loading',
          loaded: 'Page loaded',
        },
      },
    },
  },
} as Meta;
export const DictionariesCategoriesMainPage = DictionariesCategoriesMainPageComponent.bind({});
DictionariesCategoriesMainPage.args = {
  storyState: 'loaded',
};
