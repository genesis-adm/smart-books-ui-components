import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownFooter } from 'Components/SelectDropdown/components/SelectDropdownFooter';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { Button } from 'Components/base/buttons/Button';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { InputNew } from 'Components/base/inputs/InputNew';
import { NumericInput } from 'Components/base/inputs/NumericInput';
import { OwnNumberFormatValues } from 'Components/base/inputs/NumericInput/NumericInput.types';
import { SelectNew } from 'Components/base/inputs/SelectNew';
import { OptionSingleNew } from 'Components/base/inputs/SelectNew/options/OptionSingleNew';
import AccordionOption from 'Components/base/inputs/SelectNew/options/optionsWithAccordion/AccordionOption/AccordionOption';
import { TextareaNew } from 'Components/base/inputs/TextareaNew';
import { MenuTab, MenuTabWrapper } from 'Components/base/tabs/MenuTab';
import { Text } from 'Components/base/typography/Text';
import { UploadContainer } from 'Components/custom/Upload/UploadContainer';
import { UploadInfo } from 'Components/custom/Upload/UploadInfo';
import { UploadItemField } from 'Components/custom/Upload/UploadItemField';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { options } from 'Mocks/fakeOptions';
import { ReactComponent as PrinterIcon } from 'Svg/categories/16/printer.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { checkFileType, isFileInArray } from 'Utils/fileHandlers';
import classNames from 'classnames';
import React, { ReactNode, useState } from 'react';
import { FC } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Paidlog/Dictionaries/Dictionaries Create Items Modal',
};

export const CreateItems: FC = () => {
  type CategoriesType = {
    id: number;
    name: string;
    icon?: ReactNode;
    selected: boolean;
    active: boolean;
  };
  const tabs: { id: string; title: string }[] = [
    {
      id: 'general',
      title: 'General',
    },
    {
      id: 'otherInformation',
      title: 'Other information',
    },
  ];
  const categories: Array<CategoriesType[]> = [
    [
      {
        id: 1,
        name: 'Layer 1',
        selected: false,
        icon: <PrinterIcon />,
        active: false,
      },
      {
        id: 2,
        name: 'Layer 2',
        selected: false,
        icon: <PrinterIcon />,
        active: false,
      },
      {
        id: 3,
        name: 'Layer 3',
        selected: false,
        active: false,
      },
    ],
    [
      {
        id: 1,
        name: 'Layer 1',
        selected: false,
        active: false,
      },
      {
        id: 2,
        name: 'Layer 2',
        selected: false,
        active: false,
      },
    ],
  ];
  const [activeTab, setActiveTab] = useState<string>('general');
  const [newCategories, setNewCategories] = useState<Array<CategoriesType[]>>(categories);
  const [itemValue, setItemValue] = useState('');
  const [priceExclTaxesValue, setPriceExclTaxesValue] = useState<number>();
  const [priceInclTaxesValue, setPriceInclTaxesValue] = useState<number>();
  const [taxesValue, setTaxesValue] = useState<number>();
  const [skuValue, setSkuValue] = useState('');
  const [barcodeValue, setBarcodeValue] = useState('');
  const [notesValue, setNotesValue] = useState('');

  const [images, setImages] = useState<File[]>([]);
  const handleImageUpload = (image: File) => {
    if (!isFileInArray(image, images)) {
      setImages((prevState) => [...prevState, image]);
    }
  };
  const handleImageDelete = ({ name, size }: { name: string; size: number }) => {
    setImages((prev) => prev.filter((image) => !(image.name === name && image.size === size)));
  };
  const handleImageDrop = (e: React.DragEvent<HTMLElement>) => {
    const imageArray = e.dataTransfer.files;
    Array.from(imageArray).forEach((image) => {
      if (checkFileType(image, ['jpg', 'jpeg', 'png', 'webp']) && !isFileInArray(image, images)) {
        setImages((prevState) => [...prevState, image]);
      }
    });
  };
  const handlePriceExclTaxesChange = ({ floatValue }: OwnNumberFormatValues) => {
    setPriceExclTaxesValue(floatValue);
  };
  const handlePriceInclTaxesChange = ({ floatValue }: OwnNumberFormatValues) => {
    setPriceInclTaxesValue(floatValue);
  };
  const handleTaxesChange = ({ floatValue }: OwnNumberFormatValues) => {
    setTaxesValue(floatValue);
  };

  const handleSelectCategory = (selected: number, index: number) => {
    const updated = newCategories.map((items, indexOuter) => {
      return items.map((item) => {
        return index === indexOuter && item.id === selected
          ? {
              ...item,
              selected: true,
            }
          : {
              ...item,
              selected: false,
            };
      });
    });
    setNewCategories(updated);
  };
  const handleActiveCategory = (id: number, index: number) => {
    const updated = newCategories.map((items, indexOuter) => {
      return items.map((item) => {
        return index === indexOuter && item.id === id
          ? {
              ...item,
              active: !item.active,
            }
          : item;
      });
    });
    setNewCategories(updated);
  };
  return (
    <Modal
      size="720"
      background="grey-10"
      type="new"
      header={
        <ModalHeaderNew background="grey-10" title="Create new Item" border onClose={() => {}} />
      }
      footer={
        <ModalFooterNew background="grey-10">
          <Button width="230" onClick={() => {}}>
            Save
          </Button>
        </ModalFooterNew>
      }
    >
      <MenuTabWrapper>
        {tabs.map((tab) => (
          <MenuTab
            key={tab.id}
            id={tab.id}
            active={tab.id === activeTab}
            onClick={() => {
              setActiveTab(tab.id);
            }}
          >
            {tab.title}
          </MenuTab>
        ))}
      </MenuTabWrapper>
      {activeTab === 'general' && (
        <>
          <Container
            flexdirection="column"
            borderRadius="20"
            background="white-100"
            className={classNames(spacing.mt24, spacing.p24)}
            gap="16"
          >
            <Text type="body-regular-14">General</Text>
            <Container flexdirection="row" flexwrap="wrap" gap="24">
              <InputNew
                name="itemName"
                label="Item name"
                width="408"
                required
                onChange={(e) => setItemValue(e.target.value)}
                value={itemValue}
              />
              <SelectNew
                name="unitmeasure"
                onChange={() => {}}
                width="192"
                label="Unit measure"
                required
              >
                {({ onClick, state, selectElement }) => (
                  <SelectDropdown selectElement={selectElement}>
                    <SelectDropdownBody>
                      <SelectDropdownItemList>
                        {options.map(({ label, value }) => (
                          <OptionSingleNew
                            key={value}
                            label={label}
                            selected={state?.value === value}
                            onClick={() =>
                              onClick({
                                label,
                                value,
                              })
                            }
                          />
                        ))}
                      </SelectDropdownItemList>
                    </SelectDropdownBody>
                  </SelectDropdown>
                )}
              </SelectNew>
              <SelectNew
                name="category"
                onAccordionChange={() => {}}
                width="full"
                label="Category"
                required
              >
                {({ selectElement, onClickAccordion }) => (
                  <SelectDropdown selectElement={selectElement}>
                    <SelectDropdownBody>
                      <SelectDropdownItemList>
                        {newCategories.map((item, index) => (
                          <AccordionOption
                            key={index}
                            isOpen={item[0].active}
                            selected={item[0].selected}
                            name={item[0].name}
                            nestingLevel={1}
                            icon={item[0].icon}
                            onClick={() => {
                              handleSelectCategory(item[0].id, index);
                              onClickAccordion([
                                {
                                  text: item[0].name,
                                  icon: item[0].icon,
                                },
                              ]);
                            }}
                            isActive={item[0].active}
                            onIconClick={() => handleActiveCategory(item[0].id, index)}
                          >
                            <AccordionOption
                              selected={item[1].selected}
                              name={item[1].name}
                              icon={item[1].icon}
                              nestingLevel={2}
                              isActive={item[1].active}
                              isOpen={item[1].active}
                              onClick={() => {
                                onClickAccordion([
                                  {
                                    text: item[0].name,
                                    icon: item[0].icon,
                                  },
                                  {
                                    text: item[1].name,
                                    icon: item[1].icon,
                                  },
                                ]);
                                handleSelectCategory(item[1].id, index);
                              }}
                              onIconClick={() => handleActiveCategory(item[1].id, index)}
                            >
                              {item[2] && (
                                <AccordionOption
                                  selected={item[2].selected}
                                  name={item[2].name}
                                  icon={item[2].icon}
                                  nestingLevel={3}
                                  isActive={item[2].active}
                                  isOpen={item[2].active}
                                  onClick={() => {
                                    onClickAccordion([
                                      {
                                        text: item[0].name,
                                        icon: item[0].icon,
                                      },
                                      {
                                        text: item[1].name,
                                        icon: item[1].icon,
                                      },
                                      {
                                        text: item[2].name,
                                        icon: item[2].icon,
                                      },
                                    ]);
                                    handleSelectCategory(item[2].id, index);
                                  }}
                                  onIconClick={() => handleActiveCategory(item[2].id, index)}
                                />
                              )}
                            </AccordionOption>
                          </AccordionOption>
                        ))}
                      </SelectDropdownItemList>
                    </SelectDropdownBody>
                  </SelectDropdown>
                )}
              </SelectNew>
            </Container>
          </Container>
          <Container
            flexdirection="column"
            borderRadius="20"
            background="white-100"
            className={classNames(spacing.mt24, spacing.p24)}
            gap="16"
          >
            <Text type="body-regular-14">Price</Text>
            <Container flexdirection="row" flexwrap="wrap" gap="24">
              <SelectNew name="vendor" onChange={() => {}} width="408" label="Vendor">
                {({ onClick, state, selectElement }) => (
                  <SelectDropdown selectElement={selectElement}>
                    <SelectDropdownBody>
                      <SelectDropdownItemList>
                        {options.map(({ label, value }) => (
                          <OptionSingleNew
                            key={value}
                            label={label}
                            selected={state?.value === value}
                            onClick={() =>
                              onClick({
                                label,
                                value,
                              })
                            }
                          />
                        ))}
                      </SelectDropdownItemList>
                    </SelectDropdownBody>
                    <SelectDropdownFooter>
                      <Container justifycontent="flex-end" className={spacing.m12}>
                        <LinkButton icon={<PlusIcon />} onClick={() => {}} type="text-regular">
                          Create new Vendor
                        </LinkButton>
                      </Container>
                    </SelectDropdownFooter>
                  </SelectDropdown>
                )}
              </SelectNew>
              <SelectNew name="currency" onChange={() => {}} width="192" label="Currency">
                {({ onClick, state, selectElement }) => (
                  <SelectDropdown selectElement={selectElement}>
                    <SelectDropdownBody>
                      <SelectDropdownItemList>
                        {options.map(({ label, value }) => (
                          <OptionSingleNew
                            key={value}
                            label={label}
                            selected={state?.value === value}
                            onClick={() =>
                              onClick({
                                label,
                                value,
                              })
                            }
                          />
                        ))}
                      </SelectDropdownItemList>
                    </SelectDropdownBody>
                  </SelectDropdown>
                )}
              </SelectNew>
              <NumericInput
                name="num-1"
                width="192"
                height="48"
                label="Price, excl taxes"
                value={priceExclTaxesValue}
                onValueChange={handlePriceExclTaxesChange}
              />
              <NumericInput
                name="num-2"
                width="192"
                label="Taxes"
                suffix="%"
                value={taxesValue}
                onValueChange={handleTaxesChange}
              />
              <NumericInput
                name="num-3"
                width="192"
                label="Price, incl taxes"
                value={priceInclTaxesValue}
                onValueChange={handlePriceInclTaxesChange}
              />
            </Container>
          </Container>
          <Container
            flexdirection="column"
            borderRadius="20"
            background="white-100"
            className={classNames(spacing.mt24, spacing.p24)}
            gap="16"
          >
            <Text type="body-regular-14">Add photo</Text>
            <Container flexdirection="row" flexwrap="wrap" gap="24">
              <UploadContainer onDrop={handleImageDrop}>
                <UploadItemField
                  name="uploader"
                  files={images}
                  acceptFilesType="image"
                  limit={5}
                  onChange={handleImageUpload}
                  onDelete={handleImageDelete}
                  onReload={() => {}}
                />
                {!images.length && (
                  <UploadInfo
                    mainText="Upload product logo. Drag and drop or Choose file"
                    secondaryText="JPG; PNG; Maximum file size is 5MB"
                  />
                )}
              </UploadContainer>
            </Container>
          </Container>
        </>
      )}
      {activeTab === 'otherInformation' && (
        <Container
          flexdirection="column"
          borderRadius="20"
          background="white-100"
          className={classNames(spacing.mt24, spacing.p24)}
          gap="16"
        >
          <Text type="body-regular-14">General</Text>
          <Container flexdirection="row" flexwrap="wrap" gap="24">
            <InputNew
              name="sku"
              label="SKU"
              width="300"
              onChange={(e) => setSkuValue(e.target.value)}
              value={skuValue}
            />
            <InputNew
              name="barcode"
              label="Barcode"
              width="300"
              onChange={(e) => setBarcodeValue(e.target.value)}
              value={barcodeValue}
            />
            <TextareaNew
              name="notes"
              label="Notes"
              value={notesValue}
              maxLength={200}
              onChange={(e) => setNotesValue(e.target.value)}
            />
          </Container>
        </Container>
      )}
    </Modal>
  );
};
