import { Meta, Story } from '@storybook/react';
import { DropDown, useDropDown } from 'Components/DropDown';
import { Divider } from 'Components/base/Divider';
import { LoadingLine } from 'Components/base/LoadingLine';
import { Button } from 'Components/base/buttons/Button';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { ContainerMain } from 'Components/base/grid/ContainerMain';
import { MainPageContentContainer } from 'Components/base/grid/MainPageContentContainer';
import { GeneralTab, GeneralTabWrapper } from 'Components/base/tabs/GeneralTab';
import { MenuTab, MenuTabWrapper } from 'Components/base/tabs/MenuTab';
import { Text } from 'Components/base/typography/Text';
import { MainPageEmpty } from 'Components/custom/MainPage/MainPageEmpty';
import { AmountFilter } from 'Components/custom/Stories/AmountFilter/AmountFilter';
import { HeaderSB } from 'Components/custom/Stories/HeaderSB';
import { NavigationSB } from 'Components/custom/Stories/NavigationSB';
import { Pagination } from 'Components/custom/Stories/Pagination/Pagination';
import PerPageSelect from 'Components/custom/Stories/Pagination/PerPageSelect/PerPageSelect';
import { ExportDropdown } from 'Components/elements/ExportDropdown';
import { Logo } from 'Components/elements/Logo';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { FilterDropDown } from 'Components/elements/filters/FilterDropDown';
import { FilterDropDownItem } from 'Components/elements/filters/FilterDropDownItem';
import { FilterItemsContainer } from 'Components/elements/filters/FilterItemsContainer';
import { FilterSearch } from 'Components/elements/filters/FilterSearch';
import { FilterToggleButton } from 'Components/elements/filters/FilterToggleButton';
import { FiltersWrapper } from 'Components/elements/filters/FiltersWrapper';
import { Search } from 'Components/inputs/Search';
import { TableCellWidth } from 'Components/types/gridTypes';
import { rowsPerPageOptions, suggestions } from 'Mocks/fakeOptions';
import {
  CustomColumnType,
  customColumnItems,
  dictionariesTabs,
  filters,
  tableHead,
} from 'Mocks/purchasesFakeData';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as SettingsIcon } from 'Svg/v2/16/settings.svg';
import { ReactComponent as DropdownIcon } from 'Svg/v2/16/three-dots.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import { ReactComponent as UploadIcon } from 'Svg/v2/16/upload.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

const DictionariesItemsMainPageComponent: Story = ({ storyState }) => {
  const [toggle, toggleMenu] = useState<boolean>(false);
  const [showFilters, setShowFilters] = useState<boolean>(false);
  const [searchValue, setSearchValue] = useState('');
  const filteredItems = suggestions.filter((item) => item.bankname.includes(searchValue));
  const [checkedCustomColumns, setCheckedCustomColumns] = useState<CustomColumnType[]>([]);
  const [tableHeadArr, setTableHeadArr] = useState(tableHead);
  const [rowsPerPage, setRowsPerPage] = useState<string>(rowsPerPageOptions[0].value);

  const handleChange = (value: string) => {
    setSearchValue(value);
  };
  const [handleOpen] = useDropDown();

  const handleCheckCustomColumns = (col: CustomColumnType) => () => {
    setCheckedCustomColumns((prevState) => {
      return prevState.some((prev) => prev.id.includes(col.id))
        ? prevState.filter((item) => item.id !== col.id)
        : [...prevState, col];
    });
  };

  const applyCheckedColumns = () => {
    if (handleOpen) {
      handleOpen();
    }
    const updatedTableHead = tableHead.map((item) => {
      return checkedCustomColumns.some((column) => column.title === item.title)
        ? {
            ...item,
            active: !item.active,
          }
        : item;
    });
    setTableHeadArr(updatedTableHead);
  };
  return (
    <Container fullscreen flexwrap="nowrap" background={'grey-10'}>
      <NavigationSB opened={toggle} onToggleOpen={() => toggleMenu(!toggle)} />
      <ContainerMain navOpened={toggle}>
        <HeaderSB />
        <Container flexdirection="column" className={spacing.mX32}>
          <Text type="h1-semibold">Paidlog</Text>
          <MenuTabWrapper className={spacing.mt24}>
            <MenuTab id="paymentRequest" active={false} onClick={() => {}}>
              Payment request
            </MenuTab>
            <MenuTab id="approvals" active={false} onClick={() => {}}>
              Approvals
            </MenuTab>
            <MenuTab id="payments" active={false} onClick={() => {}}>
              Payments
            </MenuTab>
            <MenuTab id="dictionaries" active onClick={() => {}}>
              Dictionaries
            </MenuTab>
          </MenuTabWrapper>
        </Container>
        <MainPageContentContainer>
          {storyState === 'loaded' && (
            <>
              <Container
                className={classNames(spacing.mX24, spacing.mt24)}
                justifycontent="space-between"
                gap="24"
              >
                <Container gap="24" alignitems="center">
                  <FilterToggleButton
                    showFilters={showFilters}
                    filtersSelected={false}
                    onClear={() => {}}
                    onClick={() => {
                      setShowFilters((prevState) => !prevState);
                    }}
                  />
                  <Search
                    width="320"
                    height="40"
                    name="search-paidlog-vendors-mainPage"
                    placeholder="Search by Name or SKU"
                    value=""
                    onChange={() => {}}
                    searchParams={[]}
                    onSearch={() => {}}
                    onClear={() => {}}
                  />
                </Container>
                <Container gap="24" alignitems="center">
                  <DropDown
                    padding="none"
                    control={({ handleOpen }) => (
                      <IconButton
                        size="40"
                        background={'grey-10-grey-20'}
                        color="grey-100"
                        icon={<SettingsIcon />}
                        onClick={handleOpen}
                      />
                    )}
                  >
                    <Container flexdirection="column" className={spacing.w320fixed}>
                      <Text
                        type="caption-semibold"
                        color="grey-100"
                        className={classNames(spacing.mt24, spacing.mX16)}
                      >
                        Add Custom columns
                      </Text>
                      <FilterItemsContainer
                        className={classNames(spacing.mt14, spacing.mb16, spacing.mX12)}
                      >
                        <FilterDropDownItem
                          id="selectAll"
                          type="item"
                          checked={checkedCustomColumns.length === customColumnItems.length}
                          blurred={false}
                          value="Select All"
                          onChange={() =>
                            setCheckedCustomColumns(
                              checkedCustomColumns.length === customColumnItems.length
                                ? []
                                : customColumnItems,
                            )
                          }
                        />
                        {customColumnItems.map(({ id, title }) => (
                          <FilterDropDownItem
                            key={id}
                            id={id}
                            type="item"
                            checked={checkedCustomColumns.some((item) => item.id === id)}
                            blurred={false}
                            value={title}
                            onChange={handleCheckCustomColumns({
                              id,
                              title,
                            })}
                          />
                        ))}
                      </FilterItemsContainer>
                      <Divider fullHorizontalWidth />
                      <Container
                        className={classNames(spacing.m16, spacing.w288fixed)}
                        justifycontent="space-between"
                        alignitems="center"
                        flexdirection="row-reverse"
                      >
                        <Button
                          width="auto"
                          height="32"
                          padding="8"
                          font="text-medium"
                          onClick={applyCheckedColumns}
                          background={'violet-90-violet-100'}
                          color="white-100"
                        >
                          Apply
                        </Button>
                        <Button
                          width="auto"
                          height="32"
                          padding="8"
                          font="text-medium"
                          onClick={() => {}}
                          background={'blue-10-red-10'}
                          color="violet-90-red-90"
                        >
                          Clear all
                        </Button>
                      </Container>
                    </Container>
                  </DropDown>
                  <Divider type="vertical" background={'grey-90'} height="20" />
                  <ExportDropdown onExportToXLS={() => {}} onExportToCSV={() => {}} />
                  <IconButton
                    size="40"
                    background={'grey-10-grey-20'}
                    color="grey-100"
                    icon={<UploadIcon />}
                    tooltip="Import files"
                    onClick={() => {}}
                  />

                  <Button
                    background={'violet-90-violet-100'}
                    color="white-100"
                    width="140"
                    height="40"
                    iconLeft={<PlusIcon />}
                  >
                    Create
                  </Button>
                </Container>
              </Container>
              <FiltersWrapper active={showFilters}>
                {filters.map((item) => (
                  <FilterDropDown
                    key={item.title}
                    title={item.title}
                    value="All"
                    onClear={() => {}}
                    selectedAmount={0}
                  >
                    <Container
                      className={classNames(spacing.m16, spacing.w288fixed)}
                      flexdirection="column"
                    >
                      <FilterSearch
                        title="Account type:"
                        value={searchValue}
                        onChange={handleChange}
                      />
                      <FilterItemsContainer className={spacing.mt8}>
                        {filteredItems.map((item) => (
                          <FilterDropDownItem
                            key={item.id}
                            id={item.id}
                            type="item"
                            checked={false}
                            blurred={false}
                            value={item.bankname}
                            onChange={() => {}}
                          />
                        ))}
                      </FilterItemsContainer>
                    </Container>
                    <Divider fullHorizontalWidth />
                    <Container
                      className={classNames(spacing.m16, spacing.w288fixed)}
                      justifycontent="flex-end"
                      alignitems="center"
                    >
                      <Button
                        width="auto"
                        height="32"
                        padding="8"
                        font="text-medium"
                        onClick={() => {}}
                        background={'violet-90-violet-100'}
                        color="white-100"
                      >
                        Apply
                      </Button>
                    </Container>
                  </FilterDropDown>
                ))}
                <FilterDropDown
                  title="Price, incl. taxes"
                  value="All"
                  selectedAmount={0}
                  onClear={() => {}}
                >
                  <AmountFilter />
                </FilterDropDown>
              </FiltersWrapper>
              <GeneralTabWrapper className={classNames(spacing.mX24, spacing.mt24)}>
                {dictionariesTabs.map(({ name, active }) => (
                  <GeneralTab key={name} id={name} active={active} onClick={() => {}}>
                    {name}
                  </GeneralTab>
                ))}
              </GeneralTabWrapper>
              <TableNew
                noResults={false}
                className={classNames(spacing.mt24, spacing.ml24, spacing.pr24)}
                tableHead={
                  <TableHeadNew>
                    {tableHeadArr.map((item, index) => {
                      if (!item.active) return null;
                      return (
                        <TableTitleNew
                          key={index}
                          minWidth={item.minWidth as TableCellWidth}
                          align={item.align as TableTitleNewProps['align']}
                          fixedWidth
                          title={item.title}
                          sorting={item.sorting}
                          onClick={() => {}}
                        />
                      );
                    })}
                  </TableHeadNew>
                }
              >
                <TableBodyNew>
                  {Array.from(Array(9)).map((_, index) => (
                    <TableRowNew key={index} gap="8">
                      <TableCellNew
                        minWidth={tableHead[0].minWidth as TableCellWidth}
                        justifycontent="center"
                        alignitems="center"
                        fixedWidth
                        padding="8"
                      >
                        <Logo
                          size="44"
                          content="picture"
                          name="picture"
                          img="https://picsum.photos/60"
                          color="#6367F6"
                        />
                      </TableCellNew>
                      <TableCellTextNew
                        minWidth={tableHead[1].minWidth as TableCellWidth}
                        type="caption-regular"
                        value="MacBook Air 13"
                        secondaryColor="grey-100"
                        secondaryValue="LP0003453"
                        noWrap
                        lineClamp="none"
                        alignitems="flex-start"
                        fixedWidth
                        padding="8"
                      />
                      <TableCellTextNew
                        minWidth={tableHead[2].minWidth as TableCellWidth}
                        value="345675443223"
                        type="caption-regular"
                        fixedWidth
                        justifycontent="flex-start"
                        alignitems="flex-start"
                        padding="8"
                      />
                      {tableHeadArr[3].active && (
                        <TableCellTextNew
                          minWidth={tableHead[3].minWidth as TableCellWidth}
                          value="Pcs"
                          type="caption-regular"
                          fixedWidth
                          justifycontent="flex-start"
                          alignitems="flex-start"
                          padding="8"
                        />
                      )}
                      {tableHeadArr[4].active && (
                        <TableCellTextNew
                          minWidth={tableHead[4].minWidth as TableCellWidth}
                          value="Inventory"
                          type="caption-regular"
                          fixedWidth
                          justifycontent="flex-start"
                          alignitems="flex-start"
                          padding="8"
                        />
                      )}
                      <TableCellTextNew
                        minWidth={tableHead[5].minWidth as TableCellWidth}
                        value="Office equipment"
                        type="caption-regular"
                        justifycontent="flex-start"
                        alignitems="flex-start"
                        fixedWidth
                        padding="8"
                        noWrap
                        lineClamp="none"
                      />
                      <TableCellTextNew
                        minWidth={tableHead[6].minWidth as TableCellWidth}
                        value="Rozetka.ua"
                        type="caption-regular"
                        justifycontent="flex-start"
                        alignitems="flex-start"
                        fixedWidth
                        padding="8"
                        noWrap
                        lineClamp="none"
                      />
                      {tableHeadArr[7].active && (
                        <TableCellTextNew
                          minWidth={tableHead[7].minWidth as TableCellWidth}
                          value="Free shipping"
                          type="caption-regular"
                          justifycontent="flex-start"
                          alignitems="flex-start"
                          fixedWidth
                          padding="8"
                          noWrap
                          lineClamp="none"
                        />
                      )}
                      <TableCellTextNew
                        minWidth={tableHead[8].minWidth as TableCellWidth}
                        value="USD"
                        type="caption-regular"
                        justifycontent="flex-start"
                        alignitems="flex-start"
                        fixedWidth
                        padding="8"
                      />
                      <TableCellTextNew
                        minWidth={tableHead[9].minWidth as TableCellWidth}
                        value="1.256.99 $"
                        type="caption-regular"
                        alignitems="flex-end"
                        fixedWidth
                        padding="8"
                      />
                      <TableCellTextNew
                        minWidth={tableHead[10].minWidth as TableCellWidth}
                        value="10 %"
                        type="caption-regular"
                        alignitems="flex-end"
                        fixedWidth
                        padding="8"
                      />
                      <TableCellTextNew
                        minWidth={tableHead[11].minWidth as TableCellWidth}
                        value="1.382.99 $"
                        type="caption-regular"
                        alignitems="flex-end"
                        fixedWidth
                        padding="8"
                      />
                      <TableCellNew
                        minWidth={tableHead[12].minWidth as TableCellWidth}
                        justifycontent="center"
                        alignitems="center"
                        fixedWidth
                        padding="8"
                      >
                        <DropDown
                          flexdirection="column"
                          padding="8"
                          control={({ handleOpen }) => (
                            <IconButton
                              icon={<DropdownIcon />}
                              onClick={handleOpen}
                              background={'transparent'}
                              color="grey-100-violet-90"
                            />
                          )}
                        >
                          <DropDownButton size="160" icon={<EditIcon />} onClick={() => {}}>
                            Edit
                          </DropDownButton>
                          <DropDownButton
                            size="160"
                            type="danger"
                            icon={<TrashIcon />}
                            onClick={() => {}}
                          >
                            Delete
                          </DropDownButton>
                        </DropDown>
                      </TableCellNew>
                    </TableRowNew>
                  ))}
                </TableBodyNew>
              </TableNew>
              <Pagination
                customPagination={
                  <Container gap="16" alignitems="center">
                    <Text type="subtext-regular" color="grey-100">
                      Rows per Page
                    </Text>
                    <PerPageSelect
                      selectedValue={rowsPerPage}
                      onOptionClick={(value) => setRowsPerPage(value)}
                    />
                  </Container>
                }
              />
            </>
          )}
          {storyState === 'empty' && (
            <>
              <GeneralTabWrapper className={classNames(spacing.mX24, spacing.mt24)}>
                {dictionariesTabs.map(({ name, active }) => (
                  <GeneralTab key={name} id={name} active={active} onClick={() => {}}>
                    {name}
                  </GeneralTab>
                ))}
              </GeneralTabWrapper>
              <MainPageEmpty
                title="Create new Items"
                description="You have no items created. Your items will be displayed here."
                action={
                  <Button
                    width="220"
                    height="40"
                    background={'blue-10-blue-20'}
                    color="violet-90-violet-100"
                    iconLeft={<PlusIcon />}
                    onClick={() => {}}
                  >
                    Create new Items
                  </Button>
                }
              />
            </>
          )}
        </MainPageContentContainer>
        <LoadingLine loadingCompleted={storyState !== 'loading'} />
      </ContainerMain>
    </Container>
  );
};
export default {
  title: 'Pages/Paidlog/Dictionaries/DictionariesItemsMainPage',
  component: DictionariesItemsMainPageComponent,
  argTypes: {
    storyState: {
      options: ['empty', 'loading', 'loaded'],
      control: {
        type: 'radio',
        labels: {
          empty: 'Empty page',
          loading: 'Page is loading',
          loaded: 'Page loaded',
        },
      },
    },
  },
} as Meta;

export const DictionariesItemsMainPage = DictionariesItemsMainPageComponent.bind({});
DictionariesItemsMainPage.args = {
  storyState: 'loaded',
};
