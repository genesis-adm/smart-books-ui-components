import { Meta } from '@storybook/react';
import { Icon } from 'Components/base/Icon';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { TextareaNew } from 'Components/base/inputs/TextareaNew';
import { Text } from 'Components/base/typography/Text';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ReactComponent as DialogIcon } from 'Svg/dialog.svg';
import classNames from 'classnames';
import React, { FC } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Paidlog/Approvals/ApprovalsModals/Document Rejection Modal',
  argTypes: {
    taxAgent: {
      control: { type: 'boolean' },
    },
    marketplace: {
      control: { type: 'boolean' },
    },
  },
  args: {
    taxAgent: true,
    marketplace: true,
  },
} as Meta;

type DocumentRejectionModalType = {
  onClick?(): void;
};

export const DocumentRejectionModal: FC<DocumentRejectionModalType> = ({ onClick }) => {
  return (
    <Modal
      size="580"
      height="540"
      pluginScrollDisabled
      footer={
        <ModalFooterNew justifycontent="space-evenly">
          <Button width="196" height="48" onClick={onClick}>
            Send
          </Button>
          <Button
            width="196"
            height="48"
            background="white-100"
            color="grey-100"
            border="grey-20"
            onClick={onClick}
          >
            Cancel
          </Button>
        </ModalFooterNew>
      }
    >
      <Container
        flexdirection="column"
        alignitems="center"
        justifycontent="center"
        gap="30"
        className={classNames(spacing.pX24, spacing.pY40)}
      >
        <Icon icon={<DialogIcon />} />
        <Container
          className={classNames(spacing.w350fixed)}
          flexdirection="column"
          alignitems="center"
          justifycontent="center"
          gap="12"
        >
          <Text type="title-bold">Document rejection</Text>
          <Text type="body-regular-14" color="grey-100" align="center">
            Please provide a comment starting with the reason for a rejection
          </Text>
        </Container>
        <TextareaNew
          minHeight="155"
          maxLength={200}
          name="comment"
          label="Comment"
          value=""
          onChange={() => {}}
        />
      </Container>
    </Modal>
  );
};
