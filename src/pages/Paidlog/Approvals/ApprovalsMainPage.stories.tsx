import { Meta, Story } from '@storybook/react';
import { DropDown, useDropDown } from 'Components/DropDown';
import { ActionBar } from 'Components/base/ActionBar';
import { Checkbox } from 'Components/base/Checkbox';
import { Divider } from 'Components/base/Divider';
import { Icon } from 'Components/base/Icon';
import { Tag } from 'Components/base/Tag';
import { TagSegmented } from 'Components/base/TagSegmented';
import { Button } from 'Components/base/buttons/Button';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { ContainerMain } from 'Components/base/grid/ContainerMain';
import { MainPageContentContainer } from 'Components/base/grid/MainPageContentContainer';
import { GeneralTab, GeneralTabWrapper } from 'Components/base/tabs/GeneralTab';
import { MenuTab, MenuTabWrapper } from 'Components/base/tabs/MenuTab';
import { Text } from 'Components/base/typography/Text';
import { HeaderSB } from 'Components/custom/Stories/HeaderSB';
import { NavigationSB } from 'Components/custom/Stories/NavigationSB';
import { Pagination } from 'Components/custom/Stories/Pagination/Pagination';
import PerPageSelect from 'Components/custom/Stories/Pagination/PerPageSelect/PerPageSelect';
import { ExportDropdown } from 'Components/elements/ExportDropdown';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { UserInfo } from 'Components/elements/UserInfo';
import { FilterDropDown } from 'Components/elements/filters/FilterDropDown';
import { FilterDropDownItem } from 'Components/elements/filters/FilterDropDownItem';
import { FilterItemsContainer } from 'Components/elements/filters/FilterItemsContainer';
import { FilterSearch } from 'Components/elements/filters/FilterSearch';
import { FilterToggleButton } from 'Components/elements/filters/FilterToggleButton';
import { FiltersWrapper } from 'Components/elements/filters/FiltersWrapper';
import { Search } from 'Components/inputs/Search';
import { TableCellWidth } from 'Components/types/gridTypes';
import { rowsPerPageOptions } from 'Mocks/fakeOptions';
import {
  CustomColumnType,
  approvalsCustomColumnItems,
  approvalsTableHead,
  bankAccounts,
  billsAndApprovalsFilters,
  billsAndApprovalsTabs,
  filterOptions,
} from 'Mocks/purchasesFakeData';
import { DocumentRejectionModal } from 'Pages/Paidlog/Approvals/ApprovalsModals/DocumentRejectionModal.stories';
import { ReactComponent as RejectedIcon } from 'Svg/16/minus-circle.svg';
import { ReactComponent as MinusIcon } from 'Svg/16/minus-light.svg';
import { ReactComponent as ReplyIcon } from 'Svg/16/reply.svg';
import { ReactComponent as ArrowRightIcon } from 'Svg/v2/16/arrow-right.svg';
import { ReactComponent as ProcessingIcon } from 'Svg/v2/16/clock-filled.svg';
import { ReactComponent as SettingsIcon } from 'Svg/v2/16/settings.svg';
import { ReactComponent as DropdownIcon } from 'Svg/v2/16/three-dots.svg';
import { ReactComponent as AcceptedIcon } from 'Svg/v2/16/tick-in-filled-circle.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

const ApprovalsMainPageComponent: Story = ({ storyState }) => {
  const [toggle, toggleMenu] = useState<boolean>(false);
  const [showFilters, setShowFilters] = useState<boolean>(false);
  const [checkedCustomColumns, setCheckedCustomColumns] = useState<CustomColumnType[]>([]);
  const [tableHeadArr, setTableHeadArr] = useState(approvalsTableHead);
  const [searchValue, setSearchValue] = useState('');
  const filteredItems = filterOptions.filter((item) => item.option.includes(searchValue));
  const [checkedRows, setCheckedRows] = useState<number[]>([]);
  const [isOpenDocumentRejectionModal, setIsOpenDocumentRejectionModal] = useState<boolean>(false);
  const [handleOpen] = useDropDown();
  const [rowsPerPage, setRowsPerPage] = useState<string>(rowsPerPageOptions[0].value);

  const handleCheckRow = (rowIndex: number) => {
    setCheckedRows((prevState) => {
      return prevState.some((prev) => prev === rowIndex)
        ? prevState.filter((item) => item !== rowIndex)
        : [...prevState, rowIndex];
    });
  };
  const handleOpenDocumentRejectionModal = () => {
    setIsOpenDocumentRejectionModal(!isOpenDocumentRejectionModal);
  };
  const handleChange = (value: string) => {
    setSearchValue(value);
  };
  const handleCheckCustomColumns = (col: CustomColumnType) => () => {
    setCheckedCustomColumns((prevState) => {
      return prevState.some((prev) => prev.id.includes(col.id))
        ? prevState.filter((item) => item.id !== col.id)
        : [...prevState, col];
    });
  };

  const applyCheckedColumns = () => {
    if (handleOpen) {
      handleOpen();
    }
    const updatedTableHead = approvalsTableHead.map((item) => {
      return checkedCustomColumns.some((column) => column.title === item.title)
        ? {
            ...item,
            active: !item.active,
          }
        : item;
    });
    setTableHeadArr(updatedTableHead);
  };

  return (
    <Container fullscreen flexwrap="nowrap" background={'grey-10'}>
      <NavigationSB opened={toggle} onToggleOpen={() => toggleMenu(!toggle)} />
      <ContainerMain navOpened={toggle}>
        <HeaderSB />
        <Container flexdirection="column" className={spacing.mX32}>
          <Text type="h1-semibold">Paidlog</Text>
          <MenuTabWrapper className={spacing.mt24}>
            <MenuTab id="paymentRequest" active={false} onClick={() => {}}>
              Payment request
            </MenuTab>
            <MenuTab id="approvals" active onClick={() => {}}>
              Approvals
            </MenuTab>
            <MenuTab id="payments" active={false} onClick={() => {}}>
              Payments
            </MenuTab>
            <MenuTab id="dictionaries" active={false} onClick={() => {}}>
              Dictionaries
            </MenuTab>
          </MenuTabWrapper>
        </Container>
        <MainPageContentContainer>
          {storyState === 'loaded' && (
            <>
              <Container
                className={classNames(spacing.mX24, spacing.mt24)}
                justifycontent="space-between"
                gap="24"
              >
                <Container gap="24" alignitems="center">
                  <FilterToggleButton
                    showFilters={showFilters}
                    filtersSelected={false}
                    onClear={() => {}}
                    onClick={() => {
                      setShowFilters((prevState) => !prevState);
                    }}
                  />
                  <Search
                    width="320"
                    height="40"
                    name="search-paidlog-vendors-mainPage"
                    placeholder="Search by ID or Vendor"
                    value=""
                    onChange={() => {}}
                    searchParams={[]}
                    onSearch={() => {}}
                    onClear={() => {}}
                  />
                </Container>
                <Container gap="24" alignitems="center">
                  <DropDown
                    padding="none"
                    control={({ handleOpen }) => (
                      <IconButton
                        size="40"
                        background={'grey-10-grey-20'}
                        color="grey-100"
                        icon={<SettingsIcon />}
                        onClick={handleOpen}
                      />
                    )}
                  >
                    <Container flexdirection="column" className={spacing.w320fixed}>
                      <Text
                        type="caption-semibold"
                        color="grey-100"
                        className={classNames(spacing.mt24, spacing.mX16)}
                      >
                        Add Custom columns
                      </Text>
                      <FilterItemsContainer
                        className={classNames(spacing.mt14, spacing.mb16, spacing.mX12)}
                      >
                        <FilterDropDownItem
                          id="selectAll"
                          type="item"
                          checked={
                            checkedCustomColumns.length === approvalsCustomColumnItems.length
                          }
                          blurred={false}
                          value="Select All"
                          onChange={() =>
                            setCheckedCustomColumns(
                              checkedCustomColumns.length === approvalsCustomColumnItems.length
                                ? []
                                : approvalsCustomColumnItems,
                            )
                          }
                        />
                        {approvalsCustomColumnItems.map(({ id, title }) => (
                          <FilterDropDownItem
                            key={id}
                            id={id}
                            type="item"
                            checked={checkedCustomColumns.some((item) => item.id === id)}
                            blurred={false}
                            value={title}
                            onChange={handleCheckCustomColumns({
                              id,
                              title,
                            })}
                          />
                        ))}
                      </FilterItemsContainer>
                      <Divider fullHorizontalWidth />
                      <Container
                        className={classNames(spacing.m16, spacing.w288fixed)}
                        justifycontent="space-between"
                        alignitems="center"
                        flexdirection="row-reverse"
                      >
                        <Button
                          width="auto"
                          height="32"
                          padding="8"
                          font="text-medium"
                          onClick={applyCheckedColumns}
                          background={'violet-90-violet-100'}
                          color="white-100"
                        >
                          Apply
                        </Button>
                        <Button
                          width="auto"
                          height="32"
                          padding="8"
                          font="text-medium"
                          onClick={() => {}}
                          background={'blue-10-red-10'}
                          color="violet-90-red-90"
                        >
                          Clear all
                        </Button>
                      </Container>
                    </Container>
                  </DropDown>
                  <Divider type="vertical" background={'grey-90'} height="20" />
                  <ExportDropdown onExportToXLS={() => {}} onExportToCSV={() => {}} />
                </Container>
              </Container>
              <FiltersWrapper active={showFilters}>
                {billsAndApprovalsFilters.map((item) => (
                  <FilterDropDown
                    key={item.title}
                    title={item.title}
                    value="All"
                    onClear={() => {}}
                    selectedAmount={0}
                  >
                    {item.component}
                    {!item.component && (
                      <>
                        <Container
                          className={classNames(spacing.m16, spacing.w288fixed)}
                          flexdirection="column"
                        >
                          <FilterSearch
                            title="Account type:"
                            value={searchValue}
                            onChange={handleChange}
                          />
                          <FilterItemsContainer className={spacing.mt8}>
                            {filteredItems.map((item) => (
                              <FilterDropDownItem
                                key={item.id}
                                id={item.id}
                                type="item"
                                checked={false}
                                blurred={false}
                                value={item.option}
                                onChange={() => {}}
                              />
                            ))}
                          </FilterItemsContainer>
                        </Container>
                        <Divider fullHorizontalWidth />
                        <Container
                          className={classNames(spacing.m16, spacing.w288fixed)}
                          justifycontent="flex-end"
                          alignitems="center"
                        >
                          <Button
                            width="auto"
                            height="32"
                            padding="8"
                            font="text-medium"
                            onClick={() => {}}
                            background={'violet-90-violet-100'}
                            color="white-100"
                          >
                            Apply
                          </Button>
                        </Container>
                      </>
                    )}
                  </FilterDropDown>
                ))}
              </FiltersWrapper>
              <GeneralTabWrapper className={classNames(spacing.mX24, spacing.mt24)}>
                {billsAndApprovalsTabs.map(({ name, active, icon }) => (
                  <GeneralTab key={name} id={name} active={active} icon={icon} onClick={() => {}}>
                    {name}
                  </GeneralTab>
                ))}
              </GeneralTabWrapper>
              <TableNew
                noResults={false}
                className={classNames(spacing.mt24, spacing.ml24, spacing.pr24)}
                tableHead={
                  <TableHeadNew>
                    {tableHeadArr.map((item, index) => {
                      if (!item.active) return null;
                      return (
                        <TableTitleNew
                          key={index}
                          minWidth={item.minWidth as TableCellWidth}
                          align={item.align as TableTitleNewProps['align']}
                          padding={item.padding as TableTitleNewProps['padding']}
                          title={item.title}
                          sorting={item.sorting}
                          onClick={() => {}}
                        />
                      );
                    })}
                  </TableHeadNew>
                }
              >
                <TableBodyNew>
                  {Array.from(Array(9)).map((_, index) => (
                    <TableRowNew key={index} gap="8">
                      <TableCellNew
                        minWidth={approvalsTableHead[0].minWidth as TableCellWidth}
                        justifycontent="center"
                        alignitems="center"
                        fixedWidth
                      >
                        <Checkbox
                          name={`line-${index}`}
                          checked={checkedRows.some((item) => item === index)}
                          onChange={() => {
                            handleCheckRow(index);
                          }}
                        />
                      </TableCellNew>
                      <TableCellTextNew
                        minWidth={approvalsTableHead[1].minWidth as TableCellWidth}
                        value="ID 000000"
                        secondaryValue="12 July 2023"
                        fixedWidth
                        justifycontent="flex-start"
                      />
                      <TableCellNew
                        minWidth={approvalsTableHead[2].minWidth as TableCellWidth}
                        fixedWidth
                        justifycontent="flex-start"
                      >
                        <UserInfo name="VitaliyKvasha" isLogo secondaryText="VitalyKV@gmail.com" />
                      </TableCellNew>
                      <TableCellTextNew
                        minWidth={approvalsTableHead[3].minWidth as TableCellWidth}
                        value="Rozetka"
                        secondaryValue="Company"
                        noWrap
                        lineClamp="none"
                        justifycontent="flex-start"
                        fixedWidth
                      />
                      <TableCellNew
                        minWidth={approvalsTableHead[4].minWidth as TableCellWidth}
                        gap="8"
                        fixedWidth
                        justifycontent="flex-start"
                        alignitems="flex-start"
                      >
                        <Tag text="Furniture" color="violet" />
                        <Tag text="+4" color="violet" />
                      </TableCellNew>
                      <TableCellTextNew
                        minWidth={approvalsTableHead[5].minWidth as TableCellWidth}
                        value="15 July 2023"
                        fixedWidth
                        justifycontent="flex-start"
                        alignitems="flex-start"
                      />
                      <TableCellTextNew
                        minWidth={approvalsTableHead[6].minWidth as TableCellWidth}
                        value="EUR"
                        fixedWidth
                        justifycontent="center"
                        alignitems="center"
                        aligncontent="center"
                      />
                      <TableCellTextNew
                        minWidth={approvalsTableHead[7].minWidth as TableCellWidth}
                        value="1,200.00 €"
                        type="caption-semibold"
                        secondaryValue="1,323.04 $"
                        secondaryColor="grey-90"
                        secondaryType="subtext-semibold"
                        align="right"
                        secondaryAlign="right"
                        fixedWidth
                      />
                      {tableHeadArr[8].active && (
                        <TableCellTextNew
                          minWidth={approvalsTableHead[8].minWidth as TableCellWidth}
                          value="800.00 €"
                          type="caption-semibold"
                          secondaryValue="796.04 $"
                          secondaryColor="grey-90"
                          secondaryType="subtext-semibold"
                          align="right"
                          secondaryAlign="right"
                          fixedWidth
                        />
                      )}
                      {tableHeadArr[9].active && (
                        <TableCellTextNew
                          minWidth={approvalsTableHead[9].minWidth as TableCellWidth}
                          value="400.00 €"
                          type="caption-semibold"
                          secondaryValue="396.04 $"
                          secondaryColor="grey-90"
                          secondaryType="subtext-semibold"
                          align="right"
                          secondaryAlign="right"
                          fixedWidth
                        />
                      )}
                      <TableCellNew
                        minWidth={approvalsTableHead[10].minWidth as TableCellWidth}
                        fixedWidth
                        justifycontent="center"
                      >
                        <Tag text="Approved" color="green" icon={<AcceptedIcon />} />
                      </TableCellNew>
                      {tableHeadArr[11].active && (
                        <TableCellNew
                          minWidth={approvalsTableHead[11].minWidth as TableCellWidth}
                          fixedWidth
                          justifycontent="center"
                        >
                          <Tag text="Partially paid" color="yellow" />
                        </TableCellNew>
                      )}
                      {tableHeadArr[12].active && (
                        <TableCellTextNew
                          minWidth={approvalsTableHead[12].minWidth as TableCellWidth}
                          value="BetterMe International Limited"
                          fixedWidth
                          noWrap
                          lineClamp="none"
                          padding="8"
                        />
                      )}
                      {tableHeadArr[13].active && (
                        <TableCellNew
                          minWidth={approvalsTableHead[13].minWidth as TableCellWidth}
                          fixedWidth
                          padding="8"
                        >
                          <TagSegmented
                            {...bankAccounts}
                            tooltipMessage={
                              <Container flexdirection="column">
                                <Text color="white-100" style={{ wordBreak: 'break-all' }}>
                                  {bankAccounts.mainLabel}
                                </Text>
                                <Text color="white-100" style={{ wordBreak: 'break-all' }}>
                                  {bankAccounts.secondLabel} | {bankAccounts.thirdLabel}
                                </Text>
                              </Container>
                            }
                          />
                        </TableCellNew>
                      )}
                      {tableHeadArr[14].active && (
                        <TableCellNew
                          minWidth={approvalsTableHead[14].minWidth as TableCellWidth}
                          alignitems="center"
                          fixedWidth
                          padding="8"
                        >
                          <Text color="grey-100">Card</Text>
                          <Icon icon={<ArrowRightIcon transform="scale(0.5)" />} path="inherit" />
                          <TagSegmented
                            {...bankAccounts}
                            tooltipMessage={
                              <Container flexdirection="column">
                                <Text color="white-100" style={{ wordBreak: 'break-all' }}>
                                  {bankAccounts.mainLabel}
                                </Text>
                                <Text color="white-100" style={{ wordBreak: 'break-all' }}>
                                  {bankAccounts.secondLabel} | {bankAccounts.thirdLabel}
                                </Text>
                              </Container>
                            }
                          />
                        </TableCellNew>
                      )}
                      {tableHeadArr[15].active && (
                        <TableCellNew
                          minWidth={approvalsTableHead[15].minWidth as TableCellWidth}
                          fixedWidth
                          padding="8"
                        >
                          <UserInfo name="Dinys Vlasov" isLogo secondaryText="Dinys@gmail.com" />
                        </TableCellNew>
                      )}
                      <TableCellNew
                        minWidth={approvalsTableHead[16].minWidth as TableCellWidth}
                        justifycontent="center"
                        alignitems="center"
                        fixedWidth
                      >
                        <DropDown
                          padding="8"
                          control={({ handleOpen }) => (
                            <IconButton
                              icon={<DropdownIcon />}
                              onClick={handleOpen}
                              background={'transparent'}
                              color="grey-100-violet-90"
                            />
                          )}
                        >
                          <DropDownButton size="160" icon={<ReplyIcon />} onClick={() => {}}>
                            Cancel
                          </DropDownButton>
                        </DropDown>
                      </TableCellNew>
                    </TableRowNew>
                  ))}
                  <TableRowNew gap="8">
                    <TableCellNew
                      minWidth={approvalsTableHead[0].minWidth as TableCellWidth}
                      justifycontent="center"
                      alignitems="center"
                    >
                      <Checkbox
                        name={`line-10`}
                        checked={checkedRows.some((item) => item === 10)}
                        onChange={() => {
                          handleCheckRow(10);
                        }}
                      />
                    </TableCellNew>
                    <TableCellTextNew
                      minWidth={approvalsTableHead[1].minWidth as TableCellWidth}
                      value="ID 000000"
                      secondaryValue="12 July 2023"
                    />
                    <TableCellNew
                      minWidth={approvalsTableHead[2].minWidth as TableCellWidth}
                      fixedWidth
                    >
                      <UserInfo
                        name="VitaliyKvashaefjcdrfifdjrfg;djrgvidgjirgibjgikkktf"
                        isLogo
                        secondaryText="VitalyKV@gmail.comsdjdcjdjkdjdjsdhjds"
                      />
                    </TableCellNew>
                    <TableCellTextNew
                      minWidth={approvalsTableHead[3].minWidth as TableCellWidth}
                      value="Rozetkadfcjsdhjsdhfjfhjdhfr"
                      secondaryValue="Company"
                      noWrap
                      lineClamp="none"
                    />
                    <TableCellNew
                      minWidth={approvalsTableHead[4].minWidth as TableCellWidth}
                      gap="8"
                    >
                      <Tag text="Furniture" color="violet" />
                      <Tag text="+4" color="violet" />
                    </TableCellNew>
                    <TableCellTextNew
                      minWidth={approvalsTableHead[5].minWidth as TableCellWidth}
                      value="15 July 2023"
                    />
                    <TableCellTextNew
                      minWidth={approvalsTableHead[6].minWidth as TableCellWidth}
                      value="EUR"
                      justifycontent="center"
                      alignitems="center"
                    />
                    <TableCellTextNew
                      minWidth={approvalsTableHead[7].minWidth as TableCellWidth}
                      value="1,200.00 €"
                      type="caption-semibold"
                      secondaryValue="1,323.04 $"
                      secondaryColor="grey-90"
                      secondaryType="subtext-semibold"
                      align="right"
                      secondaryAlign="right"
                    />
                    {tableHeadArr[8].active && (
                      <TableCellTextNew
                        minWidth={approvalsTableHead[8].minWidth as TableCellWidth}
                        value="800.00 €"
                        type="caption-semibold"
                        secondaryValue="796.04 $"
                        secondaryColor="grey-90"
                        secondaryType="subtext-semibold"
                        align="right"
                        secondaryAlign="right"
                      />
                    )}
                    {tableHeadArr[9].active && (
                      <TableCellTextNew
                        minWidth={approvalsTableHead[9].minWidth as TableCellWidth}
                        value="400.00 €"
                        type="caption-semibold"
                        secondaryValue="396.04 $"
                        secondaryColor="grey-90"
                        secondaryType="subtext-semibold"
                        align="right"
                        secondaryAlign="right"
                      />
                    )}
                    <TableCellNew minWidth={approvalsTableHead[10].minWidth as TableCellWidth}>
                      <Tag text="Awaiting for approval" color="grey" icon={<ProcessingIcon />} />
                    </TableCellNew>
                    {tableHeadArr[11].active && (
                      <TableCellNew
                        minWidth={approvalsTableHead[11].minWidth as TableCellWidth}
                        justifycontent="center"
                      >
                        <Tag text="Partially paid" color="yellow" />
                      </TableCellNew>
                    )}
                    {tableHeadArr[12].active && (
                      <TableCellTextNew
                        minWidth={approvalsTableHead[12].minWidth as TableCellWidth}
                        value="BetterMe International Limitedtgrtfgjugveriughuerghrug"
                        fixedWidth
                        noWrap
                        lineClamp="none"
                      />
                    )}
                    {tableHeadArr[13].active && (
                      <TableCellNew
                        minWidth={approvalsTableHead[13].minWidth as TableCellWidth}
                        fixedWidth
                      >
                        <TagSegmented
                          {...bankAccounts}
                          tooltipMessage={
                            <Container flexdirection="column">
                              <Text color="white-100" style={{ wordBreak: 'break-all' }}>
                                {bankAccounts.mainLabel}
                              </Text>
                              <Text color="white-100" style={{ wordBreak: 'break-all' }}>
                                {bankAccounts.secondLabel} | {bankAccounts.thirdLabel}
                              </Text>
                            </Container>
                          }
                        />
                      </TableCellNew>
                    )}
                    {tableHeadArr[14].active && (
                      <TableCellNew
                        minWidth={approvalsTableHead[14].minWidth as TableCellWidth}
                        alignitems="center"
                        fixedWidth
                      >
                        <Text color="grey-100">Card</Text>
                        <Icon icon={<ArrowRightIcon transform="scale(0.5)" />} path="inherit" />
                        <TagSegmented
                          {...bankAccounts}
                          tooltipMessage={
                            <Container flexdirection="column">
                              <Text color="white-100" style={{ wordBreak: 'break-all' }}>
                                {bankAccounts.mainLabel}
                              </Text>
                              <Text color="white-100" style={{ wordBreak: 'break-all' }}>
                                {bankAccounts.secondLabel} | {bankAccounts.thirdLabel}
                              </Text>
                            </Container>
                          }
                        />
                      </TableCellNew>
                    )}
                    {tableHeadArr[15].active && (
                      <TableCellNew
                        minWidth={approvalsTableHead[15].minWidth as TableCellWidth}
                        fixedWidth
                      >
                        <UserInfo name="Dinys Vlasov" isLogo secondaryText="Dinys@gmail.com" />
                      </TableCellNew>
                    )}
                    <TableCellNew
                      minWidth={approvalsTableHead[16].minWidth as TableCellWidth}
                      justifycontent="center"
                      alignitems="center"
                      fixedWidth
                      padding="8"
                      gap="8"
                    >
                      <IconButton
                        size="24"
                        icon={<AcceptedIcon />}
                        background={'green-10'}
                        color="green-90"
                        onClick={() => {}}
                        tooltip="Accept"
                      />
                      <IconButton
                        size="24"
                        icon={<RejectedIcon />}
                        background={'red-10'}
                        color="red-90"
                        onClick={handleOpenDocumentRejectionModal}
                        tooltip="Reject"
                      />
                    </TableCellNew>
                  </TableRowNew>
                </TableBodyNew>
              </TableNew>
              <Pagination
                customPagination={
                  <Container gap="16" alignitems="center">
                    <Text type="subtext-regular" color="grey-100">
                      Rows per Page
                    </Text>
                    <PerPageSelect
                      selectedValue={rowsPerPage}
                      onOptionClick={(value) => setRowsPerPage(value)}
                    />
                  </Container>
                }
              />
              {checkedRows.length > 0 && (
                <ActionBar className={spacing.h60fixed}>
                  <Container gap="40">
                    <Container alignitems="center" gap="12">
                      <Container
                        className={classNames(spacing.w16, spacing.h16)}
                        background={'grey-100'}
                        border="grey-90"
                        alignitems="center"
                        justifycontent="center"
                        borderRadius="2"
                      >
                        <Icon icon={<MinusIcon />} />
                      </Container>
                      <Text type="body-regular-14" color={'grey-90'}>
                        Selected lines:&nbsp; 4
                      </Text>
                    </Container>

                    <Container gap="8" alignitems="center">
                      <Button
                        background={'black-90-grey-20'}
                        color="grey-20-black-90"
                        borderRadius="10"
                        width="auto"
                        height="32"
                        iconLeft={<AcceptedIcon />}
                      >
                        Approve
                      </Button>
                      <Button
                        background={'black-90-grey-20'}
                        color="grey-20-black-90"
                        borderRadius="10"
                        width="auto"
                        height="32"
                        iconLeft={<RejectedIcon />}
                      >
                        Reject
                      </Button>
                    </Container>
                  </Container>
                </ActionBar>
              )}
            </>
          )}
        </MainPageContentContainer>
      </ContainerMain>
      {isOpenDocumentRejectionModal && (
        <DocumentRejectionModal onClick={handleOpenDocumentRejectionModal} />
      )}
    </Container>
  );
};

export default {
  title: 'Pages/Paidlog/Approvals/ApprovalsMainPage',
  component: ApprovalsMainPageComponent,
  argTypes: {
    storyState: {
      options: ['empty', 'loading', 'loaded'],
      control: {
        type: 'radio',
        labels: {
          empty: 'Empty page',
          loading: 'Page is loading',
          loaded: 'Page loaded',
        },
      },
    },
  },
} as Meta;

export const ApprovalsMainPage = ApprovalsMainPageComponent.bind({});
ApprovalsMainPage.args = {
  storyState: 'loaded',
};
