import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownFooter } from 'Components/SelectDropdown/components/SelectDropdownFooter';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { Divider } from 'Components/base/Divider';
import { Icon } from 'Components/base/Icon';
import { Tag } from 'Components/base/Tag';
import { Tooltip } from 'Components/base/Tooltip';
import { Button } from 'Components/base/buttons/Button';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { InputNew } from 'Components/base/inputs/InputNew';
import { OptionDoubleHorizontalNew } from 'Components/base/inputs/SelectNew/options/OptionDoubleHorizontalNew';
import { TextareaNew } from 'Components/base/inputs/TextareaNew';
import { Text } from 'Components/base/typography/Text';
import { AccordionAccountSubItem } from 'Components/custom/Accounting/ChartOfAccounts/AccordionAccountSubItem';
import AccordionItem from 'Components/custom/Purchases/AccordionItem/AccordionItem';
import CategoryClassType from 'Components/custom/Purchases/CategoryClassType/CategoryClassType';
import IconsPicker from 'Components/elements/IconsPicker/IconsPicker';
import { Select } from 'Components/inputs/Select';
import { SelectOption } from 'Components/inputs/Select/Select.types';
import { SelectValue } from 'Components/inputs/Select/hooks/useSelectValue';
import { OptionWithTwoLabels } from 'Components/inputs/Select/options/OptionWithTwoLabels';
import { Modal } from 'Components/modules/Modal';
import { options } from 'Mocks/fakeOptions';
import { categoryTypes, exportIcons, ownersOptions } from 'Pages/Paidlog/Categories/constants';
import { ReactComponent as MinusCircleIcon } from 'Svg/16/minus-circle.svg';
import { ReactComponent as GridIcon } from 'Svg/16/view-grid.svg';
import { ReactComponent as PrinterIcon } from 'Svg/categories/16/printer.svg';
import { ReactComponent as CloseIcon } from 'Svg/v2/16/cross.svg';
import { ReactComponent as DocumentIcon } from 'Svg/v2/16/file-blank.svg';
import { ReactComponent as InfoIcon } from 'Svg/v2/16/info.svg';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import { ReactComponent as EmptyChartOfAccountIcon } from 'Svg/v2/124/empty.svg';
import { deleteArrayItemByIndex } from 'Utils/arrays';
import classNames from 'classnames';
import React, { ReactNode, useState } from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Paidlog/Categories/Create Categories Modals',
};

export const SelectCategory: React.FC = () => {
  return (
    <Modal
      size="1040"
      background="grey-10"
      type="new"
      pluginScrollDisabled
      padding="none"
      flexdirection="row"
    >
      <Container
        flexdirection="column"
        background="white-100"
        className={classNames(spacing.w320fixed, spacing.h100p, spacing.p24)}
        gap="16"
      >
        <Button
          iconLeft={<GridIcon />}
          onClick={() => {}}
          align="left"
          width="full"
          height="40"
          background="grey-10"
          font="body-medium"
          color="black-100"
        >
          Default Categories
        </Button>
        <Container justifycontent="space-between" className={classNames(spacing.p8)}>
          <Text type="body-medium" color="grey-100">
            My Categories
          </Text>
          <IconButton
            size="24"
            icon={<PlusIcon />}
            onClick={() => {}}
            background="violet-90"
            shape="circle"
            color="white-100"
          />
        </Container>
        <Container
          flexdirection="column"
          gap="12"
          justifycontent="center"
          alignitems="center"
          alignself="center"
          className={classNames(spacing.w220, spacing.p32)}
        >
          <EmptyChartOfAccountIcon />
          <Text color="grey-100" type="body-regular-14" align="center">
            You don&apos;t have any categories
          </Text>
        </Container>
      </Container>
      <Container gap="24" className={spacing.p24} flexdirection="column" flexgrow="1">
        <Container justifycontent="space-between">
          <Text color="grey-100" type="body-medium">
            Categories / Default Categories
          </Text>
          <Container gap="16" alignitems="center">
            <IconButton
              background="white-100"
              size="24"
              icon={<CloseIcon />}
              color="black-100"
              onClick={() => {}}
            />
          </Container>
        </Container>
        <Container customScroll flexdirection="column" flexgrow="1">
          <Container
            flexdirection="column"
            justifycontent="center"
            alignitems="center"
            flexgrow="1"
            gap="40"
          >
            <Text type="body-medium" align="center" className={spacing.w240}>
              Please choose Categories
            </Text>
            <Container flexdirection="row" flexwrap="wrap" gap="32-8" justifycontent="center">
              {categoryTypes.map(({ name, icon, added }) => (
                <CategoryClassType
                  key={name}
                  name={name}
                  onClick={() => {}}
                  icon={exportIcons[icon]}
                  added={added}
                />
              ))}
            </Container>
          </Container>
        </Container>
      </Container>
    </Modal>
  );
};

export const CreateCategory = () => {
  const [templateAccountDefaultOption, setTemplateAccountDefaultOption] = useState({
    value: '0',
    label: 'Default option',
  });
  const [chosenIcon, setChosenIcon] = useState<React.ReactNode>();
  const [ownersAppliedOptions, setOwnersAppliedOptions] = useState<SelectValue<true>>([]);
  const [itemActive, setItemActive] = useState<boolean>(false);

  const handleIconsPickerClick = (value: { id: string; icon: ReactNode }) => {
    setChosenIcon(value?.icon);
  };

  return (
    <Modal
      size="1040"
      background="grey-10"
      type="new"
      pluginScrollDisabled
      padding="none"
      flexdirection="row"
    >
      <Container
        flexdirection="column"
        background="white-100"
        className={classNames(spacing.w320fixed, spacing.h100p, spacing.p24)}
        gap="16"
      >
        <Button
          iconLeft={<GridIcon />}
          onClick={() => {}}
          align="left"
          width="full"
          height="40"
          background="grey-10"
          font="body-medium"
          color="black-100"
        >
          Default Categories
        </Button>
        <Container justifycontent="space-between" className={classNames(spacing.p8)}>
          <Text type="body-medium" color="grey-100">
            My Categories
          </Text>
          <IconButton
            size="24"
            icon={<PlusIcon />}
            onClick={() => {}}
            background="violet-90"
            shape="circle"
            color="white-100"
          />
        </Container>
        <AccordionItem
          name="Office equipment"
          color="grey-100"
          state="draft"
          icon={<PrinterIcon />}
          isActive={itemActive}
          onClick={() => setItemActive((prev) => !prev)}
          onAddClick={() => {}}
          dropdown={
            <>
              <DropDownButton disabled={false} size="160" icon={<EditIcon />} onClick={() => {}}>
                Edit
              </DropDownButton>
              <DropDownButton icon={<MinusCircleIcon />} onClick={() => {}}>
                Disable
              </DropDownButton>
              <Divider fullHorizontalWidth />
              <DropDownButton
                disabled={false}
                size="160"
                type="danger"
                icon={<TrashIcon />}
                onClick={() => {}}
              >
                Delete
              </DropDownButton>
            </>
          }
        >
          <AccordionAccountSubItem
            name="Level 1"
            nestingLevel={1}
            isActive={itemActive}
            isOpen={itemActive}
            onClick={() => {}}
            onIconClick={() => setItemActive((prev) => !prev)}
            onAddClick={() => {}}
            onEdit={() => {}}
            onDelete={() => {}}
          >
            <AccordionAccountSubItem
              name="Level 2"
              nestingLevel={2}
              isActive={itemActive}
              isOpen={itemActive}
              onClick={() => {}}
              onIconClick={() => setItemActive((prev) => !prev)}
              onAddClick={() => {}}
              onEdit={() => {}}
              onDelete={() => {}}
            >
              <AccordionAccountSubItem
                name="Level 3"
                nestingLevel={3}
                isActive={false}
                isOpen={false}
                addButtonShown={false}
                onClick={() => {}}
                onAddClick={() => {}}
                onEdit={() => {}}
                onDelete={() => {}}
              />
            </AccordionAccountSubItem>
          </AccordionAccountSubItem>
        </AccordionItem>
      </Container>
      <Container gap="24" className={spacing.p24} flexdirection="column" flexgrow="1">
        <Container justifycontent="space-between">
          <Text color="grey-100" type="body-medium">
            Categories / Default Categories
          </Text>
          <Container gap="16" alignitems="center">
            <Tag
              icon={<DocumentIcon />}
              text="Draft"
              color="yellow"
              padding="4-8"
              cursor="default"
            />
            <IconButton
              background="white-100"
              size="24"
              icon={<CloseIcon />}
              color="black-100"
              onClick={() => {}}
            />
          </Container>
        </Container>
        <Container customScroll flexdirection="column" flexgrow="1">
          <Container
            borderRadius="20"
            background="white-100"
            flexdirection="column"
            className={classNames(spacing.p24)}
            gap="16"
            flexgrow="1"
            style={{ boxShadow: '0px 5px 17px rgba(153, 152, 152, 0.18)' }}
          >
            <Container gap="24" flexdirection="column" flexgrow="1">
              <Text type="body-regular-14">General</Text>
              <Container flexdirection="row" gap="24">
                <IconsPicker
                  onClick={(e) => {
                    handleIconsPickerClick(e);
                  }}
                  onDelete={() => setChosenIcon(null)}
                  icon={chosenIcon}
                />
                <Container flexdirection="column" gap="24" flexgrow="1">
                  <InputNew
                    name="categoryName"
                    label="Name"
                    required
                    width="480"
                    value="Office Equipment"
                    onChange={() => {}}
                  />
                  <Select
                    isMulti
                    value={ownersAppliedOptions}
                    label="Owner"
                    height="48"
                    onChange={setOwnersAppliedOptions}
                    width="480"
                    placeholder="Search"
                  >
                    {({ selectElement, selectValue, closeDropdown, onOptionClick }) => (
                      <SelectDropdown
                        isMulti
                        selectElement={selectElement}
                        selectValue={selectValue}
                      >
                        {({ selectedOptions, setSelectedOptions }) => {
                          const appliedOptions = ownersAppliedOptions;
                          const nonSelectedOptions = ownersOptions?.filter(
                            ({ value }) =>
                              !appliedOptions?.find((applied) => applied?.value === value),
                          );

                          return (
                            <>
                              <SelectDropdownBody>
                                <SelectDropdownItemList
                                  type="multi"
                                  isSelectFilled={!!appliedOptions?.length}
                                  selectedItems={appliedOptions?.map(
                                    ({ value, label, subLabel = '' }) => (
                                      <OptionWithTwoLabels
                                        id={value}
                                        key={value}
                                        label={label}
                                        secondaryLabel={subLabel}
                                        type="checkbox"
                                        selected={
                                          !!selectedOptions?.find(
                                            ({ value: selectedValue }) => selectedValue === value,
                                          )
                                        }
                                        onClick={() => {
                                          if (!selectedOptions) return;

                                          const index = selectedOptions?.findIndex(
                                            ({ value: selectedValue }) => selectedValue === value,
                                          );

                                          if (index !== -1) {
                                            const arr: SelectOption[] = deleteArrayItemByIndex(
                                              selectedOptions,
                                              index,
                                            );
                                            setSelectedOptions(arr);
                                            return;
                                          }
                                          setSelectedOptions([
                                            ...selectedOptions,
                                            {
                                              value,
                                              label,
                                              subLabel,
                                            },
                                          ]);
                                        }}
                                      />
                                    ),
                                  )}
                                  otherItems={nonSelectedOptions?.map(
                                    ({ value, label, subLabel }) => (
                                      <OptionWithTwoLabels
                                        id={value}
                                        key={value}
                                        label={label}
                                        secondaryLabel={subLabel}
                                        type="checkbox"
                                        selected={
                                          !!selectedOptions?.find(
                                            ({ value: selectedValue }) => selectedValue === value,
                                          )
                                        }
                                        onClick={() => {
                                          if (!selectedOptions) return;

                                          const index = selectedOptions?.findIndex(
                                            ({ value: selectedValue }) => selectedValue === value,
                                          );

                                          if (index !== -1) {
                                            const arr: SelectOption[] = deleteArrayItemByIndex(
                                              selectedOptions,
                                              index,
                                            );
                                            setSelectedOptions(arr);
                                            return;
                                          }

                                          setSelectedOptions([
                                            ...selectedOptions,
                                            {
                                              value,
                                              label,
                                              subLabel,
                                            },
                                          ]);
                                        }}
                                      />
                                    ),
                                  )}
                                ></SelectDropdownItemList>
                              </SelectDropdownBody>
                              <SelectDropdownFooter>
                                <Container
                                  justifycontent="flex-end"
                                  className={classNames(spacing.mX12, spacing.mY8)}
                                >
                                  <Button
                                    width="100"
                                    height="32"
                                    onClick={() => {
                                      onOptionClick(selectedOptions);
                                      closeDropdown();
                                    }}
                                  >
                                    Add
                                  </Button>
                                </Container>
                              </SelectDropdownFooter>
                            </>
                          );
                        }}
                      </SelectDropdown>
                    )}
                  </Select>
                </Container>
              </Container>
              <TextareaNew
                name="notes"
                label="Notes"
                value=""
                maxLength={200}
                onChange={() => {}}
              />
              <Container alignitems="center">
                <Text type="body-regular-14">Accounting</Text>
                <Tooltip cursor="pointer" wrapperClassName={spacing.ml5} message="Tooltip">
                  <Icon icon={<InfoIcon />} path="grey-90" />
                </Tooltip>
              </Container>
              <Select
                label="Expenses template account*"
                onChange={() => {}}
                width="full"
                height="48"
                placeholder="Search"
                defaultValue={templateAccountDefaultOption}
              >
                {({ selectElement, selectValue, closeDropdown, onOptionClick }) => (
                  <SelectDropdown selectElement={selectElement}>
                    <SelectDropdownBody>
                      <SelectDropdownItemList>
                        {options.map((item) => (
                          <OptionDoubleHorizontalNew
                            key={item.value}
                            label={item.label}
                            secondaryLabel="Expenses"
                            selected={selectValue?.value === item.value}
                            onClick={() => {
                              onOptionClick({
                                label: item.label,
                                value: item.value,
                              });
                              closeDropdown();
                            }}
                          />
                        ))}
                      </SelectDropdownItemList>
                    </SelectDropdownBody>
                  </SelectDropdown>
                )}
              </Select>
            </Container>
            <Divider fullHorizontalWidth />
            <Container justifycontent="flex-end">
              <Button height="40" onClick={() => {}} background="grey-90">
                Create
              </Button>
            </Container>
          </Container>
        </Container>
      </Container>
    </Modal>
  );
};
