import { SelectOption } from 'Components/inputs/Select/Select.types';
import { ReactComponent as AirplaneIcon } from 'Svg/categories/16/airplane.svg';
import { ReactComponent as ArchiveRoundedIcon } from 'Svg/categories/16/archive-rounded.svg';
import { ReactComponent as BriefcaseIcon } from 'Svg/categories/16/briefcase.svg';
import { ReactComponent as CakeIcon } from 'Svg/categories/16/cake.svg';
import { ReactComponent as ClipboardIcon } from 'Svg/categories/16/clipboard-list.svg';
import { ReactComponent as CpuIcon } from 'Svg/categories/16/cpu.svg';
import { ReactComponent as CubeIcon } from 'Svg/categories/16/cube.svg';
import { ReactComponent as CursorIcon } from 'Svg/categories/16/cursor.svg';
import { ReactComponent as KeyIcon } from 'Svg/categories/16/key.svg';
import { ReactComponent as OfficeIcon } from 'Svg/categories/16/office.svg';
import { ReactComponent as PrinterIcon } from 'Svg/categories/16/printer.svg';
import { ReactComponent as PuzzleIcon } from 'Svg/categories/16/puzzle.svg';
import { ReactComponent as ReceiptIcon } from 'Svg/categories/16/receipt-tax.svg';
import { ReactComponent as ShieldIcon } from 'Svg/categories/16/shield-check.svg';
import { ReactComponent as SpeakerIcon } from 'Svg/categories/16/speakerphone.svg';
import { ReactComponent as TruckIcon } from 'Svg/categories/16/truck.svg';
import { ReactComponent as GridFilledIcon } from 'Svg/categories/16/view-grid-filled.svg';
import { ReactComponent as AirBudsIcon } from 'Svg/categories/24/air-buds.svg';
import { ReactComponent as ArchiveBinIcon } from 'Svg/categories/24/archive-bin.svg';
import { ReactComponent as ArchiveDownIcon } from 'Svg/categories/24/archive-down.svg';
import { ReactComponent as ArchiveIcon } from 'Svg/categories/24/archive.svg';
import { ReactComponent as BellIcon } from 'Svg/categories/24/bell.svg';
import { ReactComponent as BoltIcon } from 'Svg/categories/24/bolt.svg';
import { ReactComponent as CameraIcon } from 'Svg/categories/24/camera.svg';
import { ReactComponent as CaseRoundIcon } from 'Svg/categories/24/case-round.svg';
import { ReactComponent as ChatRoundDotsIcon } from 'Svg/categories/24/chat-round-dots.svg';
import { ReactComponent as ClipboardPlusIcon } from 'Svg/categories/24/clipboard-add.svg';
import { ReactComponent as ClipboardCheckIcon } from 'Svg/categories/24/clipboard-check.svg';
import { ReactComponent as ClipboardHeartIcon } from 'Svg/categories/24/clipboard-heart.svg';
import { ReactComponent as ClipboardTextIcon } from 'Svg/categories/24/clipboard-text.svg';
import { ReactComponent as ClockCircleIcon } from 'Svg/categories/24/clock-circle.svg';
import { ReactComponent as CloudIcon } from 'Svg/categories/24/cloud.svg';
import { ReactComponent as CrownIcon } from 'Svg/categories/24/crown.svg';
import { ReactComponent as CupFirstIcon } from 'Svg/categories/24/cup-first.svg';
import { ReactComponent as AddDocumentIcon } from 'Svg/categories/24/document-add.svg';
import { ReactComponent as DocumentMedicineIcon } from 'Svg/categories/24/document-medicine.svg';
import { ReactComponent as DocumentIcon } from 'Svg/categories/24/document.svg';
import { ReactComponent as DocumentsIcon } from 'Svg/categories/24/documents.svg';
import { ReactComponent as DumbbellIcon } from 'Svg/categories/24/dumbbell.svg';
import { ReactComponent as FireIcon } from 'Svg/categories/24/fire.svg';
import { ReactComponent as GalleryIcon } from 'Svg/categories/24/gallery.svg';
import { ReactComponent as GamepadIcon } from 'Svg/categories/24/gamepad.svg';
import { ReactComponent as HeartIcon } from 'Svg/categories/24/heart.svg';
import { ReactComponent as HomeIcon } from 'Svg/categories/24/home.svg';
import { ReactComponent as LightBulbIcon } from 'Svg/categories/24/lightbulb.svg';
import { ReactComponent as MapPointIcon } from 'Svg/categories/24/map-point.svg';
import { ReactComponent as MedalStarIcon } from 'Svg/categories/24/medal-star.svg';
import { ReactComponent as MoneyBagIcon } from 'Svg/categories/24/money-bag.svg';
import { ReactComponent as MonitorSmartphoneIcon } from 'Svg/categories/24/monitor-smartphone.svg';
import { ReactComponent as NotesIcon } from 'Svg/categories/24/notes.svg';
import { ReactComponent as PaletteIcon } from 'Svg/categories/24/palette.svg';
import { ReactComponent as PenIcon } from 'Svg/categories/24/pen.svg';
import { ReactComponent as PresentationGraphIcon } from 'Svg/categories/24/presentation-graph.svg';
import { ReactComponent as SmileCircleIcon } from 'Svg/categories/24/smile-circle.svg';
import { ReactComponent as StarIcon } from 'Svg/categories/24/star.svg';
import { ReactComponent as SunIcon } from 'Svg/categories/24/sun.svg';
import { ReactComponent as UserIdIcon } from 'Svg/categories/24/user-id.svg';
import { ReactComponent as GearIcon } from 'Svg/v2/16/gear.svg';
import React from 'react';

interface CategoryTypes {
  icon: keyof typeof exportIcons;
  name: string;

  onClick?(): void;

  added?: boolean;
}

export const ownersOptions: SelectOption[] = [
  {
    value: '1',
    label: '1 Karine Mnatsakanian',
    subLabel: 'User@gmail.com',
  },
  {
    value: '2',
    label: '2 Karine Mnatsakanian',
    subLabel: 'User@gmail.com',
  },
  {
    value: '3',
    label: '3 Karine Mnatsakanian',
    subLabel: 'User@gmail.com',
  },
  {
    value: '4',
    label: '4 Karine Mnatsakanian',
    subLabel: 'User@gmail.com',
  },
  {
    value: '5',
    label: '5 Karine Mnatsakanian',
    subLabel: 'User@gmail.com',
  },
];

export const categoryTypes: CategoryTypes[] = [
  {
    icon: 'megaphone',
    name: 'Advertising',
  },
  {
    icon: 'cube',
    name: 'Materials',
  },
  {
    icon: 'office_box',
    name: 'Office equipment',
    added: true,
  },
  {
    icon: 'cake',
    name: 'Meals and entertainment',
  },
  {
    icon: 'key',
    name: 'Car',
  },
  {
    icon: 'printer',
    name: 'Office supplies',
  },
  {
    icon: 'cpu',
    name: 'Equipment',
  },
  {
    icon: 'grid',
    name: 'Other',
  },
  {
    icon: 'receipt_percent',
    name: 'Fees',
  },
  {
    icon: 'briefcase',
    name: 'Professional services',
  },
  {
    icon: 'shield_check',
    name: 'Insurance',
  },
  {
    icon: 'office',
    name: 'Rent',
  },
  {
    icon: 'puzzle',
    name: 'Interest',
  },
  {
    icon: 'clipboard',
    name: 'Taxes',
  },
  {
    icon: 'truck',
    name: 'Labor',
  },
  {
    icon: 'airplane',
    name: 'Travel',
  },
  {
    icon: 'gear',
    name: 'Maintenance',
  },
  {
    icon: 'cursor',
    name: 'Utilities',
  },
];

export const exportIcons = {
  megaphone: <SpeakerIcon />,
  cube: <CubeIcon />,
  office_box: <ArchiveRoundedIcon />,
  cake: <CakeIcon />,
  key: <KeyIcon />,
  printer: <PrinterIcon />,
  cpu: <CpuIcon />,
  grid: <GridFilledIcon />,
  receipt_percent: <ReceiptIcon />,
  briefcase: <BriefcaseIcon />,
  shield_check: <ShieldIcon />,
  office: <OfficeIcon />,
  puzzle: <PuzzleIcon />,
  clipboard: <ClipboardIcon />,
  truck: <TruckIcon />,
  airplane: <AirplaneIcon />,
  gear: <GearIcon />,
  cursor: <CursorIcon />,

  document: <DocumentIcon />,
  palette: <PaletteIcon />,
  addDocument: <AddDocumentIcon />,
  medicineDocument: <DocumentMedicineIcon />,
  documents: <DocumentsIcon />,
  notes: <NotesIcon />,
  userId: <UserIdIcon />,
  fire: <FireIcon />,
  clipboardText: <ClipboardTextIcon />,
  clipboardHeart: <ClipboardHeartIcon />,
  clipboardCheck: <ClipboardCheckIcon />,
  cupFirst: <CupFirstIcon />,
  archiveDown: <ArchiveDownIcon />,
  bolt: <BoltIcon />,
  filledCrown: <CrownIcon />,
  boxWithArrowDown: <ArchiveIcon />,
  home: <HomeIcon />,
  presentationGraph: <PresentationGraphIcon />,
  boxWithMinus: <ArchiveBinIcon />,
  bell: <BellIcon />,
  arrowClock: <ClockCircleIcon />,
  dumbbell: <DumbbellIcon />,
  smileInFilledCircle: <SmileCircleIcon />,
  filledCloud: <CloudIcon />,
  airBuds: <AirBudsIcon />,
  monitorAndSmartphone: <MonitorSmartphoneIcon />,
  lightBulb: <LightBulbIcon />,
  moneyBag: <MoneyBagIcon />,
  gallery: <GalleryIcon />,
  camera: <CameraIcon />,
  caseRounded: <CaseRoundIcon />,
  heart: <HeartIcon />,
  star: <StarIcon />,
  mapPoint: <MapPointIcon />,
  chartRoundDots: <ChatRoundDotsIcon />,
  icon: <ClipboardPlusIcon />,
  medalStar: <MedalStarIcon />,
  sun: <SunIcon />,
  pen: <PenIcon />,
  gamepad: <GamepadIcon />,
};

export const pickerIcons = {
  document: <DocumentIcon />,
  palette: <PaletteIcon />,
  addDocument: <AddDocumentIcon />,
  medicineDocument: <DocumentMedicineIcon />,
  documents: <DocumentsIcon />,
  notes: <NotesIcon />,
  userId: <UserIdIcon />,
  fire: <FireIcon />,
  clipboardText: <ClipboardTextIcon />,
  clipboardHeart: <ClipboardHeartIcon />,
  clipboardCheck: <ClipboardCheckIcon />,
  cupFirst: <CupFirstIcon />,
  archiveDown: <ArchiveDownIcon />,
  bolt: <BoltIcon />,
  filledCrown: <CrownIcon />,
  boxWithArrowDown: <ArchiveIcon />,
  home: <HomeIcon />,
  presentationGraph: <PresentationGraphIcon />,
  boxWithMinus: <ArchiveBinIcon />,
  bell: <BellIcon />,
  arrowClock: <ClockCircleIcon />,
  dumbbell: <DumbbellIcon />,
  smileInFilledCircle: <SmileCircleIcon />,
  filledCloud: <CloudIcon />,
  airBuds: <AirBudsIcon />,
  monitorAndSmartphone: <MonitorSmartphoneIcon />,
  lightBulb: <LightBulbIcon />,
  moneyBag: <MoneyBagIcon />,
  gallery: <GalleryIcon />,
  camera: <CameraIcon />,
  caseRounded: <CaseRoundIcon />,
  heart: <HeartIcon />,
  star: <StarIcon />,
  mapPoint: <MapPointIcon />,
  chartRoundDots: <ChatRoundDotsIcon />,
  icon: <ClipboardPlusIcon />,
  medalStar: <MedalStarIcon />,
  sun: <SunIcon />,
  pen: <PenIcon />,
  gamepad: <GamepadIcon />,
};
