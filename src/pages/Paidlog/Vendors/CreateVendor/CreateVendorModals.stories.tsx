import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { BackdropWrapper } from 'Components/base/BackdropWrapper';
import { Checkbox } from 'Components/base/Checkbox';
import { Divider } from 'Components/base/Divider';
import { Icon } from 'Components/base/Icon';
import { Radio } from 'Components/base/Radio';
import { TagSegmented } from 'Components/base/TagSegmented';
import { Tooltip } from 'Components/base/Tooltip';
import { Button } from 'Components/base/buttons/Button';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { InputDate } from 'Components/base/inputs/InputDate';
import { InputNew } from 'Components/base/inputs/InputNew';
import { InputPhone } from 'Components/base/inputs/InputPhone';
import { SelectDouble } from 'Components/base/inputs/SelectDouble';
import { SelectNew } from 'Components/base/inputs/SelectNew';
import { OptionDoubleVerticalNew } from 'Components/base/inputs/SelectNew/options/OptionDoubleVerticalNew';
import { OptionSingleNew } from 'Components/base/inputs/SelectNew/options/OptionSingleNew';
import { TextareaNew } from 'Components/base/inputs/TextareaNew';
import { GeneralTab, GeneralTabWrapper } from 'Components/base/tabs/GeneralTab';
import { MenuTab, MenuTabWrapper } from 'Components/base/tabs/MenuTab';
import { Text } from 'Components/base/typography/Text';
import { BillingCardItem } from 'Components/elements/BillingCardItem';
import { Select } from 'Components/inputs/Select';
import { OptionWithTwoLabels } from 'Components/inputs/Select/options/OptionWithTwoLabels';
import { AssignContainer } from 'Components/modules/Assign/AssignContainer';
import { AssignLine } from 'Components/modules/Assign/AssignLine';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { options } from 'Mocks/fakeOptions';
import { ReactComponent as BankIcon } from 'Svg/v2/16/bank.svg';
import { ReactComponent as CreditCardIcon } from 'Svg/v2/16/credit-card.svg';
import { ReactComponent as InfoIcon } from 'Svg/v2/16/info-in-circle.svg';
import { ReactComponent as InfoFilledIcon } from 'Svg/v2/16/info.svg';
import { ReactComponent as MasterCardLogo } from 'Svg/v2/16/logo-mastercard.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as StarFilledIcon } from 'Svg/v2/16/star-filled.svg';
import { ReactComponent as WalletIcon } from 'Svg/v2/16/wallet.svg';
import { ReactComponent as BICSwiftCodeTooltip } from 'Svg/v2/tooltips/bic-swift-code.svg';
import { ReactComponent as IBANNumberTooltip } from 'Svg/v2/tooltips/iban-number.svg';
import { ReactComponent as RoutingNumberTooltip } from 'Svg/v2/tooltips/routing-number.svg';
import handleAccountLength from 'Utils/handleAccountLength';
import classNames from 'classnames';
import React, { ReactNode, useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Paidlog/Vendors/Modals/Create Vendor',
};

export const CreateNewVendorGeneralTab: React.FC = () => {
  const [customerType, setCustomerType] = useState<'individual' | 'company'>('individual');
  const [nameValue, setNameValue] = useState('');
  const [einValue, setEINValue] = useState('');
  const [notesValue, setNotesValue] = useState('');
  return (
    <Modal
      size="720"
      background="grey-10"
      type="new"
      header={
        <ModalHeaderNew background="grey-10" title="Create new vendor" border onClose={() => {}} />
      }
      footer={
        <ModalFooterNew background="grey-10">
          <Button width="md" onClick={() => {}}>
            Save
          </Button>
        </ModalFooterNew>
      }
    >
      <MenuTabWrapper>
        <MenuTab id="general" active onClick={() => {}}>
          General
        </MenuTab>
        <MenuTab id="contacts" onClick={() => {}}>
          Contacts
        </MenuTab>
        <MenuTab id="billing" onClick={() => {}}>
          Billing
        </MenuTab>
        <MenuTab id="payment" onClick={() => {}}>
          Payment
        </MenuTab>
        <MenuTab id="accounting" onClick={() => {}}>
          Accounting
        </MenuTab>
      </MenuTabWrapper>
      <Container
        flexdirection="column"
        borderRadius="20"
        background="white-100"
        className={classNames(spacing.mt24, spacing.p24)}
        gap="16"
      >
        <Text type="body-regular-14">General</Text>
        <Text type="caption-regular" color="grey-100">
          Choose Vendor type
        </Text>
        <Container gap="20">
          <Radio
            id="individual"
            onChange={() => setCustomerType('individual')}
            name="customerType"
            checked={customerType === 'individual'}
          >
            Individual
          </Radio>
          <Radio
            id="company"
            onChange={() => setCustomerType('company')}
            name="customerType"
            checked={customerType === 'company'}
          >
            Company
          </Radio>
        </Container>
        {customerType === 'individual' && (
          <Container justifycontent="space-between" className={classNames(spacing.w100p)}>
            <InputNew
              name="firstName"
              label="First name"
              width="192"
              required
              onChange={(e) => {
                setNameValue(e.target.value);
              }}
              value={nameValue}
            />
            <InputNew
              name="lastName"
              label="Last name"
              width="192"
              required
              onChange={(e) => {
                setNameValue(e.target.value);
              }}
              value={nameValue}
            />
            <InputNew
              name="EIN"
              label="EIN"
              width="192"
              value={einValue}
              onChange={(e) => {
                setEINValue(e.target.value);
              }}
            />
          </Container>
        )}
        {customerType === 'company' && (
          <Container justifycontent="space-between" className={classNames(spacing.w100p)}>
            <InputNew
              name="vendorName"
              label="Vendor name"
              width="300"
              required
              onChange={(e) => {
                setNameValue(e.target.value);
              }}
              value={nameValue}
            />
            <InputNew
              name="vendorEIN"
              label="Vendor EIN"
              width="300"
              required
              value={einValue}
              onChange={(e) => {
                setEINValue(e.target.value);
              }}
            />
          </Container>
        )}
        <TextareaNew
          className={spacing.mt8}
          name="notes"
          label="Notes"
          value={notesValue}
          onChange={(e) => setNotesValue(e.target.value)}
        />
        <Container className={spacing.mt8} alignitems="center" gap="8">
          <Text type="body-regular-14">Accounting</Text>
          <Tooltip message="Info block">
            <Icon icon={<InfoFilledIcon />} path="grey-90" cursor="pointer" />
          </Tooltip>
        </Container>
        <Select type="baseWithCategory" label="Payables template account*" placeholder="Search">
          {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
            <SelectDropdown selectElement={selectElement}>
              <SelectDropdownBody>
                <SelectDropdownItemList>
                  {options.map((value, index) => (
                    <OptionWithTwoLabels
                      id={value.label}
                      key={index}
                      tooltip
                      labelDirection="horizontal"
                      label={value?.label}
                      secondaryLabel={value.subLabel}
                      selected={!Array.isArray(selectValue) && selectValue?.value === value.value}
                      onClick={() => {
                        onOptionClick({
                          value: value?.value,
                          label: value?.label,
                          subLabel: value?.subLabel,
                        });
                        closeDropdown();
                      }}
                    />
                  ))}
                </SelectDropdownItemList>
              </SelectDropdownBody>
            </SelectDropdown>
          )}
        </Select>
      </Container>
    </Modal>
  );
};

export const CreateNewVendorContactsTab: React.FC = () => {
  const [contactNameValue, setContactNameValue] = useState('');
  const [emailValue, setEmailValue] = useState('');
  const [zipCodeValue, setZipCodeValue] = useState('');
  const [streetValue, setStreetValue] = useState('');
  const [sameAsBillingValue, setSameAsBillingValue] = useState(true);
  return (
    <Modal
      size="720"
      background="grey-10"
      type="new"
      header={
        <ModalHeaderNew background="grey-10" title="Create new vendor" border onClose={() => {}} />
      }
      footer={
        <ModalFooterNew background="grey-10">
          <Button width="md" onClick={() => {}}>
            Save
          </Button>
        </ModalFooterNew>
      }
    >
      <MenuTabWrapper>
        <MenuTab id="general" onClick={() => {}}>
          General
        </MenuTab>
        <MenuTab id="contacts" active onClick={() => {}}>
          Contacts
        </MenuTab>
        <MenuTab id="billing" onClick={() => {}}>
          Billing
        </MenuTab>
        <MenuTab id="payment" onClick={() => {}}>
          Payment
        </MenuTab>
        <MenuTab id="accounting" onClick={() => {}}>
          Accounting
        </MenuTab>
      </MenuTabWrapper>
      <Container
        flexdirection="column"
        borderRadius="20"
        background="white-100"
        className={classNames(spacing.mt24, spacing.p24)}
      >
        <Text type="body-regular-14">General</Text>
        <InputNew
          className={spacing.mt16}
          name="contactName"
          label="Contact person name"
          width="full"
          required
          onChange={(e) => {
            setContactNameValue(e.target.value);
          }}
          value={contactNameValue}
        />
        <Container
          justifycontent="space-between"
          className={classNames(spacing.w100p, spacing.mt24)}
        >
          <InputPhone
            width="300"
            id="nmbr-id"
            name="test-nmbr"
            value=""
            label="Phone number"
            onChange={() => {}}
            onBlur={() => {}}
          />
          <InputNew
            name="email"
            label="Email address"
            width="300"
            type="email"
            value={emailValue}
            onChange={(e) => {
              setEmailValue(e.target.value);
            }}
          />
        </Container>
        <Text className={spacing.mt24} type="body-regular-14">
          Billing address
        </Text>
        <Container
          justifycontent="space-between"
          className={classNames(spacing.w100p, spacing.mt16)}
        >
          <SelectNew name="country" onChange={() => {}} width="192" label="Country">
            {({ onClick, state, selectElement }) => (
              <SelectDropdown selectElement={selectElement}>
                <SelectDropdownBody>
                  <SelectDropdownItemList>
                    {options.map(({ label, value }) => (
                      <OptionSingleNew
                        key={value}
                        label={label}
                        selected={state?.value === value}
                        onClick={() =>
                          onClick({
                            label,
                            value,
                          })
                        }
                      />
                    ))}
                  </SelectDropdownItemList>
                </SelectDropdownBody>
              </SelectDropdown>
            )}
          </SelectNew>
          <SelectNew name="state" onChange={() => {}} width="192" label="State">
            {({ onClick, state, selectElement }) => (
              <SelectDropdown selectElement={selectElement}>
                <SelectDropdownBody>
                  <SelectDropdownItemList>
                    {options.map(({ label, value }) => (
                      <OptionSingleNew
                        key={value}
                        label={label}
                        selected={state?.value === value}
                        onClick={() =>
                          onClick({
                            label,
                            value,
                          })
                        }
                      />
                    ))}
                  </SelectDropdownItemList>
                </SelectDropdownBody>
              </SelectDropdown>
            )}
          </SelectNew>
          <SelectNew name="city" onChange={() => {}} width="192" label="City">
            {({ onClick, state, selectElement }) => (
              <SelectDropdown selectElement={selectElement}>
                <SelectDropdownBody>
                  <SelectDropdownItemList>
                    {options.map(({ label, value }) => (
                      <OptionSingleNew
                        key={value}
                        label={label}
                        selected={state?.value === value}
                        onClick={() =>
                          onClick({
                            label,
                            value,
                          })
                        }
                      />
                    ))}
                  </SelectDropdownItemList>
                </SelectDropdownBody>
              </SelectDropdown>
            )}
          </SelectNew>
        </Container>
        <Container
          justifycontent="space-between"
          className={classNames(spacing.w100p, spacing.mt16)}
        >
          <InputNew
            name="zipCode"
            label="ZIP code"
            width="192"
            value={zipCodeValue}
            onChange={(e) => {
              setZipCodeValue(e.target.value);
            }}
          />
          <InputNew
            name="street"
            label="Street"
            width="408"
            value={streetValue}
            onChange={(e) => {
              setStreetValue(e.target.value);
            }}
          />
        </Container>
        <Text className={spacing.mt24} type="body-regular-14">
          Shipping address
        </Text>
        <Checkbox
          className={spacing.mt16}
          name="sameAsBilling"
          checked={sameAsBillingValue}
          onChange={() => {
            setSameAsBillingValue((prev) => !prev);
          }}
        >
          Same as billing address
        </Checkbox>
        {!sameAsBillingValue && (
          <>
            <Container
              justifycontent="space-between"
              className={classNames(spacing.w100p, spacing.mt16)}
            >
              <SelectNew name="country" onChange={() => {}} width="192" label="Country">
                {({ onClick, state, selectElement }) => (
                  <SelectDropdown selectElement={selectElement}>
                    <SelectDropdownBody>
                      <SelectDropdownItemList>
                        {options.map(({ label, value }) => (
                          <OptionSingleNew
                            key={value}
                            label={label}
                            selected={state?.value === value}
                            onClick={() =>
                              onClick({
                                label,
                                value,
                              })
                            }
                          />
                        ))}
                      </SelectDropdownItemList>
                    </SelectDropdownBody>
                  </SelectDropdown>
                )}
              </SelectNew>
              <SelectNew name="state" onChange={() => {}} width="192" label="State">
                {({ onClick, state, selectElement }) => (
                  <SelectDropdown selectElement={selectElement}>
                    <SelectDropdownBody>
                      <SelectDropdownItemList>
                        {options.map(({ label, value }) => (
                          <OptionSingleNew
                            key={value}
                            label={label}
                            selected={state?.value === value}
                            onClick={() =>
                              onClick({
                                label,
                                value,
                              })
                            }
                          />
                        ))}
                      </SelectDropdownItemList>
                    </SelectDropdownBody>
                  </SelectDropdown>
                )}
              </SelectNew>
              <SelectNew name="city" onChange={() => {}} width="192" label="City">
                {({ onClick, state, selectElement }) => (
                  <SelectDropdown selectElement={selectElement}>
                    <SelectDropdownBody>
                      <SelectDropdownItemList>
                        {options.map(({ label, value }) => (
                          <OptionSingleNew
                            key={value}
                            label={label}
                            selected={state?.value === value}
                            onClick={() =>
                              onClick({
                                label,
                                value,
                              })
                            }
                          />
                        ))}
                      </SelectDropdownItemList>
                    </SelectDropdownBody>
                  </SelectDropdown>
                )}
              </SelectNew>
            </Container>
            <Container
              justifycontent="space-between"
              className={classNames(spacing.w100p, spacing.mt16)}
            >
              <InputNew
                name="zipCode"
                label="ZIP code"
                width="192"
                value={zipCodeValue}
                onChange={(e) => {
                  setZipCodeValue(e.target.value);
                }}
              />
              <InputNew
                name="street"
                label="Street"
                width="408"
                value={streetValue}
                onChange={(e) => {
                  setStreetValue(e.target.value);
                }}
              />
            </Container>
          </>
        )}
      </Container>
    </Modal>
  );
};

export const CreateNewVendorBillingTab: React.FC = () => {
  const [billingTabActive, setBillingTabActive] = useState<BillingType>('bankAccount');
  type BillingType = 'creditCard' | 'bankAccount' | 'wallet';

  interface TabsType<T = string> {
    type: T;
    label: string;
    icon: ReactNode;
  }

  const billingTabs: TabsType<BillingType>[] = [
    {
      type: 'creditCard',
      label: 'Card',
      icon: <CreditCardIcon />,
    },
    {
      type: 'bankAccount',
      label: 'Bank Account',
      icon: <BankIcon />,
    },
    {
      type: 'wallet',
      label: 'Wallet',
      icon: <WalletIcon />,
    },
  ];
  const bankAccounts = {
    mainLabel: 'Bank of America long | BNFPPPTAS',
    secondLabel: handleAccountLength('KW4830QZ99564776959687524627847573583676856495857', 8),
    thirdLabel: 'USD',
    icon: <MasterCardLogo />,
  };
  const [isDefaultState, setIsDefaultState] = useState<boolean>(false);
  const [isModalAddShow, setIsModalAddShow] = useState<boolean>(false);
  const [bankType, setBankType] = useState<'routing' | 'bicAccount' | 'bicIBAN'>('routing');
  const [cardNumberValue, setCardNumberValue] = useState<string>('');
  const [accountNumberValue, setAccountNumberValue] = useState<string>('');
  const [ibanAccountValue, setIbanAccountValue] = useState<string>('');
  const [routingNumberValue, setRoutingNumberValue] = useState<string>('');
  const [bankNameValue, setBankNameValue] = useState<string>('');
  const [bankAddressValue, setBankAddressValue] = useState<string>('');
  const [bankAccountNameValue, setBankAccountNameValue] = useState<string>('');
  const [walletNameValue, setWalletNameValue] = useState<string>('');
  const [bicCodeValue, setBicCodeValue] = useState<string>('');
  const [cardHolderValue, setCardHolderValue] = useState<string>('');
  return (
    <Modal
      size="720"
      background="grey-10"
      type="new"
      pluginScrollDisabled
      padding="24"
      header={
        <ModalHeaderNew background="grey-10" title="Create new vendor" border onClose={() => {}} />
      }
      footer={
        <ModalFooterNew background="grey-10">
          <Button width="md" onClick={() => {}}>
            Save
          </Button>
        </ModalFooterNew>
      }
    >
      <MenuTabWrapper>
        <MenuTab id="general" onClick={() => {}}>
          General
        </MenuTab>
        <MenuTab id="contacts" onClick={() => {}}>
          Contacts
        </MenuTab>
        <MenuTab id="billing" active onClick={() => {}}>
          Billing
        </MenuTab>
        <MenuTab id="payment" onClick={() => {}}>
          Payment
        </MenuTab>
        <MenuTab id="accounting" onClick={() => {}}>
          Accounting
        </MenuTab>
      </MenuTabWrapper>
      <Container
        flexdirection="column"
        borderRadius="20"
        background="white-100"
        className={classNames(spacing.mt24, spacing.p24, spacing.h464min)}
        gap="24"
        overflow="auto"
      >
        <Container justifycontent="space-between" alignitems="center">
          <Text type="body-regular-14">Billing</Text>
          <Container gap="8">
            <Container alignitems="center" gap="4">
              <Icon icon={<StarFilledIcon />} path="grey-90" />
              <Text color="grey-90">Default</Text>
            </Container>
            <TagSegmented
              className={spacing.w300max}
              {...bankAccounts}
              iconStaticColor
              tooltipMessage={
                <Container flexdirection="column">
                  <Text color="white-100" style={{ wordBreak: 'break-all' }}>
                    {bankAccounts.mainLabel}
                  </Text>
                  <Text color="white-100" style={{ wordBreak: 'break-all' }}>
                    {bankAccounts.secondLabel} | {bankAccounts.thirdLabel}
                  </Text>
                </Container>
              }
            />
          </Container>
        </Container>
        <Divider fullHorizontalWidth />
        <Container justifycontent="space-between" alignitems="center">
          <GeneralTabWrapper>
            {billingTabs.map(({ type, label, icon }) => (
              <GeneralTab
                key={type}
                id={type}
                icon={icon}
                font="body-regular-14"
                onClick={() => setBillingTabActive(type)}
                active={billingTabActive === type}
              >
                {label}
              </GeneralTab>
            ))}
          </GeneralTabWrapper>
          <Button
            height="24"
            background="blue-10-blue-20"
            color="violet-90"
            font="text-medium"
            iconLeft={<PlusIcon />}
            onClick={() => setIsModalAddShow(true)}
          >
            Create new {billingTabs.find(({ type }) => type === billingTabActive)?.label}
          </Button>
        </Container>
        <Container
          gap="20"
          flexwrap="wrap"
          customScroll
          overflow={isModalAddShow ? 'hidden' : 'auto'}
        >
          {billingTabActive === 'creditCard' && (
            <BackdropWrapper isVisible={isModalAddShow}>
              <>
                {isModalAddShow && (
                  <Container
                    isFocusedElm={isModalAddShow}
                    position="fixed"
                    className={classNames(spacing.w624fixed, spacing.zInd1)}
                  >
                    <Container
                      flexdirection="column"
                      border="grey-20"
                      borderRadius="10"
                      gap="12"
                      background="grey-10"
                      className={classNames(spacing.p24, spacing.w100p)}
                    >
                      <Container justifycontent="space-between">
                        <Text type="body-medium" color="grey-100">
                          Create new Card
                        </Text>
                      </Container>
                      <Container justifycontent="space-between" gap="24">
                        <InputNew
                          name="cardNumber"
                          label="Card Number"
                          width="full"
                          background="white-100"
                          required
                          placeholder="XXXX XXXX XXXX XXXX"
                          icon={<CreditCardIcon />}
                          elementPosition="left"
                          onChange={(e) => {
                            setCardNumberValue(e.target.value);
                          }}
                          value={cardNumberValue}
                        />

                        <SelectNew
                          name="currency"
                          onChange={() => {}}
                          placeholder="Choose option"
                          width="200"
                          label="Currency"
                          background="white-100"
                          required
                        >
                          {({ onClick, state, selectElement }) => (
                            <SelectDropdown selectElement={selectElement}>
                              <SelectDropdownBody>
                                <SelectDropdownItemList>
                                  {options.map(({ label, value }) => (
                                    <OptionSingleNew
                                      key={value}
                                      label={label}
                                      selected={state?.value === value}
                                      onClick={() =>
                                        onClick({
                                          label,
                                          value,
                                        })
                                      }
                                    />
                                  ))}
                                </SelectDropdownItemList>
                              </SelectDropdownBody>
                            </SelectDropdown>
                          )}
                        </SelectNew>
                      </Container>
                      <Container justifycontent="space-between" gap="24">
                        <InputNew
                          name="bankName"
                          label="Bank Name"
                          required
                          width="full"
                          background="white-100"
                          icon={<BankIcon />}
                          elementPosition="left"
                          onChange={(e) => {
                            setBankNameValue(e.target.value);
                          }}
                          value={bankNameValue}
                        />
                        <InputNew
                          name="cardHolder"
                          label="Card Holder"
                          width="full"
                          required
                          background="white-100"
                          onChange={(e) => {
                            setCardHolderValue(e.target.value);
                          }}
                          value={cardHolderValue}
                        />
                      </Container>
                      <Container justifycontent="flex-end" gap="24">
                        <Button
                          width="auto"
                          height="40"
                          onClick={() => setIsModalAddShow(false)}
                          background="white-100"
                          border="grey-20-grey-90"
                          color="grey-100"
                        >
                          Cancel
                        </Button>
                        <Button width="auto" height="40" onClick={() => setIsModalAddShow(false)}>
                          Create
                        </Button>
                      </Container>
                    </Container>
                  </Container>
                )}

                {Array.from(Array(10)).map((_, index) => (
                  <BillingCardItem
                    key={index}
                    title="MonoBank"
                    secondaryTitle="Joe Biden"
                    account="****4567"
                    currency="USD"
                    paymentSystem={index % 2 === 0 ? 'mastercard' : 'visa'}
                    isDefault={isDefaultState}
                    onToggleDefault={() => setIsDefaultState((prev) => !prev)}
                    onEdit={() => {}}
                    onDelete={() => {}}
                  />
                ))}
              </>
            </BackdropWrapper>
          )}
          {billingTabActive === 'bankAccount' && (
            <BackdropWrapper isVisible={isModalAddShow}>
              <>
                {isModalAddShow && (
                  <Container
                    isFocusedElm={isModalAddShow}
                    position="fixed"
                    className={classNames(spacing.w624fixed, spacing.zInd1)}
                  >
                    <Container
                      flexdirection="column"
                      border="grey-20"
                      borderRadius="10"
                      gap="12"
                      background="grey-10"
                      className={classNames(spacing.p24, spacing.w100p)}
                    >
                      <Container justifycontent="space-between">
                        <Text type="body-medium" color="grey-100">
                          Create new Bank Account
                        </Text>
                      </Container>

                      <Container flexdirection="column" gap="12">
                        <Text type="caption-regular">
                          Please choose type of bank id and account details:
                        </Text>
                        <Container gap="20">
                          <Radio
                            id="routingAccount"
                            color="grey-100"
                            type="caption-regular"
                            onChange={() => setBankType('routing')}
                            name="bankType"
                            checked={bankType === 'routing'}
                          >
                            Routing number & Account number
                          </Radio>
                          <Radio
                            id="bicAccount"
                            color="grey-100"
                            type="caption-regular"
                            onChange={() => setBankType('bicAccount')}
                            name="bankType"
                            checked={bankType === 'bicAccount'}
                          >
                            BIC & Account number
                          </Radio>
                          <Radio
                            id="bicIBAN"
                            color="grey-100"
                            type="caption-regular"
                            onChange={() => setBankType('bicIBAN')}
                            name="bankType"
                            checked={bankType === 'bicIBAN'}
                          >
                            BIC & IBAN account
                          </Radio>
                        </Container>
                      </Container>
                      <Container justifycontent="space-between" gap="24">
                        {bankType === 'routing' && (
                          <InputNew
                            name="routingNumber"
                            label="Routing Number"
                            width="full"
                            background="white-100"
                            placeholder="XXXX"
                            icon={<InfoIcon />}
                            required
                            elementPosition="left"
                            onChange={(e) => {
                              setRoutingNumberValue(e.target.value);
                            }}
                            value={routingNumberValue}
                            tooltip={<RoutingNumberTooltip />}
                            tooltipWidth="auto"
                          />
                        )}
                        {(bankType === 'bicIBAN' || bankType === 'bicAccount') && (
                          <InputNew
                            name="bicCode"
                            label="BIC code"
                            width="full"
                            background="white-100"
                            placeholder="XXXX"
                            icon={<InfoIcon />}
                            required
                            elementPosition="left"
                            onChange={(e) => {
                              setBicCodeValue(e.target.value);
                            }}
                            value={bicCodeValue}
                            tooltip={<BICSwiftCodeTooltip />}
                            tooltipWidth="auto"
                          />
                        )}
                        <InputNew
                          name="bankName"
                          label="Bank Name"
                          required
                          width="full"
                          background="white-100"
                          icon={<BankIcon />}
                          elementPosition="left"
                          onChange={(e) => {
                            setBankAccountNameValue(e.target.value);
                          }}
                          value={bankAccountNameValue}
                        />
                      </Container>
                      <InputNew
                        name="bankAddress"
                        label="Bank Address"
                        width="full"
                        required
                        background="white-100"
                        onChange={(e) => {
                          setBankAddressValue(e.target.value);
                        }}
                        value={bankAddressValue}
                      />
                      <Container gap="24" justifycontent="space-between">
                        {(bankType === 'routing' || bankType === 'bicAccount') && (
                          <InputNew
                            name="accountNumber"
                            label="Account Number"
                            width="full"
                            required
                            background="white-100"
                            onChange={(e) => {
                              setAccountNumberValue(e.target.value);
                            }}
                            value={accountNumberValue}
                          />
                        )}
                        {bankType === 'bicIBAN' && (
                          <InputNew
                            required
                            name="ibanAccount"
                            label="IBAN account"
                            width="full"
                            background="white-100"
                            placeholder="XXXX"
                            icon={<InfoIcon />}
                            elementPosition="left"
                            onChange={(e) => {
                              setIbanAccountValue(e.target.value);
                            }}
                            value={ibanAccountValue}
                            tooltip={<IBANNumberTooltip />}
                            tooltipWidth="auto"
                          />
                        )}
                        <SelectNew
                          name="currency"
                          onChange={() => {}}
                          placeholder="Choose option"
                          width="200"
                          label="Currency"
                          background="white-100"
                          required
                        >
                          {({ onClick, state, selectElement }) => (
                            <SelectDropdown selectElement={selectElement}>
                              <SelectDropdownBody>
                                <SelectDropdownItemList>
                                  {options.map(({ label, value }) => (
                                    <OptionSingleNew
                                      key={value}
                                      label={label}
                                      selected={state?.value === value}
                                      onClick={() =>
                                        onClick({
                                          label,
                                          value,
                                        })
                                      }
                                    />
                                  ))}
                                </SelectDropdownItemList>
                              </SelectDropdownBody>
                            </SelectDropdown>
                          )}
                        </SelectNew>
                      </Container>
                      <Container justifycontent="flex-end" gap="24">
                        <Button
                          width="auto"
                          height="40"
                          onClick={() => setIsModalAddShow(false)}
                          background="white-100"
                          border="grey-20-grey-90"
                          color="grey-100"
                        >
                          Cancel
                        </Button>
                        <Button width="auto" height="40" onClick={() => setIsModalAddShow(false)}>
                          Create
                        </Button>
                      </Container>
                    </Container>
                  </Container>
                )}
                {Array.from(Array(10)).map((_, index) => (
                  <BillingCardItem
                    key={index}
                    title="Chartered Bank of India, Australia and China | CHASUS33XXX"
                    tertiaryTitle="Floor 4, The Columbus Building, 7 Westferry Circus, London, United Kingdom"
                    account="3686******6015 | USD"
                    isDefault={isDefaultState}
                    onToggleDefault={() => setIsDefaultState((prev) => !prev)}
                    onEdit={() => {}}
                    onDelete={() => {}}
                  />
                ))}
              </>
            </BackdropWrapper>
          )}
          {billingTabActive === 'wallet' && (
            <BackdropWrapper isVisible={isModalAddShow}>
              <>
                {isModalAddShow && (
                  <Container
                    isFocusedElm={isModalAddShow}
                    position="fixed"
                    className={classNames(spacing.w624fixed, spacing.zInd1)}
                  >
                    <Container
                      flexdirection="column"
                      border="grey-20"
                      borderRadius="10"
                      gap="12"
                      background="grey-10"
                      className={classNames(spacing.p24, spacing.w100p)}
                    >
                      <Text type="body-medium" color="grey-100">
                        Create new Wallet
                      </Text>
                      <Container justifycontent="space-between" gap="24">
                        <InputNew
                          name="walletName"
                          label="Wallet Name"
                          required
                          width="full"
                          icon={<WalletIcon />}
                          background="white-100"
                          elementPosition="left"
                          onChange={(e) => {
                            setWalletNameValue(e.target.value);
                          }}
                          value={walletNameValue}
                        />
                        <SelectNew
                          name="currency"
                          onChange={() => {}}
                          placeholder="Choose option"
                          width="200"
                          label="Currency"
                          background="white-100"
                          required
                        >
                          {({ onClick, state, selectElement }) => (
                            <SelectDropdown selectElement={selectElement}>
                              <SelectDropdownBody>
                                <SelectDropdownItemList>
                                  {options.map(({ label, value }) => (
                                    <OptionSingleNew
                                      key={value}
                                      label={label}
                                      selected={state?.value === value}
                                      onClick={() =>
                                        onClick({
                                          label,
                                          value,
                                        })
                                      }
                                    />
                                  ))}
                                </SelectDropdownItemList>
                              </SelectDropdownBody>
                            </SelectDropdown>
                          )}
                        </SelectNew>
                      </Container>
                      <InputNew
                        name="walletNumber"
                        label="Wallet number"
                        required
                        width="full"
                        background="white-100"
                      />
                      <Container justifycontent="flex-end" gap="24">
                        <Button
                          width="auto"
                          height="40"
                          onClick={() => setIsModalAddShow(false)}
                          background="white-100"
                          border="grey-20-grey-90"
                          color="grey-100"
                        >
                          Cancel
                        </Button>
                        <Button width="auto" height="40" onClick={() => setIsModalAddShow(false)}>
                          Create
                        </Button>
                      </Container>
                    </Container>
                  </Container>
                )}

                {Array.from(Array(10)).map((_, index) => (
                  <BillingCardItem
                    key={index}
                    title="Wallet name"
                    account="****2XXX"
                    currency="USD"
                    paymentSystem="afterpay"
                    isDefault={isDefaultState}
                    onToggleDefault={() => setIsDefaultState((prev) => !prev)}
                    onEdit={() => {}}
                    onDelete={() => {}}
                  />
                ))}
              </>
            </BackdropWrapper>
          )}
        </Container>
      </Container>
    </Modal>
  );
};

export const CreateNewVendorPaymentTab: React.FC = () => {
  return (
    <Modal
      size="720"
      background="grey-10"
      type="new"
      header={
        <ModalHeaderNew background="grey-10" title="Create new vendor" border onClose={() => {}} />
      }
      footer={
        <ModalFooterNew background="grey-10">
          <Button width="md" onClick={() => {}}>
            Save
          </Button>
        </ModalFooterNew>
      }
    >
      <MenuTabWrapper>
        <MenuTab id="general" onClick={() => {}}>
          General
        </MenuTab>
        <MenuTab id="contacts" onClick={() => {}}>
          Contacts
        </MenuTab>
        <MenuTab id="billing" onClick={() => {}}>
          Billing
        </MenuTab>
        <MenuTab id="payment" active onClick={() => {}}>
          Payment
        </MenuTab>
        <MenuTab id="accounting" onClick={() => {}}>
          Accounting
        </MenuTab>
      </MenuTabWrapper>
      <Container className={spacing.mt16} gap="8">
        <Icon icon={<InfoFilledIcon />} path="black-100" />
        <Container flexdirection="column">
          <Text>Ukraine, 90210 Broadway blvd</Text>
          <Text>Novaya, TN 37011-5678</Text>
        </Container>
      </Container>
      <Container
        flexdirection="column"
        borderRadius="20"
        background="white-100"
        className={classNames(spacing.p24, spacing.mt16, spacing.h360)}
      >
        <AssignContainer
          className={spacing.mt24}
          titleSource="Legal entity / Business unit"
          titleTarget="Payable account"
          width="660"
        >
          <AssignLine
            order="1"
            sourceValue="BetterMe health & fit"
            sourceSecondaryValue="BetterMe"
            isAssigned={false}
          >
            <SelectDouble
              onChange={() => {}}
              label="Assign Bank Account"
              placeholder="Assign Bank Account"
              name="product"
            >
              {({ onClick, state }) => (
                <>
                  {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                    <OptionDoubleVerticalNew
                      key={value}
                      label={label}
                      secondaryLabel={secondaryLabel}
                      selected={state?.value === value}
                      onClick={() =>
                        onClick({
                          label,
                          secondaryLabel,
                          value,
                        })
                      }
                    />
                  ))}
                </>
              )}
            </SelectDouble>
          </AssignLine>
          <AssignLine
            order="1"
            sourceValue="BetterMe health & fit"
            sourceSecondaryValue="BetterMe"
            isAssigned={false}
          >
            <SelectDouble
              onChange={() => {}}
              label="Assign Bank Account"
              placeholder="Assign Bank Account"
              name="product"
            >
              {({ onClick, state }) => (
                <>
                  {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                    <OptionDoubleVerticalNew
                      key={value}
                      label={label}
                      secondaryLabel={secondaryLabel}
                      selected={state?.value === value}
                      onClick={() =>
                        onClick({
                          label,
                          secondaryLabel,
                          value,
                        })
                      }
                    />
                  ))}
                </>
              )}
            </SelectDouble>
          </AssignLine>
          <AssignLine
            order="1"
            sourceValue="BetterMe health & fit"
            sourceSecondaryValue="BetterMe"
            isAssigned={false}
          >
            <SelectDouble
              onChange={() => {}}
              label="Assign Bank Account"
              placeholder="Assign Bank Account"
              name="product"
            >
              {({ onClick, state }) => (
                <>
                  {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                    <OptionDoubleVerticalNew
                      key={value}
                      label={label}
                      secondaryLabel={secondaryLabel}
                      selected={state?.value === value}
                      onClick={() =>
                        onClick({
                          label,
                          secondaryLabel,
                          value,
                        })
                      }
                    />
                  ))}
                </>
              )}
            </SelectDouble>
          </AssignLine>
          <AssignLine
            order="1"
            sourceValue="BetterMe health & fit"
            sourceSecondaryValue="BetterMe"
            isAssigned={false}
          >
            <SelectDouble
              onChange={() => {}}
              label="Assign Bank Account"
              placeholder="Assign Bank Account"
              name="product"
            >
              {({ onClick, state }) => (
                <>
                  {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                    <OptionDoubleVerticalNew
                      key={value}
                      label={label}
                      secondaryLabel={secondaryLabel}
                      selected={state?.value === value}
                      onClick={() =>
                        onClick({
                          label,
                          secondaryLabel,
                          value,
                        })
                      }
                    />
                  ))}
                </>
              )}
            </SelectDouble>
          </AssignLine>
          <AssignLine
            order="1"
            sourceValue="BetterMe health & fit"
            sourceSecondaryValue="BetterMe"
            isAssigned={false}
          >
            <SelectDouble
              onChange={() => {}}
              label="Assign Bank Account"
              placeholder="Assign Bank Account"
              name="product"
            >
              {({ onClick, state }) => (
                <>
                  {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                    <OptionDoubleVerticalNew
                      key={value}
                      label={label}
                      secondaryLabel={secondaryLabel}
                      selected={state?.value === value}
                      onClick={() =>
                        onClick({
                          label,
                          secondaryLabel,
                          value,
                        })
                      }
                    />
                  ))}
                </>
              )}
            </SelectDouble>
          </AssignLine>
          <AssignLine
            order="1"
            sourceValue="BetterMe health & fit"
            sourceSecondaryValue="BetterMe"
            isAssigned={false}
          >
            <SelectDouble
              onChange={() => {}}
              label="Assign Bank Account"
              placeholder="Assign Bank Account"
              name="product"
            >
              {({ onClick, state }) => (
                <>
                  {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                    <OptionDoubleVerticalNew
                      key={value}
                      label={label}
                      secondaryLabel={secondaryLabel}
                      selected={state?.value === value}
                      onClick={() =>
                        onClick({
                          label,
                          secondaryLabel,
                          value,
                        })
                      }
                    />
                  ))}
                </>
              )}
            </SelectDouble>
          </AssignLine>
        </AssignContainer>
      </Container>
    </Modal>
  );
};

export const CreateNewVendorAccountingTab: React.FC = () => {
  const [amountValue, setAmountValue] = useState('');
  return (
    <Modal
      size="720"
      background="grey-10"
      type="new"
      header={
        <ModalHeaderNew background="grey-10" title="Create new vendor" border onClose={() => {}} />
      }
      footer={
        <ModalFooterNew background="grey-10">
          <Button width="md" onClick={() => {}}>
            Save
          </Button>
        </ModalFooterNew>
      }
    >
      <MenuTabWrapper>
        <MenuTab id="general" onClick={() => {}}>
          General
        </MenuTab>
        <MenuTab id="contacts" onClick={() => {}}>
          Contacts
        </MenuTab>
        <MenuTab id="billing" onClick={() => {}}>
          Billing
        </MenuTab>
        <MenuTab id="payment" onClick={() => {}}>
          Payment
        </MenuTab>
        <MenuTab id="accounting" active onClick={() => {}}>
          Accounting
        </MenuTab>
      </MenuTabWrapper>
      <Container
        flexdirection="column"
        borderRadius="20"
        background="white-100"
        className={classNames(spacing.p24, spacing.mt24, spacing.h360)}
      >
        <Text type="body-regular-14">Choose default receivables account</Text>

        <AssignContainer
          className={spacing.mt24}
          titleSource="Legal entity / Business unit"
          titleTarget="Payable account"
          width="660"
        >
          <AssignLine
            order="1"
            sourceValue="BetterMe health & fit"
            sourceSecondaryValue="BetterMe"
            isAssigned={false}
          >
            <SelectDouble
              onChange={() => {}}
              label="Assign Bank Account"
              placeholder="Assign Bank Account"
              name="product"
            >
              {({ onClick, state }) => (
                <>
                  {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                    <OptionDoubleVerticalNew
                      key={value}
                      label={label}
                      secondaryLabel={secondaryLabel}
                      selected={state?.value === value}
                      onClick={() =>
                        onClick({
                          label,
                          secondaryLabel,
                          value,
                        })
                      }
                    />
                  ))}
                </>
              )}
            </SelectDouble>
          </AssignLine>
          <AssignLine
            order="1"
            sourceValue="BetterMe health & fit"
            sourceSecondaryValue="BetterMe"
            isAssigned={false}
          >
            <SelectDouble
              onChange={() => {}}
              label="Assign Bank Account"
              placeholder="Assign Bank Account"
              name="product"
            >
              {({ onClick, state }) => (
                <>
                  {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                    <OptionDoubleVerticalNew
                      key={value}
                      label={label}
                      secondaryLabel={secondaryLabel}
                      selected={state?.value === value}
                      onClick={() =>
                        onClick({
                          label,
                          secondaryLabel,
                          value,
                        })
                      }
                    />
                  ))}
                </>
              )}
            </SelectDouble>
          </AssignLine>
          <AssignLine
            order="1"
            sourceValue="BetterMe health & fit"
            sourceSecondaryValue="BetterMe"
            isAssigned={false}
          >
            <SelectDouble
              onChange={() => {}}
              label="Assign Bank Account"
              placeholder="Assign Bank Account"
              name="product"
            >
              {({ onClick, state }) => (
                <>
                  {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                    <OptionDoubleVerticalNew
                      key={value}
                      label={label}
                      secondaryLabel={secondaryLabel}
                      selected={state?.value === value}
                      onClick={() =>
                        onClick({
                          label,
                          secondaryLabel,
                          value,
                        })
                      }
                    />
                  ))}
                </>
              )}
            </SelectDouble>
          </AssignLine>
          <AssignLine
            order="1"
            sourceValue="BetterMe health & fit"
            sourceSecondaryValue="BetterMe"
            isAssigned={false}
          >
            <SelectDouble
              onChange={() => {}}
              label="Assign Bank Account"
              placeholder="Assign Bank Account"
              name="product"
            >
              {({ onClick, state }) => (
                <>
                  {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                    <OptionDoubleVerticalNew
                      key={value}
                      label={label}
                      secondaryLabel={secondaryLabel}
                      selected={state?.value === value}
                      onClick={() =>
                        onClick({
                          label,
                          secondaryLabel,
                          value,
                        })
                      }
                    />
                  ))}
                </>
              )}
            </SelectDouble>
          </AssignLine>
          <AssignLine
            order="1"
            sourceValue="BetterMe health & fit"
            sourceSecondaryValue="BetterMe"
            isAssigned={false}
          >
            <SelectDouble
              onChange={() => {}}
              label="Assign Bank Account"
              placeholder="Assign Bank Account"
              name="product"
            >
              {({ onClick, state }) => (
                <>
                  {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                    <OptionDoubleVerticalNew
                      key={value}
                      label={label}
                      secondaryLabel={secondaryLabel}
                      selected={state?.value === value}
                      onClick={() =>
                        onClick({
                          label,
                          secondaryLabel,
                          value,
                        })
                      }
                    />
                  ))}
                </>
              )}
            </SelectDouble>
          </AssignLine>
          <AssignLine
            order="1"
            sourceValue="BetterMe health & fit"
            sourceSecondaryValue="BetterMe"
            isAssigned={false}
          >
            <SelectDouble
              onChange={() => {}}
              label="Assign Bank Account"
              placeholder="Assign Bank Account"
              name="product"
            >
              {({ onClick, state }) => (
                <>
                  {options.slice(0, 4).map(({ label, secondaryLabel, value }) => (
                    <OptionDoubleVerticalNew
                      key={value}
                      label={label}
                      secondaryLabel={secondaryLabel}
                      selected={state?.value === value}
                      onClick={() =>
                        onClick({
                          label,
                          secondaryLabel,
                          value,
                        })
                      }
                    />
                  ))}
                </>
              )}
            </SelectDouble>
          </AssignLine>
        </AssignContainer>
      </Container>
      <Container
        flexdirection="column"
        borderRadius="20"
        background="white-100"
        className={classNames(spacing.mt24, spacing.p24)}
      >
        <Text type="body-regular-14">Opening balance</Text>
        <Container
          justifycontent="space-between"
          className={classNames(spacing.w100p, spacing.mt24)}
        >
          <SelectNew
            name="businessDivision"
            onChange={() => {}}
            width="300"
            label="Business division"
          >
            {({ onClick, state, selectElement }) => (
              <SelectDropdown selectElement={selectElement}>
                <SelectDropdownBody>
                  <SelectDropdownItemList>
                    {options.map(({ label, value }) => (
                      <OptionSingleNew
                        key={value}
                        label={label}
                        selected={state?.value === value}
                        onClick={() =>
                          onClick({
                            label,
                            value,
                          })
                        }
                      />
                    ))}
                  </SelectDropdownItemList>
                </SelectDropdownBody>
              </SelectDropdown>
            )}
          </SelectNew>
          <SelectNew name="legalEntity" onChange={() => {}} width="300" label="Legal entity">
            {({ onClick, state, selectElement }) => (
              <SelectDropdown selectElement={selectElement}>
                <SelectDropdownBody>
                  <SelectDropdownItemList>
                    {options.map(({ label, value }) => (
                      <OptionSingleNew
                        key={value}
                        label={label}
                        selected={state?.value === value}
                        onClick={() =>
                          onClick({
                            label,
                            value,
                          })
                        }
                      />
                    ))}
                  </SelectDropdownItemList>
                </SelectDropdownBody>
              </SelectDropdown>
            )}
          </SelectNew>
        </Container>
        <SelectNew
          className={spacing.mt24}
          name="account"
          onChange={() => {}}
          width="full"
          label="Account"
        >
          {({ onClick, state, selectElement }) => (
            <SelectDropdown selectElement={selectElement}>
              <SelectDropdownBody>
                <SelectDropdownItemList>
                  {options.map(({ label, value }) => (
                    <OptionSingleNew
                      key={value}
                      label={label}
                      selected={state?.value === value}
                      onClick={() =>
                        onClick({
                          label,
                          value,
                        })
                      }
                    />
                  ))}
                </SelectDropdownItemList>
              </SelectDropdownBody>
            </SelectDropdown>
          )}
        </SelectNew>
        <Container
          justifycontent="space-between"
          className={classNames(spacing.w100p, spacing.mt24)}
        >
          <InputNew
            name="amount"
            label="Amount"
            width="300"
            value={amountValue}
            onChange={(e) => {
              setAmountValue(e.target.value);
            }}
          />
          <InputDate
            label="Choose date"
            name="calendar"
            selected={new Date()}
            calendarStartDay={1}
            width="300"
            onChange={() => {}}
          />
        </Container>
      </Container>
      <Container className={spacing.mt24} justifycontent="flex-end">
        <LinkButton icon={<PlusIcon />} onClick={() => {}}>
          Add one more opening balance
        </LinkButton>
      </Container>
    </Modal>
  );
};
