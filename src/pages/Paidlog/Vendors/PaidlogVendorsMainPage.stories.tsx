import { Meta, Story } from '@storybook/react';
import { DropDown } from 'Components/DropDown';
import { Checkbox } from 'Components/base/Checkbox';
import { Divider } from 'Components/base/Divider';
import { LoadingLine } from 'Components/base/LoadingLine';
import { TagSegmented } from 'Components/base/TagSegmented';
import { Button } from 'Components/base/buttons/Button';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { ContainerMain } from 'Components/base/grid/ContainerMain';
import { MainPageContentContainer } from 'Components/base/grid/MainPageContentContainer';
import { MenuTab, MenuTabWrapper } from 'Components/base/tabs/MenuTab';
import { Text } from 'Components/base/typography/Text';
import { MainPageEmpty } from 'Components/custom/MainPage/MainPageEmpty';
import { HeaderSB } from 'Components/custom/Stories/HeaderSB';
import { NavigationSB } from 'Components/custom/Stories/NavigationSB';
import { Pagination } from 'Components/custom/Stories/Pagination/Pagination';
import PerPageSelect from 'Components/custom/Stories/Pagination/PerPageSelect/PerPageSelect';
import { ExportDropdown } from 'Components/elements/ExportDropdown';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { FilterDropDown } from 'Components/elements/filters/FilterDropDown';
import { FilterToggleButton } from 'Components/elements/filters/FilterToggleButton';
import { FiltersWrapper } from 'Components/elements/filters/FiltersWrapper';
import { Search } from 'Components/inputs/Search';
import { rowsPerPageOptions } from 'Mocks/fakeOptions';
import { bankAccounts } from 'Mocks/purchasesFakeData';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as DropdownIcon } from 'Svg/v2/16/three-dots.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import { ReactComponent as UploadIcon } from 'Svg/v2/16/upload.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';
import type { TableCellWidth } from '../../../components/types/gridTypes';

const PaidlogVendorsMainPageComponent: Story = ({ storyState }) => {
  const [toggle, toggleMenu] = useState<boolean>(false);
  const [showFilters, setShowFilters] = useState<boolean>(false);
  const [rowsPerPage, setRowsPerPage] = useState<string>(rowsPerPageOptions[0].value);

  const sorting = {
    sortType: null,
    tooltipText: 'Sorting tooltip text',
  };
  const tableHead = [
    {
      minWidth: '48',
      title: <Checkbox name="tickAll" checked onChange={() => {}} />,
      padding: 'none',
    },
    {
      minWidth: '160',
      title: 'Name',
      sorting,
    },
    {
      minWidth: '200',
      title: 'Contact information',
    },
    {
      minWidth: '240',
      title: 'Billing address',
    },
    {
      minWidth: '180',
      title: 'Notes',
    },
    {
      minWidth: '340',
      title: 'Payment method',
    },
    {
      minWidth: '100',
      title: 'Action',
      align: 'center',
    },
  ];
  const filters = [
    { title: 'Name' },
    { title: 'Payment method' },
    { title: 'Bank account' },
    { title: 'Card' },
    { title: 'Wallet' },
  ];
  return (
    <Container fullscreen flexwrap="nowrap" background={'grey-10'}>
      <NavigationSB opened={toggle} onToggleOpen={() => toggleMenu(!toggle)} />
      <ContainerMain navOpened={toggle}>
        <HeaderSB />
        <Container flexdirection="column" className={spacing.mX32}>
          <Text type="h1-semibold">Paidlog</Text>
          <MenuTabWrapper className={spacing.mt24}>
            <MenuTab id="transactions" active={false} onClick={() => {}}>
              Transactions
            </MenuTab>
            <MenuTab id="bankAccounts" active onClick={() => {}}>
              Vendors
            </MenuTab>
            <MenuTab id="rules" active={false} onClick={() => {}}>
              Text
            </MenuTab>
          </MenuTabWrapper>
        </Container>
        <MainPageContentContainer>
          {storyState === 'loaded' && (
            <>
              <Container
                className={classNames(spacing.mX24, spacing.mt24)}
                justifycontent="space-between"
                gap="24"
              >
                <Container gap="24" alignitems="center">
                  <FilterToggleButton
                    showFilters={showFilters}
                    filtersSelected={false}
                    onClear={() => {}}
                    onClick={() => {
                      setShowFilters((prevState) => !prevState);
                    }}
                  />
                  <Search
                    width="320"
                    height="40"
                    name="search-paidlog-vendors-mainPage"
                    placeholder="Search by Vendor name"
                    value=""
                    onChange={() => {}}
                    searchParams={[]}
                    onSearch={() => {}}
                    onClear={() => {}}
                  />
                </Container>
                <Container gap="24" alignitems="center">
                  <Divider type="vertical" background={'grey-90'} height="20" />
                  <ExportDropdown onExportToXLS={() => {}} onExportToCSV={() => {}} />
                  <IconButton
                    size="40"
                    background={'grey-10-grey-20'}
                    color="grey-100"
                    icon={<UploadIcon />}
                    tooltip="Import files"
                    onClick={() => {}}
                  />

                  <Button
                    background={'violet-90-violet-100'}
                    color="white-100"
                    width="140"
                    height="40"
                    iconLeft={<PlusIcon />}
                  >
                    Create
                  </Button>
                </Container>
              </Container>
              <FiltersWrapper active={showFilters}>
                {filters.map((item) => (
                  <FilterDropDown
                    key={item.title}
                    title={item.title}
                    value="All"
                    onClear={() => {}}
                    selectedAmount={0}
                  />
                ))}
              </FiltersWrapper>
              <TableNew
                noResults={false}
                className={classNames(spacing.mt24, spacing.ml24, spacing.pr24)}
                tableHead={
                  <TableHeadNew>
                    {tableHead.map((item, index) => (
                      <TableTitleNew
                        key={index}
                        minWidth={item.minWidth as TableCellWidth}
                        align={item.align as TableTitleNewProps['align']}
                        title={item.title}
                        sorting={item.sorting}
                        onClick={() => {}}
                      />
                    ))}
                  </TableHeadNew>
                }
              >
                <TableBodyNew>
                  {Array.from(Array(39)).map((_, index) => (
                    <TableRowNew key={index} gap="8">
                      <TableCellNew minWidth="48" justifycontent="center" alignitems="center">
                        <Checkbox name={`line-${index}`} checked onChange={() => {}} />
                      </TableCellNew>
                      <TableCellTextNew
                        minWidth="160"
                        type="caption-semibold"
                        value="RozetkaUA"
                        secondaryColor="grey-100"
                        secondaryValue="xxx0001"
                      />
                      <TableCellTextNew
                        minWidth="200"
                        value="Vendor@gmail.com"
                        secondaryColor="grey-100"
                        secondaryValue="+380 93 987 73 32"
                      />
                      <TableCellTextNew
                        minWidth="240"
                        value="Ukraine, 90210 Broadway blvd"
                        color="grey-100"
                        secondaryValue="Novaya, TN 37011-5678"
                        secondaryColor="grey-100"
                      />
                      <TableCellTextNew
                        minWidth="180"
                        value="Regular vendor for any time"
                        color="grey-100"
                        lineClamp="2"
                      />
                      <TableCellNew minWidth="340" alignitems="center" fixedWidth>
                        <TagSegmented
                          className={spacing.w300max}
                          {...bankAccounts}
                          iconStaticColor
                          tooltipMessage={
                            <Container flexdirection="column">
                              <Text color="white-100" style={{ wordBreak: 'break-all' }}>
                                {bankAccounts.mainLabel}
                              </Text>
                              <Text color="white-100" style={{ wordBreak: 'break-all' }}>
                                {bankAccounts.secondLabel} | {bankAccounts.thirdLabel}
                              </Text>
                            </Container>
                          }
                        />
                      </TableCellNew>
                      <TableCellNew minWidth="100" justifycontent="center" alignitems="center">
                        <DropDown
                          flexdirection="column"
                          padding="8"
                          control={({ handleOpen }) => (
                            <IconButton
                              icon={<DropdownIcon />}
                              onClick={handleOpen}
                              background={'transparent'}
                              color="grey-100-violet-90"
                            />
                          )}
                        >
                          <DropDownButton size="160" icon={<EditIcon />} onClick={() => {}}>
                            Edit
                          </DropDownButton>
                          <DropDownButton
                            size="160"
                            type="danger"
                            icon={<TrashIcon />}
                            onClick={() => {}}
                          >
                            Delete
                          </DropDownButton>
                        </DropDown>
                      </TableCellNew>
                    </TableRowNew>
                  ))}
                </TableBodyNew>
              </TableNew>
              <Pagination
                customPagination={
                  <Container gap="16" alignitems="center">
                    <Text type="subtext-regular" color="grey-100">
                      Rows per Page
                    </Text>
                    <PerPageSelect
                      selectedValue={rowsPerPage}
                      onOptionClick={(value) => setRowsPerPage(value)}
                    />
                  </Container>
                }
              />
            </>
          )}
          {storyState === 'empty' && (
            <MainPageEmpty
              title="Create new Vendor"
              description="You have no vendors created. Your vendors will be displayed here."
              action={
                <Button
                  width="220"
                  height="40"
                  background={'blue-10-blue-20'}
                  color="violet-90-violet-100"
                  iconLeft={<PlusIcon />}
                  onClick={() => {}}
                >
                  Create new vendor
                </Button>
              }
            />
          )}
        </MainPageContentContainer>
        <LoadingLine loadingCompleted={storyState !== 'loading'} />
      </ContainerMain>
    </Container>
  );
};

export default {
  title: 'Pages/Paidlog/Vendors/Paidlog Vendors Main Page',
  component: PaidlogVendorsMainPageComponent,
  argTypes: {
    storyState: {
      options: ['empty', 'loading', 'loaded'],
      control: {
        type: 'radio',
        labels: {
          empty: 'Empty page',
          loading: 'Page is loading',
          loaded: 'Page loaded',
        },
      },
    },
  },
} as Meta;

export const PaidlogVendorsMainPage = PaidlogVendorsMainPageComponent.bind({});
PaidlogVendorsMainPage.args = {
  storyState: 'loaded',
};
