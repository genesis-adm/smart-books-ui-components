import { ContentLoader } from 'Components/base/ContentLoader';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import React, { ReactElement } from 'react';

import spacing from '../../../../../../assets/styles/spacing.module.scss';

type CreateIntercompanyModalFooterProps = {
  isLoading: boolean;
};

const CreateIntercompanyModalFooter = ({
  isLoading,
}: CreateIntercompanyModalFooterProps): ReactElement => {
  return (
    <ModalFooterNew gap={'16'} border>
      <ContentLoader width="153px" height="48px" isLoading={isLoading}>
        <Container alignitems="center" gap="16" flexgrow="1" justifycontent="flex-end">
          <Container flexdirection="column" className={spacing.w150fixed}>
            <Text type="body-regular-14" color="grey-100">
              Step: 2 of 2
            </Text>
            <Text type="body-regular-14" color="grey-100">
              Next: Create match
            </Text>
          </Container>
          <Button height="48" width="150" onClick={() => {}} background={'violet-90-violet-100'}>
            Create
          </Button>
        </Container>
      </ContentLoader>
    </ModalFooterNew>
  );
};

export default CreateIntercompanyModalFooter;
