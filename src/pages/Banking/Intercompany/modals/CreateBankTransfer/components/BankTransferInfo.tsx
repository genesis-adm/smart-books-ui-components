import { ContentLoader } from 'Components/base/ContentLoader';
import { TagSegmented } from 'Components/base/TagSegmented';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import TextWithTooltip from 'Pages/Banking/Intercompany/modals/CreateBankTransfer/components/TextWithTooltip';
import { bankAccounts } from 'Pages/Banking/Transactions/constants';
import { ReactComponent as CreditCardIcon } from 'Svg/v2/16/credit-card.svg';
import React, { ReactElement } from 'react';

import spacing from '../../../../../../assets/styles/spacing.module.scss';

type BankTransferInfoProps = { isLoading: boolean };

const BankTransferInfo = ({ isLoading }: BankTransferInfoProps): ReactElement => {
  return (
    <Container flexdirection="column" gap="16" className={spacing.pl24}>
      <ContentLoader isLoading={isLoading} height="16px" width="318px">
        <Container gap="4" alignitems="center">
          <Text color="grey-100" type="caption-regular">
            Bank account:
          </Text>
          <TagSegmented
            {...bankAccounts}
            icon={<CreditCardIcon />}
            tooltipMessage={
              <Container flexdirection="column">
                <Text color="white-100" style={{ wordBreak: 'break-all' }}>
                  {bankAccounts.mainLabel}
                </Text>
                <Text color="white-100" style={{ wordBreak: 'break-all' }}>
                  {bankAccounts.secondLabel} | {bankAccounts.thirdLabel}
                </Text>
              </Container>
            }
          />
        </Container>
      </ContentLoader>
      <ContentLoader isLoading={isLoading} height="16px" width="318px">
        <Container gap="4" alignitems="center">
          <Text color="grey-100" type="caption-regular">
            Legal entity:
          </Text>
          <TextWithTooltip
            text="BetterMe Limited International sdjhvdish efjlrlehfeuirwrgh errghe rge"
            fontColor="black-100"
            fontType="caption-regular"
            maxWidth="230"
          />
        </Container>
      </ContentLoader>
      <ContentLoader isLoading={isLoading} height="16px" width="318px">
        <Container gap="4" alignitems="center">
          <Text color="grey-100" type="caption-regular">
            Business unit:
          </Text>
          <TextWithTooltip
            text="BetterMe Limited  sdjhvdish efjlrlehfeuirwrgh errghe rge"
            fontColor="black-100"
            fontType="caption-regular"
            maxWidth="230"
          />
        </Container>
      </ContentLoader>
    </Container>
  );
};

export default BankTransferInfo;
