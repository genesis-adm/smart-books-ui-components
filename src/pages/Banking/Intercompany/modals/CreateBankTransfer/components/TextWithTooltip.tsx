import { Tooltip } from 'Components/base/Tooltip';
import { Text } from 'Components/base/typography/Text';
import { TextColorNewTypes } from 'Components/types/colorTypes';
import { FontTypes } from 'Components/types/fontTypes';
import React from 'react';
import { ReactElement, useRef } from 'react';

import spacing from '../../../../../../assets/styles/spacing.module.scss';
import { useTooltipValue } from '../../../../../../hooks/useTooltipValue';

type TextWithTooltipProps = {
  text: string;
  maxWidth?: string;
  fontType: FontTypes;
  fontColor?: TextColorNewTypes;
};
const TextWithTooltip = ({
  text,
  maxWidth = '100p',
  fontType,
  fontColor,
}: TextWithTooltipProps): ReactElement => {
  const textRef = useRef(null);
  const textTooltipMessage = useTooltipValue(textRef, text);
  const width = `w${maxWidth}`;

  return (
    <Tooltip message={textTooltipMessage}>
      <Text ref={textRef} color={fontColor} noWrap type={fontType} className={spacing[width]}>
        {text}
      </Text>
    </Tooltip>
  );
};
export default TextWithTooltip;
