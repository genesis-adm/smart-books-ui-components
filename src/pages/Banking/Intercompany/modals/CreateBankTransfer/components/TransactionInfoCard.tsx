import { ContentLoader } from 'Components/base/ContentLoader';
import { Icon } from 'Components/base/Icon';
import { Tooltip } from 'Components/base/Tooltip';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { CreateType } from 'Pages/Banking/Intercompany/modals/CreateBankTransfer/CreateIntercompanyModal.stories';
import {
  TransactionData,
  TransactionsType,
} from 'Pages/Banking/Intercompany/modals/MatchTransactions/constants';
import { ReactComponent as ArrowDownIcon } from 'Svg/24/arrow-down.svg';
import { ReactComponent as ArrowUpIcon } from 'Svg/24/arrow-up.svg';
import { ReactComponent as BankIcon } from 'Svg/v2/12/bank.svg';
import classNames from 'classnames';
import React, { ReactElement, ReactNode, useRef } from 'react';

import spacing from '../../../../../../assets/styles/spacing.module.scss';
import { useTooltipValue } from '../../../../../../hooks/useTooltipValue';

type TransactionInfoCardProps = {
  transaction: TransactionData;
  type: CreateType;
  isLoading: boolean;
};

const TransactionInfoCard = ({
  transaction,
  type,
  isLoading,
}: TransactionInfoCardProps): ReactElement => {
  const descriptionRef = useRef(null);
  const descriptionTooltipMessage = useTooltipValue(descriptionRef, transaction.description);

  const iconsBasedOnType: Record<
    TransactionsType,
    { icon: ReactNode; color: 'red-90' | 'green-90' }
  > = {
    inflow: { icon: <ArrowUpIcon />, color: 'green-90' },
    outflow: {
      icon: <ArrowDownIcon />,
      color: 'red-90',
    },
  };

  if (isLoading) return <ContentLoader width="468px" height="224px" isLoading />;

  return (
    <Container
      flexdirection="column"
      justifycontent="space-between"
      background={'grey-10'}
      border={'grey-20'}
      borderRadius="20"
      className={classNames(spacing.w468fixed, spacing.h224fixed, spacing.p24)}
    >
      <Container justifycontent="space-between">
        <Container gap="8" alignitems="center">
          <IconButton
            icon={iconsBasedOnType[transaction.type].icon}
            shape="circle"
            size="50"
            background={iconsBasedOnType[transaction.type].color}
            color={'white-100'}
          />
          <Container flexdirection="column" gap="4">
            <Text color={'black-100'} type="caption-semibold">
              ID {transaction.id}
            </Text>
            <Text color={'grey-100'} type="text-regular">
              08 Jul 2024
            </Text>
          </Container>
        </Container>
        <Container flexdirection="column" gap="4" alignitems="flex-end">
          <Text color={iconsBasedOnType[transaction.type].color} type="h2-bold">
            {transaction.amount.mainAmount}
          </Text>
          <Text color="grey-90" type="body-medium">
            {transaction.amount.secondaryAmount}
          </Text>
        </Container>
      </Container>
      <Container justifycontent="space-between" alignitems="flex-end">
        <Container gap="4">
          <Text type="caption-regular" color="grey-100">
            Description:
          </Text>
          <Tooltip message={descriptionTooltipMessage}>
            <Text
              ref={descriptionRef}
              lineClamp="2"
              type="text-regular"
              className={classNames(spacing.w200)}
            >
              {transaction.description}
            </Text>
          </Tooltip>
        </Container>
        {transaction.type === 'outflow' && type === 'foreignExchange' && (
          <Container flexdirection="column" alignitems="flex-end" gap="4">
            <Text color="grey-100" type="text-regular">
              FX result gain / loss
            </Text>
            <Text type="caption-semibold"> 10.00 $</Text>
          </Container>
        )}
        {transaction.type === 'inflow' && type === 'bankTransfer' && (
          <Container flexdirection="column" alignitems="flex-end" gap="4">
            <Container gap="4">
              <Icon icon={<BankIcon />} />
              <Text color="grey-100" type="text-regular">
                Bank fee
              </Text>
            </Container>
            <Text type="caption-semibold">- 100.00 €</Text>
            <Text color="grey-90" type="text-regular">
              104.04 $
            </Text>
          </Container>
        )}
      </Container>
    </Container>
  );
};

export default TransactionInfoCard;
