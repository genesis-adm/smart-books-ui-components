import { Tag } from 'Components/base/Tag';
import { ActionType } from 'Components/elements/ActionsPopup/ActionsPopup.types';
import { StatusType } from 'Pages/Banking/Intercompany/constants';
import { CreateType } from 'Pages/Banking/Intercompany/modals/CreateBankTransfer/CreateIntercompanyModal.stories';
import { ReactComponent as ForReviewIcon } from 'Svg/v2/16/document-search.svg';
import { ReactComponent as DocumentIcon } from 'Svg/v2/16/file-blank.svg';
import { ReactComponent as AcceptedIcon } from 'Svg/v2/16/tick-in-filled-circle.svg';
import { ReactComponent as ArrowsSortHorizontallyIcon } from 'Svg/v2/24/arrows-sort-horizontally.svg';
import { ReactComponent as RefreshArrowsIcon } from 'Svg/v2/24/refresh-arrows.svg';
import React, { ReactNode } from 'react';

export type ModalMode = 'create' | 'view';

export const statusTag: Record<StatusType, ReactNode> = {
  forReview: (
    <Tag padding="8" color="grey" icon={<ForReviewIcon />} cursor="default" text="For review" />
  ),
  accepted: (
    <Tag padding="8" color="green" icon={<AcceptedIcon />} cursor="default" text="Accepted" />
  ),
};

export const modalValuesBasedOnType: Record<
  CreateType,
  { title: Record<ModalMode, string>; icon: ReactNode }
> = {
  bankTransfer: {
    title: { create: 'Create Bank transfer', view: 'View  Bank transfer # 123213' },
    icon: <ArrowsSortHorizontallyIcon />,
  },
  foreignExchange: {
    title: { create: 'Create Foreign exchange', view: 'View  Foreign exchange # 123213' },
    icon: <RefreshArrowsIcon />,
  },
};

export const modalHeight: Record<ModalMode, '690' | '739'> = {
  create: '690',
  view: '739',
};

export const tagBasedOnModalMode: Record<ModalMode, ReactNode> = {
  create: <Tag padding="2-8" color="green" icon={<DocumentIcon />} cursor="default" text="New" />,
  view: statusTag['accepted'],
};

export const popupActions: Record<StatusType, ActionType> = {
  forReview: 'actionButtons',
  accepted: 'accepted',
};
