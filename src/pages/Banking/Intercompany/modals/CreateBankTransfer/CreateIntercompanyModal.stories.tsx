import { Meta, Story } from '@storybook/react';
import { DropDown } from 'Components/DropDown';
import { ContentLoader } from 'Components/base/ContentLoader';
import { Divider } from 'Components/base/Divider';
import Icon from 'Components/base/Icon/Icon';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import ModalHeaderPlaceholder from 'Components/custom/placeholders/ModalHeaderPlaceholder';
import { ActionsPopup } from 'Components/elements/ActionsPopup';
import { Modal } from 'Components/modules/Modal';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { StatusType } from 'Pages/Banking/Intercompany/constants';
import BankTransferInfo from 'Pages/Banking/Intercompany/modals/CreateBankTransfer/components/BankTransferInfo';
import CreateIntercompanyModalFooter from 'Pages/Banking/Intercompany/modals/CreateBankTransfer/components/CreateIntercompanyModalFooter';
import TransactionInfoCard from 'Pages/Banking/Intercompany/modals/CreateBankTransfer/components/TransactionInfoCard';
import {
  ModalMode,
  modalHeight,
  modalValuesBasedOnType,
  popupActions,
  tagBasedOnModalMode,
} from 'Pages/Banking/Intercompany/modals/CreateBankTransfer/constants';
import { tableRowData } from 'Pages/Banking/Intercompany/modals/MatchTransactions/constants';
import { ReactComponent as EyeIcon } from 'Svg/v2/16/eye.svg';
import { ReactComponent as DropdownIcon } from 'Svg/v2/16/three-dots.svg';
import { ReactComponent as TickIcon } from 'Svg/v2/16/tick-in-filled-circle.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import React, { ReactNode } from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';

export type CreateType = 'bankTransfer' | 'foreignExchange';

const CreateIntercompanyModal: Story = ({ type, storyState, mode }) => {
  const dropdownBody: Record<StatusType, ReactNode> = {
    forReview: (
      <>
        <DropDownButton size="204" icon={<EyeIcon />} onClick={() => {}}>
          Open journal entries
        </DropDownButton>
        <DropDownButton size="204" icon={<TickIcon />} onClick={() => {}}>
          Accept
        </DropDownButton>
        <Divider className={spacing.mY8} />
        <DropDownButton size="204" icon={<TrashIcon />} onClick={() => {}}>
          Delete
        </DropDownButton>
      </>
    ),
    accepted: (
      <>
        <DropDownButton size="204" icon={<EyeIcon />} onClick={() => {}}>
          Open journal entries
        </DropDownButton>
        <Divider className={spacing.mY8} />
        <DropDownButton size="204" icon={<TrashIcon />} onClick={() => {}}>
          Delete
        </DropDownButton>
      </>
    ),
  };

  const isLoading = storyState === 'loading';

  return (
    <Modal
      size={'1130'}
      height={modalHeight[mode as ModalMode]}
      header={
        isLoading ? (
          <ModalHeaderPlaceholder titleAlignment="center" />
        ) : (
          <ModalHeaderNew
            title={modalValuesBasedOnType[type as CreateType].title[mode as ModalMode]}
            titlePosition="center"
            border
            closeActionBackgroundColor={'grey-10'}
            dropdownBtn={
              <Container gap="24" alignitems="center">
                {tagBasedOnModalMode[mode as ModalMode]}
                {mode === 'view' && (
                  <DropDown
                    flexdirection="column"
                    padding="8"
                    control={({ handleOpen }) => (
                      <IconButton
                        icon={<DropdownIcon />}
                        onClick={handleOpen}
                        size="24"
                        background={'grey-10-grey-20'}
                        color="grey-100"
                      />
                    )}
                  >
                    {dropdownBody['accepted']}
                  </DropDown>
                )}
              </Container>
            }
            onClose={() => {}}
          />
        )
      }
      footer={mode === 'create' && <CreateIntercompanyModalFooter isLoading={isLoading} />}
    >
      <Container gap="24" alignitems="center" justifycontent="center" className={spacing.h100p}>
        <Container flexdirection="column" gap="16">
          <Container flexdirection="column" alignitems="center" gap="16">
            <ContentLoader isLoading={isLoading} height="16px" width="128px">
              <Text color="grey-100" type="body-medium">
                From
              </Text>
            </ContentLoader>
            <Container gap="24" alignitems="center">
              <TransactionInfoCard
                transaction={tableRowData[1]}
                type={type}
                isLoading={isLoading}
              />
              <ContentLoader isLoading={isLoading} height="24px" width="24px">
                <Icon icon={modalValuesBasedOnType[type as CreateType].icon} />
              </ContentLoader>
            </Container>
          </Container>
          <BankTransferInfo isLoading={isLoading} />
        </Container>
        <Container flexdirection="column" gap="16">
          <Container flexdirection="column" alignitems="center" gap="16">
            <ContentLoader isLoading={isLoading} height="16px" width="128px">
              <Text color="grey-100" type="body-medium">
                To
              </Text>
            </ContentLoader>
            <TransactionInfoCard transaction={tableRowData[0]} type={type} isLoading={isLoading} />
          </Container>
          <BankTransferInfo isLoading={isLoading} />
        </Container>
      </Container>
      {mode === 'view' && (
        <ActionsPopup
          type={popupActions['accepted']}
          itemNumber={1}
          itemsAmount={30}
          itemName="Transactions"
          itemId="23824384"
          onPreviousButtonClick={() => {}}
          onNextButtonClick={() => {}}
        />
      )}
    </Modal>
  );
};

export default {
  title: 'Pages/Banking/Intercompany/modals',
  component: CreateIntercompanyModal,
  argTypes: {
    storyState: {
      options: ['empty', 'loading', 'loaded'],
      control: {
        type: 'radio',
        labels: {
          empty: 'Empty page',
          loading: 'Page is loading',
          loaded: 'Page loaded',
        },
      },
    },
    type: {
      options: ['bankTransfer', 'foreignExchange'],
      control: {
        type: 'radio',
        labels: {
          bankTransfer: 'Bank Transfer',
          foreignExchange: 'Foreign Exchange',
        },
      },
    },
    mode: {
      options: ['create', 'view'],
      control: {
        type: 'radio',
        labels: {
          bankTransfer: 'Create',
          foreignExchange: 'View',
        },
      },
    },
  },
} as Meta;

export const CreateIntercompany = CreateIntercompanyModal.bind({});

CreateIntercompany.args = {
  storyState: 'loaded',
  type: 'bankTransfer',
  mode: 'create',
};
