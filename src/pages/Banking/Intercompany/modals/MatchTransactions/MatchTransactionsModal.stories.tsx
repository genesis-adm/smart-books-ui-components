import { Meta, Story } from '@storybook/react';
import { ContentLoader } from 'Components/base/ContentLoader';
import { Divider } from 'Components/base/Divider';
import { Container } from 'Components/base/grid/Container';
import ModalHeaderPlaceholder from 'Components/custom/placeholders/ModalHeaderPlaceholder';
import { FilterToggleButton } from 'Components/elements/filters/FilterToggleButton';
import { Search } from 'Components/inputs/Search';
import { Modal } from 'Components/modules/Modal';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import AddedTransactionView from 'Pages/Banking/Intercompany/modals/MatchTransactions/components/AddedTransactionView';
import MatchTransactionsFilters from 'Pages/Banking/Intercompany/modals/MatchTransactions/components/MatchTransactionsFilters';
import MatchTransactionsModalFooter from 'Pages/Banking/Intercompany/modals/MatchTransactions/components/MatchTransactionsModalFooter';
import MatchTransactionsTable from 'Pages/Banking/Intercompany/modals/MatchTransactions/components/MatchTransactionsTable';
import { TransactionData } from 'Pages/Banking/Intercompany/modals/MatchTransactions/constants';
import { AddedTransactionsContext } from 'Pages/Banking/Intercompany/modals/MatchTransactions/context/AddedTransactionsContext';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';

const MatchTransactionsModal: Story = ({ storyState }) => {
  const [showFilters, setShowFilters] = useState(false);

  const [addedTransactions, setAddedTransactions] = useState<TransactionData[]>([]);

  const addedOutflowTransaction = addedTransactions.find((row) => row.type === 'outflow');
  const addedInflowTransaction = addedTransactions.find((row) => row.type === 'inflow');

  const isLoading = storyState === 'loading';

  return (
    <Modal
      size="full"
      background={'grey-10'}
      pluginScrollDisabled
      header={
        isLoading ? (
          <ModalHeaderPlaceholder titleAlignment="center" />
        ) : (
          <ModalHeaderNew
            title="Match transactions"
            titlePosition="center"
            background={'grey-10'}
            closeActionBackgroundColor={'inherit'}
            onClose={() => {}}
          />
        )
      }
      footer={<MatchTransactionsModalFooter isLoading={isLoading} />}
    >
      <AddedTransactionsContext.Provider value={{ addedTransactions, setAddedTransactions }}>
        <Container flexdirection="column" gap="16" className={spacing.h100p}>
          <Container
            background={'white-100'}
            borderRadius="20"
            flexdirection="column"
            className={classNames(spacing.w100p, spacing.pY24, spacing.h220)}
            gap="24"
          >
            <AddedTransactionView
              transaction={addedOutflowTransaction}
              type={'outflow'}
              isLoading={isLoading}
            />
            <Divider fullHorizontalWidth />

            <AddedTransactionView
              transaction={addedInflowTransaction}
              type={'inflow'}
              isLoading={isLoading}
            />
          </Container>

          <Container
            background={'white-100'}
            borderRadius="20"
            flexdirection="column"
            customScroll
            className={classNames(spacing.w100p, spacing.p24)}
          >
            <Container gap="24" alignitems="center">
              <ContentLoader width="110px" height="40px" isLoading={isLoading}>
                <FilterToggleButton
                  showFilters={showFilters}
                  filtersSelected={false}
                  onClear={() => {}}
                  onClick={() => {
                    setShowFilters((prevState) => !prevState);
                  }}
                />
              </ContentLoader>

              <ContentLoader width="440px" height="40px" isLoading={isLoading}>
                <Search
                  width="440"
                  height="40"
                  name="search-match-transactions"
                  placeholder="Search by ID or description"
                  value=""
                  onChange={() => {}}
                  searchParams={[]}
                  onSearch={() => {}}
                  onClear={() => {}}
                />
              </ContentLoader>
            </Container>
            <MatchTransactionsFilters isActive={showFilters} isLoading={isLoading} />
            <MatchTransactionsTable isLoading={isLoading} />
          </Container>
        </Container>
      </AddedTransactionsContext.Provider>
    </Modal>
  );
};

export default {
  title: 'Pages/Banking/Intercompany/modals',
  component: MatchTransactionsModal,
  argTypes: {
    storyState: {
      options: ['empty', 'loading', 'loaded'],
      control: {
        type: 'radio',
        labels: {
          empty: 'Empty page',
          loading: 'Page is loading',
          loaded: 'Page loaded',
        },
      },
    },
  },
} as Meta;

export const MatchIntercompany = MatchTransactionsModal.bind({});
MatchIntercompany.args = {
  storyState: 'loaded',
};
