import { TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { TableCellWidth } from 'Components/types/gridTypes';
import { ReactComponent as ArrowDownIcon } from 'Svg/v2/16/arrow-down.svg';
import { ReactComponent as ArrowUpIcon } from 'Svg/v2/16/arrow-up.svg';
import { ReactNode } from 'react';
import React from 'react';

export type TransactionsType = 'inflow' | 'outflow';

export type TransactionData = {
  id: string;
  type: TransactionsType;
  legalEntity: string;
  businessUnit: string;
  description: string;
  amount: { mainAmount: string; secondaryAmount: string };
};

const sorting = {
  sortType: null,
  tooltipText: 'Sorting tooltip text',
};

export const tableHeader: {
  minWidth: TableCellWidth;
  title: string;
  align?: TableTitleNewProps['align'];
  sorting?: TableTitleNewProps['sorting'];
}[] = [
  { minWidth: '80', title: 'Status', align: 'center' },
  { minWidth: '130', title: 'ID / Date', align: 'center', sorting },
  { minWidth: '270', title: 'Bank account', sorting },
  { minWidth: '144', title: 'Legal entity', sorting },
  { minWidth: '180', title: 'Business division' },
  { minWidth: '300', title: 'Description' },
  { minWidth: '130', title: 'Amount', align: 'right', sorting },
  { minWidth: '115', title: 'Actions', align: 'center' },
];

export const tableRowData: TransactionData[] = [
  {
    id: '000001',
    type: 'inflow',
    legalEntity: 'Filty Group LLC',
    businessUnit: 'GENESIS',
    description:
      'ISSUE COY TT & COVER // TRANSFER OF OWN FUNDS djhfsj jsfhsdl fudsf hfihsdjhfsd jfhsfsdfsd',
    amount: { mainAmount: '+ 1,000,000.20 €', secondaryAmount: '+ 1,323,000.04 $' },
  },
  {
    id: '000002',
    type: 'outflow',
    description:
      'ISSUE COY TT & COVER // TRANSFER OF OWN FUNDS djhfsj jsfhsdl fudsf hfihsdjhfsd jfhsfsdfsd',
    legalEntity: 'Zirafit Limited',
    businessUnit: 'Genesis Media R&D',
    amount: { mainAmount: '- 1,000,000.20 €', secondaryAmount: '- 1,323,000.04 $' },
  },
  {
    id: '000007',
    type: 'outflow',
    description:
      'ISSUE COY TT & COVER // TRANSFER OF OWN FUNDS djhfsj jsfhsdl fudsf hfihsdjhfsd jfhsfsdfsd',
    legalEntity: 'Zirafit Limited',
    businessUnit: 'Genesis Media R&D',
    amount: { mainAmount: '- 1,000,000.20 €', secondaryAmount: '- 1,323,000.04 $' },
  },
  {
    id: '000008',
    type: 'outflow',
    description:
      'ISSUE COY TT & COVER // TRANSFER OF OWN FUNDS djhfsj jsfhsdl fudsf hfihsdjhfsd jfhsfsdfsd',
    legalEntity: 'Zirafit Limited',
    businessUnit: 'Genesis Media R&D',
    amount: { mainAmount: '- 1,000,000.20 €', secondaryAmount: '- 1,323,000.04 $' },
  },
  {
    id: '000003',
    type: 'inflow',
    description:
      'ISSUE COY TT & COVER // TRANSFER OF OWN FUNDS djhfsj jsfhsdl fudsf hfihsdjhfsd jfhsfsdfsd',
    legalEntity: 'Filty Group LLC',
    businessUnit: 'GENESIS',
    amount: { mainAmount: '+ 1,000,000.20 €', secondaryAmount: '+ 1,323,000.04 $' },
  },
  {
    id: '000004',
    type: 'inflow',
    description:
      'ISSUE COY TT & COVER // TRANSFER OF OWN FUNDS djhfsj jsfhsdl fudsf hfihsdjhfsd jfhsfsdfsd',
    legalEntity: 'Filty Group LLC',
    businessUnit: 'GENESIS',
    amount: { mainAmount: '+ 1,000,000.20 €', secondaryAmount: '+ 1,323,000.04 $' },
  },
  {
    id: '000005',
    type: 'inflow',
    description:
      'ISSUE COY TT & COVER // TRANSFER OF OWN FUNDS djhfsj jsfhsdl fudsf hfihsdjhfsd jfhsfsdfsd',
    legalEntity: 'Filty Group LLC',
    businessUnit: 'GENESIS',
    amount: { mainAmount: '+ 1,000,000.20 €', secondaryAmount: '+ 1,323,000.04 $' },
  },
  {
    id: '000006',
    type: 'inflow',
    description:
      'ISSUE COY TT & COVER // TRANSFER OF OWN FUNDS djhfsj jsfhsdl fudsf hfihsdjhfsd jfhsfsdfsd',
    legalEntity: 'Filty Group LLC',
    businessUnit: 'GENESIS',
    amount: { mainAmount: '+ 1,000,000.20 €', secondaryAmount: '+ 1,323,000.04 $' },
  },
];

export const iconsBasedOnType: Record<
  TransactionsType,
  { icon: ReactNode; color: 'red' | 'green' }
> = {
  inflow: { icon: <ArrowUpIcon />, color: 'green' },
  outflow: {
    icon: <ArrowDownIcon />,
    color: 'red',
  },
};
