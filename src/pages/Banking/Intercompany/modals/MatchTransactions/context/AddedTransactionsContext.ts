import { TransactionData } from 'Pages/Banking/Intercompany/modals/MatchTransactions/constants';
import { Dispatch, SetStateAction, createContext } from 'react';

export const AddedTransactionsContext = createContext<{
  addedTransactions: TransactionData[];
  setAddedTransactions: Dispatch<SetStateAction<TransactionData[]>>;
}>({ addedTransactions: [], setAddedTransactions: () => {} });
