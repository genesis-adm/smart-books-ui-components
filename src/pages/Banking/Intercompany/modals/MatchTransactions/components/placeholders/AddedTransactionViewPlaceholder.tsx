import { ContentLoader } from 'Components/base/ContentLoader';
import { Container } from 'Components/base/grid/Container';
import React, { ReactElement } from 'react';

const AddedTransactionViewPlaceholder = (): ReactElement => (
  <Container flexdirection="column" gap="8" alignitems="center" justifycontent="center">
    <ContentLoader width="24px" height="24px" isLoading />
    <ContentLoader width="183px" height="24px" isLoading />
  </Container>
);

export default AddedTransactionViewPlaceholder;
