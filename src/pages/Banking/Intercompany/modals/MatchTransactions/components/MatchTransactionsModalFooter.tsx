import { ContentLoader } from 'Components/base/ContentLoader';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { Pagination } from 'Components/custom/Stories/Pagination/Pagination';
import PerPageSelect from 'Components/custom/Stories/Pagination/PerPageSelect/PerPageSelect';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { rowsPerPageOptions } from 'Mocks/fakeOptions';
import React, { ReactElement, useState } from 'react';

import spacing from '../../../../../../assets/styles/spacing.module.scss';

type MatchTransactionsModalFooterProps = {
  isLoading: boolean;
};
const MatchTransactionsModalFooter = ({
  isLoading,
}: MatchTransactionsModalFooterProps): ReactElement => {
  const [rowsPerPage, setRowsPerPage] = useState<string>(rowsPerPageOptions[0].value);

  return (
    <ModalFooterNew background={'grey-10'} gap={'16'}>
      <ContentLoader isLoading={isLoading} width="527px">
        <Container alignitems="center" gap="16" flexgrow="1" justifycontent="flex-end">
          <Container flexdirection="column" className={spacing.w150fixed}>
            <Text type="body-regular-14" color="grey-100">
              Step: 1 of 2
            </Text>
            <Text type="body-regular-14" color="grey-100">
              Next: Confirm match
            </Text>
          </Container>
          <Button height="48" navigation width="150" onClick={() => {}} background={'grey-90'}>
            Next
          </Button>
        </Container>
        <Pagination
          borderTop="none"
          padding="0"
          customPagination={
            <Container gap="16" alignitems="center">
              <Text type="subtext-regular" color="grey-100">
                Rows per Page
              </Text>
              <PerPageSelect
                selectedValue={rowsPerPage}
                onOptionClick={(value) => setRowsPerPage(value)}
              />
            </Container>
          }
        />
      </ContentLoader>
    </ModalFooterNew>
  );
};

export default MatchTransactionsModalFooter;
