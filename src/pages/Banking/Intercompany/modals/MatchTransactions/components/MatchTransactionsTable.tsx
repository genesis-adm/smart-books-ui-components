import TablePlaceholder from 'Components/custom/placeholders/TablePlaceholder';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import type { TableCellWidth } from 'Components/types/gridTypes';
import TransactionTableRow from 'Pages/Banking/Intercompany/modals/MatchTransactions/components/TransactionTableRow';
import {
  tableHeader,
  tableRowData,
} from 'Pages/Banking/Intercompany/modals/MatchTransactions/constants';
import classNames from 'classnames';
import React, { ReactElement } from 'react';

import spacing from '../../../../../../assets/styles/spacing.module.scss';

type MatchTransactionsTableProps = { isLoading: boolean };

const MatchTransactionsTable = ({ isLoading }: MatchTransactionsTableProps): ReactElement => {
  if (isLoading) return <TablePlaceholder paddingX="none" />;

  return (
    <TableNew
      noResults={false}
      className={classNames(spacing.mt24, spacing.pr24)}
      tableHead={
        <TableHeadNew>
          {tableHeader.map((item, index) => (
            <TableTitleNew
              key={index}
              minWidth={item.minWidth as TableCellWidth}
              align={item.align as TableTitleNewProps['align']}
              title={item.title}
              sorting={item.sorting}
              onClick={() => {}}
            />
          ))}
        </TableHeadNew>
      }
    >
      <TableBodyNew>
        {tableRowData.map((item) => (
          <TransactionTableRow key={item.id} type="tableRow" transaction={item} />
        ))}
      </TableBodyNew>
    </TableNew>
  );
};

export default MatchTransactionsTable;
