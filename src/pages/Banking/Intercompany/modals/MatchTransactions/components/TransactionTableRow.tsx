import { Tag } from 'Components/base/Tag';
import { TagSegmented } from 'Components/base/TagSegmented';
import { Button } from 'Components/base/buttons/Button';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import {
  TransactionData,
  iconsBasedOnType,
  tableHeader,
} from 'Pages/Banking/Intercompany/modals/MatchTransactions/constants';
import { AddedTransactionsContext } from 'Pages/Banking/Intercompany/modals/MatchTransactions/context/AddedTransactionsContext';
import { bankAccounts } from 'Pages/Banking/Transactions/constants';
import { ReactComponent as CreditCardIcon } from 'Svg/v2/16/credit-card.svg';
import { ReactComponent as ForReviewIcon } from 'Svg/v2/16/document-search.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import React, { ReactElement, ReactNode, useContext } from 'react';

type TransactionTableRowType = 'addedRow' | 'tableRow';

type TransactionTableRowProps = {
  transaction: TransactionData;
  type: TransactionTableRowType;
  isError?: boolean;
};

const TransactionTableRow = ({
  type,
  transaction,
  isError,
}: TransactionTableRowProps): ReactElement => {
  const { addedTransactions, setAddedTransactions } = useContext(AddedTransactionsContext);

  const handleDelete = () => {
    setAddedTransactions(addedTransactions.filter((item) => item.id !== transaction.id));
  };

  const handleAddClick = (item: TransactionData) => {
    setAddedTransactions([...addedTransactions, item]);
  };

  const isAddedOutflowTransaction = !!addedTransactions.find(
    (transactionItem) => transactionItem.type === 'outflow',
  );

  const isAddedInflowTransaction = !!addedTransactions.find(
    (transactionItem) => transactionItem.type === 'inflow',
  );

  const isInflowTransaction = transaction.type === 'inflow';
  const isOutflowTransaction = transaction.type === 'outflow';

  const isAddButtonDisabled =
    (isOutflowTransaction && isAddedOutflowTransaction) ||
    (isInflowTransaction && isAddedInflowTransaction);

  const errorMessage =
    'The selected bank transactions do not align with the criteria for "Bank transfer" or "FX operations." Please review and select other transactions that meet these definitions';

  const actionsBasedOnType: Record<TransactionTableRowType, ReactNode> = {
    addedRow: (
      <Container gap="8">
        <Tag text="Added" padding="0-4" />
        <IconButton
          icon={<TrashIcon />}
          size="24"
          color="grey-90-red-90"
          background={'grey-20-red-20'}
          tooltip="Delete"
          onClick={handleDelete}
        />
      </Container>
    ),
    tableRow: (
      <Button
        width="50"
        height="24"
        font={'caption-regular'}
        background={'blue-10-blue-20'}
        color={'violet-90-violet-100'}
        disabled={isAddButtonDisabled}
        onClick={() => handleAddClick(transaction)}
      >
        Add
      </Button>
    ),
  };

  return (
    <TableRowNew key={transaction.id} gap="8" isError={isError} errorMessage={errorMessage}>
      <TableCellNew minWidth={tableHeader[0].minWidth} justifycontent="center" fixedWidth>
        <IconButton size="24" color="grey-90" icon={<ForReviewIcon />} />
      </TableCellNew>
      <TableCellTextNew
        minWidth={tableHeader[1].minWidth}
        fixedWidth
        noWrap
        lineClamp="none"
        value={`ID ${transaction.id}`}
        secondaryValue="08 September 2022"
        type="caption-semibold"
        secondaryType="text-regular"
        tagColor={iconsBasedOnType[transaction.type].color}
        tagIcon={iconsBasedOnType[transaction.type].icon}
      />
      <TableCellNew minWidth={tableHeader[2].minWidth} fixedWidth>
        <TagSegmented
          {...bankAccounts}
          icon={<CreditCardIcon />}
          tooltipMessage={
            <Container flexdirection="column">
              <Text color="white-100" style={{ wordBreak: 'break-all' }}>
                {bankAccounts.mainLabel}
              </Text>
              <Text color="white-100" style={{ wordBreak: 'break-all' }}>
                {bankAccounts.secondLabel} | {bankAccounts.thirdLabel}
              </Text>
            </Container>
          }
        />
      </TableCellNew>
      <TableCellTextNew
        minWidth={tableHeader[3].minWidth}
        fixedWidth
        value={transaction.legalEntity}
      />
      <TableCellTextNew
        minWidth={tableHeader[4].minWidth}
        fixedWidth
        value={transaction.businessUnit}
      />
      <TableCellTextNew
        minWidth={tableHeader[5].minWidth}
        fixedWidth
        value={transaction.description}
      />
      <TableCellTextNew
        minWidth={tableHeader[6].minWidth}
        fixedWidth
        type="caption-semibold"
        align="right"
        color={transaction.type === 'inflow' ? 'green-90' : 'red-90'}
        value={transaction.amount.mainAmount}
        secondaryValue={transaction.amount.secondaryAmount}
        secondaryColor="grey-90"
        secondaryAlign="right"
      />
      <TableCellNew minWidth={tableHeader[7].minWidth} justifycontent="center" fixedWidth>
        {actionsBasedOnType[type]}
      </TableCellNew>
    </TableRowNew>
  );
};

export default TransactionTableRow;
