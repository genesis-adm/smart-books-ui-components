import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import TransactionTableRow from 'Pages/Banking/Intercompany/modals/MatchTransactions/components/TransactionTableRow';
import AddedTransactionViewPlaceholder from 'Pages/Banking/Intercompany/modals/MatchTransactions/components/placeholders/AddedTransactionViewPlaceholder';
import {
  TransactionData,
  TransactionsType,
} from 'Pages/Banking/Intercompany/modals/MatchTransactions/constants';
import { ReactComponent as ArrowDownIcon } from 'Svg/v2/16/arrow-down.svg';
import { ReactComponent as ArrowUpIcon } from 'Svg/v2/16/arrow-up.svg';
import classNames from 'classnames';
import React, { ReactElement, ReactNode } from 'react';

import spacing from '../../../../../../assets/styles/spacing.module.scss';

type AddedTransactionViewProps = {
  transaction?: TransactionData;
  type: TransactionsType;
  isLoading: boolean;
};

const AddedTransactionView = ({
  transaction,
  type,
  isLoading,
}: AddedTransactionViewProps): ReactElement => {
  const valuesBasedOnType: Record<
    TransactionsType,
    {
      text: string;
      icon: ReactNode;
      background: 'red-10' | 'green-10';
      color: 'red-90' | 'green-90';
    }
  > = {
    inflow: {
      text: 'Please add inflow transaction',
      icon: <ArrowUpIcon />,
      background: 'green-10',
      color: 'green-90',
    },
    outflow: {
      text: 'Please add outflow transaction',
      icon: <ArrowDownIcon />,
      background: 'red-10',
      color: 'red-90',
    },
  };

  if (isLoading && !transaction) return <AddedTransactionViewPlaceholder />;

  return (
    <Container flexdirection="column" gap="4" className={spacing.pX24}>
      {transaction ? (
        <TableNew className={classNames(spacing.pr24)}>
          <TableBodyNew>
            <TransactionTableRow
              transaction={transaction}
              isError={type === 'outflow'}
              type="addedRow"
            />
          </TableBodyNew>
        </TableNew>
      ) : (
        <Container flexdirection="column" justifycontent="center" alignitems="center" gap="4">
          <IconButton
            icon={valuesBasedOnType[type].icon}
            size="24"
            background={valuesBasedOnType[type].background}
            color={valuesBasedOnType[type].color}
          />
          <Text type="text-regular" color="grey-100">
            {valuesBasedOnType[type].text}
          </Text>
        </Container>
      )}
    </Container>
  );
};

export default AddedTransactionView;
