import { Tab } from 'Components/custom/Stories/MenuTabs/MenuTabs';
import { TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { TableCellWidth } from 'Components/types/gridTypes';
import { TransactionsType } from 'Pages/Banking/Intercompany/modals/MatchTransactions/constants';
import { ReactComponent as ForReviewIcon } from 'Svg/v2/16/document-search.svg';
import { ReactComponent as AllStatusIcon } from 'Svg/v2/16/lines.svg';
import { ReactComponent as AcceptedIcon } from 'Svg/v2/16/tick-in-filled-circle.svg';
import { ReactNode } from 'react';
import React from 'react';

export type CustomColumnType = {
  id: string;
  title: string;
};

export const sorting = {
  sortType: null,
  tooltipText: 'Sorting tooltip text',
};

export type StatusType = 'forReview' | 'accepted';
export type CategoryType = 'bankTransfer' | 'foreignExchange';

export type TransactionData = {
  id: string;
  type: TransactionsType;
  legalEntity: string;
  businessUnit: string;

  amount: { mainAmount: string; secondaryAmount: string };
};

export type TransactionItem = {
  status: StatusType;
  category: CategoryType;
  bankFee?: { mainAmount: string; secondaryAmount: string };
  fxResult?: string;
  outflow: TransactionData;
  inflow: TransactionData;
};

export const tableRowData: TransactionItem[] = [
  {
    status: 'forReview',
    category: 'bankTransfer',
    bankFee: { mainAmount: '100.20 €', secondaryAmount: '104.04 $' },
    outflow: {
      id: '000001',
      type: 'outflow',
      legalEntity: 'Zirafit Limited',
      businessUnit: 'Genesis Media R&D',
      amount: { mainAmount: '- 1,000,000.20 €', secondaryAmount: '- 1,323,000.04 $' },
    },
    inflow: {
      id: '000001',
      type: 'inflow',
      legalEntity: 'Filty Group LLC',
      businessUnit: 'GENESIS',
      amount: { mainAmount: '+ 1,000,000.20 €', secondaryAmount: '+ 1,323,000.04 $' },
    },
  },
  {
    status: 'accepted',
    category: 'foreignExchange',
    fxResult: '- 129.20 €',
    outflow: {
      id: '000002',
      type: 'outflow',
      legalEntity: 'Zirafit Limited',
      businessUnit: 'Genesis Media R&D',
      amount: { mainAmount: '- 1,000,000.20 €', secondaryAmount: '- 1,323,000.04 $' },
    },
    inflow: {
      id: '000002',
      type: 'inflow',
      legalEntity: 'Filty Group LLC',
      businessUnit: 'GENESIS',
      amount: { mainAmount: '+ 1,000,000.20 €', secondaryAmount: '+ 1,323,000.04 $' },
    },
  },
  {
    status: 'accepted',
    category: 'foreignExchange',
    fxResult: '- 129.20 €',
    outflow: {
      id: '000007',
      type: 'outflow',
      legalEntity: 'Zirafit Limited',
      businessUnit: 'Genesis Media R&D',

      amount: { mainAmount: '- 1,000,000.20 €', secondaryAmount: '- 1,323,000.04 $' },
    },
    inflow: {
      id: '000007',
      type: 'inflow',
      legalEntity: 'Filty Group LLC',
      businessUnit: 'GENESIS',
      amount: { mainAmount: '+ 1,000,000.20 €', secondaryAmount: '+ 1,323,000.04 $' },
    },
  },
  {
    status: 'forReview',
    category: 'bankTransfer',
    bankFee: { mainAmount: '100.20 €', secondaryAmount: '104.04 $' },

    outflow: {
      id: '000008',
      type: 'outflow',
      legalEntity: 'Zirafit Limited',
      businessUnit: 'Genesis Media R&D',
      amount: { mainAmount: '- 1,000,000.20 €', secondaryAmount: '- 1,323,000.04 $' },
    },
    inflow: {
      id: '000008',
      type: 'inflow',
      legalEntity: 'Filty Group LLC',
      businessUnit: 'GENESIS',
      amount: { mainAmount: '+ 1,000,000.20 €', secondaryAmount: '+ 1,323,000.04 $' },
    },
  },
];

export const tabs: { id: string; title: string; icon: ReactNode }[] = [
  { id: 'all', title: 'All', icon: <AllStatusIcon /> },
  { id: 'forReview', title: 'For review', icon: <ForReviewIcon /> },
  { id: 'accepted', title: 'Accepted', icon: <AcceptedIcon /> },
];

export type TableHeadType = {
  minWidth: TableCellWidth;
  title: string;
  align?: TableTitleNewProps['align'];
  sorting?: TableTitleNewProps['sorting'];
  active?: boolean;
};

export const tableHead: TableHeadType[] = [
  { minWidth: '80', title: 'Status', align: 'center', active: true },
  { minWidth: '170', title: 'Category', align: 'center', active: true },
  { minWidth: '130', title: 'ID / Date', active: true, sorting },
  { minWidth: '260', title: 'Bank account', active: true, sorting },
  { minWidth: '130', title: 'Legal entity', active: true, sorting },
  { minWidth: '145', title: 'Business division', active: true },
  { minWidth: '190', title: 'Description', active: true },
  { minWidth: '150', title: 'Amount', align: 'right', active: true, sorting },
  { minWidth: '160', title: 'Bank fee after amount', align: 'right', active: false },
  { minWidth: '160', title: 'Fx result gain / loss', align: 'right', active: false },
  { minWidth: '110', title: 'Action', align: 'center', active: true },
];

export const customColumns: CustomColumnType[] = [
  { id: 'bankFee', title: 'Bank fee after amount' },
  { id: 'fxResult', title: 'Fx result gain / loss' },
];

export const menuTabs: Tab[] = [
  { id: 'bankAccount', title: 'Bank account' },
  { id: 'transactions', title: 'Transactions' },
  { id: 'rules', title: 'Rules' },
  { id: 'intercompany', title: 'Intercompany' },
];
