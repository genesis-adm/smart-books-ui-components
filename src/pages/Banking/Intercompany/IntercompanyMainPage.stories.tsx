import { Meta, Story } from '@storybook/react';
import { ContentLoader } from 'Components/base/ContentLoader';
import { Divider } from 'Components/base/Divider';
import { LoadingLine } from 'Components/base/LoadingLine';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { ContainerMain } from 'Components/base/grid/ContainerMain';
import { MainPageContentContainer } from 'Components/base/grid/MainPageContentContainer';
import { GeneralTab, GeneralTabWrapper } from 'Components/base/tabs/GeneralTab';
import { Text } from 'Components/base/typography/Text';
import { MainPageEmpty } from 'Components/custom/MainPage/MainPageEmpty';
import { HeaderSB } from 'Components/custom/Stories/HeaderSB';
import MenuTabs from 'Components/custom/Stories/MenuTabs/MenuTabs';
import { NavigationSB } from 'Components/custom/Stories/NavigationSB';
import { Pagination } from 'Components/custom/Stories/Pagination/Pagination';
import PerPageSelect from 'Components/custom/Stories/Pagination/PerPageSelect/PerPageSelect';
import { ExportDropdown } from 'Components/elements/ExportDropdown';
import { FilterToggleButton } from 'Components/elements/filters/FilterToggleButton';
import { Search } from 'Components/inputs/Search';
import { rowsPerPageOptions } from 'Mocks/fakeOptions';
import IntercompanyCustomColumns from 'Pages/Banking/Intercompany/components/IntercompanyCustomColumns';
import IntercompanyFilters from 'Pages/Banking/Intercompany/components/IntercompanyFilters';
import IntercompanyTable from 'Pages/Banking/Intercompany/components/IntercompanyTable';
import { menuTabs, tableHead, tabs } from 'Pages/Banking/Intercompany/constants';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

export type TabType = 'all' | 'forReview' | 'accepted';

const IntercompanyMainPage: Story = ({ storyState }) => {
  const [toggle, toggleMenu] = useState(false);
  const [showFilters, setShowFilters] = useState(false);
  const [activeTab, setActiveTab] = useState<TabType>('all');

  const [rowsPerPage, setRowsPerPage] = useState<string>(rowsPerPageOptions[0].value);

  const [tableHeadArr, setTableHeadArr] = useState(tableHead);

  const isLoading = storyState === 'loading';

  return (
    <Container fullscreen flexwrap="nowrap" background={'grey-10'}>
      <NavigationSB opened={toggle} onToggleOpen={() => toggleMenu(!toggle)} />
      <ContainerMain navOpened={toggle}>
        <HeaderSB />
        <MenuTabs title="Banking" tabs={menuTabs} currentTabId="intercompany" />
        <MainPageContentContainer>
          {storyState !== 'empty' && (
            <>
              <Container
                className={classNames(spacing.mX24, spacing.mt24)}
                justifycontent="space-between"
                gap="24"
              >
                <Container gap="24" alignitems="center">
                  <ContentLoader width="110px" height="40px" isLoading={isLoading}>
                    <FilterToggleButton
                      showFilters={showFilters}
                      filtersSelected={false}
                      onClear={() => {}}
                      onClick={() => {
                        setShowFilters((prevState) => !prevState);
                      }}
                    />
                  </ContentLoader>

                  <ContentLoader width="440px" height="40px" isLoading={isLoading}>
                    <Search
                      width="440"
                      height="40"
                      name="search-banking-intercompany-mainPage"
                      placeholder="Search by ID and description"
                      value=""
                      onChange={() => {}}
                      searchParams={[]}
                      onSearch={() => {}}
                      onClear={() => {}}
                    />
                  </ContentLoader>
                </Container>
                <Container gap="24" alignitems="center">
                  <ContentLoader width="40px" height="40px" isLoading={isLoading}>
                    <IntercompanyCustomColumns setTableHeadArr={setTableHeadArr} />
                  </ContentLoader>

                  <Divider type="vertical" background={'grey-90'} height="20" />
                  <ContentLoader width="40px" height="40px" isLoading={isLoading}>
                    <ExportDropdown onExportToXLS={() => {}} onExportToCSV={() => {}} />
                  </ContentLoader>
                  <ContentLoader width="140px" height="40px" isLoading={isLoading}>
                    <Button
                      background={'violet-90-violet-100'}
                      color="white-100"
                      width="140"
                      height="40"
                      iconLeft={<PlusIcon />}
                    >
                      Create
                    </Button>
                  </ContentLoader>
                </Container>
              </Container>
              <IntercompanyFilters isActive={showFilters} isLoading={isLoading} />
              <GeneralTabWrapper className={classNames(spacing.mX24, spacing.mt24)}>
                <ContentLoader isLoading={isLoading} width="527px" height="33px">
                  {tabs.map((tab) => (
                    <GeneralTab
                      id={tab.id}
                      icon={tab.icon}
                      onClick={() => {
                        setActiveTab(tab.id as TabType);
                      }}
                      active={activeTab === tab.id}
                    >
                      {tab.title}
                    </GeneralTab>
                  ))}
                </ContentLoader>
              </GeneralTabWrapper>

              <IntercompanyTable
                activeTab={activeTab}
                tableHead={tableHeadArr}
                isLoading={isLoading}
              />

              <Pagination
                isLoading={isLoading}
                customPagination={
                  <Container gap="16" alignitems="center">
                    <Text type="subtext-regular" color="grey-100">
                      Rows per Page
                    </Text>
                    <PerPageSelect
                      selectedValue={rowsPerPage}
                      onOptionClick={(value) => setRowsPerPage(value)}
                    />
                  </Container>
                }
              />
            </>
          )}
          {storyState === 'empty' && (
            <MainPageEmpty
              title="Create new Intercompany"
              description="You have no Match Intercompany transactions created. Your Match Intercompany transactions will be displayed here."
              action={
                <Button
                  width="230"
                  height="40"
                  background={'blue-10-blue-20'}
                  color="violet-90-violet-100"
                  onClick={() => {}}
                >
                  Create new Intercompany
                </Button>
              }
            />
          )}
        </MainPageContentContainer>
        <LoadingLine loadingCompleted={storyState !== 'loading'} />
      </ContainerMain>
    </Container>
  );
};

export default {
  title: 'Pages/Banking/Intercompany',
  component: IntercompanyMainPage,
  parameters: {
    layout: 'fullscreen',
  },
  argTypes: {
    storyState: {
      options: ['empty', 'loading', 'loaded'],
      control: {
        type: 'radio',
        labels: {
          empty: 'Empty page',
          loading: 'Page is loading',
          loaded: 'Page loaded',
        },
      },
    },
  },
} as Meta;

export const IntercompanyMainPageStory = IntercompanyMainPage.bind({});
IntercompanyMainPageStory.args = {
  storyState: 'loaded',
};
