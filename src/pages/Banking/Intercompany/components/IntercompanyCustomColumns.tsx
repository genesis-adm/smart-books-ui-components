import { DropDown } from 'Components/DropDown';
import { IconButton } from 'Components/base/buttons/IconButton';
import IntercompanyCustomColumnsDropdownBody from 'Pages/Banking/Intercompany/components/IntercompanyCustomColumnsDropdownBody';
import { CustomColumnType, TableHeadType } from 'Pages/Banking/Intercompany/constants';
import { ReactComponent as SettingsIcon } from 'Svg/v2/16/settings.svg';
import React, { ReactElement, useState } from 'react';

type IntercompanyCustomColumnsProps = { setTableHeadArr(items: TableHeadType[]): void };

const IntercompanyCustomColumns = ({
  setTableHeadArr,
}: IntercompanyCustomColumnsProps): ReactElement => {
  const [checkedCustomColumns, setCheckedCustomColumns] = useState<CustomColumnType[]>([]);

  return (
    <DropDown
      padding="none"
      control={({ handleOpen }) => (
        <IconButton
          size="40"
          background={'grey-10-grey-20'}
          color="grey-100"
          icon={<SettingsIcon />}
          onClick={handleOpen}
        />
      )}
    >
      <IntercompanyCustomColumnsDropdownBody
        setTableHeadArr={setTableHeadArr}
        setCheckedCustomColumns={setCheckedCustomColumns}
        checkedCustomColumns={checkedCustomColumns}
      />
    </DropDown>
  );
};

export default IntercompanyCustomColumns;
