import { ContentLoader } from 'Components/base/ContentLoader';
import { Divider } from 'Components/base/Divider';
import { Radio } from 'Components/base/Radio';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { FilterDropDown } from 'Components/elements/filters/FilterDropDown';
import { FilterDropDownItem } from 'Components/elements/filters/FilterDropDownItem';
import { FilterItemsContainer } from 'Components/elements/filters/FilterItemsContainer';
import { FilterSearch } from 'Components/elements/filters/FilterSearch';
import { FiltersWrapper } from 'Components/elements/filters/FiltersWrapper';
import { Input } from 'Components/old/Input';
import { bankAccountMultiSelectValues, currencyOptions, options } from 'Mocks/fakeOptions';
import { priceFilterItem } from 'Mocks/purchasesFakeData';
import handleAccountLength from 'Utils/handleAccountLength';
import classNames from 'classnames';
import React, { ReactElement, ReactNode, useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

type Filters = 'bankAccount' | 'currency' | 'businessUnit' | 'legalEntity' | 'amount';
type IntercompanyFiltersProps = { isActive: boolean; isLoading: boolean };

const IntercompanyFilters = ({ isActive, isLoading }: IntercompanyFiltersProps): ReactElement => {
  const [filterSearchValue, setFilterSearchValue] = useState<string>();

  const handleChange = (value: string) => {
    setFilterSearchValue(value);
  };

  const [inputValue, setInputValue] = useState<string>();

  const filtersValue: Record<Filters, { title: string; component: ReactNode }> = {
    bankAccount: {
      title: 'Bank account',
      component: (
        <FilterItemsContainer className={spacing.mt8}>
          {bankAccountMultiSelectValues.map((item) => (
            <FilterDropDownItem
              key={item.value}
              id={item.value}
              type="item"
              checked={false}
              blurred={false}
              value={
                <Container
                  justifycontent="space-between"
                  alignitems="center"
                  className={spacing.w246fixed}
                >
                  <Container justifycontent="center" flexdirection="column" flexgrow="1">
                    <Text type="caption-semibold">{item.bank}</Text>
                    <Text color="grey-100">
                      {handleAccountLength(item.accountNumber)} | {item.currency}
                    </Text>
                  </Container>
                  <Container>
                    <Text type="caption-regular" color="grey-100">
                      {item.type}
                    </Text>
                  </Container>
                </Container>
              }
              onChange={() => {}}
            />
          ))}
        </FilterItemsContainer>
      ),
    },
    currency: {
      title: 'Currency',
      component: (
        <FilterItemsContainer className={spacing.mt8}>
          {currencyOptions.map((item) => (
            <FilterDropDownItem
              key={item.value}
              id={item.value}
              type="item"
              checked={false}
              blurred={false}
              value={`${item.label} - ${item.fullLabel}`}
              onChange={() => {}}
            />
          ))}
        </FilterItemsContainer>
      ),
    },
    businessUnit: {
      title: 'Business unit',
      component: (
        <FilterItemsContainer className={spacing.mt8}>
          {options.map((item) => (
            <FilterDropDownItem
              key={item.value}
              id={item.value}
              type="item"
              checked={false}
              blurred={false}
              value={item.label}
              onChange={() => {}}
            />
          ))}
        </FilterItemsContainer>
      ),
    },
    legalEntity: {
      title: 'Legal Entity',
      component: (
        <FilterItemsContainer className={spacing.mt8}>
          {options.map((item) => (
            <FilterDropDownItem
              key={item.value}
              id={item.value}
              type="item"
              checked={false}
              blurred={false}
              value={item.label}
              onChange={() => {}}
            />
          ))}
        </FilterItemsContainer>
      ),
    },
    amount: {
      title: 'Amount',
      component: (
        <FilterItemsContainer maxHeight="344" gap="4" className={spacing.mt8}>
          <Text type="body-regular-14" color="grey-100">
            Please choose filter type
          </Text>
          <Container flexdirection="column">
            <Container gap="8" flexwrap="wrap" className={classNames(spacing.mb16)}>
              {priceFilterItem.map(({ title, checked }) => (
                <Radio
                  key={title}
                  id={title}
                  name={title}
                  checked={checked}
                  className={classNames(spacing.mt20, spacing.w120fixed)}
                  onChange={() => {}}
                >
                  <Text type="caption-regular" color="grey-100">
                    {title}
                  </Text>
                </Radio>
              ))}
            </Container>
            <Input
              className={spacing.mt20}
              type="number"
              name="amountFrom"
              step={0.01}
              value={inputValue}
              onChange={(e) => setInputValue(e.target.value)}
              placeholder="0.00"
            />
          </Container>
        </FilterItemsContainer>
      ),
    },
  };

  const filters = Object.keys(filtersValue) as Filters[];

  return (
    <FiltersWrapper active={isActive}>
      <ContentLoader isLoading={isLoading} height="32px" width="915px">
        {filters.map((item) => (
          <FilterDropDown
            key={item}
            title={filtersValue[item].title}
            value="All"
            onClear={() => {}}
            selectedAmount={0}
          >
            <Container
              className={classNames(spacing.m16, spacing.w288fixed)}
              flexdirection="column"
            >
              <FilterSearch
                title={filtersValue[item].title + ':'}
                value={filterSearchValue || ''}
                onChange={handleChange}
              />
              {filtersValue[item].component}
            </Container>
            <Divider fullHorizontalWidth />
            <Container
              className={classNames(spacing.m16, spacing.w288fixed)}
              justifycontent="space-between"
              alignitems="center"
              flexdirection="row-reverse"
            >
              <Button
                width="auto"
                height="32"
                padding="8"
                font="text-medium"
                onClick={() => {}}
                background={'violet-90-violet-100'}
                color="white-100"
              >
                Apply
              </Button>
              <Button
                width="auto"
                height="32"
                padding="8"
                font="text-medium"
                onClick={() => {}}
                background={'blue-10-red-10'}
                color="violet-90-red-90"
              >
                Clear all
              </Button>
            </Container>
          </FilterDropDown>
        ))}
      </ContentLoader>
    </FiltersWrapper>
  );
};

export default IntercompanyFilters;
