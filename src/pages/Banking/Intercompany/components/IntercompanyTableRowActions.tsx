import { DropDown } from 'Components/DropDown';
import { Divider } from 'Components/base/Divider';
import { ButtonWithDropdown } from 'Components/base/buttons/ButtonWithDropdown';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { TabType } from 'Pages/Banking/Intercompany/IntercompanyMainPage.stories';
import { ReactComponent as EyeIcon } from 'Svg/v2/16/eye.svg';
import { ReactComponent as DropdownIcon } from 'Svg/v2/16/three-dots.svg';
import { ReactComponent as TickIcon } from 'Svg/v2/16/tick-in-filled-circle.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import React, { ReactElement, ReactNode } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

type IntercompanyTableRowActionsProps = { currentTab: TabType };

const IntercompanyTableRowActions = ({
  currentTab,
}: IntercompanyTableRowActionsProps): ReactElement => {
  const actions: Record<TabType, ReactNode> = {
    all: (
      <DropDown
        flexdirection="column"
        padding="8"
        control={({ handleOpen }) => (
          <IconButton
            icon={<DropdownIcon />}
            onClick={handleOpen}
            background={'transparent'}
            color="grey-100-violet-90"
          />
        )}
      >
        <DropDownButton size="204" icon={<EyeIcon />} onClick={() => {}}>
          Open
        </DropDownButton>
        <DropDownButton size="204" icon={<EyeIcon />} onClick={() => {}}>
          Open journal entries
        </DropDownButton>
        <DropDownButton size="204" icon={<TickIcon />} onClick={() => {}}>
          Accept
        </DropDownButton>
        <Divider className={spacing.mY8} />
        <DropDownButton size="204" icon={<TrashIcon />} onClick={() => {}}>
          Delete
        </DropDownButton>
      </DropDown>
    ),
    forReview: (
      <ButtonWithDropdown
        mainActionButtonWidth="50"
        height="24"
        font="caption-semibold"
        background={'blue-10-blue-20'}
        border="blue-10-blue-20"
        color="violet-90-violet-100"
        dropdownBorder="white-100"
        dropdown={
          <>
            <DropDownButton size="204" icon={<EyeIcon />} onClick={() => {}}>
              Open
            </DropDownButton>
            <Divider className={spacing.mY8} />
            <DropDownButton size="204" icon={<TrashIcon />} onClick={() => {}}>
              Delete
            </DropDownButton>
          </>
        }
      >
        Accept
      </ButtonWithDropdown>
    ),
    accepted: (
      <DropDown
        flexdirection="column"
        padding="8"
        control={({ handleOpen }) => (
          <IconButton
            icon={<DropdownIcon />}
            onClick={handleOpen}
            background={'transparent'}
            color="grey-100-violet-90"
          />
        )}
      >
        <DropDownButton size="204" icon={<EyeIcon />} onClick={() => {}}>
          Open
        </DropDownButton>
        <DropDownButton size="204" icon={<EyeIcon />} onClick={() => {}}>
          Open journal entries
        </DropDownButton>
        <Divider className={spacing.mY8} />
        <DropDownButton size="204" icon={<TrashIcon />} onClick={() => {}}>
          Delete
        </DropDownButton>
      </DropDown>
    ),
  };

  return <>{actions[currentTab]}</>;
};

export default IntercompanyTableRowActions;
