import { Tag } from 'Components/base/Tag';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TabType } from 'Pages/Banking/Intercompany/IntercompanyMainPage.stories';
import IntercompanySubRow from 'Pages/Banking/Intercompany/components/IntercompanySubRow';
import IntercompanyTableRowActions from 'Pages/Banking/Intercompany/components/IntercompanyTableRowActions';
import {
  CategoryType,
  StatusType,
  TableHeadType,
  TransactionItem,
} from 'Pages/Banking/Intercompany/constants';
import { ReactComponent as HandFilledIcon } from 'Svg/16/hand-filled.svg';
import { ReactComponent as ArrowsRefresh16Icon } from 'Svg/v2/16/arrows-refresh.svg';
import { ReactComponent as ArrowsSwitch16Icon } from 'Svg/v2/16/arrows-switch.svg';
import { ReactComponent as ForReviewIcon } from 'Svg/v2/16/document-search.svg';
import { ReactComponent as TickIcon } from 'Svg/v2/16/tick-in-filled-circle.svg';
import React, { ReactElement, ReactNode } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

type IntercompanyTableRowProps = {
  transaction: TransactionItem;
  tableHead: TableHeadType[];
  activeTab: TabType;
  index: number;
};

const IntercompanyTableRow = ({
  transaction,
  tableHead,
  activeTab,
  index,
}: IntercompanyTableRowProps): ReactElement => {
  const statusIcon: Record<
    StatusType,
    { icon: ReactNode; color: 'grey-90' | 'green-90'; background: 'grey-20' | 'green-10' }
  > = {
    forReview: { icon: <ForReviewIcon />, color: 'grey-90', background: 'grey-20' },
    accepted: { icon: <TickIcon />, color: 'green-90', background: 'green-10' },
  };

  const categoryValues: Record<CategoryType, { icon: ReactNode; title: string }> = {
    bankTransfer: {
      icon: <ArrowsSwitch16Icon />,
      title: 'Bank transfer',
    },
    foreignExchange: {
      icon: <ArrowsRefresh16Icon />,
      title: 'Foreign exchange',
    },
  };

  return (
    <TableRowNew key={index} gap="8" size="auto" className={spacing.pY12}>
      <Container flexdirection="column" gap="24" className={spacing.w100p}>
        <Container alignitems="center" justifycontent="space-between" className={spacing.w100p}>
          <TableCellNew minWidth={tableHead[0].minWidth} justifycontent="center" fixedWidth>
            <IconButton
              icon={statusIcon[transaction.status].icon}
              size="24"
              background={statusIcon[transaction.status].background}
              color={statusIcon[transaction.status].color}
            />
          </TableCellNew>
          <TableCellNew minWidth={tableHead[1].minWidth} fixedWidth>
            <Tag
              text={categoryValues[transaction.category].title}
              icon={categoryValues[transaction.category].icon}
              additionalIconRight={<HandFilledIcon />}
            />
          </TableCellNew>
          <IntercompanySubRow transaction={transaction.outflow} />
          {tableHead[8].active && (
            <TableCellTextNew
              minWidth={tableHead[8].minWidth}
              type={'caption-semibold'}
              align="right"
              value={transaction.bankFee?.mainAmount || ''}
              secondaryValue={transaction.bankFee?.secondaryAmount}
              secondaryColor="grey-90"
              secondaryType={'subtext-semibold'}
              secondaryAlign="right"
              fixedWidth
            />
          )}
          {tableHead[9].active && (
            <TableCellTextNew
              minWidth={tableHead[9].minWidth}
              type={'caption-semibold'}
              align="right"
              color={'red-90'}
              value={transaction.fxResult || ''}
              fixedWidth
            />
          )}
          <TableCellNew
            minWidth={tableHead[10].minWidth}
            alignitems="center"
            justifycontent="center"
            fixedWidth
          >
            <IntercompanyTableRowActions currentTab={activeTab} />
          </TableCellNew>
        </Container>
        <Container alignitems="center" justifycontent="space-between" className={spacing.w100p}>
          <TableCellNew minWidth={tableHead[0].minWidth} fixedWidth />
          <TableCellNew minWidth={tableHead[1].minWidth} fixedWidth />
          <IntercompanySubRow transaction={transaction.inflow} />
          {tableHead[8].active && <TableCellNew minWidth={tableHead[8].minWidth} fixedWidth />}
          {tableHead[9].active && <TableCellNew minWidth={tableHead[9].minWidth} fixedWidth />}
          <TableCellNew minWidth={tableHead[10].minWidth} fixedWidth />
        </Container>
      </Container>
    </TableRowNew>
  );
};

export default IntercompanyTableRow;
