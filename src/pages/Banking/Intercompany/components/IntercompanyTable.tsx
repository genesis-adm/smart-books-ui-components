import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import type { TableCellWidth } from 'Components/types/gridTypes';
import { TabType } from 'Pages/Banking/Intercompany/IntercompanyMainPage.stories';
import IntercompanyTableRow from 'Pages/Banking/Intercompany/components/IntercompanyTableRow';
import IntercompanyTablePlaceholder from 'Pages/Banking/Intercompany/components/placeholders/IntercompanyTablePlaceholder';
import { TableHeadType, tableRowData } from 'Pages/Banking/Intercompany/constants';
import classNames from 'classnames';
import React, { ReactElement, useMemo } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

type IntercompanyTableProps = {
  activeTab: TabType;
  tableHead: TableHeadType[];
  isLoading: boolean;
};

const IntercompanyTable = ({
  activeTab,
  tableHead,
  isLoading,
}: IntercompanyTableProps): ReactElement => {
  const tableData = useMemo(() => {
    return activeTab === 'all'
      ? tableRowData
      : tableRowData.filter((transaction) => transaction.status === activeTab);
  }, [activeTab]);

  if (isLoading) return <IntercompanyTablePlaceholder />;

  return (
    <TableNew
      noResults={false}
      className={classNames(spacing.mt24, spacing.ml24, spacing.pr24)}
      tableHead={
        <TableHeadNew>
          {tableHead.map((item, index) => {
            if (!item.active) return null;
            return (
              <TableTitleNew
                key={index}
                minWidth={item.minWidth as TableCellWidth}
                align={item.align as TableTitleNewProps['align']}
                title={item.title}
                sorting={item.sorting}
                onClick={() => {}}
              />
            );
          })}
        </TableHeadNew>
      }
    >
      <TableBodyNew>
        {tableData.map((item, index) => (
          <IntercompanyTableRow
            transaction={item}
            tableHead={tableHead}
            activeTab={activeTab}
            index={index}
          />
        ))}
      </TableBodyNew>
    </TableNew>
  );
};

export default IntercompanyTable;
