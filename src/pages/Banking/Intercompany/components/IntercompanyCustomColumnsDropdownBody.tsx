import { useDropDown } from 'Components/DropDown';
import { Divider } from 'Components/base/Divider';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { FilterDropDownItem } from 'Components/elements/filters/FilterDropDownItem';
import { FilterItemsContainer } from 'Components/elements/filters/FilterItemsContainer';
import {
  CustomColumnType,
  TableHeadType,
  customColumns,
  tableHead,
} from 'Pages/Banking/Intercompany/constants';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

type IntercompanyCustomColumnsDropdownBodyProps = {
  setTableHeadArr(items: TableHeadType[]): void;
  setCheckedCustomColumns(items: CustomColumnType[]): void;
  checkedCustomColumns: CustomColumnType[];
};

const IntercompanyCustomColumnsDropdownBody = ({
  setTableHeadArr,
  setCheckedCustomColumns,
  checkedCustomColumns,
}: IntercompanyCustomColumnsDropdownBodyProps) => {
  const handleCheckCustomColumns = (col: CustomColumnType) => () => {
    setCheckedCustomColumns(
      checkedCustomColumns.some((prev) => prev.id.includes(col.id))
        ? checkedCustomColumns.filter((item) => item.id !== col.id)
        : [...checkedCustomColumns, col],
    );
  };

  const [handleOpen] = useDropDown();

  const applyCheckedColumns = () => {
    const updatedTableHead = tableHead.map((item) => {
      return checkedCustomColumns.some((column) => column.title === item.title)
        ? {
            ...item,
            active: !item.active,
          }
        : item;
    });
    setTableHeadArr(updatedTableHead);
  };

  return (
    <Container flexdirection="column" className={spacing.w320fixed}>
      <Text
        type="caption-semibold"
        color="grey-100"
        className={classNames(spacing.mt24, spacing.mX16)}
      >
        Add Custom columns
      </Text>
      <FilterItemsContainer className={classNames(spacing.mt14, spacing.mb16, spacing.mX12)}>
        <FilterDropDownItem
          id="selectAll"
          type="item"
          checked={checkedCustomColumns.length === customColumns.length}
          blurred={false}
          value="Select All"
          onChange={() =>
            setCheckedCustomColumns(
              checkedCustomColumns.length === customColumns.length ? [] : customColumns,
            )
          }
        />
        {customColumns.map(({ id, title }) => (
          <FilterDropDownItem
            key={id}
            id={id}
            type="item"
            checked={!!checkedCustomColumns.find((item) => item.id === id)}
            blurred={false}
            value={title}
            onChange={handleCheckCustomColumns({
              id,
              title,
            })}
          />
        ))}
      </FilterItemsContainer>
      <Divider fullHorizontalWidth />
      <Container
        className={classNames(spacing.m16, spacing.w288fixed)}
        justifycontent="space-between"
        alignitems="center"
        flexdirection="row-reverse"
      >
        <Button
          width="auto"
          height="32"
          padding="8"
          font="text-medium"
          onClick={() => {
            applyCheckedColumns();
            handleOpen?.();
          }}
          background={'violet-90-violet-100'}
          color="white-100"
        >
          Apply
        </Button>
        <Button
          width="auto"
          height="32"
          padding="8"
          font="text-medium"
          onClick={() => {}}
          background={'blue-10-red-10'}
          color="violet-90-red-90"
        >
          Clear all
        </Button>
      </Container>
    </Container>
  );
};

export default IntercompanyCustomColumnsDropdownBody;
