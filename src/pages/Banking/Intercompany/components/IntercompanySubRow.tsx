import { TagSegmented } from 'Components/base/TagSegmented';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TransactionData, tableHead } from 'Pages/Banking/Intercompany/constants';
import { iconsBasedOnType } from 'Pages/Banking/Intercompany/modals/MatchTransactions/constants';
import { bankAccounts } from 'Pages/Banking/Transactions/constants';
import { ReactComponent as CreditCardIcon } from 'Svg/v2/16/credit-card.svg';
import React, { ReactElement } from 'react';

type IntercompanySubRowProps = { transaction: TransactionData };

const IntercompanySubRow = ({ transaction }: IntercompanySubRowProps): ReactElement => {
  return (
    <>
      <TableCellTextNew
        minWidth={tableHead[2].minWidth}
        fixedWidth
        noWrap
        lineClamp={'none'}
        value={`ID ${transaction.id}`}
        secondaryValue="08 September 2022"
        type="caption-semibold"
        secondaryType="text-regular"
        tagColor={iconsBasedOnType[transaction.type].color}
        tagIcon={iconsBasedOnType[transaction.type].icon}
      />
      <TableCellNew minWidth={tableHead[3].minWidth} fixedWidth flexdirection="column" gap="12">
        <TagSegmented
          {...bankAccounts}
          icon={<CreditCardIcon />}
          tooltipMessage={
            <Container flexdirection="column">
              <Text color="white-100" style={{ wordBreak: 'break-all' }}>
                {bankAccounts.mainLabel}
              </Text>
              <Text color="white-100" style={{ wordBreak: 'break-all' }}>
                {bankAccounts.secondLabel} | {bankAccounts.thirdLabel}
              </Text>
            </Container>
          }
        />
      </TableCellNew>
      <TableCellTextNew
        minWidth={tableHead[4].minWidth}
        fixedWidth
        value={transaction.legalEntity}
      />
      <TableCellTextNew
        minWidth={tableHead[5].minWidth}
        fixedWidth
        value={transaction.businessUnit}
      />
      <TableCellTextNew
        minWidth={tableHead[6].minWidth}
        fixedWidth
        lineClamp="2"
        value="ISSUE COY TT & COVER / / TRANSFER OF OWN erjugfhweiu"
      />
      <TableCellTextNew
        minWidth={tableHead[7].minWidth}
        fixedWidth
        type="caption-semibold"
        align="right"
        color={transaction.type === 'inflow' ? 'green-90' : 'red-90'}
        value={transaction.amount.mainAmount}
        secondaryValue={transaction.amount.secondaryAmount}
        secondaryColor="grey-90"
        secondaryAlign="right"
      />
    </>
  );
};

export default IntercompanySubRow;
