import { ContentLoader } from 'Components/base/ContentLoader';
import { Container } from 'Components/base/grid/Container';
import classNames from 'classnames';
import React, { ReactElement } from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';

const IntercompanyTablePlaceholder = (): ReactElement => {
  return (
    <Container
      className={classNames(spacing.pb_auto, spacing.p24, spacing.h100p)}
      gap="8"
      flexdirection="column"
    >
      <ContentLoader height="20px" isLoading />
      {Array.from(Array(2)).map((_, index) => (
        <ContentLoader key={index} height="120px" isLoading />
      ))}
    </Container>
  );
};

export default IntercompanyTablePlaceholder;
