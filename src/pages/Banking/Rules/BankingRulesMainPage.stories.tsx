import { Meta, Story } from '@storybook/react';
import { DropDown } from 'Components/DropDown';
import { Checkbox } from 'Components/base/Checkbox';
import { Icon } from 'Components/base/Icon';
import { LoadingLine } from 'Components/base/LoadingLine';
import { Tag } from 'Components/base/Tag';
import { TagSegmented } from 'Components/base/TagSegmented';
import { Button } from 'Components/base/buttons/Button';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { ContainerMain } from 'Components/base/grid/ContainerMain';
import { MainPageContentContainer } from 'Components/base/grid/MainPageContentContainer';
import { GeneralTab, GeneralTabWrapper } from 'Components/base/tabs/GeneralTab';
import { MenuTab, MenuTabWrapper } from 'Components/base/tabs/MenuTab';
import { Text } from 'Components/base/typography/Text';
import { MainPageEmpty } from 'Components/custom/MainPage/MainPageEmpty';
import { RulesCardLE } from 'Components/custom/Rules/RulesCardLE';
import { RulesSelectLE } from 'Components/custom/Rules/RulesSelectLE';
import { HeaderSB } from 'Components/custom/Stories/HeaderSB';
import { NavigationSB } from 'Components/custom/Stories/NavigationSB';
import { Pagination } from 'Components/custom/Stories/Pagination/Pagination';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { Search } from 'Components/inputs/Search';
import type { TableCellWidth } from 'Components/types/gridTypes';
import { ReactComponent as ArrowDownIcon } from 'Svg/v2/16/arrow-down.svg';
import { ReactComponent as ArrowUpIcon } from 'Svg/v2/16/arrow-up.svg';
import { ReactComponent as BankIcon } from 'Svg/v2/16/bank.svg';
import { ReactComponent as CopyIcon } from 'Svg/v2/16/copy.svg';
import { ReactComponent as DragIcon } from 'Svg/v2/16/drag.svg';
import { ReactComponent as DisableIcon } from 'Svg/v2/16/eye-crossed.svg';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as AutoRuleIcon } from 'Svg/v2/16/reload.svg';
import { ReactComponent as SearchIcon } from 'Svg/v2/16/search.svg';
import { ReactComponent as SettingsIcon } from 'Svg/v2/16/settings.svg';
import { ReactComponent as DropdownIcon } from 'Svg/v2/16/three-dots.svg';
import { ReactComponent as ActiveRuleIcon } from 'Svg/v2/16/tick-in-circle.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import handleAccountLength from 'Utils/handleAccountLength';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

const BankingRulesMainPageComponent: Story = ({ storyState }) => {
  const [toggle, toggleMenu] = useState<boolean>(false);
  const tableHead = [
    {
      minWidth: '48',
      title: <Icon icon={<DragIcon />} path="grey-90" />,
      padding: 'none',
    },
    {
      minWidth: '48',
      title: <Checkbox name="tickAll" checked onChange={() => {}} />,
      padding: 'none',
    },
    {
      minWidth: '90',
      title: 'Priority',
      helpText: 'Priority tooltip text',
    },
    {
      minWidth: '190',
      title: 'Rule name',
    },
    {
      minWidth: '80',
      title: 'Direction',
      align: 'center',
    },
    {
      minWidth: '340',
      title: 'Bank accounts',
    },
    {
      minWidth: '380',
      title: 'If conditions',
    },
    {
      minWidth: '130',
      title: 'Rule group',
      align: 'center',
      helpText: 'Rule group tooltip text',
    },
    {
      minWidth: '120',
      title: 'Rule status',
      align: 'center',
    },
    {
      minWidth: '80',
      title: 'Action',
      align: 'center',
    },
  ];
  const bankAccounts = {
    mainLabel: 'Qatar National Bank',
    secondLabel: handleAccountLength('QA09SKQZ995647769596875246578', 8),
    thirdLabel: 'QAR',
    icon: <BankIcon />,
  };

  return (
    <Container fullscreen flexwrap="nowrap" background={'grey-10'}>
      <NavigationSB opened={toggle} onToggleOpen={() => toggleMenu(!toggle)} />
      <ContainerMain navOpened={toggle}>
        <HeaderSB />
        <Container flexdirection="column" className={spacing.mX32}>
          <Text type="h1-semibold">Banking</Text>
          <MenuTabWrapper className={spacing.mt24}>
            <MenuTab id="transactions" active={false} onClick={() => {}}>
              Transactions
            </MenuTab>
            <MenuTab id="bankAccounts" active={false} onClick={() => {}}>
              Bank accounts
            </MenuTab>
            <MenuTab id="rules" active onClick={() => {}}>
              Rules
            </MenuTab>
          </MenuTabWrapper>
        </Container>
        <MainPageContentContainer>
          {storyState === 'loaded' && (
            <>
              <Container
                className={classNames(spacing.mX24, spacing.mt24)}
                justifycontent="space-between"
                gap="24"
              >
                <Container gap="24">
                  <RulesSelectLE name="BetterMe Business">
                    <Container
                      flexdirection="column"
                      className={classNames(spacing.w1130fixed, spacing.pt24, spacing.pX24)}
                    >
                      <Container
                        className={spacing.w100p}
                        justifycontent="space-between"
                        alignitems="center"
                      >
                        <Text type="title-semibold">Please choose the legal entity</Text>
                        <IconButton
                          icon={<SearchIcon />}
                          size="40"
                          color="grey-100"
                          background={'grey-10-grey-20'}
                          onClick={() => {}}
                        />
                        {/* <SearchNew
                          width="320"
                          height="40"
                          name="search-banking-rules-mainPage"
                          placeholder="Search by If conditions"
                          value=""
                          onChange={() => {}}
                          searchParams={[]}
                          onSearch={() => {}}
                          onClear={() => {}}
                        /> */}
                      </Container>
                      <GeneralTabWrapper className={spacing.mt4}>
                        <GeneralTab id="allrules" onClick={() => {}} active>
                          All rules
                        </GeneralTab>
                        <GeneralTab id="withoutrules" onClick={() => {}} active={false}>
                          Without rules
                        </GeneralTab>
                      </GeneralTabWrapper>
                      <Container className={spacing.mY24} gap="16" flexwrap="wrap">
                        {Array.from(Array(9)).map((_, index) => (
                          <RulesCardLE
                            key={index}
                            name="BetterMe Business Holdings Corp"
                            rulesAmount={index}
                            selected={false}
                            onAddRule={() => {}}
                            onClick={() => {}}
                          />
                        ))}
                      </Container>
                      <Pagination padding="16" />
                    </Container>
                  </RulesSelectLE>
                  <Search
                    width="320"
                    height="40"
                    name="search-banking-rules-mainPage"
                    placeholder="Search by If conditions"
                    value=""
                    onChange={() => {}}
                    searchParams={[]}
                    onSearch={() => {}}
                    onClear={() => {}}
                  />
                </Container>
                <Container gap="24">
                  <IconButton
                    size="40"
                    background={'grey-10-grey-20'}
                    color="grey-100"
                    icon={<SettingsIcon />}
                    onClick={() => {}}
                  />
                  <Button
                    background={'violet-90-violet-100'}
                    color="white-100"
                    width="140"
                    height="40"
                    iconLeft={<PlusIcon />}
                  >
                    Create
                  </Button>
                </Container>
              </Container>
              <TableNew
                noResults={false}
                className={classNames(spacing.mt24, spacing.ml24, spacing.pr24)}
                tableHead={
                  <TableHeadNew>
                    {tableHead.map((item, index) => (
                      <TableTitleNew
                        key={index}
                        minWidth={item.minWidth as TableCellWidth}
                        align={item.align as TableTitleNewProps['align']}
                        padding={item.padding as TableTitleNewProps['padding']}
                        title={item.title}
                        helpText={item.helpText}
                        onClick={() => {}}
                      />
                    ))}
                  </TableHeadNew>
                }
              >
                <TableBodyNew>
                  {Array.from(Array(39)).map((_, index) => (
                    <TableRowNew key={index} gap="8">
                      <TableCellNew minWidth="48" justifycontent="center" alignitems="center">
                        <Icon icon={<DragIcon />} cursor="move" path="grey-90" />
                      </TableCellNew>
                      <TableCellNew minWidth="48" justifycontent="center" alignitems="center">
                        <Checkbox name={`line-${index}`} checked onChange={() => {}} />
                      </TableCellNew>
                      <TableCellTextNew
                        minWidth="90"
                        type="text-regular"
                        value={String(index + 1)}
                      />
                      <TableCellTextNew
                        minWidth="190"
                        type="text-regular"
                        value="My first rule name"
                      />
                      <TableCellNew minWidth="80" justifycontent="center" alignitems="center">
                        {index % 2 === 0 ? (
                          <Tag
                            icon={<ArrowDownIcon />}
                            color="green"
                            shape="circle"
                            tooltip="Money in"
                            cursor="default"
                          />
                        ) : (
                          <Tag
                            icon={<ArrowUpIcon />}
                            color="red"
                            shape="circle"
                            tooltip="Money out"
                            cursor="default"
                          />
                        )}
                      </TableCellNew>
                      <TableCellNew minWidth="340" alignitems="center">
                        <TagSegmented
                          {...bankAccounts}
                          tooltipMessage={
                            <Container flexdirection="column">
                              <Text color="white-100" style={{ wordBreak: 'break-all' }}>
                                {bankAccounts.mainLabel}
                              </Text>
                              <Text color="white-100" style={{ wordBreak: 'break-all' }}>
                                {bankAccounts.secondLabel} | {bankAccounts.thirdLabel}
                              </Text>
                            </Container>
                          }
                        />
                      </TableCellNew>
                      <TableCellTextNew
                        minWidth="380"
                        type="text-regular"
                        value="If Money out in All bank account and Description contains transfer of salary and Description contains Citibank then assign to account Payroll expenses then assign to 2 accounts and 5 business divisions"
                        lineClamp="2"
                        tooltip={
                          <>
                            <Text display="block" color="grey-100">
                              if&nbsp;
                              <Text color="white-100">Money out</Text>
                              &nbsp;in&nbsp;
                              <Text color="white-100">All bank account</Text>
                            </Text>
                            <Text className={spacing.ml16} display="block" color="grey-100">
                              and&nbsp;
                              <Text color="white-100">Description</Text>
                              &nbsp;contains&nbsp;
                              <Text color="white-100">Transfer of salary</Text>
                            </Text>
                            <Text className={spacing.ml16} display="block" color="grey-100">
                              and&nbsp;
                              <Text color="white-100">Description</Text>
                              &nbsp;contains&nbsp;
                              <Text color="white-100">Citibank</Text>
                            </Text>
                            <Text className={spacing.ml32} display="block" color="grey-100">
                              then assign to account&nbsp;
                              <Text color="white-100">Payroll expenses</Text>
                            </Text>
                            <Text className={spacing.ml32} display="block" color="grey-100">
                              then assign to&nbsp;
                              <Text color="white-100">2 accounts</Text>
                              &nbsp;and&nbsp;
                              <Text color="white-100">5 business divisions</Text>
                            </Text>
                          </>
                        }
                      />
                      <TableCellNew minWidth="130" justifycontent="center" alignitems="center">
                        <Tag padding="8" icon={<AutoRuleIcon />} color="grey" text="Auto" />
                        {/* <Tag
                          padding="8"
                          icon={<ManualRuleIcon />}
                          color="grey"
                          text="Manual"
                        /> */}
                      </TableCellNew>
                      <TableCellNew minWidth="120" justifycontent="center" alignitems="center">
                        <Tag padding="8" icon={<ActiveRuleIcon />} color="green" text="Active" />
                        {/* <Tag padding="8" icon={<DisabledRuleIcon />} color="yellow" text="Disabled" /> */}
                      </TableCellNew>
                      <TableCellNew minWidth="80" alignitems="center" justifycontent="center">
                        <DropDown
                          flexdirection="column"
                          padding="8"
                          control={({ handleOpen }) => (
                            <IconButton
                              icon={<DropdownIcon />}
                              onClick={handleOpen}
                              background={'transparent'}
                              color="grey-100-violet-90"
                            />
                          )}
                        >
                          <DropDownButton size="160" icon={<EditIcon />} onClick={() => {}}>
                            Edit
                          </DropDownButton>
                          <DropDownButton size="160" icon={<DisableIcon />} onClick={() => {}}>
                            Disable
                          </DropDownButton>
                          {/* <DropDownButton size="160" icon={<EnableIcon />} onClick={() => {}}>
                            Enable
                          </DropDownButton> */}
                          <DropDownButton size="160" icon={<CopyIcon />} onClick={() => {}}>
                            Copy
                          </DropDownButton>
                          <DropDownButton
                            size="160"
                            type="danger"
                            icon={<TrashIcon />}
                            onClick={() => {}}
                          >
                            Delete
                          </DropDownButton>
                        </DropDown>
                      </TableCellNew>
                    </TableRowNew>
                  ))}
                </TableBodyNew>
              </TableNew>
              <Pagination padding="16" />
            </>
          )}
          {storyState === 'empty' && (
            <MainPageEmpty
              title="Create new Rules"
              description="You have no rules created. Your rules will be displayed here."
              action={
                <Button
                  width="220"
                  height="40"
                  background={'blue-10-blue-20'}
                  color="violet-90-violet-100"
                  iconLeft={<PlusIcon />}
                  onClick={() => {}}
                >
                  Create rule
                </Button>
              }
            />
          )}
        </MainPageContentContainer>
        <LoadingLine loadingCompleted={storyState !== 'loading'} />
      </ContainerMain>
    </Container>
  );
};

export default {
  title: 'Pages/Banking/Rules/Banking Rules Main Page',
  component: BankingRulesMainPageComponent,
  argTypes: {
    storyState: {
      options: ['empty', 'loading', 'loaded'],
      control: {
        type: 'radio',
        labels: {
          empty: 'Empty page',
          loading: 'Page is loading',
          loaded: 'Page loaded',
        },
      },
    },
  },
} as Meta;

export const BankingRulesMainPage = BankingRulesMainPageComponent.bind({});
BankingRulesMainPage.args = {
  storyState: 'loaded',
};
