export const rulesTableHead = [
  {
    minWidth: '260',
    title: 'Parameter',
  },
  {
    minWidth: '260',
    title: 'Condition',
  },
  {
    minWidth: '260',
    title: 'Value',
  },
  {
    minWidth: '28',
  },
];

export const conditionsOptions = [
  { value: 'description', label: 'Description' },
  { value: 'currency', label: 'Currency' },
  { value: 'bankAccount ', label: 'Bank Account type ' },
  { value: 'amount', label: 'Amount' },
];

export const descriptionsSelectOptions = [
  { value: 'contains', label: 'Contains' },
  { value: "doesn'tContain", label: "Doesn't contain" },
  { value: 'notEqual', label: 'Not equal' },
];

export const currencySelectOptions = [
  { value: 'equal', label: 'Equal' },
  { value: 'notEqual', label: 'Not equal' },
];
export const bankAccountTypeSelectOptions = [
  { value: 'card', label: 'Card' },
  { value: 'bankAccount', label: 'Bank account' },
];

export const amountSelectOptions = [
  { value: 'greaterThan', label: 'Greater than' },
  { value: 'lessThan', label: 'Less than' },
  { value: 'equal', label: 'Equal' },
  { value: 'notEqual', label: 'Not equal' },
];
