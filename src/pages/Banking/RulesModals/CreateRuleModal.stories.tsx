import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownFooter } from 'Components/SelectDropdown/components/SelectDropdownFooter';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { Divider } from 'Components/base/Divider';
import { Radio } from 'Components/base/Radio';
import { Switch } from 'Components/base/Switch';
import { Tag } from 'Components/base/Tag';
import { Button } from 'Components/base/buttons/Button';
import { IconButton } from 'Components/base/buttons/IconButton';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { InputNew } from 'Components/base/inputs/InputNew';
import { Text } from 'Components/base/typography/Text';
import { Logo } from 'Components/elements/Logo';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableTitleNew } from 'Components/elements/Table/TableTitleNew';
import { CircledIcon } from 'Components/graphics/CircledIcon';
import { Search } from 'Components/inputs/Search';
import { Select } from 'Components/inputs/Select';
import { SelectOption } from 'Components/inputs/Select/Select.types';
import { OptionWithFourLabels } from 'Components/inputs/Select/options/OptionWithFourLabels';
import { OptionWithTwoLabels } from 'Components/inputs/Select/options/OptionWithTwoLabels';
import { OptionsFooter } from 'Components/inputs/Select/options/OptionsFooter';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { TableCellWidth } from 'Components/types/gridTypes';
import { options } from 'Mocks/fakeOptions';
import BankSelectDialogInfo from 'Pages/Banking/RulesModals/components/BankSelectDialogInfo';
import { TableRowExamples } from 'Pages/Banking/RulesModals/components/TableRowExamples';
import { rulesTableHead } from 'Pages/Banking/RulesModals/constants';
import AddSplitModal from 'Pages/Banking/TransactionsModals/components/AddSplitModal';
import { ReactComponent as BriefcaseIcon } from 'Svg/v2/12/briefcase.svg';
import { ReactComponent as CloseIcon } from 'Svg/v2/16/cross.svg';
import { ReactComponent as ForReviewIcon } from 'Svg/v2/16/document-search.svg';
import { ReactComponent as DocumentIcon } from 'Svg/v2/16/file-blank.svg';
import { ReactComponent as InfoIcon } from 'Svg/v2/16/info.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import { deleteArrayItemByIndex } from 'Utils/arrays';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';
import DialogTooltip from '../../../components/base/DialogTooltip/DialogTooltip';
import Icon from '../../../components/base/Icon/Icon';

export default {
  title: 'Pages/Banking/Rules Modals',
};

export const CreateRuleModal: React.FC = () => {
  const [ruleName, setRuleName] = useState('');
  const [checkedTransactionTypeRadio, setCheckedTransactionTypeRadio] = useState<
    'inflow' | 'outflow'
  >('inflow');

  const [appliedOptions, setAppliedOptions] = useState<SelectOption[] | undefined | null>([]);
  const [searchValue, setSearchValue] = useState('');

  const [isOpenedCounterpartySelect, setIsOpenedCounterpartySelect] = useState<boolean>(false);
  const [isOpenedAddSplitBlock, setIsOpenedAddSplitBlock] = useState(false);

  return (
    <Modal
      size="960"
      background={'grey-10'}
      type="new"
      overlay="black-100"
      header={
        <Container
          className={classNames(spacing.w100p, spacing.p24)}
          alignitems="center"
          justifycontent="space-between"
          flexwrap="wrap"
          style={{ borderBottom: '1px solid #EDEFF1' }}
          background={'grey-10'}
        >
          <Container gap="12">
            <Text type="title-bold">Create new Rule</Text>
            <Tag
              size="32"
              padding="8"
              color="green"
              text="New"
              icon={<DocumentIcon />}
              cursor="default"
            />
          </Container>
          <IconButton
            background={'white-100'}
            size="24"
            icon={<CloseIcon />}
            color="black-100"
            onClick={() => {}}
          />
        </Container>
      }
      footer={
        <ModalFooterNew justifycontent="space-between" background={'grey-10'} border>
          <Button height="48" width="196" background={'grey-40'} onClick={() => {}}>
            Save
          </Button>
        </ModalFooterNew>
      }
    >
      <Container
        flexdirection="column"
        borderRadius="20"
        background={'white-100'}
        className={classNames(spacing.p24)}
        flexgrow="1"
        gap="24"
      >
        <Container alignitems="center" gap="32">
          <Container gap="12" alignitems="center">
            <Logo content="user" size="24" name="Vitalii Kvasha" />
            <Container flexdirection="column">
              <Text type="caption-regular" color="grey-100">
                Created by
              </Text>
              <Text type="body-medium">Vitalii Kvasha</Text>
            </Container>
          </Container>
          <Container gap="12" alignitems="center">
            <CircledIcon size="24" background={'grey-20'} icon={<BriefcaseIcon />} />
            <Container flexdirection="column">
              <Text type="caption-regular" color="grey-100">
                Legal entity
              </Text>
              <Text type="body-medium">BetterMe Limited</Text>
            </Container>
          </Container>
        </Container>
        <Divider />
        <Container justifycontent="space-between">
          <Container flexdirection="column" gap="16">
            <Container gap="8">
              <Text type="body-regular-14" color="black-90">
                Rules only apply to transaction with status:
              </Text>
              <Tag text="For review" icon={<ForReviewIcon />} />
            </Container>
            <InputNew
              name="ruleName"
              label="Rule name"
              width="375"
              required
              onChange={(e) => {
                setRuleName(e.target.value);
              }}
              value={ruleName}
            />
          </Container>
          <Container gap="24">
            <Divider type="vertical" height="full" />
            <Container flexdirection="column" gap="24" justifycontent="center">
              <Container>
                <Switch id="automaticallyAccept" checked={false} onChange={() => {}} />
                <Text className={spacing.mX12} type="body-regular-14" color="grey-100">
                  Automatically accept transaction
                </Text>
              </Container>
              <Container>
                <Switch id="linkedToPaidlog" checked={false} onChange={() => {}} />
                <Text className={spacing.mX12} type="body-regular-14" color="grey-100">
                  Apply rule for transaction linked to paidlog
                </Text>
              </Container>
            </Container>
          </Container>
        </Container>
        <Container gap="12">
          <Text color="black-90" type="body-regular-14">
            Apply rule to transaction type:
          </Text>
          <Radio
            id="inflow"
            onChange={() => {
              setCheckedTransactionTypeRadio('inflow');
            }}
            name="inflow"
            color={checkedTransactionTypeRadio === 'inflow' ? 'violet-90' : 'grey-100'}
            checked={checkedTransactionTypeRadio === 'inflow'}
          >
            Inflow
          </Radio>
          <Radio
            id="outflow"
            onChange={() => {
              setCheckedTransactionTypeRadio('outflow');
            }}
            name="outflow"
            color={checkedTransactionTypeRadio === 'outflow' ? 'violet-90' : 'grey-100'}
            checked={checkedTransactionTypeRadio === 'outflow'}
          >
            Outflow
          </Radio>
        </Container>
        <Container gap="24" alignitems="center">
          <Text color="grey-100" type="body-regular-14">
            in
          </Text>
          <Select
            label="Bank account"
            required
            placeholder="Bank account"
            type="bankAccount"
            isMulti
            value={appliedOptions}
            onChange={(value) => {
              setAppliedOptions(value);
            }}
            icon={
              <DialogTooltip
                title="Please kindly note:"
                className={classNames(spacing.pt3)}
                control={({ handleOpen }) => (
                  <Icon
                    icon={<InfoIcon />}
                    path="grey-100-black-100"
                    cursor="pointer"
                    onClick={(e) => {
                      e.stopPropagation();
                      handleOpen();
                    }}
                  />
                )}
              >
                <BankSelectDialogInfo />
              </DialogTooltip>
            }
          >
            {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
              <SelectDropdown
                selectElement={selectElement}
                width="480"
                selectValue={selectValue}
                isMulti
              >
                {({ selectedOptions, setSelectedOptions }) => (
                  <>
                    <SelectDropdownBody>
                      <Search
                        className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                        width="full"
                        height="40"
                        name="search"
                        placeholder="Search Bank Account"
                        value={searchValue}
                        onChange={(e) => setSearchValue(e.target.value)}
                        searchParams={[]}
                        onSearch={() => {}}
                        onClear={() => {}}
                      />
                      <SelectDropdownItemList
                        type="multi"
                        isSelectFilled={!!appliedOptions?.length}
                        selectedItems={appliedOptions?.map((appliedOption, index) => (
                          <OptionWithFourLabels
                            key={index}
                            id={appliedOption.value}
                            type="checkbox"
                            label={appliedOption.label}
                            secondaryLabel={appliedOption.subLabel}
                            tertiaryLabel={appliedOption.tertiaryLabel}
                            quadroLabel={appliedOption.quadroLabel}
                            selected={
                              !!selectedOptions?.find(
                                ({ value: selectedValue }) =>
                                  selectedValue === appliedOption?.value,
                              )
                            }
                            onClick={() => {
                              if (!selectedOptions) return;
                              const index = selectedOptions?.findIndex(
                                ({ value: selectedValue }) =>
                                  selectedValue === appliedOption?.value,
                              );
                              if (index !== -1) {
                                const arr: SelectOption[] = deleteArrayItemByIndex(
                                  selectedOptions,
                                  index,
                                );
                                setSelectedOptions(arr);
                                return;
                              }
                              setSelectedOptions([...selectedOptions, appliedOption]);
                            }}
                          />
                        ))}
                        otherItems={options
                          ?.filter(
                            ({ value }) =>
                              !appliedOptions?.find(
                                ({ value: appliedValue }) => appliedValue === value,
                              ),
                          )
                          .map((value, index) => (
                            <OptionWithFourLabels
                              key={index}
                              id={value.value}
                              type="checkbox"
                              label={value.label}
                              secondaryLabel={value.subLabel}
                              tertiaryLabel={value.tertiaryLabel}
                              quadroLabel={value.quadroLabel}
                              selected={
                                !!selectedOptions?.find(
                                  ({ value: selectedValue }) => selectedValue === value?.value,
                                )
                              }
                              onClick={() => {
                                if (!selectedOptions) return;
                                const index = selectedOptions?.findIndex(
                                  ({ value: selectedValue }) => selectedValue === value?.value,
                                );
                                if (index !== -1) {
                                  const arr: SelectOption[] = deleteArrayItemByIndex(
                                    selectedOptions,
                                    index,
                                  );
                                  setSelectedOptions(arr);
                                  return;
                                }
                                setSelectedOptions([...selectedOptions, value]);
                              }}
                            />
                          ))}
                      />
                    </SelectDropdownBody>
                    <SelectDropdownFooter>
                      <OptionsFooter
                        optionsLength={options.length}
                        selectedOptionsNumber={selectedOptions?.length || 0}
                        checked={selectedOptions?.length === options.length}
                        onSelectChange={() => {
                          selectedOptions?.length !== options.length
                            ? setSelectedOptions(options)
                            : setSelectedOptions([]);
                        }}
                        onClear={() => {
                          setSelectedOptions([]);
                        }}
                        onApply={() => {
                          onOptionClick(selectedOptions);
                          closeDropdown();
                        }}
                      />
                    </SelectDropdownFooter>
                  </>
                )}
              </SelectDropdown>
            )}
          </Select>
        </Container>
        <Container gap="12">
          <Text color="black-90" type="body-regular-14">
            And include the following conditions:
          </Text>
          <Radio id="all" onChange={() => {}} name="all" color="grey-100" checked>
            All
          </Radio>
          <Radio id="any" onChange={() => {}} name="any" color="grey-100" checked={false}>
            Any
          </Radio>
        </Container>
        <TableNew
          noResults={false}
          tableHead={
            <TableHeadNew>
              {rulesTableHead.map((item, index) => (
                <TableTitleNew
                  key={index}
                  minWidth={item.minWidth as TableCellWidth}
                  title={item.title}
                  onClick={() => {}}
                />
              ))}
            </TableHeadNew>
          }
        >
          <TableBodyNew>
            <TableRowExamples />
            <Container className={classNames(spacing.mt16, spacing.mr16)} justifycontent="flex-end">
              <LinkButton icon={<PlusIcon />} onClick={() => {}}>
                Add new line
              </LinkButton>
            </Container>
          </TableBodyNew>
        </TableNew>
      </Container>
      {!isOpenedAddSplitBlock && (
        <>
          <Text className={spacing.p24} color="black-90" type="body-regular-14">
            Then assign
          </Text>
          <Container
            justifycontent="space-between"
            alignitems="center"
            borderRadius="20"
            background={'white-100'}
            className={classNames(spacing.p24)}
            gap="16"
          >
            <Container gap="28" alignitems="center">
              <Text color="grey-100" type="body-regular-14">
                Choose account
              </Text>
              <Select
                label="Account"
                width="502"
                placeholder="Select option"
                type="baseWithCategory"
              >
                {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
                  <SelectDropdown selectElement={selectElement}>
                    <SelectDropdownBody>
                      <Search
                        className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                        width="full"
                        height="40"
                        name="search-account"
                        placeholder="Search Account"
                        value={searchValue}
                        onChange={(e) => setSearchValue(e.target.value)}
                        searchParams={[]}
                        onSearch={() => {}}
                        onClear={() => {}}
                      />
                      <SelectDropdownItemList>
                        {options.map((value, index) => (
                          <OptionWithTwoLabels
                            id={value.label}
                            key={index}
                            tooltip
                            labelDirection="horizontal"
                            label={value?.label}
                            secondaryLabel={value.subLabel}
                            selected={
                              !Array.isArray(selectValue) && selectValue?.value === value.value
                            }
                            onClick={() => {
                              onOptionClick({
                                value: value?.value,
                                label: value?.label,
                                subLabel: value?.subLabel,
                              });
                              closeDropdown();
                            }}
                          />
                        ))}
                      </SelectDropdownItemList>
                    </SelectDropdownBody>
                    <SelectDropdownFooter>
                      <Container justifycontent="flex-end" className={classNames(spacing.p16)}>
                        <LinkButton icon={<PlusIcon />} onClick={() => {}}>
                          Create New Template Account
                        </LinkButton>
                      </Container>
                    </SelectDropdownFooter>
                  </SelectDropdown>
                )}
              </Select>
            </Container>
            <LinkButton
              icon={<PlusIcon />}
              onClick={() => {
                setIsOpenedAddSplitBlock(true);
              }}
            >
              Add Split
            </LinkButton>
          </Container>
        </>
      )}
      {isOpenedAddSplitBlock && (
        <Container
          borderRadius="20"
          background={'white-100'}
          className={classNames(spacing.p24, spacing.mt24)}
          flexgrow="1"
          gap="24"
        >
          <AddSplitModal type="rules" />
        </Container>
      )}
      {!isOpenedCounterpartySelect && (
        <Container justifycontent="flex-end" className={spacing.pY24}>
          <LinkButton
            icon={<PlusIcon />}
            onClick={() => {
              setIsOpenedCounterpartySelect(true);
            }}
          >
            Add Counterparty
          </LinkButton>
        </Container>
      )}
      {isOpenedCounterpartySelect && (
        <Container
          justifycontent="space-between"
          alignitems="center"
          borderRadius="20"
          background={'white-100'}
          className={classNames(spacing.p24, spacing.mt24)}
          flexgrow="1"
          gap="16"
        >
          <Container gap="28" alignitems="center">
            <Text color="grey-100" type="body-regular-14">
              Choose counterparty
            </Text>
            <Select
              label="Counterparty"
              width="502"
              placeholder="Select option"
              type="baseWithCategory"
            >
              {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
                <SelectDropdown selectElement={selectElement}>
                  <SelectDropdownBody>
                    <Search
                      className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                      width="full"
                      height="40"
                      name="search-counterparty"
                      placeholder="Search option"
                      value={searchValue}
                      onChange={(e) => setSearchValue(e.target.value)}
                      searchParams={[]}
                      onSearch={() => {}}
                      onClear={() => {}}
                    />
                    <SelectDropdownItemList>
                      {options.map((value, index) => (
                        <OptionWithFourLabels
                          key={index}
                          label={value?.label}
                          secondaryLabel={value.subLabel}
                          tertiaryLabel={value.tertiaryLabel}
                          selected={selectValue === value}
                          onClick={() => {
                            onOptionClick(value);
                            closeDropdown();
                          }}
                        />
                      ))}
                    </SelectDropdownItemList>
                  </SelectDropdownBody>
                </SelectDropdown>
              )}
            </Select>
          </Container>
          <Container alignitems="center">
            <Divider type="vertical" height="26" />
            <IconButton
              icon={<TrashIcon />}
              background={'transparent'}
              color="grey-90-red-90"
              onClick={() => {}}
            />
          </Container>
        </Container>
      )}
    </Modal>
  );
};
