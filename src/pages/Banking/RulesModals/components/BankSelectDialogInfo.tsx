import { Text } from 'Components/base/typography/Text';
import React from 'react';
import { ReactElement } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

const BankSelectDialogInfo = (): ReactElement => (
  <Text type="caption-regular-height-24" color="grey-100" className={spacing.w420}>
    When a single bank account is added to the condition, the available split options are: Direct,
    Intracompany, and Intercompany. However, when multiple bank accounts are added, the split
    options are limited to Direct and Intercompany only.
  </Text>
);

export default BankSelectDialogInfo;
