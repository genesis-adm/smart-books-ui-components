import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { IconButton } from 'Components/base/buttons/IconButton';
import { NumericInput } from 'Components/base/inputs/NumericInput';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { Search } from 'Components/inputs/Search';
import { Select } from 'Components/inputs/Select';
import { OptionWithSingleLabel } from 'Components/inputs/Select/options/OptionWithSingleLabel';
import { TextInput } from 'Components/inputs/TextInput';
import { currencyOptions } from 'Mocks/fakeOptions';
import {
  amountSelectOptions,
  bankAccountTypeSelectOptions,
  conditionsOptions,
  currencySelectOptions,
  descriptionsSelectOptions,
} from 'Pages/Banking/RulesModals/constants';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

export const TableRowExamples = () => {
  const [searchValue, setSearchValue] = useState('');
  const [inputValue, setInputValue] = useState<number>(0);
  const handleChange = ({ target: { value } }: React.ChangeEvent<HTMLInputElement>) => {
    setInputValue(+value);
  };

  return (
    <>
      <TableRowNew gap="8">
        <TableCellNew minWidth="260">
          <Select
            label="Choose option"
            placeholder="Choose option"
            required
            height="36"
            width="230"
            value={conditionsOptions[0]}
          >
            {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
              <SelectDropdown selectElement={selectElement}>
                <SelectDropdownBody>
                  <Search
                    className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                    width="full"
                    height="40"
                    name="search"
                    placeholder="Search option"
                    value={searchValue}
                    onChange={(e) => setSearchValue(e.target.value)}
                    searchParams={[]}
                    onSearch={() => {}}
                    onClear={() => {}}
                  />
                  <SelectDropdownItemList>
                    {conditionsOptions.map((value, index) => (
                      <OptionWithSingleLabel
                        id={value.label}
                        key={index}
                        label={value?.label}
                        selected={!Array.isArray(selectValue) && selectValue?.value === value.value}
                        onClick={() => {
                          onOptionClick({
                            value: value?.value,
                            label: value?.label,
                          });
                          closeDropdown();
                        }}
                      />
                    ))}
                  </SelectDropdownItemList>
                </SelectDropdownBody>
              </SelectDropdown>
            )}
          </Select>
        </TableCellNew>
        <TableCellNew minWidth="260">
          <Select
            label="Choose option"
            placeholder="Choose option"
            required
            height="36"
            width="230"
          >
            {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
              <SelectDropdown selectElement={selectElement}>
                <SelectDropdownBody>
                  <Search
                    className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                    width="full"
                    height="40"
                    name="search"
                    placeholder="Search option"
                    value={searchValue}
                    onChange={(e) => setSearchValue(e.target.value)}
                    searchParams={[]}
                    onSearch={() => {}}
                    onClear={() => {}}
                  />
                  <SelectDropdownItemList>
                    {descriptionsSelectOptions.map((value, index) => (
                      <OptionWithSingleLabel
                        id={value.label}
                        key={index}
                        label={value?.label}
                        selected={!Array.isArray(selectValue) && selectValue?.value === value.value}
                        onClick={() => {
                          onOptionClick({
                            value: value?.value,
                            label: value?.label,
                          });
                          closeDropdown();
                        }}
                      />
                    ))}
                  </SelectDropdownItemList>
                </SelectDropdownBody>
              </SelectDropdown>
            )}
          </Select>
        </TableCellNew>
        <TableCellNew minWidth="260">
          <TextInput
            height="36"
            width="full"
            name="text"
            placeholder="Enter Text"
            required
            onChange={() => {}}
            label="Enter Text"
          />
        </TableCellNew>
        <TableCellNew minWidth="28" justifycontent="center" alignitems="center" padding="0">
          <IconButton
            icon={<TrashIcon />}
            background="transparent"
            color="grey-90-red-90"
            onClick={() => {}}
          />
        </TableCellNew>
      </TableRowNew>
      <TableRowNew gap="8" background="focusActive">
        <TableCellNew minWidth="260">
          <Select
            label="Choose option"
            placeholder="Choose option"
            required
            height="36"
            width="230"
            value={conditionsOptions[1]}
          >
            {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
              <SelectDropdown selectElement={selectElement}>
                <SelectDropdownBody>
                  <Search
                    className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                    width="full"
                    height="40"
                    name="search"
                    placeholder="Search option"
                    value={searchValue}
                    onChange={(e) => setSearchValue(e.target.value)}
                    searchParams={[]}
                    onSearch={() => {}}
                    onClear={() => {}}
                  />
                  <SelectDropdownItemList>
                    {conditionsOptions.map((value, index) => (
                      <OptionWithSingleLabel
                        id={value.label}
                        key={index}
                        label={value?.label}
                        selected={!Array.isArray(selectValue) && selectValue?.value === value.value}
                        onClick={() => {
                          onOptionClick({
                            value: value?.value,
                            label: value?.label,
                          });
                          closeDropdown();
                        }}
                      />
                    ))}
                  </SelectDropdownItemList>
                </SelectDropdownBody>
              </SelectDropdown>
            )}
          </Select>
        </TableCellNew>
        <TableCellNew minWidth="260">
          <Select
            label="Choose option"
            placeholder="Choose option"
            required
            height="36"
            width="230"
          >
            {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
              <SelectDropdown selectElement={selectElement}>
                <SelectDropdownBody>
                  <Search
                    className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                    width="full"
                    height="40"
                    name="search"
                    placeholder="Search option"
                    value={searchValue}
                    onChange={(e) => setSearchValue(e.target.value)}
                    searchParams={[]}
                    onSearch={() => {}}
                    onClear={() => {}}
                  />
                  <SelectDropdownItemList>
                    {currencySelectOptions.map((value, index) => (
                      <OptionWithSingleLabel
                        id={value.label}
                        key={index}
                        label={value?.label}
                        selected={!Array.isArray(selectValue) && selectValue?.value === value.value}
                        onClick={() => {
                          onOptionClick({
                            value: value?.value,
                            label: value?.label,
                          });
                          closeDropdown();
                        }}
                      />
                    ))}
                  </SelectDropdownItemList>
                </SelectDropdownBody>
              </SelectDropdown>
            )}
          </Select>
        </TableCellNew>
        <TableCellNew minWidth="260">
          <Select label="Choose Currency" placeholder="Choose Currency" height="36">
            {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
              <SelectDropdown selectElement={selectElement}>
                <SelectDropdownBody>
                  <Search
                    className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                    width="full"
                    height="40"
                    name="search"
                    placeholder="Search option"
                    value={searchValue}
                    onChange={(e) => setSearchValue(e.target.value)}
                    searchParams={[]}
                    onSearch={() => {}}
                    onClear={() => {}}
                  />
                  <SelectDropdownItemList>
                    {currencyOptions.map((value, index) => (
                      <OptionWithSingleLabel
                        id={value.label}
                        key={index}
                        label={`${value?.label} - ${value.fullLabel}`}
                        selected={!Array.isArray(selectValue) && selectValue?.value === value.value}
                        onClick={() => {
                          onOptionClick({
                            value: value?.value,
                            label: value?.label,
                          });
                          closeDropdown();
                        }}
                      />
                    ))}
                  </SelectDropdownItemList>
                </SelectDropdownBody>
              </SelectDropdown>
            )}
          </Select>
        </TableCellNew>
        <TableCellNew minWidth="28" justifycontent="center" alignitems="center" padding="0">
          <IconButton
            icon={<TrashIcon />}
            background="transparent"
            color="grey-90-red-90"
            onClick={() => {}}
          />
        </TableCellNew>
      </TableRowNew>
      <TableRowNew gap="8">
        <TableCellNew minWidth="260">
          <Select
            label="Choose option"
            placeholder="Choose option"
            required
            height="36"
            width="230"
            value={conditionsOptions[2]}
          >
            {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
              <SelectDropdown selectElement={selectElement}>
                <SelectDropdownBody>
                  <Search
                    className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                    width="full"
                    height="40"
                    name="search"
                    placeholder="Search option"
                    value={searchValue}
                    onChange={(e) => setSearchValue(e.target.value)}
                    searchParams={[]}
                    onSearch={() => {}}
                    onClear={() => {}}
                  />
                  <SelectDropdownItemList>
                    {conditionsOptions.map((value, index) => (
                      <OptionWithSingleLabel
                        id={value.label}
                        key={index}
                        label={value?.label}
                        selected={!Array.isArray(selectValue) && selectValue?.value === value.value}
                        onClick={() => {
                          onOptionClick({
                            value: value?.value,
                            label: value?.label,
                          });
                          closeDropdown();
                        }}
                      />
                    ))}
                  </SelectDropdownItemList>
                </SelectDropdownBody>
              </SelectDropdown>
            )}
          </Select>
        </TableCellNew>
        <TableCellNew minWidth="260">
          <Select
            label="Choose option"
            placeholder="Choose option"
            required
            height="36"
            width="230"
          >
            {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
              <SelectDropdown selectElement={selectElement}>
                <SelectDropdownBody>
                  <Search
                    className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                    width="full"
                    height="40"
                    name="search"
                    placeholder="Search option"
                    value={searchValue}
                    onChange={(e) => setSearchValue(e.target.value)}
                    searchParams={[]}
                    onSearch={() => {}}
                    onClear={() => {}}
                  />
                  <SelectDropdownItemList>
                    {currencySelectOptions.map((value, index) => (
                      <OptionWithSingleLabel
                        id={value.label}
                        key={index}
                        label={value?.label}
                        selected={!Array.isArray(selectValue) && selectValue?.value === value.value}
                        onClick={() => {
                          onOptionClick({
                            value: value?.value,
                            label: value?.label,
                          });
                          closeDropdown();
                        }}
                      />
                    ))}
                  </SelectDropdownItemList>
                </SelectDropdownBody>
              </SelectDropdown>
            )}
          </Select>
        </TableCellNew>
        <TableCellNew minWidth="260">
          <Select
            label="Choose Bank account type"
            placeholder="Choose Bank account type"
            height="36"
          >
            {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
              <SelectDropdown selectElement={selectElement}>
                <SelectDropdownBody>
                  <Search
                    className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                    width="full"
                    height="40"
                    name="search"
                    placeholder="Search option"
                    value={searchValue}
                    onChange={(e) => setSearchValue(e.target.value)}
                    searchParams={[]}
                    onSearch={() => {}}
                    onClear={() => {}}
                  />
                  <SelectDropdownItemList>
                    {bankAccountTypeSelectOptions.map((value, index) => (
                      <OptionWithSingleLabel
                        id={value.label}
                        key={index}
                        label={value?.label}
                        selected={!Array.isArray(selectValue) && selectValue?.value === value.value}
                        onClick={() => {
                          onOptionClick({
                            value: value?.value,
                            label: value?.label,
                          });
                          closeDropdown();
                        }}
                      />
                    ))}
                  </SelectDropdownItemList>
                </SelectDropdownBody>
              </SelectDropdown>
            )}
          </Select>
        </TableCellNew>
        <TableCellNew minWidth="28" justifycontent="center" alignitems="center" padding="0">
          <IconButton
            icon={<TrashIcon />}
            background="transparent"
            color="grey-90-red-90"
            onClick={() => {}}
          />
        </TableCellNew>
      </TableRowNew>
      <TableRowNew gap="8">
        <TableCellNew minWidth="260">
          <Select
            label="Choose option"
            placeholder="Choose option"
            required
            height="36"
            width="230"
            value={conditionsOptions[3]}
          >
            {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
              <SelectDropdown selectElement={selectElement}>
                <SelectDropdownBody>
                  <Search
                    className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                    width="full"
                    height="40"
                    name="search"
                    placeholder="Search option"
                    value={searchValue}
                    onChange={(e) => setSearchValue(e.target.value)}
                    searchParams={[]}
                    onSearch={() => {}}
                    onClear={() => {}}
                  />
                  <SelectDropdownItemList>
                    {conditionsOptions.map((value, index) => (
                      <OptionWithSingleLabel
                        id={value.label}
                        key={index}
                        label={value?.label}
                        selected={!Array.isArray(selectValue) && selectValue?.value === value.value}
                        onClick={() => {
                          onOptionClick({
                            value: value?.value,
                            label: value?.label,
                          });
                          closeDropdown();
                        }}
                      />
                    ))}
                  </SelectDropdownItemList>
                </SelectDropdownBody>
              </SelectDropdown>
            )}
          </Select>
        </TableCellNew>
        <TableCellNew minWidth="260">
          <Select
            label="Choose option"
            placeholder="Choose option"
            required
            height="36"
            width="230"
          >
            {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
              <SelectDropdown selectElement={selectElement}>
                <SelectDropdownBody>
                  <Search
                    className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                    width="full"
                    height="40"
                    name="search"
                    placeholder="Search option"
                    value={searchValue}
                    onChange={(e) => setSearchValue(e.target.value)}
                    searchParams={[]}
                    onSearch={() => {}}
                    onClear={() => {}}
                  />
                  <SelectDropdownItemList>
                    {amountSelectOptions.map((value, index) => (
                      <OptionWithSingleLabel
                        id={value.label}
                        key={index}
                        label={value?.label}
                        selected={!Array.isArray(selectValue) && selectValue?.value === value.value}
                        onClick={() => {
                          onOptionClick({
                            value: value?.value,
                            label: value?.label,
                          });
                          closeDropdown();
                        }}
                      />
                    ))}
                  </SelectDropdownItemList>
                </SelectDropdownBody>
              </SelectDropdown>
            )}
          </Select>
        </TableCellNew>
        <TableCellNew minWidth="260">
          <NumericInput
            name="num-1"
            label="Amount"
            required
            height="36"
            value={inputValue}
            onChange={handleChange}
          />
        </TableCellNew>
        <TableCellNew minWidth="28" justifycontent="center" alignitems="center" padding="0">
          <IconButton
            icon={<TrashIcon />}
            background="transparent"
            color="grey-90-red-90"
            onClick={() => {}}
          />
        </TableCellNew>
      </TableRowNew>
    </>
  );
};
