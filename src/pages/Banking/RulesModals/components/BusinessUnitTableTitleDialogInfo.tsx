import { Divider } from 'Components/base/Divider';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import React from 'react';
import { ReactElement } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

const BusinessUnitTableTitleDialogInfo = (): ReactElement => (
  <Container gap="16" flexdirection="column">
    <Container gap="4">
      <Text color={'grey-100'} type="caption-regular-height-24">
        <Text color="grey-100" type="caption-bold">
          Direct split
        </Text>
        - under the direct split, only business unit owners of the selected bank accounts will be
        displayed. For example, if a user selects Bank Account #1, owned by Legal Entity (LE) #1 and
        Business Unit (BU) #2, and Bank account #2, owned by Legal Entity (LE) #1 and Business Unit
        (BU) #3, the business unit dropdown will display BU#1 and BU#2 as "Direct". For journal
        entry creation, the system will use the chart of accounts linked to the template account
        selected by the user, where the legal entity and business unit match those of the bank
        account where booked bank transaction.
      </Text>
    </Container>
    <Divider fullHorizontalWidth />
    <Container gap="4">
      <Text color={'grey-100'} type="caption-regular-height-24">
        <Text color="grey-100" type="caption-bold">
          Intracompany split
        </Text>
        - Under the intracompany split, only business units with established relationships to the
        legal entities owning the selected bank account will be displayed. For example, if a user
        selects Bank Account #1, owned by Legal Entity (LE) #1 and Business Unit (BU) #2, and LE #1
        has relationships with BU#1, BU#2, and BU#3, the business unit dropdown will display BU#1 as
        "Direct" and BU#2 and BU#3 as "Intracompany." For journal entry creation, the system will
        generate three records: two records for recognizing intracompany receivables and payables
        between the bank account owner and the selected business unit, and one journal entry based
        on the chart of accounts linked to the template account chosen by the user, where the legal
        entity matches that to bank account legal entity and the business unit corresponds to the
        one selected under the intracompany split.
      </Text>
    </Container>
    <Divider fullHorizontalWidth />
    <Container gap="12" flexdirection="column">
      <Text color="grey-100" type="caption-regular-height-24">
        <Text color="grey-100" type="caption-bold">
          Intercompany split
        </Text>
        - Under the intercompany split, all other business structures of the organization, which are
        a combination of legal entities and business units, will be displayed. When creating a
        journal entry, selecting the intercompany split will trigger the automatic creation of two
        entries:
      </Text>
      <Container justifycontent="center" gap="4" className={spacing.pl16}>
        <Text color="grey-100" className={spacing.mt4}>
          &bull;
        </Text>
        <Text color="grey-100" type="caption-regular-height-24">
          The first journal entry will record the intercompany receivables (for outflow bank
          transactions) or intercompany payables (for inflow bank transactions) from the perspective
          of the bank account owner.
        </Text>
      </Container>
      <Container justifycontent="center" gap="4" className={spacing.pl16}>
        <Text color="grey-100" className={spacing.mt4}>
          &bull;
        </Text>
        <Text color="grey-100" type="caption-regular-height-24">
          The second journal entry is automatically generated for the legal entity and business unit
          selected by the user as part of the intercompany split. This entry will recognize the
          contra intercompany receivables (for inflow bank transactions) or intercompany payables
          (for outflow bank transactions). The entry is also booked according to the nature of the
          transaction, with accounts linked to the chart of accounts based on the template account
          chosen by the user during the split process
        </Text>
      </Container>
    </Container>
  </Container>
);

export default BusinessUnitTableTitleDialogInfo;
