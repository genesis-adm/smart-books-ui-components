import { DropDownItem } from 'Components/DropDownItem';
import { Checkbox } from 'Components/base/Checkbox';
import { Divider } from 'Components/base/Divider';
import { Radio } from 'Components/base/Radio';
import { Switch } from 'Components/base/Switch';
import { TooltipIcon } from 'Components/base/TooltipIcon';
import { Button } from 'Components/base/buttons/Button';
import { ButtonGroup } from 'Components/base/buttons/ButtonGroup';
import { IconButton } from 'Components/base/buttons/IconButton';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { InputMessage } from 'Components/base/inputs/InputElements/InputMessage';
import { SelectNew } from 'Components/base/inputs/SelectNew';
import { OptionDoubleHorizontalNew } from 'Components/base/inputs/SelectNew/options/OptionDoubleHorizontalNew';
import { OptionSingleNew } from 'Components/base/inputs/SelectNew/options/OptionSingleNew';
import { Text } from 'Components/base/typography/Text';
import { RuleSplit } from 'Components/custom/Banking/RuleSplit';
import { CheckboxOption } from 'Components/elements/CheckboxOption';
import { InputAutocomplete } from 'Components/elements/InputAutocomplete';
import { SplitContainer } from 'Components/elements/SplitContainer';
import { Modal } from 'Components/modules/Modal';
import ModalFooter from 'Components/modules/Modal/ModalFooter';
import ModalHeader from 'Components/modules/Modal/ModalHeader';
import { Input } from 'Components/old/Input';
import { options } from 'Mocks/fakeOptions';
import { ReactComponent as QuestionIcon } from 'Svg/16/question.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Banking/Rules Modals',
};

export const CreateRuleNew: React.FC = () => (
  <Modal
    centered
    size="md-medium"
    header={<ModalHeader titlePosition="center" title="Create rule" />}
    classNameModalBody={spacing.mY32}
    footer={
      <ModalFooter justifycontent="center">
        <ButtonGroup align="center">
          <Button
            width="md"
            background="white-100"
            color="black-100"
            border="grey-20-grey-90"
            onClick={() => {}}
          >
            Cancel
          </Button>
          <Button width="md" onClick={() => {}}>
            Create
          </Button>
        </ButtonGroup>
      </ModalFooter>
    }
  >
    <Container flexdirection="column" className={spacing.w100p}>
      <Input
        width="full"
        type="text"
        name="rulename"
        placeholder="Enter name"
        maxLength={100}
        label="Rule name"
        isRequired
      />
      <Container className={spacing.mt24} alignitems="center">
        <Switch id="autoconfirm" checked onChange={() => {}} />
        <Text className={spacing.mX12} type="body-regular-14" color="black-100">
          Automatically confirm transaction
        </Text>
        <TooltipIcon
          icon={<QuestionIcon />}
          size="16"
          message={
            <>
              <Text display="block" color="white-100">
                • “On” - transactions processed by the rule will be assigned with status “Accepted”
              </Text>
              <Text display="block" color="white-100">
                • “Off” - transactions processed by the rule will be assigned with status “Rule
                processing”. You have to accept transaction.
              </Text>
            </>
          }
        />
      </Container>
      <Container className={spacing.mt8} alignitems="center">
        <Switch id="applyrulepaidlog" checked={false} onChange={() => {}} />
        <Text className={spacing.mX12} type="body-regular-14" color="black-100">
          Apply rule for transaction linked to paidlog
        </Text>
        <TooltipIcon
          icon={<QuestionIcon />}
          size="16"
          message={
            <>
              <Text display="block" color="white-100">
                • “On” - if bank transaction linked to paidlog Chart of accounts, Business division
                will be assigned to transaction from the RULE
              </Text>
              <Text display="block" color="white-100">
                • “Off” - if bank transaction linked to paidlog Chart of accounts, Business division
                will be assigned to transaction from the PAIDLOG
              </Text>
            </>
          }
        />
      </Container>
    </Container>
    <Text type="body-medium" className={spacing.mt24}>
      Apply this rule to transaction that are
    </Text>
    <Container className={spacing.mt16} justifycontent="space-between" alignitems="center">
      <SelectNew
        name="legalentity"
        onChange={() => {}}
        placeholder="Choose legal entity"
        label="Legal entity"
        required
        width="260"
      >
        {({ onClick, state }) => (
          <>
            {options.map(({ label, value }) => (
              <OptionSingleNew
                key={value}
                label={label}
                selected={state?.value === value}
                onClick={() => onClick({ label, value })}
              />
            ))}
          </>
        )}
      </SelectNew>
      <SelectNew
        name="wheremoney"
        onChange={() => {}}
        placeholder="Choose transaction type"
        label="Where money"
        required
        width="260"
      >
        {({ onClick, state }) => (
          <>
            {options.map(({ label, value }) => (
              <OptionSingleNew
                key={value}
                label={label}
                selected={state?.value === value}
                onClick={() => onClick({ label, value })}
              />
            ))}
          </>
        )}
      </SelectNew>
    </Container>
    <SelectNew
      name="inbankaccount"
      className={spacing.mt16}
      width="full"
      onChange={() => {}}
      placeholder="Choose bank account"
      label="In bank account"
      required
    >
      {({ onClick, state }) => (
        <>
          <Container
            className={classNames(spacing.mX16, spacing.mY16)}
            justifycontent="space-between"
          >
            <Text type="caption-semibold" color="grey-100">
              Found 234 bank accounts:
            </Text>
            <LinkButton color="black-100-violet-90" type="caption-semibold" onClick={() => {}}>
              Clear (1)
            </LinkButton>
          </Container>
          <Checkbox option name="allbankaccounts" checked={false} onChange={() => {}}>
            <CheckboxOption checked={false} label="All bank accounts" />
          </Checkbox>
          {options.map(({ label, secondaryLabel, tertiaryLabel, quadroLabel, value }) => (
            <Checkbox
              option
              key={value}
              name={value}
              checked={state?.value === value}
              onChange={() => onClick({ label: `${label} | ${tertiaryLabel}`, value })}
            >
              <CheckboxOption
                checked={false}
                blurred={false}
                label={label}
                secondaryLabel={secondaryLabel}
                tertiaryLabel={tertiaryLabel}
                quadroLabel={quadroLabel}
              />
            </Checkbox>
          ))}
        </>
      )}
    </SelectNew>
    <Container className={spacing.mt24}>
      <Text type="body-medium">If the folowing exist for</Text>
      <Radio className={spacing.ml16} id="and-1" onChange={() => {}} name="condition-1" checked>
        All
      </Radio>
      <Radio
        className={spacing.ml16}
        id="or-1"
        onChange={() => {}}
        name="condition-1"
        checked={false}
      >
        Any
      </Radio>
    </Container>
    <Container alignitems="center" className={spacing.mt16}>
      <Text type="body-regular-14" color="grey-100">
        Rule condition 1
      </Text>
      <Divider type="horizontal" position="both" />
      <IconButton
        size="minimal"
        icon={<TrashIcon />}
        color="grey-100-red-90"
        background="transparent"
        onClick={() => {}}
      />
    </Container>
    <Container className={spacing.mt16} justifycontent="space-between" alignitems="center">
      <SelectNew
        name="option3"
        onChange={() => {}}
        placeholder="Choose option"
        label="Choose option"
        width="260"
      >
        {({ onClick, state }) => (
          <>
            {options.map(({ label, value }) => (
              <OptionSingleNew
                key={value}
                label={label}
                selected={state?.value === value}
                onClick={() => onClick({ label, value })}
              />
            ))}
          </>
        )}
      </SelectNew>
      <SelectNew
        name="option4"
        label="Choose option"
        onChange={() => {}}
        placeholder="Choose option"
        width="260"
      >
        {({ onClick, state }) => (
          <>
            {options.map(({ label, value }) => (
              <OptionSingleNew
                key={value}
                label={label}
                selected={state?.value === value}
                onClick={() => onClick({ label, value })}
              />
            ))}
          </>
        )}
      </SelectNew>
    </Container>
    <Input className={spacing.mt16} width="full" name="rule-condition-1" placeholder="Enter text" />
    <Container alignitems="center" className={spacing.mt16}>
      <Text type="body-regular-14" color="grey-100">
        Rule condition 2
      </Text>
      <Divider type="horizontal" position="both" />
      <IconButton
        size="minimal"
        icon={<TrashIcon />}
        color="grey-100-red-90"
        background="transparent"
        onClick={() => {}}
      />
    </Container>
    <Container className={spacing.mt16} justifycontent="space-between" alignitems="center">
      <SelectNew
        name="option"
        onChange={() => {}}
        label="Choose option"
        placeholder="Choose option"
        width="260"
      >
        {({ onClick, state }) => (
          <>
            {options.map(({ label, value }) => (
              <OptionSingleNew
                key={value}
                label={label}
                selected={state?.value === value}
                onClick={() => onClick({ label, value })}
              />
            ))}
          </>
        )}
      </SelectNew>
      <SelectNew
        name="option2"
        onChange={() => {}}
        label="Choose option"
        placeholder="Choose option"
        width="260"
      >
        {({ onClick, state }) => (
          <>
            {options.map(({ label, value }) => (
              <OptionSingleNew
                key={value}
                label={label}
                selected={state?.value === value}
                onClick={() => onClick({ label, value })}
              />
            ))}
          </>
        )}
      </SelectNew>
    </Container>
    <Input
      className={spacing.mt16}
      width="full"
      name="rule-condition-2"
      align="right"
      placeholder="Enter amount"
    />
    <Container alignitems="center" className={spacing.mt16}>
      <Text type="body-regular-14" color="grey-100">
        Rule condition 3
      </Text>
      <Divider type="horizontal" position="both" />
      <IconButton
        size="minimal"
        icon={<TrashIcon />}
        color="grey-100-red-90"
        background="transparent"
        onClick={() => {}}
      />
    </Container>
    <Container className={spacing.mt16} justifycontent="space-between" alignitems="center">
      <SelectNew
        name="option6"
        onChange={() => {}}
        label="Choose option"
        placeholder="Choose option"
        width="260"
      >
        {({ onClick, state }) => (
          <>
            {options.map(({ label, value }) => (
              <OptionSingleNew
                key={value}
                label={label}
                selected={state?.value === value}
                onClick={() => onClick({ label, value })}
              />
            ))}
          </>
        )}
      </SelectNew>
      <SelectNew
        name="option7"
        onChange={() => {}}
        label="Choose option"
        placeholder="Choose option"
        width="260"
      >
        {({ onClick, state }) => (
          <>
            {options.map(({ label, value }) => (
              <OptionSingleNew
                key={value}
                label={label}
                selected={state?.value === value}
                onClick={() => onClick({ label, value })}
              />
            ))}
          </>
        )}
      </SelectNew>
    </Container>
    <SelectNew
      name="currency"
      className={spacing.mt16}
      width="full"
      onChange={() => {}}
      label="Choose currency"
      placeholder="Choose currency"
    >
      {({ onClick, state }) => (
        <>
          {options.map(({ label, value }) => (
            <OptionSingleNew
              key={value}
              label={label}
              selected={state?.value === value}
              onClick={() => onClick({ label, value })}
            />
          ))}
        </>
      )}
    </SelectNew>
    <Divider fullHorizontalWidth className={spacing.mt16} />
    <Button
      className={spacing.mt16}
      width="full"
      background="grey-10-blue-10"
      color="violet-90-violet-100"
      iconLeft={<PlusIcon />}
      border="violet-90-violet-100"
      dashed="always"
      onClick={() => {}}
    >
      Add condition
    </Button>
    <Divider fullHorizontalWidth className={spacing.mt16} />
    <Text type="body-medium" className={spacing.mt24}>
      Then assign
    </Text>
    <SplitContainer
      splitType="percent"
      className={spacing.mt16}
      onDelete={() => {}}
      splitCurrentNumber={1}
      splitTotalCount={1}
      splitTotalSum={100}
    >
      <InputAutocomplete
        className={spacing.mt8}
        width="full"
        insideRules
        input={
          <Input
            name="chartofaccount"
            autoComplete="off"
            width="full"
            label="Choose account"
            isRequired
            placeholder="Please choose account"
            onChange={() => {}}
          />
        }
      >
        {/* {suggestions.map((item) => (
          <InputAutocompleteItemDoubleHorizontal
            label={item.bankname}
            secondaryLabel="Expenses"
            key={item.id}
            onClick={() => {}}
          />
        ))} */}
        {/* {isOpen && <InputAutocompleteText label="Please enter 2 or more characters" />} */}
      </InputAutocomplete>
      <RuleSplit type="division" showDeleteButton>
        <SelectNew
          onChange={() => {}}
          name="businessdivision"
          placeholder="Select option"
          width="300"
          label="Business division"
          required
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, secondaryLabel, value }) => (
                <OptionDoubleHorizontalNew
                  key={value}
                  label={label}
                  secondaryLabel={secondaryLabel}
                  selected={state?.value === value}
                  onClick={() => onClick({ label, value })}
                />
              ))}
            </>
          )}
        </SelectNew>
        <Input width="rules" name="split-1" placeholder="%" label="%" align="right" />
      </RuleSplit>
      {/* <RuleSplit name="split-1" showDeleteButton type="division" onChange={() => {}}> */}
      {/*  {options.map((item) => ( */}
      {/*    <OptionDoubleHorizontal */}
      {/*      key={item.value} */}
      {/*      value={item.value} */}
      {/*      label={item.label} */}
      {/*      secondaryLabel={item.secondaryLabel} */}
      {/*    /> */}
      {/*  ))} */}
      {/* </RuleSplit> */}
      {/* <RuleSplit name="split-2" showDeleteButton type="division" onChange={() => {}}> */}
      {/*  {options.map((item) => ( */}
      {/*    <OptionDoubleVertical */}
      {/*      key={item.value} */}
      {/*      value={item.value} */}
      {/*      label={item.label} */}
      {/*      secondaryLabel={item.secondaryLabel} */}
      {/*    /> */}
      {/*  ))} */}
      {/* </RuleSplit> */}
      <Button
        className={classNames(spacing.ml16, spacing.mt16)}
        width="lg"
        height="small"
        background="transparent"
        border="grey-20-grey-90"
        color="violet-90-violet-100"
        onClick={() => {}}
        iconLeft={<PlusIcon />}
        dropdown={
          <>
            <DropDownItem type="option" width="240" icon={<PlusIcon />} onClick={() => {}}>
              Add counterparty
            </DropDownItem>
            <DropDownItem type="option" width="240" icon={<PlusIcon />} onClick={() => {}}>
              Add third party
            </DropDownItem>
          </>
        }
        dropdownBorder="grey-20-grey-90"
      >
        Add business division
      </Button>
    </SplitContainer>
    <Divider fullHorizontalWidth className={spacing.mt16} />
    <Button
      className={spacing.mt16}
      width="full"
      background="grey-10-blue-10"
      color="violet-90-violet-100"
      iconLeft={<PlusIcon />}
      border="violet-90-violet-100"
      dashed="always"
      onClick={() => {}}
    >
      Add new split
    </Button>
    <Divider fullHorizontalWidth className={spacing.mt16} />
    <Container justifycontent="space-between" className={spacing.mt24}>
      <Text>Split amount</Text>
      <Text>%&nbsp;0.00</Text>
    </Container>
    <Container justifycontent="space-between" className={spacing.mt16}>
      <Text>Original amount</Text>
      <Text>%&nbsp;100.00</Text>
    </Container>
    <Container justifycontent="space-between" className={spacing.mt16}>
      <Text>Difference</Text>
      <Text color="red-90">%&nbsp;-100.00</Text>
    </Container>
    <InputMessage type="error" text="Split percentages must add up exactly to 100%" />
    <Divider fullHorizontalWidth className={spacing.mt16} />
    <SelectNew
      name="payee"
      className={spacing.mt16}
      width="full"
      onChange={() => {}}
      placeholder="Choose payee"
      label="Payee"
      required
    >
      {({ onClick, state }) => (
        <>
          {options.map(({ label, secondaryLabel, value }) => (
            <OptionDoubleHorizontalNew
              key={value}
              label={label}
              secondaryLabel={secondaryLabel}
              selected={state?.value === value}
              onClick={() => onClick({ label, value })}
            />
          ))}
        </>
      )}
    </SelectNew>
  </Modal>
);
