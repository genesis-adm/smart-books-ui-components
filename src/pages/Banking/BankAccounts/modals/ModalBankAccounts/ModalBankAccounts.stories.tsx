import { Meta } from '@storybook/react';
import { DropDown } from 'Components/DropDown';
import BackdropWrapper from 'Components/base/BackdropWrapper/BackdropWrapper';
import { Divider } from 'Components/base/Divider';
import { Icon } from 'Components/base/Icon';
import { Radio } from 'Components/base/Radio';
import { Tag } from 'Components/base/Tag';
import { Button } from 'Components/base/buttons/Button';
import { ButtonGroup } from 'Components/base/buttons/ButtonGroup';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { InputDate } from 'Components/base/inputs/InputDate';
import { InputNew } from 'Components/base/inputs/InputNew';
import { NumericInput } from 'Components/base/inputs/NumericInput';
import { OwnNumberFormatValues } from 'Components/base/inputs/NumericInput/NumericInput.types';
import { SelectNew } from 'Components/base/inputs/SelectNew';
import { OptionSingleNew } from 'Components/base/inputs/SelectNew/options/OptionSingleNew';
import { TextareaNew } from 'Components/base/inputs/TextareaNew';
import { Text } from 'Components/base/typography/Text';
import { Currencies } from 'Components/elements/Currencies';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { options } from 'Mocks/fakeOptions';
import { ReactComponent as BankIcon } from 'Svg/v2/16/bank.svg';
import { ReactComponent as CreditCardIcon } from 'Svg/v2/16/credit-card.svg';
import { ReactComponent as DisableEyeIcon } from 'Svg/v2/16/eye-crossed.svg';
import { ReactComponent as DocumentIcon } from 'Svg/v2/16/file-blank.svg';
import { ReactComponent as InfoIcon } from 'Svg/v2/16/info-in-circle.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as DropdownIcon } from 'Svg/v2/16/three-dots.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import { ReactComponent as WalletIcon } from 'Svg/v2/16/wallet.svg';
import { ReactComponent as IBANNumberTooltip } from 'Svg/v2/tooltips/iban-number.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Banking/Bank Accounts/Modals/Bank Accounts',
  argTypes: {
    editingBalance: {
      control: { type: 'boolean' },
    },
  },
  args: {
    editingBalance: false,
  },
} as Meta;

export const CreateNewCard: React.FC<{ editingBalance: boolean }> = ({ editingBalance }) => {
  const [customCurrencyValue, setCustomCurrencyValue] = useState<string>('1.031');
  const [inputCurrencyValue, setInputCurrencyValue] = useState<string>(customCurrencyValue);
  const [inputValue, setInputValue] = useState<number>();
  const [startDate, setStartDate] = useState<Date | null>(new Date());

  const handleChange = ({ floatValue }: OwnNumberFormatValues) => {
    setInputValue(floatValue);
  };

  return (
    <Modal
      size="720"
      background={'grey-10'}
      type="new"
      classNameModalBody={spacing.h100p}
      header={
        <ModalHeaderNew
          background={'grey-10'}
          title="Create new Card"
          tag={
            <Tag padding="2-8" color="green" icon={<DocumentIcon />} cursor="default" text="New" />
          }
          icon={<Icon icon={<CreditCardIcon />} />}
          border
          onClose={() => {}}
        />
      }
      footer={
        <ModalFooterNew border background={'grey-10'}>
          <Button width="md" onClick={() => {}}>
            Save
          </Button>
        </ModalFooterNew>
      }
    >
      <Container
        flexdirection="column"
        borderRadius="20"
        background={'white-100'}
        className={spacing.p24}
        flexgrow="1"
      >
        <BackdropWrapper isVisible={editingBalance}>
          <>
            <Text type="body-regular-14" color="grey-100">
              Please choose the Legal entity & Busines Division
            </Text>
            <Container
              justifycontent="space-between"
              className={classNames(spacing.w100p, spacing.mt16)}
            >
              <SelectNew
                name="businessDivision"
                required
                onChange={() => {}}
                width="300"
                label="Business division"
              >
                {({ onClick, state }) => (
                  <>
                    {options.map(({ label, value }) => (
                      <OptionSingleNew
                        key={value}
                        label={label}
                        selected={state?.value === value}
                        onClick={() =>
                          onClick({
                            label,
                            value,
                          })
                        }
                      />
                    ))}
                  </>
                )}
              </SelectNew>
              <SelectNew
                name="legalEntity"
                required
                onChange={() => {}}
                width="300"
                label="Legal entity"
              >
                {({ onClick, state }) => (
                  <>
                    {options.map(({ label, value }) => (
                      <OptionSingleNew
                        key={value}
                        label={label}
                        selected={state?.value === value}
                        onClick={() =>
                          onClick({
                            label,
                            value,
                          })
                        }
                      />
                    ))}
                  </>
                )}
              </SelectNew>
            </Container>
            <Divider className={spacing.mt24} fullHorizontalWidth />
            <Text className={spacing.mt24} type="body-regular-14" color="grey-100">
              Card information
            </Text>
            <Container gap="20" className={spacing.mt16}>
              <Radio
                id="iban"
                color="grey-100"
                type="caption-regular"
                onChange={() => {}}
                name="bankType"
                checked
              >
                IBAN
              </Radio>
              <Radio
                id="account"
                color="grey-100"
                type="caption-regular"
                onChange={() => {}}
                name="bankType"
                checked={false}
              >
                Account number
              </Radio>
            </Container>
            <Container justifycontent="space-between" gap="24" className={spacing.mt12}>
              <InputNew
                name="account"
                label="IBAN Account"
                width="full"
                placeholder="XXXX"
                icon={<InfoIcon />}
                elementPosition="left"
                required
                tooltip={<IBANNumberTooltip />}
                tooltipWidth="auto"
              />
              <SelectNew
                name="currency"
                onChange={() => {}}
                placeholder="Choose option"
                width="200"
                label="Currency"
                required
              >
                {({ onClick, state }) => (
                  <>
                    {options.map(({ label, value }) => (
                      <OptionSingleNew
                        key={value}
                        label={label}
                        selected={state?.value === value}
                        onClick={() =>
                          onClick({
                            label,
                            value,
                          })
                        }
                      />
                    ))}
                  </>
                )}
              </SelectNew>
            </Container>
            <SelectNew
              className={spacing.mt24}
              name="bank"
              onChange={() => {}}
              placeholder="Choose bank"
              width="full"
              label="Choose bank"
              required
            >
              {({ onClick, state }) => (
                <>
                  {options.map(({ label, value }) => (
                    <OptionSingleNew
                      key={value}
                      label={label}
                      selected={state?.value === value}
                      onClick={() =>
                        onClick({
                          label,
                          value,
                        })
                      }
                    />
                  ))}
                  <Divider type="horizontal" fullHorizontalWidth className={spacing.mY8} />
                  <Container justifycontent="flex-end" className={spacing.mr12}>
                    <LinkButton icon={<PlusIcon />} onClick={() => {}}>
                      Create new Bank
                    </LinkButton>
                  </Container>
                </>
              )}
            </SelectNew>
            <Container className={spacing.mt24} justifycontent="flex-end">
              <LinkButton icon={<PlusIcon />} onClick={() => {}}>
                Add opening balance
              </LinkButton>
            </Container>
            {/*  */}
            <Container
              isFocusedElm={editingBalance}
              flexdirection="column"
              gap="12"
              border="grey-20"
              borderRadius="10"
              className={classNames(spacing.pX24, spacing.pt20, spacing.pb24, spacing.mt24)}
            >
              <Container justifycontent="space-between" alignitems="center">
                <Text type="body-regular-14">Opening Balance</Text>
                <Container alignitems="center" gap="12">
                  <Currencies
                    background={'grey-10'}
                    selectedCurrency="eur"
                    selectedCurrencyValue="1"
                    baseCurrency="usd"
                    showCustomRate
                    baseCurrencyValue={customCurrencyValue}
                    baseCurrencyInputValue={inputCurrencyValue}
                    onChange={(e) => {
                      setInputCurrencyValue(e.target.value);
                    }}
                    onApply={() => setCustomCurrencyValue(inputCurrencyValue)}
                  />
                  <IconButton
                    size="16"
                    icon={<TrashIcon />}
                    background={'transparent'}
                    color="grey-90-red-90"
                    onClick={() => {}}
                  />
                </Container>
              </Container>
              <Container gap="16">
                <Radio
                  id="debit"
                  color="violet-90"
                  type="caption-regular"
                  onChange={() => {}}
                  name="openingBalanceRadio"
                  checked
                >
                  Debit
                </Radio>
                <Radio
                  id="credit"
                  color="grey-90"
                  type="caption-regular"
                  onChange={() => {}}
                  name="openingBalanceRadio"
                  checked={false}
                >
                  Credit
                </Radio>
              </Container>
              <Container className={spacing.mt4} justifycontent="space-between">
                <NumericInput
                  name="num-2"
                  width="280"
                  height="48"
                  label="Amount"
                  value={inputValue}
                  onValueChange={handleChange}
                />
                <InputDate
                  name="inputdate"
                  placeholder="dd.mm.yyyy"
                  label="Date"
                  width="280"
                  selected={startDate}
                  onChange={(date: Date) => setStartDate(date)}
                  calendarStartDay={1}
                />
              </Container>
              {editingBalance && (
                <Container justifycontent="flex-end">
                  <ButtonGroup align="right">
                    <Button
                      background={'white-100'}
                      color="grey-100-black-100"
                      border="grey-20"
                      width="88"
                      height="40"
                      onClick={() => {}}
                    >
                      Cancel
                    </Button>
                    <Button width="88" height="40" onClick={() => {}}>
                      Update
                    </Button>
                  </ButtonGroup>
                </Container>
              )}
            </Container>
            {/*  */}
            <Divider className={spacing.mt24} fullHorizontalWidth />
            <Container className={spacing.mt24} alignitems="center" justifycontent="space-between">
              <Text color="grey-100" type="body-regular-14">
                Cards
              </Text>
              <Button
                background={'blue-10-blue-20'}
                color="violet-90-violet-100"
                width="108"
                height="24"
                iconLeft={<PlusIcon />}
              >
                Add new
              </Button>
            </Container>
            <Container className={spacing.mt24} gap="24" justifycontent="space-between">
              <Container
                className={classNames(spacing.p16)}
                border="grey-20"
                borderRadius="8"
                gap="12"
                flexdirection="column"
              >
                <Container justifycontent="space-between" alignitems="center">
                  <Text type="caption-regular" color="grey-100">
                    Card status: None
                  </Text>
                  <DropDown
                    flexdirection="column"
                    padding="8"
                    gap="4"
                    control={({ handleOpen }) => (
                      <IconButton
                        icon={<DropdownIcon />}
                        size="16"
                        onClick={handleOpen}
                        background={'transparent'}
                        color="grey-100-violet-90"
                      />
                    )}
                  >
                    <DropDownButton size="160" icon={<DisableEyeIcon />} onClick={() => {}}>
                      Disable
                    </DropDownButton>
                    <Divider fullHorizontalWidth />
                    <DropDownButton
                      size="160"
                      type="danger"
                      icon={<TrashIcon />}
                      onClick={() => {}}
                    >
                      Delete
                    </DropDownButton>
                  </DropDown>
                </Container>
                <InputNew
                  name="cardNumber"
                  label="Card Number"
                  width="260"
                  height="36"
                  background={'grey-10'}
                  placeholder="Card Number"
                  icon={<CreditCardIcon />}
                  elementPosition="left"
                  required
                />
                <InputNew
                  name="cardHolder"
                  label="Card Holder"
                  width="full"
                  height="36"
                  background={'grey-10'}
                  placeholder="Card Holder"
                  required
                />
              </Container>
              <Container
                className={classNames(spacing.p16)}
                border="grey-20"
                borderRadius="8"
                gap="12"
                flexdirection="column"
              >
                <Container justifycontent="space-between" alignitems="center">
                  <Text type="caption-regular" color="grey-100">
                    Card status: None
                  </Text>
                  <DropDown
                    flexdirection="column"
                    padding="8"
                    gap="4"
                    control={({ handleOpen }) => (
                      <IconButton
                        icon={<DropdownIcon />}
                        size="16"
                        onClick={handleOpen}
                        background={'transparent'}
                        color="grey-100-violet-90"
                      />
                    )}
                  >
                    <DropDownButton size="160" icon={<DisableEyeIcon />} onClick={() => {}}>
                      Disable
                    </DropDownButton>
                    <Divider fullHorizontalWidth />
                    <DropDownButton
                      size="160"
                      type="danger"
                      icon={<TrashIcon />}
                      onClick={() => {}}
                    >
                      Delete
                    </DropDownButton>
                  </DropDown>
                </Container>
                <InputNew
                  name="cardNumber"
                  label="Card Number"
                  width="260"
                  height="36"
                  background={'grey-10'}
                  placeholder="Card Number"
                  icon={<CreditCardIcon />}
                  elementPosition="left"
                  required
                />
                <InputNew
                  name="cardHolder"
                  label="Card Holder"
                  width="full"
                  height="36"
                  background={'grey-10'}
                  placeholder="Card Holder"
                  required
                />
              </Container>
            </Container>
          </>
        </BackdropWrapper>
      </Container>
    </Modal>
  );
};

export const CreateNewBankAccount: React.FC<{ editingBalance: boolean }> = ({ editingBalance }) => {
  const [currentValue, setCurrentValue] = useState('');
  const [customCurrencyValue, setCustomCurrencyValue] = useState<string>('1.031');
  const [inputCurrencyValue, setInputCurrencyValue] = useState<string>(customCurrencyValue);
  const [inputValue, setInputValue] = useState<number>();
  const [startDate, setStartDate] = useState<Date | null>(new Date());

  const handleChange = ({ floatValue }: OwnNumberFormatValues) => {
    setInputValue(floatValue);
  };

  return (
    <Modal
      size="720"
      background={'grey-10'}
      type="new"
      classNameModalBody={spacing.h100p}
      header={
        <ModalHeaderNew
          background={'grey-10'}
          title="Create new Bank account"
          tag={
            <Tag padding="2-8" color="green" icon={<DocumentIcon />} cursor="default" text="New" />
          }
          icon={<Icon icon={<BankIcon />} />}
          border
          onClose={() => {}}
        />
      }
      footer={
        <ModalFooterNew border background={'grey-10'}>
          <Button width="md" onClick={() => {}}>
            Save
          </Button>
        </ModalFooterNew>
      }
    >
      <Container
        flexdirection="column"
        borderRadius="20"
        background={'white-100'}
        className={spacing.p24}
        flexgrow="1"
      >
        <BackdropWrapper isVisible={editingBalance}>
          <>
            <Text type="body-regular-14" color="grey-100">
              Please choose the Legal entity & Business Division
            </Text>
            <Container
              justifycontent="space-between"
              className={classNames(spacing.w100p, spacing.mt16)}
            >
              <SelectNew
                name="businessDivision"
                required
                onChange={() => {}}
                width="300"
                label="Business division"
              >
                {({ onClick, state }) => (
                  <>
                    {options.map(({ label, value }) => (
                      <OptionSingleNew
                        key={value}
                        label={label}
                        selected={state?.value === value}
                        onClick={() =>
                          onClick({
                            label,
                            value,
                          })
                        }
                      />
                    ))}
                  </>
                )}
              </SelectNew>
              <SelectNew
                name="legalEntity"
                required
                onChange={() => {}}
                width="300"
                label="Legal entity"
              >
                {({ onClick, state }) => (
                  <>
                    {options.map(({ label, value }) => (
                      <OptionSingleNew
                        key={value}
                        label={label}
                        selected={state?.value === value}
                        onClick={() =>
                          onClick({
                            label,
                            value,
                          })
                        }
                      />
                    ))}
                  </>
                )}
              </SelectNew>
            </Container>
            <Divider className={spacing.mt24} fullHorizontalWidth />
            <Container className={spacing.mt24} justifycontent="space-between" alignitems="center">
              <Text type="body-regular-14" color="grey-100">
                Bank account information
              </Text>
              <DropDown
                flexdirection="column"
                padding="8"
                gap="4"
                control={({ handleOpen }) => (
                  <IconButton
                    icon={<DropdownIcon />}
                    size="16"
                    onClick={handleOpen}
                    background={'transparent'}
                    color="grey-100-violet-90"
                  />
                )}
              >
                <DropDownButton size="160" icon={<DisableEyeIcon />} onClick={() => {}}>
                  Disable
                </DropDownButton>
                <Divider fullHorizontalWidth />
                <DropDownButton size="160" type="danger" icon={<TrashIcon />} onClick={() => {}}>
                  Delete
                </DropDownButton>
              </DropDown>
            </Container>
            <Text className={spacing.mt12} type="caption-regular" color="grey-100">
              Please choose type of bank id and account details:
            </Text>
            <Container gap="20" className={spacing.mt16}>
              <Radio
                id="iban"
                color="grey-100"
                type="caption-regular"
                onChange={() => {}}
                name="bankType"
                checked
              >
                IBAN
              </Radio>
              <Radio
                id="account"
                color="grey-100"
                type="caption-regular"
                onChange={() => {}}
                name="bankType"
                checked={false}
              >
                Account number
              </Radio>
            </Container>
            <Container justifycontent="space-between" gap="24" className={spacing.mt12}>
              <InputNew
                name="account"
                label="IBAN Account"
                width="full"
                placeholder="XXXX"
                icon={<InfoIcon />}
                elementPosition="left"
                tooltip={<IBANNumberTooltip />}
                tooltipWidth="auto"
              />
              <SelectNew
                name="currency"
                onChange={() => {}}
                placeholder="Choose option"
                width="200"
                label="Currency"
                required
              >
                {({ onClick, state }) => (
                  <>
                    {options.map(({ label, value }) => (
                      <OptionSingleNew
                        key={value}
                        label={label}
                        selected={state?.value === value}
                        onClick={() =>
                          onClick({
                            label,
                            value,
                          })
                        }
                      />
                    ))}
                  </>
                )}
              </SelectNew>
            </Container>
            <SelectNew
              className={spacing.mt24}
              name="bank"
              onChange={() => {}}
              placeholder="Choose bank"
              width="full"
              label="Choose bank"
              required
            >
              {({ onClick, state }) => (
                <>
                  {options.map(({ label, value }) => (
                    <OptionSingleNew
                      key={value}
                      label={label}
                      selected={state?.value === value}
                      onClick={() =>
                        onClick({
                          label,
                          value,
                        })
                      }
                    />
                  ))}
                  <Divider type="horizontal" fullHorizontalWidth className={spacing.mY8} />
                  <Container justifycontent="flex-end" className={spacing.mr12}>
                    <LinkButton icon={<PlusIcon />} onClick={() => {}}>
                      Create new Bank
                    </LinkButton>
                  </Container>
                </>
              )}
            </SelectNew>
            <TextareaNew
              className={spacing.mt24}
              name="notes"
              label="Notes"
              value={currentValue}
              onChange={(e) => setCurrentValue(e.target.value)}
            />
            <Container className={spacing.mt24} justifycontent="flex-end">
              <LinkButton icon={<PlusIcon />} onClick={() => {}}>
                Add opening balance
              </LinkButton>
            </Container>
            {/*  */}
            <Container
              isFocusedElm={editingBalance}
              flexdirection="column"
              gap="12"
              border="grey-20"
              borderRadius="10"
              className={classNames(spacing.pX24, spacing.pt20, spacing.pb24, spacing.mt24)}
            >
              <Container justifycontent="space-between" alignitems="center">
                <Text type="body-regular-14">Opening Balance</Text>
                <Container alignitems="center" gap="12">
                  <Currencies
                    background={'grey-10'}
                    selectedCurrency="eur"
                    selectedCurrencyValue="1"
                    baseCurrency="usd"
                    showCustomRate
                    baseCurrencyValue={customCurrencyValue}
                    baseCurrencyInputValue={inputCurrencyValue}
                    onChange={(e) => {
                      setInputCurrencyValue(e.target.value);
                    }}
                    onApply={() => setCustomCurrencyValue(inputCurrencyValue)}
                  />
                  <IconButton
                    size="16"
                    icon={<TrashIcon />}
                    background={'transparent'}
                    color="grey-90-red-90"
                    onClick={() => {}}
                  />
                </Container>
              </Container>
              <Container gap="16">
                <Radio
                  id="debit"
                  color="violet-90"
                  type="caption-regular"
                  onChange={() => {}}
                  name="openingBalanceRadio"
                  checked
                >
                  Debit
                </Radio>
                <Radio
                  id="credit"
                  color="grey-90"
                  type="caption-regular"
                  onChange={() => {}}
                  name="openingBalanceRadio"
                  checked={false}
                >
                  Credit
                </Radio>
              </Container>
              <Container className={spacing.mt4} justifycontent="space-between">
                <NumericInput
                  name="num-1"
                  width="280"
                  height="48"
                  label="Amount"
                  value={inputValue}
                  onValueChange={handleChange}
                />
                <InputDate
                  name="inputdate"
                  placeholder="dd.mm.yyyy"
                  label="Date"
                  width="280"
                  selected={startDate}
                  onChange={(date: Date) => setStartDate(date)}
                  calendarStartDay={1}
                />
              </Container>
              {editingBalance && (
                <Container justifycontent="flex-end">
                  <ButtonGroup align="right">
                    <Button
                      background={'white-100'}
                      color="grey-100-black-100"
                      border="grey-20"
                      width="88"
                      height="40"
                      onClick={() => {}}
                    >
                      Cancel
                    </Button>
                    <Button background={'grey-90'} width="88" height="40" onClick={() => {}}>
                      Save
                    </Button>
                  </ButtonGroup>
                </Container>
              )}
            </Container>
            {/*  */}
          </>
        </BackdropWrapper>
      </Container>
    </Modal>
  );
};

export const CreateNewWallet: React.FC = () => {
  const [currentValue, setCurrentValue] = useState('');

  return (
    <Modal
      size="720"
      background={'grey-10'}
      type="new"
      classNameModalBody={spacing.h100p}
      header={
        <ModalHeaderNew
          background={'grey-10'}
          title="Create new Wallet"
          tag={
            <Tag padding="2-8" color="green" icon={<DocumentIcon />} cursor="default" text="New" />
          }
          icon={<Icon icon={<WalletIcon />} />}
          border
          onClose={() => {}}
        />
      }
      footer={
        <ModalFooterNew border background={'grey-10'}>
          <Button width="md" onClick={() => {}}>
            Save
          </Button>
        </ModalFooterNew>
      }
    >
      <Container
        flexdirection="column"
        borderRadius="20"
        background={'white-100'}
        className={spacing.p24}
        flexgrow="1"
      >
        <Text type="body-regular-14" color="grey-100">
          Please choose the Legal entity & Business Division
        </Text>
        <Container
          justifycontent="space-between"
          className={classNames(spacing.w100p, spacing.mt16)}
        >
          <SelectNew
            name="businessDivision"
            required
            onChange={() => {}}
            width="300"
            label="Business division"
          >
            {({ onClick, state }) => (
              <>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={state?.value === value}
                    onClick={() =>
                      onClick({
                        label,
                        value,
                      })
                    }
                  />
                ))}
              </>
            )}
          </SelectNew>
          <SelectNew
            name="legalEntity"
            required
            onChange={() => {}}
            width="300"
            label="Legal entity"
          >
            {({ onClick, state }) => (
              <>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={state?.value === value}
                    onClick={() =>
                      onClick({
                        label,
                        value,
                      })
                    }
                  />
                ))}
              </>
            )}
          </SelectNew>
        </Container>
        <Divider className={spacing.mt24} fullHorizontalWidth />
        <Container className={spacing.mt24} justifycontent="space-between" alignitems="center">
          <Text type="body-regular-14" color="grey-100">
            Wallet information
          </Text>
          <DropDown
            flexdirection="column"
            padding="8"
            gap="4"
            control={({ handleOpen }) => (
              <IconButton
                icon={<DropdownIcon />}
                size="16"
                onClick={handleOpen}
                background={'transparent'}
                color="grey-100-violet-90"
              />
            )}
          >
            <DropDownButton size="160" icon={<DisableEyeIcon />} onClick={() => {}}>
              Disable
            </DropDownButton>
            <Divider fullHorizontalWidth />
            <DropDownButton size="160" type="danger" icon={<TrashIcon />} onClick={() => {}}>
              Delete
            </DropDownButton>
          </DropDown>
        </Container>
        <Container justifycontent="space-between" gap="24" className={spacing.mt12}>
          <InputNew
            name="wallet"
            label="Wallet name"
            width="full"
            placeholder="XXXX"
            icon={<WalletIcon />}
            elementPosition="left"
            required
            tooltipWidth="auto"
          />
          <SelectNew
            name="currency"
            onChange={() => {}}
            placeholder="Choose option"
            width="200"
            label="Currency"
            required
          >
            {({ onClick, state }) => (
              <>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={state?.value === value}
                    onClick={() =>
                      onClick({
                        label,
                        value,
                      })
                    }
                  />
                ))}
              </>
            )}
          </SelectNew>
        </Container>
        <InputNew
          className={spacing.mt24}
          name="wallet"
          label="Wallet number"
          width="full"
          placeholder="XXXX"
          required
          tooltipWidth="auto"
        />
        <TextareaNew
          className={spacing.mt24}
          name="notes"
          label="Notes"
          value={currentValue}
          onChange={(e) => setCurrentValue(e.target.value)}
        />
      </Container>
    </Modal>
  );
};
