import { Meta } from '@storybook/react';
import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { BackdropWrapper } from 'Components/base/BackdropWrapper';
import { Divider } from 'Components/base/Divider';
import { Icon } from 'Components/base/Icon';
import { Tag } from 'Components/base/Tag';
import { Button } from 'Components/base/buttons/Button';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { Search } from 'Components/inputs/Search';
import { Select } from 'Components/inputs/Select';
import { OptionWithSingleLabel } from 'Components/inputs/Select/options/OptionWithSingleLabel';
import { OptionWithTwoLabels } from 'Components/inputs/Select/options/OptionWithTwoLabels';
import { TextArea } from 'Components/inputs/TextArea';
import { TextInput } from 'Components/inputs/TextInput';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { currencyOptions, options } from 'Mocks/fakeOptions';
import OpeningBalance from 'Pages/Accounting/ChartOfAccountsModals/components/OpeningBalance';
import { ReactComponent as CashIcon } from 'Svg/v2/16/cash.svg';
import { ReactComponent as DocumentIcon } from 'Svg/v2/16/file-blank.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Banking/Bank Accounts/Modals/Bank Accounts',
} as Meta;

export const CreateNewCash: React.FC = () => {
  const [searchValue, setSearchValue] = useState('');
  const [notesValue, setNotesValue] = useState<string>('');
  const [nameValue, setNameValue] = useState<string>('');
  const [holderValue, setHolderValue] = useState<string>('');
  const [addingOpeningBalance, setAddingOpeningBalance] = useState<boolean>(false);
  const [newOpeningBalance, setNewOpeningBalance] = useState<boolean>(false);
  const [editingOpeningBalance, setEditingOpeningBalance] = useState<boolean>(false);
  return (
    <Modal
      size="720"
      background={'grey-10'}
      type="new"
      classNameModalBody={spacing.h100p}
      header={
        <ModalHeaderNew
          background={'grey-10'}
          title="Create new Cash"
          tag={
            <Tag padding="2-8" color="green" icon={<DocumentIcon />} cursor="default" text="New" />
          }
          icon={<Icon icon={<CashIcon />} />}
          border
          onClose={() => {}}
        />
      }
      footer={
        <ModalFooterNew border background={'grey-10'}>
          <Button width="md" onClick={() => {}}>
            Save
          </Button>
        </ModalFooterNew>
      }
    >
      <Container
        flexdirection="column"
        borderRadius="20"
        background={'white-100'}
        className={spacing.p24}
        flexgrow="1"
      >
        <BackdropWrapper isVisible={addingOpeningBalance && editingOpeningBalance}>
          <>
            <Text type="body-regular-14" color="grey-100">
              Please choose the Legal entity & Business Division
            </Text>
            <Container
              justifycontent="space-between"
              className={classNames(spacing.w100p, spacing.mt16)}
            >
              <Select label="Business unit" placeholder="Choose" type="column" width="300" required>
                {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
                  <SelectDropdown selectElement={selectElement} width="408">
                    <SelectDropdownBody>
                      <Search
                        className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                        width="full"
                        height="40"
                        name="search"
                        placeholder="Search option"
                        value={searchValue}
                        onChange={(e) => setSearchValue(e.target.value)}
                        searchParams={[]}
                        onSearch={() => {}}
                        onClear={() => {}}
                      />
                      <SelectDropdownItemList>
                        {options.map((value, index) => (
                          <OptionWithTwoLabels
                            id={value.label}
                            key={index}
                            tooltip
                            // labelDirection="horizontal"
                            label={value?.label}
                            secondaryLabel={value.subLabel}
                            selected={
                              !Array.isArray(selectValue) && selectValue?.value === value.value
                            }
                            onClick={() => {
                              onOptionClick({
                                value: value?.value,
                                label: value?.label,
                                subLabel: value?.subLabel,
                              });
                              closeDropdown();
                            }}
                          />
                        ))}
                      </SelectDropdownItemList>
                    </SelectDropdownBody>
                  </SelectDropdown>
                )}
              </Select>
              <Select label="Legal entity" placeholder="Choose" type="column" width="300" required>
                {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
                  <SelectDropdown selectElement={selectElement} width="408">
                    <SelectDropdownBody>
                      <Search
                        className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                        width="full"
                        height="40"
                        name="search"
                        placeholder="Search option"
                        value={searchValue}
                        onChange={(e) => setSearchValue(e.target.value)}
                        searchParams={[]}
                        onSearch={() => {}}
                        onClear={() => {}}
                      />
                      <SelectDropdownItemList>
                        {options.map((value, index) => (
                          <OptionWithSingleLabel
                            id={value.label}
                            key={index}
                            label={value?.label}
                            selected={
                              !Array.isArray(selectValue) && selectValue?.value === value.value
                            }
                            onClick={() => {
                              onOptionClick({
                                value: value?.value,
                                label: value?.label,
                              });
                              closeDropdown();
                            }}
                          />
                        ))}
                      </SelectDropdownItemList>
                    </SelectDropdownBody>
                  </SelectDropdown>
                )}
              </Select>
            </Container>
            <Divider className={spacing.mt24} fullHorizontalWidth />
            <Text type="body-regular-14" color="grey-100" className={spacing.mt24}>
              Cash account information
            </Text>
            <Container flexdirection="column" gap="24">
              <Container justifycontent="space-between" gap="24" className={spacing.mt12}>
                <TextInput
                  name="accountName"
                  label="Account name"
                  required
                  width="full"
                  value={nameValue}
                  onChange={(e) => {
                    setNameValue(e.target.value);
                  }}
                />
                <TextInput
                  name="accountHolder"
                  label="Account holder"
                  width="full"
                  value={holderValue}
                  onChange={(e) => {
                    setHolderValue(e.target.value);
                  }}
                />

                <Select label="Currency" placeholder="Choose option" width="172" required>
                  {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
                    <SelectDropdown selectElement={selectElement}>
                      <SelectDropdownBody>
                        <Search
                          className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                          width="full"
                          height="40"
                          name="search"
                          placeholder="Search option"
                          value={searchValue}
                          onChange={(e) => setSearchValue(e.target.value)}
                          searchParams={[]}
                          onSearch={() => {}}
                          onClear={() => {}}
                        />
                        <SelectDropdownItemList>
                          {currencyOptions.map((value, index) => (
                            <OptionWithSingleLabel
                              id={value.label}
                              key={index}
                              label={`${value?.label} - ${value.fullLabel}`}
                              selected={
                                !Array.isArray(selectValue) && selectValue?.value === value.value
                              }
                              onClick={() => {
                                onOptionClick({
                                  value: value?.value,
                                  label: value?.label,
                                });
                                closeDropdown();
                              }}
                            />
                          ))}
                        </SelectDropdownItemList>
                      </SelectDropdownBody>
                    </SelectDropdown>
                  )}
                </Select>
              </Container>
              <TextArea
                label="Add Notes"
                name="notes"
                minHeight="134"
                placeholder="Enter"
                counter
                value={notesValue}
                onChange={(e) => {
                  setNotesValue(e.target.value);
                }}
              />
              {!addingOpeningBalance && !editingOpeningBalance && !newOpeningBalance && (
                <Container justifycontent="flex-end">
                  <LinkButton
                    icon={<PlusIcon />}
                    onClick={() => {
                      setAddingOpeningBalance(true);
                    }}
                  >
                    Add opening balance
                  </LinkButton>
                </Container>
              )}
              <OpeningBalance
                bankAccountType
                addingOpeningBalance={addingOpeningBalance}
                newOpeningBalance={newOpeningBalance}
                editingOpeningBalance={editingOpeningBalance}
                setEditingOpeningBalance={setEditingOpeningBalance}
                setNewOpeningBalance={setNewOpeningBalance}
                setAddingOpeningBalance={setAddingOpeningBalance}
              />
            </Container>
          </>
        </BackdropWrapper>
      </Container>
    </Modal>
  );
};
