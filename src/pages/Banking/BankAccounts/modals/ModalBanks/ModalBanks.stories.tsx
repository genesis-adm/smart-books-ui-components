import { Icon } from 'Components/base/Icon';
import { Container } from 'Components/base/grid/Container';
import { InputNew } from 'Components/base/inputs/InputNew';
import { TextareaNew } from 'Components/base/inputs/TextareaNew';
import { Text } from 'Components/base/typography/Text';
import { Modal } from 'Components/modules/Modal';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { ReactComponent as BankIcon } from 'Svg/v2/16/bank.svg';
import { ReactComponent as InfoIcon } from 'Svg/v2/16/info-in-circle.svg';
import { ReactComponent as BICSwiftCodeTooltip } from 'Svg/v2/tooltips/bic-swift-code.svg';
import React, { useState } from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Banking/Bank Accounts/Modals/Banks',
};

export const CreateNewBank: React.FC = () => {
  const [currentValue, setCurrentValue] = useState('');

  return (
    <Modal
      size="720"
      background="grey-10"
      type="new"
      header={
        <ModalHeaderNew background="grey-10" title="Create new Bank" border onClose={() => {}} />
      }
    >
      <Container
        flexdirection="column"
        borderRadius="20"
        background="white-100"
        className={spacing.p24}
        gap="24"
      >
        <Container gap="8" className={spacing.w380}>
          <Icon className={spacing.mt4} icon={<BankIcon />} path="grey-100" />
          <Text type="body-regular-14" color="grey-100">
            Please add Bank bank identification. At least one identification required
          </Text>
        </Container>
        <Container justifycontent="space-between" gap="24">
          <InputNew
            name="bicCode"
            label="BIC"
            width="300"
            placeholder="XXXX"
            icon={<InfoIcon />}
            // icon={<StatusSuccessIcon />}
            elementPosition="left"
            // onChange={(e) => {
            //   setNameValue(e.target.value);
            // }}
            // value={nameValue}
            tooltip={<BICSwiftCodeTooltip />}
            tooltipWidth="auto"
          />
          <InputNew
            name="abaNumber"
            label="ABA routing"
            width="300"
            placeholder="XXXX"
            icon={<InfoIcon />}
            // icon={<StatusSuccessIcon />}
            elementPosition="left"
            // onChange={(e) => {
            //   setNameValue(e.target.value);
            // }}
            // value={nameValue}
            // tooltip={<RoutingNumberTooltip />}
            // tooltipWidth="auto"
          />
        </Container>
        <InputNew
          name="bankName"
          label="Bank name"
          width="full"
          required
          value={currentValue}
          onChange={(e) => setCurrentValue(e.target.value)}
        />
        <TextareaNew
          name="bankAddress"
          label="Bank address"
          required
          value={currentValue}
          onChange={(e) => setCurrentValue(e.target.value)}
        />
      </Container>
    </Modal>
  );
};
