import { Checkbox } from 'Components/base/Checkbox';
import { Divider } from 'Components/base/Divider';
import { Icon } from 'Components/base/Icon';
import { Radio } from 'Components/base/Radio';
import { TooltipIcon } from 'Components/base/TooltipIcon';
import { Button } from 'Components/base/buttons/Button';
import { ButtonGroup } from 'Components/base/buttons/ButtonGroup';
import { IconButton } from 'Components/base/buttons/IconButton';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { SelectNew } from 'Components/base/inputs/SelectNew';
import { OptionQuadroNew } from 'Components/base/inputs/SelectNew/options/OptionQuadroNew';
import { OptionSingleNew } from 'Components/base/inputs/SelectNew/options/OptionSingleNew';
import { Text } from 'Components/base/typography/Text';
import { UploadContainer } from 'Components/custom/Upload/UploadContainer';
import { UploadInfo } from 'Components/custom/Upload/UploadInfo';
import { UploadItemField } from 'Components/custom/Upload/UploadItemField';
import { ProgressBar } from 'Components/elements/ProgressBar';
import { Modal } from 'Components/modules/Modal';
import ModalFooter from 'Components/modules/Modal/ModalFooter';
import ModalHeader from 'Components/modules/Modal/ModalHeader';
import { Table } from 'Components/old/Table';
import { TableBody } from 'Components/old/Table/TableBody';
import { TableCell } from 'Components/old/Table/TableCell';
import { TableHead } from 'Components/old/Table/TableHead';
import { TableRow } from 'Components/old/Table/TableRow';
import { TableTitle } from 'Components/old/Table/TableTitle';
import { options } from 'Mocks/fakeOptions';
import { ReactComponent as AlertIcon } from 'Svg/16/alert.svg';
import { ReactComponent as QuestionIcon } from 'Svg/16/question.svg';
import { ReactComponent as ArrowLeftIcon } from 'Svg/v2/16/arrow-left.svg';
import { ReactComponent as ArrowRightIcon } from 'Svg/v2/16/arrow-right.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import { checkFileType } from 'Utils/fileHandlers';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Banking/Bank Accounts/Modals/Upload Transactions',
};

export const UploadTransactionsStep1: React.FC = () => {
  const [file, setFile] = useState<File | null>(null);

  const handleFileUpload = (item: File) => {
    setFile(item);
  };
  const handleFileDelete = () => {
    setFile(null);
  };
  const handleFileDrop = (e: React.DragEvent<HTMLElement>) => {
    const dropFile = e.dataTransfer.files[0];
    if (checkFileType(dropFile, ['xls', 'xlsx', 'csv'])) {
      setFile(e.dataTransfer.files[0]);
    }
  };
  return (
    <Modal
      overlay="grey-10"
      size="full"
      background="grey-10"
      header={
        <ModalHeader type="fullwidth" backgroundColor="white-100" title="Import bank accounts" />
      }
      footer={
        <ModalFooter justifycontent="space-between">
          <ProgressBar steps={3} activeStep={1} />
          <ButtonGroup align="right">
            <Button
              width="sm"
              background="white-100"
              color="black-100"
              border="grey-20-grey-90"
              onClick={() => {}}
            >
              Cancel
            </Button>
            <Button width="md" navigation iconRight={<ArrowRightIcon />} onClick={() => {}}>
              Next
            </Button>
          </ButtonGroup>
        </ModalFooter>
      }
    >
      <Container
        background="white-100"
        radius
        className={classNames(spacing.p24, spacing.mX20, spacing.mt16)}
      >
        <Container flexdirection="column" className={classNames(spacing.w450, spacing.w100p)}>
          <Text type="body-medium">Select bank account to upload transactions</Text>
          <SelectNew
            name="legalentity"
            className={spacing.mt24}
            onChange={() => {}}
            placeholder="Choose from list"
            width="full"
            label="Choose legal entity"
            required
          >
            {({ onClick, state }) => (
              <>
                {options.map(({ label, value }) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    selected={state?.value === value}
                    onClick={() => onClick({ label, value })}
                  />
                ))}
              </>
            )}
          </SelectNew>
          <ButtonGroup className={spacing.mt8} align="right" nospace>
            <LinkButton icon={<PlusIcon />} onClick={() => {}}>
              Add new legal entity
            </LinkButton>
          </ButtonGroup>
          <SelectNew
            name="bankaccount"
            className={spacing.mt24}
            onChange={() => {}}
            placeholder="Choose from list"
            width="full"
            label="Bank account"
            required
          >
            {({ onClick, state }) => (
              <>
                {options.map(({ label, secondaryLabel, tertiaryLabel, quadroLabel, value }) => (
                  <OptionQuadroNew
                    key={value}
                    label={label}
                    secondaryLabel={secondaryLabel}
                    tertiaryLabel={tertiaryLabel}
                    quadroLabel={quadroLabel}
                    selected={state?.value === value}
                    onClick={() => onClick({ label: `${label} | ${tertiaryLabel}`, value })}
                  />
                ))}
              </>
            )}
          </SelectNew>
          <ButtonGroup className={spacing.mt8} align="right" nospace>
            <LinkButton icon={<PlusIcon />} onClick={() => {}}>
              Add new bank account
            </LinkButton>
          </ButtonGroup>
        </Container>
      </Container>
      <Container
        background="white-100"
        flexdirection="column"
        radius
        gap="24"
        flexgrow="1"
        className={classNames(spacing.p24, spacing.mX20, spacing.mY16)}
      >
        <Text type="body-medium">Upload a file with your transactions</Text>
        <Container flexdirection="column">
          <Container alignitems="center">
            <Icon icon={<AlertIcon />} path="yellow-90" className={spacing.mr12} />
            <Text type="body-regular-14" color="yellow-90">
              All your transaction information must be in one file
            </Text>
          </Container>
          <Container alignitems="center">
            <Icon icon={<AlertIcon />} path="yellow-90" className={spacing.mr12} />
            <Text type="body-regular-14" color="yellow-90">
              The top row of your file must contain a header title for each column of information
            </Text>
          </Container>
        </Container>
        <UploadContainer onDrop={handleFileDrop}>
          <UploadItemField
            name="uploader"
            files={file}
            acceptFilesType="csv"
            onChange={handleFileUpload}
            onDelete={handleFileDelete}
            onReload={() => {}}
          />
          <UploadInfo
            mainText={file ? file.name : 'Drag and drop or Choose file'}
            secondaryText="CSV; Maximum file size is 5MB"
          />
        </UploadContainer>
        <Text className={spacing.mt12} color="grey-100">
          Formats: XLS, XLSX, CSV, TXT.
        </Text>
        <Text className={spacing.mt6} color="grey-100">
          Size: Maximum file size is 5MB
        </Text>
      </Container>
    </Modal>
  );
};

export const UploadTransactionsStep2: React.FC = () => (
  <Modal
    overlay="grey-10"
    size="full"
    background="grey-10"
    header={
      <ModalHeader type="fullwidth" backgroundColor="white-100" title="Import bank accounts" />
    }
    footer={
      <ModalFooter justifycontent="space-between">
        <ProgressBar steps={3} activeStep={2} />
        <ButtonGroup align="right">
          <Button
            width="sm"
            navigation
            iconLeft={<ArrowLeftIcon />}
            background="white-100"
            color="black-100"
            border="grey-20-grey-90"
            onClick={() => {}}
          >
            Cancel
          </Button>
          <Button width="md" navigation iconRight={<ArrowRightIcon />} onClick={() => {}}>
            Next
          </Button>
        </ButtonGroup>
      </ModalFooter>
    }
  >
    <Container
      background="white-100"
      flexdirection="column"
      radius
      className={classNames(spacing.p24, spacing.mX20, spacing.mt16)}
    >
      <Container alignitems="center">
        <Text type="body-medium">File has amounts in:</Text>
        <TooltipIcon
          size="16"
          className={spacing.ml10}
          icon={<QuestionIcon />}
          message="Some text"
        />
      </Container>
      <Radio
        className={spacing.mt12}
        id="1column"
        name="columnsquantity"
        checked
        onChange={() => {}}
      >
        1 column: both positive and negative numbers
      </Radio>
      <Radio
        className={spacing.mt8}
        id="2columns"
        name="columnsquantity"
        checked={false}
        onChange={() => {}}
      >
        2 columns: separate positive and negative numbers
      </Radio>
    </Container>
    <Container
      background="white-100"
      flexdirection="column"
      radius
      flexgrow="1"
      className={classNames(spacing.p24, spacing.mX20, spacing.mY16)}
    >
      <Text type="body-medium">
        Map columns. For each SmartBooks field, select bank account fields
      </Text>
      <Container className={spacing.mt12} alignitems="center">
        <Container className={classNames(spacing.h50min, spacing.mr20)} alignitems="center">
          <Text type="body-regular-14" className={classNames(spacing.w130min, spacing.w130)} noWrap>
            Columns
          </Text>
          <ArrowLeftIcon />
        </Container>
        <Text type="body-regular-14" className={spacing.w300} noWrap>
          File report_09.10.20.xls
        </Text>
      </Container>
      <Container className={spacing.mt12} alignitems="center">
        <Container className={classNames(spacing.h50min, spacing.mr20)} alignitems="center">
          <Text
            type="body-regular-14"
            color="grey-100"
            className={classNames(spacing.w130min, spacing.w130)}
            noWrap
          >
            Date
            <Text type="caption-regular" color="red-90">
              *
            </Text>
          </Text>
          <ArrowLeftIcon />
        </Container>
        <SelectNew
          name="option5"
          onChange={() => {}}
          placeholder="Choose option"
          label="Choose option"
          className={spacing.mr20}
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() => onClick({ label, value })}
                />
              ))}
            </>
          )}
        </SelectNew>
        <SelectNew
          name="option6"
          label="Choose option"
          onChange={() => {}}
          placeholder="Choose option"
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() => onClick({ label, value })}
                />
              ))}
            </>
          )}
        </SelectNew>
      </Container>
      <Container className={spacing.mt12} alignitems="center">
        <Container className={classNames(spacing.h50min, spacing.mr20)} alignitems="center">
          <Text
            type="body-regular-14"
            color="grey-100"
            className={classNames(spacing.w130min, spacing.w130)}
            noWrap
          >
            Description
            <Text type="caption-regular" color="red-90">
              *
            </Text>
          </Text>
          <ArrowLeftIcon />
        </Container>
        <SelectNew
          name="option7"
          onChange={() => {}}
          placeholder="Choose option"
          label="Choose option"
          className={spacing.mr20}
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() => onClick({ label, value })}
                />
              ))}
            </>
          )}
        </SelectNew>
      </Container>
      <Container className={classNames(spacing.mt12, spacing.ml170)}>
        <LinkButton icon={<PlusIcon />} onClick={() => {}}>
          Add line
        </LinkButton>
      </Container>
      <Container className={spacing.mt12} alignitems="center">
        <Container className={classNames(spacing.w146fixed, spacing.mr20)} />
        <SelectNew
          name="option17"
          onChange={() => {}}
          placeholder="Choose option"
          label="Choose option"
          className={spacing.mr20}
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() => onClick({ label, value })}
                />
              ))}
            </>
          )}
        </SelectNew>
        <IconButton
          size="16"
          icon={<TrashIcon />}
          background="transparent"
          color="grey-90-red-90"
          onClick={() => {}}
        />
      </Container>
      <Container className={spacing.mt12} alignitems="center">
        <Container className={classNames(spacing.h50min, spacing.mr20)} alignitems="center">
          <Text
            type="body-regular-14"
            color="grey-100"
            className={classNames(spacing.w130min, spacing.w130)}
            noWrap
          >
            Amount
            <Text type="caption-regular" color="red-90">
              *
            </Text>
          </Text>
          <ArrowLeftIcon />
        </Container>
        <SelectNew
          name="option"
          onChange={() => {}}
          placeholder="Choose option"
          label="Choose option"
          className={spacing.mr20}
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() => onClick({ label, value })}
                />
              ))}
            </>
          )}
        </SelectNew>
      </Container>
    </Container>
  </Modal>
);

export const UploadTransactionsStep3: React.FC = () => (
  <Modal
    size="full"
    overlay="grey-10"
    background="grey-10"
    header={
      <ModalHeader type="fullwidth" backgroundColor="white-100" title="Upload transactions" />
    }
    footer={
      <ModalFooter justifycontent="space-between">
        <ProgressBar steps={3} activeStep={3} />
        <ButtonGroup align="right">
          <Button
            width="sm"
            navigation
            iconLeft={<ArrowLeftIcon />}
            background="white-100"
            color="black-100"
            border="grey-20-grey-90"
            onClick={() => {}}
          >
            Back
          </Button>
          <Button width="md" onClick={() => {}}>
            Import transactions
          </Button>
        </ButtonGroup>
      </ModalFooter>
    }
  >
    <Container flexdirection="column" className={spacing.mX30}>
      <Text type="body-medium" className={spacing.mt32}>
        Select transactions to import:
      </Text>
      <Text color="grey-100" className={spacing.mt12}>
        Organization:&nbsp;
        <Text>GENESIS</Text>
      </Text>
      <Text color="grey-100" className={spacing.mt6}>
        Legal entity:&nbsp;
        <Text>Matar</Text>
      </Text>
      <Container className={spacing.mt6} alignitems="center" justifycontent="space-between">
        <Text color="grey-100">
          Bank account:&nbsp;
          <Text>BNP Paribas IBAN 030303030030303030330 USD</Text>
        </Text>
        <Container alignitems="center">
          <Text type="text-medium">Number of transactions:&nbsp;</Text>
          <Text type="caption-semibold">145</Text>
          <Divider type="vertical" className={spacing.mX16} />
          <Text type="text-medium">Selected:&nbsp;</Text>
          <Text type="caption-semibold">145</Text>
        </Container>
      </Container>
    </Container>
    <Table
      className={classNames(spacing.w100p, spacing.mt24, spacing.pX30)}
      tableHead={
        <TableHead margin="none">
          <TableTitle minWidth="60" onClick={() => {}}>
            <Checkbox name="tickAll" checked onChange={() => {}} />
          </TableTitle>
          <TableTitle minWidth="160" title="Date" sortIcon onClick={() => {}} />
          <TableTitle minWidth="160" title="Description" flexgrow="1" sortIcon onClick={() => {}} />
          <TableTitle minWidth="160" title="Amount" sortIcon onClick={() => {}} />
        </TableHead>
      }
    >
      <TableBody margin="none" display="block">
        {Array.from(Array(30)).map((_, index) => (
          <TableRow key={index}>
            <TableCell minWidth="60" justifycontent="center" alignitems="center">
              <Checkbox name={`line-${index}`} checked onChange={() => {}} />
            </TableCell>
            <TableCell minWidth="160" justifycontent="center">
              <Text>{index + 1}/01/2021</Text>
            </TableCell>
            <TableCell minWidth="160" flexgrow="1" justifycontent="center">
              <Text noWrap>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. In euismod turpis placerat,
                posuere nunc eu, faucibus sem.
              </Text>
            </TableCell>
            <TableCell minWidth="160" justifycontent="center">
              <Container>
                <Text color={index % 2 === 0 ? 'green-90' : 'red-90'} align="right">
                  {index % 2 === 0 ? '+' : '-'}
                  1,000,000.87 $
                </Text>
              </Container>
            </TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  </Modal>
);
