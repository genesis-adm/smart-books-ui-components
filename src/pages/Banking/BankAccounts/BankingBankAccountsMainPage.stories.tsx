import { Meta, Story } from '@storybook/react';
import { LoadingLine } from 'Components/base/LoadingLine';
import { Container } from 'Components/base/grid/Container';
import { ContainerMain } from 'Components/base/grid/ContainerMain';
import { MainPageContentContainer } from 'Components/base/grid/MainPageContentContainer';
import { GeneralTab, GeneralTabWrapper } from 'Components/base/tabs/GeneralTab';
import { Text } from 'Components/base/typography/Text';
import { NavigationSB } from 'Components/custom/Stories/NavigationSB';
import { Pagination } from 'Components/custom/Stories/Pagination/Pagination';
import PerPageSelect from 'Components/custom/Stories/Pagination/PerPageSelect/PerPageSelect';
import { rowsPerPageOptions } from 'Mocks/fakeOptions';
import { ReactComponent as BankIcon } from 'Svg/v2/16/bank.svg';
import { ReactComponent as AccountsIcon } from 'Svg/v2/16/briefcase-filled-dollar.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';
import BankAccountsEmptyPage from './components/BankAccountsEmptyPage';
import BankAccountsFiltersSearch from './components/BankAccountsFiltersSearch';
// PAGE PARTS START
import BankAccountsHeader from './components/BankAccountsHeader';
import BankAccountsTable from './components/BankAccountsTable';

// PAGE PARTS END

export type BankAccountsPageType = 'banks' | 'accounts';

const BankingBankAccountsMainPageComponent: Story = ({ storyState }) => {
  const [pageType, setPageType] = useState<BankAccountsPageType>('banks');
  const [toggle, toggleMenu] = useState<boolean>(false);
  const [rowsPerPage, setRowsPerPage] = useState<string>(rowsPerPageOptions[0].value);

  return (
    <Container fullscreen flexwrap="nowrap" background={'grey-10'}>
      <NavigationSB opened={toggle} onToggleOpen={() => toggleMenu(!toggle)} />
      <ContainerMain navOpened={toggle}>
        <BankAccountsHeader />
        <MainPageContentContainer>
          {storyState === 'loaded' && <BankAccountsFiltersSearch pageType={pageType} />}
          <GeneralTabWrapper className={classNames(spacing.mX24, spacing.mt24)}>
            <GeneralTab
              id="banks"
              icon={<BankIcon />}
              onClick={() => setPageType('banks')}
              active={pageType === 'banks'}
            >
              Banks
            </GeneralTab>
            <GeneralTab
              id="bankAccounts"
              icon={<AccountsIcon />}
              onClick={() => setPageType('accounts')}
              active={pageType === 'accounts'}
            >
              Bank accounts
            </GeneralTab>
          </GeneralTabWrapper>
          {storyState === 'loaded' && (
            <>
              <BankAccountsTable pageType={pageType} />
              <Pagination
                padding="16"
                customPagination={
                  <Container gap="16" alignitems="center">
                    <Text type="subtext-regular" color="grey-100">
                      Rows per Page
                    </Text>

                    <PerPageSelect
                      selectedValue={rowsPerPage}
                      onOptionClick={(value) => {
                        setRowsPerPage(value);
                      }}
                    />
                  </Container>
                }
              />
            </>
          )}
          {storyState === 'empty' && <BankAccountsEmptyPage pageType={pageType} />}
        </MainPageContentContainer>
        <LoadingLine loadingCompleted={storyState !== 'loading'} />
      </ContainerMain>
    </Container>
  );
};

export default {
  title: 'Pages/Banking/Bank Accounts/Banking Bank Accounts Main Page',
  component: BankingBankAccountsMainPageComponent,
  argTypes: {
    storyState: {
      options: ['empty', 'loading', 'loaded'],
      control: {
        type: 'radio',
        labels: {
          empty: 'Empty page',
          loading: 'Page is loading',
          loaded: 'Page loaded',
        },
      },
    },
  },
} as Meta;

export const BankingBankAccountsMainPage = BankingBankAccountsMainPageComponent.bind({});
BankingBankAccountsMainPage.args = {
  storyState: 'loaded',
};
