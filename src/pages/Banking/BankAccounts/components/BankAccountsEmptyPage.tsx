import { Button } from 'Components/base/buttons/Button';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { Container } from 'Components/base/grid/Container';
import { MainPageEmpty } from 'Components/custom/MainPage/MainPageEmpty';
import { ReactComponent as BankIcon } from 'Svg/v2/16/bank.svg';
import { ReactComponent as CashIcon } from 'Svg/v2/16/cash.svg';
import { ReactComponent as CreditCardIcon } from 'Svg/v2/16/credit-card.svg';
import { ReactComponent as UploadIcon } from 'Svg/v2/16/upload.svg';
import { ReactComponent as WalletIcon } from 'Svg/v2/16/wallet.svg';
import React, { ReactElement } from 'react';

import { BankAccountsPageType } from '../BankingBankAccountsMainPage.stories';

interface BankAccountsTableProps {
  pageType: BankAccountsPageType;
}

const BankAccountsEmptyPage = ({ pageType }: BankAccountsTableProps): ReactElement => (
  <>
    {pageType === 'banks' && (
      <MainPageEmpty
        title="Create new Bank"
        description="You have no banks created. Your banks will be displayed here."
        action={
          <Button
            width="220"
            height="40"
            background="blue-10-blue-20"
            color="violet-90-violet-100"
            // iconLeft={<PlusIcon />}
            onClick={() => {}}
          >
            Create bank account
          </Button>
        }
      />
    )}
    {pageType === 'accounts' && (
      <MainPageEmpty
        title="Create new Bank accounts"
        description="You have no bank accounts created. Your bank accounts will be displayed here."
        action={
          <Container gap="24">
            <Button
              width="220"
              height="40"
              background="blue-10-blue-20"
              color="violet-90-violet-100"
              // iconLeft={<PlusIcon />}
              onClick={() => {}}
              dropdownBorder="white-100"
              dropdown={
                <>
                  <DropDownButton size="204" icon={<CreditCardIcon />} onClick={() => {}}>
                    Credit Card
                  </DropDownButton>
                  <DropDownButton size="204" icon={<BankIcon />} onClick={() => {}}>
                    Bank Account
                  </DropDownButton>
                  <DropDownButton size="204" icon={<WalletIcon />} onClick={() => {}}>
                    Wallet
                  </DropDownButton>
                  <DropDownButton size="204" icon={<CashIcon />} onClick={() => {}}>
                    Cash
                  </DropDownButton>
                </>
              }
            >
              Create Bank account
            </Button>
            <Button
              width="220"
              height="40"
              background="blue-10-blue-20"
              color="violet-90-violet-100"
              iconLeft={<UploadIcon />}
              onClick={() => {}}
            >
              Import from CSV
            </Button>
          </Container>
        }
      />
    )}
  </>
);

export default BankAccountsEmptyPage;
