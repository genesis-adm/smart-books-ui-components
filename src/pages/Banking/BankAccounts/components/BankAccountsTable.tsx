import { DropDown } from 'Components/DropDown';
import { Checkbox } from 'Components/base/Checkbox';
import { Divider } from 'Components/base/Divider';
import { Tag } from 'Components/base/Tag';
import { TagSegmented } from 'Components/base/TagSegmented';
import { Button } from 'Components/base/buttons/Button';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { bankAccounts } from 'Mocks/purchasesFakeData';
import { ReactComponent as DisabledStatusIcon } from 'Svg/v2/16/exclamation-in-circle.svg';
import { ReactComponent as DisableEyeIcon } from 'Svg/v2/16/eye-crossed.svg';
import { ReactComponent as ActivateEyeIcon } from 'Svg/v2/16/eye.svg';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import { ReactComponent as DropdownIcon } from 'Svg/v2/16/three-dots.svg';
import { ReactComponent as ActiveStatusIcon } from 'Svg/v2/16/tick-in-circle.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import classNames from 'classnames';
import React, { ReactElement } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';
import type { TableCellWidth } from '../../../../components/types/gridTypes';
import { BankAccountsPageType } from '../BankingBankAccountsMainPage.stories';

interface BankAccountsTableProps {
  pageType: BankAccountsPageType;
}

const sorting = {
  sortType: null,
  tooltipText: 'Sorting tooltip text',
};

const tableHeadBanks = [
  {
    minWidth: '48',
    title: <Checkbox name="tickAll" checked onChange={() => {}} />,
    padding: 'none',
  },
  { minWidth: '240', title: 'Bank name', sorting },
  { minWidth: '380', title: 'Bank address', flexgrow: '1' },
  { minWidth: '210', title: 'BIC' },
  { minWidth: '210', title: 'ABA routing' },
  { minWidth: '120', title: 'Action', align: 'center' },
];

const tableHeadBankAccounts = [
  {
    minWidth: '48',
    title: <Checkbox name="tickAll" checked onChange={() => {}} />,
    padding: 'none',
  },
  { minWidth: '120', title: 'ID' },
  { minWidth: '160', title: 'Legal entity', sorting },
  { minWidth: '160', title: 'Business division', sorting },
  { minWidth: '330', title: 'Bank account', sorting }, // 290
  { minWidth: '80', title: 'Rules', align: 'center', sorting },
  { minWidth: '200', title: 'Last update' },
  { minWidth: '210', title: 'Balance', align: 'right' },
  { minWidth: '60', title: 'Status', align: 'center' },
  { minWidth: '120', title: 'Action', align: 'center' },
];

const BankAccountsTable = ({ pageType }: BankAccountsTableProps): ReactElement => (
  <TableNew
    noResults={false}
    className={classNames(spacing.mt24, spacing.ml24, spacing.pr24)}
    tableHead={
      <TableHeadNew>
        {pageType === 'banks' &&
          tableHeadBanks.map((item, index) => (
            <TableTitleNew
              key={index}
              minWidth={item.minWidth as TableCellWidth}
              align={item.align as TableTitleNewProps['align']}
              title={item.title}
              sorting={item.sorting}
              flexgrow={item.flexgrow as TableTitleNewProps['flexgrow']}
              onClick={() => {}}
            />
          ))}
        {pageType === 'accounts' &&
          tableHeadBankAccounts.map((item, index) => (
            <TableTitleNew
              key={index}
              minWidth={item.minWidth as TableCellWidth}
              align={item.align as TableTitleNewProps['align']}
              title={item.title}
              sorting={item.sorting}
              // flexgrow={item.flexgrow as TableTitleNewProps['flexgrow']}
              onClick={() => {}}
            />
          ))}
      </TableHeadNew>
    }
  >
    <TableBodyNew>
      {Array.from(Array(25)).map((_, index) => (
        <TableRowNew key={index} gap="8">
          {pageType === 'banks' && (
            <>
              <TableCellNew minWidth="48" justifycontent="center" alignitems="center">
                <Checkbox name={`line-${index}`} checked onChange={() => {}} />
              </TableCellNew>
              <TableCellTextNew minWidth="240" type="caption-semibold" value="Obrio Limited" />
              <TableCellTextNew
                minWidth="380"
                lineClamp="2"
                flexgrow="1"
                value="Floor 4, The Columbus Building, 7 Westferry Circus, London, United Kingdom"
                color="grey-100"
              />
              <TableCellTextNew minWidth="210" value="AAAA BB CC 123" color="grey-100" />
              <TableCellTextNew minWidth="210" value="None" color="grey-100" />
              <TableCellNew minWidth="120" alignitems="center" justifycontent="center">
                <DropDown
                  flexdirection="column"
                  padding="8"
                  gap="4"
                  control={({ handleOpen }) => (
                    <IconButton
                      icon={<DropdownIcon />}
                      onClick={handleOpen}
                      background="transparent"
                      color="grey-100-violet-90"
                    />
                  )}
                >
                  <DropDownButton size="160" icon={<EditIcon />} onClick={() => {}}>
                    Edit
                  </DropDownButton>
                  <DropDownButton size="160" type="danger" icon={<TrashIcon />} onClick={() => {}}>
                    Delete
                  </DropDownButton>
                </DropDown>
              </TableCellNew>
            </>
          )}
          {pageType === 'accounts' && (
            <>
              <TableCellNew minWidth="48" justifycontent="center" alignitems="center">
                <Checkbox name={`line-${index}`} checked onChange={() => {}} />
              </TableCellNew>
              <TableCellTextNew minWidth="120" value={`ID 000000${index + 1}`} />
              <TableCellTextNew minWidth="160" value="Obrio Limited" type="caption-semibold" />
              <TableCellTextNew minWidth="160" value="Obrio Limited" />
              <TableCellNew minWidth="330" fixedWidth>
                <TagSegmented
                  {...bankAccounts}
                  iconStaticColor
                  noWrapSecondLabel
                  tooltipMessage={
                    <Container flexdirection="column">
                      <Text color="white-100" style={{ wordBreak: 'break-all' }}>
                        {bankAccounts.mainLabel}
                      </Text>
                      <Text color="white-100" style={{ wordBreak: 'break-all' }}>
                        {bankAccounts.secondLabel} | {bankAccounts.thirdLabel}
                      </Text>
                    </Container>
                  }
                />
              </TableCellNew>
              <TableCellTextNew minWidth="80" value="No" align="center" />
              <TableCellTextNew
                minWidth="200"
                value="Joseph Robinette Biden, Jr."
                type="caption-bold"
                secondaryValue="08 September 2021 | 22:15"
                secondaryColor="grey-100"
              />
              <TableCellTextNew
                minWidth="210"
                value="$ 1,000,000.00"
                type="caption-bold"
                align="right"
                color="violet-90"
                secondaryValue="08 September 2021 | 10:15 AM"
                secondaryColor="grey-100"
                secondaryAlign="right"
              />
              <TableCellNew minWidth="60" justifycontent="center">
                {index % 2 === 0 ? (
                  <Tag icon={<ActiveStatusIcon />} tooltip="Active" color="green" />
                ) : (
                  <Tag icon={<DisabledStatusIcon />} tooltip="Disabled" color="yellow" />
                )}
              </TableCellNew>
              <TableCellNew minWidth="120" alignitems="center" justifycontent="center">
                <Button
                  width="80"
                  height="24"
                  font="text-medium"
                  onClick={() => {}}
                  background="blue-10-blue-20"
                  border="blue-10-blue-20"
                  color="violet-90-violet-100"
                  dropdownBorder="white-100"
                  dropdown={
                    <>
                      <DropDownButton size="160" icon={<EditIcon />} onClick={() => {}}>
                        Edit
                      </DropDownButton>
                      <DropDownButton
                        size="160"
                        icon={index % 2 === 0 ? <DisableEyeIcon /> : <ActivateEyeIcon />}
                        onClick={() => {}}
                      >
                        {index % 2 === 0 ? 'Disable' : 'Activate'}
                      </DropDownButton>
                      <Divider className={spacing.mY8} />
                      <DropDownButton
                        size="160"
                        type="danger"
                        icon={<TrashIcon />}
                        onClick={() => {}}
                      >
                        Delete
                      </DropDownButton>
                    </>
                  }
                >
                  Import
                </Button>
              </TableCellNew>
            </>
          )}
        </TableRowNew>
      ))}
    </TableBodyNew>
  </TableNew>
);

export default BankAccountsTable;
