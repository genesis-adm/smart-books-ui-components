import { Container } from 'Components/base/grid/Container';
import { MenuTab, MenuTabWrapper } from 'Components/base/tabs/MenuTab';
import { Text } from 'Components/base/typography/Text';
import { HeaderSB } from 'Components/custom/Stories/HeaderSB';
import React, { ReactElement } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

const BankAccountsHeader = (): ReactElement => (
  <>
    <HeaderSB />
    <Container flexdirection="column" className={spacing.mX32}>
      <Text type="h1-semibold">Banking</Text>
      <MenuTabWrapper className={spacing.mt24}>
        <MenuTab id="transactions" active={false} onClick={() => {}}>
          Transactions
        </MenuTab>
        <MenuTab id="bankAccounts" active onClick={() => {}}>
          Bank accounts
        </MenuTab>
        <MenuTab id="rules" active={false} onClick={() => {}}>
          Rules
        </MenuTab>
      </MenuTabWrapper>
    </Container>
  </>
);

export default BankAccountsHeader;
