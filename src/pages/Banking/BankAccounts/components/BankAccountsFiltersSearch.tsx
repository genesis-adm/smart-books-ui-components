import { DropDown } from 'Components/DropDown';
import { Divider } from 'Components/base/Divider';
import { Button } from 'Components/base/buttons/Button';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { ExportDropdown } from 'Components/elements/ExportDropdown';
import { FilterDropDown } from 'Components/elements/filters/FilterDropDown';
import { FilterToggleButton } from 'Components/elements/filters/FilterToggleButton';
import { FiltersWrapper } from 'Components/elements/filters/FiltersWrapper';
import { Search } from 'Components/inputs/Search';
import useSearchExample from 'Components/inputs/Search/hooks/useSearchExample';
import { ReactComponent as BankIcon } from 'Svg/v2/16/bank.svg';
import { ReactComponent as CashIcon } from 'Svg/v2/16/cash.svg';
import { ReactComponent as ChevronDown } from 'Svg/v2/16/chevron-down.svg';
import { ReactComponent as ChevronUp } from 'Svg/v2/16/chevron-up.svg';
import { ReactComponent as CreditCardIcon } from 'Svg/v2/16/credit-card.svg';
import { ReactComponent as PieChartIcon } from 'Svg/v2/16/piechart.svg';
import { ReactComponent as UploadIcon } from 'Svg/v2/16/upload.svg';
import { ReactComponent as WalletIcon } from 'Svg/v2/16/wallet.svg';
import classNames from 'classnames';
import React, { ReactElement, useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';
import { BankAccountsPageType } from '../BankingBankAccountsMainPage.stories';

interface BankAccountsTableProps {
  pageType: BankAccountsPageType;
}

const BankAccountsFiltersSearch = ({ pageType }: BankAccountsTableProps): ReactElement => {
  const [showFilters, setShowFilters] = useState<boolean>(false);

  const { searchValue, setSearchValue, searchValuesList, handleSearchValueAdd, handleClearAll } =
    useSearchExample();

  const filters = [
    { title: 'Bank account' },
    { title: 'Legal entity' },
    { title: 'Business division' },
    { title: 'Currency' },
    { title: 'Rules' },
    { title: 'Type' },
  ];
  return (
    <>
      {pageType === 'banks' && (
        <Container
          className={classNames(spacing.mX24, spacing.mt24)}
          justifycontent="space-between"
          gap="24"
        >
          <Search
            width="320"
            height="40"
            name="search-banking-bankAccounts-banks"
            placeholder="Search by name"
            value={searchValue}
            onChange={(e) => setSearchValue(e.target.value)}
            searchParams={searchValuesList}
            onSearch={handleSearchValueAdd}
            onClear={handleClearAll}
          />
          <Container gap="24">
            <Button
              height="40"
              width="140"
              background="blue-10-blue-20"
              color="violet-90-violet-100"
              onClick={() => {}}
              iconLeft={<PieChartIcon />}
            >
              Reports
            </Button>
            <DropDown
              flexdirection="column"
              padding="8"
              control={({ handleOpen, isOpen }) => (
                <Button
                  onClick={handleOpen}
                  background="violet-90-violet-100"
                  color="white-100"
                  width="140"
                  height="40"
                  iconRight={isOpen ? <ChevronUp /> : <ChevronDown />}
                >
                  Create
                </Button>
              )}
            >
              <>
                <DropDownButton size="160" icon={<CreditCardIcon />} onClick={() => {}}>
                  Credit Card
                </DropDownButton>
                <DropDownButton size="160" icon={<BankIcon />} onClick={() => {}}>
                  Bank Account
                </DropDownButton>
              </>
            </DropDown>
          </Container>
        </Container>
      )}

      {pageType === 'accounts' && (
        <>
          <Container
            className={classNames(spacing.mX24, spacing.mt24)}
            justifycontent="space-between"
            gap="24"
          >
            <Container gap="24" alignitems="center">
              <FilterToggleButton
                showFilters={showFilters}
                filtersSelected={false}
                onClear={() => {}}
                onClick={() => {
                  setShowFilters((prevState) => !prevState);
                }}
              />
              <Search
                width="320"
                height="40"
                name="search-banking-transactions-mainPage"
                placeholder="Search by ID and description"
                value={searchValue}
                onChange={(e) => setSearchValue(e.target.value)}
                searchParams={searchValuesList}
                onSearch={handleSearchValueAdd}
                onClear={handleClearAll}
              />
            </Container>
            <Container gap="24" alignitems="center">
              <Divider type="vertical" background="grey-90" height="20" />
              <ExportDropdown onExportToXLS={() => {}} onExportToCSV={() => {}} />
              <IconButton
                size="40"
                background="grey-10-grey-20"
                color="grey-100"
                icon={<UploadIcon />}
                tooltip="Import files"
                onClick={() => {}}
              />
              <Button
                background="violet-90-violet-100"
                color="white-100"
                width="140"
                height="40"
                dropdown={
                  <>
                    <DropDownButton size="160" icon={<CreditCardIcon />} onClick={() => {}}>
                      Credit Card
                    </DropDownButton>
                    <DropDownButton size="160" icon={<BankIcon />} onClick={() => {}}>
                      Bank Account
                    </DropDownButton>
                    <DropDownButton size="160" icon={<WalletIcon />} onClick={() => {}}>
                      Wallet
                    </DropDownButton>
                    <DropDownButton size="160" icon={<CashIcon />} onClick={() => {}}>
                      Cash
                    </DropDownButton>
                  </>
                }
                dropdownBorder="white-100"
              >
                Create
              </Button>
            </Container>
          </Container>
          <FiltersWrapper active={showFilters}>
            {filters.map((item) => (
              <FilterDropDown
                key={item.title}
                title={item.title}
                value="All"
                onClear={() => {}}
                selectedAmount={0}
              />
            ))}
          </FiltersWrapper>
        </>
      )}
    </>
  );
};

export default BankAccountsFiltersSearch;
