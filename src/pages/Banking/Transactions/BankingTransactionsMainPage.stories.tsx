import { Meta, Story } from '@storybook/react';
import { DropDown } from 'Components/DropDown';
import { ActionBar } from 'Components/base/ActionBar';
import { Checkbox } from 'Components/base/Checkbox';
import { ContentLoader } from 'Components/base/ContentLoader';
import { Divider } from 'Components/base/Divider';
import { Icon } from 'Components/base/Icon';
import { LoadingLine } from 'Components/base/LoadingLine';
import { Tag } from 'Components/base/Tag';
import { TagSegmented } from 'Components/base/TagSegmented';
import { Button } from 'Components/base/buttons/Button';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { ContainerMain } from 'Components/base/grid/ContainerMain';
import { MainPageContentContainer } from 'Components/base/grid/MainPageContentContainer';
import { GeneralTab, GeneralTabWrapper } from 'Components/base/tabs/GeneralTab';
import { MenuTab, MenuTabWrapper } from 'Components/base/tabs/MenuTab';
import { Text } from 'Components/base/typography/Text';
import { MainPageEmpty } from 'Components/custom/MainPage/MainPageEmpty';
import { HeaderSB } from 'Components/custom/Stories/HeaderSB';
import { NavigationSB } from 'Components/custom/Stories/NavigationSB';
import { Pagination } from 'Components/custom/Stories/Pagination/Pagination';
import { ExportDropdown } from 'Components/elements/ExportDropdown';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { FilterDropDown } from 'Components/elements/filters/FilterDropDown';
import { FilterToggleButton } from 'Components/elements/filters/FilterToggleButton';
import { FiltersWrapper } from 'Components/elements/filters/FiltersWrapper';
import { Search } from 'Components/inputs/Search';
import type { TableCellWidth } from 'Components/types/gridTypes';
import { bankAccounts, statusColumn, tableHead } from 'Pages/Banking/Transactions/constants';
import { ReactComponent as ArrowFilledCircleDown } from 'Svg/16/arrow-filled-circle-down.svg';
import { ReactComponent as ArrowFilledCircleUp } from 'Svg/16/arrow-filled-circle-up.svg';
import { ReactComponent as FlagIcon } from 'Svg/16/flag-filled.svg';
import { ReactComponent as HandIcon } from 'Svg/16/hand-filled.svg';
import { ReactComponent as MinusIcon } from 'Svg/16/minus-light.svg';
import { ReactComponent as VectorIcon } from 'Svg/16/vector.svg';
import { ReactComponent as ArrowDownIcon } from 'Svg/v2/10/arrow-down.svg';
import { ReactComponent as ChevronDown } from 'Svg/v2/16/chevron-down.svg';
import { ReactComponent as ChevronUp } from 'Svg/v2/16/chevron-up.svg';
import { ReactComponent as ClearIcon } from 'Svg/v2/16/cross.svg';
import { ReactComponent as ForReviewIcon } from 'Svg/v2/16/document-search.svg';
import { ReactComponent as EyeIcon } from 'Svg/v2/16/eye.svg';
import { ReactComponent as AllStatusIcon } from 'Svg/v2/16/lines.svg';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import { ReactComponent as PieChartIcon } from 'Svg/v2/16/piechart.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as SummaryIcon } from 'Svg/v2/16/sigma.svg';
import { ReactComponent as DropdownIcon } from 'Svg/v2/16/three-dots.svg';
import { ReactComponent as AcceptedIcon } from 'Svg/v2/16/tick-in-filled-circle.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trash.svg';
import { ReactComponent as UploadIcon } from 'Svg/v2/16/upload.svg';
import { ReactComponent as ExcludedIcon } from 'Svg/v2/16/warning.svg';
import classNames from 'classnames';
import React, { useCallback, useState } from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';
import { useToggle } from '../../../hooks/useToggle';

const BankingTransactionsMainPageComponent: Story = ({ storyState }) => {
  const { isOpen, onClose, onOpen } = useToggle();
  const [toggle, toggleMenu] = useState(false);
  const [showFilters, setShowFilters] = useState(false);
  const isLoading = storyState === 'loading';

  const filters = [{ title: 'Customer name' }, { title: 'Payment method' }];

  const handleDropdownStateOnChange = useCallback(
    (val: boolean) => {
      val ? onOpen() : onClose();
    },
    [onOpen, onClose],
  );

  return (
    <>
      <Container fullscreen flexwrap="nowrap" background={'grey-10'}>
        <NavigationSB opened={toggle} onToggleOpen={() => toggleMenu(!toggle)} />
        <ContainerMain navOpened={toggle}>
          <HeaderSB />
          <Container flexdirection="column" className={spacing.mX32}>
            <Text type="h1-semibold">Banking</Text>
            <MenuTabWrapper className={spacing.mt24}>
              <MenuTab id="bankAccounts" active={false} onClick={() => {}}>
                Bank accounts
              </MenuTab>
              <MenuTab id="transactions" active onClick={() => {}}>
                Transactions
              </MenuTab>
              <MenuTab id="rules" active={false} onClick={() => {}}>
                Rules
              </MenuTab>
            </MenuTabWrapper>
          </Container>
          <MainPageContentContainer>
            {storyState !== 'empty' && (
              <>
                <Container
                  className={classNames(spacing.mX24, spacing.mt24)}
                  justifycontent="space-between"
                  gap="24"
                >
                  <Container gap="24" alignitems="center">
                    <ContentLoader width="110px" height="40px" isLoading={isLoading}>
                      <FilterToggleButton
                        showFilters={showFilters}
                        filtersSelected={false}
                        onClear={() => {}}
                        onClick={() => {
                          setShowFilters((prevState) => !prevState);
                        }}
                      />
                    </ContentLoader>

                    <ContentLoader width="320px" height="40px" isLoading={isLoading}>
                      <Search
                        width="320"
                        height="40"
                        name="search-banking-transactions-mainPage"
                        placeholder="Search by ID and description"
                        value=""
                        onChange={() => {}}
                        searchParams={[]}
                        onSearch={() => {}}
                        onClear={() => {}}
                      />
                    </ContentLoader>
                  </Container>
                  <Container gap="24" alignitems="center">
                    <ContentLoader width="40px" height="40px" isLoading={isLoading}>
                      <Button
                        height="40"
                        width="140"
                        background={'blue-10-blue-20'}
                        color="violet-90-violet-100"
                        onClick={() => {}}
                        iconLeft={<PieChartIcon />}
                      >
                        Reports
                      </Button>
                    </ContentLoader>
                    <ContentLoader width="40px" height="40px" isLoading={isLoading}>
                      <ExportDropdown onExportToXLS={() => {}} onExportToCSV={() => {}} />
                    </ContentLoader>
                    <ContentLoader width="40px" height="40px" isLoading={isLoading}>
                      <IconButton
                        size="40"
                        background={'grey-10-grey-20'}
                        color="grey-100"
                        icon={<UploadIcon />}
                        tooltip="Import files"
                        onClick={() => {}}
                      />
                    </ContentLoader>

                    <ContentLoader width="140px" height="40px" isLoading={isLoading}>
                      <DropDown
                        flexdirection="column"
                        padding="8"
                        control={({ handleOpen, isOpen }) => (
                          <Button
                            onClick={handleOpen}
                            background={'violet-90-violet-100'}
                            color="white-100"
                            width="140"
                            height="40"
                            iconRight={isOpen ? <ChevronUp /> : <ChevronDown />}
                          >
                            Create
                          </Button>
                        )}
                      >
                        <DropDownButton
                          size="204"
                          onClick={() => {}}
                          icon={<ArrowFilledCircleDown />}
                          fontType="caption-semibold"
                        >
                          Inflow
                        </DropDownButton>
                        <DropDownButton
                          size="204"
                          onClick={() => {}}
                          icon={<ArrowFilledCircleUp />}
                          fontType="caption-semibold"
                        >
                          Outflow
                        </DropDownButton>
                      </DropDown>
                    </ContentLoader>
                  </Container>
                </Container>
                <FiltersWrapper active={showFilters}>
                  {filters.map((item) => (
                    <FilterDropDown
                      key={item.title}
                      title={item.title}
                      value="All"
                      onClear={() => {}}
                      selectedAmount={0}
                    />
                  ))}
                </FiltersWrapper>
                <GeneralTabWrapper className={classNames(spacing.mX24, spacing.mt24)}>
                  <GeneralTab id="forReview" icon={<AllStatusIcon />} onClick={() => {}} active>
                    All
                  </GeneralTab>
                  <GeneralTab
                    id="forReview"
                    icon={<ForReviewIcon />}
                    onClick={() => {}}
                    active={false}
                  >
                    For review
                  </GeneralTab>
                  <GeneralTab
                    id="accepted"
                    icon={<AcceptedIcon />}
                    onClick={() => {}}
                    active={false}
                  >
                    Accepted
                  </GeneralTab>
                  <GeneralTab
                    id="excluded"
                    icon={<ExcludedIcon />}
                    onClick={() => {}}
                    active={false}
                  >
                    Excluded
                  </GeneralTab>
                  <GeneralTab id="deleted" icon={<TrashIcon />} onClick={() => {}} active={false}>
                    Deleted
                  </GeneralTab>
                </GeneralTabWrapper>
                <TableNew
                  noResults={false}
                  className={classNames(spacing.mt24, spacing.ml24, spacing.pr24)}
                  tableHead={
                    <TableHeadNew>
                      {tableHead.map((item, index) => (
                        <TableTitleNew
                          key={index}
                          minWidth={item.minWidth as TableCellWidth}
                          flexgrow={item.flexgrow as TableTitleNewProps['flexgrow']}
                          padding={item.padding as TableTitleNewProps['padding']}
                          align={item.align as TableTitleNewProps['align']}
                          title={item.title}
                          sorting={item.sorting}
                          onClick={() => {}}
                        />
                      ))}
                    </TableHeadNew>
                  }
                >
                  <TableBodyNew>
                    <ContentLoader isTableBody height="60px" isLoading={isLoading}>
                      {Array.from(Array(39)).map((_, index) => (
                        <TableRowNew key={index} gap="8">
                          <TableCellNew minWidth="48" justifycontent="center" alignitems="center">
                            <Checkbox name={`line-${index}`} checked onChange={() => {}} />
                          </TableCellNew>
                          <TableCellTextNew
                            minWidth="150"
                            value="ID 000001"
                            secondaryValue="08 September 2022"
                            type="caption-semibold"
                            secondaryType="text-regular"
                            tagColor="green"
                            tagIcon={<ArrowDownIcon />}
                          />
                          <TableCellNew minWidth="330" alignitems="center">
                            <TagSegmented
                              {...bankAccounts}
                              tooltipMessage={
                                <Container flexdirection="column">
                                  <Text color="white-100" style={{ wordBreak: 'break-all' }}>
                                    {bankAccounts.mainLabel}
                                  </Text>
                                  <Text color="white-100" style={{ wordBreak: 'break-all' }}>
                                    {bankAccounts.secondLabel} | {bankAccounts.thirdLabel}
                                  </Text>
                                </Container>
                              }
                            />
                          </TableCellNew>
                          <TableCellTextNew
                            minWidth="140"
                            type="text-regular"
                            value="Obrio Limited"
                          />
                          <TableCellNew minWidth="230" justifycontent="center">
                            {index !== 3 ? (
                              <Tag
                                text="Account name"
                                icon={<FlagIcon />}
                                iconPosition="left"
                                additionalIconRight={<HandIcon />}
                                additionalIconTooltip="Rule name"
                              />
                            ) : (
                              <DropDown
                                flexdirection="column"
                                padding="8"
                                control={({ handleOpen, isOpen }) => (
                                  <Button
                                    onClick={handleOpen}
                                    iconRight={isOpen ? <ChevronUp /> : <ChevronDown />}
                                    height="24"
                                    background={'blue-10'}
                                    color="violet-90"
                                    font="caption-semibold"
                                  >
                                    Add
                                  </Button>
                                )}
                              >
                                <DropDownButton
                                  size="204"
                                  onClick={() => {}}
                                  icon={<VectorIcon />}
                                  fontType="caption-semibold"
                                >
                                  Split
                                </DropDownButton>
                                <DropDownButton
                                  size="204"
                                  onClick={() => {}}
                                  icon={<FlagIcon />}
                                  fontType="caption-semibold"
                                >
                                  Account
                                </DropDownButton>
                              </DropDown>
                            )}
                          </TableCellNew>
                          {index !== 3 ? (
                            <TableCellTextNew
                              minWidth="200"
                              align="center"
                              type="text-regular"
                              value={
                                index % 2 === 0
                                  ? 'Loooooooooooong Counterparty naaaaaaaaaaaame'
                                  : 'Counterparty'
                              }
                            />
                          ) : (
                            <TableCellNew minWidth="200" justifycontent="center">
                              <Button
                                height="24"
                                background={'blue-10'}
                                color="violet-90"
                                font="caption-semibold"
                                onClick={() => {}}
                              >
                                Add
                              </Button>
                            </TableCellNew>
                          )}
                          <TableCellTextNew
                            minWidth="230"
                            flexgrow="1"
                            type="text-regular"
                            value="GOOGLE DESADWORDS73 IDUS0028XUIK INDNInternet Website Limit CO IDF770493581 WEB"
                            lineClamp="2"
                          />
                          <TableCellNew minWidth="100">
                            {index % 2 === 0 && index !== 4 && (
                              <IconButton
                                size="24"
                                icon={statusColumn.forReview.icon}
                                color={statusColumn.forReview.color}
                                background={statusColumn.forReview.background}
                                tooltip={statusColumn.forReview.tooltipMessage}
                                onClick={() => {}}
                              />
                            )}
                            {index % 2 !== 0 && index !== 3 && (
                              <IconButton
                                size="24"
                                icon={statusColumn.accepted.icon}
                                color={statusColumn.accepted.color}
                                background={statusColumn.accepted.background}
                                tooltip={statusColumn.accepted.tooltipMessage}
                                onClick={() => {}}
                              />
                            )}
                            {index === 3 && (
                              <IconButton
                                size="24"
                                icon={statusColumn.excluded.icon}
                                color={statusColumn.excluded.color}
                                background={statusColumn.excluded.background}
                                tooltip={statusColumn.excluded.tooltipMessage}
                                onClick={() => {}}
                              />
                            )}
                            {index === 4 && (
                              <IconButton
                                size="24"
                                icon={statusColumn.deleted.icon}
                                color={statusColumn.deleted.color}
                                background={statusColumn.deleted.background}
                                tooltip={statusColumn.deleted.tooltipMessage}
                                onClick={() => {}}
                              />
                            )}
                          </TableCellNew>
                          <TableCellTextNew
                            minWidth="160"
                            type="caption-semibold"
                            align="right"
                            color="green-90"
                            value="+ 1,000,000.20 $"
                            secondaryValue="+ 1,323,000.04 $"
                            secondaryColor="grey-90"
                            secondaryAlign="right"
                          />
                          <TableCellNew minWidth="140" alignitems="center" justifycontent="center">
                            <DropDown
                              flexdirection="column"
                              padding="8"
                              control={({ handleOpen }) => (
                                <IconButton
                                  icon={<DropdownIcon />}
                                  onClick={handleOpen}
                                  background={'transparent'}
                                  color="grey-100-violet-90"
                                />
                              )}
                            >
                              <DropDownButton size="160" icon={<EyeIcon />} onClick={() => {}}>
                                Open
                              </DropDownButton>
                              <DropDownButton size="160" icon={<EditIcon />} onClick={() => {}}>
                                Edit Split
                              </DropDownButton>
                              <DropDownButton size="160" icon={<EyeIcon />} onClick={() => {}}>
                                Open journal entries
                              </DropDownButton>
                              <Divider className={spacing.mY8} />
                              <DropDownButton
                                size="160"
                                icon={<ForReviewIcon />}
                                onClick={() => {}}
                              >
                                For review
                              </DropDownButton>
                            </DropDown>
                          </TableCellNew>
                        </TableRowNew>
                      ))}
                    </ContentLoader>
                  </TableBodyNew>
                </TableNew>
                <ActionBar>
                  <Container gap="40">
                    <Container alignitems="center" gap="12">
                      <Container
                        className={classNames(spacing.w16, spacing.h16)}
                        background={'grey-100'}
                        border="grey-90"
                        alignitems="center"
                        justifycontent="center"
                        borderRadius="2"
                      >
                        <Icon icon={<MinusIcon />} />
                      </Container>
                      <Text type="body-regular-14" color={'grey-90'}>
                        Selected lines:&nbsp; 4
                      </Text>
                    </Container>

                    <Container gap="8" alignitems="center">
                      <Button
                        background={'black-90-grey-20'}
                        color="grey-20-black-90"
                        width="88"
                        borderRadius="10"
                        height="32"
                        iconLeft={<TrashIcon />}
                        onClick={() => {}}
                      >
                        Exclude
                      </Button>
                      <Divider type="vertical" height="20" />
                      <Button
                        background={'black-90-grey-20'}
                        color="grey-20-black-90"
                        borderRadius="10"
                        width="auto"
                        height="32"
                        iconLeft={<TrashIcon />}
                        onClick={() => {}}
                      >
                        Delete split
                      </Button>
                      <Divider type="vertical" height="20" />
                      <Button
                        background={'black-90-grey-20'}
                        color="grey-20-black-90"
                        borderRadius="10"
                        width="auto"
                        height="32"
                        iconLeft={<TrashIcon />}
                        onClick={() => {}}
                      >
                        Delete transaction
                      </Button>
                    </Container>
                  </Container>
                </ActionBar>
                <Pagination
                  action={
                    <DropDown
                      opened={isOpen}
                      currentStateInfo={handleDropdownStateOnChange}
                      position="top-left"
                      flexdirection="column"
                      padding="16"
                      control={({ handleOpen }) => (
                        <Button
                          iconLeft={<SummaryIcon />}
                          onClick={handleOpen}
                          background={'grey-10-grey-20'}
                          color="grey-100"
                        >
                          Sum
                        </Button>
                      )}
                    >
                      <Container flexdirection="column" gap="12" className={spacing.w260fixed}>
                        <Container justifycontent="space-between" alignitems="center">
                          <Text type="caption-semibold" color="grey-100">
                            Sum
                          </Text>
                          <IconButton
                            size="minimal"
                            icon={<ClearIcon />}
                            background={'transparent'}
                            color="grey-100-black-90"
                            tooltip="Close"
                            onClick={onClose}
                          />
                        </Container>
                        <Text color="grey-100">Total amount in currency</Text>
                        <Container
                          flexdirection="column"
                          gap="8"
                          customScroll
                          className={spacing.h64}
                        >
                          <Text type="caption-semibold" color="green-90">
                            + £ 1,000,000.20 | GBP
                          </Text>
                          <Text type="caption-semibold" color="green-90">
                            + $ 1,000,000.20 | EUR
                          </Text>
                          <Text type="caption-semibold" color="red-90">
                            + £ 1,000,000.20 | GBP
                          </Text>
                          <Text type="caption-semibold" color="red-90">
                            + $ 1,000,000.20 | EUR
                          </Text>
                          <Text type="caption-semibold" color="green-90">
                            + £ 1,000,000.20 | GBP
                          </Text>
                          <Text type="caption-semibold" color="green-90">
                            + $ 1,000,000.20 | EUR
                          </Text>
                          <Text type="caption-semibold" color="red-90">
                            + £ 1,000,000.20 | GBP
                          </Text>
                          <Text type="caption-semibold" color="red-90">
                            + $ 1,000,000.20 | EUR
                          </Text>
                        </Container>
                        <Divider fullHorizontalWidth />
                        <Text color="grey-100">Total amount in base currency</Text>
                        <Container customScroll>
                          <Text type="caption-semibold" color="green-90">
                            + $ 1,000,000.20 | EUR
                          </Text>
                        </Container>
                      </Container>
                    </DropDown>
                  }
                />
              </>
            )}
            {storyState === 'empty' && (
              <MainPageEmpty
                title="Create new Transactions"
                description="You have no transactions created. Your created transactions will be displayed here."
                action={
                  <>
                    <Button
                      width="220"
                      height="40"
                      background={'blue-10-blue-20'}
                      color="violet-90-violet-100"
                      iconLeft={<PlusIcon />}
                      onClick={() => {}}
                    >
                      Create Transactions
                    </Button>
                    <Button
                      width="220"
                      height="40"
                      background={'blue-10-blue-20'}
                      color="violet-90-violet-100"
                      iconLeft={<UploadIcon />}
                      onClick={() => {}}
                    >
                      Import Transactions
                    </Button>
                  </>
                }
              />
            )}
          </MainPageContentContainer>
          <LoadingLine loadingCompleted={storyState !== 'loading'} />
        </ContainerMain>
      </Container>
    </>
  );
};

export default {
  title: 'Pages/Banking/Transactions/Banking Transactions Main Page',
  component: BankingTransactionsMainPageComponent,
  parameters: {
    layout: 'fullscreen',
  },
  argTypes: {
    storyState: {
      options: ['empty', 'loading', 'loaded'],
      control: {
        type: 'radio',
        labels: {
          empty: 'Empty page',
          loading: 'Page is loading',
          loaded: 'Page loaded',
        },
      },
    },
  },
} as Meta;

export const BankingTransactionsMainPage = BankingTransactionsMainPageComponent.bind({});
BankingTransactionsMainPage.args = {
  storyState: 'loaded',
};
