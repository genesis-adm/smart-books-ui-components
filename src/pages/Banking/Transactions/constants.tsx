import { Checkbox } from 'Components/base/Checkbox';
import { BackgroundColorStaticNewTypes } from 'Components/types/colorTypes';
import { ReactComponent as ForReviewIcon } from 'Svg/v2/16/document-search.svg';
import { ReactComponent as AcceptedIcon } from 'Svg/v2/16/tick-in-filled-circle.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trash.svg';
import { ReactComponent as ExcludedIcon } from 'Svg/v2/16/warning.svg';
import handleAccountLength from 'Utils/handleAccountLength';
import React, { ReactNode } from 'react';

export const bankAccounts = {
  mainLabel: 'Mono',
  secondLabel: handleAccountLength('4830QZ995647769596875246278', 8),
  thirdLabel: 'USD',
};

export const enum TransactionStatus {
  forReview = 'forReview',
  accepted = 'accepted',
  excluded = 'excluded',
  deleted = 'deleted',
}
export const sorting = {
  sortType: null,
  tooltipText: 'Sorting tooltip text',
};
export const tableHead = [
  {
    minWidth: '48',
    title: <Checkbox name="tickAll" checked onChange={() => {}} />,
    padding: 'none',
  },
  {
    minWidth: '150',
    title: 'ID / Date',
    sorting,
  },
  {
    minWidth: '330',
    title: 'Bank account',
    sorting,
  },
  {
    minWidth: '140',
    title: 'Legal entity',
    sorting,
  },
  {
    minWidth: '230',
    title: 'Template account',
    sorting,
    align: 'center',
  },
  {
    minWidth: '200',
    title: 'Counterparty',
    align: 'center',
  },
  {
    minWidth: '230',
    title: 'Description',
    flexgrow: '1',
  },
  {
    minWidth: '100',
    title: 'Status',
  },
  {
    minWidth: '160',
    title: 'Amount in currency',
    sorting,
    align: 'center',
  },
  {
    minWidth: '140',
    title: 'Action',
    align: 'center',
  },
];

export const statusColumn: Record<
  TransactionStatus,
  {
    icon: ReactNode;
    tooltipMessage: string;
    color: BackgroundColorStaticNewTypes;
    background: BackgroundColorStaticNewTypes;
  }
> = {
  [TransactionStatus.forReview]: {
    icon: <ForReviewIcon />,
    tooltipMessage: 'For review',
    color: 'grey-90',
    background: 'grey-20',
  },
  [TransactionStatus.accepted]: {
    icon: <AcceptedIcon />,
    tooltipMessage: 'Accepted',
    color: 'green-90',
    background: 'green-10',
  },
  [TransactionStatus.excluded]: {
    icon: <ExcludedIcon />,
    tooltipMessage: 'Excluded',
    color: 'yellow-90',
    background: 'yellow-10',
  },
  [TransactionStatus.deleted]: {
    icon: <TrashIcon />,
    tooltipMessage: 'Deleted',
    color: 'red-90',
    background: 'red-10',
  },
};
