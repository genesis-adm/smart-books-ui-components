import { Checkbox } from 'Components/base/Checkbox';
import { ReactComponent as VectorIcon } from 'Svg/16/vector.svg';
import React from 'react';

export const addSplitTableHead = [
  {
    minWidth: '544',
    minWidthWhileSplitByTAAndBU: '290',
    title: 'Account',
  },
  {
    minWidth: '254',
    minWidthWhileSplitByTAAndBU: '254',
    title: 'Business unit',
    helpText: 'Text',
    hideOnSplitByTemplateAccount: true,
  },
  {
    minWidth: '155',
    minWidthWhileSplitByTAAndBU: '155',
    title: 'Amount',
    align: 'right',
    icon: <VectorIcon />,
  },
  {
    minWidth: '100',
    minWidthWhileSplitByTAAndBU: '100',
    title: 'Percent',
    align: 'right',
    icon: '%',
  },
  {
    minWidth: '40',
    minWidthWhileSplitByTAAndBU: '40',
  },
];

export const addSplitTableHeadInRulesModal = [
  {
    minWidth: '560',
    minWidthWhileSplitByTAAndBU: '380',
    title: 'Account',
    applyFlexBasis: true,
  },
  {
    minWidth: '220',
    minWidthWhileSplitByTAAndBU: '320',
    title: 'Business unit',
    hideOnSplitByTemplateAccount: true,
    showInfoIcon: true,
  },
  {
    minWidth: '240',
    minWidthWhileSplitByTAAndBU: '100',
    title: 'Percent',
    align: 'right',
    icon: '%',
  },
  {
    minWidth: '40',
    minWidthWhileSplitByTAAndBU: '40',
  },
];

const sorting = {
  sortType: null,
  tooltipText: 'Sorting tooltip text',
};

export const importOfBankTransactionTableHead = [
  {
    minWidth: '48',
    title: <Checkbox name="tickAll" checked tick="unselect" onChange={() => {}} />,
    padding: 'none',
  },
  {
    minWidth: '160',
    title: 'Bank transaction ID',
    sorting,
  },
  {
    minWidth: '220',
    title: 'Error notification',
    flexgrow: '1',
  },
];
