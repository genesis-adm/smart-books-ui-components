import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownFooter } from 'Components/SelectDropdown/components/SelectDropdownFooter';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { Button } from 'Components/base/buttons/Button';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { Search } from 'Components/inputs/Search';
import { Select } from 'Components/inputs/Select';
import { OptionWithTwoLabels } from 'Components/inputs/Select/options/OptionWithTwoLabels';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { options } from 'Mocks/fakeOptions';
import { ReactComponent as VectorIcon } from 'Svg/16/vector.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Banking/Transactions Modals',
};
export const AddTemplateAccountModal: React.FC = () => {
  const [searchValue, setSearchValue] = useState('');

  return (
    <Modal
      size="630"
      height="336"
      centered
      pluginScrollDisabled
      overlay="black-100"
      header={
        <ModalHeaderNew
          background="grey-10"
          title="Add Template account"
          titlePosition="center"
          fontType="body-medium"
          border
          onClose={() => {}}
        />
      }
      footer={
        <ModalFooterNew border>
          <Container gap="16">
            <LinkButton icon={<VectorIcon />} onClick={() => {}}>
              Add split
            </LinkButton>
            <Button width="md" background="grey-90" onClick={() => {}}>
              Save
            </Button>
          </Container>
        </ModalFooterNew>
      }
    >
      <Container className={classNames(spacing.p24)}>
        <Select label="Account" placeholder="Select option" type="baseWithCategory">
          {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
            <SelectDropdown selectElement={selectElement}>
              <SelectDropdownBody>
                <Search
                  className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                  width="full"
                  height="40"
                  name="search-account"
                  placeholder="Search Account"
                  value={searchValue}
                  onChange={(e) => setSearchValue(e.target.value)}
                  searchParams={[]}
                  onSearch={() => {}}
                  onClear={() => {}}
                />
                <SelectDropdownItemList>
                  {options.map((value, index) => (
                    <OptionWithTwoLabels
                      id={value.label}
                      key={index}
                      tooltip
                      labelDirection="horizontal"
                      label={value?.label}
                      secondaryLabel={value.subLabel}
                      selected={!Array.isArray(selectValue) && selectValue?.value === value.value}
                      onClick={() => {
                        onOptionClick({
                          value: value?.value,
                          label: value?.label,
                          subLabel: value?.subLabel,
                        });
                        closeDropdown();
                      }}
                    />
                  ))}
                </SelectDropdownItemList>
              </SelectDropdownBody>
              <SelectDropdownFooter>
                <Container justifycontent="flex-end" className={classNames(spacing.p16)}>
                  <LinkButton icon={<PlusIcon />} onClick={() => {}}>
                    Create New Template Account
                  </LinkButton>
                </Container>
              </SelectDropdownFooter>
            </SelectDropdown>
          )}
        </Select>
      </Container>
    </Modal>
  );
};
