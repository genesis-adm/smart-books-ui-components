import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { Search } from 'Components/inputs/Search';
import { Select } from 'Components/inputs/Select';
import { OptionWithFourLabels } from 'Components/inputs/Select/options/OptionWithFourLabels';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { options } from 'Mocks/fakeOptions';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Banking/Transactions Modals',
};
export const AddCounterpartyModal: React.FC = () => {
  const [searchValue, setSearchValue] = useState('');

  return (
    <Modal
      size="630"
      height="336"
      centered
      pluginScrollDisabled
      overlay="black-100"
      header={
        <ModalHeaderNew
          background="grey-10"
          title="Add Counterparty"
          titlePosition="center"
          fontType="body-medium"
          border
          onClose={() => {}}
        />
      }
      footer={
        <ModalFooterNew border>
          <Button width="md" background="grey-90" onClick={() => {}}>
            Save
          </Button>
        </ModalFooterNew>
      }
    >
      <Container className={classNames(spacing.p24)}>
        <Select label="Counterparty" placeholder="Select option" type="baseWithCategory">
          {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
            <SelectDropdown selectElement={selectElement}>
              <SelectDropdownBody>
                <Search
                  className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                  width="full"
                  height="40"
                  name="search-counterparty"
                  placeholder="Search Counterparty"
                  value={searchValue}
                  onChange={(e) => setSearchValue(e.target.value)}
                  searchParams={[]}
                  onSearch={() => {}}
                  onClear={() => {}}
                />
                <SelectDropdownItemList>
                  {options.map((value, index) => (
                    <OptionWithFourLabels
                      key={index}
                      label={value?.label}
                      secondaryLabel={value.secondaryLabel}
                      tertiaryLabel={value.tertiaryLabel}
                      selected={selectValue === value}
                      onClick={() => {
                        onOptionClick(value);
                        closeDropdown();
                      }}
                    />
                  ))}
                </SelectDropdownItemList>
              </SelectDropdownBody>
            </SelectDropdown>
          )}
        </Select>
      </Container>
    </Modal>
  );
};
