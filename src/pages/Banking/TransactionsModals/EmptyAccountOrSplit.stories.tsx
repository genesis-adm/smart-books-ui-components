import { Divider } from 'Components/base/Divider';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as ManWithPaperImage } from 'Svg/man-with-paper.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Banking/Transactions Modals',
};
export const EmptyAccountOrSplit: React.FC = () => (
  <Container
    justifycontent="center"
    alignitems="center"
    flexdirection="column"
    gap="16"
    className={classNames(spacing.pY60)}
  >
    <ManWithPaperImage />
    <Container flexdirection="column" gap="8" alignitems="center">
      <Text type="body-medium">Add Account or Split</Text>
      <Text type="body-regular-16" color="grey-100">
        You may split amount by several accounts
      </Text>
    </Container>
    <Container gap="16">
      <LinkButton icon={<PlusIcon />} type="button" onClick={() => {}}>
        Add Template account
      </LinkButton>
      <Divider type="vertical" height="auto" />
      <LinkButton icon={<PlusIcon />} type="button" onClick={() => {}}>
        Add Split
      </LinkButton>
    </Container>
  </Container>
);
