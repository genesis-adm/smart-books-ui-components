import { Meta, Story } from '@storybook/react';
import { Tag } from 'Components/base/Tag';
import { Container } from 'Components/base/grid/Container';
import { OptionSingleNew } from 'Components/base/inputs/SelectNew/options/OptionSingleNew';
import { Text } from 'Components/base/typography/Text';
import ImportModalFooter from 'Components/custom/Stories/ImportSharedComponents/ImportModalFooter';
import MatchColumnsSelect from 'Components/custom/Stories/MatchColumnsSelect/MatchColumnsSelect';
import { Modal } from 'Components/modules/Modal';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { options } from 'Mocks/fakeOptions';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

const fields = [
  { name: 'Bank transaction ID' },
  { name: 'Type' },
  { name: 'Template account ID' },
  { name: 'Business unit name' },
  { name: 'Legal entity name' },
  { name: 'Amount' },
];
const ImportOfBanKTransactionMatchStep2Component: Story = () => {
  return (
    <Modal
      overlay="grey-10"
      size="full"
      padding="none"
      background={'grey-10'}
      pluginScrollDisabled
      footer={<ImportModalFooter modalType="matchColumns" />}
      header={
        <ModalHeaderNew
          background={'grey-10'}
          title="Import Bank transaction match"
          icon={
            <Tag
              color="grey"
              text="File name: name file 09.10.20.xls"
              padding="4-8"
              cursor="default"
            />
          }
          border
          closeActionBackgroundColor="inherit"
          onClose={() => {}}
        />
      }
    >
      <Container
        background={'white-100'}
        flexdirection="column"
        radius
        flexgrow="1"
        className={classNames(spacing.p24, spacing.mX20, spacing.mY16)}
        customScroll
        gap="24"
      >
        <Text type="body-regular-16">
          Map columns from&nbsp;
          <Text type="body-medium" display="inline-flex">
            namefile 09.10.20.xls
          </Text>
          &nbsp;for each Smart Books field
        </Text>
        <Container
          gap="16"
          flexwrap="wrap"
          flexdirection="column"
          className={classNames(spacing.w1250, spacing.h300)}
        >
          {fields.map(({ name }) => (
            <Container gap="16">
              <MatchColumnsSelect
                name={name}
                tooltip="Tooltip text"
                label="Choose column from your file"
                placeholder="Choose option"
                background={'grey-10'}
                textWidth="160"
                required
                width="320"
              >
                {({ onClick, state, closeDropdown }) => (
                  <>
                    {options.map(({ label, value }) => (
                      <OptionSingleNew
                        key={value}
                        label={label}
                        selected={state?.value === value}
                        onClick={() => {
                          onClick({
                            label,
                            value,
                          });
                          closeDropdown?.();
                        }}
                      />
                    ))}
                  </>
                )}
              </MatchColumnsSelect>
            </Container>
          ))}
        </Container>
      </Container>
    </Modal>
  );
};

export default {
  title: 'Pages/Banking/Transactions Modals/Import ',
  component: ImportOfBanKTransactionMatchStep2Component,
} as Meta;
export const ImportOfBankTransactionsMatchStep2 = ImportOfBanKTransactionMatchStep2Component.bind(
  {},
);
