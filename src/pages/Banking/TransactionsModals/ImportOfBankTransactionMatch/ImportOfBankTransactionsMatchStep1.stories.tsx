import { Meta, Story } from '@storybook/react';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import ImportModalFooter from 'Components/custom/Stories/ImportSharedComponents/ImportModalFooter';
import UploadFile from 'Components/custom/Stories/Upload/UploadFile';
import UploadInstruction from 'Components/custom/Stories/Upload/UploadInstruction';
import { Modal } from 'Components/modules/Modal';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import React, { useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

const ImportOfBankTransactionsMatchStep1Component: Story = () => {
  const [file, setFile] = useState<File | null>(null);

  return (
    <Modal
      size="md-extrawide"
      padding="none"
      header={<ModalHeaderNew onClose={() => {}} closeActionBackgroundColor="grey-10" />}
      footer={<ImportModalFooter isActive={!!file} modalType="upload" hideExportListButton />}
    >
      <Container flexdirection="column" alignitems="center">
        <Text type="h2-semibold" align="center">
          Import Bank transaction match
        </Text>
        <UploadFile file={file} setFile={setFile} className={spacing.mt40} />
        <UploadInstruction />
      </Container>
    </Modal>
  );
};

export default {
  title: 'Pages/Banking/Transactions Modals/Import ',
  component: ImportOfBankTransactionsMatchStep1Component,
} as Meta;
export const ImportOfBankTransactionsMatchStep1 = ImportOfBankTransactionsMatchStep1Component.bind(
  {},
);
