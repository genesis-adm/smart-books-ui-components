import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ReactComponent as ArrowLeftIcon } from 'Svg/v2/16/arrow-left.svg';
import { ReactComponent as CopyIcon } from 'Svg/v2/24/copy.svg';
import React, { useState } from 'react';

type ImportOfBankTransactionsMatchFooterProps = {
  modalState?: 'loaded';
};
const ImportOfBankTransactionMatchFooter = ({
  modalState,
}: ImportOfBankTransactionsMatchFooterProps) => {
  const [isCopiedToClipboard, setIsCopiedToClipboard] = useState<boolean>(false);

  return (
    <ModalFooterNew justifycontent="space-between" border background={'grey-10'}>
      <Container alignitems="center" gap="16" justifycontent="flex-end">
        <Container flexdirection="column">
          <Text type="body-regular-14" color="grey-100">
            Step: 3 of 3
          </Text>
          <Text type="body-regular-14" color="grey-100">
            Import results
          </Text>
        </Container>
        <Button
          height="50"
          navigation
          width="230"
          color="black-100"
          background={'white-100'}
          border="grey-20-grey-90"
        >
          Close
        </Button>
      </Container>
      {modalState === 'loaded' && (
        <>
          {isCopiedToClipboard ? (
            <Button
              background={'black-100'}
              iconLeft={<CopyIcon />}
              height="48"
              width="150"
              borderRadius="16"
            >
              Copied!
            </Button>
          ) : (
            <Button
              background={'black-100'}
              iconLeft={<CopyIcon />}
              height="48"
              width="215"
              borderRadius="16"
              onClick={() => setIsCopiedToClipboard(true)}
            >
              Copy to clipboard
            </Button>
          )}
        </>
      )}
      <Button
        height="50"
        width="150"
        navigation
        iconLeft={<ArrowLeftIcon />}
        background={'white-100'}
        color="black-100"
        border="grey-20-grey-90"
      >
        Back
      </Button>
    </ModalFooterNew>
  );
};

export default ImportOfBankTransactionMatchFooter;
