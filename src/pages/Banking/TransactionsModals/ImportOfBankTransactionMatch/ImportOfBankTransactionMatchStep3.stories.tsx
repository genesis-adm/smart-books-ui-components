import { Meta, Story } from '@storybook/react';
import { Checkbox } from 'Components/base/Checkbox';
import { Divider } from 'Components/base/Divider';
import { Tag } from 'Components/base/Tag';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { Modal } from 'Components/modules/Modal';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { TableCellWidth } from 'Components/types/gridTypes';
import ImportOfBankTransactionMatchFooter from 'Pages/Banking/TransactionsModals/ImportOfBankTransactionMatch/components/ImportOfBankTransactionMatchFooter';
import SuccessImportOfBankTransaction from 'Pages/Banking/TransactionsModals/components/SuccessImportOfBankTransaction';
import { importOfBankTransactionTableHead } from 'Pages/Banking/TransactionsModals/constants';
import React from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

const ImportOfBankTransactionMatchStep3Component: Story = ({ storyState }) => {
  return (
    <Modal
      overlay="grey-10"
      size="full"
      background={'grey-10'}
      pluginScrollDisabled
      header={
        <ModalHeaderNew
          background={'grey-10'}
          title="Import Bank transaction match"
          icon={
            <Tag
              color="grey"
              text="File name: name file 09.10.20.xls"
              padding="4-8"
              cursor="default"
            />
          }
          border
          closeActionBackgroundColor="inherit"
          onClose={() => {}}
        />
      }
      footer={<ImportOfBankTransactionMatchFooter modalState={storyState} />}
    >
      <Container flexdirection="column" radius flexgrow="1" overflow="hidden">
        {storyState === 'loaded' && (
          <>
            <Container alignitems="center" justifycontent="flex-end">
              <Container gap="8">
                <Text type="text-medium" color="grey-100">
                  Number of successfully imported
                </Text>
                <Text type="caption-semibold" color="green-90">
                  2
                </Text>
              </Container>
              <Divider
                type="vertical"
                height="20"
                background={'grey-90'}
                className={spacing.mX16}
              />
              <Container gap="8">
                <Text type="text-medium" color="grey-100">
                  Number of not valid records
                </Text>
                <Text type="caption-semibold" color="red-90">
                  3
                </Text>
              </Container>
            </Container>
            <TableNew
              noResults={false}
              className={spacing.mt24}
              tableHead={
                <TableHeadNew>
                  {importOfBankTransactionTableHead.map((item, index) => (
                    <TableTitleNew
                      key={index}
                      minWidth={item.minWidth as TableCellWidth}
                      padding={item.padding as TableTitleNewProps['padding']}
                      flexgrow={item.flexgrow as TableTitleNewProps['flexgrow']}
                      title={item.title}
                      sorting={item.sorting}
                      onClick={() => {}}
                    />
                  ))}
                </TableHeadNew>
              }
            >
              <TableBodyNew>
                {Array.from(Array(39)).map((_, index) => (
                  <TableRowNew key={index} background={'white'} size={'52'}>
                    <TableCellNew
                      minWidth={importOfBankTransactionTableHead[0].minWidth as TableCellWidth}
                      justifycontent="center"
                      alignitems="center"
                    >
                      <Checkbox name={`line-${index}`} checked={true} onChange={() => {}} />
                    </TableCellNew>
                    <TableCellTextNew
                      minWidth={importOfBankTransactionTableHead[1].minWidth as TableCellWidth}
                      value="ID 123456"
                      type="text-regular"
                      color="red-100"
                    />
                    <TableCellTextNew
                      minWidth={importOfBankTransactionTableHead[1].minWidth as TableCellWidth}
                      value="“Not allowed to import split to bank transactions with status other than “for review. Please check you records once again”"
                      type="text-regular"
                      flexgrow="1"
                      color="red-100"
                    />
                  </TableRowNew>
                ))}
              </TableBodyNew>
            </TableNew>
          </>
        )}
        {storyState === 'success' && <SuccessImportOfBankTransaction />}
      </Container>
    </Modal>
  );
};

export default {
  title: 'Pages/Banking/Transactions Modals/Import ',
  component: ImportOfBankTransactionMatchStep3Component,
  argTypes: {
    storyState: {
      options: ['success', 'loaded'],
      control: {
        type: 'radio',
        labels: {
          success: 'Successfully imported',
          loaded: 'Page loaded',
        },
      },
    },
  },
} as Meta;
export const ImportOfBankTransactionsMatchStep3 = ImportOfBankTransactionMatchStep3Component.bind(
  {},
);
ImportOfBankTransactionsMatchStep3.args = {
  storyState: 'loaded',
};
