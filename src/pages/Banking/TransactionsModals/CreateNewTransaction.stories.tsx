import { Meta } from '@storybook/react';
import { DropDown } from 'Components/DropDown';
import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownFooter } from 'Components/SelectDropdown/components/SelectDropdownFooter';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { Divider } from 'Components/base/Divider';
import { Tag } from 'Components/base/Tag';
import { Button } from 'Components/base/buttons/Button';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { InputDate } from 'Components/base/inputs/InputDate';
import { NumericInput } from 'Components/base/inputs/NumericInput';
import { OwnNumberFormatValues } from 'Components/base/inputs/NumericInput/NumericInput.types';
import { TextareaNew } from 'Components/base/inputs/TextareaNew';
import { CurrencyTab, CurrencyTabWrapper } from 'Components/base/tabs/CurrencyTab';
import { Text } from 'Components/base/typography/Text';
import { ActionsPopup } from 'Components/elements/ActionsPopup';
import { Currencies } from 'Components/elements/Currencies';
import { Search } from 'Components/inputs/Search';
import { Select } from 'Components/inputs/Select';
import { OptionWithFourLabels } from 'Components/inputs/Select/options/OptionWithFourLabels';
import { OptionWithTwoLabels } from 'Components/inputs/Select/options/OptionWithTwoLabels';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { options } from 'Mocks/fakeOptions';
import { EmptyAccountOrSplit } from 'Pages/Banking/TransactionsModals/EmptyAccountOrSplit.stories';
import AddSplitModal from 'Pages/Banking/TransactionsModals/components/AddSplitModal';
import { ReactComponent as ArrowFilledCircleDown } from 'Svg/16/arrow-filled-circle-down.svg';
import { ReactComponent as ClearIcon } from 'Svg/v2/16/cross.svg';
import { ReactComponent as ForReviewIcon } from 'Svg/v2/16/document-search.svg';
import { ReactComponent as DocumentIcon } from 'Svg/v2/16/file-blank.svg';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as DropdownIcon } from 'Svg/v2/16/three-dots.svg';
import { ReactComponent as AcceptedIcon } from 'Svg/v2/16/tick-in-filled-circle.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trash.svg';
import { ReactComponent as ExcludedIcon } from 'Svg/v2/16/warning.svg';
import handleAccountLength from 'Utils/handleAccountLength';
import classNames from 'classnames';
import React, { useEffect, useState } from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Banking/Transactions Modals',
  argTypes: {
    viewMode: {
      control: { type: 'boolean' },
    },
    viewModeEditSplit: {
      control: { type: 'boolean' },
    },
    emptyAccountOrSplitOnView: {
      control: { type: 'boolean' },
    },
  },
  args: {
    viewMode: false,
    viewModeEditSplit: false,
    emptyAccountOrSplitOnView: false,
  },
} as Meta;

const GENERAL_MODAL_ID = 'bank-transaction';

export const CreateNewTransaction = ({
  viewMode,
  viewModeEditSplit,
  emptyAccountOrSplitOnView,
}: {
  viewMode: boolean;
  viewModeEditSplit: boolean;
  emptyAccountOrSplitOnView: boolean;
}) => {
  const modalTitle = viewMode ? 'View Transaction # 123213' : 'Create new Transaction';

  const [customCurrencyValue, setCustomCurrencyValue] = useState<string>('1.0012');
  const [inputCurrencyValue, setInputCurrencyValue] = useState<string>(customCurrencyValue);

  const [searchValue, setSearchValue] = useState('');

  const [inputValue, setInputValue] = useState<number>();
  const [currentDescriptionValue, setCurrentDescriptionValue] = useState('');
  const [isOpenedAddSplitModal, setIsOpenedAddSplitModal] = useState<boolean>(false);
  const handleChange = ({ floatValue }: OwnNumberFormatValues) => {
    setInputValue(floatValue);
  };

  useEffect(() => {
    setIsOpenedAddSplitModal(viewMode);
  }, [viewMode]);

  const tagInModalHeader = viewMode ? (
    <Tag
      size="32"
      icon={<ForReviewIcon />}
      padding="4-8"
      cursor="default"
      text="For review"
      color="grey"
    />
  ) : (
    <Tag
      size="32"
      icon={<DocumentIcon />}
      padding="4-8"
      cursor="default"
      text="New"
      color="green"
    />
  );

  return (
    <Modal
      size="960"
      modalId={GENERAL_MODAL_ID}
      background="grey-10"
      type="new"
      pluginScrollDisabled={viewModeEditSplit}
      header={
        <Container
          className={classNames(spacing.w100p, spacing.p24)}
          alignitems="center"
          justifycontent="space-between"
          flexwrap="wrap"
          style={{ borderBottom: '1px solid #EDEFF1' }}
        >
          <Container gap="16">
            <Text type="title-bold">{modalTitle}</Text>
            {tagInModalHeader}
          </Container>
          <Container gap="24" alignitems="center">
            <Currencies
              background="white-100"
              selectedCurrency="eur"
              selectedCurrencyValue="1"
              baseCurrency="usd"
              showCustomRate
              baseCurrencyValue={customCurrencyValue}
              baseCurrencyInputValue={inputCurrencyValue}
              onChange={(e) => {
                setInputCurrencyValue(e.target.value);
              }}
              onApply={() => setCustomCurrencyValue(inputCurrencyValue)}
            />
            <CurrencyTabWrapper background="white-100">
              <CurrencyTab id="usd" onClick={() => {}} active>
                USD
              </CurrencyTab>
              <CurrencyTab id="eur" onClick={() => {}} active={false}>
                EUR
              </CurrencyTab>
            </CurrencyTabWrapper>
            {viewMode && (
              <DropDown
                flexdirection="column"
                padding="8"
                gap="4"
                control={({ handleOpen }) => (
                  <IconButton
                    icon={<DropdownIcon />}
                    onClick={handleOpen}
                    background="white-100"
                    color="grey-100-violet-90"
                  />
                )}
              >
                <DropDownButton size="160" icon={<EditIcon />} onClick={() => {}}>
                  Edit
                </DropDownButton>
                <DropDownButton size="160" icon={<AcceptedIcon />} onClick={() => {}}>
                  Accept
                </DropDownButton>
                <DropDownButton size="160" icon={<ExcludedIcon />} onClick={() => {}}>
                  Exclude
                </DropDownButton>
                <Divider type="horizontal" fullHorizontalWidth className={spacing.mY4} />
                <DropDownButton size="204" type="danger" icon={<TrashIcon />} onClick={() => {}}>
                  Delete
                </DropDownButton>
              </DropDown>
            )}
            <IconButton
              size="32"
              icon={<ClearIcon />}
              onClick={() => {}}
              background="white-100"
              color="grey-100"
            />
          </Container>
        </Container>
      }
      footer={
        <ModalFooterNew border background="grey-10">
          <Button width="230" onClick={() => {}} background="grey-90">
            Save
          </Button>
        </ModalFooterNew>
      }
    >
      <Container flexdirection="column" gap="24">
        <Container
          flexdirection="column"
          borderRadius="20"
          background="white-100"
          className={spacing.p24}
          flexgrow="1"
          gap="16"
        >
          <Container gap="8">
            <Text type="body-regular-14" color="black-90">
              Transaction type
            </Text>
            <Tag text="Inflow" icon={<ArrowFilledCircleDown />} color="green" />
          </Container>
          <Container gap="24">
            <Select
              label="Bank account"
              width="502"
              required
              placeholder="Bank account"
              type="bankAccount"
            >
              {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
                <SelectDropdown selectElement={selectElement}>
                  <SelectDropdownBody>
                    <Search
                      className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                      width="full"
                      height="40"
                      name="search-LE"
                      placeholder="Search option"
                      value={searchValue}
                      onChange={(e) => setSearchValue(e.target.value)}
                      searchParams={[]}
                      onSearch={() => {}}
                      onClear={() => {}}
                    />
                    <SelectDropdownItemList>
                      {options.map(
                        ({
                          label,
                          secondaryLabel,
                          tertiaryLabel,
                          quadroLabel,
                          subLabel,
                          value,
                        }) => (
                          <OptionWithFourLabels
                            key={value}
                            label={label}
                            secondaryLabel={secondaryLabel}
                            tertiaryLabel={tertiaryLabel}
                            quadroLabel={quadroLabel}
                            selected={selectValue?.value === value}
                            onClick={() => {
                              onOptionClick({
                                label: label,
                                subLabel: handleAccountLength(subLabel || '', 8),
                                tertiaryLabel: tertiaryLabel,
                                value,
                              });
                              closeDropdown();
                            }}
                          />
                        ),
                      )}
                    </SelectDropdownItemList>
                  </SelectDropdownBody>
                </SelectDropdown>
              )}
            </Select>
            <InputDate
              label="Date"
              name="calendar"
              selected={null}
              calendarStartDay={1}
              width="157"
              onChange={() => {}}
              required
              popperDirection="bottom"
            />
            <NumericInput
              name="num-1"
              limit="positiveOnlyColored"
              width="157"
              value={inputValue}
              onValueChange={handleChange}
              label="Amount"
              required
            />
          </Container>
          {viewModeEditSplit && (
            <Modal
              size="912"
              containerId={GENERAL_MODAL_ID}
              overlay="white-100"
              header={
                <ModalHeaderNew
                  title="Edit split"
                  titlePosition="center"
                  height="60"
                  border
                  onClose={() => {}}
                />
              }
              footer={
                <ModalFooterNew border direction="normal">
                  <Container gap="39">
                    <Container flexdirection="column" gap="4">
                      <Text type="caption-regular" color="grey-100">
                        Split amount
                      </Text>
                      <Text type="caption-semibold">1.200 $</Text>
                    </Container>
                    <Container flexdirection="column" gap="4">
                      <Text type="caption-regular" color="grey-100">
                        Original amount
                      </Text>
                      <Text type="caption-semibold">1.200 $</Text>
                    </Container>
                    <Container flexdirection="column" gap="4">
                      <Text type="caption-regular" color="grey-100">
                        Difference
                      </Text>
                      <Text type="caption-semibold" color="green-90">
                        0.00
                      </Text>
                    </Container>
                  </Container>
                  <Button width="md" height="40" background="grey-90" onClick={() => {}}>
                    Save
                  </Button>
                </ModalFooterNew>
              }
            >
              <Container className={classNames(spacing.h280fixed, spacing.w100p)}>
                <AddSplitModal type="modal" />
              </Container>
            </Modal>
          )}
          <Container gap="16">
            <Container gap="4">
              <Text color="grey-100" type="caption-regular">
                Legal entity:
              </Text>
              <Text type="caption-regular">BetterMe Limited </Text>
            </Container>
            <Container gap="4">
              <Text color="grey-100" type="caption-regular">
                Business unit:
              </Text>
              <Text type="caption-regular">BetterMe Limited </Text>
            </Container>
          </Container>

          <TextareaNew
            name="description"
            label="Description"
            value={currentDescriptionValue}
            minHeight="128"
            onChange={(e) => {
              setCurrentDescriptionValue(e.target.value);
            }}
            required
          />
          <Divider />
          <Container gap="28" alignitems="center">
            <Text color="black-90" type="body-regular-14" className={classNames(spacing.w108fixed)}>
              Money received from
            </Text>
            <Select
              label="Counterparty"
              width="502"
              placeholder="Select option"
              type="baseWithCategory"
            >
              {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
                <SelectDropdown selectElement={selectElement}>
                  <SelectDropdownBody>
                    <Search
                      className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                      width="full"
                      height="40"
                      name="search-counterparty"
                      placeholder="Search option"
                      value={searchValue}
                      onChange={(e) => setSearchValue(e.target.value)}
                      searchParams={[]}
                      onSearch={() => {}}
                      onClear={() => {}}
                    />
                    <SelectDropdownItemList>
                      {options.map((value, index) => (
                        <OptionWithFourLabels
                          key={index}
                          label={value?.label}
                          secondaryLabel={value.subLabel}
                          tertiaryLabel={value.tertiaryLabel}
                          selected={selectValue === value}
                          onClick={() => {
                            onOptionClick(value);
                            closeDropdown();
                          }}
                        />
                      ))}
                    </SelectDropdownItemList>
                  </SelectDropdownBody>
                </SelectDropdown>
              )}
            </Select>
          </Container>
        </Container>
        <Container
          flexdirection="column"
          borderRadius="20"
          background="white-100"
          className={classNames(spacing.p24, spacing.mb24)}
          flexgrow="1"
          gap="16"
        >
          {!isOpenedAddSplitModal && (
            <Container alignitems="center" justifycontent="space-between">
              <Container gap="28" alignitems="center">
                <Text color="black-90" type="body-regular-14">
                  Choose account
                </Text>
                <Select
                  label="Account"
                  width="502"
                  placeholder="Select option"
                  type="baseWithCategory"
                >
                  {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
                    <SelectDropdown selectElement={selectElement}>
                      <SelectDropdownBody>
                        <Search
                          className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                          width="full"
                          height="40"
                          name="search-account"
                          placeholder="Search Account"
                          value={searchValue}
                          onChange={(e) => setSearchValue(e.target.value)}
                          searchParams={[]}
                          onSearch={() => {}}
                          onClear={() => {}}
                        />
                        <SelectDropdownItemList>
                          {options.map((value, index) => (
                            <OptionWithTwoLabels
                              id={value.label}
                              key={index}
                              tooltip
                              labelDirection="horizontal"
                              label={value?.label}
                              secondaryLabel={value.subLabel}
                              selected={
                                !Array.isArray(selectValue) && selectValue?.value === value.value
                              }
                              onClick={() => {
                                onOptionClick({
                                  value: value?.value,
                                  label: value?.label,
                                  subLabel: value?.subLabel,
                                });
                                closeDropdown();
                              }}
                            />
                          ))}
                        </SelectDropdownItemList>
                      </SelectDropdownBody>
                      <SelectDropdownFooter>
                        <Container justifycontent="flex-end" className={classNames(spacing.p16)}>
                          <LinkButton icon={<PlusIcon />} onClick={() => {}}>
                            Create New Template Account
                          </LinkButton>
                        </Container>
                      </SelectDropdownFooter>
                    </SelectDropdown>
                  )}
                </Select>
              </Container>
              <LinkButton
                icon={<PlusIcon />}
                onClick={() => {
                  setIsOpenedAddSplitModal((prev) => !prev);
                }}
              >
                Add Split
              </LinkButton>
            </Container>
          )}
          {isOpenedAddSplitModal && <AddSplitModal type={viewMode ? 'viewMode' : undefined} />}
        </Container>

        {emptyAccountOrSplitOnView && <EmptyAccountOrSplit />}
        {(viewMode || emptyAccountOrSplitOnView) && (
          <ActionsPopup
            type="actionButtons"
            itemNumber={1}
            itemsAmount={30}
            itemName="Transactions"
            itemId="23824384"
            onPreviousButtonClick={() => {}}
            onNextButtonClick={() => {}}
          />
        )}
      </Container>
    </Modal>
  );
};
