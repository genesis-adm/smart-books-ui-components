import { Icon } from 'Components/base/Icon';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as TableIcon } from 'Svg/v2/180/empty-white-background.svg';
import React from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

const SuccessImportOfBankTransaction = () => (
  <Container
    flexdirection="column"
    alignitems="center"
    justifycontent="center"
    gap="40"
    className={spacing.h100p}
  >
    <Icon icon={<TableIcon />} />
    <Text color="grey-100" type="body-regular-14">
      All your data successfully imported
    </Text>
  </Container>
);

export default SuccessImportOfBankTransaction;
