import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import AddSplitModal from 'Pages/Banking/TransactionsModals/components/AddSplitModal';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Banking/Transactions Modals',
};
export const AddSplitModalStories: React.FC = () => {
  return (
    <Modal
      size="912"
      centered
      overlay="black-100"
      classNameModalMain={spacing.z_ind15}
      header={
        <ModalHeaderNew
          title="Add Split"
          titlePosition="center"
          fontType="body-medium"
          border
          onClose={() => {}}
        />
      }
      footer={
        <ModalFooterNew border className={classNames(spacing.z_ind10)}>
          <Container
            justifycontent="space-between"
            alignitems="center"
            className={classNames(spacing.w100p)}
          >
            <Container gap="39">
              <Container flexdirection="column" gap="4">
                <Text type="caption-regular" color="grey-100">
                  Split amount
                </Text>
                <Text type="caption-semibold">1.200 $</Text>
              </Container>
              <Container flexdirection="column" gap="4">
                <Text type="caption-regular" color="grey-100">
                  Original amount
                </Text>
                <Text type="caption-semibold">1.200 $</Text>
              </Container>
              <Container flexdirection="column" gap="4">
                <Text type="caption-regular" color="grey-100">
                  Difference
                </Text>
                <Text type="caption-semibold" color="green-90">
                  0.00
                </Text>
              </Container>
            </Container>
            <Button width="md" background="grey-90" onClick={() => {}}>
              Save
            </Button>
          </Container>
        </ModalFooterNew>
      }
    >
      <Container className={classNames(spacing.pl24, spacing.pY24, spacing.z_ind15)}>
        <AddSplitModal type="modal" />
      </Container>
    </Modal>
  );
};
