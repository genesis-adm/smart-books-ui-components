import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { Divider } from 'Components/base/Divider';
import { Radio } from 'Components/base/Radio';
import { Tag } from 'Components/base/Tag';
import { IconButton } from 'Components/base/buttons/IconButton';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { NumericInput } from 'Components/base/inputs/NumericInput';
import { OwnNumberFormatValues } from 'Components/base/inputs/NumericInput/NumericInput.types';
import { Text } from 'Components/base/typography/Text';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { Search } from 'Components/inputs/Search';
import { Select } from 'Components/inputs/Select';
import { OptionWithTwoLabels } from 'Components/inputs/Select/options/OptionWithTwoLabels';
import { TableCellWidth } from 'Components/types/gridTypes';
import { options } from 'Mocks/fakeOptions';
import RulesAddSplitTable from 'Pages/Banking/TransactionsModals/components/RulesAddSplitTable';
import { addSplitTableHead } from 'Pages/Banking/TransactionsModals/constants';
import { ReactComponent as HandIcon } from 'Svg/16/hand-filled.svg';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

type AddSplitModalProps = {
  type?: 'modal' | 'viewMode' | 'rules';
};

const AddSplitModal: React.FC<AddSplitModalProps> = ({ type }: AddSplitModalProps) => {
  const [searchValue, setSearchValue] = useState('');
  const [inputValue, setInputValue] = useState<number>();
  const [chosenRadioValue, setChosenRadioValue] = useState<
    'byTemplateAccount' | 'byTemplateAccountAndBU'
  >('byTemplateAccount');

  const [currentRowIndex, setCurrentRowIndex] = useState<number | null>(null);
  const handleChange = ({ floatValue }: OwnNumberFormatValues) => {
    setInputValue(floatValue);
  };

  return (
    <Container flexdirection="column" className={classNames(spacing.w100p)}>
      <Container justifycontent="space-between" alignitems="center">
        <Container gap="16" alignitems="center">
          <Radio
            id="byTemplateAccount"
            color="grey-100"
            type="caption-regular"
            onChange={() => {
              setChosenRadioValue('byTemplateAccount');
            }}
            name="byTemplateAccount"
            checked={chosenRadioValue === 'byTemplateAccount'}
          >
            Split by Template accounts
          </Radio>
          <Radio
            id="byTemplateAccountAndBU"
            color="grey-100"
            type="caption-regular"
            onChange={() => {
              setChosenRadioValue('byTemplateAccountAndBU');
            }}
            name="byTemplateAccountAndBU"
            checked={chosenRadioValue === 'byTemplateAccountAndBU'}
          >
            Split by Template accounts & business units
          </Radio>
        </Container>
        <Container gap="12" alignitems="center">
          {type === 'viewMode' && (
            <>
              <Tag text="Manual" icon={<HandIcon />} color="grey" padding="2-8" />
              <IconButton
                size="24"
                tooltip="Edit split"
                icon={<EditIcon />}
                background="transparent-yellow-10"
                color="grey-90-yellow-90"
                onClick={() => {}}
              />
            </>
          )}
          <IconButton
            size="24"
            tooltip="Delete split"
            icon={<TrashIcon />}
            background="transparent-red-10"
            color="grey-90-red-90"
            onClick={() => {}}
          />
        </Container>
      </Container>
      {type !== 'rules' && (
        <TableNew
          noResults={false}
          className={classNames(spacing.mY24)}
          tableHead={
            <TableHeadNew>
              {addSplitTableHead.map((item, index) => {
                if (item.hideOnSplitByTemplateAccount && chosenRadioValue === 'byTemplateAccount') {
                  return null;
                }

                return (
                  <TableTitleNew
                    key={index}
                    minWidth={
                      chosenRadioValue === 'byTemplateAccount'
                        ? (item.minWidth as TableCellWidth)
                        : (item.minWidthWhileSplitByTAAndBU as TableCellWidth)
                    }
                    icon={item.icon}
                    align={item.align as TableTitleNewProps['align']}
                    title={item.title}
                    helpText={item.helpText}
                    onClick={() => {}}
                  />
                );
              })}
            </TableHeadNew>
          }
        >
          <TableBodyNew>
            {Array.from(Array(2)).map((_, index) => (
              <TableRowNew
                key={index}
                gap="8"
                onMouseEnter={() => setCurrentRowIndex(index)}
                onMouseLeave={() => setCurrentRowIndex(null)}
              >
                <TableCellNew
                  minWidth={
                    chosenRadioValue === 'byTemplateAccount'
                      ? (addSplitTableHead[0].minWidth as TableCellWidth)
                      : (addSplitTableHead[0].minWidthWhileSplitByTAAndBU as TableCellWidth)
                  }
                  fixedWidth
                >
                  <Select
                    label="Choose"
                    placeholder="Choose"
                    height="36"
                    type="baseWithCategory"
                    error={index === 1 ? { messages: ['ErrOr 1', 'Error'] } : undefined}
                  >
                    {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
                      <SelectDropdown
                        selectElement={selectElement}
                        className={spacing.z_ind20}
                        width={chosenRadioValue === 'byTemplateAccount' ? 'full' : '416'}
                      >
                        <SelectDropdownBody>
                          <Search
                            className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                            width="full"
                            height="40"
                            name="search"
                            placeholder="Search option"
                            value={searchValue}
                            onChange={(e) => setSearchValue(e.target.value)}
                            searchParams={[]}
                            onSearch={() => {}}
                            onClear={() => {}}
                          />
                          <SelectDropdownItemList>
                            {options.map((value, index) => (
                              <OptionWithTwoLabels
                                id={value.label}
                                key={index}
                                tooltip
                                labelDirection="horizontal"
                                label={value?.label}
                                secondaryLabel={value.subLabel}
                                selected={
                                  !Array.isArray(selectValue) && selectValue?.value === value.value
                                }
                                onClick={() => {
                                  onOptionClick({
                                    value: value?.value,
                                    label: value?.label,
                                    subLabel: value?.subLabel,
                                  });
                                  closeDropdown();
                                }}
                              />
                            ))}
                          </SelectDropdownItemList>
                        </SelectDropdownBody>
                      </SelectDropdown>
                    )}
                  </Select>
                </TableCellNew>
                {chosenRadioValue === 'byTemplateAccountAndBU' && (
                  <>
                    <TableCellNew minWidth={addSplitTableHead[1].minWidth as TableCellWidth}>
                      <Select
                        label="Choose"
                        placeholder="Choose"
                        height="36"
                        type="column"
                        width="220"
                        error={index === 1 ? { messages: ['ErrOr 1', 'Error'] } : undefined}
                      >
                        {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
                          <SelectDropdown selectElement={selectElement} width="408">
                            <SelectDropdownBody>
                              <Search
                                className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                                width="full"
                                height="40"
                                name="search"
                                placeholder="Search option"
                                value={searchValue}
                                onChange={(e) => setSearchValue(e.target.value)}
                                searchParams={[]}
                                onSearch={() => {}}
                                onClear={() => {}}
                              />
                              <SelectDropdownItemList>
                                {options.map((value, index) => (
                                  <OptionWithTwoLabels
                                    id={value.label}
                                    key={index}
                                    tooltip
                                    labelDirection="horizontal"
                                    label={value?.label}
                                    secondaryLabel={value.subLabel}
                                    selected={
                                      !Array.isArray(selectValue) &&
                                      selectValue?.value === value.value
                                    }
                                    onClick={() => {
                                      onOptionClick({
                                        value: value?.value,
                                        label: value?.label,
                                        subLabel: value?.subLabel,
                                      });
                                      closeDropdown();
                                    }}
                                  />
                                ))}
                              </SelectDropdownItemList>
                            </SelectDropdownBody>
                          </SelectDropdown>
                        )}
                      </Select>
                    </TableCellNew>
                  </>
                )}
                <TableCellNew
                  minWidth={
                    chosenRadioValue === 'byTemplateAccount'
                      ? (addSplitTableHead[2].minWidth as TableCellWidth)
                      : (addSplitTableHead[2].minWidthWhileSplitByTAAndBU as TableCellWidth)
                  }
                  alignitems="flex-end"
                  padding="0"
                >
                  <Container
                    display="flex"
                    justifycontent="space-between"
                    gap="4"
                    alignitems="center"
                    className={spacing.pr4}
                  >
                    <NumericInput
                      name="num-2"
                      label=""
                      suffix="€"
                      height="36"
                      value={inputValue}
                      onValueChange={handleChange}
                      error={index === 1 ? { messages: ['Error'] } : undefined}
                    />
                    <Text color="grey-100">or</Text>
                  </Container>
                </TableCellNew>
                <TableCellNew
                  minWidth={
                    chosenRadioValue === 'byTemplateAccount'
                      ? (addSplitTableHead[3].minWidth as TableCellWidth)
                      : (addSplitTableHead[3].minWidthWhileSplitByTAAndBU as TableCellWidth)
                  }
                  alignitems="flex-end"
                  padding="0"
                >
                  <NumericInput
                    name="num-1"
                    label=""
                    suffix="%"
                    height="36"
                    value={inputValue}
                    onValueChange={handleChange}
                    error={index === 1 ? { messages: ['Error'] } : undefined}
                  />
                </TableCellNew>
                <TableCellNew
                  minWidth={addSplitTableHead[4].minWidth as TableCellWidth}
                  alignitems="center"
                  justifycontent="center"
                  fixedWidth
                >
                  {currentRowIndex === index && (
                    <IconButton
                      size="16"
                      icon={<TrashIcon />}
                      onClick={() => {}}
                      background="transparent"
                      color="grey-90-red-90"
                    />
                  )}
                </TableCellNew>
              </TableRowNew>
            ))}
            <Container className={classNames(spacing.mt16, spacing.mr16)} justifycontent="flex-end">
              <LinkButton icon={<PlusIcon />} onClick={() => {}}>
                Add new line
              </LinkButton>
            </Container>
          </TableBodyNew>
        </TableNew>
      )}
      {type === 'rules' && <RulesAddSplitTable chosenRadioValue={chosenRadioValue} />}
      {(!type || type === 'rules') && (
        <>
          <Divider />
          <Container gap="39" className={classNames(spacing.pt24)}>
            <Container flexdirection="column" alignitems="flex-end" gap="4">
              <Text type="caption-regular" color="grey-100">
                {type === 'rules' ? 'Split %' : 'Split amount'}
              </Text>
              <Text type="caption-semibold">1.200 $</Text>
            </Container>
            <Container flexdirection="column" alignitems="flex-end" gap="4">
              <Text type="caption-regular" color="grey-100">
                {type === 'rules' ? 'Total %' : 'Original amount'}
              </Text>
              <Text type="caption-semibold">1.200 $</Text>
            </Container>
            <Container flexdirection="column" alignitems="flex-end" gap="4">
              <Text type="caption-regular" color="grey-100">
                {type === 'rules' ? 'Difference %' : 'Difference'}
              </Text>
              <Text type="caption-semibold" color="green-90">
                0.00
              </Text>
            </Container>
          </Container>
        </>
      )}
    </Container>
  );
};

export default AddSplitModal;
