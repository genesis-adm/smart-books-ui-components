import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { IconButton } from 'Components/base/buttons/IconButton';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { NumericInput } from 'Components/base/inputs/NumericInput';
import { OwnNumberFormatValues } from 'Components/base/inputs/NumericInput/NumericInput.types';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { Search } from 'Components/inputs/Search';
import { Select } from 'Components/inputs/Select';
import { OptionWithTwoLabels } from 'Components/inputs/Select/options/OptionWithTwoLabels';
import { TableCellWidth } from 'Components/types/gridTypes';
import { options } from 'Mocks/fakeOptions';
import BusinessUnitTableTitleDialogInfo from 'Pages/Banking/RulesModals/components/BusinessUnitTableTitleDialogInfo';
import { addSplitTableHeadInRulesModal } from 'Pages/Banking/TransactionsModals/constants';
import { ReactComponent as InfoIcon } from 'Svg/v2/16/info.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';
import DialogTooltip from '../../../../components/base/DialogTooltip/DialogTooltip';
import Icon from '../../../../components/base/Icon/Icon';

type RulesAddSplitTableProps = {
  chosenRadioValue: 'byTemplateAccount' | 'byTemplateAccountAndBU';
};
const RulesAddSplitTable: React.FC<RulesAddSplitTableProps> = ({
  chosenRadioValue,
}: RulesAddSplitTableProps) => {
  const [currentRowIndex, setCurrentRowIndex] = useState<number | null>(null);
  const [searchValue, setSearchValue] = useState('');
  const [inputValue, setInputValue] = useState<number>();
  const handleChange = ({ floatValue }: OwnNumberFormatValues) => {
    setInputValue(floatValue);
  };
  return (
    <TableNew
      noResults={false}
      className={classNames(spacing.mY24)}
      tableHead={
        <TableHeadNew style={{ transform: 'none' }}>
          {addSplitTableHeadInRulesModal.map((item, index) => {
            if (item.hideOnSplitByTemplateAccount && chosenRadioValue === 'byTemplateAccount') {
              return null;
            }

            return (
              <TableTitleNew
                key={index}
                minWidth={
                  chosenRadioValue === 'byTemplateAccount'
                    ? (item.minWidth as TableCellWidth)
                    : (item.minWidthWhileSplitByTAAndBU as TableCellWidth)
                }
                iconRight={
                  item.showInfoIcon && (
                    <DialogTooltip
                      title="Note"
                      className={classNames(spacing.pt3)}
                      control={({ handleOpen }) => (
                        <Icon
                          icon={<InfoIcon />}
                          path="grey-100-black-100"
                          cursor="pointer"
                          onClick={(e) => {
                            e.stopPropagation();
                            handleOpen();
                          }}
                        />
                      )}
                    >
                      <BusinessUnitTableTitleDialogInfo />
                    </DialogTooltip>
                  )
                }
                icon={item.icon}
                align={item.align as TableTitleNewProps['align']}
                title={item.title}
                onClick={() => {}}
              />
            );
          })}
        </TableHeadNew>
      }
    >
      <TableBodyNew>
        {Array.from(Array(2)).map((_, index) => (
          <TableRowNew
            key={index}
            gap="8"
            onMouseEnter={() => setCurrentRowIndex(index)}
            onMouseLeave={() => setCurrentRowIndex(null)}
          >
            <TableCellNew
              minWidth={
                chosenRadioValue === 'byTemplateAccount'
                  ? (addSplitTableHeadInRulesModal[0].minWidth as TableCellWidth)
                  : (addSplitTableHeadInRulesModal[0].minWidthWhileSplitByTAAndBU as TableCellWidth)
              }
              fixedWidth
            >
              <Select
                label="Choose"
                placeholder="Choose"
                height="36"
                type="baseWithCategory"
                width={chosenRadioValue === 'byTemplateAccount' ? '542' : '370'}
              >
                {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
                  <SelectDropdown
                    selectElement={selectElement}
                    className={spacing.z_ind20}
                    width="416"
                  >
                    <SelectDropdownBody>
                      <Search
                        className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                        width="full"
                        height="40"
                        name="search"
                        placeholder="Search option"
                        value={searchValue}
                        onChange={(e) => setSearchValue(e.target.value)}
                        searchParams={[]}
                        onSearch={() => {}}
                        onClear={() => {}}
                      />
                      <SelectDropdownItemList>
                        {options.map((value, index) => (
                          <OptionWithTwoLabels
                            id={value.label}
                            key={index}
                            tooltip
                            labelDirection="horizontal"
                            label={value?.label}
                            secondaryLabel={value.subLabel}
                            selected={
                              !Array.isArray(selectValue) && selectValue?.value === value.value
                            }
                            onClick={() => {
                              onOptionClick({
                                value: value?.value,
                                label: value?.label,
                                subLabel: value?.subLabel,
                              });
                              closeDropdown();
                            }}
                          />
                        ))}
                      </SelectDropdownItemList>
                    </SelectDropdownBody>
                  </SelectDropdown>
                )}
              </Select>
            </TableCellNew>
            {chosenRadioValue === 'byTemplateAccountAndBU' && (
              <>
                <TableCellNew
                  minWidth={addSplitTableHeadInRulesModal[1].minWidth as TableCellWidth}
                >
                  <Select label="Choose" placeholder="Choose" height="36" type="column" width="300">
                    {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
                      <SelectDropdown selectElement={selectElement} width="408">
                        <SelectDropdownBody>
                          <Search
                            className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                            width="full"
                            height="40"
                            name="search"
                            placeholder="Search option"
                            value={searchValue}
                            onChange={(e) => setSearchValue(e.target.value)}
                            searchParams={[]}
                            onSearch={() => {}}
                            onClear={() => {}}
                          />
                          <SelectDropdownItemList>
                            {options.map((value, index) => (
                              <OptionWithTwoLabels
                                id={value.label}
                                key={index}
                                tooltip
                                labelDirection="horizontal"
                                label={value?.label}
                                secondaryLabel={value.subLabel}
                                selected={
                                  !Array.isArray(selectValue) && selectValue?.value === value.value
                                }
                                onClick={() => {
                                  onOptionClick({
                                    value: value?.value,
                                    label: value?.label,
                                    subLabel: value?.subLabel,
                                  });
                                  closeDropdown();
                                }}
                              />
                            ))}
                          </SelectDropdownItemList>
                        </SelectDropdownBody>
                      </SelectDropdown>
                    )}
                  </Select>
                </TableCellNew>
              </>
            )}
            <TableCellNew
              minWidth={
                chosenRadioValue === 'byTemplateAccount'
                  ? (addSplitTableHeadInRulesModal[2].minWidth as TableCellWidth)
                  : (addSplitTableHeadInRulesModal[2].minWidthWhileSplitByTAAndBU as TableCellWidth)
              }
              alignitems="flex-end"
              padding="0"
              fixedWidth={chosenRadioValue === 'byTemplateAccountAndBU'}
            >
              <NumericInput
                name="num-1"
                label=""
                suffix="%"
                height="36"
                value={inputValue}
                onValueChange={handleChange}
              />
            </TableCellNew>
            <TableCellNew
              minWidth={addSplitTableHeadInRulesModal[3].minWidth as TableCellWidth}
              alignitems="center"
              justifycontent="center"
              fixedWidth
            >
              {currentRowIndex === index && (
                <IconButton
                  tooltip="Delete line"
                  size="16"
                  icon={<TrashIcon />}
                  onClick={() => {}}
                  background="transparent"
                  color="grey-90-red-90"
                />
              )}
            </TableCellNew>
          </TableRowNew>
        ))}
        <Container className={classNames(spacing.mt16, spacing.mr16)} justifycontent="flex-end">
          <LinkButton icon={<PlusIcon />} onClick={() => {}}>
            Add new line
          </LinkButton>
        </Container>
      </TableBodyNew>
    </TableNew>
  );
};

export default RulesAddSplitTable;
