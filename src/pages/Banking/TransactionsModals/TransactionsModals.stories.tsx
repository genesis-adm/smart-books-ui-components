import { DropDownItem } from 'Components/DropDownItem';
import { SelectDropdown } from 'Components/SelectDropdown';
import { Divider } from 'Components/base/Divider';
import { Radio } from 'Components/base/Radio';
import { Switch } from 'Components/base/Switch';
import { TooltipIcon } from 'Components/base/TooltipIcon';
import { Button } from 'Components/base/buttons/Button';
import { ButtonGroup } from 'Components/base/buttons/ButtonGroup';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { InputDate } from 'Components/base/inputs/InputDate';
import { InputNew } from 'Components/base/inputs/InputNew';
import { InputTime } from 'Components/base/inputs/InputTime';
import { SelectNew } from 'Components/base/inputs/SelectNew';
import { OptionDoubleHorizontalNew } from 'Components/base/inputs/SelectNew/options/OptionDoubleHorizontalNew';
import { OptionDoubleVerticalNew } from 'Components/base/inputs/SelectNew/options/OptionDoubleVerticalNew';
import { TextareaNew } from 'Components/base/inputs/TextareaNew';
import { GeneralTab, GeneralTabWrapper } from 'Components/base/tabs/GeneralTab';
import { Text } from 'Components/base/typography/Text';
import { ModalTransactionAction } from 'Components/custom/Banking/ModalTransactionAction';
import { RuleSplit } from 'Components/custom/Banking/RuleSplit';
import { BankingTransactionsSplitCard } from 'Components/custom/Banking/Transactions/BankingTransactionsSplitCard';
import { InputAutocomplete } from 'Components/elements/InputAutocomplete';
import { SplitContainer } from 'Components/elements/SplitContainer';
import { TransactionPageSwitch } from 'Components/elements/TransactionPageSwitch';
import { OptionWithFourLabels } from 'Components/inputs/Select/options/OptionWithFourLabels';
import OptionWithTwoLabels from 'Components/inputs/Select/options/OptionWithTwoLabels/OptionWithTwoLabels';
import { Modal, ModalFooter, ModalHeader } from 'Components/modules/Modal';
import { Input } from 'Components/old/Input';
import { TransactionData } from 'Components/old/TransactionData';
import { options } from 'Mocks/fakeOptions';
import { ReactComponent as DownloadIcon } from 'Svg/16/download.svg';
import { ReactComponent as QuestionIcon } from 'Svg/16/question.svg';
import { ReactComponent as InIcon } from 'Svg/40/transaction-in.svg';
import { ReactComponent as OutIcon } from 'Svg/40/transaction-out.svg';
import { ReactComponent as LinkIcon } from 'Svg/v2/16/link.svg';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Banking/Transactions Modals',
};

const testTransaction = 'in';

export const CreateAndEditTransaction: React.FC = () => {
  const [startDate, setStartDate] = useState<Date | null>(new Date());
  return (
    <Modal
      size="md-medium"
      centered
      classNameModalBody={spacing.mY32}
      overlay="black-100"
      header={
        <ModalHeader
          backgroundColor="grey-10"
          headerHeight="medium"
          title="Create | Edit transaction"
          titlePosition="center"
          onClick={() => {}}
        />
      }
      footer={
        <ModalFooter justifycontent="center">
          <ButtonGroup align="center">
            <Button
              width="md"
              height="large"
              background="grey-20"
              color="black-100"
              border="grey-20-grey-90"
              onClick={() => {}}
            >
              Cancel
            </Button>
            <Button
              width="md"
              height="large"
              onClick={() => {}}
              dropdown={
                <>
                  <DropDownItem type="button" onClick={() => {}}>
                    Save and accept
                  </DropDownItem>
                </>
              }
              dropdownBorder="white-100"
            >
              Save and close
            </Button>
          </ButtonGroup>
        </ModalFooter>
      }
    >
      <Container flexdirection="column">
        <Container justifycontent="space-between">
          <InputDate
            name="inputdate"
            placeholder="08 July 2021"
            label="Date"
            required
            width="260"
            selected={startDate}
            onChange={(date: Date) => setStartDate(date)}
            calendarStartDay={1}
          />
          <InputTime
            width="260"
            name="time"
            placeholder="10:30 AM"
            label="Time"
            required
            onChange={() => {}}
            onBlur={() => {}}
          />
        </Container>
        <SelectNew
          name="bankaccount"
          className={spacing.mt16}
          width="full"
          onChange={() => {}}
          placeholder="Choose bank account"
          label="Bank account"
          required
        >
          {({ onClick, state, selectElement }) => (
            <SelectDropdown selectElement={selectElement}>
              {options.map(({ label, secondaryLabel, tertiaryLabel, quadroLabel, value }) => (
                <OptionWithFourLabels
                  key={value}
                  label={label}
                  secondaryLabel={secondaryLabel}
                  tertiaryLabel={tertiaryLabel}
                  quadroLabel={quadroLabel}
                  selected={state?.value === value}
                  onClick={() => onClick({ label: `${label} | ${tertiaryLabel}`, value })}
                />
              ))}
            </SelectDropdown>
          )}
        </SelectNew>
        <Text color="grey-100" className={classNames(spacing.ml16, spacing.mt8)}>
          Legal entity: BetterMe Limited
        </Text>
        <Container className={spacing.mt16} justifycontent="space-between">
          <Input
            align="right"
            color="green-90"
            name="amount"
            width="modal-md"
            placeholder="Enter amount"
            label="Amount"
            symbol="€"
            symbolPosition="left"
            isRequired
          />
          <Input
            align="right"
            disabled
            name="amount"
            width="modal-md"
            placeholder="Enter amount"
            label="Base amount"
            secondaryLabel="FX rate 0.8"
            symbol="$"
            symbolPosition="left"
          />
        </Container>
        <SelectNew
          name="payee"
          className={spacing.mt16}
          width="full"
          onChange={() => {}}
          placeholder="Choose payee"
          label="Payee"
        >
          {({ onClick, state, selectElement }) => (
            <SelectDropdown selectElement={selectElement}>
              {options.map(({ label, secondaryLabel, value }) => (
                <OptionWithTwoLabels
                  id={label}
                  key={value}
                  label={label}
                  labelDirection="horizontal"
                  secondaryLabel={secondaryLabel}
                  selected={state?.value === value}
                  onClick={() => onClick({ label, value })}
                />
              ))}
            </SelectDropdown>
          )}
        </SelectNew>
        <TextareaNew
          className={spacing.mt16}
          label="Description"
          name="description"
          placeholder="Enter"
          required
          value=""
          onChange={() => {}}
        />
        <Divider fullHorizontalWidth className={spacing.mt16} />
        <Container className={spacing.mt16} justifycontent="space-between" alignitems="center">
          <Text type="body-regular-14">Transaction split</Text>
          <Switch id="transactionsplit" checked onChange={() => {}} />
        </Container>
        <Container className={spacing.mt8}>
          <Radio id="company" onChange={() => {}} name="splitBy" checked>
            By percent
          </Radio>
          <Radio
            className={spacing.ml32}
            id="project"
            onChange={() => {}}
            name="splitBy"
            checked={false}
          >
            By amount
          </Radio>
        </Container>
        <Divider fullHorizontalWidth className={classNames(spacing.mt16)} />
        <Text type="body-medium" className={spacing.mt24}>
          Assign split
        </Text>
        <SplitContainer
          splitType="amount"
          splitCurrency="€"
          className={spacing.mt16}
          onDelete={() => {}}
          splitCurrentNumber={1}
          splitTotalCount={2}
          splitTotalSum={600000}
        >
          <InputAutocomplete
            className={spacing.mt8}
            width="full"
            insideRules
            input={
              <Input
                name="chartofaccount"
                autoComplete="off"
                width="full"
                label="Choose account"
                isRequired
                placeholder="Please choose account"
                onChange={() => {}}
              />
            }
          >
            {/* {suggestions.map((item) => (
            <InputAutocompleteItemDoubleHorizontal
              label={item.bankname}
              secondaryLabel="Expenses"
              key={item.id}
              onClick={() => {}}
            />
          ))} */}
            {/* {isOpen && <InputAutocompleteText label="Please enter 2 or more characters" />} */}
          </InputAutocomplete>
          <RuleSplit type="division" showDeleteButton>
            <SelectNew
              name="businessdivision"
              onChange={() => {}}
              placeholder="Select option"
              width="300"
              label="Business division"
              required
            >
              {({ onClick, state }) => (
                <>
                  {options.map(({ label, secondaryLabel, value }) => (
                    <OptionWithTwoLabels
                      id={label}
                      key={value}
                      label={label}
                      labelDirection="horizontal"
                      secondaryLabel={secondaryLabel}
                      selected={state?.value === value}
                      onClick={() => onClick({ label, value })}
                    />
                  ))}
                </>
              )}
            </SelectNew>
            <InputNew width="190" name="split-1" placeholder="%" label="%" align="right" />
          </RuleSplit>
          <RuleSplit type="division" showDeleteButton>
            <SelectNew
              name="businessdivision"
              onChange={() => {}}
              placeholder="Select option"
              width="300"
              label="Business division"
              required
            >
              {({ onClick, state, selectElement }) => (
                <SelectDropdown selectElement={selectElement}>
                  {options.map(({ label, secondaryLabel, value }) => (
                    <OptionWithTwoLabels
                      id={label}
                      key={value}
                      label={label}
                      secondaryLabel={secondaryLabel}
                      selected={state?.value === value}
                      onClick={() => onClick({ label, value })}
                    />
                  ))}
                </SelectDropdown>
              )}
            </SelectNew>
            <InputNew width="190" name="split-2" placeholder="%" label="%" align="right" />
          </RuleSplit>
          <Button
            className={classNames(spacing.ml16, spacing.mt16)}
            width="lg"
            height="small"
            background="transparent"
            border="grey-20-grey-90"
            color="violet-90-violet-100"
            onClick={() => {}}
            iconLeft={<PlusIcon />}
            dropdown={
              <>
                <DropDownItem type="option" width="240" icon={<PlusIcon />} onClick={() => {}}>
                  Add counterparty
                </DropDownItem>
                <DropDownItem type="option" width="240" icon={<PlusIcon />} onClick={() => {}}>
                  Add third party
                </DropDownItem>
              </>
            }
            dropdownBorder="grey-20-grey-90"
          >
            Add business division
          </Button>
        </SplitContainer>
        <SplitContainer
          splitType="amount"
          splitCurrency="€"
          className={spacing.mt16}
          onDelete={() => {}}
          splitCurrentNumber={2}
          splitTotalCount={2}
          splitTotalSum={400000.2}
        >
          <InputAutocomplete
            className={spacing.mt8}
            width="full"
            insideRules
            input={
              <Input
                name="chartofaccount"
                autoComplete="off"
                width="full"
                label="Choose account"
                isRequired
                placeholder="Please choose account"
                onChange={() => {}}
              />
            }
          >
            {/* {suggestions.map((item) => (
            <InputAutocompleteItemDoubleHorizontal
              label={item.bankname}
              secondaryLabel="Expenses"
              key={item.id}
              onClick={() => {}}
            />
          ))} */}
            {/* {isOpen && <InputAutocompleteText label="Please enter 2 or more characters" />} */}
          </InputAutocomplete>
          <RuleSplit type="division" showDeleteButton>
            <SelectNew
              name="businessdivision"
              onChange={() => {}}
              placeholder="Select option"
              width="300"
              label="Business division"
              required
            >
              {({ onClick, state }) => (
                <>
                  {options.map(({ label, secondaryLabel, value }) => (
                    <OptionDoubleHorizontalNew
                      key={value}
                      label={label}
                      secondaryLabel={secondaryLabel}
                      selected={state?.value === value}
                      onClick={() => onClick({ label, value })}
                    />
                  ))}
                </>
              )}
            </SelectNew>
            <InputNew width="190" name="split-3" placeholder="%" label="%" align="right" />
          </RuleSplit>
          <RuleSplit type="division" showDeleteButton>
            <SelectNew
              name="businessdivision"
              onChange={() => {}}
              placeholder="Select option"
              width="300"
              label="Business division"
              required
            >
              {({ onClick, state }) => (
                <>
                  {options.map(({ label, secondaryLabel, value }) => (
                    <OptionDoubleVerticalNew
                      key={value}
                      label={label}
                      secondaryLabel={secondaryLabel}
                      selected={state?.value === value}
                      onClick={() => onClick({ label, value })}
                    />
                  ))}
                </>
              )}
            </SelectNew>
            <InputNew width="190" name="split-4" placeholder="%" label="%" align="right" />
          </RuleSplit>
          <Button
            className={classNames(spacing.ml16, spacing.mt16)}
            width="lg"
            height="small"
            background="transparent"
            border="grey-20-grey-90"
            color="violet-90-violet-100"
            onClick={() => {}}
            iconLeft={<PlusIcon />}
            dropdown={
              <>
                <DropDownItem type="option" width="240" icon={<PlusIcon />} onClick={() => {}}>
                  Add counterparty
                </DropDownItem>
                <DropDownItem type="option" width="240" icon={<PlusIcon />} onClick={() => {}}>
                  Add third party
                </DropDownItem>
              </>
            }
            dropdownBorder="grey-20-grey-90"
          >
            Add business division
          </Button>
        </SplitContainer>
        <Divider fullHorizontalWidth className={spacing.mt16} />
        <Button
          className={spacing.mt16}
          width="full"
          background="grey-10-blue-10"
          color="violet-90-violet-100"
          iconLeft={<PlusIcon />}
          border="violet-90-violet-100"
          dashed="always"
          onClick={() => {}}
        >
          Add new split
        </Button>
        <Divider fullHorizontalWidth className={spacing.mt16} />
        <Container justifycontent="space-between" className={spacing.mt24}>
          <Text>Split amount</Text>
          <Text>€&nbsp;1.000.000,00</Text>
        </Container>
        <Container justifycontent="space-between" className={spacing.mt16}>
          <Text>Original amount</Text>
          <Text>€&nbsp;1.000.000,20</Text>
        </Container>
        <Container justifycontent="space-between" className={spacing.mt16}>
          <Text>Difference</Text>
          <Text color="red-90">€&nbsp;-0,20</Text>
        </Container>
      </Container>
    </Modal>
  );
};

export const TransactionInformationGeneral: React.FC = () => (
  <Modal
    size="md-wide"
    centered
    overlay="black-100"
    header={
      <ModalHeader
        backgroundColor="white-100"
        headerHeight="medium"
        titlePosition="center"
        onClick={() => {}}
      />
    }
    footer={
      <ModalFooter justifycontent="space-between">
        <ButtonGroup align="left" margin="small">
          <IconButton
            icon={<DownloadIcon />}
            background="grey-10"
            color="black-100-violet-100"
            onClick={() => {}}
            tooltip="Download CSV"
          />
          <IconButton
            icon={<LinkIcon />}
            background="grey-10"
            color="black-100-violet-100"
            onClick={() => {}}
            tooltip="Copy link"
          />
        </ButtonGroup>
        <ButtonGroup align="center">
          <Button
            width="sm"
            height="small"
            background="grey-20-violet-90"
            color="grey-100-white-100"
            onClick={() => {}}
          >
            Accept
          </Button>
          <Button
            width="sm"
            height="small"
            background="grey-20-grey-100"
            color="grey-100-white-100"
            onClick={() => {}}
          >
            Exclude
          </Button>
        </ButtonGroup>
        <ButtonGroup align="right" margin="small">
          <IconButton
            icon={<EditIcon />}
            background="grey-10"
            color="black-100-violet-100"
            onClick={() => {}}
            tooltip="Edit transaction"
          />
          <IconButton
            icon={<TrashIcon />}
            background="grey-10"
            color="black-100-red-90"
            onClick={() => {}}
            tooltip="Delete transaction"
          />
        </ButtonGroup>
      </ModalFooter>
    }
  >
    <Container
      className={classNames(spacing.w100p, spacing.w600)}
      flexdirection="column"
      alignitems="center"
    >
      {testTransaction === 'in' ? <InIcon /> : <OutIcon />}
      <Text
        className={spacing.mt16}
        type="h2-semibold"
        align="center"
        color={testTransaction === 'in' ? 'green-90' : 'red-90'}
      >
        {testTransaction === 'in' ? '+' : '-'}
        &nbsp;$ 1,000,000.20
      </Text>
      <Text className={spacing.mt8} type="text-medium" align="center">
        Unlimint EU Ltd
      </Text>
      <Text className={spacing.mt8} type="text-medium" align="center">
        CY20902000010000020105000910 | USD
      </Text>
      <Text className={spacing.mt10} type="text-regular" align="center" color="grey-100">
        ID 000001 | 08 July 2021, at 10:30 AM
      </Text>
      <Divider className={spacing.mt16} type="horizontal" fullHorizontalWidth />
      <GeneralTabWrapper className={spacing.mt16}>
        <GeneralTab id="review" onClick={() => {}} active>
          General
        </GeneralTab>
        <GeneralTab id="accepted" onClick={() => {}} active={false}>
          Split
        </GeneralTab>
        <GeneralTab id="accepted" onClick={() => {}} active={false}>
          History
        </GeneralTab>
      </GeneralTabWrapper>
      <Container
        flexdirection="column"
        className={classNames(
          spacing.w100p,
          spacing.w450,
          spacing.h290,
          spacing.mt16,
          spacing.pX15,
        )}
        overflow="y-auto"
      >
        {/* <TransactionDataMain type="bank" value="BNP Paribas | USD" />
        <TransactionDataCategory category="сF84843" name="Category name" onClick={() => { }} />
        <TransactionDataMain type="description"value="Lorem" /> */}
        <TransactionData name="Account type:" value="Cash" />
        <TransactionData name="Counterparty:" value="Becker, Hartmann and Andrey Braun" />
        <TransactionData name="Legal Entity:" value="LCC Matard and Yard corporate" />
        <TransactionData name="Created:" value="08 July 2021, at 10:30 AM" />
        <TransactionData name="Reporting amount:" value="$ 34,000,000.00" />
        <Text className={spacing.mt16} color="grey-100">
          Description:
        </Text>
        <Text className={classNames(spacing.mt12, spacing.mb20)}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur viverra, orci id semper
          dapibus, elit eros ullamcorper diam, vel ultrices metus erat quis quam.
        </Text>
        {/* <TransactionTotal total="$ 4,002,000.00" /> */}
      </Container>
    </Container>
    <TransactionPageSwitch page={1} totalPages={28} onPrevPage={() => {}} onNextPage={() => {}} />
  </Modal>
);

export const TransactionInformationSplitEmpty: React.FC = () => (
  <Modal
    size="md-wide"
    centered
    overlay="black-100"
    header={
      <ModalHeader
        backgroundColor="white-100"
        headerHeight="medium"
        titlePosition="center"
        onClick={() => {}}
      />
    }
    footer={
      <ModalFooter justifycontent="space-between">
        <ButtonGroup align="left" margin="small">
          <IconButton
            icon={<DownloadIcon />}
            background="grey-10"
            color="black-100-violet-100"
            onClick={() => {}}
            tooltip="Download CSV"
          />
          <IconButton
            icon={<LinkIcon />}
            background="grey-10"
            color="black-100-violet-100"
            onClick={() => {}}
            tooltip="Copy link"
          />
        </ButtonGroup>
        <ButtonGroup align="center">
          <Button
            width="sm"
            height="small"
            background="grey-20-violet-90"
            color="grey-100-white-100"
            onClick={() => {}}
          >
            Accept
          </Button>
          <Button
            width="sm"
            height="small"
            background="grey-20-grey-100"
            color="grey-100-white-100"
            onClick={() => {}}
          >
            Exclude
          </Button>
        </ButtonGroup>
        <ButtonGroup align="right" margin="small">
          <IconButton
            icon={<EditIcon />}
            background="grey-10"
            color="black-100-violet-100"
            onClick={() => {}}
            tooltip="Edit transaction"
          />
          <IconButton
            icon={<TrashIcon />}
            background="grey-10"
            color="black-100-red-90"
            onClick={() => {}}
            tooltip="Delete transaction"
          />
        </ButtonGroup>
      </ModalFooter>
    }
  >
    <Container
      className={classNames(spacing.w100p, spacing.w600)}
      flexdirection="column"
      alignitems="center"
    >
      {testTransaction === 'in' ? <InIcon /> : <OutIcon />}
      <Text
        className={spacing.mt16}
        type="h2-semibold"
        align="center"
        color={testTransaction === 'in' ? 'green-90' : 'red-90'}
      >
        {testTransaction === 'in' ? '+' : '-'}
        &nbsp;$ 1,000,000.20
      </Text>
      <Text className={spacing.mt8} type="text-medium" align="center">
        Unlimint EU Ltd
      </Text>
      <Text className={spacing.mt8} type="text-medium" align="center">
        CY20902000010000020105000910 | USD
      </Text>
      <Text className={spacing.mt10} type="text-regular" align="center" color="grey-100">
        ID 000001 | 08 July 2021, at 10:30 AM
      </Text>
      <Divider className={spacing.mt16} type="horizontal" fullHorizontalWidth />
      <GeneralTabWrapper className={spacing.mt16}>
        <GeneralTab id="review" onClick={() => {}} active={false}>
          General
        </GeneralTab>
        <GeneralTab id="accepted" onClick={() => {}} active>
          Split
        </GeneralTab>
        <GeneralTab id="accepted" onClick={() => {}} active={false}>
          History
        </GeneralTab>
      </GeneralTabWrapper>
      <Container flexdirection="column" className={classNames(spacing.w420, spacing.mY90)}>
        <Text type="body-regular-14" align="center">
          It is empty here now
        </Text>
        <Text color="grey-100" align="center">
          If you want add a chart of account (Business division and category of match) Please go to
          edit transaction.
        </Text>
      </Container>
    </Container>
    <TransactionPageSwitch page={1} totalPages={28} onPrevPage={() => {}} onNextPage={() => {}} />
  </Modal>
);

export const TransactionInformationSplitItems: React.FC = () => (
  <Modal
    size="md-wide"
    centered
    overlay="black-100"
    header={
      <ModalHeader
        backgroundColor="white-100"
        headerHeight="medium"
        titlePosition="center"
        onClick={() => {}}
      />
    }
    footer={
      <ModalFooter justifycontent="space-between">
        <ButtonGroup align="left" margin="small">
          <IconButton
            icon={<DownloadIcon />}
            background="grey-10"
            color="black-100-violet-100"
            onClick={() => {}}
            tooltip="Download CSV"
          />
          <IconButton
            icon={<LinkIcon />}
            background="grey-10"
            color="black-100-violet-100"
            onClick={() => {}}
            tooltip="Copy link"
          />
        </ButtonGroup>
        <ButtonGroup align="center">
          <Button
            width="sm"
            height="small"
            background="grey-20-violet-90"
            color="grey-100-white-100"
            onClick={() => {}}
          >
            Accept
          </Button>
          <Button
            width="sm"
            height="small"
            background="grey-20-grey-100"
            color="grey-100-white-100"
            onClick={() => {}}
          >
            Exclude
          </Button>
        </ButtonGroup>
        <ButtonGroup align="right" margin="small">
          <IconButton
            icon={<EditIcon />}
            background="grey-10"
            color="black-100-violet-100"
            onClick={() => {}}
            tooltip="Edit transaction"
          />
          <IconButton
            icon={<TrashIcon />}
            background="grey-10"
            color="black-100-red-90"
            onClick={() => {}}
            tooltip="Delete transaction"
          />
        </ButtonGroup>
      </ModalFooter>
    }
  >
    <Container
      className={classNames(spacing.w100p, spacing.w600)}
      flexdirection="column"
      alignitems="center"
    >
      {testTransaction === 'in' ? <InIcon /> : <OutIcon />}
      <Text
        className={spacing.mt16}
        type="h2-semibold"
        align="center"
        color={testTransaction === 'in' ? 'green-90' : 'red-90'}
      >
        {testTransaction === 'in' ? '+' : '-'}
        &nbsp;$ 1,000,000.20
      </Text>
      <Text className={spacing.mt8} type="text-medium" align="center">
        Unlimint EU Ltd
      </Text>
      <Text className={spacing.mt8} type="text-medium" align="center">
        CY20902000010000020105000910 | USD
      </Text>
      <Text className={spacing.mt10} type="text-regular" align="center" color="grey-100">
        ID 000001 | 08 July 2021, at 10:30 AM
      </Text>
      <Divider className={spacing.mt16} type="horizontal" fullHorizontalWidth />
      <GeneralTabWrapper className={spacing.mt16}>
        <GeneralTab id="review" onClick={() => {}} active={false}>
          General
        </GeneralTab>
        <GeneralTab id="accepted" onClick={() => {}} active>
          Split
        </GeneralTab>
        <GeneralTab id="accepted" onClick={() => {}} active={false}>
          History
        </GeneralTab>
      </GeneralTabWrapper>
      <Container className={classNames(spacing.mt32, spacing.w100p)}>
        <Text type="text-medium" className={spacing.mr10}>
          This transaction was divided to 8 splits:
        </Text>
        <TooltipIcon
          icon={<QuestionIcon />}
          size="16"
          message={
            <Text display="block" color="white-100">
              Tooltip text
            </Text>
          }
        />
      </Container>
      <Container
        overflow="y-auto"
        flexdirection="column"
        className={classNames(spacing.w100p, spacing.mY16, spacing.h210)}
      >
        <>
          {Array.from(Array(3)).map((_, index) => (
            <BankingTransactionsSplitCard
              key={index}
              className={spacing.mt8}
              splitType="Business division"
              businessUnitName="Genesis Media & Mobile Application"
              legalEntity="Genesis"
              accountName="Direct advertising"
              accountClass="Revenue"
              amount="+ $ 821,66"
              isPositive
            />
          ))}
        </>
      </Container>
      {/* <Container flexdirection="column" className={classNames(spacing.w420, spacing.mY90)}>
        <Text type="body-regular-14" align="center">It is empty here now</Text>
        <Text color="grey-100" align="center">If you want to add a chart of account
        (Business division and category of match) Please go to edit transaction.</Text>
      </Container> */}
    </Container>
    <TransactionPageSwitch page={1} totalPages={28} onPrevPage={() => {}} onNextPage={() => {}} />
  </Modal>
);

export const TransactionAccepted: React.FC = () => (
  <Modal size="minimal" height="minimal" centered overlay="black-100">
    <Text className={spacing.pY25} type="body-medium" align="center">
      Transaction&nbsp;
      <Text type="inherit" color="violet-90" display="inline-flex">
        ID 000000
      </Text>
      &nbsp;accepted!
    </Text>
    <TransactionPageSwitch page={1} totalPages={28} onPrevPage={() => {}} onNextPage={() => {}} />
  </Modal>
);

export const TransactionExcluded: React.FC = () => (
  <Modal size="minimal" height="minimal" centered overlay="black-100">
    <Text className={spacing.pY25} type="body-medium" align="center">
      Transaction&nbsp;
      <Text type="inherit" color="violet-90" display="inline-flex">
        ID 000000
      </Text>
      &nbsp;excluded!
    </Text>
    <TransactionPageSwitch page={1} totalPages={28} onPrevPage={() => {}} onNextPage={() => {}} />
  </Modal>
);

export const TransactionBatchDelete: React.FC = () => (
  <ModalTransactionAction
    height="minimal"
    size="xs"
    titleActionButton="Delete"
    titleCancelButton="Cancel"
    onAction={() => {}}
    onCancel={() => {}}
    onClose={() => {}}
  >
    <Text className={spacing.mt50} type="title-bold" align="center">
      Delete transactions
    </Text>
    <Text
      className={classNames(spacing.mt16, spacing.mb50)}
      type="body-regular-16"
      align="center"
      color="grey-100"
    >
      You want to delete 123 transactions. Are you sure?
    </Text>
  </ModalTransactionAction>
);

export const TransactionBatchExclude: React.FC = () => (
  <ModalTransactionAction
    height="minimal"
    size="xs"
    titleActionButton="Exclude"
    titleCancelButton="Cancel"
    onAction={() => {}}
    onCancel={() => {}}
    onClose={() => {}}
  >
    <Text className={spacing.mt50} type="title-bold" align="center">
      Exclude transactions
    </Text>
    <Text
      className={classNames(spacing.mt16, spacing.mb50)}
      type="body-regular-16"
      align="center"
      color="grey-100"
    >
      You want to exclude 123 transactions. Are you sure?
    </Text>
  </ModalTransactionAction>
);

export const TransactionBatchAccept: React.FC = () => (
  <ModalTransactionAction
    height="minimal"
    size="xs"
    titleActionButton="Accept"
    titleCancelButton="Cancel"
    onAction={() => {}}
    onCancel={() => {}}
    onClose={() => {}}
  >
    <Text className={spacing.mt50} type="title-bold" align="center">
      Accept transactions
    </Text>
    <Text
      className={classNames(spacing.mt16, spacing.mb50)}
      type="body-regular-16"
      align="center"
      color="grey-100"
    >
      You want to accept 123 transactions. Are you sure?
    </Text>
  </ModalTransactionAction>
);
