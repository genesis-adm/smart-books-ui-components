import { DropDownUser } from 'Components/DropDownUser';
import { Divider } from 'Components/base/Divider';
import { Button } from 'Components/base/buttons/Button';
import { ButtonGroup } from 'Components/base/buttons/ButtonGroup';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { ErrorText } from 'Components/base/typography/ErrorText';
import { Text } from 'Components/base/typography/Text';
import { ErrorImage } from 'Components/graphics/ErrorImage';
import { ReactComponent as LogoutIcon } from 'Svg/16/logout.svg';
import { ReactComponent as GearIcon } from 'Svg/v2/16/settings.svg';
import { ReactComponent as HistoryIcon } from 'Svg/v2/24/clock.svg';
import { ReactComponent as NotificationIcon } from 'Svg/v2/24/notification.svg';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Error',
};

export const Error: React.FC = () => (
  <Container fullscreen flexdirection="column">
    <Container
      justifycontent="flex-end"
      className={classNames(spacing.mt10, spacing.mr20, spacing.ml30)}
    >
      <ButtonGroup align="right" nospace>
        <IconButton
          size="xs"
          background="transparent"
          color="grey-100-black-100"
          icon={<HistoryIcon />}
          onClick={() => {}}
        />
        <Divider type="vertical" className={spacing.mX20} />
        <IconButton
          size="xs"
          background="transparent"
          color="grey-100-black-100"
          icon={<NotificationIcon />}
          onClick={() => {}}
        />
        <Divider type="vertical" className={spacing.mX20} />
        <DropDownUser user="Karine Mnatsakanian">
          <DropDownButton icon={<GearIcon />} onClick={() => {}}>
            My account settings
          </DropDownButton>
          <Divider className={classNames(spacing.mX10, spacing.mY6)} />
          <DropDownButton icon={<LogoutIcon />} onClick={() => {}}>
            Sign out
          </DropDownButton>
        </DropDownUser>
      </ButtonGroup>
    </Container>
    <Container className={spacing.mt40} justifycontent="center">
      <ErrorImage error="404" />
      <ErrorText error="404" />
    </Container>
    <Text className={spacing.mt30} type="h1-bold" align="center">
      Oh, sorry!
    </Text>
    <Text className={spacing.mt5} type="title-regular" color="grey-100" align="center">
      Something went wrong ...
    </Text>
    <Button
      className={classNames(spacing.mX_auto, spacing.mt40)}
      width="md"
      background="black-100-violet-90"
      onClick={() => {}}
    >
      Go back
    </Button>
  </Container>
);
