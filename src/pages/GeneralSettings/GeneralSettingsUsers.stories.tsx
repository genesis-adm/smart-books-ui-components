import { Meta, Story } from '@storybook/react';
import { DropDown } from 'Components/DropDown';
import { Divider } from 'Components/base/Divider';
import { Icon } from 'Components/base/Icon';
import { Tag } from 'Components/base/Tag';
import { Button } from 'Components/base/buttons/Button';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { SettingsContainer } from 'Components/base/grid/SettingsContainer';
import { GeneralTab, GeneralTabWrapper } from 'Components/base/tabs/GeneralTab';
import { Text } from 'Components/base/typography/Text';
import { MainPageEmpty } from 'Components/custom/MainPage/MainPageEmpty';
import { ModalSettings } from 'Components/custom/Settings/ModalSettings';
import { NavigationSettingsSB } from 'Components/custom/Stories/NavigationSettingsSB';
import { EmailNotesInput } from 'Components/custom/Users/EmailNotesInput';
import { TagType } from 'Components/custom/Users/EmailNotesInput/TagsEditable/TagsEditable.types';
import { UserProfileCard } from 'Components/custom/Users/UserProfileCard';
import { Logo } from 'Components/elements/Logo';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { CircledIcon } from 'Components/graphics/CircledIcon';
import { Search } from 'Components/inputs/Search';
import useSearchExample from 'Components/inputs/Search/hooks/useSearchExample';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { TableCellWidth } from 'Components/types/gridTypes';
import SetUpAccessAccountingContent from 'Pages/GeneralSettings/components/SetUpAccessAccountingContent';
import SetUpAccessBankingContent from 'Pages/GeneralSettings/components/SetUpAccessBankingContent';
import SetUpAccessReportsContent from 'Pages/GeneralSettings/components/SetUpAccessReportsContent';
import UserProfileAccountingTabContent from 'Pages/GeneralSettings/components/UserProfileAccountingTabContent';
import UserProfileBankingTabContent from 'Pages/GeneralSettings/components/UserProfileBankingTabContent';
import { ActiveTabs, tabs } from 'Pages/GeneralSettings/mocks/setUpAccessMock';
import { ReactComponent as CalendarIcon } from 'Svg/16/calendar.svg';
import { ReactComponent as DropdownIcon } from 'Svg/20/dropdownbutton.svg';
import { ReactComponent as ArrowsSwitchIcon } from 'Svg/v2/12/arrows.svg';
import { ReactComponent as ArrowLeftIcon } from 'Svg/v2/16/arrow-left.svg';
import { ReactComponent as CrossIcon } from 'Svg/v2/16/cross-in-circle.svg';
import { ReactComponent as RemoveIcon } from 'Svg/v2/16/cross-in-filled-circle.svg';
import { ReactComponent as ClearIcon } from 'Svg/v2/16/cross.svg';
import { ReactComponent as ExclamationIcon } from 'Svg/v2/16/exclamation-in-circle.svg';
import { ReactComponent as EyeIcon } from 'Svg/v2/16/eye.svg';
import { ReactComponent as DocumentIcon } from 'Svg/v2/16/file-blank.svg';
import { ReactComponent as AllIcon } from 'Svg/v2/16/lines.svg';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as ResendIcon } from 'Svg/v2/16/resend.svg';
import { ReactComponent as AcceptedIcon } from 'Svg/v2/16/tick-in-circle.svg';
import { ReactComponent as TickCircleIcon } from 'Svg/v2/16/tick-in-filled-circle.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import classNames from 'classnames';
import React, { ReactNode, useEffect, useState } from 'react';

import spacing from '../../assets/styles/spacing.module.scss';

const SettingsUsersPage: Story = ({ storyState }) => {
  const [isActiveTab, setIsActiveTab] = useState('all');

  const { searchValue, setSearchValue, searchValuesList, handleSearchValueAdd, handleClearAll } =
    useSearchExample();

  const tableHead = [
    {
      minWidth: '220',
      title: 'User',
      flexgrow: '1',
    },
    {
      minWidth: '220',
      title: 'Invite by',
      flexgrow: '1',
    },
    {
      minWidth: '180',
      title: 'Invite date',
    },
    {
      minWidth: '180',
      title: 'Last update',
    },
    {
      minWidth: '160',
      title: 'User Status',
      align: 'center',
    },
    {
      minWidth: '110',
      title: 'Action',
      align: 'center',
    },
  ];

  const usersTabs = [
    {
      id: 'all',
      name: 'All',
      icon: <AllIcon />,
    },
    {
      id: 'accepted',
      name: 'Accepted',
      icon: <AcceptedIcon />,
    },
    {
      id: 'invited',
      name: 'Invited',
      icon: <ExclamationIcon />,
    },
    {
      id: 'blocked',
      name: 'Blocked',
      icon: <CrossIcon />,
    },
  ];

  const handleToggleTabStatus = (id: string) => {
    setIsActiveTab(id);
  };

  return (
    <ModalSettings title="Organization settings" className={spacing.p20}>
      <Container className={spacing.mt20} flexgrow="1" overflow="y-auto">
        <NavigationSettingsSB selected="Users" />
        <SettingsContainer className={spacing.p24} width="100p" paddingX="0" paddingY="0">
          {storyState === 'loaded' && (
            <>
              <Container justifycontent="space-between" alignitems="center">
                <Text type="title-bold">Users</Text>
                <Button width="220" height="40" iconLeft={<PlusIcon />} onClick={() => {}}>
                  Create New Invite
                </Button>
              </Container>
              <Container className={classNames(spacing.mt24, spacing.w460fixed)}>
                <Search
                  height="40"
                  width="full"
                  name="settings-user-search"
                  placeholder="Search by name"
                  value={searchValue}
                  onChange={(e) => setSearchValue(e.target.value)}
                  searchParams={searchValuesList}
                  onSearch={handleSearchValueAdd}
                  onClear={handleClearAll}
                />
              </Container>
              <GeneralTabWrapper>
                {usersTabs.map(({ id, name }) => (
                  <GeneralTab
                    key={id}
                    id={id}
                    active={isActiveTab === id}
                    onClick={() => {
                      handleToggleTabStatus(id);
                    }}
                  >
                    {name}
                  </GeneralTab>
                ))}
              </GeneralTabWrapper>
              <TableNew
                noResults={false}
                className={spacing.mt24}
                tableHead={
                  <TableHeadNew>
                    {tableHead.map((item, index) => (
                      <TableTitleNew
                        key={index}
                        minWidth={item.minWidth as TableCellWidth}
                        flexgrow={item.flexgrow as TableTitleNewProps['flexgrow']}
                        align={item.align as TableTitleNewProps['align']}
                        title={item.title}
                        onClick={() => {}}
                      />
                    ))}
                  </TableHeadNew>
                }
              >
                <TableBodyNew>
                  {Array.from(Array(19)).map((_, index) => (
                    <TableRowNew key={index} gap="8">
                      <TableCellTextNew
                        minWidth="220"
                        secondaryColor="grey-100"
                        value="User name"
                        secondaryValue="username@gmail.com"
                        iconLeft={
                          <Logo content="user" radius="rounded" size="24" name="User name" />
                        }
                        flexgrow="1"
                      />
                      <TableCellTextNew
                        minWidth="220"
                        secondaryColor="grey-100"
                        value="Vitalii Kvasha"
                        secondaryValue="vitaliikvasha@gmail.com"
                        iconLeft={
                          <Logo content="user" radius="rounded" size="24" name="Vitalii Kvasha" />
                        }
                        flexgrow="1"
                      />
                      <TableCellTextNew
                        minWidth="180"
                        color="grey-100"
                        type="caption-regular"
                        value="Feb 22, 2023"
                      />
                      <TableCellTextNew
                        minWidth="180"
                        color="grey-100"
                        type="caption-regular"
                        value="Feb 22, 2023"
                      />
                      <TableCellNew minWidth="160" justifycontent="center" alignitems="center">
                        <Tag icon={<CrossIcon />} color="red" text="Blocked" padding="4-8" />
                      </TableCellNew>

                      <TableCellNew minWidth="110" alignitems="center" justifycontent="center">
                        <DropDown
                          flexdirection="column"
                          padding="8"
                          gap="4"
                          control={({ handleOpen }) => (
                            <IconButton
                              icon={<DropdownIcon />}
                              onClick={handleOpen}
                              background={'transparent'}
                              color="grey-100-violet-90"
                            />
                          )}
                        >
                          <DropDownButton size="160" icon={<EyeIcon />} onClick={() => {}}>
                            Open
                          </DropDownButton>
                          <DropDownButton size="160" icon={<ResendIcon />} onClick={() => {}}>
                            Resend
                          </DropDownButton>
                          <DropDownButton size="160" icon={<TickCircleIcon />} onClick={() => {}}>
                            Unblock user
                          </DropDownButton>
                          <DropDownButton
                            type="danger"
                            size="160"
                            icon={<RemoveIcon />}
                            onClick={() => {}}
                          >
                            Block user
                          </DropDownButton>
                        </DropDown>
                      </TableCellNew>
                    </TableRowNew>
                  ))}
                </TableBodyNew>
              </TableNew>
            </>
          )}
          {storyState === 'empty' && (
            <MainPageEmpty
              title="Create new Invite"
              description="You have no invites created. Your invites will be displayed here."
              action={
                <Button
                  width="220"
                  height="40"
                  background={'blue-10-blue-20'}
                  color="violet-90-violet-100"
                  iconLeft={<PlusIcon />}
                  onClick={() => {}}
                >
                  Create Invite
                </Button>
              }
            />
          )}
        </SettingsContainer>
      </Container>
    </ModalSettings>
  );
};

export const SetUpAccess: React.FC = () => {
  const tabContent: Record<ActiveTabs, ReactNode> = {
    banking: <SetUpAccessBankingContent />,
    accounting: <SetUpAccessAccountingContent />,
    sales: <SetUpAccessBankingContent />,
    expenses: <SetUpAccessBankingContent />,
    reports: <SetUpAccessReportsContent />,
  };

  const [activeTab, setActiveTab] = useState<ActiveTabs>('banking');

  return (
    <Modal
      size="960"
      background={'grey-10'}
      header={
        <ModalHeaderNew title="Set up access" border titlePosition="center" onClose={() => {}} />
      }
      pluginScrollDisabled
      footer={
        <ModalFooterNew justifycontent="space-between" border>
          <Container alignitems="center" gap="16">
            <Button
              height="48"
              iconLeft={<ArrowLeftIcon />}
              navigation
              width="180"
              background={'white-100'}
              border="grey-20-grey-90"
              color="grey-100-black-100"
              onClick={() => {}}
            >
              Back
            </Button>
            <Button height="48" width="180" onClick={() => {}}>
              Save
            </Button>
          </Container>
        </ModalFooterNew>
      }
    >
      <Container
        gap="32"
        className={classNames(spacing.pl40, spacing.mr24, spacing.h100p)}
        justifycontent="space-between"
      >
        <Container flexdirection="column" justifycontent="center" gap="12">
          {tabs.map(({ id, title, icon }) => (
            <Button
              height="40"
              background={activeTab === id ? 'white-100' : 'grey-10-white-100'}
              color={activeTab === id ? 'black-100' : 'grey-90-black-100'}
              onClick={() => {
                setActiveTab(id);
              }}
              iconLeft={icon}
              align="left"
            >
              {title}
            </Button>
          ))}
        </Container>
        <Container
          gap="32"
          flexdirection="row"
          customScroll
          overflow="auto"
          className={classNames(spacing.h100p, spacing.w100p, spacing.w750)}
        >
          {tabContent[activeTab]}
        </Container>
      </Container>
    </Modal>
  );
};

export const SendNewInvite: React.FC = () => {
  const tabContent: Record<ActiveTabs, ReactNode> = {
    banking: <UserProfileBankingTabContent />,
    accounting: <UserProfileAccountingTabContent />,
    sales: <UserProfileBankingTabContent />,
    expenses: <UserProfileBankingTabContent />,
    reports: <UserProfileBankingTabContent />,
  };

  const [notesValue, setNotesValue] = useState('');
  const [tags, setTags] = useState<TagType[]>([]);
  const [activeTab, setActiveTab] = useState<ActiveTabs>('banking');
  const onEmailChangeCallBack = (t: TagType[]) => setTags(t);
  const [isDisabled, setIsDisabled] = useState<boolean>(true);

  useEffect(() => {
    setIsDisabled(tags.some((tag) => tag.error));
  }, [tags]);
  return (
    <Modal
      overlay="grey-10"
      size="full"
      background={'grey-10'}
      pluginScrollDisabled
      classNameModalBody={spacing.p24}
      header={
        <Container
          className={classNames(spacing.w100p, spacing.p24)}
          alignitems="center"
          justifycontent="space-between"
          flexwrap="wrap"
          style={{ borderBottom: '1px solid #EDEFF1' }}
        >
          <Container gap="24" alignitems="center">
            <Text type="title-bold">Send new invite</Text>
            <Divider type="vertical" height="20" background={'grey-90'} />
            <Container alignitems="center" gap="32">
              <Container gap="12" alignitems="center">
                <Logo content="user" size="24" name="Vitalii Kvasha" />
                <Container flexdirection="column">
                  <Text type="caption-regular" color="grey-100">
                    Created by
                  </Text>
                  <Text type="body-medium">Vitalii Kvasha</Text>
                </Container>
              </Container>
              <Container gap="12" alignitems="center">
                <CircledIcon size="24" background={'grey-20'} icon={<CalendarIcon />} />
                <Container flexdirection="column">
                  <Text type="caption-regular" color="grey-100">
                    Date
                  </Text>
                  <Text type="body-medium">12.02.2023</Text>
                </Container>
              </Container>
            </Container>

            <Divider type="vertical" height="20" background={'grey-90'} />

            <Tag
              size="32"
              icon={<DocumentIcon />}
              padding="4-8"
              cursor="default"
              text="New"
              color="green"
            />
          </Container>

          <IconButton
            size="32"
            icon={<ClearIcon />}
            onClick={() => {}}
            background={'white-100'}
            color="grey-100"
          />
        </Container>
      }
      footer={
        <ModalFooterNew justifycontent="space-between" background={'grey-10'} border>
          <Button height="48" width="220" onClick={() => {}} disabled={isDisabled}>
            Send
          </Button>
        </ModalFooterNew>
      }
    >
      <EmailNotesInput
        onChange={onEmailChangeCallBack}
        notesValue={notesValue}
        onNotesChange={(e) => setNotesValue(e.target.value)}
      />

      <Container
        className={spacing.mt24}
        flexdirection="column"
        radius
        flexgrow="1"
        overflow="hidden"
      >
        <Container
          background={'white-100'}
          className={spacing.p24}
          borderRadius="10"
          gap="16"
          flexdirection="column"
          flexgrow="1"
          overflow="hidden"
        >
          <Container justifycontent="space-between">
            <Text type="body-medium">Permission setup</Text>
            <Button
              background={'blue-10-blue-20'}
              color="violet-90-violet-100"
              width="auto"
              font="text-medium"
              height="24"
              iconLeft={<PlusIcon />}
            >
              Create new
            </Button>
          </Container>
          <Container customScroll gap="16">
            <Container
              background={'grey-10'}
              borderRadius="8"
              gap="12"
              flexdirection="column"
              className={classNames(spacing.pX24, spacing.w506, spacing.pY16)}
              customScroll
              flexgrow="1"
            >
              <Container justifycontent="space-between" alignitems="center">
                <Text type="body-regular-14" color="grey-100">
                  Business Structure
                </Text>
                {/* <IconButton
                  icon={<EditIcon />}
                  size="24"
                  background="transparent"
                  color="grey-100-yellow-90"
                  onClick={() => {}}
                /> */}
                <DropDown
                  flexdirection="column"
                  padding="8"
                  gap="4"
                  control={({ handleOpen }) => (
                    <IconButton
                      size="16"
                      icon={<DropdownIcon />}
                      onClick={handleOpen}
                      background={'transparent'}
                      color="grey-100-violet-90"
                    />
                  )}
                >
                  <DropDownButton size="160" icon={<EditIcon />} onClick={() => {}}>
                    Edit Business structure
                  </DropDownButton>
                  <DropDownButton size="160" icon={<EditIcon />} onClick={() => {}}>
                    Edit Access
                  </DropDownButton>
                  <Divider fullHorizontalWidth />
                  <DropDownButton size="160" type="danger" icon={<TrashIcon />} onClick={() => {}}>
                    Delete Permission setup
                  </DropDownButton>
                </DropDown>
              </Container>
              <Container className={spacing.h100}>
                <TableNew
                  noResults={false}
                  tableHead={
                    <TableHeadNew background={'grey-10'} margin="none">
                      <TableTitleNew
                        background={'grey-10'}
                        minWidth="32"
                        padding="none"
                        title="#"
                      />
                      <TableTitleNew
                        background={'grey-10'}
                        minWidth="200"
                        title="Business Division"
                      />
                      <TableTitleNew
                        background={'grey-10'}
                        minWidth="210"
                        title="Legal entity"
                        flexgrow="1"
                      />
                    </TableHeadNew>
                  }
                >
                  <TableBodyNew>
                    {Array.from(Array(3)).map((_, index) => (
                      <TableRowNew
                        key={index}
                        background={'transparent'}
                        size="28"
                        cursor="auto"
                        gap="8"
                      >
                        <TableCellTextNew
                          minWidth="32"
                          padding="8"
                          align="center"
                          color="grey-100"
                          value={`${index + 1}`}
                        />
                        <TableCellNew minWidth="200" fixedWidth padding="0" gap="8">
                          <Container
                            className={classNames(spacing.w100p, spacing.pr30, spacing.pl10)}
                            alignitems="center"
                            justifycontent="space-between"
                            gap="8"
                          >
                            <Tag
                              color="grey-black"
                              font="body-regular-14"
                              padding="0-4"
                              truncateText
                              text={`Some name of business # ${index + 1}`}
                              align="left"
                            />
                            <Icon icon={<ArrowsSwitchIcon />} />
                          </Container>
                        </TableCellNew>
                        <TableCellNew minWidth="210" alignitems="center" padding="0" fixedWidth>
                          <Tag
                            color="grey-black"
                            font="body-regular-14"
                            padding="0-4"
                            text={`Legal Entity # 00${index + 1}`}
                            align="left"
                            truncateText
                            additionalCounter={2}
                            tooltip={
                              <Container flexdirection="column">
                                <Text color="white-100">Legal Entity #001-1</Text>
                                <Text color="white-100">Legal Entity #001-2</Text>
                                <Text color="white-100">Legal Entity #001-3</Text>
                              </Container>
                            }
                          />
                        </TableCellNew>
                      </TableRowNew>
                    ))}
                  </TableBodyNew>
                </TableNew>
              </Container>
              <Divider fullHorizontalWidth />
              <Container justifycontent="space-between" alignitems="center">
                <Text type="body-regular-14" color="grey-100">
                  Access
                </Text>
                {/* <IconButton
                  icon={<EditIcon />}
                  size="24"
                  background="transparent"
                  color="grey-100-yellow-90"
                  onClick={() => {}}
                /> */}
              </Container>
              <Container gap="16">
                <Container flexdirection="column" justifycontent="flex-start" gap="12">
                  {tabs.map(({ id, title, icon }) => (
                    <Button
                      height="32"
                      background={activeTab === id ? 'white-100' : 'grey-10-white-100'}
                      color={activeTab === id ? 'black-100' : 'grey-90-black-100'}
                      onClick={() => {
                        setActiveTab(id);
                      }}
                      iconLeft={icon}
                      align="left"
                    >
                      {title}
                    </Button>
                  ))}
                </Container>
                <Container
                  flexgrow="1"
                  flexdirection="column"
                  gap="12"
                  overflow="auto"
                  customScroll
                >
                  {tabContent[activeTab]}
                </Container>
              </Container>
            </Container>
          </Container>
        </Container>
      </Container>
    </Modal>
  );
};

export const UserProfile: React.FC = () => {
  const tabContent: Record<ActiveTabs, ReactNode> = {
    banking: <UserProfileBankingTabContent />,
    accounting: <UserProfileAccountingTabContent />,
    sales: <UserProfileBankingTabContent />,
    expenses: <UserProfileBankingTabContent />,
    reports: <UserProfileBankingTabContent />,
  };

  const [activeTab, setActiveTab] = useState<ActiveTabs>('banking');

  return (
    <Modal
      overlay="grey-10"
      size="full"
      background={'grey-10'}
      pluginScrollDisabled
      classNameModalBody={spacing.p24}
      flexdirection="row"
      header={
        <Container
          className={classNames(spacing.w100p, spacing.p24)}
          alignitems="center"
          justifycontent="space-between"
          flexwrap="wrap"
          style={{ borderBottom: '1px solid #EDEFF1' }}
        >
          <Container gap="24" alignitems="center">
            <Text type="title-bold">User Profile</Text>
          </Container>

          <IconButton
            size="32"
            icon={<ClearIcon />}
            onClick={() => {}}
            background={'white-100'}
            color="grey-100"
          />
        </Container>
      }
      footer={
        <ModalFooterNew justifycontent="space-between" background={'grey-10'} border>
          <Button
            background={'white-100'}
            border="grey-20-grey-90"
            color="grey-100-black-100"
            height="48"
            width="220"
            onClick={() => {}}
          >
            Close
          </Button>
        </ModalFooterNew>
      }
    >
      <UserProfileCard
        name="Vitalii Kvasha"
        status="accepted"
        position="Product manager"
        email="vitaliikvasha@gmail.com"
        phone="+380442569991"
      />
      <Container
        flexdirection="column"
        background={'white-100'}
        radius
        flexgrow="1"
        gap="24"
        className={spacing.p24}
      >
        <Container alignitems="center" justifycontent="space-between" gap="24">
          <GeneralTabWrapper>
            <GeneralTab font="body-regular-14" id="role-1" onClick={() => {}} active>
              Role 1
            </GeneralTab>
            <GeneralTab font="body-regular-14" id="role-2" onClick={() => {}} active={false}>
              Role 2
            </GeneralTab>
            <GeneralTab font="body-regular-14" id="role-3" onClick={() => {}} active={false}>
              Role 3
            </GeneralTab>
          </GeneralTabWrapper>
          <Button
            background={'blue-10-blue-20'}
            color="violet-90-violet-100"
            width="auto"
            font="text-medium"
            height="24"
            iconLeft={<PlusIcon />}
          >
            Create new
          </Button>
        </Container>
        <Container
          className={spacing.p24}
          radius
          flexgrow="1"
          background={'grey-10'}
          gap="24"
          overflow="hidden"
        >
          <Container borderRadius="8" flexdirection="column" overflow="hidden" gap="16">
            <Container justifycontent="space-between" alignitems="center">
              <Text type="body-regular-14" color="grey-100">
                Business Structure
              </Text>
              <IconButton
                icon={<EditIcon />}
                size="24"
                background={'transparent'}
                color="grey-100-yellow-90"
                onClick={() => {}}
              />
            </Container>
            <TableNew
              noResults={false}
              tableHead={
                <TableHeadNew background={'grey-10'} margin="none">
                  <TableTitleNew background={'grey-10'} minWidth="32" padding="none" title="#" />
                  <TableTitleNew background={'grey-10'} minWidth="200" title="Business Division" />
                  <TableTitleNew
                    background={'grey-10'}
                    minWidth="210"
                    title="Legal entity"
                    flexgrow="1"
                  />
                </TableHeadNew>
              }
            >
              <TableBodyNew>
                {Array.from(Array(20)).map((_, index) => (
                  <TableRowNew
                    key={index}
                    background={'transparent'}
                    size="28"
                    cursor="auto"
                    gap="8"
                  >
                    <TableCellTextNew
                      minWidth="32"
                      padding="8"
                      align="center"
                      color="grey-100"
                      value={`${index + 1}`}
                    />
                    <TableCellNew minWidth="200" fixedWidth padding="0" gap="8">
                      <Container
                        className={classNames(spacing.w100p, spacing.pr30, spacing.pl10)}
                        alignitems="center"
                        justifycontent="space-between"
                        gap="8"
                      >
                        <Tag
                          color="grey-black"
                          font="body-regular-14"
                          padding="0-4"
                          truncateText
                          text={`Some name of business # ${index + 1}`}
                          align="left"
                        />
                        <Icon icon={<ArrowsSwitchIcon />} />
                      </Container>
                    </TableCellNew>
                    <TableCellNew minWidth="210" alignitems="center" padding="0" fixedWidth>
                      <Tag
                        color="grey-black"
                        font="body-regular-14"
                        padding="0-4"
                        text={`Legal Entity # 00${index + 1}`}
                        align="left"
                        truncateText
                        additionalCounter={2}
                        tooltip={
                          <Container flexdirection="column">
                            <Text color="white-100">Legal Entity #001-1</Text>
                            <Text color="white-100">Legal Entity #001-2</Text>
                            <Text color="white-100">Legal Entity #001-3</Text>
                          </Container>
                        }
                      />
                    </TableCellNew>
                  </TableRowNew>
                ))}
              </TableBodyNew>
            </TableNew>
          </Container>
          <Divider type="vertical" background={'grey-20'} height="full" />
          <Container flexgrow="1" flexdirection="column" gap="24">
            <Container justifycontent="space-between" alignitems="center">
              <Text type="body-regular-14" color="grey-100">
                Access
              </Text>
              {/* <IconButton
                icon={<EditIcon />}
                size="24"
                background="transparent"
                color="grey-100-yellow-90"
                onClick={() => {}}
              /> */}
            </Container>
            <Container gap="16" overflow="hidden">
              <Container gap="16" overflow="hidden" flexgrow="1">
                <Container flexdirection="column" gap="12">
                  {tabs.map(({ id, title, icon }) => (
                    <Button
                      height="32"
                      background={activeTab === id ? 'white-100' : 'grey-10-white-100'}
                      color={activeTab === id ? 'black-100' : 'grey-90-black-100'}
                      onClick={() => {
                        setActiveTab(id);
                      }}
                      iconLeft={icon}
                      align="left"
                    >
                      {title}
                    </Button>
                  ))}
                </Container>
                <Container
                  flexgrow="1"
                  flexdirection="column"
                  gap="12"
                  overflow="auto"
                  customScroll
                >
                  {tabContent[activeTab]}
                </Container>
              </Container>
            </Container>
          </Container>
        </Container>
      </Container>
    </Modal>
  );
};

export default {
  title: 'Pages/General Settings/Users',
  component: SettingsUsersPage,
  argTypes: {
    storyState: {
      options: ['empty', 'loading', 'loaded'],
      control: {
        type: 'radio',
        labels: {
          empty: 'Empty page',
          loading: 'Page is loading',
          loaded: 'Page loaded',
        },
      },
    },
  },
} as Meta;

export const SettingsUsers = SettingsUsersPage.bind({});
SettingsUsers.args = {
  storyState: 'loaded',
};
