import { DropDown } from 'Components/DropDown';
import { ContentLoader } from 'Components/base/ContentLoader';
import { Divider } from 'Components/base/Divider';
import { Icon } from 'Components/base/Icon';
import { Switch } from 'Components/base/Switch';
import { TagSegmented } from 'Components/base/TagSegmented';
import { Tooltip } from 'Components/base/Tooltip';
import { Button } from 'Components/base/buttons/Button';
import { ButtonGroup } from 'Components/base/buttons/ButtonGroup';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { SettingsContainer } from 'Components/base/grid/SettingsContainer';
import { SelectNew } from 'Components/base/inputs/SelectNew';
import { OptionSingleNew } from 'Components/base/inputs/SelectNew/options/OptionSingleNew';
import { Text } from 'Components/base/typography/Text';
import { MainPageEmpty } from 'Components/custom/MainPage/MainPageEmpty';
import { ModalSettings } from 'Components/custom/Settings/ModalSettings';
import { SettingsInfoAction } from 'Components/custom/Settings/SettingsInfoAction';
import { SettingsNavigation } from 'Components/custom/Settings/SettingsNavigation';
import { SettingsNavigationButton } from 'Components/custom/Settings/SettingsNavigationButton';
import { SettingsTitle } from 'Components/custom/Settings/SettingsTitle';
import { Logo } from 'Components/elements/Logo';
import { PreferencesInfo } from 'Components/elements/PreferencesInfo';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableTitleNew } from 'Components/elements/Table/TableTitleNew';
import { FilterDropDown } from 'Components/elements/filters/FilterDropDown';
import { CircledIcon } from 'Components/graphics/CircledIcon';
import { FolderIcon } from 'Components/graphics/FolderIcon';
import { Search } from 'Components/inputs/Search';
import { Table } from 'Components/old/Table';
import { TableBody } from 'Components/old/Table/TableBody';
import { TableCell } from 'Components/old/Table/TableCell';
import { TableHead } from 'Components/old/Table/TableHead';
import { TableRow } from 'Components/old/Table/TableRow';
import { TableTitle } from 'Components/old/Table/TableTitle';
import { options } from 'Mocks/fakeOptions';
import { ReactComponent as EyeIcon } from 'Svg/16/eye.svg';
import { ReactComponent as FilterIcon } from 'Svg/16/filter.svg';
import { ReactComponent as DropdownIcon } from 'Svg/20/dropdownbutton.svg';
import { ReactComponent as InfoIcon } from 'Svg/24/info-dark.svg';
import { ReactComponent as InfoGrayIcon } from 'Svg/24/info-gray.svg';
import { ReactComponent as ArrowRightIcon } from 'Svg/v2/16/arrow-right.svg';
import { ReactComponent as PencilIcon } from 'Svg/v2/16/pencil.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/General Settings',
};

interface Information {
  id: string;
  name: string;
  division: string;
  quantity: number;
  divisionsQuantity: number;
}

const data: Information[] = [
  {
    id: '1',
    name: 'Genesis Media & Mobile Application',
    division: 'Company in GENESIS',
    quantity: 25,
    divisionsQuantity: 1,
  },
  {
    id: '2',
    name: 'Genesis Media & Mobile Application',
    division: 'Company in GENESIS',
    quantity: 25,
    divisionsQuantity: 2,
  },
  {
    id: '3',
    name: 'Genesis Media & Mobile Application',
    division: 'Company in GENESIS',
    quantity: 25,
    divisionsQuantity: 2,
  },
  {
    id: '4',
    name: 'Genesis Media & Mobile Application',
    division: 'Company in GENESIS',
    quantity: 25,
    divisionsQuantity: 0,
  },
  {
    id: '5',
    name: 'Genesis Media & Mobile Application',
    division: 'Company in GENESIS',
    quantity: 25,
    divisionsQuantity: 122,
  },
  {
    id: '6',
    name: 'Genesis Media & Mobile Application',
    division: 'Company in GENESIS',
    quantity: 25,
    divisionsQuantity: 13,
  },
  {
    id: '7',
    name: 'Genesis Media & Mobile Application',
    division: 'Company in GENESIS',
    quantity: 25,
    divisionsQuantity: 53,
  },
  {
    id: '8',
    name: 'Genesis Media & Mobile Application',
    division: 'Company in GENESIS',
    quantity: 25,
    divisionsQuantity: 1,
  },
  {
    id: '9',
    name: 'Genesis Media & Mobile Application',
    division: 'Company in GENESIS',
    quantity: 25,
    divisionsQuantity: 15,
  },
  {
    id: '10',
    name: 'Genesis Media & Mobile Application',
    division: 'Company in GENESIS',
    quantity: 25,
    divisionsQuantity: 18,
  },
  {
    id: '11',
    name: 'Genesis Media & Mobile Application',
    division: 'Company in GENESIS',
    quantity: 25,
    divisionsQuantity: 1,
  },
  {
    id: '12',
    name: 'Genesis Media & Mobile Application',
    division: 'Company in GENESIS',
    quantity: 25,
    divisionsQuantity: 1,
  },
];

export const SettingsOrganizationUser: React.FC = () => (
  <ModalSettings title="Organization settings" className={spacing.p20}>
    <Container className={spacing.mt20} flexgrow="1" overflow="y-auto">
      <SettingsNavigation className={spacing.mr20}>
        <Container
          className={spacing.w100p}
          flexdirection="column"
          alignitems="center"
          flexgrow="1"
        >
          <Logo content="company" size="lg" name="Genesis" color="#FF7052" />
          <Text className={spacing.mt20} type="body-medium" align="center" transform="uppercase">
            Genesis
          </Text>
          <Divider className={spacing.mY20} fullHorizontalWidth />
          <SettingsNavigationButton title="Organization" selected onClick={() => {}} />
          <SettingsNavigationButton title="Preferences" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Advanced" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Security" selected={false} onClick={() => {}} />
          <SettingsNavigationButton
            title="Business divisions"
            selected={false}
            onClick={() => {}}
          />
          <SettingsNavigationButton title="Legal entities" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Users" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="User roles" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Paidlog approvers" selected={false} onClick={() => {}} />
        </Container>
      </SettingsNavigation>
      <SettingsContainer>
        <Container alignitems="center" className={spacing.mb40}>
          <InfoIcon />
          <Text className={spacing.ml10} type="caption-semibold">
            Only admin role can change this settings
          </Text>
        </Container>
        <SettingsTitle className={spacing.mb20} title="Information:" />
        <SettingsInfoAction title="Organization name" value="GENESIS" />
        <SettingsInfoAction title="Date of creation" value="10 May 2019" />
        <SettingsInfoAction title="Creator" value="karine.mnatsakanian@mail.com" />
      </SettingsContainer>
    </Container>
  </ModalSettings>
);

export const SettingsOrganizationAdmin: React.FC = () => (
  <ModalSettings title="Organization settings" className={spacing.p20}>
    <Container className={spacing.mt20} flexgrow="1" overflow="y-auto">
      <SettingsNavigation className={spacing.mr20}>
        <Container
          className={spacing.w100p}
          flexdirection="column"
          alignitems="center"
          flexgrow="1"
        >
          <Logo content="company" size="lg" name="Genesis" color="#FF7052" />
          <Text className={spacing.mt20} type="body-medium" align="center" transform="uppercase">
            Genesis
          </Text>
          <Divider className={spacing.mY20} fullHorizontalWidth />
          <SettingsNavigationButton title="Organization" selected onClick={() => {}} />
          <SettingsNavigationButton title="Preferences" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Advanced" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Security" selected={false} onClick={() => {}} />
          <SettingsNavigationButton
            title="Business divisions"
            selected={false}
            onClick={() => {}}
          />
          <SettingsNavigationButton title="Legal entities" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Users" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="User roles" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Paidlog approvers" selected={false} onClick={() => {}} />
        </Container>
      </SettingsNavigation>
      <SettingsContainer>
        <SettingsTitle title="Information:" />
        <Container
          className={classNames(spacing.mY30, spacing.h100p, spacing.h80)}
          alignitems="center"
        >
          <Logo
            className={spacing.mr24}
            content="company"
            size="lg"
            name="Genesis"
            color="#FF7052"
          />
          <LinkButton color="grey-100-violet-90" onClick={() => {}}>
            Change logo
          </LinkButton>
        </Container>
        <SettingsInfoAction
          title="Organization name"
          value="GENESIS"
          actionText="Change"
          onClick={() => {}}
        />
        <SettingsInfoAction title="Date of creation" value="10 May 2019" />
        <SettingsInfoAction title="Creator" value="karine.mnatsakanian@mail.com" />
      </SettingsContainer>
    </Container>
  </ModalSettings>
);

export const SettingsPreferencesUser: React.FC = () => (
  <ModalSettings title="Organization settings" className={spacing.p20}>
    <Container className={spacing.mt20} flexgrow="1" overflow="y-auto">
      <SettingsNavigation className={spacing.mr20}>
        <Container
          className={spacing.w100p}
          flexdirection="column"
          alignitems="center"
          flexgrow="1"
        >
          <Logo content="company" size="lg" name="Genesis" color="#FF7052" />
          <Text className={spacing.mt20} type="body-medium" align="center" transform="uppercase">
            Genesis
          </Text>
          <Divider className={spacing.mY20} fullHorizontalWidth />
          <SettingsNavigationButton title="Organization" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Preferences" selected onClick={() => {}} />
          <SettingsNavigationButton title="Advanced" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Security" selected={false} onClick={() => {}} />
          <SettingsNavigationButton
            title="Business divisions"
            selected={false}
            onClick={() => {}}
          />
          <SettingsNavigationButton title="Legal entities" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Users" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="User roles" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Paidlog approvers" selected={false} onClick={() => {}} />
        </Container>
      </SettingsNavigation>
      <SettingsContainer>
        <Container alignitems="center" className={spacing.mb40}>
          <InfoIcon />
          <Text className={spacing.ml10} type="caption-semibold">
            Only admin role can change this settings
          </Text>
        </Container>
        <SettingsTitle className={spacing.mb20} title="Date and time settings:" />
        <SettingsInfoAction title="Date format" value="MM/dd/yyyy" />
        <SettingsInfoAction title="Number format" value="100,000.00" />
        <SettingsInfoAction title="Customer label" value="Clients" />
        <SettingsTitle className={spacing.mt30} title="Preferences:" />
        <PreferencesInfo
          className={spacing.mt12}
          active
          conditionText="Warn if duplicate check number is used"
        />
        <PreferencesInfo
          className={spacing.mt12}
          active={false}
          conditionText="Warn if duplicate bill number is used"
        />
        <PreferencesInfo
          className={spacing.mt12}
          active={false}
          conditionText="Warn if duplicate journal number is used"
        />
        <PreferencesInfo
          className={spacing.mt12}
          active={false}
          conditionText="Warn if duplicate invoice number is used"
        />
      </SettingsContainer>
    </Container>
  </ModalSettings>
);

export const SettingsPreferencesAdmin: React.FC = () => (
  <ModalSettings title="Organization settings" className={spacing.p20}>
    <Container className={spacing.mt20} flexgrow="1" overflow="y-auto">
      <SettingsNavigation className={spacing.mr20}>
        <Container
          className={spacing.w100p}
          flexdirection="column"
          alignitems="center"
          flexgrow="1"
        >
          <Logo content="company" size="lg" name="Genesis" color="#FF7052" />
          <Text className={spacing.mt20} type="body-medium" align="center" transform="uppercase">
            Genesis
          </Text>
          <Divider className={spacing.mY20} fullHorizontalWidth />
          <SettingsNavigationButton title="Organization" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Preferences" selected onClick={() => {}} />
          <SettingsNavigationButton title="Advanced" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Security" selected={false} onClick={() => {}} />
          <SettingsNavigationButton
            title="Business divisions"
            selected={false}
            onClick={() => {}}
          />
          <SettingsNavigationButton title="Legal entities" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Users" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="User roles" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Paidlog approvers" selected={false} onClick={() => {}} />
        </Container>
      </SettingsNavigation>
      <SettingsContainer width="325">
        <SettingsTitle className={spacing.mb20} title="Date and time settings:" />
        <SelectNew name="date" onChange={() => {}} placeholder="MM/dd/yyyy" label="Date format">
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() =>
                    onClick({
                      label,
                      value,
                    })
                  }
                />
              ))}
            </>
          )}
        </SelectNew>
        <SelectNew
          name="number"
          className={spacing.mt12}
          onChange={() => {}}
          placeholder="100,000.00"
          label="Number format"
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() =>
                    onClick({
                      label,
                      value,
                    })
                  }
                />
              ))}
            </>
          )}
        </SelectNew>
        <SelectNew
          name="customerlabel"
          className={spacing.mt12}
          onChange={() => {}}
          placeholder="Clients"
          label="Customer label"
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() =>
                    onClick({
                      label,
                      value,
                    })
                  }
                />
              ))}
            </>
          )}
        </SelectNew>
        <SettingsTitle className={spacing.mt30} title="Preferences:" />
        <Container className={spacing.mt12}>
          <Switch className={spacing.mr12} id="checknumber" checked onChange={() => {}} />
          <Text type="body-regular-14" color="black-100">
            Warn if duplicate check number is used
          </Text>
        </Container>
        <Container className={spacing.mt12}>
          <Switch className={spacing.mr12} id="billnumber" checked={false} onChange={() => {}} />
          <Text type="body-regular-14" color="black-100">
            Warn if duplicate bill number is used
          </Text>
        </Container>
        <Container className={spacing.mt12}>
          <Switch className={spacing.mr12} id="journalnumber" checked={false} onChange={() => {}} />
          <Text type="body-regular-14" color="black-100">
            Warn if duplicate journal number is used
          </Text>
        </Container>
        <Container className={spacing.mt12}>
          <Switch className={spacing.mr12} id="invoicenumber" checked={false} onChange={() => {}} />
          <Text type="body-regular-14" color="black-100">
            Warn if duplicate invoice number is used
          </Text>
        </Container>
      </SettingsContainer>
    </Container>
  </ModalSettings>
);

export const SettingsAdvanced: React.FC = () => (
  <ModalSettings title="Organization settings" className={spacing.p20}>
    <Container className={spacing.mt20} flexgrow="1" overflow="y-auto">
      <SettingsNavigation className={spacing.mr20}>
        <Container
          className={spacing.w100p}
          flexdirection="column"
          alignitems="center"
          flexgrow="1"
        >
          <Logo content="company" size="lg" name="Genesis" color="#FF7052" />
          <Text className={spacing.mt20} type="body-medium" align="center" transform="uppercase">
            Genesis
          </Text>
          <Divider className={spacing.mY20} fullHorizontalWidth />
          <SettingsNavigationButton title="Organization" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Preferences" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Advanced" selected onClick={() => {}} />
          <SettingsNavigationButton title="Security" selected={false} onClick={() => {}} />
          <SettingsNavigationButton
            title="Business divisions"
            selected={false}
            onClick={() => {}}
          />
          <SettingsNavigationButton title="Legal entities" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Users" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="User roles" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Paidlog approvers" selected={false} onClick={() => {}} />
        </Container>
      </SettingsNavigation>
      <SettingsContainer width="325">
        <SettingsTitle className={spacing.mb20} title="Accounting settings:" />
        <SelectNew
          name="firstfiscalmonth"
          onChange={() => {}}
          placeholder="January"
          label="First month of fiscal year"
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() =>
                    onClick({
                      label,
                      value,
                    })
                  }
                />
              ))}
            </>
          )}
        </SelectNew>
        <SelectNew
          name="firstfiscalmonth"
          className={spacing.mt12}
          onChange={() => {}}
          placeholder="Same as fiscal month"
          label="First month of income tax year"
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() =>
                    onClick({
                      label,
                      value,
                    })
                  }
                />
              ))}
            </>
          )}
        </SelectNew>
        <SelectNew
          name="accountingmethod"
          className={spacing.mt12}
          onChange={() => {}}
          placeholder="Accrual"
          label="Accounting method"
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() =>
                    onClick({
                      label,
                      value,
                    })
                  }
                />
              ))}
            </>
          )}
        </SelectNew>
        <Container className={spacing.mt12} justifycontent="space-between">
          <Text type="body-regular-14" color="black-100">
            Close the books
          </Text>
          <Switch id="closebooks" checked onChange={() => {}} />
        </Container>
        <SettingsTitle className={spacing.mt30} title="Chart of accounts:" />
        <Container className={spacing.mt12} justifycontent="space-between">
          <Text type="body-regular-14" color="black-100">
            Enable account numbers
          </Text>
          <Switch id="closebooks" checked={false} onChange={() => {}} />
        </Container>
        <SettingsTitle className={spacing.mt30} title="Currency:" />
        <SelectNew
          name="reportingcurrency"
          className={spacing.mt20}
          onChange={() => {}}
          placeholder="United States Dollar"
          label="Reporting currency"
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() =>
                    onClick({
                      label,
                      value,
                    })
                  }
                />
              ))}
            </>
          )}
        </SelectNew>
        <Container className={spacing.mt12} justifycontent="space-between">
          <Text type="body-regular-14" color="black-100">
            Multicurrency
          </Text>
          <Switch id="closebooks" checked={false} onChange={() => {}} />
        </Container>
        <Container className={spacing.mt12} alignitems="center">
          <LinkButton icon={<ArrowRightIcon />} iconRight onClick={() => {}}>
            Manage currencies
          </LinkButton>
        </Container>
      </SettingsContainer>
    </Container>
  </ModalSettings>
);

export const SettingsSecurity: React.FC = () => (
  <ModalSettings title="Organization settings" className={spacing.p20}>
    <Container className={spacing.mt20} flexgrow="1" overflow="y-auto">
      <SettingsNavigation className={spacing.mr20}>
        <Container
          className={spacing.w100p}
          flexdirection="column"
          alignitems="center"
          flexgrow="1"
        >
          <Logo content="company" size="lg" name="Genesis" color="#FF7052" />
          <Text className={spacing.mt20} type="body-medium" align="center" transform="uppercase">
            Genesis
          </Text>
          <Divider className={spacing.mY20} fullHorizontalWidth />
          <SettingsNavigationButton title="Organization" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Preferences" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Advanced" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Security" selected onClick={() => {}} />
          <SettingsNavigationButton
            title="Business divisions"
            selected={false}
            onClick={() => {}}
          />
          <SettingsNavigationButton title="Legal entities" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Users" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="User roles" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Paidlog approvers" selected={false} onClick={() => {}} />
        </Container>
      </SettingsNavigation>
      <SettingsContainer width="325">
        <SettingsTitle className={spacing.mb20} title="Security settings:" />
        <SelectNew
          name="signout"
          onChange={() => {}}
          placeholder="One week"
          label="Sign out if user inactive for"
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() =>
                    onClick({
                      label,
                      value,
                    })
                  }
                />
              ))}
            </>
          )}
        </SelectNew>
        <SettingsTitle className={spacing.mt30} title="Authentication:" />
        <Container className={spacing.mt12} justifycontent="space-between">
          <Text type="body-regular-14" color="black-100">
            Two-Step Authentication
          </Text>
          <Switch id="closebooks" checked onChange={() => {}} />
        </Container>
        <Text className={spacing.mt16} type="text-regular" color="grey-100">
          Email Two-Step Authentication enabled for each user. So all accounts are protected with an
          additional password. Users cannot finish registration without verification code.
        </Text>
      </SettingsContainer>
    </Container>
  </ModalSettings>
);

export const SettingsBusinessDivisionsNone: React.FC = () => (
  <ModalSettings title="Organization settings" className={spacing.p20}>
    <Container className={spacing.mt20} flexgrow="1" overflow="y-auto">
      <SettingsNavigation className={spacing.mr20}>
        <Container
          className={spacing.w100p}
          flexdirection="column"
          alignitems="center"
          flexgrow="1"
        >
          <Logo content="company" size="lg" name="Genesis" color="#FF7052" />
          <Text className={spacing.mt20} type="body-medium" align="center" transform="uppercase">
            Genesis
          </Text>
          <Divider className={spacing.mY20} fullHorizontalWidth />
          <SettingsNavigationButton title="Organization" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Preferences" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Advanced" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Security" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Business divisions" selected onClick={() => {}} />
          <SettingsNavigationButton title="Legal entities" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Users" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="User roles" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Paidlog approvers" selected={false} onClick={() => {}} />
        </Container>
      </SettingsNavigation>
      <SettingsContainer width="100p">
        <Container flexgrow="1" flexdirection="column" justifycontent="center" alignitems="center">
          <Container
            className={classNames(spacing.w450)}
            flexdirection="column"
            justifycontent="center"
            alignitems="center"
          >
            <FolderIcon />
            <Text className={spacing.mt30} align="center" type="caption-semibold" color="grey-100">
              What is it
            </Text>
            <Text className={spacing.mt6} align="center" type="title-bold">
              Business divisions
            </Text>
            <Text className={spacing.mt16} align="center" type="body-regular-16" color="grey-100">
              Here you can create business divisions for your organization, including projects,
              subprojects etc.
            </Text>
            <LinkButton className={spacing.mt30} icon={<PlusIcon />} onClick={() => {}}>
              Create business division
            </LinkButton>
          </Container>
        </Container>
      </SettingsContainer>
    </Container>
  </ModalSettings>
);

export const SettingsBusinessDivisionsDefault: React.FC = () => (
  <ModalSettings title="Organization settings" className={spacing.p20}>
    <Container className={spacing.mt20} flexgrow="1" overflow="y-auto">
      <SettingsNavigation className={spacing.mr20}>
        <Container
          className={spacing.w100p}
          flexdirection="column"
          alignitems="center"
          flexgrow="1"
        >
          <Logo content="company" size="lg" name="Genesis" color="#FF7052" />
          <Text className={spacing.mt20} type="body-medium" align="center" transform="uppercase">
            Genesis
          </Text>
          <Divider className={spacing.mY20} fullHorizontalWidth />
          <SettingsNavigationButton title="Organization" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Preferences" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Advanced" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Security" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Business divisions" selected onClick={() => {}} />
          <SettingsNavigationButton title="Legal entities" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Users" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="User roles" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Paidlog approvers" selected={false} onClick={() => {}} />
        </Container>
      </SettingsNavigation>
      <SettingsContainer width="100p" paddingX="0" paddingY="0">
        <Container
          justifycontent="space-between"
          className={classNames(spacing.pt40, spacing.pX20)}
        >
          <Search name="search" placeholder="Search" />
          <ButtonGroup align="right">
            <LinkButton icon={<PlusIcon />} onClick={() => {}}>
              Create business division
            </LinkButton>
            <Divider type="vertical" className={spacing.mX20} />
            <Text type="button">What is it?</Text>
          </ButtonGroup>
        </Container>
        <Container alignitems="center" className={classNames(spacing.pX20, spacing.pY20)}>
          <Icon className={spacing.mr10} icon={<FilterIcon />} stroke="grey-100" />
          <FilterDropDown
            title="Business divisions type"
            value="All"
            onClear={() => {}}
            selectedAmount={0}
          />
          <Text type="text-medium" color="grey-100" className={spacing.mr10}>
            Divisions found:
          </Text>
          <Text type="caption-semibold">264</Text>
        </Container>
        <Table
          tableHead={
            <TableHead>
              <TableTitle minWidth="80" title="Logo" onClick={() => {}} />
              <TableTitle minWidth="380" flexgrow="1" title="Name" sortIcon onClick={() => {}} />
              <TableTitle
                minWidth="380"
                flexgrow="1"
                title="Division type"
                sortIcon
                onClick={() => {}}
              />
              <TableTitle minWidth="130" title="Number of LE" onClick={() => {}} />
              <TableTitle minWidth="60" title="Action" onClick={() => {}} />
            </TableHead>
          }
        >
          <TableBody result={264}>
            {data.map(({ id, ...item }) => (
              <TableRow key={id} background={'secondary'}>
                <TableCell minWidth="80" justifycontent="center" alignitems="center">
                  <Logo content="company" size="md" name={item.name} />
                </TableCell>
                <TableCell
                  minWidth="380"
                  flexgrow="1"
                  flexdirection="column"
                  justifycontent="center"
                >
                  <Text type="body-regular-14">{item.name}</Text>
                </TableCell>
                <TableCell minWidth="380" flexgrow="1" justifycontent="center">
                  <Text type="body-regular-14" color="grey-100">
                    {item.division}
                  </Text>
                </TableCell>
                <TableCell minWidth="130" justifycontent="center" alignitems="center">
                  <Text type="body-regular-14" color="grey-100">
                    {item.quantity}
                  </Text>
                </TableCell>
                <TableCell
                  minWidth="60"
                  flexdirection="row"
                  alignitems="center"
                  justifycontent="center"
                >
                  <DropDown
                    flexdirection="column"
                    padding="10"
                    control={({ handleOpen }) => (
                      <IconButton
                        icon={<DropdownIcon />}
                        onClick={handleOpen}
                        background={'transparent'}
                        color="grey-100-violet-90"
                      />
                    )}
                  >
                    <DropDownButton size="160" icon={<EyeIcon />} onClick={() => {}}>
                      Open
                    </DropDownButton>
                    <DropDownButton
                      size="160"
                      icon={<PlusIcon />}
                      onClick={() => {}}
                      className={spacing.mt5}
                    >
                      Add legal entity
                    </DropDownButton>
                    <Divider type="horizontal" fullHorizontalWidth className={spacing.mY5} />
                    <DropDownButton
                      size="160"
                      type="danger"
                      icon={<TrashIcon />}
                      onClick={() => {}}
                    >
                      Delete
                    </DropDownButton>
                  </DropDown>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </SettingsContainer>
    </Container>
  </ModalSettings>
);

export const SettingsBusinessDivisionsNoResults: React.FC = () => (
  <ModalSettings title="Organization settings" className={spacing.p20}>
    <Container className={spacing.mt20} flexgrow="1" overflow="y-auto">
      <SettingsNavigation className={spacing.mr20}>
        <Container
          className={spacing.w100p}
          flexdirection="column"
          alignitems="center"
          flexgrow="1"
        >
          <Logo content="company" size="lg" name="Genesis" color="#FF7052" />
          <Text className={spacing.mt20} type="body-medium" align="center" transform="uppercase">
            Genesis
          </Text>
          <Divider className={spacing.mY20} fullHorizontalWidth />
          <SettingsNavigationButton title="Organization" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Preferences" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Advanced" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Security" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Business divisions" selected onClick={() => {}} />
          <SettingsNavigationButton title="Legal entities" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Users" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="User roles" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Paidlog approvers" selected={false} onClick={() => {}} />
        </Container>
      </SettingsNavigation>
      <SettingsContainer width="100p" paddingX="0" paddingY="0">
        <Container
          justifycontent="space-between"
          className={classNames(spacing.pt40, spacing.pX20)}
        >
          <Search name="search" placeholder="Search" />
          <ButtonGroup align="right">
            <LinkButton icon={<PlusIcon />} onClick={() => {}}>
              Create business division
            </LinkButton>
            <Divider type="vertical" className={spacing.mX20} />
            <Text type="button">What is it?</Text>
          </ButtonGroup>
        </Container>
        <Container alignitems="center" className={classNames(spacing.pX20, spacing.pY20)}>
          <Icon className={spacing.mr10} icon={<FilterIcon />} stroke="grey-100" />
          <FilterDropDown
            title="Business divisions type"
            value="All"
            onClear={() => {}}
            selectedAmount={0}
          />
          <Text type="text-medium" color="grey-100" className={spacing.mr10}>
            Divisions found:
          </Text>
          <Text type="caption-semibold">0</Text>
        </Container>
        <Table
          tableHead={
            <TableHead>
              <TableTitle minWidth="80" title="Logo" onClick={() => {}} />
              <TableTitle minWidth="380" flexgrow="1" title="Name" sortIcon onClick={() => {}} />
              <TableTitle
                minWidth="380"
                flexgrow="1"
                title="Division type"
                sortIcon
                onClick={() => {}}
              />
              <TableTitle minWidth="130" title="Number of LE" onClick={() => {}} />
              <TableTitle minWidth="60" title="Action" onClick={() => {}} />
            </TableHead>
          }
        >
          <TableBody result={0}>
            <Container flexdirection="column" alignitems="center">
              <CircledIcon icon={<InfoGrayIcon />} />
              <Text className={classNames(spacing.mb16, spacing.mt30)} type="title-bold">
                No results found
              </Text>
              <Text type="body-regular-16">
                It seems we can’t find any results based on your search
              </Text>
            </Container>
          </TableBody>
        </Table>
      </SettingsContainer>
    </Container>
  </ModalSettings>
);

export const SettingsLegalEntitiesNone: React.FC = () => (
  <ModalSettings title="Organization settings" className={spacing.p20}>
    <Container className={spacing.mt20} flexgrow="1" overflow="y-auto">
      <SettingsNavigation className={spacing.mr20}>
        <Container
          className={spacing.w100p}
          flexdirection="column"
          alignitems="center"
          flexgrow="1"
        >
          <Logo content="company" size="lg" name="Genesis" color="#FF7052" />
          <Text className={spacing.mt20} type="body-medium" align="center" transform="uppercase">
            Genesis
          </Text>
          <Divider className={spacing.mY20} fullHorizontalWidth />
          <SettingsNavigationButton title="Organization" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Preferences" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Advanced" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Security" selected={false} onClick={() => {}} />
          <SettingsNavigationButton
            title="Business divisions"
            selected={false}
            onClick={() => {}}
          />
          <SettingsNavigationButton title="Legal entities" selected onClick={() => {}} />
          <SettingsNavigationButton title="Users" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="User roles" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Paidlog approvers" selected={false} onClick={() => {}} />
        </Container>
      </SettingsNavigation>
      <SettingsContainer width="100p">
        <Container flexdirection="column" className={spacing.h100p} gap="24">
          <Text type="title-bold">Legal entities</Text>

          <Container flexgrow="1" justifycontent="center" alignitems="center">
            <MainPageEmpty
              title="Create new Legal entity"
              description="You have no Legal entities created. Your Legal entities will be displayed here."
              action={
                <Button
                  width="220"
                  height="40"
                  background={'blue-10-blue-20'}
                  color="violet-90-violet-100"
                  iconLeft={<PlusIcon />}
                >
                  Create Legal entity
                </Button>
              }
            />
          </Container>
        </Container>
      </SettingsContainer>
    </Container>
  </ModalSettings>
);

export const SettingsLegalEntitiesDefault: React.FC = () => (
  <ModalSettings title="Organization settings" className={spacing.p20}>
    <Container className={spacing.mt20} flexgrow="1" overflow="y-auto">
      <SettingsNavigation className={spacing.mr20}>
        <Container
          className={spacing.w100p}
          flexdirection="column"
          alignitems="center"
          flexgrow="1"
        >
          <Logo content="company" size="lg" name="Genesis" color="#FF7052" />
          <Text className={spacing.mt20} type="body-medium" align="center" transform="uppercase">
            Genesis
          </Text>
          <Divider className={spacing.mY20} fullHorizontalWidth />
          <SettingsNavigationButton title="Organization" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Preferences" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Advanced" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Security" selected={false} onClick={() => {}} />
          <SettingsNavigationButton
            title="Business divisions"
            selected={false}
            onClick={() => {}}
          />
          <SettingsNavigationButton title="Legal entities" selected onClick={() => {}} />
          <SettingsNavigationButton title="Users" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="User roles" selected={false} onClick={() => {}} />
          <SettingsNavigationButton title="Paidlog approvers" selected={false} onClick={() => {}} />
        </Container>
      </SettingsNavigation>

      <SettingsContainer width="100p" paddingX="24" paddingY="24">
        <Container flexdirection="column" gap="24" className={spacing.h100p}>
          <Container flexdirection="column" gap="16" style={{ flexShrink: '0' }}>
            <Container justifycontent="space-between" alignitems="center">
              <Text type="title-bold">Legal entities</Text>
              <ContentLoader width="220px" height="40px" isLoading={!!0}>
                <Button
                  width="220"
                  height="40"
                  background={'violet-90-violet-100'}
                  color="white-100"
                  iconLeft={<PlusIcon />}
                >
                  Create Legal entity
                </Button>
              </ContentLoader>
            </Container>

            <ContentLoader height="40px" width="440px" isLoading={!!0}>
              <Search height="40" width="440" name="le-search" placeholder="Search by Name" />
            </ContentLoader>
          </Container>

          <Container flexdirection="column" flexgrow="1" className={spacing.h10min}>
            <TableNew
              tableHead={
                <TableHeadNew>
                  <TableTitleNew flexgrow="1" minWidth="300" title="Legal entity name" />
                  <TableTitleNew minWidth="340" title="Business units" fixedWidth />
                  <TableTitleNew minWidth="150" title="TAX ID" />
                  <TableTitleNew minWidth="380" title="Address" />
                  <TableTitleNew minWidth="120" title="Actions" align="center" />
                </TableHeadNew>
              }
            >
              <TableBodyNew>
                {[
                  {
                    id: 1,
                    name: 'Legal Entity Name',
                    country: 'country',
                    street: 'street',
                    city: 'city',
                    state: 'state',
                    zip: 'zip',
                    ein: '1234567890',
                    business_units: [
                      {
                        id: 1,
                        name: 'BU name',
                      },
                    ],
                  },
                ]?.map((le) => (
                  <TableRowNew key={le.id} gap="8">
                    <TableCellTextNew
                      flexgrow="1"
                      minWidth="300"
                      value={le.name}
                      iconLeft={<Logo content="user" radius="rounded" size="24" name={le.name} />}
                    />
                    <TableCellNew minWidth="340" fixedWidth alignitems="center">
                      <TagSegmented
                        mainLabel={'Chosen Business Unit'}
                        showCounter={le?.business_units?.length > 1}
                        counter={
                          <DropDown
                            flexdirection="column"
                            padding="8"
                            classnameInner={classNames(spacing.h300)}
                            customScroll
                            control={({ handleOpen }) => (
                              <Tooltip message={'Show All'}>
                                <TagSegmented.Counter
                                  counterValue={le?.business_units?.length - 1}
                                  onClick={handleOpen}
                                />
                              </Tooltip>
                            )}
                          >
                            <Text type="button" color="grey-100" style={{ paddingBottom: '4px' }}>
                              Business units
                            </Text>
                            {le?.business_units?.map(({ id }) => (
                              <li key={id}>
                                <Text type="body-regular-14" key={id} noWrap>
                                  {[
                                    {
                                      id: 1,
                                      name: 'Bu Name',
                                    },
                                  ]?.find(({ id: buId }) => id === buId)?.name || ''}
                                </Text>
                              </li>
                            ))}
                          </DropDown>
                        }
                      />
                    </TableCellNew>
                    <TableCellTextNew minWidth="150" value={le?.ein} />
                    <TableCellTextNew
                      minWidth="380"
                      value={
                        le.street +
                        ', ' +
                        le.city +
                        ', ' +
                        le?.state +
                        ', ' +
                        le?.zip +
                        ', ' +
                        le?.country
                      }
                    />
                    <TableCellNew minWidth="120" justifycontent="center">
                      <DropDown
                        flexdirection="column"
                        padding="10"
                        control={({ handleOpen }) => (
                          <IconButton
                            icon={<DropdownIcon />}
                            onClick={(event) => {
                              event.stopPropagation();
                              handleOpen();
                            }}
                            background={'transparent'}
                            color="grey-100-violet-90"
                          />
                        )}
                      >
                        <DropDownButton size="204" icon={<EyeIcon />} onClick={() => {}}>
                          Open
                        </DropDownButton>
                        <DropDownButton size="204" icon={<PencilIcon />} onClick={() => {}}>
                          Edit General information
                        </DropDownButton>
                        <DropDownButton size="204" icon={<PencilIcon />} onClick={() => {}}>
                          Edit Business Units
                        </DropDownButton>
                        <Divider type="horizontal" fullHorizontalWidth className={spacing.mY5} />
                        <DropDownButton
                          size="204"
                          type="danger"
                          icon={<TrashIcon />}
                          onClick={() => {}}
                        >
                          Delete
                        </DropDownButton>
                      </DropDown>
                    </TableCellNew>
                  </TableRowNew>
                ))}
              </TableBodyNew>
            </TableNew>
          </Container>
        </Container>
      </SettingsContainer>
    </Container>
  </ModalSettings>
);
