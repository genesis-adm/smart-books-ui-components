import { Checkbox } from 'Components/base/Checkbox';
import { Radio } from 'Components/base/Radio';
import { Button } from 'Components/base/buttons/Button';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { SelectNew } from 'Components/base/inputs/SelectNew';
import { OptionSingleNew } from 'Components/base/inputs/SelectNew/options/OptionSingleNew';
import { Text } from 'Components/base/typography/Text';
import { SettingsModalBD } from 'Components/custom/Settings/SettingsModalBD';
import { SettingsModalBDWide } from 'Components/custom/Settings/SettingsModalBDWide';
import { BusinessDivisionSelect } from 'Components/elements/BusinessDivisionSelect';
import { LegalEntityItem } from 'Components/elements/LegalEntityItem';
import { LegalEntitySelect } from 'Components/elements/LegalEntitySelect';
import { LogoDouble } from 'Components/elements/LogoDouble';
import { UserRoleItem } from 'Components/elements/UserRoleItem';
import { UserSelect } from 'Components/elements/UserSelect';
import { ImageUpload } from 'Components/old/ImageUpload';
import { Input } from 'Components/old/Input';
import { options } from 'Mocks/fakeOptions';
import { ReactComponent as WarningIcon } from 'Svg/24/warning-dark.svg';
import { ReactComponent as ArrowLeftIcon } from 'Svg/v2/16/arrow-left.svg';
import { ReactComponent as ArrowRightIcon } from 'Svg/v2/16/arrow-right.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/General Settings/Modals',
};

export const CreateBusinessDivisionCompanyStep1: React.FC = () => (
  <SettingsModalBD
    title="Create business division"
    onClose={() => {}}
    footer={
      <>
        <Button
          width="md"
          background="white-100"
          color="black-100"
          border="grey-20-grey-10"
          onClick={() => {}}
        >
          Cancel
        </Button>
        <Button navigation iconRight={<ArrowRightIcon />} width="md" onClick={() => {}}>
          Next
        </Button>
      </>
    }
  >
    <Container className={spacing.pY30} flexdirection="column">
      <Text type="caption-semibold" align="center" color="violet-90">
        Step 1
        <Text type="caption-semibold" color="grey-100">
          &nbsp;/ 2
        </Text>
      </Text>
      <Text type="body-medium" align="center">
        Please setup general info
      </Text>
      <Text type="caption-regular" className={spacing.mt20}>
        Choose division type
        <Text type="caption-regular" color="red-90">
          *
        </Text>
      </Text>
      <Container className={spacing.mt20}>
        <Radio id="company" onChange={() => {}} name="divisionType" checked={false}>
          Company
        </Radio>
        <Radio
          className={spacing.ml30}
          id="project"
          onChange={() => {}}
          name="divisionType"
          checked={false}
        >
          Project
        </Radio>
        <Radio
          className={spacing.ml30}
          id="subproject"
          onChange={() => {}}
          name="divisionType"
          checked
        >
          Subproject
        </Radio>
      </Container>
      <Input
        className={spacing.mt20}
        width="full"
        name="Company"
        type="text"
        label="Company name"
        isRequired
        placeholder="Enter unique company name"
      />
      <SelectNew
        name="parentcompany"
        className={spacing.mt20}
        onChange={() => {}}
        placeholder="Choose parent company"
        width="full"
        label="Parent company"
        required
      >
        {({ onClick, state }) => (
          <>
            {options.map(({ label, value }) => (
              <OptionSingleNew
                key={value}
                label={label}
                selected={state?.value === value}
                onClick={() => onClick({ label, value })}
              />
            ))}
          </>
        )}
      </SelectNew>
      <SelectNew
        name="parentproject"
        className={spacing.mt20}
        onChange={() => {}}
        placeholder="Choose parent project"
        width="full"
        label="Parent project"
        required
      >
        {({ onClick, state }) => (
          <>
            {options.map(({ label, value }) => (
              <OptionSingleNew
                key={value}
                label={label}
                selected={state?.value === value}
                onClick={() => onClick({ label, value })}
              />
            ))}
          </>
        )}
      </SelectNew>
      <ImageUpload className={classNames(spacing.mt20)} name="" image="" onChange={() => {}} />
    </Container>
  </SettingsModalBD>
);

export const CreateBusinessDivisionCompanyStep2: React.FC = () => (
  <SettingsModalBDWide
    title="Create business division"
    onClose={() => {}}
    footer={
      <>
        <Button
          navigation
          background="white-100"
          color="black-100"
          border="grey-20-grey-10"
          iconLeft={<ArrowLeftIcon />}
          width="md"
          onClick={() => {}}
        >
          Back
        </Button>
        <Button width="md" onClick={() => {}}>
          Create business division
        </Button>
      </>
    }
  >
    <Container className={spacing.pY30} flexdirection="column">
      <Text type="caption-semibold" align="center" color="violet-90">
        Step 2
        <Text type="caption-semibold" color="grey-100">
          &nbsp;/ 2
        </Text>
      </Text>
      <Container justifycontent="center" alignitems="center">
        <Text type="body-medium" align="center">
          Please choose legal entity or&nbsp;
        </Text>
        <LinkButton type="body-medium" onClick={() => {}}>
          Create legal entity
        </LinkButton>
      </Container>
      <Container className={spacing.mt20} flexdirection="column">
        <LegalEntitySelect name="Genesis" address="Address" checked={false} onChange={() => {}} />
        <LegalEntitySelect name="Genesis" address="Address" checked onChange={() => {}} />
        <LegalEntitySelect name="Genesis" address="Address" checked={false} onChange={() => {}} />
        <LegalEntitySelect name="Genesis" address="Address" checked={false} onChange={() => {}} />
        <LegalEntitySelect name="Genesis" address="Address" checked={false} onChange={() => {}} />
        <LegalEntitySelect name="Genesis" address="Address" checked={false} onChange={() => {}} />
        <LegalEntitySelect name="Genesis" address="Address" checked={false} onChange={() => {}} />
        <LegalEntitySelect name="Genesis" address="Address" checked={false} onChange={() => {}} />
        <LegalEntitySelect name="Genesis" address="Address" checked={false} onChange={() => {}} />
        <LegalEntitySelect name="Genesis" address="Address" checked={false} onChange={() => {}} />
      </Container>
    </Container>
  </SettingsModalBDWide>
);

export const CreateBusinessDivisionCompanyAddLegalEntity: React.FC = () => (
  <SettingsModalBD
    title="Create business division"
    onClose={() => {}}
    footer={
      <>
        <Button
          navigation
          background="white-100"
          color="black-100"
          border="grey-20-grey-10"
          iconLeft={<ArrowLeftIcon />}
          width="md"
          onClick={() => {}}
        >
          Back
        </Button>
        <Button width="md" onClick={() => {}}>
          Save
        </Button>
      </>
    }
  >
    <Container className={spacing.pY30} flexdirection="column">
      <Text type="body-medium" align="center">
        Please add legal entity
      </Text>
      <SelectNew
        name="organization"
        className={spacing.mt20}
        onChange={() => {}}
        placeholder="Choose"
        width="full"
        label="Choose organization or business division"
      >
        {({ onClick, state }) => (
          <>
            {options.map(({ label, value }) => (
              <OptionSingleNew
                key={value}
                label={label}
                selected={state?.value === value}
                onClick={() => onClick({ label, value })}
              />
            ))}
          </>
        )}
      </SelectNew>
      <Container className={spacing.mt16} justifycontent="space-between">
        <Input
          name="name"
          width="modal-sm"
          placeholder="Enter name"
          label="Legal entity name"
          isRequired
        />
        <Input name="ein" width="modal-sm" placeholder="Enter EIN" label="EIN" isRequired />
      </Container>
      <Input
        className={spacing.mt20}
        width="full"
        name="street"
        type="text"
        label="Street"
        placeholder="Enter street name"
        isRequired
      />
      <Container className={spacing.mt16} justifycontent="space-between">
        <SelectNew
          name="country"
          onChange={() => {}}
          placeholder="Choose option"
          width="230"
          label="Country"
          required
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() => onClick({ label, value })}
                />
              ))}
            </>
          )}
        </SelectNew>
        <SelectNew
          name="state"
          onChange={() => {}}
          placeholder="Choose option"
          width="230"
          label="State"
          required
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() => onClick({ label, value })}
                />
              ))}
            </>
          )}
        </SelectNew>
      </Container>
      <Container className={spacing.mt16} justifycontent="space-between">
        <Input name="city" width="modal-sm" placeholder="Enter city" label="City" isRequired />
        <Input
          name="zip"
          width="modal-sm"
          placeholder="Enter ZIP code"
          label="ZIP code"
          isRequired
        />
      </Container>
    </Container>
  </SettingsModalBD>
);

export const CreateBusinessDivisionCompanyLegalEntitiesList: React.FC = () => (
  <SettingsModalBDWide title="Genesis Media & Mobile Application" onClose={() => {}}>
    <Container className={spacing.pY30} flexdirection="column">
      <Container justifycontent="space-between" alignitems="center">
        <Container flexgrow="1">
          <LogoDouble
            className={spacing.mr20}
            organizationName="Genesis"
            businessDivisionName="Genesis"
            organizationImg="https://picsum.photos/60"
            businessDivisionImg="https://picsum.photos/70"
          />
          <Container
            className={spacing.w100p}
            flexdirection="column"
            justifycontent="space-between"
          >
            <Container>
              <Text className={classNames(spacing.w120, spacing.w100p)} color="grey-100">
                Company name
              </Text>
              <Text>Genesis Media & Mobile Application</Text>
            </Container>
            <Container>
              <Text className={classNames(spacing.w120, spacing.w100p)} color="grey-100">
                Date of creation
              </Text>
              <Text>10 May 2019</Text>
            </Container>
            <Container>
              <Text className={classNames(spacing.w120, spacing.w100p)} color="grey-100">
                Creator
              </Text>
              <Text>karine.mnatsakanian@mail.com</Text>
            </Container>
          </Container>
        </Container>
        <Button background="grey-20" width="xs" height="small" color="grey-100-black-100">
          Edit
        </Button>
      </Container>
      <Container className={spacing.mt30} justifycontent="space-between" alignitems="center">
        <Text type="text-medium" color="grey-100">
          Number of legal entities:&nbsp;
          <Text type="caption-semibold" display="inline-flex">
            24
          </Text>
        </Text>
        <LinkButton icon={<PlusIcon />} onClick={() => {}}>
          Add legal entity
        </LinkButton>
      </Container>
      <Container
        className={classNames(spacing.mt10, spacing.h420)}
        flexdirection="column"
        overflow="auto"
      >
        <LegalEntityItem name="Genesis" address="Address" onClick={() => {}} />
        <LegalEntityItem name="Genesis" address="Address" onClick={() => {}} />
        <LegalEntityItem name="Genesis" address="Address" onClick={() => {}} />
        <LegalEntityItem name="Genesis" address="Address" onClick={() => {}} />
        <LegalEntityItem name="Genesis" address="Address" onClick={() => {}} />
        <LegalEntityItem name="Genesis" address="Address" onClick={() => {}} />
        <LegalEntityItem name="Genesis" address="Address" onClick={() => {}} />
        <LegalEntityItem name="Genesis" address="Address" onClick={() => {}} />
        <LegalEntityItem name="Genesis" address="Address" onClick={() => {}} />
        <LegalEntityItem name="Genesis" address="Address" onClick={() => {}} />
        <LegalEntityItem name="Genesis" address="Address" onClick={() => {}} />
        <LegalEntityItem name="Genesis" address="Address" onClick={() => {}} />
      </Container>
    </Container>
  </SettingsModalBDWide>
);

export const CreateLegalEntityStep1: React.FC = () => (
  <SettingsModalBD
    title="Create legal entity"
    onClose={() => {}}
    footer={
      <>
        <Button
          width="md"
          background="white-100"
          color="black-100"
          border="grey-20-grey-10"
          onClick={() => {}}
        >
          Cancel
        </Button>
        <Button navigation iconRight={<ArrowRightIcon />} width="md" onClick={() => {}}>
          Next
        </Button>
      </>
    }
  >
    <Container className={spacing.pY30} flexdirection="column">
      <Text type="caption-semibold" align="center" color="violet-90">
        Step 1
        <Text type="caption-semibold" color="grey-100">
          &nbsp;/ 2
        </Text>
      </Text>
      <Text type="body-medium" align="center">
        Please setup general info
      </Text>
      <Container className={spacing.mt16} justifycontent="space-between">
        <Input
          name="name"
          width="modal-sm"
          placeholder="Enter name"
          label="Legal entity name"
          isRequired
        />
        <Input name="ein" width="modal-sm" placeholder="Enter EIN" label="EIN" isRequired />
      </Container>
      <Input
        className={spacing.mt20}
        width="full"
        name="street"
        type="text"
        label="Street"
        placeholder="Enter street name"
        isRequired
      />
      <Container className={spacing.mt16} justifycontent="space-between">
        <SelectNew
          name="country"
          onChange={() => {}}
          placeholder="Choose option"
          width="230"
          label="Country"
          required
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() => onClick({ label, value })}
                />
              ))}
            </>
          )}
        </SelectNew>
        <SelectNew
          name="state"
          onChange={() => {}}
          placeholder="Choose option"
          width="230"
          label="State"
          required
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() => onClick({ label, value })}
                />
              ))}
            </>
          )}
        </SelectNew>
      </Container>
      <Container className={spacing.mt16} justifycontent="space-between">
        <Input name="city" width="modal-sm" placeholder="Enter city" label="City" isRequired />
        <Input
          name="zip"
          width="modal-sm"
          placeholder="Enter ZIP code"
          label="ZIP code"
          isRequired
        />
      </Container>
      <Checkbox className={spacing.mt20} name="customeraddress" checked={false} onChange={() => {}}>
        Same a customer-facing address
      </Checkbox>
      <Input
        className={spacing.mt20}
        width="full"
        name="street2"
        type="text"
        label="Street"
        placeholder="Enter street name"
        isRequired
      />
      <Container className={spacing.mt16} justifycontent="space-between">
        <SelectNew
          name="country"
          onChange={() => {}}
          placeholder="Choose option"
          width="230"
          label="Country"
          required
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() => onClick({ label, value })}
                />
              ))}
            </>
          )}
        </SelectNew>
        <SelectNew
          name="state"
          onChange={() => {}}
          placeholder="Choose option"
          width="230"
          label="State"
          required
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() => onClick({ label, value })}
                />
              ))}
            </>
          )}
        </SelectNew>
      </Container>
      <Container className={spacing.mt16} justifycontent="space-between">
        <Input name="city2" width="modal-sm" placeholder="Enter city" label="City" isRequired />
        <Input
          name="zip2"
          width="modal-sm"
          placeholder="Enter ZIP code"
          label="ZIP code"
          isRequired
        />
      </Container>
    </Container>
  </SettingsModalBD>
);

export const CreateLegalEntityStep2: React.FC = () => (
  <SettingsModalBDWide
    title="Create legal entity"
    onClose={() => {}}
    footer={
      <>
        <Button
          navigation
          background="white-100"
          color="black-100"
          border="grey-20-grey-10"
          iconLeft={<ArrowLeftIcon />}
          width="md"
          onClick={() => {}}
        >
          Back
        </Button>
        <Button width="md" onClick={() => {}}>
          Create legal entity
        </Button>
      </>
    }
  >
    <Container className={spacing.pY30} flexdirection="column">
      <Text type="caption-semibold" align="center" color="violet-90">
        Step 2
        <Text type="caption-semibold" color="grey-100">
          &nbsp;/ 2
        </Text>
      </Text>
      <Container justifycontent="center" alignitems="center">
        <Text type="body-medium" align="center">
          Please choose business division
        </Text>
      </Container>
      <Container className={spacing.mt20} flexdirection="column">
        <BusinessDivisionSelect
          division="Genesis Media & Mobile Application"
          company="Company in GENESIS"
          checked={false}
          onChange={() => {}}
        />
        <BusinessDivisionSelect
          division="Genesis Media & Mobile Application"
          company="Company in GENESIS"
          checked
          onChange={() => {}}
        />
        <BusinessDivisionSelect
          division="Genesis Media & Mobile Application"
          company="Company in GENESIS"
          checked={false}
          onChange={() => {}}
        />
        <BusinessDivisionSelect
          division="Genesis Media & Mobile Application"
          company="Company in GENESIS"
          checked={false}
          onChange={() => {}}
        />
        <BusinessDivisionSelect
          division="Genesis Media & Mobile Application"
          company="Company in GENESIS"
          checked={false}
          onChange={() => {}}
        />
        <BusinessDivisionSelect
          division="Genesis Media & Mobile Application"
          company="Company in GENESIS"
          checked={false}
          onChange={() => {}}
        />
        <BusinessDivisionSelect
          division="Genesis Media & Mobile Application"
          company="Company in GENESIS"
          checked={false}
          onChange={() => {}}
        />
        <BusinessDivisionSelect
          division="Genesis Media & Mobile Application"
          company="Company in GENESIS"
          checked={false}
          onChange={() => {}}
        />
        <BusinessDivisionSelect
          division="Genesis Media & Mobile Application"
          company="Company in GENESIS"
          checked={false}
          onChange={() => {}}
        />
        <BusinessDivisionSelect
          division="Genesis Media & Mobile Application"
          company="Company in GENESIS"
          checked={false}
          onChange={() => {}}
        />
      </Container>
    </Container>
  </SettingsModalBDWide>
);

export const LegalEntityInfo: React.FC = () => (
  <SettingsModalBDWide
    title="LCC Matard"
    onClose={() => {}}
    footer={
      <>
        <Button
          background="white-100"
          color="black-100"
          border="grey-20-grey-10"
          width="md"
          onClick={() => {}}
        >
          Cancel
        </Button>
        <Button
          width="md"
          background="grey-20-violet-90"
          color="black-100-white-100"
          onClick={() => {}}
        >
          Edit
        </Button>
      </>
    }
  >
    <Container className={spacing.pY30} flexdirection="column">
      <Container
        background="grey-10"
        className={spacing.pY16}
        flexdirection="column"
        alignitems="center"
      >
        <Container className={classNames(spacing.w450, spacing.w100p)}>
          <Text
            type="body-regular-14"
            color="grey-100"
            className={classNames(spacing.w150, spacing.w100p)}
          >
            Belongs to
          </Text>
          <Container flexdirection="column" className={classNames(spacing.w300, spacing.w100p)}>
            <Text type="body-regular-14" noWrap>
              Business division name
            </Text>
            <Text type="body-regular-14" noWrap>
              Business division name
            </Text>
            <Text type="body-regular-14" noWrap>
              Business division name
            </Text>
            <Text type="body-regular-14" noWrap>
              Business division name
            </Text>
          </Container>
        </Container>
        <Container className={classNames(spacing.w450, spacing.w100p)}>
          <Text
            type="body-regular-14"
            color="grey-100"
            className={classNames(spacing.w150, spacing.w100p)}
          >
            Creator
          </Text>
          <Text type="body-regular-14" noWrap>
            karine.mnatsakanian@mail.com
          </Text>
        </Container>
      </Container>
      <Container
        background="grey-10"
        className={classNames(spacing.pY16, spacing.mt10)}
        flexdirection="column"
        alignitems="center"
      >
        <Container className={classNames(spacing.w450, spacing.w100p)}>
          <Text
            type="body-regular-14"
            color="grey-100"
            className={classNames(spacing.w150, spacing.w100p)}
          >
            Legal entity name
          </Text>
          <Text type="body-regular-14" noWrap>
            LCC Matard
          </Text>
        </Container>
        <Container className={classNames(spacing.w450, spacing.w100p)}>
          <Text
            type="body-regular-14"
            color="grey-100"
            className={classNames(spacing.w150, spacing.w100p)}
          >
            EIN
          </Text>
          <Text type="body-regular-14" noWrap>
            12-03857
          </Text>
        </Container>
        <Container className={classNames(spacing.w450, spacing.w100p)}>
          <Text
            type="body-regular-14"
            color="grey-100"
            className={classNames(spacing.w150, spacing.w100p)}
          >
            Street
          </Text>
          <Text type="body-regular-14" noWrap>
            11 Giannou Kranidioti Street, Limassol
          </Text>
        </Container>
        <Container className={classNames(spacing.w450, spacing.w100p)}>
          <Text
            type="body-regular-14"
            color="grey-100"
            className={classNames(spacing.w150, spacing.w100p)}
          >
            Country
          </Text>
          <Text type="body-regular-14" noWrap>
            Cyprus
          </Text>
        </Container>
        <Container className={classNames(spacing.w450, spacing.w100p)}>
          <Text
            type="body-regular-14"
            color="grey-100"
            className={classNames(spacing.w150, spacing.w100p)}
          >
            State
          </Text>
          <Text type="body-regular-14" noWrap>
            Limassol
          </Text>
        </Container>
        <Container className={classNames(spacing.w450, spacing.w100p)}>
          <Text
            type="body-regular-14"
            color="grey-100"
            className={classNames(spacing.w150, spacing.w100p)}
          >
            City
          </Text>
          <Text type="body-regular-14" noWrap>
            Schizas Complex
          </Text>
        </Container>
        <Container className={classNames(spacing.w450, spacing.w100p)}>
          <Text
            type="body-regular-14"
            color="grey-100"
            className={classNames(spacing.w150, spacing.w100p)}
          >
            ZIP code
          </Text>
          <Text type="body-regular-14" noWrap>
            30457
          </Text>
        </Container>
      </Container>
      <Container
        background="grey-10"
        className={classNames(spacing.pY16, spacing.mt10)}
        justifycontent="center"
      >
        <Text
          type="body-regular-14"
          className={classNames(spacing.w450, spacing.w100p)}
          color="grey-100"
        >
          Customer-facing address as same
        </Text>
      </Container>
      <Container justifycontent="center" alignitems="center" className={spacing.mt30}>
        <WarningIcon className={spacing.mr10} />
        <Text type="caption-semibold">You do not have permission to edit this information</Text>
      </Container>
    </Container>
  </SettingsModalBDWide>
);

export const LegalEntityEdit: React.FC = () => (
  <SettingsModalBD
    title="Edit legal entity"
    onClose={() => {}}
    footer={
      <>
        <Button
          background="white-100"
          color="black-100"
          border="grey-20-grey-10"
          width="md"
          onClick={() => {}}
        >
          Cancel
        </Button>
        <Button width="md" onClick={() => {}}>
          Create
        </Button>
      </>
    }
  >
    <Container className={spacing.pY30} flexdirection="column">
      <SelectNew
        name="organization"
        onChange={() => {}}
        placeholder="Choose"
        width="full"
        label="Choose organization or business division"
      >
        {({ onClick, state }) => (
          <>
            {options.map(({ label, value }) => (
              <OptionSingleNew
                key={value}
                label={label}
                selected={state?.value === value}
                onClick={() => onClick({ label, value })}
              />
            ))}
          </>
        )}
      </SelectNew>
      <Container className={spacing.mt16} justifycontent="space-between">
        <Input
          name="name"
          width="modal-sm"
          placeholder="Enter name"
          label="Legal entity name"
          isRequired
        />
        <Input name="ein" width="modal-sm" placeholder="Enter EIN" label="EIN" isRequired />
      </Container>
      <Input
        className={spacing.mt20}
        width="full"
        name="street"
        type="text"
        label="Street"
        placeholder="Enter street name"
        isRequired
      />
      <Container className={spacing.mt16} justifycontent="space-between">
        <SelectNew
          name="country"
          onChange={() => {}}
          placeholder="Choose option"
          width="230"
          label="Country"
          required
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() => onClick({ label, value })}
                />
              ))}
            </>
          )}
        </SelectNew>
        <SelectNew
          name="state"
          onChange={() => {}}
          placeholder="Choose option"
          width="230"
          label="State"
          required
        >
          {({ onClick, state }) => (
            <>
              {options.map(({ label, value }) => (
                <OptionSingleNew
                  key={value}
                  label={label}
                  selected={state?.value === value}
                  onClick={() => onClick({ label, value })}
                />
              ))}
            </>
          )}
        </SelectNew>
      </Container>
      <Container className={spacing.mt16} justifycontent="space-between">
        <Input name="city" width="modal-sm" placeholder="Enter city" label="City" isRequired />
        <Input
          name="zip"
          width="modal-sm"
          placeholder="Enter ZIP code"
          label="ZIP code"
          isRequired
        />
      </Container>
      <Checkbox className={spacing.mt20} name="customeraddress" checked onChange={() => {}}>
        Same a customer-facing address
      </Checkbox>
    </Container>
  </SettingsModalBD>
);

export const CreateNewUserStep1: React.FC = () => (
  <SettingsModalBD
    title="Create new user"
    onClose={() => {}}
    footer={
      <>
        <Button
          width="md"
          background="white-100"
          color="black-100"
          border="grey-20-grey-10"
          onClick={() => {}}
        >
          Cancel
        </Button>
        <Button navigation iconRight={<ArrowRightIcon />} width="md" onClick={() => {}}>
          Next
        </Button>
      </>
    }
  >
    <Container className={spacing.pY30} flexdirection="column">
      <Text type="caption-semibold" align="center" color="violet-90">
        Step 1
        <Text type="caption-semibold" color="grey-100">
          &nbsp;/ 3
        </Text>
      </Text>
      <Text type="body-medium" align="center">
        Please provide general information
      </Text>
      <Input
        className={spacing.mt20}
        width="full"
        name="firstname"
        type="text"
        label="First name"
        isRequired
        placeholder="Enter unique first name"
      />
      <Input
        className={spacing.mt20}
        width="full"
        name="lastname"
        type="text"
        label="Last name"
        isRequired
        placeholder="Enter unique last name"
      />
      <Input
        className={spacing.mt20}
        width="full"
        name="email"
        type="email"
        label="Email"
        isRequired
        description="Invitation will be send in this email address"
        placeholder="Enter valid email"
      />
      <Input
        className={spacing.mt20}
        width="full"
        name="organization"
        type="text"
        label="Organization"
        placeholder="Organization"
        disabled
        value="Organization name"
      />
    </Container>
  </SettingsModalBD>
);

export const CreateNewUserStep2: React.FC = () => (
  <SettingsModalBDWide
    title="Create new user"
    onClose={() => {}}
    footer={
      <>
        <Button
          navigation
          background="white-100"
          color="black-100"
          border="grey-20-grey-10"
          iconLeft={<ArrowLeftIcon />}
          width="md"
          onClick={() => {}}
        >
          Back
        </Button>
        <Button navigation iconRight={<ArrowRightIcon />} width="md" onClick={() => {}}>
          Next
        </Button>
      </>
    }
  >
    <Container className={spacing.pY30} flexdirection="column">
      <Text type="caption-semibold" align="center" color="violet-90">
        Step 2
        <Text type="caption-semibold" color="grey-100">
          &nbsp;/ 3
        </Text>
      </Text>
      <Text type="body-medium" align="center">
        Please choose user role(s)
      </Text>
      <Text type="text-medium" color="grey-100" className={spacing.mt30}>
        Selected:&nbsp;
        <Text type="text-medium" display="inline-flex">
          0
        </Text>
      </Text>
      <Container
        className={classNames(spacing.mt30, spacing.h350)}
        flexdirection="column"
        overflow="auto"
      >
        <UserSelect
          name="Genesis"
          organization="Address"
          checked={false}
          onChange={() => {}}
          onLinkClick={() => {}}
        />
        <UserSelect
          name="Genesis"
          organization="Address"
          checked
          onChange={() => {}}
          onLinkClick={() => {}}
        />
        <UserSelect
          name="Genesis"
          organization="Address"
          checked={false}
          onChange={() => {}}
          onLinkClick={() => {}}
        />
        <UserSelect
          name="Genesis"
          organization="Address"
          checked={false}
          onChange={() => {}}
          onLinkClick={() => {}}
        />
        <UserSelect
          name="Genesis"
          organization="Address"
          checked={false}
          onChange={() => {}}
          onLinkClick={() => {}}
        />
        <UserSelect
          name="Genesis"
          organization="Address"
          checked={false}
          onChange={() => {}}
          onLinkClick={() => {}}
        />
        <UserSelect
          name="Genesis"
          organization="Address"
          checked={false}
          onChange={() => {}}
          onLinkClick={() => {}}
        />
      </Container>
    </Container>
  </SettingsModalBDWide>
);

export const CreateNewUserStep3: React.FC = () => (
  <SettingsModalBDWide
    title="Create new user"
    onClose={() => {}}
    footer={
      <>
        <Button
          navigation
          background="white-100"
          color="black-100"
          border="grey-20-grey-10"
          iconLeft={<ArrowLeftIcon />}
          width="md"
          onClick={() => {}}
        >
          Back
        </Button>
        <Button width="md" onClick={() => {}}>
          Send invite
        </Button>
      </>
    }
  >
    <Container className={spacing.pY30} flexdirection="column">
      <Text type="caption-semibold" align="center" color="violet-90">
        Step 3
        <Text type="caption-semibold" color="grey-100">
          &nbsp;/ 3
        </Text>
      </Text>
      <Text type="body-medium" align="center">
        Send invite
      </Text>
      <Container className={spacing.mt30} flexdirection="column">
        <Container>
          <Text color="grey-100" className={classNames(spacing.w120, spacing.w100p)}>
            First name
          </Text>
          <Text noWrap>Karine</Text>
        </Container>
        <Container className={spacing.mt20}>
          <Text color="grey-100" className={classNames(spacing.w120, spacing.w100p)}>
            Last name
          </Text>
          <Text noWrap>Mnatsakanian</Text>
        </Container>
        <Container className={spacing.mt20}>
          <Text color="grey-100" className={classNames(spacing.w120, spacing.w100p)}>
            Email
          </Text>
          <Text noWrap>karine.mnatsakanian@mail.com</Text>
        </Container>
        <Container className={spacing.mt20}>
          <Text color="grey-100" className={classNames(spacing.w120, spacing.w100p)}>
            Organization
          </Text>
          <Text noWrap>GENESIS</Text>
        </Container>
        <Text color="grey-100" className={spacing.mt20}>
          List of user roles and account access rights:
        </Text>
      </Container>
      <Container
        className={classNames(spacing.mt5, spacing.h210)}
        flexdirection="column"
        overflow="auto"
      >
        <UserRoleItem
          organization="Organization"
          userrole="Role"
          onLinkClick={() => {}}
          onDelete={() => {}}
        />
        <UserRoleItem
          organization="Organization"
          userrole="Role"
          onLinkClick={() => {}}
          onDelete={() => {}}
        />
        <UserRoleItem
          organization="Organization"
          userrole="Role"
          onLinkClick={() => {}}
          onDelete={() => {}}
        />
        <UserRoleItem
          organization="Organization"
          userrole="Role"
          onLinkClick={() => {}}
          onDelete={() => {}}
        />
        <UserRoleItem
          organization="Organization"
          userrole="Role"
          onLinkClick={() => {}}
          onDelete={() => {}}
        />
        <UserRoleItem
          organization="Organization"
          userrole="Role"
          onLinkClick={() => {}}
          onDelete={() => {}}
        />
        <UserRoleItem
          organization="Organization"
          userrole="Role"
          onLinkClick={() => {}}
          onDelete={() => {}}
        />
        <UserRoleItem
          organization="Organization"
          userrole="Role"
          onLinkClick={() => {}}
          onDelete={() => {}}
        />
      </Container>
    </Container>
  </SettingsModalBDWide>
);
