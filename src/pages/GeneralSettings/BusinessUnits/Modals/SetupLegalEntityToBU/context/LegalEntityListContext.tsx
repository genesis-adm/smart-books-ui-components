import { LegalEntity } from 'Pages/GeneralSettings/mocks/setupLegalEntity';
import { Dispatch, SetStateAction, createContext } from 'react';

export const LegalEntitiesListContext = createContext<{
  leList: LegalEntity[];
  setLeList: Dispatch<SetStateAction<LegalEntity[]>>;
}>({ leList: [], setLeList: () => {} });
