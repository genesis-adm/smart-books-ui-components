import { Tag } from 'Components/base/Tag';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { MainPageEmpty } from 'Components/custom/MainPage/MainPageEmpty';
import { LegalEntitiesListContext } from 'Pages/GeneralSettings/BusinessUnits/Modals/SetupLegalEntityToBU/context/LegalEntityListContext';
import ListPlaceholder from 'Pages/GeneralSettings/components/setupModalsplaceholders/ListPlaceholder';
import {
  LegalEntity,
  disableUnlinkTooltipMessage,
} from 'Pages/GeneralSettings/mocks/setupLegalEntity';
import { ReactComponent as InfoIcon } from 'Svg/v2/16/info.svg';
import classNames from 'classnames';
import React, { useContext } from 'react';

import spacing from '../../../../../../assets/styles/spacing.module.scss';

type LegalEntitiesListProps = {
  isLoading: boolean;
};
const LegalEntitiesList = ({ isLoading }: LegalEntitiesListProps) => {
  const { leList, setLeList } = useContext(LegalEntitiesListContext);

  const handleDelete = (item: LegalEntity) => {
    setLeList(leList.filter((el) => el.id !== item.id));
  };

  if (isLoading) return <ListPlaceholder />;

  return (
    <Container
      background={'white-100'}
      flexdirection="column"
      borderRadius="20"
      className={classNames(spacing.p24, spacing.h100p, spacing.w404fixed)}
      gap="24"
    >
      <Text type="body-regular-14" color="grey-100" align="center">
        Legal entities list
      </Text>
      {!leList.length ? (
        <MainPageEmpty description="Please choose Legal entities" />
      ) : (
        <Container flexwrap="wrap" gap="16">
          {leList.map((item) => (
            <Tag
              key={item.id}
              text={item.name}
              deleteFlow={!item.disableUnlink}
              truncateText
              icon={item.disableUnlink && <InfoIcon />}
              iconPosition="right"
              iconTooltip={item.disableUnlink && disableUnlinkTooltipMessage}
              onDelete={() => handleDelete(item)}
            />
          ))}
        </Container>
      )}
    </Container>
  );
};

export default LegalEntitiesList;
