import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { FilterDropDown } from 'Components/elements/filters/FilterDropDown';
import { FilterDropDownItem } from 'Components/elements/filters/FilterDropDownItem';
import { FilterItemsContainer } from 'Components/elements/filters/FilterItemsContainer';
import { FilterSearch } from 'Components/elements/filters/FilterSearch';
import { FiltersWrapper } from 'Components/elements/filters/FiltersWrapper';
import { Search } from 'Components/inputs/Search';
import { TableCellWidth } from 'Components/types/gridTypes';
import { options } from 'Mocks/fakeOptions';
import SetupLegalEntityTableRow from 'Pages/GeneralSettings/BusinessUnits/Modals/SetupLegalEntityToBU/components/SetupLegalEntityTableRow';
import ChooseBUorLEPlaceholder from 'Pages/GeneralSettings/components/setupModalsplaceholders/ChooseBUorLEPlaceholder';
import {
  legalEntitiesList,
  setupLegalEntitiesTableHead,
} from 'Pages/GeneralSettings/mocks/setupLegalEntity';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../../../../assets/styles/spacing.module.scss';

type ChooseLegalEntityProps = {
  isLoading: boolean;
};
const ChooseLegalEntity = ({ isLoading }: ChooseLegalEntityProps) => {
  const [searchValue, setSearchValue] = useState<string>();
  const [businessTypeSearchValue, setBusinessTypeSearchValue] = useState<string>();

  const handleChange = (value: string) => {
    setBusinessTypeSearchValue(value);
  };

  if (isLoading) return <ChooseBUorLEPlaceholder />;

  return (
    <Container
      background={'white-100'}
      flexdirection="column"
      borderRadius="20"
      flexgrow="4"
      className={classNames(spacing.p24, spacing.h100p)}
      gap="24"
    >
      <Text type="body-regular-14" color="grey-100" align="center">
        Please choose Legal entities
      </Text>
      <Container flexdirection="column">
        <Container justifycontent="space-between">
          <Search
            name="search-le"
            placeholder="Search Legal entities"
            width="440"
            value={searchValue}
            onChange={(e) => setSearchValue(e.target.value)}
          />
          <Button
            background={'violet-90-violet-100'}
            color="white-100"
            width="220"
            height="40"
            iconLeft={<PlusIcon />}
          >
            Create Legal entities
          </Button>
        </Container>
        <FiltersWrapper active margins="none">
          <FilterDropDown title="Business type" value="All" onClear={() => {}} selectedAmount={0}>
            <Container
              className={classNames(spacing.m16, spacing.w288fixed)}
              flexdirection="column"
            >
              <FilterSearch
                title="Business type:"
                value={businessTypeSearchValue || ''}
                onChange={handleChange}
              />
              <FilterItemsContainer className={spacing.mt8}>
                {options.map((item) => (
                  <FilterDropDownItem
                    key={item.value}
                    id={item.value}
                    type="item"
                    checked={false}
                    blurred={false}
                    value={item.label}
                    onChange={() => {}}
                  />
                ))}
              </FilterItemsContainer>
            </Container>
          </FilterDropDown>
        </FiltersWrapper>
      </Container>
      <TableNew
        noResults={false}
        className={classNames(spacing.pr24)}
        tableHead={
          <TableHeadNew>
            {setupLegalEntitiesTableHead.map((item, index) => (
              <TableTitleNew
                key={index}
                minWidth={item.minWidth as TableCellWidth}
                align={item.align as TableTitleNewProps['align']}
                title={item.title}
                onClick={() => {}}
              />
            ))}
          </TableHeadNew>
        }
      >
        <TableBodyNew>
          {legalEntitiesList.map((le) => (
            <SetupLegalEntityTableRow legalEntity={le} />
          ))}
        </TableBodyNew>
      </TableNew>
    </Container>
  );
};

export default ChooseLegalEntity;
