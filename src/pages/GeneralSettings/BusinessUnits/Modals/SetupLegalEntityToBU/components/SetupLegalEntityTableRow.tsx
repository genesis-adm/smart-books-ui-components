import { DropDown } from 'Components/DropDown';
import { Tag } from 'Components/base/Tag';
import { TagSegmented } from 'Components/base/TagSegmented';
import { Tooltip } from 'Components/base/Tooltip';
import { Button } from 'Components/base/buttons/Button';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import Logo from 'Components/elements/Logo/Logo';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableCellWidth } from 'Components/types/gridTypes';
import { LegalEntitiesListContext } from 'Pages/GeneralSettings/BusinessUnits/Modals/SetupLegalEntityToBU/context/LegalEntityListContext';
import {
  LegalEntity,
  disableUnlinkTooltipMessage,
  setupLegalEntitiesTableHead,
} from 'Pages/GeneralSettings/mocks/setupLegalEntity';
import { ReactComponent as TrashIcon } from 'Svg/v2/12/delete.svg';
import { ReactComponent as InfoIcon } from 'Svg/v2/16/info.svg';
import classNames from 'classnames';
import React, { useContext } from 'react';

import spacing from '../../../../../../assets/styles/spacing.module.scss';

type SetupLegalEntityTableRowProps = {
  legalEntity: LegalEntity;
};
const SetupLegalEntityTableRow = ({ legalEntity }: SetupLegalEntityTableRowProps) => {
  const { leList, setLeList } = useContext(LegalEntitiesListContext);

  const handleAdd = () => {
    setLeList([...leList, legalEntity]);
  };
  const handleDelete = () => {
    setLeList(leList.filter((el) => el.id !== legalEntity.id));
  };

  return (
    <TableRowNew key={legalEntity.id} gap="8">
      <TableCellTextNew
        minWidth={setupLegalEntitiesTableHead[0].minWidth as TableCellWidth}
        fixedWidth
        value={legalEntity.name}
        iconLeft={<Logo content="user" name={legalEntity.name} size="24" radius="rounded" />}
      />
      <TableCellNew minWidth={setupLegalEntitiesTableHead[1].minWidth as TableCellWidth} fixedWidth>
        <TagSegmented
          mainLabel={legalEntity.businessUnits[0]}
          showCounter={legalEntity.businessUnits.length > 1}
          counter={
            <DropDown
              flexdirection="column"
              padding="8"
              classnameInner={classNames(spacing.h300)}
              customScroll
              control={({ handleOpen }) => (
                <Tooltip message={'Show All'}>
                  <TagSegmented.Counter
                    counterValue={legalEntity.businessUnits.length}
                    onClick={handleOpen}
                  />
                </Tooltip>
              )}
            >
              <Container flexdirection="column" gap="12">
                <Text type="button" color="grey-100">
                  Legal entities
                </Text>
                {legalEntity.businessUnits.map((le) => (
                  <Text key={le} type="body-regular-14">
                    {le}
                  </Text>
                ))}
              </Container>
            </DropDown>
          }
        />
      </TableCellNew>
      <TableCellTextNew
        minWidth={setupLegalEntitiesTableHead[2].minWidth as TableCellWidth}
        fixedWidth
        value={legalEntity.taxId}
      />
      <TableCellTextNew
        minWidth={setupLegalEntitiesTableHead[3].minWidth as TableCellWidth}
        fixedWidth
        lineClamp="2"
        value={legalEntity.address}
      />
      <TableCellNew
        minWidth={setupLegalEntitiesTableHead[4].minWidth as TableCellWidth}
        alignitems="center"
      >
        {leList.find((el) => el.id === legalEntity.id) ? (
          <Container gap="12">
            <Tag text="Added" size="24" cursor="default" font="caption-regular" />
            {legalEntity.disableUnlink ? (
              <IconButton
                icon={<InfoIcon />}
                size="24"
                color="grey-100-black-100"
                background={'grey-20'}
                tooltip={disableUnlinkTooltipMessage}
              />
            ) : (
              <IconButton
                icon={<TrashIcon />}
                tooltip="Delete"
                size="24"
                color="grey-90-red-90"
                background={'grey-20-red-10'}
                onClick={handleDelete}
              />
            )}
          </Container>
        ) : (
          <Button
            width="50"
            height="24"
            font="text-medium"
            background={'blue-10-blue-20'}
            color="violet-90-violet-100"
            onClick={handleAdd}
          >
            Add
          </Button>
        )}
      </TableCellNew>
    </TableRowNew>
  );
};

export default SetupLegalEntityTableRow;
