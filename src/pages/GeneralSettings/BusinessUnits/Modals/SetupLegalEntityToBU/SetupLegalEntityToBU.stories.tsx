import { Meta, Story } from '@storybook/react';
import { Tag } from 'Components/base/Tag';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { Pagination } from 'Components/custom/Stories/Pagination/Pagination';
import PerPageSelect from 'Components/custom/Stories/Pagination/PerPageSelect/PerPageSelect';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { rowsPerPageOptions } from 'Mocks/fakeOptions';
import ChooseLegalEntity from 'Pages/GeneralSettings/BusinessUnits/Modals/SetupLegalEntityToBU/components/ChooseLegalEntity';
import LegalEntitiesList from 'Pages/GeneralSettings/BusinessUnits/Modals/SetupLegalEntityToBU/components/LegalEntitiesList';
import { LegalEntitiesListContext } from 'Pages/GeneralSettings/BusinessUnits/Modals/SetupLegalEntityToBU/context/LegalEntityListContext';
import SetupFooterPlaceholder from 'Pages/GeneralSettings/components/setupModalsplaceholders/SetupFooterPlaceholder';
import SetupModalHeaderPlaceholder from 'Pages/GeneralSettings/components/setupModalsplaceholders/SetupModalHeaderPlaceholder';
import { LegalEntity } from 'Pages/GeneralSettings/mocks/setupLegalEntity';
import { ReactComponent as DocumentIcon } from 'Svg/v2/16/file-blank.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';

export const SetupLegalEntityToBU: Story = ({ storyState }) => {
  const [rowsPerPage, setRowsPerPage] = useState<string>(rowsPerPageOptions[0].value);
  const [leList, setLeList] = useState<Array<LegalEntity>>([]);

  const isLoading = storyState === 'loading';

  return (
    <Modal
      size="1392"
      centered
      background={'grey-10'}
      pluginScrollDisabled
      header={
        isLoading ? (
          <SetupModalHeaderPlaceholder />
        ) : (
          <ModalHeaderNew
            title="Setup Legal entities to SmartBooks"
            titlePosition="center"
            tag={
              <Tag
                icon={<DocumentIcon />}
                padding="4-8"
                cursor="default"
                text="New"
                color="green"
              />
            }
            closeActionBackgroundColor="white-100"
            border
            background={'grey-10'}
            onClose={() => {}}
          />
        )
      }
      footer={
        <ModalFooterNew border background={'grey-10'}>
          {isLoading ? (
            <SetupFooterPlaceholder />
          ) : (
            <>
              <Container alignitems="center" gap="16" justifycontent="flex-end">
                <Container flexdirection="column">
                  <Text type="body-regular-14" color="grey-100">
                    Step: 2 of 2
                  </Text>
                  <Text
                    type="body-regular-14"
                    color="grey-100"
                    lineClamp="none"
                    className={spacing.w140fixed}
                  >
                    Setup Legal entities
                  </Text>
                </Container>
                <Button
                  height="48"
                  navigation
                  width="134"
                  background={!!leList.length ? 'violet-90-violet-100' : 'grey-90'}
                  onClick={() => {}}
                >
                  Create
                </Button>
              </Container>
              <Pagination
                borderTop="none"
                customPagination={
                  <Container gap="16" alignitems="center">
                    <Text type="subtext-regular" color="grey-100">
                      Rows per Page
                    </Text>
                    <PerPageSelect
                      tagColor="white"
                      selectedValue={rowsPerPage}
                      onOptionClick={(value) => setRowsPerPage(value)}
                    />
                  </Container>
                }
                elementsBackground="white-100"
              />
            </>
          )}
        </ModalFooterNew>
      }
    >
      <LegalEntitiesListContext.Provider value={{ leList: leList, setLeList: setLeList }}>
        <Container gap="24" className={classNames(spacing.h100p, spacing.w100p, spacing.pr24)}>
          <ChooseLegalEntity isLoading={isLoading} />
          <LegalEntitiesList isLoading={isLoading} />
        </Container>
      </LegalEntitiesListContext.Provider>
    </Modal>
  );
};

export default {
  title: 'Pages/General Settings/Business Units/Modals/Setup Legal Entity to BU',
  component: SetupLegalEntityToBU,
  argTypes: {
    storyState: {
      options: ['empty', 'loading', 'loaded'],
      control: {
        type: 'radio',
        labels: {
          empty: 'Empty page',
          loading: 'Page is loading',
          loaded: 'Page loaded',
        },
      },
    },
  },
} as Meta;
