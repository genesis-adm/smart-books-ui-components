import { ContentLoader } from 'Components/base/ContentLoader';
import { Tag } from 'Components/base/Tag';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import ModalHeaderPlaceholder from 'Components/custom/placeholders/ModalHeaderPlaceholder';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import CreateNewCompanyForm from 'Pages/GeneralSettings/BusinessUnits/Modals/CreateBusinessUnit/components/CreateNewCompanyForm';
import CreateNewProjectForm from 'Pages/GeneralSettings/BusinessUnits/Modals/CreateBusinessUnit/components/CreateNewProjectForm';
import CreateNewSubProjectForm from 'Pages/GeneralSettings/BusinessUnits/Modals/CreateBusinessUnit/components/CreateNewSubprojectForm';
import { ReactComponent as ArrowRightIcon } from 'Svg/v2/16/arrow-right.svg';
import { ReactComponent as DocumentIcon } from 'Svg/v2/16/file-blank.svg';
import React, { ReactNode, useState } from 'react';

type ModalType = 'company' | 'project' | 'subproject';
type CreateBusinessUnitProps = {
  type: ModalType;
  isLoading: boolean;
  editMode: boolean;
};
const CreateBusinessUnit = ({ type, isLoading, editMode }: CreateBusinessUnitProps) => {
  const [isActiveNextButton, setIsActiveNextButton] = useState<boolean>(false);

  const modalData: Record<ModalType, { title: string; form: ReactNode }> = {
    company: {
      title: 'Company',
      form: (
        <CreateNewCompanyForm
          isLoading={isLoading}
          toggleActive={(active) => {
            setIsActiveNextButton(active);
          }}
        />
      ),
    },
    project: {
      title: 'Project',
      form: (
        <CreateNewProjectForm
          isLoading={isLoading}
          toggleActive={(active) => {
            setIsActiveNextButton(active);
          }}
        />
      ),
    },
    subproject: {
      title: 'Subproject',
      form: (
        <CreateNewSubProjectForm
          isLoading={isLoading}
          toggleActive={(active) => {
            setIsActiveNextButton(active);
          }}
        />
      ),
    },
  };

  const title = editMode ? `Edit ${modalData[type].title}` : `Create new ${modalData[type].title}`;

  return (
    <Modal
      size="720"
      type="new"
      background={'grey-10'}
      header={
        isLoading ? (
          <ModalHeaderPlaceholder />
        ) : (
          <ModalHeaderNew
            title={title}
            tag={
              <Tag
                icon={<DocumentIcon />}
                padding="4-8"
                cursor="default"
                text="New"
                color="green"
              />
            }
            closeActionBackgroundColor="white-100"
            border
            background={'grey-10'}
            onClose={() => {}}
          />
        )
      }
      footer={
        <ModalFooterNew border background={'grey-10'}>
          <Container alignitems="center" gap="16" flexgrow="1" justifycontent="flex-end">
            <ContentLoader isLoading={isLoading} height="48px" width="200px">
              {!editMode && (
                <Container flexdirection="column">
                  <Text type="body-regular-14" color="grey-100">
                    Step: 1 of 2
                  </Text>
                  <Text type="body-regular-14" color="grey-100">
                    Next: Setup Legal entities
                  </Text>
                </Container>
              )}
              {editMode ? (
                <Button height="48" width="134" background={'violet-90-violet-100'}>
                  Update
                </Button>
              ) : (
                <Button
                  height="48"
                  navigation
                  iconRight={<ArrowRightIcon />}
                  width="150"
                  background={isActiveNextButton ? 'violet-90-violet-100' : 'grey-90'}
                >
                  Next
                </Button>
              )}
            </ContentLoader>
          </Container>
        </ModalFooterNew>
      }
    >
      {modalData[type].form}
    </Modal>
  );
};

export default CreateBusinessUnit;
