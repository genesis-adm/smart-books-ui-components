import { SelectDropdown } from 'Components/SelectDropdown';
import { SelectDropdownBody } from 'Components/SelectDropdown/components/SelectDropdownBody';
import { SelectDropdownItemList } from 'Components/SelectDropdown/components/SelectDropdownItemList';
import { ContentLoader } from 'Components/base/ContentLoader';
import { Container } from 'Components/base/grid/Container';
import { Search } from 'Components/inputs/Search';
import { Select } from 'Components/inputs/Select';
import { OptionWithSingleLabel } from 'Components/inputs/Select/options/OptionWithSingleLabel';
import { TextInput } from 'Components/inputs/TextInput';
import { options } from 'Mocks/fakeOptions';
import UploadImage from 'Pages/GeneralSettings/BusinessUnits/Modals/CreateBusinessUnit/components/UploadImage';
import classNames from 'classnames';
import React, { useEffect, useState } from 'react';

import spacing from '../../../../../../assets/styles/spacing.module.scss';

type CreateNewProjectProps = { toggleActive(active: boolean): void; isLoading: boolean };

const CreateNewSubProjectForm = ({ toggleActive, isLoading }: CreateNewProjectProps) => {
  const [subprojectName, setSubprojectName] = useState<string>('');
  const [parentCompanyValue, setParentCompanyValue] = useState<string>('');
  const [parentProjectValue, setParentProjectValue] = useState<string>('');

  const [searchValue, setSearchValue] = useState<string>('');

  useEffect(() => {
    toggleActive(!!subprojectName && !!parentCompanyValue && !!parentProjectValue);
  }, [subprojectName, parentCompanyValue, parentProjectValue]);

  return (
    <Container
      background={'white-100'}
      flexdirection="column"
      borderRadius="20"
      className={classNames(spacing.p24, spacing.mt12)}
      gap="24"
    >
      <ContentLoader isLoading={isLoading} height="48px">
        <TextInput
          name="subprojectName"
          label="Subproject name"
          value={subprojectName}
          onChange={(e) => {
            setSubprojectName(e.target.value);
          }}
          required
          width="full"
        />
      </ContentLoader>
      <Container gap="24">
        <ContentLoader isLoading={isLoading} height="48px">
          <Select
            label="Parent company"
            placeholder="Select option"
            height="48"
            width="300"
            required
          >
            {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
              <SelectDropdown selectElement={selectElement}>
                <SelectDropdownBody>
                  <Search
                    className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                    width="full"
                    height="40"
                    name="search"
                    placeholder="Search Parent company"
                    value={searchValue}
                    onChange={(e) => setSearchValue(e.target.value)}
                    searchParams={[]}
                    onSearch={() => {}}
                    onClear={() => {}}
                  />
                  <SelectDropdownItemList>
                    {options.map((value, index) => (
                      <OptionWithSingleLabel
                        id={value.label}
                        key={index}
                        label={value?.label}
                        selected={!Array.isArray(selectValue) && selectValue?.value === value.value}
                        onClick={() => {
                          onOptionClick({
                            value: value?.value,
                            label: value?.label,
                          });
                          closeDropdown();
                          setParentCompanyValue(value.value);
                        }}
                      />
                    ))}
                  </SelectDropdownItemList>
                </SelectDropdownBody>
              </SelectDropdown>
            )}
          </Select>
        </ContentLoader>
        <ContentLoader isLoading={isLoading} height="48px">
          <Select
            label="Parent project"
            placeholder="Select option"
            height="48"
            required
            width="300"
          >
            {({ selectElement, selectValue, onOptionClick, closeDropdown }) => (
              <SelectDropdown selectElement={selectElement}>
                <SelectDropdownBody>
                  <Search
                    className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
                    width="full"
                    height="40"
                    name="search"
                    placeholder="Search Parent project"
                    value={searchValue}
                    onChange={(e) => setSearchValue(e.target.value)}
                    searchParams={[]}
                    onSearch={() => {}}
                    onClear={() => {}}
                  />
                  <SelectDropdownItemList>
                    {options.map((value, index) => (
                      <OptionWithSingleLabel
                        id={value.label}
                        key={index}
                        label={value?.label}
                        selected={!Array.isArray(selectValue) && selectValue?.value === value.value}
                        onClick={() => {
                          onOptionClick({
                            value: value?.value,
                            label: value?.label,
                          });
                          closeDropdown();
                          setParentProjectValue(value.value);
                        }}
                      />
                    ))}
                  </SelectDropdownItemList>
                </SelectDropdownBody>
              </SelectDropdown>
            )}
          </Select>
        </ContentLoader>
      </Container>
      <ContentLoader isLoading={isLoading} height="132px">
        <UploadImage />
      </ContentLoader>
    </Container>
  );
};

export default CreateNewSubProjectForm;
