import { ContentLoader } from 'Components/base/ContentLoader';
import { Container } from 'Components/base/grid/Container';
import { TextInput } from 'Components/inputs/TextInput';
import UploadImage from 'Pages/GeneralSettings/BusinessUnits/Modals/CreateBusinessUnit/components/UploadImage';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../../../../assets/styles/spacing.module.scss';

type CreateNewCompanyProps = { toggleActive(active: boolean): void; isLoading: boolean };
const CreateNewCompanyForm = ({ toggleActive, isLoading }: CreateNewCompanyProps) => {
  const [companyName, setCompanyName] = useState<string>('');
  return (
    <Container
      background={'white-100'}
      flexdirection="column"
      borderRadius="20"
      className={classNames(spacing.p24, spacing.mt12)}
      gap="24"
    >
      <ContentLoader isLoading={isLoading} height="48px">
        <TextInput
          name="companyName"
          label={'Company name'}
          value={companyName}
          onChange={(e) => {
            setCompanyName(e.target.value);
            toggleActive(!!e.target.value);
          }}
          required
          width="full"
        />
      </ContentLoader>
      <ContentLoader isLoading={isLoading} height="132px">
        <UploadImage />
      </ContentLoader>
    </Container>
  );
};

export default CreateNewCompanyForm;
