import { Container } from 'Components/base/grid/Container';
import { UploadContainer } from 'Components/custom/Upload/UploadContainer';
import { UploadInfo } from 'Components/custom/Upload/UploadInfo';
import { UploadItemField } from 'Components/custom/Upload/UploadItemField';
import { checkFileType } from 'Utils/fileHandlers';
import React, { useState } from 'react';

const UploadImage = () => {
  const [image, setImage] = useState<File | null>(null);
  const handleImageUpload = (item: File) => {
    setImage(item);
  };
  const handleImageDelete = () => {
    setImage(null);
  };
  const handleImageDrop = (e: React.DragEvent<HTMLElement>) => {
    const dropImage = e.dataTransfer.files[0];
    if (checkFileType(dropImage, ['jpg', 'jpeg', 'png', 'webp'])) {
      setImage(dropImage);
    }
  };
  return (
    <Container flexdirection="row" flexwrap="wrap" gap="24">
      <UploadContainer onDrop={handleImageDrop}>
        <UploadItemField
          name="uploader"
          files={image}
          acceptFilesType="image"
          onChange={handleImageUpload}
          onDelete={handleImageDelete}
        />

        <UploadInfo
          mainText={image ? image.name : 'Upload product logo. Drag and drop or Choose file'}
          secondaryText="JPG; PNG; Maximum file size is 5MB"
        />
      </UploadContainer>
    </Container>
  );
};

export default UploadImage;
