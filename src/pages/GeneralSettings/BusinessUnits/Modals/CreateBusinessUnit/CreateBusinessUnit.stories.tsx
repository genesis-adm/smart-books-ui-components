import { Meta, Story } from '@storybook/react';
import CreateBusinessUnit from 'Pages/GeneralSettings/BusinessUnits/Modals/CreateBusinessUnit/components/CreateBusinessUnit';
import React from 'react';

export const CreateNewCompany: Story = ({ storyState, editMode }) => (
  <CreateBusinessUnit type="company" isLoading={storyState === 'loading'} editMode={editMode} />
);

export const CreateNewProject: Story = ({ storyState, editMode }) => (
  <CreateBusinessUnit type="project" isLoading={storyState === 'loading'} editMode={editMode} />
);
export const CreateNewSubProject: Story = ({ storyState, editMode }) => (
  <CreateBusinessUnit type="subproject" isLoading={storyState === 'loading'} editMode={editMode} />
);

export default {
  title: 'Pages/General Settings/Business Units/Modals/Create Business Unit',
  argTypes: {
    storyState: {
      options: ['empty', 'loading', 'loaded'],
      control: {
        type: 'radio',
        labels: {
          empty: 'Empty page',
          loading: 'Page is loading',
          loaded: 'Page loaded',
        },
      },
    },
    editMode: {
      control: { type: 'boolean' },
    },
  },
} as Meta;
