import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import ViewBusinessUnitOrLegalEntityPlaceholder from 'Pages/GeneralSettings/components/ViewBusinessUnitOrLegalEntityPlaceholder';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../../../../assets/styles/spacing.module.scss';
import IconButton from '../../../../../../components/base/buttons/IconButton/IconButton';

const generalInfo = [
  { name: 'Company name*', value: 'SmartBooks' },
  { name: 'Project*', value: 'SmartBooks' },
];
const GeneralInfo = ({ isLoading }: { isLoading: boolean }) => {
  if (isLoading) return <ViewBusinessUnitOrLegalEntityPlaceholder />;

  return (
    <Container
      background={'white-100'}
      flexdirection="column"
      borderRadius="20"
      className={classNames(spacing.p24, spacing.mt12)}
      gap="24"
    >
      <Container justifycontent="space-between">
        <Text type="body-regular-14">General information</Text>
        <IconButton
          icon={<EditIcon />}
          color="grey-100-yellow-90"
          background={'grey-10-yellow-10'}
          size="24"
          tooltip="Edit General information"
        />
      </Container>
      <Container flexdirection="column" gap="20">
        {generalInfo.map(({ name, value }) => (
          <Container key={name} alignitems="center">
            <Text className={spacing.w150min} type="caption-regular" color="grey-100">
              {name}
            </Text>
            <Text type="body-regular-14">{value}</Text>
          </Container>
        ))}
      </Container>
    </Container>
  );
};

export default GeneralInfo;
