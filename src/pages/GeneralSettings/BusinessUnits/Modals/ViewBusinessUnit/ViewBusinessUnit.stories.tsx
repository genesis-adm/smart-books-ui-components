import { Meta, Story } from '@storybook/react';
import { DropDown } from 'Components/DropDown';
import { ContentLoader } from 'Components/base/ContentLoader';
import { Divider } from 'Components/base/Divider';
import { Tag } from 'Components/base/Tag';
import { Button } from 'Components/base/buttons/Button';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import ModalHeaderPlaceholder from 'Components/custom/placeholders/ModalHeaderPlaceholder';
import Logo from 'Components/elements/Logo/Logo';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import GeneralInfo from 'Pages/GeneralSettings/BusinessUnits/Modals/ViewBusinessUnit/components/GeneralInfo';
import LegalEntities from 'Pages/GeneralSettings/BusinessUnits/Modals/ViewBusinessUnit/components/LegalEntities';
import { ReactComponent as CloseIcon } from 'Svg/v2/16/cross.svg';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import { ReactComponent as DropdownIcon } from 'Svg/v2/16/three-dots.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';

export const ViewBusinessUnit: Story = ({ storyState }) => {
  const isLoading = storyState === 'loading';
  return (
    <Modal
      size="720"
      type="new"
      background={'grey-10'}
      header={
        isLoading ? (
          <ModalHeaderPlaceholder />
        ) : (
          <Container
            justifycontent="space-between"
            border="grey-20"
            background={'grey-10'}
            className={spacing.p24}
          >
            <Container gap="8" alignitems="center">
              <Logo content="user" name="Smart Books" size="24" radius="rounded" />
              <Text type="title-bold">SmartBooks</Text>
            </Container>
            <Container gap="16">
              <DropDown
                flexdirection="column"
                padding="8"
                control={({ handleOpen }) => (
                  <IconButton
                    size="24"
                    icon={<DropdownIcon />}
                    onClick={handleOpen}
                    background={'white-100-grey-20'}
                    color="black-100"
                  />
                )}
              >
                <DropDownButton size="204" icon={<EditIcon />} onClick={() => {}}>
                  Edit General information
                </DropDownButton>
                <DropDownButton size="204" icon={<EditIcon />} onClick={() => {}}>
                  Edit Legal entities
                </DropDownButton>
                <Divider className={spacing.m8} />
                <DropDownButton size="204" type="danger" icon={<TrashIcon />} onClick={() => {}}>
                  Delete
                </DropDownButton>
              </DropDown>
              <IconButton
                icon={<CloseIcon />}
                size="24"
                background={'white-100-grey-20'}
                color="black-100"
              />
            </Container>
          </Container>
        )
      }
      footer={
        <ModalFooterNew border background={'grey-10'}>
          <Container justifycontent="flex-end" alignitems="center" className={spacing.w100p}>
            <Container
              position="absolute"
              justifycontent="center"
              className={classNames(spacing.w100p)}
            >
              <ContentLoader isLoading={isLoading} height="48px" width="98px">
                <Tag
                  text="View mode"
                  size="48"
                  color="black"
                  font="body-regular-14"
                  cursor="default"
                />
              </ContentLoader>
            </Container>
            <Container justifycontent="flex-end">
              <ContentLoader isLoading={isLoading} height="48px" width="153px">
                <Button
                  height="48"
                  width="150"
                  border="grey-20"
                  background={'white-100'}
                  color="grey-100-black-100"
                >
                  Close
                </Button>
              </ContentLoader>
            </Container>
          </Container>
        </ModalFooterNew>
      }
    >
      <GeneralInfo isLoading={isLoading} />
      <LegalEntities isLoading={isLoading} />
    </Modal>
  );
};

export default {
  title: 'Pages/General Settings/Business Units/Modals/View Business unit',
  component: ViewBusinessUnit,
  argTypes: {
    storyState: {
      options: ['empty', 'loading', 'loaded'],
      control: {
        type: 'radio',
        labels: {
          empty: 'Empty page',
          loading: 'Page is loading',
          loaded: 'Page loaded',
        },
      },
    },
  },
} as Meta;
