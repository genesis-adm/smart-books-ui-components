import { Meta, Story } from '@storybook/react';
import { DropDown } from 'Components/DropDown';
import { Divider } from 'Components/base/Divider';
import { Button } from 'Components/base/buttons/Button';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { Container } from 'Components/base/grid/Container';
import { SettingsContainer } from 'Components/base/grid/SettingsContainer';
import { Text } from 'Components/base/typography/Text';
import { MainPageEmpty } from 'Components/custom/MainPage/MainPageEmpty';
import NavigationSettingsSB from 'Components/custom/Stories/NavigationSettingsSB/NavigationSettingsSB';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import {
  TableRowExpandable,
  TableRowExpandableWrapper,
} from 'Components/elements/Table/TableRowExpandable';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { FilterDropDown } from 'Components/elements/filters/FilterDropDown';
import { FilterDropDownItem } from 'Components/elements/filters/FilterDropDownItem';
import { FilterItemsContainer } from 'Components/elements/filters/FilterItemsContainer';
import { FilterSearch } from 'Components/elements/filters/FilterSearch';
import { FilterToggleButton } from 'Components/elements/filters/FilterToggleButton';
import { FiltersWrapper } from 'Components/elements/filters/FiltersWrapper';
import { Search } from 'Components/inputs/Search';
import { Modal } from 'Components/modules/Modal';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { TableCellWidth } from 'Components/types/gridTypes';
import { options } from 'Mocks/fakeOptions';
import BusinessUnitsSettingsTableRowExample from 'Pages/GeneralSettings/components/BusinessUnitsSettingsTableRowExample';
import { tableHead } from 'Pages/GeneralSettings/mocks/BUSettings';
import { ReactComponent as BuildingsIcon } from 'Svg/v2/16/buildings.svg';
import { ReactComponent as CaretDownIcon } from 'Svg/v2/16/caret-down.svg';
import { ReactComponent as DocumentIcon } from 'Svg/v2/16/document.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as SmartphoneIcon } from 'Svg/v2/16/smartphone.svg';
import classNames from 'classnames';
import React, { useCallback, useState } from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

export const BusinessUnitsSettingsComponent: Story = ({ storyState }) => {
  const [showFilters, setShowFilters] = useState<boolean>(false);
  const [searchValue, setSearchValue] = useState('');

  const [isActiveNested, setIsActiveNested] = useState(false);
  const [selected, setSelect] = useState<Array<number>>([]);

  const handleSelect = useCallback(
    (arg: number) => setSelect([...selected, arg]),
    [setSelect, selected],
  );

  const handleUnSelect = useCallback(
    (arg: number) => setSelect(selected?.filter((id) => arg !== id)),
    [setSelect, selected],
  );
  const handleToggle = (id: number) => () => {
    if (selected?.includes(id)) handleUnSelect(id);
    else handleSelect(id);
  };
  const handleToggleNested = () => setIsActiveNested((prevState) => !prevState);
  const handleChange = (value: string) => {
    setSearchValue(value);
  };

  return (
    <Modal
      size="full"
      background={'grey-10'}
      overlay="grey-10"
      pluginScrollDisabled
      header={
        <ModalHeaderNew title="Organization settings" onClose={() => {}} background={'grey-10'} />
      }
    >
      <Container flexgrow="1" overflow="y-auto">
        <NavigationSettingsSB selected="Business units" />
        <SettingsContainer className={spacing.p24} width="100p" paddingX="0" paddingY="0">
          {storyState === 'loaded' && (
            <>
              <Container justifycontent="space-between" alignitems="center">
                <Text type="title-bold">Business units</Text>
                <Button width="220" height="40" iconLeft={<PlusIcon />} onClick={() => {}}>
                  Create Business units
                </Button>
              </Container>
              <Container
                className={classNames(spacing.mt24)}
                justifycontent="space-between"
                gap="24"
              >
                <Container gap="24" alignitems="center">
                  <FilterToggleButton
                    showFilters={showFilters}
                    filtersSelected={false}
                    onClear={() => {}}
                    onClick={() => {
                      setShowFilters((prevState) => !prevState);
                    }}
                  />
                  <Search
                    width="320"
                    height="40"
                    name="search-settings-bu"
                    placeholder="Search by Name"
                    value=""
                    onChange={() => {}}
                    searchParams={[]}
                    onSearch={() => {}}
                    onClear={() => {}}
                  />
                </Container>
              </Container>
              <FiltersWrapper active={showFilters} margins="none">
                <FilterDropDown
                  title={'Business type'}
                  value={'All'}
                  onClear={() => {}}
                  selectedAmount={0}
                >
                  <Container
                    className={classNames(spacing.m16, spacing.w288fixed)}
                    flexdirection="column"
                  >
                    <FilterSearch
                      title="Business type:"
                      value={searchValue}
                      onChange={handleChange}
                    />
                    <FilterItemsContainer className={spacing.mt8}>
                      {options.map((item) => (
                        <FilterDropDownItem
                          key={item.value}
                          id={item.value}
                          type="item"
                          checked={false}
                          blurred={false}
                          value={item.label}
                          onChange={() => {}}
                        />
                      ))}
                    </FilterItemsContainer>
                  </Container>
                  <Divider fullHorizontalWidth />
                  <Container
                    className={classNames(spacing.m16, spacing.w288fixed)}
                    justifycontent="flex-end"
                    alignitems="center"
                  >
                    <Button
                      width="auto"
                      height="32"
                      padding="8"
                      font="text-medium"
                      onClick={() => {}}
                      background={'violet-90-violet-100'}
                      color="white-100"
                    >
                      Apply
                    </Button>
                  </Container>
                </FilterDropDown>
              </FiltersWrapper>
              <TableNew
                noResults={false}
                className={classNames(spacing.mt24, spacing.ml24, spacing.pr24, spacing.h100p)}
                tableHead={
                  <TableHeadNew>
                    {tableHead.map((item, index) => (
                      <TableTitleNew
                        key={index}
                        minWidth={item.minWidth as TableCellWidth}
                        padding={item.padding as TableTitleNewProps['padding']}
                        align={item.align as TableTitleNewProps['align']}
                        title={item.title}
                        onClick={() => {}}
                      />
                    ))}
                  </TableHeadNew>
                }
              >
                <TableBodyNew>
                  {Array.from(Array(4)).map((_, index) => (
                    <TableRowExpandableWrapper isActive={selected.includes(index)} key={index}>
                      <TableRowExpandable
                        type="primary"
                        isRowVisible
                        isToggleActive={selected.includes(index)}
                        onToggle={handleToggle(index)}
                      >
                        <BusinessUnitsSettingsTableRowExample index={index} type="primary" />
                      </TableRowExpandable>
                      <TableRowExpandable
                        isRowVisible={selected.includes(index)}
                        isToggleActive={isActiveNested}
                        onToggle={handleToggleNested}
                      >
                        <BusinessUnitsSettingsTableRowExample index={index} type="secondary" />
                      </TableRowExpandable>
                      {isActiveNested && selected.includes(index) && (
                        <TableRowExpandable isRowVisible type="tertiary">
                          <BusinessUnitsSettingsTableRowExample index={index} type="tertiary" />
                        </TableRowExpandable>
                      )}
                    </TableRowExpandableWrapper>
                  ))}
                </TableBodyNew>
              </TableNew>
            </>
          )}
          {storyState === 'empty' && (
            <MainPageEmpty
              title="Create new Business unit"
              description="You have no business units created. Your business units will be displayed here."
              action={
                <DropDown
                  flexdirection="column"
                  padding="8"
                  control={({ handleOpen }) => (
                    <Button
                      onClick={handleOpen}
                      background={'blue-10-blue-20'}
                      color="violet-90-violet-100"
                      width="220"
                      height="40"
                      iconRight={<CaretDownIcon />}
                    >
                      Create Business unit
                    </Button>
                  )}
                >
                  <DropDownButton size="204" onClick={() => {}} icon={<BuildingsIcon />}>
                    Company
                  </DropDownButton>
                  <DropDownButton
                    size="204"
                    disabled
                    tooltip="Please create Company first"
                    onClick={() => {}}
                    icon={<SmartphoneIcon />}
                  >
                    Project
                  </DropDownButton>
                  <DropDownButton
                    size="204"
                    disabled
                    tooltip="Please create Project first"
                    onClick={() => {}}
                    icon={<DocumentIcon />}
                  >
                    Subproject
                  </DropDownButton>
                </DropDown>
              }
            />
          )}
        </SettingsContainer>
      </Container>
    </Modal>
  );
};

export default {
  title: 'Pages/General Settings/Business Units/BusinessUnitsSettings',
  component: BusinessUnitsSettingsComponent,
  argTypes: {
    storyState: {
      options: ['empty', 'loading', 'loaded'],
      control: {
        type: 'radio',
        labels: {
          empty: 'Empty page',
          loading: 'Page is loading',
          loaded: 'Page loaded',
        },
      },
    },
  },
} as Meta;

export const BusinessUnitsSettings = BusinessUnitsSettingsComponent.bind({});

BusinessUnitsSettings.args = {
  storyState: 'loaded',
};
