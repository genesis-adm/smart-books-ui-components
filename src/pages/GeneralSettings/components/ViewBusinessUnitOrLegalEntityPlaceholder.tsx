import { ContentLoader } from 'Components/base/ContentLoader';
import { Container } from 'Components/base/grid/Container';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

const ViewBusinessUnitOrLegalEntityPlaceholder = () => (
  <Container
    background={'white-100'}
    flexdirection="column"
    borderRadius="20"
    className={classNames(spacing.p24, spacing.mt12)}
    gap="16"
  >
    <Container justifycontent="space-between">
      <ContentLoader isLoading height="24px" width="182px" />
      <ContentLoader isLoading height="24px" width="24px" />
    </Container>
    <ContentLoader isLoading height="24px" width="182px" />
  </Container>
);

export default ViewBusinessUnitOrLegalEntityPlaceholder;
