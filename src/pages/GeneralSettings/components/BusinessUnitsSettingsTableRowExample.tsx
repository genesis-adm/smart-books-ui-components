import { DropDown } from 'Components/DropDown';
import { Divider } from 'Components/base/Divider';
import { TagSegmented } from 'Components/base/TagSegmented';
import { Tooltip } from 'Components/base/Tooltip';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { Logo } from 'Components/elements/Logo';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableCellWidth } from 'Components/types/gridTypes';
import { tableHead } from 'Pages/GeneralSettings/mocks/BUSettings';
import { ReactComponent as EyeIcon } from 'Svg/v2/16/eye.svg';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as DropdownIcon } from 'Svg/v2/16/three-dots.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

type RowType = 'primary' | 'secondary' | 'tertiary';
type BusinessUnitsSettingsTableRowExampleProps = {
  index: number;
  type?: RowType;
};
const BusinessUnitsSettingsTableRowExample = ({
  index,
  type = 'primary',
}: BusinessUnitsSettingsTableRowExampleProps) => {
  const legalEntityName = 'Legal entities name sdkjhfvsiugwjsg8nergsug8swhern8h8uetjjhrtjkry';

  const valueByType: Record<RowType, { name: string; type: string }> = {
    primary: { name: 'First level company', type: 'Company' },
    secondary: { name: 'Second level - project', type: 'Project' },
    tertiary: { name: 'Third level - subproject', type: 'Subproject' },
  };

  return (
    <>
      <TableCellNew
        minWidth={tableHead[1].minWidth as TableCellWidth}
        justifycontent="center"
        alignitems="center"
      >
        <Logo
          content="picture"
          size="44"
          name="Genesis Media & Mobile Application"
          img={index % 2 === 1 ? 'https://picsum.photos/60' : undefined}
        />
      </TableCellNew>
      <TableCellTextNew
        minWidth={tableHead[2].minWidth as TableCellWidth}
        fixedWidth
        value={valueByType[type].name}
      />
      <TableCellTextNew
        minWidth={tableHead[3].minWidth as TableCellWidth}
        value={valueByType[type].type}
      />
      <TableCellNew minWidth={tableHead[4].minWidth as TableCellWidth} fixedWidth>
        <TagSegmented
          mainLabel={legalEntityName}
          showCounter={index % 2 === 0}
          counter={
            <DropDown
              flexdirection="column"
              padding="8"
              classnameInner={classNames(spacing.h300)}
              customScroll
              control={({ handleOpen }) => (
                <Tooltip message={'Show All'}>
                  <TagSegmented.Counter counterValue={2} onClick={handleOpen} />
                </Tooltip>
              )}
            >
              <Container flexdirection="column" gap="12">
                <Text type="button" color="grey-100">
                  Legal entities
                </Text>
                {Array.from(Array(16)).map((_) => (
                  <Text type="body-regular-14"> Legal entities name</Text>
                ))}
              </Container>
            </DropDown>
          }
        />
      </TableCellNew>
      <TableCellNew
        minWidth={tableHead[5].minWidth as TableCellWidth}
        justifycontent="center"
        alignitems="center"
      >
        <DropDown
          flexdirection="column"
          padding="8"
          gap="4"
          control={({ handleOpen }) => (
            <IconButton
              icon={<DropdownIcon />}
              onClick={handleOpen}
              background={'transparent'}
              color="grey-100-violet-90"
            />
          )}
        >
          <DropDownButton size="204" icon={<EyeIcon />} onClick={() => {}}>
            Open
          </DropDownButton>
          <DropDownButton size="204" icon={<PlusIcon />} onClick={() => {}}>
            Add Project
          </DropDownButton>
          <DropDownButton size="204" icon={<EditIcon />} onClick={() => {}}>
            Edit Legal entities
          </DropDownButton>
          <DropDownButton size="204" icon={<EditIcon />} onClick={() => {}}>
            Edit Company
          </DropDownButton>
          <Divider type="horizontal" fullHorizontalWidth className={spacing.mY4} />
          <DropDownButton size="204" type="danger" icon={<TrashIcon />} onClick={() => {}}>
            Delete
          </DropDownButton>
        </DropDown>
      </TableCellNew>
    </>
  );
};

export default BusinessUnitsSettingsTableRowExample;
