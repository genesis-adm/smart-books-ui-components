import { ContentLoader } from 'Components/base/ContentLoader';
import { Container } from 'Components/base/grid/Container';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

const ListPlaceholder = () => (
  <Container
    background={'white-100'}
    flexdirection="column"
    borderRadius="20"
    alignitems="center"
    className={classNames(spacing.p24, spacing.h100p, spacing.w404fixed)}
    gap="24"
  >
    <ContentLoader isLoading height="24px" width="190px" />
    <Container
      flexdirection="column"
      justifycontent="center"
      alignitems="center"
      gap="20"
      className={spacing.h100p}
    >
      <ContentLoader isLoading type="circle" width="124px" height="124px" />
      <ContentLoader isLoading width="200px" height="24px" />
    </Container>
  </Container>
);

export default ListPlaceholder;
