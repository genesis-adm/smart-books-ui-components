import { ContentLoader } from 'Components/base/ContentLoader';
import { Container } from 'Components/base/grid/Container';
import React from 'react';

const SetupFooterPlaceholder = () => (
  <Container gap="24">
    <ContentLoader isLoading width="808px" />
    <ContentLoader isLoading width="133px" />
  </Container>
);

export default SetupFooterPlaceholder;
