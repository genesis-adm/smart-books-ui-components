import { ContentLoader } from 'Components/base/ContentLoader';
import { Container } from 'Components/base/grid/Container';
import TablePlaceholder from 'Components/custom/placeholders/TablePlaceholder';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

const ChooseBUorLEPlaceholder = () => (
  <Container
    background={'white-100'}
    flexdirection="column"
    borderRadius="20"
    flexgrow="4"
    className={classNames(spacing.p24)}
    gap="24"
  >
    <Container justifycontent="center" alignitems="center">
      <ContentLoader isLoading height="24px" width="190px" />
    </Container>
    <Container justifycontent="space-between">
      <ContentLoader isLoading width="440px" />
      <ContentLoader isLoading width="223px" />
    </Container>
    <Container flexdirection="column">
      <ContentLoader isLoading width="190px" height="32px" />
      <TablePlaceholder paddingX="none" />
    </Container>
  </Container>
);

export default ChooseBUorLEPlaceholder;
