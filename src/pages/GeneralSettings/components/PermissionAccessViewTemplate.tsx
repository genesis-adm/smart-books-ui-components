import { Divider } from 'Components/base/Divider';
import { Icon } from 'Components/base/Icon';
import { Tag } from 'Components/base/Tag';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { SwitcherType, accessData, smallHeader } from 'Pages/GeneralSettings/mocks/setUpAccessMock';
import React from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

type PermissionAccessViewTemplateProps = { title: string; switchers?: SwitcherType[] };
const PermissionAccessViewTemplate = ({ title, switchers }: PermissionAccessViewTemplateProps) => (
  <Container
    flexdirection="column"
    gap="12"
    background={'white-100'}
    className={spacing.p16}
    borderRadius="8"
  >
    <Text color="grey-100">{title}</Text>
    {smallHeader.map(({ icon, title, accessType }) => (
      <Container justifycontent="space-between">
        <Container gap="4" alignitems="center">
          <Icon icon={icon} path="grey-100" />
          <Text color="grey-100">{title}</Text>
        </Container>
        <Tag
          color={accessData[accessType].color}
          font="subtext-regular"
          padding="0-4"
          size="16"
          text={accessData[accessType].title}
        />
      </Container>
    ))}
    {switchers && <Divider fullHorizontalWidth />}
    {switchers &&
      switchers.map(({ title, accessType }) => (
        <Container justifycontent="space-between">
          <Text color="grey-100">{title}</Text>
          <Tag
            color={accessData[accessType].color}
            font="subtext-regular"
            padding="0-4"
            size="16"
            text={accessData[accessType].title}
          />
        </Container>
      ))}
  </Container>
);

export default PermissionAccessViewTemplate;
