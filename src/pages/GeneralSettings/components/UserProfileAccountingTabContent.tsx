import PermissionAccessViewTemplate from 'Pages/GeneralSettings/components/PermissionAccessViewTemplate';
import {
  chartOfAccountsSwitchers,
  journalSwitchers,
  templateAccountsSwitchers,
} from 'Pages/GeneralSettings/mocks/setUpAccessMock';
import React from 'react';

const UserProfileAccountingTabContent = () => (
  <>
    <PermissionAccessViewTemplate title="Chart of accounts" switchers={chartOfAccountsSwitchers} />
    <PermissionAccessViewTemplate title="Template accounts" switchers={templateAccountsSwitchers} />
    <PermissionAccessViewTemplate title="Journal" switchers={journalSwitchers} />
  </>
);

export default UserProfileAccountingTabContent;
