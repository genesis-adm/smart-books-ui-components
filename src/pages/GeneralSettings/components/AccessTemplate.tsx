import { Divider } from 'Components/base/Divider';
import { Switch } from 'Components/base/Switch';
import { RadioButtonStoplight } from 'Components/base/buttons/RadioButtonStoplight';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { accessOptions } from 'Pages/GeneralSettings/mocks/setUpAccessMock';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

type AccessTemplateProps = {
  title: string;
  switchers: { id: string; title: string }[];
};
const AccessTemplate = ({ title, switchers }: AccessTemplateProps) => {
  return (
    <Container
      background={'white-100'}
      flexdirection="column"
      radius
      className={spacing.p24}
      gap="16"
      borderRadius="20"
    >
      <Container alignitems="center" justifycontent="space-between">
        <Text color="grey-100" type="body-regular-14">
          {title}
        </Text>
        <Container justifycontent="space-between" className={spacing.w330fixed}>
          {Array.from(Array(4)).map((_, index) => (
            <Container key={index} flexdirection="column" gap="16">
              {Array.from(Array(3)).map((_, i) => (
                <RadioButtonStoplight
                  key={i}
                  name={`name-${i}`}
                  id={String(i)}
                  checked={i === 0}
                  color={i === 0 ? 'error' : i === 1 ? 'warning' : 'success'}
                  onChange={() => {}}
                />
              ))}
            </Container>
          ))}
        </Container>
        <Container flexdirection="column" gap="16">
          {accessOptions.map(({ title }) => (
            <Text color="grey-100" type="body-regular-14">
              {title}
            </Text>
          ))}
        </Container>
      </Container>
      <Divider fullHorizontalWidth />
      <Container flexwrap="wrap" gap="20" justifycontent="space-between">
        {switchers.map(({ title, id }, index) => (
          <>
            <Container
              justifycontent="flex-start"
              alignitems="center"
              className={classNames(spacing.w300min, spacing.pY12)}
              gap="24"
            >
              <Switch id={id} />
              <Text type="body-regular-14" color="grey-100">
                {title}
              </Text>
            </Container>
            {index !== switchers.length - 1 && index % 2 === 0 && (
              <Divider type="vertical" height="auto" />
            )}
          </>
        ))}
      </Container>
    </Container>
  );
};

export default AccessTemplate;
