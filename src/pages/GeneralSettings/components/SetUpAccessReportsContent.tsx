import { Divider } from 'Components/base/Divider';
import { Switch } from 'Components/base/Switch';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

const SetUpAccessReportsContent: React.FC = () => (
  <Container
    flexgrow="1"
    flexdirection="column"
    className={spacing.mt20}
    gap="16"
    justifycontent="center"
  >
    <Container
      background="white-100"
      flexdirection="column"
      borderRadius="20"
      className={classNames(spacing.pY44, spacing.pX24)}
      gap="36"
    >
      <Container alignitems="center" justifycontent="space-between">
        <Container flexdirection="column" gap="24">
          <Container
            alignitems="center"
            className={classNames(spacing.w320min, spacing.pY12, spacing.pX12)}
            gap="24"
          >
            <Switch id="import-transactions" />

            <Text type="body-regular-14" color="grey-100">
              Trial balance
            </Text>
          </Container>
          <Container
            alignitems="center"
            className={classNames(spacing.w320min, spacing.pY12, spacing.pX12)}
            gap="24"
          >
            <Switch id="import-transactions" />

            <Text type="body-regular-14" color="grey-100">
              Trial balance for the period
            </Text>
          </Container>
        </Container>

        <Divider type="vertical" background="grey-20" height="full" />

        <Container flexdirection="column" gap="24">
          <Container
            alignitems="center"
            className={classNames(spacing.w320min, spacing.pY12, spacing.pX12)}
            gap="24"
          >
            <Switch id="import-transactions" />

            <Text type="body-regular-14" color="grey-100">
              Account register report
            </Text>
          </Container>
          <Container
            alignitems="center"
            className={classNames(spacing.w320min, spacing.pY12, spacing.pX12)}
            gap="24"
          >
            <Switch id="import-transactions" />

            <Text type="body-regular-14" color="grey-100">
              Bank Statement Report
            </Text>
          </Container>
        </Container>
      </Container>
    </Container>
  </Container>
);

export default SetUpAccessReportsContent;
