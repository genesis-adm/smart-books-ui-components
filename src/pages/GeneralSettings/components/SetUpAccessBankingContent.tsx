import { Divider } from 'Components/base/Divider';
import { Icon } from 'Components/base/Icon';
import { Switch } from 'Components/base/Switch';
import { RadioButtonStoplight } from 'Components/base/buttons/RadioButtonStoplight';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as EyeIcon } from 'Svg/v2/16/eye.svg';
import { ReactComponent as PencilIcon } from 'Svg/v2/16/pencil.svg';
import { ReactComponent as PlusFilledIcon } from 'Svg/v2/16/plus-filled-in-square.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

const SetUpAccessBankingContent: React.FC = () => (
  <Container flexgrow="1" flexdirection="column" className={spacing.mt20} gap="16">
    <Container gap="36" justifycontent="center">
      <Container gap="8" alignitems="center">
        <Icon path="grey-100" icon={<PlusFilledIcon />} />
        <Text type="body-regular-14" color="grey-100">
          Create
        </Text>
      </Container>
      <Container gap="8" alignitems="center">
        <Icon path="grey-100" icon={<EyeIcon />} />
        <Text type="body-regular-14" color="grey-100">
          View
        </Text>
      </Container>
      <Container gap="8" alignitems="center">
        <Icon path="grey-100" icon={<PencilIcon />} />
        <Text type="body-regular-14" color="grey-100">
          Edit
        </Text>
      </Container>
      <Container gap="8" alignitems="center">
        <Icon path="grey-100" icon={<TrashIcon />} />
        <Text type="body-regular-14" color="grey-100">
          Delete
        </Text>
      </Container>
    </Container>
    <Container gap="16" overflow="auto" flexdirection="column">
      <Container
        background="white-100"
        flexdirection="column"
        radius
        className={spacing.p24}
        gap="16"
        borderRadius="20"
      >
        <Container alignitems="center" justifycontent="space-between">
          <Text color="grey-100" type="body-regular-14">
            Transactions
          </Text>
          <Container justifycontent="space-between" className={spacing.w330fixed}>
            {Array.from(Array(4)).map((_, index) => (
              <Container key={index} flexdirection="column" gap="16">
                {Array.from(Array(3)).map((_, i) => (
                  <RadioButtonStoplight
                    key={i}
                    name={`name-${i}`}
                    id={String(i)}
                    checked={i === 0}
                    color={i === 0 ? 'error' : i === 1 ? 'warning' : 'success'}
                    onChange={() => {}}
                  />
                ))}
              </Container>
            ))}
          </Container>
          <Container flexdirection="column" gap="16">
            <Text color="grey-100" type="body-regular-14">
              Deny
            </Text>
            <Text color="grey-100" type="body-regular-14">
              Users own
            </Text>
            <Text color="grey-100" type="body-regular-14">
              Allowed
            </Text>
          </Container>
        </Container>
        <Divider fullHorizontalWidth />
        <Container flexwrap="wrap" gap="20" justifycontent="space-between">
          <Container
            justifycontent="flex-start"
            alignitems="center"
            className={classNames(spacing.w300min, spacing.pY12)}
            gap="24"
          >
            <Switch id="import-transactions" />
            <Text type="body-regular-14" color="grey-100">
              Import transactions
            </Text>
          </Container>
          <Divider type="vertical" height="auto" />
          <Container
            justifycontent="flex-start"
            alignitems="center"
            className={classNames(spacing.w300min, spacing.pY12)}
            gap="24"
          >
            <Switch id="export-transactions" />
            <Text type="body-regular-14" color="grey-100">
              Export transactions
            </Text>
          </Container>
          <Container
            justifycontent="flex-start"
            alignitems="center"
            className={classNames(spacing.w300min, spacing.pY12)}
            gap="24"
          >
            <Switch id="accept-transactions" />
            <Text type="body-regular-14" color="grey-100">
              Accept transactions
            </Text>
          </Container>
          <Divider type="vertical" height="auto" />
          <Container
            justifycontent="flex-start"
            alignitems="center"
            className={classNames(spacing.w300min, spacing.pY12)}
            gap="24"
          >
            <Switch id="transactions-history" />
            <Text type="body-regular-14" color="grey-100">
              Transactions history
            </Text>
          </Container>
        </Container>
      </Container>

      <Container
        background="white-100"
        flexdirection="column"
        radius
        className={classNames(spacing.pY44, spacing.pX24)}
        gap="36"
      >
        <Container alignitems="center" justifycontent="space-between">
          <Container
            alignitems="center"
            className={classNames(spacing.w320min, spacing.pY12, spacing.pX12)}
            gap="24"
          >
            <Switch id="import-transactions" />

            <Text type="body-regular-14" color="grey-100">
              Trial balance
            </Text>
          </Container>

          <Divider type="vertical" background="grey-20" height="full" />

          <Container
            alignitems="center"
            className={classNames(spacing.w320min, spacing.pY12, spacing.pX12)}
            gap="24"
          >
            <Switch id="import-transactions" />

            <Text type="body-regular-14" color="grey-100">
              Account register report
            </Text>
          </Container>
        </Container>
      </Container>
    </Container>
  </Container>
);

export default SetUpAccessBankingContent;
