import PermissionAccessViewTemplate from 'Pages/GeneralSettings/components/PermissionAccessViewTemplate';
import {
  bankAccountsSwitchers,
  transactionsSwitchers,
} from 'Pages/GeneralSettings/mocks/setUpAccessMock';
import React from 'react';

const UserProfileBankingTabContent = () => (
  <>
    <PermissionAccessViewTemplate title="Transactions" switchers={transactionsSwitchers} />
    <PermissionAccessViewTemplate title="Bank accounts" switchers={bankAccountsSwitchers} />
    <PermissionAccessViewTemplate title="Splits" />
  </>
);

export default UserProfileBankingTabContent;
