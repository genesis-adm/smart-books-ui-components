import { Icon } from 'Components/base/Icon';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import AccessTemplate from 'Pages/GeneralSettings/components/AccessTemplate';
import {
  chartOfAccountsSwitchers,
  header,
  journalSwitchers,
} from 'Pages/GeneralSettings/mocks/setUpAccessMock';
import React from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

const SetUpAccessAccountingContent = () => {
  return (
    <Container flexgrow="1" flexdirection="column" className={spacing.mt20} gap="16">
      <Container gap="36" justifycontent="center">
        {header.map(({ icon, title }) => (
          <Container gap="8" alignitems="center">
            <Icon path="grey-100" icon={icon} />
            <Text type="body-regular-14" color="grey-100">
              {title}
            </Text>
          </Container>
        ))}
      </Container>
      <Container gap="16" flexdirection="column">
        <AccessTemplate title="Journal" switchers={journalSwitchers} />
        <AccessTemplate title="Chart of accounts" switchers={chartOfAccountsSwitchers} />
      </Container>
    </Container>
  );
};

export default SetUpAccessAccountingContent;
