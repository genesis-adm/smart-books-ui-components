import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { DescriptionInput } from 'Components/base/inputs/DescriptionInput';
import { InputDate } from 'Components/base/inputs/InputDate';
import { Text } from 'Components/base/typography/Text';
import { Currencies } from 'Components/elements/Currencies';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableTitleNew } from 'Components/elements/Table/TableTitleNew';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { TableCellWidth } from 'Components/types/gridTypes';
import { createFXStep1Table } from 'Pages/GeneralSettings/mocks/CurrencyFXRevaluation';
import { ReactComponent as DocumentIcon } from 'Svg/v2/16/file-text.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/General Settings/CreateFXRevaluationModalStep1',
};
export const CreateFXRevaluationModalStep1: React.FC = () => {
  const [startDate, setStartDate] = useState<Date | null>(new Date());
  const [descriptionValue, setDescriptionValue] = useState<string>('');
  const [customCurrencyValue, setCustomCurrencyValue] = useState<string>('1.0012');
  const [inputCurrencyValue, setInputCurrencyValue] = useState<string>(customCurrencyValue);

  return (
    <Modal
      size="912"
      height="540"
      centered
      overlay="black-100"
      pluginScrollDisabled
      header={
        <ModalHeaderNew
          title="Create FX revaluation"
          titlePosition="center"
          fontType="body-medium"
          border
          background="grey-10"
          onClose={() => {}}
          closeActionBackgroundColor="inherit"
        />
      }
      footer={
        <ModalFooterNew justifycontent="space-between" border>
          <Container alignitems="center" gap="16" flexgrow="1" justifycontent="flex-end">
            <Container flexdirection="column">
              <Text type="body-regular-14" color="grey-100">
                Step: 1 of 2
              </Text>
              <Text type="body-regular-14" color="grey-100">
                Next: FX revaluation Preview
              </Text>
            </Container>
            <Button height="48" navigation width="180" background="grey-90" onClick={() => {}}>
              Next
            </Button>
          </Container>
        </ModalFooterNew>
      }
    >
      <Container
        className={classNames(spacing.pr24, spacing.pl44, spacing.pY24)}
        flexdirection="column"
      >
        <Container gap="24" className={spacing.w100p}>
          <InputDate
            name="inputdate"
            height="36"
            dateFormat="dd MMM yyyy"
            placeholder="08 July 2021"
            label="Date"
            width="180"
            selected={startDate}
            onChange={(date: Date) => setStartDate(date)}
            calendarStartDay={1}
            popperDirection="bottom"
          />

          <DescriptionInput
            className={spacing.pr20}
            type="description"
            name="description"
            minHeight="36"
            label="Add Description"
            maxLength={200}
            placeholder="Add Description"
            value={descriptionValue}
            required
            onChange={(e) => setDescriptionValue(e.target.value)}
            width="full"
            iconLeft={<DocumentIcon />}
          />
        </Container>
        <TableNew
          noResults={false}
          className={classNames(spacing.mt24, spacing.pr24, spacing.h290)}
          tableHead={
            <TableHeadNew background="grey-10" radius="10">
              {createFXStep1Table.map((item, index) => (
                <TableTitleNew
                  key={index}
                  minWidth={item.minWidth as TableCellWidth}
                  background="grey-10"
                  radius="10"
                  title={item.title}
                  onClick={() => {}}
                />
              ))}
            </TableHeadNew>
          }
        >
          <TableBodyNew>
            {Array.from(Array(8)).map((_, index) => (
              <TableRowNew key={index} background="transparent-with-border" size="36">
                <TableCellNew minWidth={createFXStep1Table[0].minWidth as TableCellWidth} gap="80">
                  <Text>EUR</Text>
                  <Text>Revaluate EUR against USD</Text>
                </TableCellNew>
                <TableCellNew minWidth={createFXStep1Table[1].minWidth as TableCellWidth}>
                  <Currencies
                    height="24"
                    background="grey-10"
                    selectedCurrency="eur"
                    selectedCurrencyValue="1"
                    baseCurrency="usd"
                    showCustomRate
                    baseCurrencyValue={customCurrencyValue}
                    baseCurrencyInputValue={inputCurrencyValue}
                    onChange={(e) => {
                      setInputCurrencyValue(e.target.value);
                    }}
                    onApply={() => setCustomCurrencyValue(inputCurrencyValue)}
                  />
                </TableCellNew>
              </TableRowNew>
            ))}
          </TableBodyNew>
        </TableNew>
      </Container>
    </Modal>
  );
};
