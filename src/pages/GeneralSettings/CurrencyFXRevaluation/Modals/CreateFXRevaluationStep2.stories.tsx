import { DropDown } from 'Components/DropDown';
import { Checkbox } from 'Components/base/Checkbox';
import { Divider } from 'Components/base/Divider';
import { Tag } from 'Components/base/Tag';
import { Button } from 'Components/base/buttons/Button';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ReportsTotalValues } from 'Components/custom/Reports/ReportsTotalValues';
import { Pagination } from 'Components/custom/Stories/Pagination/Pagination';
import PerPageSelect from 'Components/custom/Stories/Pagination/PerPageSelect/PerPageSelect';
import { Currencies } from 'Components/elements/Currencies';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { TransactionPageSwitch } from 'Components/elements/TransactionPageSwitch';
import { FilterDropDown } from 'Components/elements/filters/FilterDropDown';
import { FilterDropDownItem } from 'Components/elements/filters/FilterDropDownItem';
import { FilterItemsContainer } from 'Components/elements/filters/FilterItemsContainer';
import { FilterSearch } from 'Components/elements/filters/FilterSearch';
import { FilterToggleButton } from 'Components/elements/filters/FilterToggleButton';
import { FiltersWrapper } from 'Components/elements/filters/FiltersWrapper';
import { Search } from 'Components/inputs/Search';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { TableCellWidth } from 'Components/types/gridTypes';
import { options, rowsPerPageOptions } from 'Mocks/fakeOptions';
import {
  createFXStep2Filters,
  createFXStep2Table,
} from 'Pages/GeneralSettings/mocks/CurrencyFXRevaluation';
import { ReactComponent as ArrowLeftIcon } from 'Svg/v2/16/arrow-left.svg';
import { ReactComponent as EyeIcon } from 'Svg/v2/16/eye.svg';
import { ReactComponent as DropdownIcon } from 'Svg/v2/16/three-dots.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/General Settings/CreateFXRevaluationStep2',
  argTypes: {
    viewMode: {
      control: { type: 'boolean' },
    },
  },
  args: {
    viewMode: false,
  },
};
export const CreateFXRevaluationStep2 = ({ viewMode }: { viewMode: boolean }) => {
  const modalTitle = viewMode ? 'View FX revaluation # 1' : 'Create FX revaluation';
  const [showFilters, setShowFilters] = useState<boolean>(false);
  const [searchValue, setSearchValue] = useState<string>('');
  const [rowsPerPage, setRowsPerPage] = useState<string>(rowsPerPageOptions[0].value);

  const handleChange = (value: string) => {
    setSearchValue(value);
  };
  return (
    <Modal
      background={'grey-10'}
      size="full-with-scroll"
      type="new"
      padding="none"
      pluginScrollDisabled
      header={
        <ModalHeaderNew
          onClose={() => {}}
          background={'grey-10'}
          title={modalTitle}
          titlePosition="center"
          dropdownBtn={
            viewMode && (
              <DropDown
                flexdirection="column"
                padding="8"
                control={({ handleOpen }) => (
                  <IconButton
                    size="24"
                    icon={<DropdownIcon />}
                    onClick={handleOpen}
                    background={'transparent-grey-20'}
                    color="black-100"
                  />
                )}
              >
                <DropDownButton size="160" icon={<EyeIcon />} onClick={() => {}}>
                  Open Journal entry
                </DropDownButton>
                <Divider />
                <DropDownButton size="160" type="danger" icon={<TrashIcon />} onClick={() => {}}>
                  Delete
                </DropDownButton>
              </DropDown>
            )
          }
          closeActionBackgroundColor="inherit"
        />
      }
      footer={
        <ModalFooterNew justifycontent="space-between" background={'grey-10'}>
          {!viewMode ? (
            <Container alignitems="center" gap="16" flexgrow="1" justifycontent="flex-end">
              <Container flexdirection="column">
                <Text type="body-regular-14" color="grey-100">
                  Step: 2 of 2
                </Text>
                <Text type="body-regular-14" color="grey-100">
                  FX revaluation Preview
                </Text>
              </Container>
              <Button
                height="48"
                width="180"
                navigation
                iconLeft={<ArrowLeftIcon />}
                background={'white-100'}
                color="black-100"
                border="grey-20-grey-90"
                onClick={() => {}}
              >
                Back
              </Button>
              <Button height="48" navigation width="180" background={'grey-90'} onClick={() => {}}>
                Next
              </Button>
            </Container>
          ) : (
            <>
              <Button
                height="48"
                width="230"
                background={'white-100'}
                color="black-100"
                border="grey-20-grey-90"
                onClick={() => {}}
              >
                Close
              </Button>
              <TransactionPageSwitch
                page={1}
                totalPages={30}
                isViewMode
                bottom="none"
                onPrevPage={() => {}}
                onNextPage={() => {}}
              />
            </>
          )}
        </ModalFooterNew>
      }
    >
      <Container
        justifycontent="space-between"
        alignitems="center"
        className={classNames(spacing.mt16, spacing.mX24)}
      >
        <Container alignitems="center" gap="16">
          <Container flexdirection="column" gap="8">
            <Text color="grey-100" className={spacing.ml4}>
              As of date
            </Text>
            <Tag text="19.02.2023" color="grey-black" />
          </Container>
          <Container flexdirection="column" gap="8">
            <Text color="grey-100" className={spacing.ml4}>
              Description
            </Text>
            <Tag
              className={spacing.w336}
              cursor="pointer"
              text="Description Text veeeeeeerrrrrrryyyyyyyyy loooooooooong deeeeeescriptiiiiiooon"
              color="grey-black"
              noWrap
              truncateText
            />
          </Container>
        </Container>
        <Container>
          <Container flexdirection="column" className={classNames(spacing.w150fixed, spacing.pX8)}>
            <Text type="subtext-bold" align="right" color="grey-90">
              Total USD Balance before revaluation
            </Text>
            <Text type="body-bold-14" align="right">
              22,000.00 $
            </Text>
          </Container>
          <Container flexdirection="column" className={classNames(spacing.w150fixed, spacing.pX8)}>
            <Text type="subtext-bold" align="right" color="grey-90">
              Total USD Balance after revaluation
            </Text>
            <Text type="body-bold-14" align="right">
              23,000.00 $
            </Text>
          </Container>
          <Container flexdirection="column" className={classNames(spacing.w150fixed, spacing.pX8)}>
            <Text type="subtext-bold" align="right" color="grey-90">
              Total Exchange Gain / (loss) USD
            </Text>
            <Text type="body-bold-14" align="right">
              1,000.00 $
            </Text>
          </Container>
        </Container>
      </Container>
      <Container
        background={'white-100'}
        flexgrow="1"
        className={spacing.mt24}
        borderRadius="20"
        flexdirection="column"
        overflow="hidden"
      >
        <Container
          className={classNames(spacing.mt16, spacing.mX24, spacing.h100p)}
          justifycontent="space-between"
          flexdirection="column"
        >
          <Container gap="24" alignitems="center">
            <FilterToggleButton
              showFilters={showFilters}
              filtersSelected={false}
              onClear={() => {}}
              onClick={() => {
                setShowFilters((prevState) => !prevState);
              }}
            />
            <Search
              width="320"
              height="40"
              name="create-fx-step2 "
              placeholder="Search by Account name "
            />
          </Container>
          <FiltersWrapper active={showFilters} margins="none">
            {createFXStep2Filters.map(({ title }) => (
              <FilterDropDown
                key={title}
                title={title}
                value="All"
                selectedAmount={0}
                onClear={() => {}}
              >
                <Container
                  className={classNames(spacing.m16, spacing.w288fixed)}
                  flexdirection="column"
                >
                  <FilterSearch title="Account type:" value={searchValue} onChange={handleChange} />
                  <FilterItemsContainer className={spacing.mt8}>
                    {options.map((item) => (
                      <FilterDropDownItem
                        key={item.value}
                        id={item.value}
                        type="item"
                        checked={false}
                        blurred={false}
                        value={item.label}
                        onChange={() => {}}
                      />
                    ))}
                  </FilterItemsContainer>
                </Container>
                <Divider fullHorizontalWidth />
                <Container
                  className={classNames(spacing.m16, spacing.w288fixed)}
                  justifycontent="flex-end"
                  alignitems="center"
                >
                  <Button
                    width="auto"
                    height="32"
                    padding="8"
                    font="text-medium"
                    onClick={() => {}}
                    background={'violet-90-violet-100'}
                    color="white-100"
                  >
                    Apply
                  </Button>
                </Container>
              </FilterDropDown>
            ))}
          </FiltersWrapper>
          <TableNew
            noResults={false}
            className={classNames(spacing.mt24, spacing.pr24, spacing.h100p)}
            tableHead={
              <TableHeadNew background={'grey-10'} radius="10">
                {createFXStep2Table.map((item, index) => (
                  <TableTitleNew
                    key={index}
                    minWidth={item.minWidth as TableCellWidth}
                    background={'grey-10'}
                    fixedWidth
                    align={item.align as TableTitleNewProps['align']}
                    title={item.title}
                    onClick={() => {}}
                  />
                ))}
              </TableHeadNew>
            }
          >
            <TableBodyNew>
              {Array.from(Array(15)).map((_, index) => (
                <TableRowNew key={index} background={'transparent-with-border'} size="36">
                  <TableCellNew
                    minWidth={createFXStep2Table[0].minWidth as TableCellWidth}
                    justifycontent="center"
                    alignitems="center"
                  >
                    <Checkbox name={`line-${index}`} checked onChange={() => {}} />
                  </TableCellNew>
                  <TableCellNew
                    minWidth={createFXStep2Table[1].minWidth as TableCellWidth}
                    justifycontent="space-between"
                  >
                    <Text>Receivables from PSP</Text>
                    <Text color="grey-100">Assets</Text>
                  </TableCellNew>
                  <TableCellTextNew
                    minWidth={createFXStep2Table[2].minWidth as TableCellWidth}
                    value="GTWH"
                    noWrap
                    lineClamp="none"
                    fixedWidth
                  />
                  <TableCellTextNew
                    minWidth={createFXStep2Table[3].minWidth as TableCellWidth}
                    value="Headway"
                    noWrap
                    lineClamp="none"
                    fixedWidth
                  />
                  <TableCellTextNew
                    minWidth={createFXStep2Table[4].minWidth as TableCellWidth}
                    value="UAH"
                  />
                  <TableCellNew
                    minWidth={createFXStep2Table[5].minWidth as TableCellWidth}
                    alignitems="center"
                  >
                    <Currencies
                      height="24"
                      selectedCurrencyValue="1"
                      selectedCurrency="eur"
                      baseCurrencyValue="1.01"
                      baseCurrency="usd"
                    />
                  </TableCellNew>
                  <TableCellTextNew
                    minWidth={createFXStep2Table[6].minWidth as TableCellWidth}
                    value="423,161.01 ₴"
                    alignitems="flex-end"
                  />
                  <TableCellTextNew
                    minWidth={createFXStep2Table[7].minWidth as TableCellWidth}
                    value="10,000.00 $"
                    alignitems="flex-end"
                  />
                  <TableCellTextNew
                    minWidth={createFXStep2Table[8].minWidth as TableCellWidth}
                    value="12,000.00 $"
                    alignitems="flex-end"
                  />
                  <TableCellTextNew
                    minWidth={createFXStep2Table[9].minWidth as TableCellWidth}
                    value="+2,000.00 $"
                    color="green-100"
                    alignitems="flex-end"
                  />
                </TableRowNew>
              ))}
              <ReportsTotalValues
                type="balance"
                title="SubTotal"
                data={[
                  ['UAH', '10,000.00 $', '12,000.00 $', '+2,000.00 $'],
                  ['EUR', '2000.00 €', '2000.00 €', '-1000.00 €'],
                ]}
              />
            </TableBodyNew>
          </TableNew>
          <Divider className={spacing.mt16} fullHorizontalWidth />
          <Container alignitems="center" className={spacing.pb16}>
            {!viewMode && (
              <Text color="grey-100" className={spacing.w200fixed}>
                Selected 0 of 2
              </Text>
            )}
            <Pagination
              borderTop="none"
              customPagination={
                <Container gap="16" alignitems="center">
                  <Text type="subtext-regular" color="grey-100">
                    Rows per Page
                  </Text>
                  <PerPageSelect
                    selectedValue={rowsPerPage}
                    onOptionClick={(value) => setRowsPerPage(value)}
                  />
                </Container>
              }
            />
          </Container>
        </Container>
      </Container>
    </Modal>
  );
};
