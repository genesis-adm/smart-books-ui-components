import { DropDown } from 'Components/DropDown';
import { Divider } from 'Components/base/Divider';
import { Tag } from 'Components/base/Tag';
import { Button } from 'Components/base/buttons/Button';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { SettingsContainer } from 'Components/base/grid/SettingsContainer';
import { MenuTab, MenuTabWrapper } from 'Components/base/tabs/MenuTab';
import { Text } from 'Components/base/typography/Text';
import { ModalSettings } from 'Components/custom/Settings/ModalSettings';
import { SettingsNavigation } from 'Components/custom/Settings/SettingsNavigation';
import { SettingsNavigationButton } from 'Components/custom/Settings/SettingsNavigationButton';
import { Pagination } from 'Components/custom/Stories/Pagination/Pagination';
import PerPageSelect from 'Components/custom/Stories/Pagination/PerPageSelect/PerPageSelect';
import { Logo } from 'Components/elements/Logo';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { TableCellWidth } from 'Components/types/gridTypes';
import { rowsPerPageOptions } from 'Mocks/fakeOptions';
import { mainPageTableHead } from 'Pages/GeneralSettings/mocks/CurrencyFXRevaluation';
import { navigationList } from 'Pages/GeneralSettings/mocks/currencyListMockData';
import { ReactComponent as EyeIcon } from 'Svg/v2/16/eye.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as DropdownIcon } from 'Svg/v2/16/three-dots.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/General Settings/SettingsCurrencyFXRevaluation',
};
export const SettingsCurrencyFXRevaluation: React.FC = () => {
  const [rowsPerPage, setRowsPerPage] = useState<string>(rowsPerPageOptions[0].value);
  return (
    <ModalSettings title="Organization settings" className={spacing.p20}>
      <Container className={spacing.mt20} flexgrow="1" overflow="y-auto">
        <SettingsNavigation className={spacing.mr20}>
          <Container
            className={spacing.w100p}
            flexdirection="column"
            alignitems="center"
            flexgrow="1"
          >
            <Logo content="company" size="lg" name="Genesis" color="#FF7052" />
            <Text className={spacing.mt20} type="body-medium" align="center" transform="uppercase">
              Genesis
            </Text>
            <Divider className={spacing.mY20} fullHorizontalWidth />
            {navigationList.map(({ title, selected }) => (
              <SettingsNavigationButton
                key={title}
                title={title}
                onClick={() => {}}
                selected={selected}
              />
            ))}
          </Container>
        </SettingsNavigation>
        <SettingsContainer>
          <Container justifycontent="space-between" alignitems="center">
            <MenuTabWrapper>
              <MenuTab id="currency" active={false} onClick={() => {}}>
                Currency
              </MenuTab>
              <MenuTab id="fx-revaluation" active onClick={() => {}}>
                FX revaluation
              </MenuTab>
            </MenuTabWrapper>
            <Button height="40" width="180" iconLeft={<PlusIcon />}>
              Create
            </Button>
          </Container>
          <TableNew
            noResults={false}
            className={classNames(spacing.mt24, spacing.pr24)}
            tableHead={
              <TableHeadNew>
                {mainPageTableHead.map((item, index) => (
                  <TableTitleNew
                    key={index}
                    minWidth={item.minWidth as TableCellWidth}
                    align={item.align as TableTitleNewProps['align']}
                    title={item.title}
                    onClick={() => {}}
                  />
                ))}
              </TableHeadNew>
            }
          >
            <TableBodyNew>
              {Array.from(Array(9)).map((_, index) => (
                <TableRowNew key={index} gap="8">
                  <TableCellTextNew
                    minWidth={mainPageTableHead[0].minWidth as TableCellWidth}
                    value={String(index + 1)}
                  />
                  <TableCellTextNew
                    minWidth={mainPageTableHead[1].minWidth as TableCellWidth}
                    value="Feb 22, 2023"
                  />
                  <TableCellTextNew
                    minWidth={mainPageTableHead[2].minWidth as TableCellWidth}
                    secondaryColor="grey-100"
                    value="Vitalii Kvasha"
                    secondaryValue="vitaliikvasha@gmail.com"
                    iconLeft={
                      <Logo content="user" radius="rounded" size="24" name="Vitalii Kvasha" />
                    }
                  />
                  <TableCellTextNew
                    minWidth={mainPageTableHead[3].minWidth as TableCellWidth}
                    secondaryColor="grey-100"
                    value="Feb 22, 2023"
                    secondaryValue="23:20:12"
                  />
                  <TableCellTextNew
                    minWidth={mainPageTableHead[4].minWidth as TableCellWidth}
                    value="12,000.00 $"
                    align="right"
                  />
                  <TableCellNew
                    minWidth={mainPageTableHead[5].minWidth as TableCellWidth}
                    justifycontent="center"
                  >
                    <DropDown
                      flexdirection="column"
                      padding="8"
                      control={({ handleOpen }) => (
                        <IconButton
                          icon={<DropdownIcon />}
                          onClick={handleOpen}
                          background={'transparent'}
                          color="grey-100-violet-90"
                        />
                      )}
                    >
                      <DropDownButton size="160" icon={<EyeIcon />} onClick={() => {}}>
                        Open
                      </DropDownButton>
                      <DropDownButton size="160" icon={<EyeIcon />} onClick={() => {}}>
                        Open Journal entry
                      </DropDownButton>
                      <Divider />
                      <DropDownButton
                        size="160"
                        type="danger"
                        icon={<TrashIcon />}
                        onClick={() => {}}
                      >
                        Delete currency
                      </DropDownButton>
                    </DropDown>
                  </TableCellNew>
                </TableRowNew>
              ))}
            </TableBodyNew>
          </TableNew>
          <Container alignitems="center">
            <Container alignitems="center" gap="8">
              <Text color="grey-100" type="text-regular">
                Functional currency:
              </Text>
              <Tag text="United State Dollar | USD | $" size="24" />
            </Container>
            <Pagination
              customPagination={
                <Container gap="16" alignitems="center">
                  <Text type="subtext-regular" color="grey-100">
                    Rows per Page
                  </Text>
                  <PerPageSelect
                    selectedValue={rowsPerPage}
                    onOptionClick={(value) => setRowsPerPage(value)}
                  />
                </Container>
              }
            />
          </Container>
        </SettingsContainer>
      </Container>
    </ModalSettings>
  );
};
