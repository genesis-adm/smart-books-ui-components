import { BusinessUnit } from 'Pages/GeneralSettings/mocks/setupBusinessUnits';
import { Dispatch, SetStateAction, createContext } from 'react';

export const ListContext = createContext<{
  buList: BusinessUnit[];
  setBuList: Dispatch<SetStateAction<BusinessUnit[]>>;
}>({ buList: [], setBuList: () => {} });
