import { Container } from 'Components/base/grid/Container';
import { TextArea } from 'Components/inputs/TextArea';
import { TextInput } from 'Components/inputs/TextInput';
import GeneralFormPlaceholder from 'Pages/GeneralSettings/LegalEntity/CreateLegalEntity/components/placeholders/GeneralFormPlaceholder';
import React, { useState } from 'react';

type GeneralFormProps = {
  toggleActive(active: boolean): void;
  isLoading: boolean;
};
const GeneralForm = ({ toggleActive, isLoading }: GeneralFormProps) => {
  const [notesValue, setNotesValue] = useState<string>('');
  const [leNameValue, setLeNameValue] = useState<string>('');
  const [taxIDValue, setTaxIDValue] = useState<string>('');

  if (isLoading) return <GeneralFormPlaceholder />;

  return (
    <>
      <Container gap="24">
        <TextInput
          name="leName"
          label={'Legal entity name'}
          value={leNameValue}
          onChange={(e) => {
            setLeNameValue(e.target.value);
            toggleActive(!!e.target.value);
          }}
          required
          width="300"
        />
        <TextInput
          name="taxID"
          label={'TAX ID'}
          value={taxIDValue}
          onChange={(e) => {
            setTaxIDValue(e.target.value);
          }}
          width="300"
        />
      </Container>
      <TextArea
        name="notes"
        label="Notes"
        value={notesValue}
        maxLength={200}
        counter
        onChange={(e) => {
          setNotesValue(e.target.value);
        }}
      />
    </>
  );
};

export default GeneralForm;
