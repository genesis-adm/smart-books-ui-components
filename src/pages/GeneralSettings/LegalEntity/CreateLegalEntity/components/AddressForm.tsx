import { Container } from 'Components/base/grid/Container';
import { TextInput } from 'Components/inputs/TextInput';
import React, { useState } from 'react';

const AddressForm = () => {
  const [countryValue, setCountryValue] = useState<string>('');
  const [stateValue, setStateValue] = useState<string>('');
  const [cityValue, setCityValue] = useState<string>('');
  const [zipValue, setZIPValue] = useState<string>('');
  const [streetValue, setStreetValue] = useState<string>('');
  return (
    <Container flexdirection="column" gap="24">
      <Container gap="24">
        <TextInput
          name="country"
          label="Country"
          value={stateValue}
          onChange={(e) => {
            setStateValue(e.target.value);
          }}
        />
        <TextInput
          name="sate"
          label="State"
          value={cityValue}
          onChange={(e) => {
            setCityValue(e.target.value);
          }}
        />
        <TextInput
          name="city"
          label="City"
          value={countryValue}
          onChange={(e) => {
            setCountryValue(e.target.value);
          }}
        />
      </Container>
      <Container gap="24">
        <TextInput
          name="zip"
          label="ZIP code"
          value={zipValue}
          width="192"
          onChange={(e) => {
            setZIPValue(e.target.value);
          }}
        />
        <TextInput
          name="street"
          label="Street"
          value={streetValue}
          width="408"
          onChange={(e) => {
            setStreetValue(e.target.value);
          }}
        />
      </Container>
    </Container>
  );
};

export default AddressForm;
