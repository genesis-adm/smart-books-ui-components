import { ContentLoader } from 'Components/base/ContentLoader';
import { Container } from 'Components/base/grid/Container';
import React from 'react';

const ContactsFormPlaceholder = () => {
  return (
    <Container flexdirection="column" gap="24">
      <ContentLoader isLoading height="48px" />
      <Container gap="24">
        <ContentLoader isLoading height="48px" />
        <ContentLoader isLoading height="48px" />
      </Container>
      <ContentLoader isLoading height="24px" width="228px" />
      <Container gap="24">
        <ContentLoader isLoading height="48px" />
        <ContentLoader isLoading height="48px" />
        <ContentLoader isLoading height="48px" />
      </Container>
      <Container gap="24">
        <ContentLoader isLoading height="48px" width="292px" />
        <ContentLoader isLoading height="48px" />
      </Container>
      <ContentLoader isLoading height="24px" width="228px" />
      <ContentLoader isLoading height="24px" width="228px" />
    </Container>
  );
};

export default ContactsFormPlaceholder;
