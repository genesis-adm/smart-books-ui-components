import { ContentLoader } from 'Components/base/ContentLoader';
import { Container } from 'Components/base/grid/Container';
import React from 'react';

const GeneralFormPlaceholder = () => {
  return (
    <Container flexdirection="column" gap="24">
      <Container gap="24">
        <ContentLoader isLoading width="300px" height="48px" />
        <ContentLoader isLoading width="300px" height="48px" />
      </Container>
      <ContentLoader isLoading height="106px" />
    </Container>
  );
};

export default GeneralFormPlaceholder;
