import { Checkbox } from 'Components/base/Checkbox';
import { Container } from 'Components/base/grid/Container';
import { InputPhone } from 'Components/base/inputs/InputPhone';
import { Text } from 'Components/base/typography/Text';
import { TextInput } from 'Components/inputs/TextInput';
import AddressForm from 'Pages/GeneralSettings/LegalEntity/CreateLegalEntity/components/AddressForm';
import ContactsFormPlaceholder from 'Pages/GeneralSettings/LegalEntity/CreateLegalEntity/components/placeholders/ContactsFormPlaceholder';
import React, { ReactElement, useState } from 'react';

const ContactsForm = ({ isLoading }: { isLoading: boolean }): ReactElement => {
  const [personName, setPersonName] = useState<string>('');
  const [phoneValue, setPhoneValue] = useState<string>('');
  const [emailValue, setEmailValue] = useState<string>('');

  const [checkAsBillingAddress, setCheckAsBillingAddress] = useState<boolean>(false);

  if (isLoading) return <ContactsFormPlaceholder />;

  return (
    <>
      <TextInput
        name="personName"
        label="Contact person name"
        value={personName}
        width="full"
        onChange={(e) => {
          setPersonName(e.target.value);
        }}
      />
      <Container gap="24">
        <InputPhone
          width="300"
          id="phone-id"
          name="phone-nmbr"
          value={phoneValue}
          label="Phone number"
          onChange={(e) => {
            setPhoneValue(e.phone);
          }}
          onBlur={() => {}}
        />
        <TextInput
          name="email"
          label="Email address"
          value={emailValue}
          width="300"
          onChange={(e) => {
            setEmailValue(e.target.value);
          }}
        />
      </Container>
      <Text type="body-regular-14">Billing address:</Text>
      <AddressForm />
      <Text type="body-regular-14">Shipping address:</Text>
      {!checkAsBillingAddress && <AddressForm />}
      <Checkbox
        font="body-regular-14"
        name="subaccount"
        checked={checkAsBillingAddress}
        onChange={() => {
          setCheckAsBillingAddress(!checkAsBillingAddress);
        }}
      >
        Same as billing address
      </Checkbox>
    </>
  );
};

export default ContactsForm;
