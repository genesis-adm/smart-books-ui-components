import { Meta, Story } from '@storybook/react';
import { ContentLoader } from 'Components/base/ContentLoader';
import { Tag } from 'Components/base/Tag';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { MenuTab, MenuTabWrapper } from 'Components/base/tabs/MenuTab';
import { Text } from 'Components/base/typography/Text';
import ModalHeaderPlaceholder from 'Components/custom/placeholders/ModalHeaderPlaceholder';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import ContactsForm from 'Pages/GeneralSettings/LegalEntity/CreateLegalEntity/components/ContactsForm';
import GeneralForm from 'Pages/GeneralSettings/LegalEntity/CreateLegalEntity/components/GeneralForm';
import { ReactComponent as ArrowRightIcon } from 'Svg/v2/16/arrow-right.svg';
import { ReactComponent as DocumentIcon } from 'Svg/v2/16/file-blank.svg';
import classNames from 'classnames';
import React, { ReactNode, useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

type Tabs = 'general' | 'contacts';

export const CreateLegalEntity: Story = ({ storyState, editMode }) => {
  const [tab, setTab] = useState<Tabs>('general');
  const [isActiveNextButton, setIsActiveNextButton] = useState<boolean>(false);
  const isLoading = storyState === 'loading';

  const tabContent: Record<Tabs, ReactNode> = {
    general: (
      <GeneralForm
        toggleActive={(active) => {
          setIsActiveNextButton(active);
        }}
        isLoading={isLoading}
      />
    ),
    contacts: <ContactsForm isLoading={isLoading} />,
  };

  const title = editMode ? 'Edit Admin' : 'Create new Legal entity';

  return (
    <Modal
      size="720"
      type="new"
      background={'grey-10'}
      header={
        isLoading ? (
          <ModalHeaderPlaceholder />
        ) : (
          <ModalHeaderNew
            title={title}
            tag={
              <Tag
                icon={<DocumentIcon />}
                padding="4-8"
                cursor="default"
                text="New"
                color="green"
              />
            }
            closeActionBackgroundColor="white-100"
            border
            background={'grey-10'}
            onClose={() => {}}
          />
        )
      }
      footer={
        <ModalFooterNew border background={'grey-10'}>
          <Container alignitems="center" gap="16" flexgrow="1" justifycontent="flex-end">
            <ContentLoader isLoading={isLoading} height="48px" width="200px">
              {!editMode && (
                <Container flexdirection="column">
                  <Text type="body-regular-14" color="grey-100">
                    Step: 1 of 2
                  </Text>
                  <Text type="body-regular-14" color="grey-100">
                    Next: Setup Business Units
                  </Text>
                </Container>
              )}
              {editMode ? (
                <Button
                  height="48"
                  width="134"
                  background={'violet-90-violet-100'}
                  onClick={() => {}}
                >
                  Update
                </Button>
              ) : (
                <Button
                  height="48"
                  navigation
                  iconRight={<ArrowRightIcon />}
                  width="150"
                  background={isActiveNextButton ? 'violet-90-violet-100' : 'grey-90'}
                  onClick={() => {}}
                >
                  Next
                </Button>
              )}
            </ContentLoader>
          </Container>
        </ModalFooterNew>
      }
    >
      <ContentLoader isLoading={isLoading} height="28px" width="228px">
        <MenuTabWrapper>
          <MenuTab
            id="general"
            active={tab === 'general'}
            onClick={() => {
              setTab('general');
            }}
          >
            General
          </MenuTab>
          <MenuTab
            id="contacts"
            active={tab === 'contacts'}
            onClick={() => {
              setTab('contacts');
            }}
          >
            Contacts
          </MenuTab>
        </MenuTabWrapper>
      </ContentLoader>
      <Container
        background={'white-100'}
        flexdirection="column"
        borderRadius="20"
        className={classNames(spacing.p24, spacing.mt12)}
        gap="24"
      >
        {tabContent[tab]}
      </Container>
    </Modal>
  );
};
export default {
  title: 'Pages/General Settings/Legal Entity',
  component: CreateLegalEntity,
  argTypes: {
    storyState: {
      options: ['empty', 'loading', 'loaded'],
      control: {
        type: 'radio',
        labels: {
          empty: 'Empty page',
          loading: 'Page is loading',
          loaded: 'Page loaded',
        },
      },
    },
    editMode: {
      control: { type: 'boolean' },
    },
  },
} as Meta;
