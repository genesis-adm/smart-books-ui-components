import { DropDown } from 'Components/DropDown';
import { Divider } from 'Components/base/Divider';
import { Button } from 'Components/base/buttons/Button';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import {
  TableRowExpandable,
  TableRowExpandableWrapper,
} from 'Components/elements/Table/TableRowExpandable';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { FilterDropDown } from 'Components/elements/filters/FilterDropDown';
import { FilterDropDownItem } from 'Components/elements/filters/FilterDropDownItem';
import { FilterItemsContainer } from 'Components/elements/filters/FilterItemsContainer';
import { FilterSearch } from 'Components/elements/filters/FilterSearch';
import { FiltersWrapper } from 'Components/elements/filters/FiltersWrapper';
import { Search } from 'Components/inputs/Search';
import { TableCellWidth } from 'Components/types/gridTypes';
import { options } from 'Mocks/fakeOptions';
import SetupBusinessUnitTableRowExample from 'Pages/GeneralSettings/LegalEntity/SetupBusinessUnits/components/SetupBusinessUnitTableRowExample';
import ChooseBUorLEPlaceholder from 'Pages/GeneralSettings/components/setupModalsplaceholders/ChooseBUorLEPlaceholder';
import {
  businessUnitsList,
  setupBusinessUnitsTableHead,
} from 'Pages/GeneralSettings/mocks/setupBusinessUnits';
import { ReactComponent as BuildingsIcon } from 'Svg/v2/16/buildings.svg';
import { ReactComponent as CaretDownIcon } from 'Svg/v2/16/caret-down.svg';
import { ReactComponent as DocumentIcon } from 'Svg/v2/16/document.svg';
import { ReactComponent as SmartphoneIcon } from 'Svg/v2/16/smartphone.svg';
import classNames from 'classnames';
import React, { useCallback, useState } from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';

type ChooseBusinessUnitsProps = {
  isLoading: boolean;
};
const ChooseBusinessUnits = ({ isLoading }: ChooseBusinessUnitsProps) => {
  const [searchValue, setSearchValue] = useState<string>();
  const [businessTypeSearchValue, setBusinessTypeSearchValue] = useState<string>();
  const [isActiveNested, setIsActiveNested] = useState(false);
  const [selected, setSelect] = useState<Array<number>>([]);

  const handleSelect = useCallback(
    (arg: number) => setSelect([...selected, arg]),
    [setSelect, selected],
  );

  const handleUnSelect = useCallback(
    (arg: number) => setSelect(selected?.filter((id) => arg !== id)),
    [setSelect, selected],
  );
  const handleToggle = (id: number) => () => {
    if (selected?.includes(id)) handleUnSelect(id);
    else handleSelect(id);
  };
  const handleToggleNested = () => setIsActiveNested((prevState) => !prevState);

  const handleChange = (value: string) => {
    setBusinessTypeSearchValue(value);
  };
  if (isLoading) return <ChooseBUorLEPlaceholder />;

  return (
    <Container
      background={'white-100'}
      flexdirection="column"
      borderRadius="20"
      flexgrow="4"
      className={classNames(spacing.p24, spacing.h100p)}
      customScroll
      gap="24"
    >
      <Text type="body-regular-14" color="grey-100" align="center">
        Please choose business units
      </Text>
      <Container flexdirection="column">
        <Container justifycontent="space-between">
          <Search
            name="search-bu"
            placeholder="Search business units"
            width="440"
            value={searchValue}
            onChange={(e) => setSearchValue(e.target.value)}
          />
          <DropDown
            flexdirection="column"
            padding="8"
            control={({ handleOpen }) => (
              <Button
                onClick={handleOpen}
                background={'violet-90-violet-100'}
                color="white-100"
                width="220"
                height="40"
                iconRight={<CaretDownIcon />}
              >
                Create Business unit
              </Button>
            )}
          >
            <DropDownButton size="204" onClick={() => {}} icon={<BuildingsIcon />}>
              Company
            </DropDownButton>
            <DropDownButton size="204" disabled onClick={() => {}} icon={<SmartphoneIcon />}>
              Project
            </DropDownButton>
            <DropDownButton size="204" disabled onClick={() => {}} icon={<DocumentIcon />}>
              Subproject
            </DropDownButton>
          </DropDown>
        </Container>
        <FiltersWrapper active margins="none">
          <FilterDropDown title="Business type" value="All" onClear={() => {}} selectedAmount={0}>
            <Container
              className={classNames(spacing.m16, spacing.w288fixed)}
              flexdirection="column"
            >
              <FilterSearch
                title="Business type:"
                value={businessTypeSearchValue || ''}
                onChange={handleChange}
              />
              <FilterItemsContainer className={spacing.mt8}>
                {options.map((item) => (
                  <FilterDropDownItem
                    key={item.value}
                    id={item.value}
                    type="item"
                    checked={false}
                    blurred={false}
                    value={item.label}
                    onChange={() => {}}
                  />
                ))}
              </FilterItemsContainer>
            </Container>
            <Divider fullHorizontalWidth />
            <Container
              className={classNames(spacing.m16, spacing.w288fixed)}
              justifycontent="flex-end"
              alignitems="center"
            >
              <Button
                width="auto"
                height="32"
                padding="8"
                font="text-medium"
                onClick={() => {}}
                background={'violet-90-violet-100'}
                color="white-100"
              >
                Apply
              </Button>
            </Container>
          </FilterDropDown>
        </FiltersWrapper>
      </Container>
      <TableNew
        noResults={false}
        className={classNames(spacing.pr24)}
        tableHead={
          <TableHeadNew>
            {setupBusinessUnitsTableHead.map((item, index) => (
              <TableTitleNew
                key={index}
                minWidth={item.minWidth as TableCellWidth}
                padding={item.padding as TableTitleNewProps['padding']}
                align={item.align as TableTitleNewProps['align']}
                title={item.title}
                onClick={() => {}}
              />
            ))}
          </TableHeadNew>
        }
      >
        <TableBodyNew>
          {businessUnitsList.map((item, index) => {
            if (item.rowType !== 'primary') return null;
            return (
              <TableRowExpandableWrapper isActive={selected.includes(item.id)} key={index}>
                <TableRowExpandable
                  type="primary"
                  isRowVisible
                  isToggleActive={selected.includes(item.id)}
                  onToggle={handleToggle(item.id)}
                >
                  <SetupBusinessUnitTableRowExample businessUnit={item} />
                </TableRowExpandable>
                {item.isParent && (
                  <>
                    <TableRowExpandable
                      isRowVisible={selected.includes(businessUnitsList[0].id)}
                      isToggleActive={isActiveNested}
                      onToggle={handleToggleNested}
                    >
                      <SetupBusinessUnitTableRowExample businessUnit={businessUnitsList[1]} />
                    </TableRowExpandable>
                    {isActiveNested && selected.includes(businessUnitsList[0].id) && (
                      <TableRowExpandable isRowVisible type="tertiary">
                        <SetupBusinessUnitTableRowExample businessUnit={businessUnitsList[2]} />
                      </TableRowExpandable>
                    )}
                  </>
                )}
              </TableRowExpandableWrapper>
            );
          })}
        </TableBodyNew>
      </TableNew>
    </Container>
  );
};

export default ChooseBusinessUnits;
