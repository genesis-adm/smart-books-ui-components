import { DropDown } from 'Components/DropDown';
import { Tag } from 'Components/base/Tag';
import { TagSegmented } from 'Components/base/TagSegmented';
import { Tooltip } from 'Components/base/Tooltip';
import { Button } from 'Components/base/buttons/Button';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { Logo } from 'Components/elements/Logo';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableCellWidth } from 'Components/types/gridTypes';
import { ListContext } from 'Pages/GeneralSettings/LegalEntity/CreateLegalEntity/context/ListContext';
import {
  BusinessUnit,
  disableUnlinkTooltipText,
  setupBusinessUnitsTableHead,
} from 'Pages/GeneralSettings/mocks/setupBusinessUnits';
import { ReactComponent as TrashIcon } from 'Svg/v2/12/delete.svg';
import { ReactComponent as InfoIcon } from 'Svg/v2/16/info.svg';
import classNames from 'classnames';
import React, { useContext } from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';

type SetupBusinessUnitTableRowExampleProps = {
  businessUnit: BusinessUnit;
};
const SetupBusinessUnitTableRowExample = ({
  businessUnit,
}: SetupBusinessUnitTableRowExampleProps) => {
  const { buList, setBuList } = useContext(ListContext);
  const handleAdd = () => {
    setBuList([...buList, businessUnit]);
  };

  const handleDelete = () => {
    setBuList(buList.filter((el) => el.id !== businessUnit.id));
  };
  return (
    <>
      <TableCellNew
        minWidth={setupBusinessUnitsTableHead[1].minWidth as TableCellWidth}
        justifycontent="center"
      >
        <Logo content="picture" size="44" name="Genesis Media & Mobile Application" />
      </TableCellNew>
      <TableCellTextNew
        minWidth={setupBusinessUnitsTableHead[2].minWidth as TableCellWidth}
        fixedWidth
        value={businessUnit.name}
      />
      <TableCellTextNew
        minWidth={setupBusinessUnitsTableHead[3].minWidth as TableCellWidth}
        value={businessUnit.type}
      />
      <TableCellNew minWidth={setupBusinessUnitsTableHead[4].minWidth as TableCellWidth} fixedWidth>
        <TagSegmented
          mainLabel={businessUnit.legalEntitiesName[0]}
          showCounter={businessUnit.legalEntitiesName.length > 1}
          counter={
            <DropDown
              flexdirection="column"
              padding="8"
              classnameInner={classNames(spacing.h300)}
              customScroll
              control={({ handleOpen }) => (
                <Tooltip message={'Show All'}>
                  <TagSegmented.Counter
                    counterValue={businessUnit.legalEntitiesName.length}
                    onClick={handleOpen}
                  />
                </Tooltip>
              )}
            >
              <Container flexdirection="column" gap="12">
                <Text type="button" color="grey-100">
                  Legal entities
                </Text>
                {businessUnit.legalEntitiesName.map((le) => (
                  <Text key={le} type="body-regular-14">
                    {le}
                  </Text>
                ))}
              </Container>
            </DropDown>
          }
        />
      </TableCellNew>
      <TableCellNew minWidth={setupBusinessUnitsTableHead[5].minWidth as TableCellWidth}>
        {buList.find((el) => el.id === businessUnit.id) ? (
          <Container gap="12">
            <Tag text="Added" size="24" cursor="default" font="caption-regular" />
            {businessUnit.disableUnlink ? (
              <IconButton
                icon={<InfoIcon />}
                size="24"
                color="grey-100-black-100"
                background={'grey-20'}
                tooltip={disableUnlinkTooltipText}
              />
            ) : (
              <IconButton
                icon={<TrashIcon />}
                size="24"
                color="grey-90-red-90"
                background={'grey-20-red-10'}
                tooltip="Delete"
                onClick={handleDelete}
              />
            )}
          </Container>
        ) : (
          <Button
            width="50"
            height="24"
            font="text-medium"
            background={'blue-10-blue-20'}
            color="violet-90-violet-100"
            onClick={handleAdd}
          >
            Add
          </Button>
        )}
      </TableCellNew>
    </>
  );
};

export default SetupBusinessUnitTableRowExample;
