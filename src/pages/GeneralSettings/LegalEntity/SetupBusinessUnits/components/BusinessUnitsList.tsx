import { Tag } from 'Components/base/Tag';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { MainPageEmpty } from 'Components/custom/MainPage/MainPageEmpty';
import { ListContext } from 'Pages/GeneralSettings/LegalEntity/CreateLegalEntity/context/ListContext';
import ListPlaceholder from 'Pages/GeneralSettings/components/setupModalsplaceholders/ListPlaceholder';
import {
  BusinessUnit,
  disableUnlinkTooltipText,
} from 'Pages/GeneralSettings/mocks/setupBusinessUnits';
import { ReactComponent as InfoIcon } from 'Svg/v2/16/info.svg';
import classNames from 'classnames';
import React, { useContext } from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';

type BusinessUnitsListProps = {
  isLoading: boolean;
};
const BusinessUnitsList = ({ isLoading }: BusinessUnitsListProps) => {
  const { buList, setBuList } = useContext(ListContext);
  const handleDelete = (item: BusinessUnit) => {
    setBuList(buList.filter((el) => el.id !== item.id));
  };

  if (isLoading) return <ListPlaceholder />;

  return (
    <Container
      background={'white-100'}
      flexdirection="column"
      borderRadius="20"
      className={classNames(spacing.p24, spacing.h100p, spacing.w404fixed)}
      gap="24"
    >
      <Text type="body-regular-14" color="grey-100" align="center">
        Business units list
      </Text>
      {!buList?.length && <MainPageEmpty description="Please choose business units" />}
      {!!buList.length && (
        <Container flexwrap="wrap" gap="16">
          {buList.map((item) => (
            <Tag
              key={item.id}
              text={item.name}
              deleteFlow={!item.disableUnlink}
              truncateText
              icon={item.disableUnlink && <InfoIcon />}
              iconPosition="right"
              iconTooltip={item.disableUnlink && disableUnlinkTooltipText}
              onDelete={() => handleDelete(item)}
            />
          ))}
        </Container>
      )}
    </Container>
  );
};

export default BusinessUnitsList;
