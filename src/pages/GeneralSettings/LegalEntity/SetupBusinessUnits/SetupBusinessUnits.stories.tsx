import { Meta, Story } from '@storybook/react';
import { Tag } from 'Components/base/Tag';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { Pagination } from 'Components/custom/Stories/Pagination/Pagination';
import PerPageSelect from 'Components/custom/Stories/Pagination/PerPageSelect/PerPageSelect';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { rowsPerPageOptions } from 'Mocks/fakeOptions';
import BusinessUnitsList from 'Pages/GeneralSettings/LegalEntity/SetupBusinessUnits/components/BusinessUnitsList';
import ChooseBusinessUnits from 'Pages/GeneralSettings/LegalEntity/SetupBusinessUnits/components/ChooseBusinessUnits';
import SetupFooterPlaceholder from 'Pages/GeneralSettings/components/setupModalsplaceholders/SetupFooterPlaceholder';
import SetupModalHeaderPlaceholder from 'Pages/GeneralSettings/components/setupModalsplaceholders/SetupModalHeaderPlaceholder';
import { BusinessUnit } from 'Pages/GeneralSettings/mocks/setupBusinessUnits';
import { ReactComponent as DocumentIcon } from 'Svg/v2/16/file-blank.svg';
import React, { useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';
import { ListContext } from '../CreateLegalEntity/context/ListContext';

export const SetupBusinessUnits: Story = ({ storyState }) => {
  const [rowsPerPage, setRowsPerPage] = useState<string>(rowsPerPageOptions[0].value);
  const [buList, setBuList] = useState<Array<BusinessUnit>>([]);

  const isLoading = storyState === 'loading';
  return (
    <Modal
      size="1392"
      centered
      background={'grey-10'}
      pluginScrollDisabled
      header={
        isLoading ? (
          <SetupModalHeaderPlaceholder />
        ) : (
          <ModalHeaderNew
            title="Setup business units to Admin"
            titlePosition="center"
            tag={
              <Tag
                icon={<DocumentIcon />}
                padding="4-8"
                cursor="default"
                text="New"
                color="green"
              />
            }
            closeActionBackgroundColor="white-100"
            border
            background={'grey-10'}
            onClose={() => {}}
          />
        )
      }
      footer={
        <ModalFooterNew border background={'grey-10'}>
          {isLoading ? (
            <SetupFooterPlaceholder />
          ) : (
            <>
              <Container alignitems="center" gap="16" justifycontent="flex-end">
                <Container flexdirection="column">
                  <Text type="body-regular-14" color="grey-100">
                    Step: 2 of 2
                  </Text>
                  <Text
                    type="body-regular-14"
                    color="grey-100"
                    lineClamp="none"
                    className={spacing.w140fixed}
                  >
                    Setup Business Units
                  </Text>
                </Container>
                <Button
                  height="48"
                  navigation
                  width="md"
                  background={!!buList.length ? 'violet-90-violet-100' : 'grey-90'}
                  onClick={() => {}}
                >
                  Create
                </Button>
              </Container>
              <Pagination
                borderTop="none"
                customPagination={
                  <Container gap="16" alignitems="center">
                    <Text type="subtext-regular" color="grey-100">
                      Rows per Page
                    </Text>
                    <PerPageSelect
                      tagColor="white"
                      selectedValue={rowsPerPage}
                      onOptionClick={(value) => setRowsPerPage(value)}
                    />
                  </Container>
                }
                elementsBackground="white-100"
              />
            </>
          )}
        </ModalFooterNew>
      }
    >
      <ListContext.Provider value={{ buList: buList, setBuList: setBuList }}>
        <Container gap="24" className={spacing.h100p} customScroll>
          <ChooseBusinessUnits isLoading={isLoading} />
          <BusinessUnitsList isLoading={isLoading} />
        </Container>
      </ListContext.Provider>
    </Modal>
  );
};

export default {
  title: 'Pages/General Settings/Legal Entity',
  component: SetupBusinessUnits,
  argTypes: {
    storyState: {
      options: ['empty', 'loading', 'loaded'],
      control: {
        type: 'radio',
        labels: {
          empty: 'Empty page',
          loading: 'Page is loading',
          loaded: 'Page loaded',
        },
      },
    },
  },
} as Meta;
