import { Tag } from 'Components/base/Tag';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import ViewBusinessUnitOrLegalEntityPlaceholder from 'Pages/GeneralSettings/components/ViewBusinessUnitOrLegalEntityPlaceholder';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';
import IconButton from '../../../../../components/base/buttons/IconButton/IconButton';

const BusinessUnits = ({ isLoading }: { isLoading: boolean }) => {
  if (isLoading) return <ViewBusinessUnitOrLegalEntityPlaceholder />;

  return (
    <Container
      background={'white-100'}
      flexdirection="column"
      borderRadius="20"
      className={classNames(spacing.p24, spacing.mt12)}
      gap="24"
    >
      <Container justifycontent="space-between">
        <Text type="body-regular-14">Business Units</Text>
        <IconButton
          icon={<EditIcon />}
          color="grey-100-yellow-90"
          background={'grey-10-yellow-10'}
          size="24"
          tooltip="Edit Business Units"
        />
      </Container>
      <Container gap="16" flexwrap="wrap">
        <Tag text="SmartBooks" font="body-regular-14" cursor="default" />
      </Container>
    </Container>
  );
};

export default BusinessUnits;
