import Icon from 'Components/base/Icon/Icon';
import { Container } from 'Components/base/grid/Container';
import { GeneralTab, GeneralTabWrapper } from 'Components/base/tabs/GeneralTab';
import { Text } from 'Components/base/typography/Text';
import GeneralInfoPlaceholder from 'Pages/GeneralSettings/LegalEntity/ViewLegalEntity/components/placeholders/GeneralInfoPlaceholder';
import {
  addressInfo,
  contactsInfo,
  generalInfoFields,
} from 'Pages/GeneralSettings/LegalEntity/ViewLegalEntity/constants';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';
import IconButton from '../../../../../components/base/buttons/IconButton/IconButton';

const GeneralInfo = ({ isLoading }: { isLoading: boolean }) => {
  if (isLoading) return <GeneralInfoPlaceholder />;

  return (
    <Container
      background={'white-100'}
      flexdirection="column"
      borderRadius="20"
      className={classNames(spacing.p24, spacing.mt12)}
      gap="24"
    >
      <Container justifycontent="space-between">
        <Text type="body-regular-14">General information</Text>
        <IconButton
          icon={<EditIcon />}
          color="grey-100-yellow-90"
          background={'grey-10-yellow-10'}
          size="24"
          tooltip="Edit General information"
        />
      </Container>
      <Container flexdirection="column" gap="20">
        {generalInfoFields.map(({ name, value }) => (
          <Container key={name} alignitems="center">
            <Text className={spacing.w150min} type="caption-regular" color="grey-100">
              {name}
            </Text>
            <Text type="body-regular-14">{value}</Text>
          </Container>
        ))}
      </Container>
      <GeneralTabWrapper>
        <GeneralTab id="forReview" onClick={() => {}} active>
          Billing address
        </GeneralTab>
        <GeneralTab id="forReview" onClick={() => {}} active={false}>
          Shipping address
        </GeneralTab>
      </GeneralTabWrapper>
      <Container flexdirection="column" gap="20">
        {addressInfo.map(({ name, value }) => (
          <Container key={name} alignitems="center">
            <Text className={spacing.w150min} type="caption-regular" color="grey-100">
              {name}
            </Text>
            <Text type="body-regular-14">{value}</Text>
          </Container>
        ))}
      </Container>
      <Text type="body-regular-14">Contacts</Text>
      <Container flexdirection="column" gap="20">
        {contactsInfo.map(({ name, value, icon }) => (
          <Container key={name} alignitems="center">
            <Container gap="4" className={spacing.w180min}>
              <Icon icon={icon} path="grey-100" />
              <Text type="caption-regular" color="grey-100">
                {name}
              </Text>
            </Container>
            <Text type="body-regular-14">{value}</Text>
          </Container>
        ))}
      </Container>
    </Container>
  );
};

export default GeneralInfo;
