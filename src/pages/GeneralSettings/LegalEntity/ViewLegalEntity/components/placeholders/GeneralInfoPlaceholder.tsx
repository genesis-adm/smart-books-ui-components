import { ContentLoader } from 'Components/base/ContentLoader';
import { Container } from 'Components/base/grid/Container';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../../../../assets/styles/spacing.module.scss';

const GeneralInfoPlaceholder = () => (
  <Container
    background={'white-100'}
    flexdirection="column"
    borderRadius="20"
    className={classNames(spacing.p24, spacing.mt12)}
    gap="24"
  >
    <Container justifycontent="space-between">
      <ContentLoader isLoading height="24px" width="96px" />
      <ContentLoader isLoading height="24px" width="24px" />
    </Container>
    <Container flexdirection="column" gap="12">
      {Array.from(Array(3)).map((_, index) => (
        <ContentLoader key={index} isLoading height="16px" width="200px" />
      ))}
    </Container>
    <ContentLoader isLoading width="249px" />
    <Container flexdirection="column" gap="12">
      {Array.from(Array(5)).map((_, index) => (
        <ContentLoader key={index} isLoading height="16px" width="200px" />
      ))}
    </Container>
    <ContentLoader isLoading height="24px" width="96px" />
    <Container flexdirection="column" gap="12">
      {Array.from(Array(3)).map((_, index) => (
        <ContentLoader key={index} isLoading height="16px" width="200px" />
      ))}
    </Container>
  </Container>
);

export default GeneralInfoPlaceholder;
