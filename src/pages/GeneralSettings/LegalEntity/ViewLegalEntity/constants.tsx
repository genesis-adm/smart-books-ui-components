import { ReactComponent as EnvelopeIcon } from 'Svg/v2/16/envelope-filled.svg';
import { ReactComponent as PhoneIcon } from 'Svg/v2/16/phone.svg';
import { ReactComponent as UserIcon } from 'Svg/v2/16/user-rounded.svg';
import React from 'react';

export const generalInfoFields = [
  { name: 'Legal entity name*', value: 'Admin' },
  { name: 'TAX ID', value: '-' },
  { name: 'Notes', value: '-' },
];

export const addressInfo = [
  { name: 'Country', value: 'USA' },
  { name: 'State', value: 'Florida' },
  { name: 'City', value: 'Orlando' },
  { name: 'ZIP code', value: '00-728' },
  { name: 'Street', value: 'International Drive 32' },
];

export const contactsInfo = [
  { name: 'Contact person name', value: 'Vitaliy Kvasha', icon: <UserIcon /> },
  { name: 'Phone number', value: '(555) 555-1234', icon: <PhoneIcon /> },
  { name: 'Email address', value: 'vitaliy.kvasha@gen.tech', icon: <EnvelopeIcon /> },
];
