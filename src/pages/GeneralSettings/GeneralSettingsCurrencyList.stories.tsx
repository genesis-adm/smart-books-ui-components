import { DropDown } from 'Components/DropDown';
import { Checkbox } from 'Components/base/Checkbox';
import { DatePicker } from 'Components/base/DatePicker';
import { Divider } from 'Components/base/Divider';
import { Tag } from 'Components/base/Tag';
import { Button } from 'Components/base/buttons/Button';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { SettingsContainer } from 'Components/base/grid/SettingsContainer';
import { GeneralTab, GeneralTabWrapper } from 'Components/base/tabs/GeneralTab';
import { MenuTab, MenuTabWrapper } from 'Components/base/tabs/MenuTab';
import { Text } from 'Components/base/typography/Text';
import { ModalSettings } from 'Components/custom/Settings/ModalSettings';
import { SettingsNavigation } from 'Components/custom/Settings/SettingsNavigation';
import { SettingsNavigationButton } from 'Components/custom/Settings/SettingsNavigationButton';
import { Pagination } from 'Components/custom/Stories/Pagination/Pagination';
import PerPageSelect from 'Components/custom/Stories/Pagination/PerPageSelect/PerPageSelect';
import { Logo } from 'Components/elements/Logo';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { FilterDropDown } from 'Components/elements/filters/FilterDropDown';
import { FilterDropDownItem } from 'Components/elements/filters/FilterDropDownItem';
import { FilterItemsContainer } from 'Components/elements/filters/FilterItemsContainer';
import { FilterSearch } from 'Components/elements/filters/FilterSearch';
import { FilterToggleButton } from 'Components/elements/filters/FilterToggleButton';
import { FiltersWrapper } from 'Components/elements/filters/FiltersWrapper';
import { Search } from 'Components/inputs/Search';
import { TableCellWidth } from 'Components/types/gridTypes';
import { rowsPerPageOptions } from 'Mocks/fakeOptions';
import { CustomColumnType } from 'Mocks/purchasesFakeData';
import {
  filterItems,
  historyTableHead,
  navigationList,
  todayTableHead,
} from 'Pages/GeneralSettings/mocks/currencyListMockData';
import { ReactComponent as CalendarIcon } from 'Svg/v2/16/calendar-filled.svg';
import { ReactComponent as HistoryIcon } from 'Svg/v2/16/clock-filled.svg';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import { ReactComponent as PlusIcon } from 'Svg/v2/16/plus.svg';
import { ReactComponent as DropdownIcon } from 'Svg/v2/16/three-dots.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/General Settings',
};

export const SettingsCurrencyList: React.FC = () => {
  const [showFilters, setShowFilters] = useState<boolean>(false);
  const [activeTab, setActiveTab] = useState<string>('today');
  const [checkedRows, setCheckedRows] = useState<number[]>([]);
  const [searchValue, setSearchValue] = useState('');
  const [checkedFilterByName, setCheckedFilterByName] = useState<CustomColumnType[]>([]);
  const [rowsPerPage, setRowsPerPage] = useState<string>(rowsPerPageOptions[0].value);

  const handleCheckRow = (rowIndex: number) => {
    setCheckedRows((prevState) => {
      return prevState.some((prev) => prev === rowIndex)
        ? prevState.filter((item) => item !== rowIndex)
        : [...prevState, rowIndex];
    });
  };
  const handleCheckFilterByName = (col: CustomColumnType) => () => {
    setCheckedFilterByName((prevState) => {
      return prevState.some((prev) => prev.id.includes(col.id))
        ? prevState.filter((item) => item.id !== col.id)
        : [...prevState, col];
    });
  };
  const handleChange = (value: string) => {
    setSearchValue(value);
  };

  const filters = [
    {
      title: 'Date',
      component: (
        <>
          <DatePicker name="date" onChange={() => {}} selected={new Date()} />
          <Divider fullHorizontalWidth />
          <Container justifycontent="flex-end" className={spacing.m16}>
            <Button width="108" height="32" onClick={() => {}}>
              Apply
            </Button>
          </Container>
        </>
      ),
    },
    {
      title: 'Currency name',
      component: (
        <>
          <Container className={classNames(spacing.m16, spacing.w288fixed)} flexdirection="column">
            <FilterSearch title="Account type:" value={searchValue} onChange={handleChange} />
            <FilterItemsContainer className={spacing.mt8}>
              {filterItems.map((item) => (
                <FilterDropDownItem
                  key={item.id}
                  id={item.id}
                  type="item"
                  checked={checkedFilterByName.some((filter) => filter.id === item.id)}
                  blurred={false}
                  value={item.title}
                  onChange={handleCheckFilterByName({
                    id: item.id,
                    title: item.title,
                  })}
                />
              ))}
            </FilterItemsContainer>
          </Container>
          <Divider fullHorizontalWidth />
          <Container
            className={classNames(spacing.m16, spacing.w288fixed)}
            justifycontent="flex-end"
            alignitems="center"
          >
            <Button
              width="auto"
              height="32"
              padding="8"
              font="text-medium"
              onClick={() => {}}
              background={'violet-90-violet-100'}
              color="white-100"
            >
              Apply
            </Button>
          </Container>
        </>
      ),
    },
  ];
  const RowExample = ({ index }: { index: number }) => (
    <>
      {activeTab === 'history' && (
        <>
          <TableCellNew minWidth="48" justifycontent="center" alignitems="center">
            <Checkbox
              name="historyCurrency"
              checked={checkedRows.some((item) => item === index)}
              onChange={() => {
                handleCheckRow(index);
              }}
            />
          </TableCellNew>
          <TableCellTextNew
            minWidth={historyTableHead[1].minWidth as TableCellWidth}
            value="Feb 22, 2023"
          />
        </>
      )}
      <TableCellTextNew
        minWidth={
          activeTab === 'today'
            ? (todayTableHead[0].minWidth as TableCellWidth)
            : (historyTableHead[2].minWidth as TableCellWidth)
        }
        value="Euro"
      />
      <TableCellNew
        minWidth={
          activeTab === 'today'
            ? (todayTableHead[1].minWidth as TableCellWidth)
            : (historyTableHead[3].minWidth as TableCellWidth)
        }
        justifycontent="center"
        flexgrow="1"
      >
        <Tag text="EUR | € " size="24" />
      </TableCellNew>
      <TableCellTextNew
        minWidth={
          activeTab === 'today'
            ? (todayTableHead[2].minWidth as TableCellWidth)
            : (historyTableHead[4].minWidth as TableCellWidth)
        }
        value="1.234353"
        flexgrow="1"
      />
      {activeTab === 'today' && (
        <TableCellTextNew
          minWidth={todayTableHead[3].minWidth as TableCellWidth}
          value="Feb 22, 2023"
        />
      )}
      <TableCellNew
        minWidth={
          activeTab === 'today'
            ? (todayTableHead[4].minWidth as TableCellWidth)
            : (historyTableHead[5].minWidth as TableCellWidth)
        }
        justifycontent="center"
      >
        <DropDown
          flexdirection="column"
          padding="8"
          control={({ handleOpen }) => (
            <IconButton
              icon={<DropdownIcon />}
              onClick={handleOpen}
              background={'transparent'}
              color="grey-100-violet-90"
            />
          )}
        >
          <DropDownButton size="160" icon={<EditIcon />} onClick={() => {}}>
            Edit currency exchange
          </DropDownButton>
          <Divider />
          <DropDownButton size="160" type="danger" icon={<TrashIcon />} onClick={() => {}}>
            Delete currency
          </DropDownButton>
        </DropDown>
      </TableCellNew>
    </>
  );
  return (
    <ModalSettings title="Organization settings" className={spacing.p20}>
      <Container className={spacing.mt20} flexgrow="1" overflow="y-auto">
        <SettingsNavigation className={spacing.mr20}>
          <Container
            className={spacing.w100p}
            flexdirection="column"
            alignitems="center"
            flexgrow="1"
          >
            <Logo content="company" size="lg" name="Genesis" color="#FF7052" />
            <Text className={spacing.mt20} type="body-medium" align="center" transform="uppercase">
              Genesis
            </Text>
            <Divider className={spacing.mY20} fullHorizontalWidth />
            {navigationList.map(({ title, selected }) => (
              <SettingsNavigationButton
                key={title}
                title={title}
                onClick={() => {}}
                selected={selected}
              />
            ))}
          </Container>
        </SettingsNavigation>
        <SettingsContainer>
          <Container justifycontent="space-between" alignitems="center">
            <MenuTabWrapper>
              <MenuTab id="currency" active onClick={() => {}}>
                Currency
              </MenuTab>
              <MenuTab id="fx-revaluation" active={false} onClick={() => {}}>
                FX revaluation
              </MenuTab>
            </MenuTabWrapper>
            <Button height="40" width="180" iconLeft={<PlusIcon />}>
              Add currency
            </Button>
          </Container>
          <Container className={classNames(spacing.mt24)} justifycontent="space-between" gap="24">
            <Container gap="24" alignitems="center">
              <FilterToggleButton
                showFilters={showFilters}
                filtersSelected={false}
                onClear={() => {}}
                onClick={() => {
                  setShowFilters((prevState) => !prevState);
                }}
              />
              <Search
                width="320"
                height="40"
                name="search-sales-products-mainPage"
                placeholder="Search by ID and description"
                value=""
                onChange={() => {}}
                searchParams={[]}
                onSearch={() => {}}
                onClear={() => {}}
              />
            </Container>
          </Container>
          <FiltersWrapper active={showFilters} margins="none">
            {filters.map((item) => (
              <FilterDropDown
                key={item.title}
                title={item.title}
                value={item.title}
                onClear={() => {}}
                selectedAmount={0}
              >
                {item.component}
              </FilterDropDown>
            ))}
          </FiltersWrapper>
          <GeneralTabWrapper className={classNames(spacing.mt24)}>
            <GeneralTab
              id="today"
              icon={<CalendarIcon />}
              onClick={() => {
                setActiveTab('today');
              }}
              active={activeTab === 'today'}
            >
              Today
            </GeneralTab>
            <GeneralTab
              id="history"
              icon={<HistoryIcon />}
              onClick={() => {
                setActiveTab('history');
              }}
              active={activeTab === 'history'}
            >
              History
            </GeneralTab>
          </GeneralTabWrapper>
          {activeTab === 'today' && (
            <TableNew
              noResults={false}
              className={classNames(spacing.mt24, spacing.pr24)}
              tableHead={
                <TableHeadNew>
                  {todayTableHead.map((item, index) => (
                    <TableTitleNew
                      key={index}
                      minWidth={item.minWidth as TableCellWidth}
                      flexgrow={item.flexgrow as TableTitleNewProps['flexgrow']}
                      align={item.align as TableTitleNewProps['align']}
                      title={item.title}
                      sorting={item.sorting}
                      onClick={() => {}}
                    />
                  ))}
                </TableHeadNew>
              }
            >
              <TableBodyNew>
                {Array.from(Array(9)).map((_, index) => (
                  <TableRowNew key={index} gap="8">
                    <RowExample index={index} />
                  </TableRowNew>
                ))}
              </TableBodyNew>
            </TableNew>
          )}
          {activeTab === 'history' && (
            <TableNew
              noResults={false}
              className={classNames(spacing.mt24, spacing.pr24)}
              tableHead={
                <TableHeadNew>
                  {historyTableHead.map((item, index) => (
                    <TableTitleNew
                      key={index}
                      minWidth={item.minWidth as TableCellWidth}
                      flexgrow={item.flexgrow as TableTitleNewProps['flexgrow']}
                      align={item.align as TableTitleNewProps['align']}
                      title={item.title}
                      sorting={item.sorting}
                      onClick={() => {}}
                    />
                  ))}
                </TableHeadNew>
              }
            >
              <TableBodyNew>
                {Array.from(Array(9)).map((_, index) => (
                  <TableRowNew key={index} gap="8">
                    <RowExample index={index} />
                  </TableRowNew>
                ))}
              </TableBodyNew>
            </TableNew>
          )}
          <Container>
            <Container alignitems="center" gap="8">
              <Text color="grey-100" type="text-regular">
                Functional currency:
              </Text>
              <Tag text="United State Dollar | USD | $" size="24" />
            </Container>
            <Pagination
              customPagination={
                <Container gap="16" alignitems="center">
                  <Text type="subtext-regular" color="grey-100">
                    Rows per Page
                  </Text>
                  <PerPageSelect
                    selectedValue={rowsPerPage}
                    onOptionClick={(value) => setRowsPerPage(value)}
                  />
                </Container>
              }
            />
          </Container>
        </SettingsContainer>
      </Container>
    </ModalSettings>
  );
};
