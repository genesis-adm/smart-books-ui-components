import { IconButton } from 'Components/base/buttons/IconButton';
import { TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { ReactComponent as ChevronIcon } from 'Svg/v2/16/chevron-up.svg';
import React from 'react';

export const tableHead: TableTitleNewProps[] = [
  {
    minWidth: '48',
    title: (
      <IconButton
        size="24"
        background={'transparent'}
        border="grey-20"
        color="grey-100"
        icon={<ChevronIcon />}
        onClick={() => {}}
      />
    ),
    padding: 'none',
  },
  {
    minWidth: '150',
    title: 'Logo',
    align: 'center',
  },
  {
    minWidth: '220',
    title: 'Name',
  },
  {
    minWidth: '230',
    title: 'Business type',
  },
  {
    minWidth: '360',
    title: 'Legal entities',
  },
  {
    minWidth: '110',
    title: 'Actions',
    align: 'center',
  },
];
