import { Checkbox } from 'Components/base/Checkbox';
import React from 'react';

export const mainPageTableHead = [
  {
    minWidth: '60',
    title: 'ID',
  },
  {
    minWidth: '200',
    title: 'Revaluate date',
  },
  {
    minWidth: '310',
    title: 'Create by',
  },
  {
    minWidth: '200',
    title: 'Create Date',
  },
  {
    minWidth: '230',
    title: 'Exchange Gain / (loss) USD',
    align: 'right',
  },
  {
    minWidth: '120',
    title: 'Actions',
    align: 'center',
  },
];

export const createFXStep1Table = [
  {
    minWidth: '410',
    title: 'Currency',
  },
  {
    minWidth: '410',
    title: 'FX rate',
  },
];

export const createFXStep2Filters = [
  { title: 'Account name' },
  { title: 'Legal Entity' },
  { title: 'Business unit' },
  { title: 'Currency' },
];

export const createFXStep2Table = [
  {
    minWidth: '48',
    title: <Checkbox name="tickAll" checked onChange={() => {}} />,
    padding: 'none',
  },
  {
    minWidth: '255',
    title: 'Account name',
  },
  {
    minWidth: '125',
    title: 'Legal Entity',
  },
  {
    minWidth: '125',
    title: 'Business Unit',
  },
  {
    minWidth: '90',
    title: 'Currency',
  },
  {
    minWidth: '200',
    title: 'FX rate',
    align: 'center',
  },
  {
    minWidth: '135',
    title: 'Account Balance',
    align: 'right',
  },
  {
    minWidth: '145',
    title: 'USD Balance before revaluation',
    align: 'right',
  },
  {
    minWidth: '145',
    title: 'USD Balance after revaluation',
    align: 'right',
  },
  {
    minWidth: '145',
    title: 'Exchange gain / loss USD',
    align: 'right',
  },
];
