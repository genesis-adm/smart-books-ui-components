export type LegalEntity = {
  id: number;
  name: string;
  businessUnits: string[];
  taxId: string;
  address: string;
  disableUnlink: boolean;
};

export const setupLegalEntitiesTableHead = [
  { minWidth: '210', title: 'Legal entity name' },
  { minWidth: '200', title: 'Business units' },
  { minWidth: '100', title: 'TAX ID' },
  { minWidth: '205', title: 'Address' },
  { minWidth: '130', title: 'Actions', align: 'center' },
];

export const legalEntitiesList: LegalEntity[] = [
  {
    id: 1,
    name: 'DM dev',
    businessUnits: ['Business unit 1', 'Business unit 2', 'Business unit 3'],
    taxId: 'zxcvbnc',
    address: '125, Georgiou Griva Digeni, Limassol, 3101, Cyprus',
    disableUnlink: false,
  },
  {
    id: 2,
    name: 'Matar and Trade company',
    businessUnits: ['Business unit 1', 'Business unit 2', 'Business unit 3', 'Business unit 4'],
    taxId: 'zxcvbnc',
    address: '125, Georgiou Griva Digeni, Limassol, 3101, Cyprus',
    disableUnlink: false,
  },
  {
    id: 3,
    name: 'Veeeeeeeeryyyyyyy Loooooooooong leeeeegaaaallll entity naame',
    businessUnits: ['Business unit 1'],
    taxId: 'zxcvbnc',
    address: '125, Georgiou Griva Digeni, Limassol, 3101, Cyprus',
    disableUnlink: false,
  },
  {
    id: 4,
    name: 'LEGAL ENTITY 4',
    businessUnits: ['Business unit loooooooooooooong 1'],
    taxId: 'zxcvbnc',
    address: '125, Georgiou Griva Digeni, Limassol, 3101, Cyprus',
    disableUnlink: false,
  },
  {
    id: 5,
    name: 'Legal entity 5',
    businessUnits: ['Business unit 1', 'Business unit 2', 'Business unit 3'],
    taxId: 'zxcvbnc',
    address: '125, Georgiou Griva Digeni, Limassol, 3101, Cyprus',
    disableUnlink: false,
  },
  {
    id: 6,
    name: 'Legal entity 6',
    businessUnits: ['Business unit 1', 'Business unit 2', 'Business unit 3'],
    taxId: 'zxcvbnc',
    address: '125, Georgiou Griva Digeni, Limassol, 3101, Cyprus',
    disableUnlink: true,
  },
];

export const disableUnlinkTooltipMessage =
  'You are not allowed to unlink the Legal entity because this business structure is already in use.';
