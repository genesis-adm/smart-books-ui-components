import { ReactComponent as PieChartIcon } from 'Svg/16/piechart-light.svg';
import { ReactComponent as SuitcaseIcon } from 'Svg/16/suitcase-light.svg';
import { ReactComponent as EyeSmallIcon } from 'Svg/v2/12/eye.svg';
import { ReactComponent as PencilSmallIcon } from 'Svg/v2/12/pencil-filled-in-circle.svg';
import { ReactComponent as PlusSmallIcon } from 'Svg/v2/12/plus-filled-in-square.svg';
import { ReactComponent as TrashSmallIcon } from 'Svg/v2/12/trashbox.svg';
import { ReactComponent as ChartsIcon } from 'Svg/v2/16/charts.svg';
import { ReactComponent as EyeIcon } from 'Svg/v2/16/eye.svg';
import { ReactComponent as FoldersIcon } from 'Svg/v2/16/folder.svg';
import { ReactComponent as PencilIcon } from 'Svg/v2/16/pencil.svg';
import { ReactComponent as PlusFilledIcon } from 'Svg/v2/16/plus-filled-in-square.svg';
import { ReactComponent as TrashIcon } from 'Svg/v2/16/trashbox.svg';
import { ReactComponent as WalletIcon } from 'Svg/v2/16/wallet.svg';
import React, { ReactNode } from 'react';

type AccessType = 'Deny' | 'UsersOwn' | 'Allowed';
type HeaderType = { icon: ReactNode; title: string };
type SmallHeaderType = {
  icon: ReactNode;
  title: string;
  accessType: AccessType;
};
type AccessColorType = 'red' | 'green' | 'yellow';
type AccessOption = { id: string; title: string };
type TabType = { id: ActiveTabs; title: string; icon: ReactNode };
export type ActiveTabs = 'banking' | 'sales' | 'expenses' | 'accounting' | 'reports';
type AccessData = { id: string; title: string; color: AccessColorType };
export type SwitcherType = {
  id: string;
  title: string;
  accessType: AccessType;
};
export const header: HeaderType[] = [
  { icon: <PlusFilledIcon />, title: 'Create' },
  { icon: <EyeIcon />, title: 'View' },
  { icon: <PencilIcon />, title: 'Edit' },
  { icon: <TrashIcon />, title: 'Delete' },
];

export const smallHeader: SmallHeaderType[] = [
  { icon: <PlusSmallIcon />, title: 'Create', accessType: 'Deny' },
  { icon: <EyeSmallIcon />, title: 'View', accessType: 'UsersOwn' },
  { icon: <PencilSmallIcon />, title: 'Edit', accessType: 'Allowed' },
  { icon: <TrashSmallIcon />, title: 'Delete', accessType: 'Deny' },
];

export const accessOptions: AccessOption[] = [
  { id: 'Deny', title: 'Deny' },
  { id: 'UsersOwn', title: 'Users own' },
  { id: 'Allowed', title: 'Allowed' },
];

export const accessData: Record<AccessType, AccessData> = {
  Deny: { id: 'Deny', title: 'Deny', color: 'red' },
  UsersOwn: { id: 'UsersOwn', title: 'Users own', color: 'yellow' },
  Allowed: { id: 'Allowed', title: 'Allowed', color: 'green' },
};

export const journalSwitchers: SwitcherType[] = [
  { id: 'import_journal', title: 'Import Journal', accessType: 'Deny' },
  { id: 'export_journal', title: 'Export Journal', accessType: 'Allowed' },
];
export const templateAccountsSwitchers: SwitcherType[] = [
  { id: 'import_template_accounts', title: 'Import Template accounts', accessType: 'Deny' },
  { id: 'export_template_accounts', title: 'Export Template accounts', accessType: 'Allowed' },
];
export const chartOfAccountsSwitchers: SwitcherType[] = [
  { id: 'export_chart_of_accounts ', title: 'Export Chart of accounts', accessType: 'Deny' },
  { id: 'view_sensitive_accounts', title: 'View sensitive accounts', accessType: 'UsersOwn' },
  { id: 'import_opening_balance', title: 'Import Opening balance', accessType: 'Allowed' },
];

export const transactionsSwitchers: SwitcherType[] = [
  { id: 'import_transactions', title: 'Import transactions', accessType: 'Deny' },
  { id: 'export_transactions', title: 'Export transactions', accessType: 'Allowed' },
];
export const bankAccountsSwitchers: SwitcherType[] = [
  { id: 'bank_accounts_type_1', title: 'Bank accounts type 1', accessType: 'Deny' },
  { id: 'bank_accounts_type_2', title: 'Bank accounts type 2', accessType: 'Allowed' },
];

export const tabs: TabType[] = [
  { id: 'banking', title: 'Banking', icon: <WalletIcon /> },
  { id: 'sales', title: 'Sales', icon: <ChartsIcon /> },
  { id: 'expenses', title: 'Expenses', icon: <FoldersIcon /> },
  { id: 'accounting', title: 'Accounting', icon: <SuitcaseIcon /> },
  { id: 'reports', title: 'Reports', icon: <PieChartIcon /> },
];
