import { Checkbox } from 'Components/base/Checkbox';
import React from 'react';

export const navigationList = [
  { title: 'Organization', selected: false },
  { title: 'Preferences', selected: false },
  { title: 'Advanced', selected: false },
  { title: 'Security', selected: false },
  { title: 'Business divisions', selected: false },
  { title: 'Legal entities', selected: false },
  { title: 'Users', selected: false },
  { title: 'Currency', selected: true },
];

export const filterItems = [
  { title: 'Euro', id: 'euro' },
  { title: 'Ukrainian Hryvnia', id: 'uah' },
  { title: 'Currency name', id: 'name1' },
  { title: 'Currency name', id: 'name2' },
  { title: 'Currency name', id: 'name3' },
];

const sorting = {
  sortType: null,
  tooltipText: 'Sorting tooltip text',
};
export const todayTableHead = [
  {
    minWidth: '160',
    title: 'Currency name',
    sorting,
  },
  {
    minWidth: '200',
    title: 'Currency code | Symbol',
    align: 'center',
    flexgrow: '1',
  },
  {
    minWidth: '220',
    title: 'FX rate to functional currency',
    flexgrow: '1',
  },
  {
    minWidth: '180',
    title: 'Last update',
    sorting,
  },
  {
    minWidth: '100',
    title: 'Actions',
    align: 'center',
  },
];
export const historyTableHead = [
  {
    minWidth: '48',
    title: <Checkbox name="tickAll" tick="unselect" checked={false} onChange={() => {}} />,
    padding: 'none',
  },
  {
    minWidth: '130',
    title: 'Date',
    sorting,
  },
  {
    minWidth: '160',
    title: 'Currency name',
    sorting,
  },
  {
    minWidth: '200',
    title: 'Currency code | Symbol',
    align: 'center',
    flexgrow: '1',
  },
  {
    minWidth: '220',
    title: 'FX rate to functional currency',
    flexgrow: '1',
  },
  {
    minWidth: '100',
    title: 'Actions',
    align: 'center',
  },
];
