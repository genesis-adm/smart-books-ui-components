import { IconButton } from 'Components/base/buttons/IconButton';
import { ReactComponent as ChevronIcon } from 'Svg/v2/16/chevron-up.svg';
import React from 'react';

export const setupBusinessUnitsTableHead = [
  {
    minWidth: '48',
    title: (
      <IconButton
        size="24"
        background={'transparent'}
        border="grey-20"
        color="grey-100"
        icon={<ChevronIcon />}
        onClick={() => {}}
      />
    ),
    padding: 'none',
  },
  { minWidth: '150', title: 'Logo', align: 'center' },
  { minWidth: '180', title: 'Business unit name' },
  { minWidth: '160', title: 'Business type' },
  { minWidth: '200', title: 'Legal entities' },
  { minWidth: '130', title: 'Actions', align: 'center' },
];

export type BusinessUnit = {
  id: number;
  name: string;
  type: 'Company' | 'Project' | 'Subproject';
  legalEntitiesName: string[];
  rowType: 'primary' | 'secondary' | 'tertiary';
  disableUnlink: boolean;
  isParent: boolean;
};
export const businessUnitsList: BusinessUnit[] = [
  {
    id: 1,
    name: 'First level company',
    type: 'Company',
    legalEntitiesName: ['Legal entity 1', 'Legal entity 2', 'Legal entity 3'],
    rowType: 'primary',
    disableUnlink: false,
    isParent: true,
  },
  {
    id: 2,
    name: 'Second level - project',
    type: 'Project',
    legalEntitiesName: ['Legal entity 1', 'Legal entity 2', 'Legal entity 3'],
    rowType: 'secondary',
    disableUnlink: false,
    isParent: false,
  },
  {
    id: 3,
    name: 'Third level - subproject',
    type: 'Subproject',
    legalEntitiesName: ['Legal entity 1', 'Legal entity 2', 'Legal entity 3'],
    rowType: 'tertiary',
    disableUnlink: false,
    isParent: false,
  },
  {
    id: 4,
    name: 'Company loooooong veeerryyyy veeeryyyyyyyy loong name',
    type: 'Company',
    legalEntitiesName: ['Legal entity 1'],
    rowType: 'primary',
    disableUnlink: false,
    isParent: false,
  },
  {
    id: 5,
    name: 'Company loooooong veeerryyyy veeeryyyyyyyy loong name',
    type: 'Company',
    legalEntitiesName: ['Legal entity 1'],
    rowType: 'primary',
    disableUnlink: true,
    isParent: false,
  },
];

export const disableUnlinkTooltipText: string =
  'You are not allowed to unlink the Business unit because this business structure is already in use.';
