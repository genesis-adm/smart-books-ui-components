import { Meta, Story } from '@storybook/react';
import { Container } from 'Components/base/grid/Container';
import { ContainerMain } from 'Components/base/grid/ContainerMain';
import { MainPageContentContainer } from 'Components/base/grid/MainPageContentContainer';
import { Text } from 'Components/base/typography/Text';
import { HeaderSB } from 'Components/custom/Stories/HeaderSB';
import { NavigationSB } from 'Components/custom/Stories/NavigationSB';
import { CubeAnimation } from 'Components/graphics/CubeAnimation';
import React, { useState } from 'react';

import spacing from '../../assets/styles/spacing.module.scss';

const PageDevelopmentInProgressComponent: Story = () => {
  const [toggle, toggleMenu] = useState(false);
  return (
    <Container fullscreen flexwrap="nowrap" background="grey-10">
      <NavigationSB opened={toggle} onToggleOpen={() => toggleMenu(!toggle)} />
      <ContainerMain navOpened={toggle}>
        <HeaderSB />
        <Container flexdirection="column" className={spacing.mX32}>
          <Text type="h1-semibold">Expenses</Text>
        </Container>
        <Container className={spacing.mt60} /> {/* Dummy container instead of General Tabs */}
        <MainPageContentContainer>
          <Container
            className={spacing.h100p}
            justifycontent="center"
            flexdirection="column"
            gap="12"
            alignitems="center"
          >
            <CubeAnimation size={200} />
            <Text type="title-bold">Expenses</Text>
            <Text type="body-regular-16" color="grey-100">
              Development in progress
            </Text>
          </Container>
        </MainPageContentContainer>
      </ContainerMain>
    </Container>
  );
};

export default {
  title: 'Pages/Service Pages/Page Development In Progress',
  component: PageDevelopmentInProgressComponent,
} as Meta;

export const PageDevelopmentInProgress = PageDevelopmentInProgressComponent.bind({});
PageDevelopmentInProgress.args = {};
