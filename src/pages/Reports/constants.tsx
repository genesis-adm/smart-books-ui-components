import { TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { TableCellWidth } from 'Components/types/gridTypes';
import { GroupType } from 'Pages/Reports/modals/combinateReport/hooks/useExpandableCheckboxData';

export const trialBalanceForThePeriodFilters = [
  { title: 'Account name' },
  { title: 'Business unit' },
  { title: 'Currency' },
];

export const trialBalanceForThePeriodTableHead = [
  { minWidth: '400', title: 'Account Name' },
  { minWidth: '144', title: 'Account ID', align: 'center' },
  { minWidth: '135', title: 'Legal entity' },
  { minWidth: '120', title: 'Currency', align: 'center' },
  { minWidth: '140', title: 'Opening balance', align: 'right' },
  { minWidth: '160', title: 'Debit Turnover', align: 'right' },
  { minWidth: '140', title: 'Credit Turnover', align: 'right' },
  { minWidth: '160', title: 'Closing balance', align: 'right' },
];

export const bankStatementsFilters = [
  { title: 'Bank account' },
  { title: 'Bank' },
  { title: 'Legal entity' },
  { title: 'Business unit' },
  { title: 'Currency' },
];

export const bankStatementReportTableHead = [
  { minWidth: '260', title: 'Bank account' },
  { minWidth: '170', title: 'Account ID' },
  { minWidth: '190', title: 'Bank' },
  { minWidth: '220', title: 'Legal entity' },
  { minWidth: '220', title: 'Business unit' },
  { minWidth: '120', title: 'Currency', align: 'center' },
  { minWidth: '210', title: 'Outstanding balance', align: 'right', font: 'caption-semibold' },
];

export const combinateBalanceFilters = [
  { title: 'Account name' },
  { title: 'Legal entity' },
  { title: 'Business unit' },
  { title: 'Currency' },
];

export const combinateBalanceTableHeader: {
  minWidth: TableCellWidth;
  title: string;
  align?: TableTitleNewProps['align'];
}[] = [
  { minWidth: '350', title: 'Account Name' },
  { minWidth: '160', title: 'Account ID', align: 'center' },
  { minWidth: '170', title: 'Legal entity' },
  { minWidth: '170', title: 'Business unit' },
  { minWidth: '120', title: 'Currency', align: 'center' },
  { minWidth: '190', title: 'Debit', align: 'right' },
  { minWidth: '190', title: 'Credit', align: 'right' },
];

export const businessUnits: GroupType[] = [
  {
    id: 'bu-1',
    name: 'Business unit 1',
    children: [
      {
        id: 'project-bu-1',
        name: 'Project 1',
        children: [
          {
            id: 'subproject 1',
            name: 'Subproject 1',
          },
          { id: 'subproject 12', name: 'Subproject 12' },
        ],
      },
      {
        id: 'project-bu-2',
        name: 'Project BU 2',
        children: [{ id: 'subproject bu 2', name: 'Subproject BU 2' }],
      },
      {
        id: 'project-bu-3',
        name: 'Project BU 3',
      },
      {
        id: 'project-bu-4',
        name: 'Project BU 4',
      },
    ],
  },
  {
    id: 'bu-2',
    name: 'Business unit 2',
    children: [
      {
        id: 'project-2',
        name: 'Project 2',
      },
    ],
  },
  {
    id: 'bu-3',
    name: 'Business unit 3',
  },
];
