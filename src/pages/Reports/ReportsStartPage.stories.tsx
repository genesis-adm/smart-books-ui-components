import { Meta, Story } from '@storybook/react';
import { Container } from 'Components/base/grid/Container';
import { ContainerMain } from 'Components/base/grid/ContainerMain';
import { MainPageContentContainer } from 'Components/base/grid/MainPageContentContainer';
import { Text } from 'Components/base/typography/Text';
import { HeaderSB } from 'Components/custom/Stories/HeaderSB';
import { NavigationSB } from 'Components/custom/Stories/NavigationSB';
import ReportCard from 'Pages/Reports/ReportCard';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../assets/styles/spacing.module.scss';

const ReportsStartPageComponent: Story = () => {
  const [toggle, toggleMenu] = useState(false);
  return (
    <Container fullscreen flexwrap="nowrap" background={'grey-10'}>
      <NavigationSB opened={toggle} onToggleOpen={() => toggleMenu(!toggle)} />
      <ContainerMain navOpened={toggle}>
        <HeaderSB />
        <Container flexdirection="column" className={spacing.mX32}>
          <Text type="h1-semibold">Reports</Text>
        </Container>
        <MainPageContentContainer>
          <Container className={classNames(spacing.p24)} gap="24" flexwrap="wrap" customScroll>
            <ReportCard type="balance" />
            <ReportCard type="register" />
            <ReportCard type="forThePeriod" />
            <ReportCard type="bankStatement" />
            <ReportCard type="combinateReport" />
          </Container>
        </MainPageContentContainer>
      </ContainerMain>
    </Container>
  );
};

export default {
  title: 'Pages/Reports/Reports Start Page',
  component: ReportsStartPageComponent,
} as Meta;

export const ReportsStartPage = ReportsStartPageComponent.bind({});
ReportsStartPage.args = {};
