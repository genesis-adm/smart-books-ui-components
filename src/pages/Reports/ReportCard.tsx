import { DropDown } from 'Components/DropDown';
import { Icon } from 'Components/base/Icon';
import { Button } from 'Components/base/buttons/Button';
import { DropDownButton } from 'Components/base/buttons/DropDownButton';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as DotChartVertical } from 'Svg/colored/dot-chart-vertical.svg';
import { ReactComponent as DotChart } from 'Svg/colored/dot-chart.svg';
import { ReactComponent as LineChartNoDots } from 'Svg/colored/line-chart-no-dots.svg';
import { ReactComponent as LineChartRounded } from 'Svg/colored/line-chart-rounded.svg';
import { ReactComponent as LineChartSimple } from 'Svg/colored/line-chart-simple.svg';
import { ReactComponent as ChevronDown } from 'Svg/v2/16/chevron-down.svg';
import { ReactComponent as ChevronUp } from 'Svg/v2/16/chevron-up.svg';
import classNames from 'classnames';
import React, { ReactElement, ReactNode } from 'react';

import spacing from '../../assets/styles/spacing.module.scss';

type CardType = 'balance' | 'register' | 'forThePeriod' | 'bankStatement' | 'combinateReport';
type CardValues = { title: string; text: string; chart: ReactNode; button: ReactNode };

export interface ReportsCreateCardProps {
  type: CardType;
}

const cardPreset: Record<CardType, CardValues> = {
  balance: {
    title: 'Trial Balance Report',
    text: 'Trial Balance Report is a financial statement that summarizes the balances for each chart of account in the general ledger at a specific point in time, specifically for a particular Legal Entity or Business division.',
    chart: <DotChart />,
    button: (
      <Button width="100" height="40" color="white-100" background={'violet-90-violet-100'}>
        Create
      </Button>
    ),
  },
  register: {
    title: 'Account Register Report',
    text: 'Account Register Report is a financial statement that provides turnover of a selected chart of account in both the base currency and the original currency.',
    chart: <LineChartSimple />,
    button: (
      <Button width="100" height="40" color="white-100" background={'violet-90-violet-100'}>
        Create
      </Button>
    ),
  },
  forThePeriod: {
    title: 'Trial Вalance for the period',
    text: 'Trial Balance for the Period is a financial report that summarizes the balances and turnovers of all the general ledger accounts within a specific accounting period for a particular Legal Entity or Business Unit in both base and original currency.',
    chart: <LineChartNoDots />,
    button: (
      <Button width="100" height="40" color="white-100" background={'violet-90-violet-100'}>
        Create
      </Button>
    ),
  },
  bankStatement: {
    title: 'Bank Statement Report',
    text: 'Bank Statement Report is a financial report that provides a summary of the outstanding balances for all bank accounts as of selected date in both the base currency and the original currency',
    chart: <LineChartRounded />,
    button: (
      <Button width="100" height="40" color="white-100" background={'violet-90-violet-100'}>
        Create
      </Button>
    ),
  },
  combinateReport: {
    title: 'Combinate trial balance',
    text: 'Trial Balance Report is a financial statement that summarizes the balances for each chart of account in the general ledger at a specific point in time, specifically for a particular Legal Entity or Business division.',
    chart: <DotChartVertical />,
    button: (
      <DropDown
        flexdirection="column"
        padding="8"
        control={({ handleOpen, isOpen }) => (
          <Button
            width="100"
            height="40"
            color="white-100"
            background={'violet-90-violet-100'}
            onClick={handleOpen}
            iconRight={isOpen ? <ChevronUp /> : <ChevronDown />}
          >
            Create
          </Button>
        )}
      >
        <>
          <DropDownButton size="204">Legal entity</DropDownButton>
          <DropDownButton size="204">Business unit</DropDownButton>
        </>
      </DropDown>
    ),
  },
};

const ReportCard = ({ type }: ReportsCreateCardProps): ReactElement => (
  <Container
    borderRadius="20"
    border="grey-20"
    background={'grey-10'}
    position={'relative'}
    className={classNames(spacing.p32, spacing.w594fixed, spacing.h282fixed)}
  >
    <Container flexdirection="column" gap="16">
      <Text type="title-semibold">{cardPreset[type].title}</Text>
      <Text
        className={classNames(spacing.w400fixed, spacing.z_ind1)}
        type="body-regular-14"
        color="grey-100"
        transition="unset"
      >
        {cardPreset[type].text}
      </Text>
      {cardPreset[type].button}
    </Container>

    <Icon icon={cardPreset[type].chart} className={spacing.chart} />
  </Container>
);

export default ReportCard;
