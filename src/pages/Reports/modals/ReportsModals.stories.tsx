import { DropDown } from 'Components/DropDown';
import { DatePicker } from 'Components/base/DatePicker';
import { Divider } from 'Components/base/Divider';
import { Tag } from 'Components/base/Tag';
import { Button } from 'Components/base/buttons/Button';
import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Container } from 'Components/base/grid/Container';
import { MainPageContentContainer } from 'Components/base/grid/MainPageContentContainer';
import { OptionDoubleVerticalNew } from 'Components/base/inputs/SelectNew/options/OptionDoubleVerticalNew';
import { OptionSingleNew } from 'Components/base/inputs/SelectNew/options/OptionSingleNew';
import { CurrencyTab, CurrencyTabWrapper } from 'Components/base/tabs/CurrencyTab';
import { Text } from 'Components/base/typography/Text';
import { ReportsCreateItem } from 'Components/custom/Reports/ReportsCreateItem';
import { ReportsTotalValues } from 'Components/custom/Reports/ReportsTotalValues';
import { Pagination } from 'Components/custom/Stories/Pagination/Pagination';
import PerPageSelect from 'Components/custom/Stories/Pagination/PerPageSelect/PerPageSelect';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { FilterDropDown } from 'Components/elements/filters/FilterDropDown';
import { FilterItemsContainer } from 'Components/elements/filters/FilterItemsContainer';
import { FilterSearch } from 'Components/elements/filters/FilterSearch';
import { FilterToggleButton } from 'Components/elements/filters/FilterToggleButton';
import { FiltersWrapper } from 'Components/elements/filters/FiltersWrapper';
import { Search } from 'Components/inputs/Search';
import useSearchExample from 'Components/inputs/Search/hooks/useSearchExample';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { TableCellWidth } from 'Components/types/gridTypes';
import { options, rowsPerPageOptions } from 'Mocks/fakeOptions';
import { ReactComponent as ArrowRightIcon } from 'Svg/v2/16/arrow-right.svg';
import { ReactComponent as CSVIcon } from 'Svg/v2/16/file-csv.svg';
import { ReactComponent as XLSIcon } from 'Svg/v2/16/file-xls.svg';
import { ReactComponent as InfoIcon } from 'Svg/v2/16/info.svg';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Reports/Reports Modals',
};

export const SetUpAccess: React.FC = () => {
  const [startDay, setStartDay] = useState<Date | null>(new Date());
  const [endDay, setEndDay] = useState<Date | null>(null);
  const changeDayHandler = (dates: [Date | null, Date | null]) => {
    const [start, end] = dates;

    setStartDay(start);
    setEndDay(end);
  };

  return (
    <Modal
      size="960"
      header={
        <ModalHeaderNew
          title="Account Register Report"
          // title="Trial Balance Report"
          border
          titlePosition="center"
          onClose={() => {}}
        />
      }
      pluginScrollDisabled
      footer={
        <ModalFooterNew justifycontent="space-between" border>
          <Button
            height="48"
            iconRight={<ArrowRightIcon />}
            navigation
            width="180"
            onClick={() => {}}
          >
            Next
          </Button>
        </ModalFooterNew>
      }
    >
      {/* For trial balance */}
      {/* <Container justifycontent="center" className={spacing.mt16} gap="16" alignitems="center">
        <Text type="caption-regular" color="grey-100">
          Group by:
        </Text>
        <Radio type="caption-regular" id="le" name="sort" onChange={() => {}}>
          Legal entity
        </Radio>
        <Radio type="caption-regular" id="bu" name="sort" onChange={() => {}}>
          Business unit
        </Radio>
      </Container> */}
      <Container
        justifycontent="space-between"
        className={classNames(spacing.pY24, spacing.pX80, spacing.h100p)}
        gap="24"
      >
        <Container
          flexdirection="column"
          alignitems="center"
          flexgrow="1"
          gap="16"
          className={classNames(spacing.w360)}
        >
          <Text type="body-medium">Choose Account</Text>
          <Search
            className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
            width="360"
            height="40"
            name="search-LE"
            placeholder="Search account"
            value=""
            onChange={() => {}}
            searchParams={[]}
            onSearch={() => {}}
            onClear={() => {}}
          />
          <Container flexdirection="column" gap="4" className={spacing.w100p} customScroll>
            {Array.from(Array(10)).map((_, index) => (
              <ReportsCreateItem
                key={index}
                id={`${index}`}
                name="accountRegister"
                mainText={`Main text #${index + 1}`}
                secondaryText={`Secondary text #${index + 1}`}
                checked={index === 2}
                onClick={() => {}}
              />
            ))}
          </Container>
        </Container>
        <Divider type="vertical" height="full" />
        <Container
          flexdirection="column"
          alignitems="center"
          flexgrow="1"
          gap="16"
          className={classNames(spacing.w360, spacing.pb16, spacing.pX16)}
          radius
        >
          <Text type="body-medium">Choose Date</Text>
          <Container
            className={classNames(spacing.p16, spacing.w320fixed)}
            radius
            flexdirection="column"
            background={'white-100'}
            style={{ boxShadow: '0px 10px 30px rgba(192, 196, 205, 0.3)' }}
            border="grey-20"
          >
            <DatePicker
              name="calendar"
              withRange
              withPresets
              startDate={startDay}
              endDate={endDay}
              onChange={changeDayHandler}
            />
          </Container>
        </Container>
      </Container>
    </Modal>
  );
};

export const AccountRegisterBalanceAccounts: React.FC = () => {
  const [showFilters, setShowFilters] = useState<boolean>(false);
  const [rowsPerPage, setRowsPerPage] = useState<string>(rowsPerPageOptions[0].value);
  const [startDay, setStartDay] = useState<Date | null>(new Date());
  const [endDay, setEndDay] = useState<Date | null>(null);
  const changeDayHandler = (dates: [Date | null, Date | null]) => {
    const [start, end] = dates;

    setStartDay(start);
    setEndDay(end);
  };

  const { searchValue, setSearchValue, searchValuesList, handleSearchValueAdd, handleClearAll } =
    useSearchExample();

  const filters = [{ title: 'Counterparty' }, { title: 'Debit' }, { title: 'Credit' }];

  const sorting = {
    sortType: null,
    tooltipText: 'Sorting tooltip text',
  };

  const tableHead = [
    {
      minWidth: '150',
      title: 'Journal date',
      sorting,
    },
    {
      minWidth: '140',
      title: 'Journal entry ID',
    },
    {
      minWidth: '210',
      title: 'Description',
      flexgrow: '1',
    },
    {
      minWidth: '160',
      title: 'Counterparty',
    },
    {
      minWidth: '130',
      title: 'FX rate base',
    },
    {
      minWidth: '80',
      title: 'Currency',
      align: 'center',
    },
    {
      minWidth: '140',
      title: 'Opening balance',
      align: 'right',
    },
    {
      minWidth: '140',
      title: 'Debit',
      align: 'right',
      font: 'caption-semibold',
    },
    {
      minWidth: '140',
      title: 'Credit',
      align: 'right',
      font: 'caption-semibold',
    },
    {
      minWidth: '140',
      title: 'Closing balance',
      align: 'right',
    },
  ];

  return (
    <Modal
      size="full"
      background={'grey-10'}
      overlay="grey-10"
      header={
        <ModalHeaderNew
          title="Account Register Report"
          background={'grey-10'}
          titlePosition="center"
          onClose={() => {}}
        />
      }
      pluginScrollDisabled
      footer={
        <ModalFooterNew justifycontent="space-between" border>
          <Pagination
            borderTop="none"
            padding="0"
            customPagination={
              <Container gap="16" alignitems="center">
                <Text type="subtext-regular" color="grey-100">
                  Rows per Page
                </Text>
                <PerPageSelect
                  selectedValue={rowsPerPage}
                  onOptionClick={(value) => setRowsPerPage(value)}
                />
              </Container>
            }
          />
          <Container>
            <Button
              background={'transparent-blue-10'}
              font="caption-semibold"
              height="40"
              color="grey-100-violet-90"
              iconLeft={<XLSIcon />}
              onClick={() => {}}
            >
              Export to XLS
            </Button>
            <Button
              background={'transparent-blue-10'}
              font="caption-semibold"
              height="40"
              color="grey-100-violet-90"
              iconLeft={<CSVIcon />}
              onClick={() => {}}
            >
              Export to CSV
            </Button>
          </Container>
        </ModalFooterNew>
      }
    >
      <Container
        justifycontent="space-between"
        alignitems="center"
        className={classNames(spacing.mt16, spacing.mX24)}
      >
        <Container alignitems="center" gap="16">
          <Container flexdirection="column" gap="8">
            <Text color="grey-100" className={spacing.ml4}>
              Account
            </Text>
            <Tag
              align="left"
              icon={<InfoIcon />}
              className={classNames(spacing.w350)}
              iconPosition="left"
              cursor="pointer"
              color="grey-black"
              text="Payroll liabilities - EUR | LLC Health&Fitness | BetterME ABBDDDAAdfffddfffdfdfsfdffdfgfggtgt "
              iconTooltip="Account name | Legal entity | Business division"
              additionalDropdownBlock={{
                icon: <EditIcon />,
                tooltip: 'Edit account',
                padding: '16',
                dropdown: (
                  <>
                    <FilterSearch
                      className={spacing.ml8}
                      title="Please choose Accounts"
                      value=""
                      inputWidth="220"
                      onChange={() => {}}
                    />
                    <FilterItemsContainer className={spacing.w280} gap="0">
                      {options.map(({ label, secondaryLabel, value }, index) => (
                        <OptionDoubleVerticalNew
                          key={value}
                          label={label}
                          secondaryLabel={secondaryLabel}
                          type="radio"
                          selected={index === 2}
                          onClick={() => {}}
                          tooltip
                        />
                      ))}
                    </FilterItemsContainer>
                    <Divider fullHorizontalWidth />
                    <Container justifycontent="flex-end">
                      <Button width="108" height="32" onClick={() => {}}>
                        Apply
                      </Button>
                    </Container>
                  </>
                ),
              }}
              noWrap
              onClick={() => {}}
            />
          </Container>
          <Container flexdirection="column" gap="8">
            <Text color="grey-100" className={spacing.ml4}>
              Period
            </Text>
            <Tag
              color={'grey-black'}
              className={spacing.w180}
              cursor="pointer"
              text="01.04.2023-05.04.2023"
              align="left"
              noWrap
              additionalDropdownBlock={{
                icon: <EditIcon />,
                tooltip: 'Edit Period',
                padding: 'none',
                dropdown: (
                  <>
                    <DatePicker
                      name="calendar"
                      withRange
                      withPresets
                      startDate={startDay}
                      endDate={endDay}
                      onChange={changeDayHandler}
                    />
                    <Divider />

                    <Container
                      className={classNames(spacing.m16)}
                      justifycontent="space-between"
                      alignitems="center"
                      flexdirection="row-reverse"
                    >
                      <Button
                        width="auto"
                        height="32"
                        padding="8"
                        onClick={() => {}}
                        background={'violet-90-violet-100'}
                        color="white-100"
                      >
                        Apply
                      </Button>
                    </Container>
                  </>
                ),
              }}
            />
          </Container>
        </Container>
        <Container>
          <Container flexdirection="column" className={classNames(spacing.w150fixed, spacing.pX8)}>
            <Text type="subtext-regular" align="right" color="grey-90">
              Opening balance, €
            </Text>
            <Text type="body-bold-14" align="right">
              0.00
            </Text>
          </Container>
          <Container flexdirection="column" className={classNames(spacing.w150fixed, spacing.pX8)}>
            <Text type="subtext-regular" align="right" color="grey-90">
              Total Debit, €
            </Text>
            <Text type="body-bold-14" align="right">
              0.00
            </Text>
          </Container>
          <Container flexdirection="column" className={classNames(spacing.w150fixed, spacing.pX8)}>
            <Text type="subtext-regular" align="right" color="grey-90">
              Total Credit, €
            </Text>
            <Text type="body-bold-14" align="right">
              0.00
            </Text>
          </Container>
          <Container flexdirection="column" className={classNames(spacing.w150fixed, spacing.pX8)}>
            <Text type="subtext-regular" align="right" color="grey-90">
              Closing balance, €
            </Text>
            <Text type="body-bold-14" align="right">
              0.00
            </Text>
          </Container>
        </Container>
      </Container>
      <MainPageContentContainer>
        <Container
          className={classNames(spacing.mX24, spacing.mt24)}
          justifycontent="space-between"
          gap="24"
        >
          <Container gap="24" alignitems="center">
            <FilterToggleButton
              showFilters={showFilters}
              filtersSelected={false}
              onClear={() => {}}
              onClick={() => {
                setShowFilters((prevState) => !prevState);
              }}
            />
            <Search
              width="320"
              height="40"
              name="search-reports-description-search"
              placeholder="Search by Description"
              value={searchValue}
              onChange={(e) => setSearchValue(e.target.value)}
              searchParams={searchValuesList}
              onSearch={handleSearchValueAdd}
              onClear={handleClearAll}
            />
          </Container>
          <CurrencyTabWrapper>
            <CurrencyTab id="usd" onClick={() => {}} active>
              EUR
            </CurrencyTab>
            <CurrencyTab id="eur" onClick={() => {}} active={false}>
              USD
            </CurrencyTab>
          </CurrencyTabWrapper>
        </Container>
        <FiltersWrapper active={showFilters}>
          {filters.map((item) => (
            <FilterDropDown
              key={item.title}
              title={item.title}
              value="All"
              onClear={() => {}}
              selectedAmount={0}
            />
          ))}
        </FiltersWrapper>
        <TableNew
          noResults={false}
          className={classNames(spacing.mt24, spacing.mX24)}
          tableHead={
            <TableHeadNew background={'grey-10'} radius="10">
              {tableHead.map((item, index) => (
                <TableTitleNew
                  key={index}
                  background={'grey-10'}
                  radius="10"
                  minWidth={item.minWidth as TableCellWidth}
                  flexgrow={item.flexgrow as TableTitleNewProps['flexgrow']}
                  // padding={item.padding as TableTitleNewProps['padding']}
                  align={item.align as TableTitleNewProps['align']}
                  font={item.font as TableTitleNewProps['font']}
                  title={item.title}
                  sorting={item.sorting}
                  onClick={() => {}}
                />
              ))}
            </TableHeadNew>
          }
        >
          <TableBodyNew>
            {Array.from(Array(4)).map((_, index) => (
              <TableRowNew key={index} background={'info'} size="36" cursor="auto">
                <TableCellTextNew minWidth="150" type="text-regular" value="12.02.2023" />
                <TableCellNew minWidth="140">
                  <LinkButton type="caption-regular" onClick={() => {}}>
                    1233
                  </LinkButton>
                </TableCellNew>
                <TableCellTextNew
                  minWidth="210"
                  lineClamp="1"
                  type="text-regular"
                  value="Description text"
                  flexgrow="1"
                />
                <TableCellTextNew
                  minWidth="160"
                  lineClamp="1"
                  type="text-regular"
                  value="Peter Parker"
                />
                <TableCellTextNew
                  minWidth="130"
                  lineClamp="1"
                  type="text-regular"
                  value="1020874"
                />
                <TableCellTextNew minWidth="80" type="text-regular" align="center" value="EUR" />
                <TableCellTextNew
                  minWidth="140"
                  type="caption-semibold"
                  value="2000.00 €"
                  align="right"
                />
                <TableCellTextNew
                  minWidth="140"
                  type="caption-semibold"
                  value="2000.00 €"
                  align="right"
                />
                <TableCellTextNew
                  minWidth="140"
                  type="caption-semibold"
                  value="2000.00 €"
                  align="right"
                />
                <TableCellTextNew
                  minWidth="140"
                  type="caption-regular"
                  value="2000.00 €"
                  align="right"
                />
              </TableRowNew>
            ))}
            <ReportsTotalValues
              type="balance"
              title="Subtotal page 1"
              data={[
                ['2000.00 €', '2000.00 €', '2000.00 €', '-1000.00 €'],
                ['2000.00 €', '2000.00 €', '2000.00 €', '-1000.00 €'],
                ['2000.00 €', '2000.00 €', '2000.00 €', '-1000.00 €'],
              ]}
              // data={[
              //   ['EUR', '2000.00 €', '-1000.00 €'],
              //   ['EUR', '2000.00 €', '-1000.00 €'],
              //   ['EUR', '2000.00 €', '-1000.00 €'],
              // ]}
              filterActive
            />
          </TableBodyNew>
        </TableNew>
      </MainPageContentContainer>
    </Modal>
  );
};

export const AccountRegisterPLAccounts: React.FC = () => {
  const [showFilters, setShowFilters] = useState<boolean>(false);
  const [startDay, setStartDay] = useState<Date | null>(new Date());
  const [rowsPerPage, setRowsPerPage] = useState<string>(rowsPerPageOptions[0].value);
  const [endDay, setEndDay] = useState<Date | null>(null);
  const changeDayHandler = (dates: [Date | null, Date | null]) => {
    const [start, end] = dates;
    setStartDay(start);
    setEndDay(end);
  };

  const { searchValue, setSearchValue, searchValuesList, handleSearchValueAdd, handleClearAll } =
    useSearchExample();

  const filters = [{ title: 'Counterparty' }, { title: 'Debit' }, { title: 'Credit' }];

  const sorting = {
    sortType: null,
    tooltipText: 'Sorting tooltip text',
  };

  const tableHead = [
    {
      minWidth: '150',
      title: 'Journal date',
      sorting,
    },
    {
      minWidth: '140',
      title: 'Journal entry ID',
    },
    {
      minWidth: '210',
      title: 'Description',
      flexgrow: '1',
    },
    {
      minWidth: '160',
      title: 'Counterparty',
    },
    {
      minWidth: '130',
      title: 'FX rate base',
    },
    {
      minWidth: '80',
      title: 'Currency',
      align: 'center',
    },
    {
      minWidth: '140',
      title: 'Debit',
      align: 'right',
      font: 'caption-semibold',
    },
    {
      minWidth: '140',
      title: 'Credit',
      align: 'right',
      font: 'caption-semibold',
    },
    {
      minWidth: '140',
      title: 'Turnover',
      align: 'right',
    },
  ];

  return (
    <Modal
      size="full"
      background={'grey-10'}
      overlay="grey-10"
      header={
        <ModalHeaderNew
          title="Account Register Report"
          background={'grey-10'}
          titlePosition="center"
          onClose={() => {}}
        />
      }
      pluginScrollDisabled
      footer={
        <ModalFooterNew justifycontent="space-between" border>
          <Pagination
            borderTop="none"
            padding="0"
            customPagination={
              <Container gap="16" alignitems="center">
                <Text type="subtext-regular" color="grey-100">
                  Rows per Page
                </Text>
                <PerPageSelect
                  selectedValue={rowsPerPage}
                  onOptionClick={(value) => setRowsPerPage(value)}
                />
              </Container>
            }
          />
          <Container>
            <Button
              background={'transparent-blue-10'}
              font="caption-semibold"
              height="40"
              color="grey-100-violet-90"
              iconLeft={<XLSIcon />}
              onClick={() => {}}
            >
              Export to XLS
            </Button>
            <Button
              background={'transparent-blue-10'}
              font="caption-semibold"
              height="40"
              color="grey-100-violet-90"
              iconLeft={<CSVIcon />}
              onClick={() => {}}
            >
              Export to CSV
            </Button>
          </Container>
        </ModalFooterNew>
      }
    >
      <Container
        justifycontent="space-between"
        alignitems="center"
        className={classNames(spacing.mt16, spacing.mX24)}
      >
        <Container alignitems="center" gap="16">
          <Container flexdirection="column" gap="8">
            <Text color="grey-100" className={spacing.ml4}>
              Account
            </Text>
            <DropDown
              flexdirection="column"
              padding="8"
              gap="8"
              control={({ isOpen, handleOpen }) => (
                <Tag
                  icon={<EditIcon />}
                  iconPosition="right"
                  color={isOpen ? 'violet' : 'grey-black'}
                  text="Payroll liabilities - EUR | LLC Health&Fitness | BetterME"
                  onClick={handleOpen}
                />
              )}
            >
              <FilterSearch
                className={spacing.ml8}
                title="Please choose Accounts"
                value=""
                inputWidth="220"
                onChange={() => {}}
              />
              <FilterItemsContainer className={spacing.w280} gap="0">
                {options.map(({ label, secondaryLabel, value }, index) => (
                  <OptionDoubleVerticalNew
                    key={value}
                    label={label}
                    secondaryLabel={secondaryLabel}
                    type="radio"
                    selected={index === 2}
                    onClick={() => {}}
                    tooltip
                  />
                ))}
              </FilterItemsContainer>
              <Divider fullHorizontalWidth />
              <Container justifycontent="flex-end">
                <Button width="108" height="32" onClick={() => {}}>
                  Apply
                </Button>
              </Container>
            </DropDown>
          </Container>
          <Container flexdirection="column" gap="8">
            <Text color="grey-100" className={spacing.ml4}>
              Period
            </Text>
            <DropDown
              flexdirection="column"
              padding="none"
              control={({ isOpen, handleOpen }) => (
                <Tag
                  icon={<EditIcon />}
                  iconPosition="right"
                  color={isOpen ? 'violet' : 'grey-black'}
                  text="01.04.2023-05.04.2023"
                  onClick={handleOpen}
                />
              )}
            >
              <DatePicker
                withRange
                name="calendar"
                startDate={startDay}
                endDate={endDay}
                onChange={changeDayHandler}
              />
            </DropDown>
          </Container>
        </Container>
        <Container>
          <Container flexdirection="column" className={classNames(spacing.w150fixed, spacing.pX8)}>
            <Text type="subtext-regular" align="right" color="grey-90">
              Total Debit, $
            </Text>
            <Text type="body-bold-14" align="right">
              0.00
            </Text>
          </Container>
          <Container flexdirection="column" className={classNames(spacing.w150fixed, spacing.pX8)}>
            <Text type="subtext-regular" align="right" color="grey-90">
              Total Credit, $
            </Text>
            <Text type="body-bold-14" align="right">
              0.00
            </Text>
          </Container>
          <Container flexdirection="column" className={classNames(spacing.w150fixed, spacing.pX8)}>
            <Text type="subtext-regular" align="right" color="grey-90">
              Total turnover, $
            </Text>
            <Text type="body-bold-14" align="right">
              0.00
            </Text>
          </Container>
        </Container>
      </Container>
      <MainPageContentContainer>
        <Container
          className={classNames(spacing.mX24, spacing.mt24)}
          justifycontent="space-between"
          gap="24"
        >
          <Container gap="24" alignitems="center">
            <FilterToggleButton
              showFilters={showFilters}
              filtersSelected={false}
              onClear={() => {}}
              onClick={() => {
                setShowFilters((prevState) => !prevState);
              }}
            />
            <Search
              width="320"
              height="40"
              name="search-reports-description-search"
              placeholder="Search by Description"
              value={searchValue}
              onChange={(e) => setSearchValue(e.target.value)}
              searchParams={searchValuesList}
              onSearch={handleSearchValueAdd}
              onClear={handleClearAll}
            />
          </Container>

          <CurrencyTabWrapper>
            <CurrencyTab id="usd" onClick={() => {}} active>
              Base currency USD
            </CurrencyTab>
            <CurrencyTab id="eur" onClick={() => {}} active={false}>
              Original currency
            </CurrencyTab>
          </CurrencyTabWrapper>
        </Container>
        <FiltersWrapper active={showFilters}>
          {filters.map((item) => (
            <FilterDropDown
              key={item.title}
              title={item.title}
              value="All"
              onClear={() => {}}
              selectedAmount={0}
            />
          ))}
        </FiltersWrapper>
        <TableNew
          noResults={false}
          className={classNames(spacing.mt24, spacing.mX24)}
          tableHead={
            <TableHeadNew background={'grey-10'} radius="10">
              {tableHead.map((item, index) => (
                <TableTitleNew
                  key={index}
                  background={'grey-10'}
                  radius="10"
                  minWidth={item.minWidth as TableCellWidth}
                  flexgrow={item.flexgrow as TableTitleNewProps['flexgrow']}
                  // padding={item.padding as TableTitleNewProps['padding']}
                  align={item.align as TableTitleNewProps['align']}
                  font={item.font as TableTitleNewProps['font']}
                  title={item.title}
                  sorting={item.sorting}
                  onClick={() => {}}
                />
              ))}
            </TableHeadNew>
          }
        >
          <TableBodyNew>
            {Array.from(Array(4)).map((_, index) => (
              <TableRowNew key={index} background={'info'} size="36" cursor="auto">
                <TableCellTextNew minWidth="150" type="text-regular" value="12.02.2023" />
                <TableCellNew minWidth="140">
                  <LinkButton
                    tooltip="Open Journal entry"
                    type="caption-regular"
                    onClick={() => {}}
                  >
                    1233
                  </LinkButton>
                </TableCellNew>
                <TableCellTextNew
                  minWidth="210"
                  lineClamp="1"
                  type="text-regular"
                  value="Description text"
                  flexgrow="1"
                />
                <TableCellTextNew
                  minWidth="160"
                  lineClamp="1"
                  type="text-regular"
                  value="Peter Parker"
                />
                <TableCellTextNew
                  minWidth="130"
                  lineClamp="1"
                  type="text-regular"
                  value="1020874"
                />
                <TableCellTextNew minWidth="80" type="text-regular" align="center" value="EUR" />
                <TableCellTextNew
                  minWidth="140"
                  type="caption-semibold"
                  value="2000.00 €"
                  align="right"
                />
                <TableCellTextNew
                  minWidth="140"
                  type="caption-semibold"
                  value="2000.00 €"
                  align="right"
                />
                <TableCellTextNew
                  minWidth="140"
                  type="caption-regular"
                  value="2000.00 €"
                  align="right"
                />
              </TableRowNew>
            ))}
            <ReportsTotalValues
              type="simple"
              title="Subtotal page 1"
              data={[
                ['EUR', '2000.00 €', '2000.00 €', '-1000.00 €'],
                ['EUR', '2000.00 €', '2000.00 €', '-1000.00 €'],
                ['EUR', '2000.00 €', '2000.00 €', '-1000.00 €'],
              ]}
              filterActive
            />
          </TableBodyNew>
        </TableNew>
      </MainPageContentContainer>
    </Modal>
  );
};

export const TrialBalanceLegalEntityBusinessUnit: React.FC = () => {
  const [showFilters, setShowFilters] = useState<boolean>(false);
  const { searchValue, setSearchValue, searchValuesList, handleSearchValueAdd, handleClearAll } =
    useSearchExample();
  const [rowsPerPage, setRowsPerPage] = useState<string>(rowsPerPageOptions[0].value);

  const filters = [{ title: 'Account name' }, { title: 'Business unit' }, { title: 'Currency' }];

  const tableHead = [
    {
      minWidth: '240',
      title: 'Account Name',
      flexgrow: '1',
    },
    {
      minWidth: '160',
      title: 'Account ID',
      align: 'center',
    },
    {
      minWidth: '230',
      title: 'Business unit',
      flexgrow: '1',
    },
    // { minWidth: '230', title: 'Legal entity', flexgrow: '1' },
    {
      minWidth: '80',
      title: 'Currency',
      align: 'center',
    },
    {
      minWidth: '140',
      title: 'Debit',
      align: 'right',
      font: 'caption-semibold',
    },
    {
      minWidth: '140',
      title: 'Credit',
      align: 'right',
      font: 'caption-semibold',
    },
  ];

  return (
    <Modal
      size="full"
      background={'grey-10'}
      overlay="grey-10"
      header={
        <ModalHeaderNew
          title="Trial Balance Report"
          background={'grey-10'}
          titlePosition="center"
          onClose={() => {}}
        />
      }
      pluginScrollDisabled
      footer={
        <ModalFooterNew justifycontent="space-between" border>
          <Pagination
            borderTop="none"
            padding="0"
            customPagination={
              <Container gap="16" alignitems="center">
                <Text type="subtext-regular" color="grey-100">
                  Rows per Page
                </Text>
                <PerPageSelect
                  selectedValue={rowsPerPage}
                  onOptionClick={(value) => setRowsPerPage(value)}
                />
              </Container>
            }
          />
          <Container>
            <Button
              background={'transparent-blue-10'}
              font="caption-semibold"
              height="40"
              color="grey-100-violet-90"
              iconLeft={<XLSIcon />}
              onClick={() => {}}
            >
              Export to XLS
            </Button>
            <Button
              background={'transparent-blue-10'}
              font="caption-semibold"
              height="40"
              color="grey-100-violet-90"
              iconLeft={<CSVIcon />}
              onClick={() => {}}
            >
              Export to CSV
            </Button>
          </Container>
        </ModalFooterNew>
      }
    >
      <Container
        justifycontent="space-between"
        alignitems="center"
        className={classNames(spacing.mt16, spacing.mX24)}
      >
        <Container alignitems="center" gap="16">
          <Container flexdirection="column" gap="8">
            <Text color="grey-100" className={spacing.ml4}>
              As of date
            </Text>
            <DropDown
              padding="none"
              flexdirection="column"
              control={({ isOpen, handleOpen }) => (
                <Tag
                  icon={<EditIcon />}
                  iconPosition="right"
                  color={isOpen ? 'violet' : 'grey-black'}
                  text="19.02.2023"
                  onClick={handleOpen}
                />
              )}
            >
              <DatePicker name="calendar" selected={new Date()} onChange={() => {}} />
              <Divider fullHorizontalWidth />
              <Container justifycontent="flex-end" className={spacing.mt12}>
                <Button width="108" height="32" onClick={() => {}}>
                  Apply
                </Button>
              </Container>
            </DropDown>
          </Container>
          <Container flexdirection="column" gap="8">
            <Text color="grey-100" className={spacing.ml4}>
              Business unit
            </Text>
            <DropDown
              flexdirection="column"
              padding="16"
              gap="8"
              control={({ isOpen, handleOpen }) => (
                <Tag
                  icon={<EditIcon />}
                  iconPosition="right"
                  color={isOpen ? 'violet' : 'grey-black'}
                  text="Business unit name"
                  onClick={handleOpen}
                />
              )}
            >
              <FilterSearch
                className={spacing.ml8}
                title="Legal entity"
                value=""
                inputWidth="220"
                onChange={() => {}}
              />
              <FilterItemsContainer className={spacing.w280} gap="0">
                {options.map(({ label, value }, index) => (
                  <OptionSingleNew
                    key={value}
                    label={label}
                    type="radio"
                    height="32"
                    selected={index === 2}
                    onClick={() => {}}
                  />
                ))}
              </FilterItemsContainer>
              <Divider fullHorizontalWidth />
              <Container justifycontent="flex-end">
                <Button width="108" height="32" onClick={() => {}}>
                  Apply
                </Button>
              </Container>
            </DropDown>
          </Container>
        </Container>
      </Container>
      <MainPageContentContainer>
        <Container
          className={classNames(spacing.mX24, spacing.mt24)}
          justifycontent="space-between"
          gap="24"
        >
          <Container gap="24" alignitems="center">
            <FilterToggleButton
              showFilters={showFilters}
              filtersSelected={false}
              onClear={() => {}}
              onClick={() => {
                setShowFilters((prevState) => !prevState);
              }}
            />
            <Search
              width="440"
              height="40"
              name="search-reports-description-search"
              placeholder="Search by Account name or Account ID"
              value={searchValue}
              onChange={(e) => setSearchValue(e.target.value)}
              searchParams={searchValuesList}
              onSearch={handleSearchValueAdd}
              onClear={handleClearAll}
            />
          </Container>
        </Container>
        <FiltersWrapper active={showFilters}>
          {filters.map((item) => (
            <FilterDropDown
              key={item.title}
              title={item.title}
              value="All"
              onClear={() => {}}
              selectedAmount={0}
            />
          ))}
        </FiltersWrapper>
        <TableNew
          noResults={false}
          className={classNames(spacing.mt24, spacing.mX24)}
          tableHead={
            <TableHeadNew background={'grey-10'} radius="10">
              {tableHead.map((item, index) => (
                <TableTitleNew
                  key={index}
                  background={'grey-10'}
                  radius="10"
                  minWidth={item.minWidth as TableCellWidth}
                  flexgrow={item.flexgrow as TableTitleNewProps['flexgrow']}
                  // padding={item.padding as TableTitleNewProps['padding']}
                  align={item.align as TableTitleNewProps['align']}
                  font={item.font as TableTitleNewProps['font']}
                  title={item.title}
                  // sorting={item.sorting}
                  onClick={() => {}}
                />
              ))}
            </TableHeadNew>
          }
        >
          <TableBodyNew>
            {Array.from(Array(4)).map((_, index) => (
              <TableRowNew key={index} background={'info'} size="36" cursor="auto">
                <TableCellTextNew
                  minWidth="240"
                  type="text-regular"
                  value="Payroll liabilities - EUR"
                  secondaryValue="Liabilities"
                  flexgrow="1"
                  flexdirection="row"
                  justifycontent="space-between"
                />
                <TableCellTextNew minWidth="160" type="text-regular" align="center" value="9834" />
                <TableCellTextNew
                  minWidth="230"
                  type="text-regular"
                  value="Business unit name"
                  // value="Legal entity name"
                  flexgrow="1"
                />
                <TableCellTextNew minWidth="80" type="text-regular" align="center" value="EUR" />
                <TableCellTextNew
                  minWidth="140"
                  type="caption-semibold"
                  value="2000.00 €"
                  align="right"
                />
                <TableCellTextNew
                  minWidth="140"
                  type="caption-regular"
                  value="2000.00 €"
                  align="right"
                />
              </TableRowNew>
            ))}
            <ReportsTotalValues
              type="simple"
              title="Subtotal page 1"
              // data={[
              //   ['2000.00 €', '2000.00 €', '2000.00 €', '-1000.00 €'],
              //   ['2000.00 €', '2000.00 €', '2000.00 €', '-1000.00 €'],
              //   ['2000.00 €', '2000.00 €', '2000.00 €', '-1000.00 €'],
              // ]}
              data={[
                ['EUR', '2000.00 €', '-1000.00 €'],
                ['EUR', '2000.00 €', '-1000.00 €'],
                ['EUR', '2000.00 €', '-1000.00 €'],
              ]}
              filterActive
            />
          </TableBodyNew>
        </TableNew>
      </MainPageContentContainer>
    </Modal>
  );
};
