import { DatePicker } from 'Components/base/DatePicker';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Reports/Reports Modals/BankStatementReport',
};
export const BankStatementReportStep1: React.FC = () => (
  <Modal
    size="614"
    pluginScrollDisabled
    header={
      <ModalHeaderNew
        title="Bank Statement Report"
        titlePosition="center"
        onClose={() => {}}
        closeActionBackgroundColor="inherit"
      />
    }
    footer={
      <ModalFooterNew justifycontent="space-between" border>
        <Button height="48" background="grey-90" navigation width="156" onClick={() => {}}>
          Next
        </Button>
      </ModalFooterNew>
    }
  >
    <Container justifycontent="center" alignitems="center" flexdirection="column" gap="24">
      <Text type="body-regular-14" color="grey-100">
        Please choose a date for the trial balance report period.
      </Text>
      <Container
        className={classNames(spacing.p16, spacing.w320fixed)}
        radius
        flexdirection="column"
        background="white-100"
        style={{ boxShadow: '0px 10px 30px rgba(192, 196, 205, 0.3)' }}
        border="grey-20"
      >
        <DatePicker name="calendar" selected={new Date()} onChange={() => {}} />
      </Container>
    </Container>
  </Modal>
);
