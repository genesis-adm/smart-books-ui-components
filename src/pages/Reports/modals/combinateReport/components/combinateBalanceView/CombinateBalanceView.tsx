import { ContentLoader } from 'Components/base/ContentLoader';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { MainPageContentContainer } from 'Components/base/grid/MainPageContentContainer';
import { CurrencyTab, CurrencyTabWrapper } from 'Components/base/tabs/CurrencyTab';
import { Text } from 'Components/base/typography/Text';
import { Pagination } from 'Components/custom/Stories/Pagination/Pagination';
import PerPageSelect from 'Components/custom/Stories/Pagination/PerPageSelect/PerPageSelect';
import { FilterToggleButton } from 'Components/elements/filters/FilterToggleButton';
import { Search } from 'Components/inputs/Search';
import useSearchExample from 'Components/inputs/Search/hooks/useSearchExample';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { rowsPerPageOptions } from 'Mocks/fakeOptions';
import CombinateBalanceChosenSettings from 'Pages/Reports/modals/combinateReport/components/combinateBalanceView/components/CombinateBalanceChosenSettings';
import CombinateReportFilters from 'Pages/Reports/modals/combinateReport/components/combinateBalanceView/components/CombinateReportFilters';
import CombinateReportTable from 'Pages/Reports/modals/combinateReport/components/combinateBalanceView/components/CombinateReportTable';
import ReportHeaderPlaceholder from 'Pages/Reports/modals/combinateReport/components/combinateBalanceView/components/placeholders/ReportHeaderPlaceholder';
import ReportPlaceholder from 'Pages/Reports/modals/combinateReport/components/combinateBalanceView/components/placeholders/ReportsPlaceholder';
import { ReactComponent as CSVIcon } from 'Svg/v2/16/file-csv.svg';
import { ReactComponent as XLSIcon } from 'Svg/v2/16/file-xls.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../../../../assets/styles/spacing.module.scss';

type CombinateBalanceViewProps = {
  groupedBy: 'businessUnit' | 'legalEntity';
  isLoading: boolean;
};

const CombinateBalanceView = ({ groupedBy, isLoading }: CombinateBalanceViewProps) => {
  const [showFilters, setShowFilters] = useState<boolean>(false);
  const [currencyType, setCurrencyType] = useState<'original' | 'base'>('base');
  const [rowsPerPage, setRowsPerPage] = useState<string>(rowsPerPageOptions[0].value);

  const { searchValue, setSearchValue, handleSearchValueAdd, searchValuesList, handleClearAll } =
    useSearchExample();

  return (
    <Modal
      size="full"
      background={'grey-10'}
      overlay="grey-10"
      padding="none"
      header={
        isLoading ? (
          <ReportHeaderPlaceholder />
        ) : (
          <ModalHeaderNew
            title="Combinate trial balance"
            titlePosition="center"
            background={'grey-10'}
            closeActionBackgroundColor="inherit"
            onClose={() => {}}
          />
        )
      }
      pluginScrollDisabled
      footer={
        <ModalFooterNew justifycontent="space-between" border>
          <ContentLoader isLoading={isLoading} width="580px">
            <Pagination
              borderTop="none"
              padding="0"
              customPagination={
                <Container gap="16" alignitems="center">
                  <Text type="subtext-regular" color="grey-100">
                    Rows per Page
                  </Text>
                  <PerPageSelect
                    selectedValue={rowsPerPage}
                    onOptionClick={(value) => setRowsPerPage(value)}
                  />
                </Container>
              }
            />
          </ContentLoader>
          <Container gap="24">
            <ContentLoader isLoading={isLoading} width="120px">
              <Button
                background={'transparent-blue-10'}
                font="caption-semibold"
                height="40"
                color="grey-100-violet-90"
                iconLeft={<XLSIcon />}
                onClick={() => {}}
              >
                Export to XLS
              </Button>
            </ContentLoader>
            <ContentLoader isLoading={isLoading} width="120px">
              <Button
                background={'transparent-blue-10'}
                font="caption-semibold"
                height="40"
                color="grey-100-violet-90"
                iconLeft={<CSVIcon />}
                onClick={() => {}}
              >
                Export to CSV
              </Button>
            </ContentLoader>
          </Container>
        </ModalFooterNew>
      }
    >
      <Container
        justifycontent="space-between"
        alignitems="center"
        className={classNames(spacing.mX24)}
      >
        <CombinateBalanceChosenSettings groupedBy={groupedBy} isLoading={isLoading} />
      </Container>
      <MainPageContentContainer>
        {isLoading ? (
          <ReportPlaceholder />
        ) : (
          <>
            <Container
              className={classNames(spacing.mX24, spacing.mt24)}
              justifycontent="space-between"
              gap="24"
            >
              <Container gap="24" alignitems="center">
                <FilterToggleButton
                  showFilters={showFilters}
                  filtersSelected={false}
                  onClear={() => {}}
                  onClick={() => {
                    setShowFilters((prevState) => !prevState);
                  }}
                />
                <Search
                  width="440"
                  height="40"
                  name="search-reports-description-search"
                  placeholder="Search by Account name or Account ID"
                  value={searchValue}
                  onChange={(e) => setSearchValue(e.target.value)}
                  searchParams={searchValuesList}
                  onSearch={handleSearchValueAdd}
                  onClear={handleClearAll}
                />
              </Container>
              <CurrencyTabWrapper>
                <CurrencyTab
                  id="origin"
                  onClick={() => setCurrencyType('original')}
                  active={currencyType === 'original'}
                >
                  Original currency
                </CurrencyTab>
                <CurrencyTab
                  id="base"
                  onClick={() => setCurrencyType('base')}
                  active={currencyType === 'base'}
                >
                  Base currency USD
                </CurrencyTab>
              </CurrencyTabWrapper>
            </Container>
            <CombinateReportFilters showFilters={showFilters} />
            <CombinateReportTable />
          </>
        )}
      </MainPageContentContainer>
    </Modal>
  );
};

export default CombinateBalanceView;
