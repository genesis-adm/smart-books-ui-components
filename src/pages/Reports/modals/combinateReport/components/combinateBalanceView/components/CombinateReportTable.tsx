import { LinkButton } from 'Components/base/buttons/LinkButton';
import { Text } from 'Components/base/typography/Text';
import { ReportsTotalValues } from 'Components/custom/Reports/ReportsTotalValues';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableTitleNew } from 'Components/elements/Table/TableTitleNew';
import { TableCellWidth } from 'Components/types/gridTypes';
import { combinateBalanceTableHeader } from 'Pages/Reports/constants';
import classNames from 'classnames';
import React, { useRef } from 'react';

import spacing from '../../../../../../../assets/styles/spacing.module.scss';
import Tooltip from '../../../../../../../components/base/Tooltip/Tooltip';
import { useTooltipValue } from '../../../../../../../hooks/useTooltipValue';

const CombinateReportTable = () => {
  const accountNameRef = useRef(null);
  const accountNameTooltipMessage = useTooltipValue(accountNameRef, 'Payroll liabilities');

  return (
    <TableNew
      noResults={false}
      className={classNames(spacing.mt24, spacing.mX24)}
      tableHead={
        <TableHeadNew background={'grey-10'} radius="10">
          {combinateBalanceTableHeader.map((item, index) => (
            <TableTitleNew
              key={index}
              background={'grey-10'}
              radius="10"
              minWidth={item.minWidth}
              align={item.align}
              title={item.title}
              onClick={() => {}}
            />
          ))}
        </TableHeadNew>
      }
    >
      <TableBodyNew>
        {Array.from(Array(4)).map((_, index) => (
          <TableRowNew key={index} background={'info'} size="36" cursor="auto">
            <TableCellNew
              minWidth={combinateBalanceTableHeader[0].minWidth as TableCellWidth}
              justifycontent="space-between"
              fixedWidth
            >
              <LinkButton onClick={() => {}}>
                <Tooltip message={accountNameTooltipMessage}>
                  <Text
                    className={spacing.w200}
                    ref={accountNameRef}
                    color="violet-90"
                    type="caption-regular"
                    noWrap
                  >
                    Payroll liabilities
                  </Text>
                  <Text color="violet-90" type="caption-regular">
                    - USD | Liabilities
                  </Text>
                </Tooltip>
              </LinkButton>
              {/*<Text>Payroll liabilities - EUR</Text>*/}
              {/*<Text color="grey-100">Liabilities</Text>*/}
            </TableCellNew>
            <TableCellTextNew
              minWidth={combinateBalanceTableHeader[1].minWidth as TableCellWidth}
              value="9834"
              alignitems="center"
              fixedWidth
            />
            <TableCellTextNew
              minWidth={combinateBalanceTableHeader[2].minWidth as TableCellWidth}
              value="Legal entity"
              fixedWidth
            />
            <TableCellTextNew
              minWidth={combinateBalanceTableHeader[3].minWidth as TableCellWidth}
              value="Business unit name"
              fixedWidth
            />
            <TableCellTextNew
              minWidth={combinateBalanceTableHeader[4].minWidth as TableCellWidth}
              value="USD"
              alignitems="center"
              fixedWidth
            />
            <TableCellTextNew
              minWidth={combinateBalanceTableHeader[5].minWidth as TableCellWidth}
              type={'caption-semibold'}
              value="1,000.00 $"
              alignitems="flex-end"
              fixedWidth
            />
            <TableCellTextNew
              minWidth={combinateBalanceTableHeader[6].minWidth as TableCellWidth}
              value="2,000.00 $"
              type={'caption-semibold'}
              alignitems="flex-end"
              fixedWidth
            />
          </TableRowNew>
        ))}
        <ReportsTotalValues
          type="balance"
          title="Total"
          data={[['EUR', '2,000.00 €', '1,000.00 €']]}
        />
      </TableBodyNew>
    </TableNew>
  );
};

export default CombinateReportTable;
