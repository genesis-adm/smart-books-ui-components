import { ContentLoader } from 'Components/base/ContentLoader';
import { Divider } from 'Components/base/Divider';
import { Tag } from 'Components/base/Tag';
import { IconButton } from 'Components/base/buttons/IconButton';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ReactComponent as EditIcon } from 'Svg/v2/12/pencil.svg';
import classNames from 'classnames';
import { format } from 'date-fns';
import React, { ReactNode } from 'react';

import spacing from '../../../../../../../assets/styles/spacing.module.scss';

type CombinateBalanceChosenSettingsProps = {
  groupedBy: 'businessUnit' | 'legalEntity';
  isLoading: boolean;
};

const CombinateBalanceChosenSettings = ({
  groupedBy,
  isLoading,
}: CombinateBalanceChosenSettingsProps) => {
  const legalEntitiesList = [
    { name: 'Matar and Trade Company LLC' },
    { name: 'Legal entity name 1' },
  ];

  const businessUnitsList = [
    { name: 'Smartbooks', type: 'Group' },
    { name: 'Name 1', type: 'Project' },
    { name: 'Smartbooks', type: 'Group' },
    { name: 'Smartbooks', type: 'Group' },
    { name: 'Name ', type: 'Subproject' },
  ];

  const settingsByGroup: Record<
    'businessUnit' | 'legalEntity',
    { name: string; buttonTooltip: string; dropdown: ReactNode }
  > = {
    legalEntity: {
      name: 'Legal entity',
      buttonTooltip: 'Edit Date or Legal entity',
      dropdown: (
        <Container gap="16" flexdirection="column">
          <Text color="grey-100" type="button">
            Legal entities
          </Text>
          <Container flexdirection="column" gap="12">
            {legalEntitiesList.map((le) => (
              <Text key={le.name} type="body-regular-14">
                {le.name}
              </Text>
            ))}
          </Container>
        </Container>
      ),
    },
    businessUnit: {
      name: 'Business unit',
      buttonTooltip: 'Edit Date or Business unit',
      dropdown: (
        <Container
          gap="16"
          flexdirection="column"
          className={classNames(spacing.w332fixed, spacing.h300)}
        >
          <Text color="grey-100" type="button">
            Business units
          </Text>
          <Container flexdirection="column" gap="12">
            {businessUnitsList.map((bu) => (
              <Container key={bu.name} gap="4">
                <Text type="body-regular-14" color="grey-90">
                  {bu.type}:
                </Text>
                <Text type="body-regular-14">{bu.name}</Text>
              </Container>
            ))}
          </Container>
        </Container>
      ),
    },
  };

  if (isLoading) return <ContentLoader isLoading width="424px" height="76px" />;

  return (
    <Container
      alignitems="center"
      gap="16"
      className={classNames(spacing.h76, spacing.p16)}
      border="grey-30"
      borderRadius="20"
    >
      <Container flexdirection="column" gap="8">
        <Text color="grey-100" className={spacing.ml4}>
          As of date
        </Text>
        <Tag
          color={'grey-black'}
          text={format(new Date(), 'dd.MM.yyyy')}
          align="left"
          noWrap
          cursor="default"
        />
      </Container>

      <Container flexdirection="column" gap="8">
        <Text color="grey-100" className={spacing.ml4}>
          {settingsByGroup[groupedBy].name}
        </Text>
        <Container gap="16" alignitems="center">
          <Tag
            size="24"
            padding="0-4"
            cursor="default"
            className={classNames(spacing.w350)}
            color="grey-black"
            text="Matar and Trade Company LLC"
            additionalDropdownBlock={{
              icon: <Text type="body-regular-14">+1</Text>,
              padding: '16',
              customScroll: true,
              dropdown: settingsByGroup[groupedBy].dropdown,
            }}
            truncateText
          />
          <Divider type="vertical" height="26" background={'grey-30'} />
          <IconButton
            icon={<EditIcon />}
            size="24"
            background={'grey-20-grey-30'}
            tooltip={settingsByGroup[groupedBy].buttonTooltip}
          />
        </Container>
      </Container>
    </Container>
  );
};

export default CombinateBalanceChosenSettings;
