import { ContentLoader } from 'Components/base/ContentLoader';
import { Container } from 'Components/base/grid/Container';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../../../../../../assets/styles/spacing.module.scss';

const ReportHeaderPlaceholder = () => {
  return (
    <Container
      justifycontent="space-between"
      alignitems="center"
      className={classNames(spacing.w100p, spacing.p24)}
    >
      <Container justifycontent="center" className={spacing.w100p}>
        <ContentLoader isLoading height="24px" width="360px" />
      </Container>
      <Container justifycontent="flex-end" gap="16">
        <ContentLoader height="24px" width="24px" type="circle" isLoading />
      </Container>
    </Container>
  );
};
export default ReportHeaderPlaceholder;
