import { Divider } from 'Components/base/Divider';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { FilterDropDown } from 'Components/elements/filters/FilterDropDown';
import { FilterDropDownItem } from 'Components/elements/filters/FilterDropDownItem';
import { FilterItemsContainer } from 'Components/elements/filters/FilterItemsContainer';
import { FilterSearch } from 'Components/elements/filters/FilterSearch';
import { FiltersWrapper } from 'Components/elements/filters/FiltersWrapper';
import { options } from 'Mocks/fakeOptions';
import { combinateBalanceFilters } from 'Pages/Reports/constants';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../../../../../assets/styles/spacing.module.scss';

type CombinateReportFiltersProps = { showFilters: boolean };

const CombinateReportFilters = ({ showFilters }: CombinateReportFiltersProps) => {
  const [searchValue, setSearchValue] = useState<string>('');

  return (
    <FiltersWrapper active={showFilters}>
      {combinateBalanceFilters.map((item) => (
        <FilterDropDown
          key={item.title}
          title={item.title}
          value="All"
          onClear={() => {}}
          selectedAmount={0}
        >
          <Container className={classNames(spacing.m16, spacing.w288fixed)} flexdirection="column">
            <FilterSearch
              title={`${item.title}:`}
              value={searchValue}
              onChange={(val) => setSearchValue(val)}
            />
            <FilterItemsContainer className={spacing.mt8}>
              {options.map((item) => (
                <FilterDropDownItem
                  key={item.value}
                  id={item.value}
                  type="item"
                  checked={false}
                  blurred={false}
                  value={item.label}
                  onChange={() => {}}
                />
              ))}
            </FilterItemsContainer>
          </Container>
          <Divider fullHorizontalWidth />
          <Container
            className={classNames(spacing.m16, spacing.w288fixed)}
            justifycontent="flex-end"
            alignitems="center"
          >
            <Button
              width="auto"
              height="32"
              padding="8"
              font="text-medium"
              onClick={() => {}}
              background={'violet-90-violet-100'}
              color="white-100"
            >
              Apply
            </Button>
          </Container>
        </FilterDropDown>
      ))}
    </FiltersWrapper>
  );
};

export default CombinateReportFilters;
