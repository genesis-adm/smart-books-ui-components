import { ContentLoader } from 'Components/base/ContentLoader';
import { Container } from 'Components/base/grid/Container';
import TablePlaceholder from 'Components/custom/placeholders/TablePlaceholder';
import React from 'react';

import spacing from '../../../../../../../../assets/styles/spacing.module.scss';

const ReportPlaceholder = () => (
  <Container flexdirection="column">
    <Container gap="20" flexdirection="column" className={spacing.p24}>
      <Container justifycontent="space-between">
        <ContentLoader isLoading width="200px" />
      </Container>
      <Container justifycontent="space-between">
        <ContentLoader isLoading width="400px" />
        <ContentLoader isLoading width="200px" />
      </Container>
    </Container>
    <TablePlaceholder />
  </Container>
);

export default ReportPlaceholder;
