import { ContentLoader } from 'Components/base/ContentLoader';
import { DatePicker } from 'Components/base/DatePicker';
import { Divider } from 'Components/base/Divider';
import { Radio } from 'Components/base/Radio';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import BusinessUnitMultiChoice from 'Pages/Reports/modals/combinateReport/components/combinateBalanceSettings/components/BusinessUnitMultiChoice';
import LegalEntityMultiChoice from 'Pages/Reports/modals/combinateReport/components/combinateBalanceSettings/components/LegalEntityMultiChoice';
import ReportHeaderPlaceholder from 'Pages/Reports/modals/combinateReport/components/combinateBalanceView/components/placeholders/ReportHeaderPlaceholder';
import { ReactComponent as ArrowRightIcon } from 'Svg/v2/16/arrow-right.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../../../../assets/styles/spacing.module.scss';

type CombinateBalanceSettingsModalProps = {
  isLoading: boolean;
};

const CombinateBalanceSettingsModal = ({ isLoading }: CombinateBalanceSettingsModalProps) => {
  const [isGroupedBy, setIsGroupedBy] = useState<'legalEntity' | 'businessUnit'>('legalEntity');

  return (
    <Modal
      size="990"
      padding="none"
      header={
        isLoading ? (
          <ReportHeaderPlaceholder />
        ) : (
          <ModalHeaderNew
            title="Combinate trial balance"
            titlePosition="center"
            closeActionBackgroundColor="inherit"
            onClose={() => {}}
          />
        )
      }
      footer={
        <ModalFooterNew justifycontent="space-between" border>
          <ContentLoader isLoading={isLoading} width="120px">
            <Button
              iconRight={<ArrowRightIcon />}
              height="48"
              navigation
              width="120"
              background={'grey-90'}
              onClick={() => {}}
            >
              Next
            </Button>
          </ContentLoader>
        </ModalFooterNew>
      }
    >
      <Container justifycontent="center" gap="16" alignitems="center">
        <ContentLoader isLoading={isLoading} width="310px" height="24px">
          <Text type="caption-regular" color="grey-100">
            Group by:
          </Text>
          <Radio
            type="caption-regular"
            id="le"
            name="sort"
            color={isGroupedBy === 'legalEntity' ? 'violet-90' : 'grey-90'}
            checked={isGroupedBy === 'legalEntity'}
            onChange={() => {
              setIsGroupedBy('legalEntity');
            }}
          >
            Legal entity
          </Radio>
          <Radio
            type="caption-regular"
            id="bu"
            name="sort"
            color={isGroupedBy === 'businessUnit' ? 'violet-90' : 'grey-90'}
            checked={isGroupedBy === 'businessUnit'}
            onChange={() => {
              setIsGroupedBy('businessUnit');
            }}
          >
            Business unit
          </Radio>
        </ContentLoader>
      </Container>
      <Container
        justifycontent="space-between"
        className={classNames(spacing.pY24, spacing.pX80, spacing.h100p)}
        gap="24"
      >
        {isGroupedBy === 'businessUnit' && <BusinessUnitMultiChoice isLoading={isLoading} />}
        {isGroupedBy === 'legalEntity' && <LegalEntityMultiChoice isLoading={isLoading} />}
        <Divider type="vertical" height="full" />
        <Container
          flexdirection="column"
          alignitems="center"
          flexgrow="1"
          gap="16"
          className={classNames(spacing.w360, spacing.pb16, spacing.pX16)}
          radius
        >
          <ContentLoader isLoading={isLoading} width="180px" height="28px">
            <Text type="body-medium">Choose Date</Text>
          </ContentLoader>

          <ContentLoader isLoading={isLoading} width="320px" height="296px">
            <Container
              radius
              flexdirection="column"
              background={'white-100'}
              style={{ boxShadow: '0px 10px 30px rgba(192, 196, 205, 0.3)' }}
              border="grey-20"
            >
              <DatePicker name="calendar" selected={new Date()} onChange={() => {}} />
            </Container>
          </ContentLoader>
        </Container>
      </Container>
    </Modal>
  );
};

export default CombinateBalanceSettingsModal;
