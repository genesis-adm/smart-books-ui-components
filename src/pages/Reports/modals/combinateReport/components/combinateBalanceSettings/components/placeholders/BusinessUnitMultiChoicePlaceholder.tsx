import { ContentLoader } from 'Components/base/ContentLoader';
import { Container } from 'Components/base/grid/Container';
import React from 'react';

const BusinessUnitMultiChoicePlaceholder = ({ isLoading }: { isLoading: boolean }) => (
  <Container flexdirection="column" alignitems={'center'} justifycontent="center" gap="16">
    <ContentLoader isLoading={isLoading} width="180px" height="28px" />
    <ContentLoader isLoading={isLoading} width="350px" height="40px" />
    <ContentLoader isLoading={isLoading} width="65px" height="16px" />
    <ContentLoader isLoading={isLoading} width="350px" height="40px" />
    <ContentLoader isLoading={isLoading} width="65px" height="16px" />
    {Array.from(Array(4)).map((_, index) => (
      <ContentLoader key={index} isLoading={isLoading} width="350px" height="40px" />
    ))}
  </Container>
);

export default BusinessUnitMultiChoicePlaceholder;
