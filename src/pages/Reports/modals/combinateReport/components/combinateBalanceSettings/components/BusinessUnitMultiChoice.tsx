import ExpandableCheckbox from 'Components/base/ExpandableCheckbox/ExpandableCheckbox';
import {
  ExpandableCheckboxGroup,
  ExpandableCheckboxItem,
} from 'Components/base/ExpandableCheckboxItem';
import { Icon } from 'Components/base/Icon';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { businessUnits } from 'Pages/Reports/constants';
import BusinessUnitSearch from 'Pages/Reports/modals/combinateReport/components/combinateBalanceSettings/components/BusinessUnitSearch';
import BusinessUnitMultiChoicePlaceholder from 'Pages/Reports/modals/combinateReport/components/combinateBalanceSettings/components/placeholders/BusinessUnitMultiChoicePlaceholder';
import { BusinessUnitsSearchedValuesContext } from 'Pages/Reports/modals/combinateReport/context/BusinessUnitsSearchedValuesContext';
import { useExpandableCheckboxData } from 'Pages/Reports/modals/combinateReport/hooks/useExpandableCheckboxData';
import { getAllDescendantIds } from 'Pages/Reports/modals/combinateReport/utils';
import { ReactComponent as BuildingsIcon } from 'Svg/v2/12/buildings.svg';
import { ReactComponent as GlobusIcon } from 'Svg/v2/12/globus.svg';
import { ReactComponent as DocumentIcon } from 'Svg/v2/16/document.svg';
import { ReactComponent as SmartphoneIcon } from 'Svg/v2/16/smartphone.svg';
import React, { useState } from 'react';

type BusinessUnitMultiChoiceProps = {
  isLoading: boolean;
};

const BusinessUnitMultiChoice = ({ isLoading }: BusinessUnitMultiChoiceProps) => {
  const { modifiedData, handleSelect, handleSelectByIds, handleSelectAll, allSelected } =
    useExpandableCheckboxData({
      data: businessUnits,
    });
  const [checkedSearchedItemIds, setCheckedSearchedItemIds] = useState<string[]>([]);

  const handleDeleteSearchedValue = (ids: string[]) => {
    const filteredValues = checkedSearchedItemIds.filter((itemId) => !ids.includes(itemId));
    setCheckedSearchedItemIds(filteredValues);
  };

  if (isLoading) return <BusinessUnitMultiChoicePlaceholder isLoading={isLoading} />;

  return (
    <Container flexdirection="column" gap="16" alignitems="center">
      <Text type="body-medium">Choose Business unit</Text>
      <BusinessUnitsSearchedValuesContext.Provider
        value={{ checkedSearchedItemIds, setCheckedSearchedItemIds }}
      >
        <BusinessUnitSearch handleSelectByIds={handleSelectByIds} />
      </BusinessUnitsSearchedValuesContext.Provider>
      <Container flexdirection="column" alignitems="center" gap="8">
        <Container gap="4">
          <Icon icon={<GlobusIcon />} />
          <Text color="grey-90" type="subtext-regular">
            Organisation:
          </Text>
          <Text color="grey-100" type="subtext-regular">
            Including 4 Group
          </Text>
        </Container>
        <ExpandableCheckbox
          id={'genesis'}
          checkboxName={'Genesis'}
          checked={allSelected}
          checkboxTooltip={!allSelected ? 'Select all Group' : ''}
          onCheckboxChange={() => {
            handleSelectAll();
          }}
        />
      </Container>
      <Container flexdirection="column" gap="4" alignitems="center">
        <Container gap="4">
          <Icon icon={<BuildingsIcon />} />
          <Text color="grey-90" type="subtext-regular">
            Group:
          </Text>
          <Text color="grey-100" type="subtext-regular">
            {businessUnits.length}
          </Text>
        </Container>
        <Container gap="16" flexdirection="column">
          {modifiedData.map((unit) => (
            <ExpandableCheckbox
              key={unit.id}
              id={unit.id}
              checkboxName={unit.name}
              checked={!!unit.checked}
              onCheckboxChange={() => {
                handleSelect(unit);
              }}
              checkboxTooltip={unit.checked ? 'Unselect Group' : ''}
              collapseBtnTitle={`${unit.children?.length} Project`}
              showCollapseBtn={!!unit.children?.length}
              showBadge={checkedSearchedItemIds.some((item) => {
                const allIds = getAllDescendantIds(unit);
                return allIds.includes(item);
              })}
              onToggle={() =>
                unit.children && handleDeleteSearchedValue(unit.children?.map((child) => child.id))
              }
            >
              {unit.children &&
                unit.children.map((project, index) => (
                  <ExpandableCheckboxGroup
                    key={project.id}
                    firstChild={index === 0}
                    groupName="Projects"
                    groupIcon={<SmartphoneIcon />}
                  >
                    <ExpandableCheckboxItem
                      key={project.id}
                      id={project.id}
                      showCollapseBtn={!!project.children?.length}
                      collapseBtnTitle={`${project.children?.length} Subproject`}
                      checkboxName={project.name}
                      checked={!!project.checked}
                      itemType={'Subproject'}
                      onCheckboxChange={() => {
                        handleSelect(project);
                      }}
                      showBadge={checkedSearchedItemIds.some((item) => {
                        const allIds = getAllDescendantIds(project);
                        return allIds.includes(item);
                      })}
                      onToggle={() =>
                        project.children &&
                        handleDeleteSearchedValue(project.children?.map((child) => child.id))
                      }
                    >
                      {project.children &&
                        project.children.map((subproject, index) => (
                          <ExpandableCheckboxGroup
                            key={subproject.id}
                            firstChild={index === 0}
                            groupName="Subproject"
                            groupIcon={<DocumentIcon />}
                          >
                            <ExpandableCheckboxItem
                              key={subproject.id}
                              id={subproject.id}
                              checkboxName={subproject.name}
                              checked={!!subproject.checked}
                              showCollapseBtn={!!subproject.children?.length}
                              onCheckboxChange={() => {
                                handleSelect(subproject);
                              }}
                            />
                          </ExpandableCheckboxGroup>
                        ))}
                    </ExpandableCheckboxItem>
                  </ExpandableCheckboxGroup>
                ))}
            </ExpandableCheckbox>
          ))}
        </Container>
      </Container>
    </Container>
  );
};

export default BusinessUnitMultiChoice;
