import { DropDown } from 'Components/DropDown';
import { Search } from 'Components/inputs/Search';
import { businessUnits } from 'Pages/Reports/constants';
import BusinessUnitSearchDropdownBody from 'Pages/Reports/modals/combinateReport/components/combinateBalanceSettings/components/BusinessUnitSearchDropdownBody';
import { GroupType } from 'Pages/Reports/modals/combinateReport/hooks/useExpandableCheckboxData';
import { getMembers } from 'Pages/Reports/modals/combinateReport/utils';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../../../../../assets/styles/spacing.module.scss';

type BusinessUnitSearchProps = {
  handleSelectByIds(list: string[], allData: GroupType[]): void;
};

const BusinessUnitSearch = ({ handleSelectByIds }: BusinessUnitSearchProps) => {
  const [searchValue, setSearchValue] = useState<string>('');

  const businessUnitsFilterData = getMembers(businessUnits);

  const [isOpenedDrop, setIsOpenedDrop] = useState<boolean>(false);

  const handleCloseDrop = () => setIsOpenedDrop(false);
  const handleOpenDrop = () => setIsOpenedDrop(true);

  return (
    <DropDown
      padding={'none'}
      flexdirection="column"
      style={{ width: '100%', height: '100%' }}
      opened={isOpenedDrop}
      stopDropdownPropagation
      classnameInner={classNames(spacing.w350fixed, spacing.h250fixed)}
      control={() => (
        <Search
          className={classNames(spacing.mb8, spacing.mt4)}
          width="350"
          height="40"
          name="search"
          placeholder="Search Business unit"
          value={searchValue}
          onChange={(e) => {
            setSearchValue(e.target.value);
          }}
          isFocused={isOpenedDrop}
          searchParams={[]}
          onSearch={() => {}}
          onClear={() => {
            setSearchValue('');
          }}
          onClick={handleOpenDrop}
          onBlur={handleCloseDrop}
          onFocus={handleOpenDrop}
        />
      )}
    >
      <BusinessUnitSearchDropdownBody
        closeDrop={handleCloseDrop}
        searchValue={searchValue}
        handleSelectByIds={handleSelectByIds}
        filterData={businessUnitsFilterData}
      />
    </DropDown>
  );
};

export default BusinessUnitSearch;
