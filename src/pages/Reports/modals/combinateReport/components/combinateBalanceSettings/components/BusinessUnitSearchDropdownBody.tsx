import { Divider } from 'Components/base/Divider';
import { Icon } from 'Components/base/Icon';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { FilterDropDownItem } from 'Components/elements/filters/FilterDropDownItem';
import { FilterItemsContainer } from 'Components/elements/filters/FilterItemsContainer';
import { BusinessUnitsSearchedValuesContext } from 'Pages/Reports/modals/combinateReport/context/BusinessUnitsSearchedValuesContext';
import { GroupType } from 'Pages/Reports/modals/combinateReport/hooks/useExpandableCheckboxData';
import { useSearchedData } from 'Pages/Reports/modals/combinateReport/hooks/useSearchedData';
import { ReactComponent as SearchIcon } from 'Svg/v2/16/search.svg';
import classNames from 'classnames';
import React, { useContext, useState } from 'react';

import spacing from '../../../../../../../assets/styles/spacing.module.scss';

type BusinessUnitSearchDropdownBodyProps = {
  searchValue: string;
  filterData: GroupType[];

  handleSelectByIds(list: string[], allData: GroupType[]): void;

  closeDrop(): void;
};

const BusinessUnitSearchDropdownBody = ({
  searchValue,
  closeDrop,
  filterData,
  handleSelectByIds,
}: BusinessUnitSearchDropdownBodyProps) => {
  const { searchedList } = useSearchedData({ searchValue, list: filterData.slice() });

  const [modifiedSearchedList, setModifiedSearchList] = useState<GroupType[]>(filterData.slice());
  const [modifiedSearchedValues, setModifiedSearchValues] = useState<string[]>([]);
  const { setCheckedSearchedItemIds } = useContext(BusinessUnitsSearchedValuesContext);

  const handleCheckItem = (id: string) => {
    const newList = modifiedSearchedList.map((item) => {
      item.id === id ? (item.checked = !item.checked) : item.checked;
      return item;
    });
    setModifiedSearchList(newList.slice());
    setModifiedSearchValues([...modifiedSearchedValues, id]);
  };

  const handleApply = () => {
    handleSelectByIds(modifiedSearchedValues, modifiedSearchedList);
    const checkedItems = modifiedSearchedList.filter((item) => {
      if (item.checked) {
        return item;
      }
    });
    setCheckedSearchedItemIds(checkedItems.map((item) => item?.id));
    closeDrop();
    setModifiedSearchValues([]);
  };

  return (
    <Container
      flexdirection="column"
      justifycontent={'space-between'}
      className={classNames(spacing.w100p, spacing.h100p)}
    >
      {!searchValue ? (
        <Container
          className={classNames(spacing.w100p, spacing.h100p)}
          justifycontent="center"
          alignitems="center"
          gap="4"
        >
          <Icon icon={<SearchIcon />} path={'grey-90'} />
          <Text type="body-regular-14" color="grey-90">
            Search results
          </Text>
        </Container>
      ) : (
        <>
          <FilterItemsContainer className={classNames(spacing.p16)}>
            {searchedList.map((bu) => (
              <FilterDropDownItem
                key={bu.id}
                id={bu.id}
                type="item"
                checked={!!bu.checked}
                blurred={false}
                value={bu.name}
                onChange={() => handleCheckItem(bu.id)}
              />
            ))}
          </FilterItemsContainer>

          <Container flexdirection="column">
            <Divider fullHorizontalWidth />

            <Container
              className={classNames(spacing.p16, spacing.w100p, spacing.h56)}
              justifycontent="flex-end"
              alignitems="center"
            >
              <Button
                width="auto"
                height="24"
                padding="8"
                font="text-medium"
                onClick={() => {
                  handleApply();
                }}
                disabled={!modifiedSearchedValues.length}
                background={'violet-90-violet-100'}
                color="white-100"
              >
                Apply
              </Button>
            </Container>
          </Container>
        </>
      )}
    </Container>
  );
};

export default BusinessUnitSearchDropdownBody;
