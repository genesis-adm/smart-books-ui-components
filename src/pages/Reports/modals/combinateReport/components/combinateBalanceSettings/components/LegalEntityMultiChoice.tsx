import ExpandableCheckbox from 'Components/base/ExpandableCheckbox/ExpandableCheckbox';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { FilterEmpty } from 'Components/elements/filters/FilterEmpty';
import { Search } from 'Components/inputs/Search';
import LegalEntityMultiChoicePlaceholder from 'Pages/Reports/modals/combinateReport/components/combinateBalanceSettings/components/placeholders/LegalEntityMultiChoicePlaceholder';
import classNames from 'classnames';
import React, { useMemo, useState } from 'react';

import spacing from '../../../../../../../assets/styles/spacing.module.scss';

type LegalEntity = { id: string; name: string };
const legalEntities: LegalEntity[] = [
  { id: 'le-1', name: 'Matar and Trade Company LLC' },
  { id: 'le-2', name: 'Legal entity 2' },
  { id: 'le-3', name: 'Legal entity 3' },
  { id: 'le-4', name: 'Legal entity 4' },
  { id: 'le-5', name: 'Legal entity 5' },
];

type LegalEntityMultiChoiceProps = {
  isLoading: boolean;
};

const LegalEntityMultiChoice = ({ isLoading }: LegalEntityMultiChoiceProps) => {
  const [selectedLE, setSelectedLE] = useState<string[]>([]);
  const [searchValue, setSearchValue] = useState<string>('');

  const handleSelect = (id: string) => {
    if (!selectedLE.find((leId) => id === leId)) {
      setSelectedLE([...selectedLE, id]);
    } else {
      const filteredItems = selectedLE.filter((leId) => id !== leId);
      setSelectedLE(filteredItems);
    }
  };

  const filteredLegalEntityList = useMemo(
    () =>
      legalEntities?.filter((le) => le?.name?.toLowerCase().includes(searchValue.toLowerCase())),
    [legalEntities, searchValue],
  );

  const otherItems = filteredLegalEntityList.filter(
    (le) => !selectedLE?.find((selected) => le.id === selected),
  );

  if (isLoading) return <LegalEntityMultiChoicePlaceholder isLoading={isLoading} />;

  return (
    <Container flexdirection="column" gap="12">
      <Container flexdirection="column" gap="16" alignitems="center">
        <Text type="body-medium">Choose Legal entity</Text>
        <Search
          className={classNames(spacing.mb8, spacing.mt4)}
          width="350"
          height="40"
          name="search"
          placeholder="Search Legal entity"
          value={searchValue}
          onChange={(e) => {
            setSearchValue(e.target.value);
          }}
          searchParams={[]}
          onSearch={() => {}}
          onClear={() => {
            setSearchValue('');
          }}
        />
      </Container>
      {!filteredLegalEntityList?.length ? (
        <FilterEmpty />
      ) : (
        <>
          <Container flexdirection="column" gap="8">
            <Text type="text-regular" color="grey-100">
              {`Selected: ${selectedLE.length}`}
            </Text>
            <Container flexdirection="column" gap="16">
              {filteredLegalEntityList
                .filter((le) => selectedLE?.find((selected) => le.id === selected))
                .map((le) => (
                  <ExpandableCheckbox
                    key={le.id}
                    id={le.id}
                    checkboxName={le.name}
                    checked={!!selectedLE.find((id) => le.id === id)}
                    onCheckboxChange={() => {
                      handleSelect(le.id);
                    }}
                  />
                ))}
            </Container>
          </Container>
          <Container flexdirection="column" gap="8">
            {!!selectedLE.length && !!otherItems.length && (
              <Text type="text-regular" color="grey-100">
                Other:
              </Text>
            )}
            <Container flexdirection="column" gap="16">
              {otherItems.map((item) => (
                <ExpandableCheckbox
                  key={item.id}
                  id={item.id}
                  checkboxName={item.name}
                  checked={!!selectedLE.find((le) => le === item.id)}
                  onCheckboxChange={() => {
                    handleSelect(item.id);
                  }}
                />
              ))}
            </Container>
          </Container>
        </>
      )}
    </Container>
  );
};

export default LegalEntityMultiChoice;
