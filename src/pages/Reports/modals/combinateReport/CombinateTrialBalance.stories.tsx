import { Meta, Story } from '@storybook/react';
import { ModalProps } from 'Components/modules/Modal/Modal.types';
import CombinateBalanceSettingsModal from 'Pages/Reports/modals/combinateReport/components/combinateBalanceSettings/CombinateBalanceSettingsModal';
import CombinateBalanceView from 'Pages/Reports/modals/combinateReport/components/combinateBalanceView/CombinateBalanceView';
import React, { ReactNode } from 'react';

const CombinateTrialBalance: Story = ({ storyState, step, viewBalanceBy }) => {
  const isLoading = storyState === 'loading';

  const modalStep: Record<
    'settings' | 'view',
    { modalProps: ModalProps; render: () => ReactNode }
  > = {
    settings: {
      modalProps: {
        size: '990',
      },
      render: () => <CombinateBalanceSettingsModal isLoading={isLoading} />,
    },
    view: {
      modalProps: { size: 'full', background: 'grey-10', overlay: 'grey-10' },
      render: () => <CombinateBalanceView groupedBy={viewBalanceBy} isLoading={isLoading} />,
    },
  };

  return <>{modalStep[step as 'settings' | 'view'].render()}</>;
};

export default {
  title: 'Pages/Reports/Reports modals/Combinate Report',
  component: CombinateTrialBalance,
  argTypes: {
    storyState: {
      options: ['empty', 'loading', 'loaded'],
      control: {
        type: 'radio',
        labels: {
          empty: 'Empty page',
          loading: 'Page is loading',
          loaded: 'Page loaded',
        },
      },
    },
    step: {
      options: ['settings', 'view'],
      control: { type: 'radio', labels: { settings: 'Settings', view: 'View' } },
    },
    viewBalanceBy: {
      options: ['businessUnit', 'legalEntity'],
      control: {
        type: 'radio',
        labels: { businessUnit: 'Business unit', legalEntity: 'Legal entity' },
      },
    },
  },
} as Meta;

export const CombinateReport = CombinateTrialBalance.bind({});
CombinateReport.args = {
  storyState: 'loaded',
  step: 'settings',
  viewBalanceBy: 'legalEntity',
};
