import { Dispatch, SetStateAction, createContext } from 'react';

export const BusinessUnitsSearchedValuesContext = createContext<{
  checkedSearchedItemIds: string[];
  setCheckedSearchedItemIds: Dispatch<SetStateAction<string[]>>;
}>({ checkedSearchedItemIds: [], setCheckedSearchedItemIds: () => {} });
