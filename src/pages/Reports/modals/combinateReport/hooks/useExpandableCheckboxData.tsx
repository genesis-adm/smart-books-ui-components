import { useState } from 'react';

export type GroupType = {
  id: string;
  name: string;
  checked?: boolean;
  children?: GroupType[];
};

type HookData = {
  data: GroupType[];
};

type HookReturnedData = {
  modifiedData: GroupType[];
  handleSelect(node: GroupType): void;
  handleSelectByIds(list: string[], allData: GroupType[]): void;
  handleSelectAll(): void;
  allSelected: boolean;
  areAnyChildSelected(parentItem: GroupType): boolean;
};

export const useExpandableCheckboxData = ({ data }: HookData): HookReturnedData => {
  const [modifiedData, setModifiedData] = useState<GroupType[]>(data);

  const areAnyChildSelected = (parentItem: GroupType): boolean => {
    if (!parentItem.children) return false;
    return parentItem.children.some((child) => child.checked || areAnyChildSelected(child));
  };

  const handleCheckParent = (group: GroupType) => {
    if (group.children) {
      group.children.forEach((child) => handleCheckParent(child));

      group.checked = group.children.every((child) => child.checked);
    }
  };

  const handleCheckChildren = (group: GroupType) => {
    if (group.children) {
      group.children.forEach((child) => {
        child.checked = group.checked;
        handleCheckChildren(child);
      });
    }
  };

  const handleSelect = (group: GroupType) => {
    group.checked = !group.checked;
    handleCheckChildren(group);
    data.forEach((node) => handleCheckParent(node));
    setModifiedData(data.slice());
  };

  const allSelected = modifiedData.every((child) => child.checked);

  const selectAll = (data: GroupType[]) => {
    data.forEach((item) => {
      item.checked = !allSelected;
      if (item.children) {
        selectAll(item.children);
      }
    });
    return data;
  };

  const handleSelectAll = () => {
    const checkedAllData = selectAll(modifiedData);
    setModifiedData(checkedAllData.slice());
  };

  const getCurrentItem = (items: GroupType[], id: string): GroupType | null => {
    for (let item of items) {
      if (item.id === id) {
        return item;
      }

      if (item.children) {
        const foundNode = getCurrentItem(item.children, id);
        if (foundNode) {
          return foundNode;
        }
      }
    }

    return null;
  };

  const handleSelectByIds = (idsList: string[], allData: GroupType[]) => {
    const newDataList = modifiedData.map((item) => ({ ...item }));

    idsList.forEach((id) => {
      const item = getCurrentItem(allData, id);
      const modifiedListItem = getCurrentItem(newDataList, id);
      if (modifiedListItem && item) {
        modifiedListItem.checked = !!item.checked;
      }

      if (modifiedListItem) {
        handleCheckChildren(modifiedListItem);
      }
    });

    newDataList.forEach((item) => handleCheckParent(item));
    setModifiedData(newDataList);
  };

  return {
    handleSelect,
    handleSelectByIds,
    handleSelectAll,
    allSelected,
    modifiedData,
    areAnyChildSelected,
  };
};
