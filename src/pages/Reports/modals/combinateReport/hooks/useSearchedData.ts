import { GroupType } from 'Pages/Reports/modals/combinateReport/hooks/useExpandableCheckboxData';
import { useEffect, useState } from 'react';

type HookData = {
  searchValue: string;
  list: GroupType[];
};

type HookReturnedData = {
  searchedList: GroupType[];
};

export const useSearchedData = ({ searchValue, list }: HookData): HookReturnedData => {
  const [searchedList, setSearchedList] = useState<GroupType[]>([]);

  useEffect(() => {
    const filteredList = list.filter((item) =>
      item.name.toLowerCase().includes(searchValue.toLowerCase()),
    );
    setSearchedList(filteredList);
  }, [searchValue]);

  return { searchedList };
};
