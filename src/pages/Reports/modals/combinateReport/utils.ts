import { GroupType } from 'Pages/Reports/modals/combinateReport/hooks/useExpandableCheckboxData';

export const getMembers = (members: GroupType[]): GroupType[] => {
  let children: GroupType[] = [];

  return members
    .map((m) => {
      if (m.children && m.children.length) {
        children = [...children, ...m.children];
      }
      return m;
    })
    .concat(children.length ? getMembers(children) : children);
};

export const getAllDescendantIds = (item: GroupType) => {
  let ids = [item.id];
  if (item.children) {
    item.children.forEach((child) => {
      ids = ids.concat(getAllDescendantIds(child));
    });
  }
  return ids;
};
