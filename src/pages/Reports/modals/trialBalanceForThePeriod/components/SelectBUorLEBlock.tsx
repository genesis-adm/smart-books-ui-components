import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { ReportsCreateItem } from 'Components/custom/Reports/ReportsCreateItem';
import { Search } from 'Components/inputs/Search';
import classNames from 'classnames';
import React from 'react';

import spacing from '../../../../../assets/styles/spacing.module.scss';

type SelectBUorLEBlockProps = {
  type: 'businessUnit' | 'legalEntity';
};
export const SelectBUorLEBlock: React.FC<SelectBUorLEBlockProps> = ({
  type,
}: SelectBUorLEBlockProps) => {
  const text = type === 'businessUnit' ? 'Business unit' : 'Legal entity';

  return (
    <Container
      flexdirection="column"
      alignitems="center"
      flexgrow="1"
      gap="16"
      className={classNames(spacing.w360)}
    >
      <Text type="body-medium">Choose {text}</Text>
      <Search
        className={classNames(spacing.mX8, spacing.mb8, spacing.mt4)}
        width="350"
        height="40"
        name="search-BU"
        placeholder={`Search ${text}`}
        value=""
        onChange={() => {}}
        searchParams={[]}
        onSearch={() => {}}
        onClear={() => {}}
      />
      <Container flexdirection="column" gap="4" className={spacing.w100p} customScroll>
        {Array.from(Array(10)).map((_, index) => (
          <ReportsCreateItem
            mainTextColor="grey-100"
            key={index}
            id={`${index}`}
            name="trialBalanceForTheReport"
            mainText={`${text} ${index + 1}`}
            checked={index === 2}
            onClick={() => {}}
          />
        ))}
      </Container>
    </Container>
  );
};
