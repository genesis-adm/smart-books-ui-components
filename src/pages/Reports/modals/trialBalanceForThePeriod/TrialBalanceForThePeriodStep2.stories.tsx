import { DatePicker } from 'Components/base/DatePicker';
import { Divider } from 'Components/base/Divider';
import { Tag } from 'Components/base/Tag';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { MainPageContentContainer } from 'Components/base/grid/MainPageContentContainer';
import { OptionDoubleVerticalNew } from 'Components/base/inputs/SelectNew/options/OptionDoubleVerticalNew';
import { CurrencyTab, CurrencyTabWrapper } from 'Components/base/tabs/CurrencyTab';
import { Text } from 'Components/base/typography/Text';
import { ReportsTotalValues } from 'Components/custom/Reports/ReportsTotalValues';
import { Pagination } from 'Components/custom/Stories/Pagination/Pagination';
import PerPageSelect from 'Components/custom/Stories/Pagination/PerPageSelect/PerPageSelect';
import { TableBodyNew } from 'Components/elements/Table/TableBodyNew';
import { TableCellNew } from 'Components/elements/Table/TableCellNew';
import { TableCellTextNew } from 'Components/elements/Table/TableCellTextNew';
import { TableHeadNew } from 'Components/elements/Table/TableHeadNew';
import { TableNew } from 'Components/elements/Table/TableNew';
import { TableRowNew } from 'Components/elements/Table/TableRowNew';
import { TableTitleNew, TableTitleNewProps } from 'Components/elements/Table/TableTitleNew';
import { FilterDropDown } from 'Components/elements/filters/FilterDropDown';
import { FilterDropDownItem } from 'Components/elements/filters/FilterDropDownItem';
import { FilterItemsContainer } from 'Components/elements/filters/FilterItemsContainer';
import { FilterSearch } from 'Components/elements/filters/FilterSearch';
import { FilterToggleButton } from 'Components/elements/filters/FilterToggleButton';
import { FiltersWrapper } from 'Components/elements/filters/FiltersWrapper';
import { Search } from 'Components/inputs/Search';
import useSearchExample from 'Components/inputs/Search/hooks/useSearchExample';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { TableCellWidth } from 'Components/types/gridTypes';
import { options, rowsPerPageOptions } from 'Mocks/fakeOptions';
import {
  trialBalanceForThePeriodFilters,
  trialBalanceForThePeriodTableHead,
} from 'Pages/Reports/constants';
import { ReactComponent as CSVIcon } from 'Svg/v2/16/file-csv.svg';
import { ReactComponent as XLSIcon } from 'Svg/v2/16/file-xls.svg';
import { ReactComponent as InfoIcon } from 'Svg/v2/16/info.svg';
import { ReactComponent as EditIcon } from 'Svg/v2/16/pencil.svg';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Reports/Reports Modals/TrialBalanceForThePeriod',
};
export const TrialBalanceForThePeriodStep2: React.FC = () => {
  const [startDay, setStartDay] = useState<Date | null>(new Date());
  const [endDay, setEndDay] = useState<Date | null>(null);
  const [showFilters, setShowFilters] = useState<boolean>(false);
  const [rowsPerPage, setRowsPerPage] = useState<string>(rowsPerPageOptions[0].value);

  const { searchValue, setSearchValue, searchValuesList, handleSearchValueAdd, handleClearAll } =
    useSearchExample();
  const changeDayHandler = (dates: [Date | null, Date | null]) => {
    const [start, end] = dates;

    setStartDay(start);
    setEndDay(end);
  };
  const handleChange = (value: string) => {
    setSearchValue(value);
  };
  return (
    <Modal
      size="full"
      background={'grey-10'}
      overlay="grey-10"
      header={
        <ModalHeaderNew
          title="Trial Вalance for the period"
          background={'grey-10'}
          titlePosition="center"
          onClose={() => {}}
          closeActionBackgroundColor="inherit"
        />
      }
      footer={
        <ModalFooterNew justifycontent="space-between" border>
          <Pagination
            borderTop="none"
            padding="0"
            customPagination={
              <Container gap="16" alignitems="center">
                <Text type="subtext-regular" color="grey-100">
                  Rows per Page
                </Text>
                <PerPageSelect
                  selectedValue={rowsPerPage}
                  onOptionClick={(value) => setRowsPerPage(value)}
                />
              </Container>
            }
          />
          <Container>
            <Button
              background={'transparent-blue-10'}
              font="caption-semibold"
              height="40"
              color="grey-100-violet-90"
              iconLeft={<XLSIcon />}
              onClick={() => {}}
            >
              Export to XLS
            </Button>
            <Button
              background={'transparent-blue-10'}
              font="caption-semibold"
              height="40"
              color="grey-100-violet-90"
              iconLeft={<CSVIcon />}
              onClick={() => {}}
            >
              Export to CSV
            </Button>
          </Container>
        </ModalFooterNew>
      }
      pluginScrollDisabled
    >
      <Container
        justifycontent="space-between"
        alignitems="center"
        className={classNames(spacing.mt16, spacing.mX24)}
      >
        <Container alignitems="center" gap="16">
          <Container flexdirection="column" gap="8">
            <Text color="grey-100" className={spacing.ml4}>
              Legal entity
            </Text>
            <Tag
              align="left"
              icon={<InfoIcon />}
              className={classNames(spacing.w350)}
              iconPosition="left"
              cursor="pointer"
              color="grey-black"
              text="Matar and Trade Company LLC"
              iconTooltip="Account name | Legal entity | Business division"
              additionalDropdownBlock={{
                icon: <EditIcon />,
                tooltip: 'Edit account',
                padding: '16',
                dropdown: (
                  <>
                    <FilterSearch
                      className={spacing.ml8}
                      title="Please choose Accounts"
                      value=""
                      inputWidth="220"
                      onChange={() => {}}
                    />
                    <FilterItemsContainer className={spacing.w280} gap="0">
                      {options.map(({ label, secondaryLabel, value }, index) => (
                        <OptionDoubleVerticalNew
                          key={value}
                          label={label}
                          secondaryLabel={secondaryLabel}
                          type="radio"
                          selected={index === 2}
                          onClick={() => {}}
                          tooltip
                        />
                      ))}
                    </FilterItemsContainer>
                    <Divider fullHorizontalWidth />
                    <Container justifycontent="flex-end">
                      <Button width="108" height="32" onClick={() => {}}>
                        Apply
                      </Button>
                    </Container>
                  </>
                ),
              }}
              noWrap
              onClick={() => {}}
            />
          </Container>
          <Container flexdirection="column" gap="8">
            <Text color="grey-100" className={spacing.ml4}>
              Period
            </Text>
            <Tag
              color={'grey-black'}
              className={spacing.w180}
              cursor="pointer"
              text="01.04.2023-05.04.2023"
              align="left"
              noWrap
              additionalDropdownBlock={{
                icon: <EditIcon />,
                tooltip: 'Edit Period',
                padding: 'none',
                dropdown: (
                  <>
                    <DatePicker
                      name="calendar"
                      withRange
                      withPresets
                      startDate={startDay}
                      endDate={endDay}
                      onChange={changeDayHandler}
                    />
                    <Divider />

                    <Container
                      className={classNames(spacing.m16)}
                      justifycontent="space-between"
                      alignitems="center"
                      flexdirection="row-reverse"
                    >
                      <Button
                        width="auto"
                        height="32"
                        padding="8"
                        onClick={() => {}}
                        background={'violet-90-violet-100'}
                        color="white-100"
                      >
                        Apply
                      </Button>
                    </Container>
                  </>
                ),
              }}
            />
          </Container>
        </Container>
      </Container>
      <MainPageContentContainer>
        <Container
          className={classNames(spacing.mX24, spacing.mt24)}
          justifycontent="space-between"
          gap="24"
        >
          <Container gap="24" alignitems="center">
            <FilterToggleButton
              showFilters={showFilters}
              filtersSelected={false}
              onClear={() => {}}
              onClick={() => {
                setShowFilters((prevState) => !prevState);
              }}
            />
            <Search
              width="320"
              height="40"
              name="search-reports-description-search"
              placeholder="Search by Account name or Account ID"
              value={searchValue}
              onChange={(e) => setSearchValue(e.target.value)}
              searchParams={searchValuesList}
              onSearch={handleSearchValueAdd}
              onClear={handleClearAll}
            />
          </Container>
          <CurrencyTabWrapper>
            <CurrencyTab id="usd" onClick={() => {}} active>
              Base currency USD
            </CurrencyTab>
            <CurrencyTab id="eur" onClick={() => {}} active={false}>
              Original currency
            </CurrencyTab>
          </CurrencyTabWrapper>
        </Container>
        <FiltersWrapper active={showFilters}>
          {trialBalanceForThePeriodFilters.map((item) => (
            <FilterDropDown
              key={item.title}
              title={item.title}
              value="All"
              onClear={() => {}}
              selectedAmount={0}
            >
              <Container
                className={classNames(spacing.m16, spacing.w288fixed)}
                flexdirection="column"
              >
                <FilterSearch title="Account type:" value={searchValue} onChange={handleChange} />
                <FilterItemsContainer className={spacing.mt8}>
                  {options.map((item) => (
                    <FilterDropDownItem
                      key={item.value}
                      id={item.value}
                      type="item"
                      checked={false}
                      blurred={false}
                      value={item.label}
                      onChange={() => {}}
                    />
                  ))}
                </FilterItemsContainer>
              </Container>
              <Divider fullHorizontalWidth />
              <Container
                className={classNames(spacing.m16, spacing.w288fixed)}
                justifycontent="flex-end"
                alignitems="center"
              >
                <Button
                  width="auto"
                  height="32"
                  padding="8"
                  font="text-medium"
                  onClick={() => {}}
                  background={'violet-90-violet-100'}
                  color="white-100"
                >
                  Apply
                </Button>
              </Container>
            </FilterDropDown>
          ))}
        </FiltersWrapper>
        <TableNew
          noResults={false}
          className={classNames(spacing.mt24, spacing.mX24)}
          tableHead={
            <TableHeadNew background={'grey-10'} radius="10">
              {trialBalanceForThePeriodTableHead.map((item, index) => (
                <TableTitleNew
                  key={index}
                  background={'grey-10'}
                  radius="10"
                  minWidth={item.minWidth as TableCellWidth}
                  align={item.align as TableTitleNewProps['align']}
                  title={item.title}
                  onClick={() => {}}
                />
              ))}
            </TableHeadNew>
          }
        >
          <TableBodyNew>
            {Array.from(Array(4)).map((_, index) => (
              <TableRowNew key={index} background={'info'} size="36" cursor="auto">
                <TableCellNew
                  minWidth={trialBalanceForThePeriodTableHead[0].minWidth as TableCellWidth}
                  justifycontent="space-between"
                >
                  <Text>Payroll liabilities - EUR</Text>
                  <Text color="grey-100">Liabilities</Text>
                </TableCellNew>
                <TableCellTextNew
                  minWidth={trialBalanceForThePeriodTableHead[1].minWidth as TableCellWidth}
                  value="9834"
                  alignitems="center"
                />
                <TableCellTextNew
                  minWidth={trialBalanceForThePeriodTableHead[2].minWidth as TableCellWidth}
                  value="Legal entity"
                />
                <TableCellTextNew
                  minWidth={trialBalanceForThePeriodTableHead[3].minWidth as TableCellWidth}
                  value="EUR"
                  alignitems="center"
                />
                <TableCellTextNew
                  minWidth={trialBalanceForThePeriodTableHead[4].minWidth as TableCellWidth}
                  value="3,000.00 €"
                  alignitems="flex-end"
                />
                <TableCellTextNew
                  minWidth={trialBalanceForThePeriodTableHead[5].minWidth as TableCellWidth}
                  value="1,000.00 €"
                  alignitems="flex-end"
                />
                <TableCellTextNew
                  minWidth={trialBalanceForThePeriodTableHead[6].minWidth as TableCellWidth}
                  value="- 2,000.00 €"
                  alignitems="flex-end"
                />
                <TableCellTextNew
                  minWidth={trialBalanceForThePeriodTableHead[7].minWidth as TableCellWidth}
                  value="2.000.00 €"
                  alignitems="flex-end"
                />
              </TableRowNew>
            ))}
            <ReportsTotalValues
              type="balance"
              title="SubTotal"
              data={[
                ['USD', '500.00 $', '500.00 $', '-1,000.00 $', '0.00 $'],
                ['EUR', '2,000.00 €', '1,000.00 €', '-3,000.00 €', '0.00 €'],
              ]}
            />
          </TableBodyNew>
        </TableNew>
      </MainPageContentContainer>
    </Modal>
  );
};
