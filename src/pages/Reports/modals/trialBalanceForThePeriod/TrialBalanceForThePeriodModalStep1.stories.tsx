import { DatePicker } from 'Components/base/DatePicker';
import { Divider } from 'Components/base/Divider';
import { Radio } from 'Components/base/Radio';
import { Button } from 'Components/base/buttons/Button';
import { Container } from 'Components/base/grid/Container';
import { Text } from 'Components/base/typography/Text';
import { Modal } from 'Components/modules/Modal';
import { ModalFooterNew } from 'Components/modules/Modal/elements/ModalFooterNew';
import { ModalHeaderNew } from 'Components/modules/Modal/elements/ModalHeaderNew';
import { SelectBUorLEBlock } from 'Pages/Reports/modals/trialBalanceForThePeriod/components/SelectBUorLEBlock';
import classNames from 'classnames';
import React, { useState } from 'react';

import spacing from '../../../../assets/styles/spacing.module.scss';

export default {
  title: 'Pages/Reports/Reports Modals/TrialBalanceForThePeriod',
};

export const TrialBalanceForThePeriodModalStep1: React.FC = () => {
  const [isGroupedBy, setIsGroupedBy] = useState<'legalEntity' | 'businessUnit'>('legalEntity');
  const [startDay, setStartDay] = useState<Date | null>(new Date());
  const [endDay, setEndDay] = useState<Date | null>(null);
  const changeDayHandler = (dates: [Date | null, Date | null]) => {
    const [start, end] = dates;

    setStartDay(start);
    setEndDay(end);
  };
  return (
    <Modal
      size="990"
      pluginScrollDisabled
      header={
        <ModalHeaderNew
          title="Trial Вalance for the period"
          titlePosition="center"
          onClose={() => {}}
        />
      }
      footer={
        <ModalFooterNew justifycontent="space-between" border>
          <Button height="48" navigation width="180" background="grey-90" onClick={() => {}}>
            Next
          </Button>
        </ModalFooterNew>
      }
    >
      <Container justifycontent="center" gap="16" alignitems="center">
        <Text type="caption-regular" color="grey-100">
          Group by:
        </Text>
        <Radio
          type="caption-regular"
          id="le"
          name="sort"
          color={isGroupedBy === 'legalEntity' ? 'violet-90' : 'grey-90'}
          checked={isGroupedBy === 'legalEntity'}
          onChange={() => {
            setIsGroupedBy('legalEntity');
          }}
        >
          Legal entity
        </Radio>
        <Radio
          type="caption-regular"
          id="bu"
          name="sort"
          color={isGroupedBy === 'businessUnit' ? 'violet-90' : 'grey-90'}
          checked={isGroupedBy === 'businessUnit'}
          onChange={() => {
            setIsGroupedBy('businessUnit');
          }}
        >
          Business unit
        </Radio>
      </Container>
      <Container
        justifycontent="space-between"
        className={classNames(spacing.pY24, spacing.pX80, spacing.h100p)}
        gap="24"
      >
        <SelectBUorLEBlock type={isGroupedBy === 'businessUnit' ? 'businessUnit' : 'legalEntity'} />
        <Divider type="vertical" height="full" />
        <Container
          flexdirection="column"
          alignitems="center"
          flexgrow="1"
          gap="16"
          className={classNames(spacing.w360, spacing.pb16, spacing.pX16)}
          radius
        >
          <Text type="body-medium">Choose Date</Text>

          <Container
            radius
            flexdirection="column"
            background="white-100"
            style={{ boxShadow: '0px 10px 30px rgba(192, 196, 205, 0.3)' }}
            border="grey-20"
          >
            <DatePicker
              name="dateRange"
              withRange
              withPresets
              startDate={startDay}
              endDate={endDay}
              onChange={changeDayHandler}
            />
          </Container>
        </Container>
      </Container>
    </Modal>
  );
};
