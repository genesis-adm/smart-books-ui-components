---
to: <%= absPath %>/<%= component_name %>.stories.tsx
---
import React from 'react';
import { Story, Meta } from '@storybook/react';
import { <%= component_name %> as Component, <%= component_name %>Props as Props } from 'Components/<%= folder %><%= component_name %>';

const hideProperty = {
  table: {
    disable: true,
  },
};

export default {
  title: 'Draft Components/<%= component_name %>',
  component: Component,
  argTypes: {
    className: hideProperty,
  },
} as Meta;

const <%= component_name %>Component: Story<Props> = (args) => <Component {...args} />;

export const <%= component_name %> = <%= component_name %>Component.bind({});
<%= component_name %>.args = {};
