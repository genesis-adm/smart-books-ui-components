---
to: <%= absPath %>/<%= component_name %>.tsx
---
import React, { ReactElement } from 'react';
import classNames from 'classnames';
import { <%= component_name %>Props } from './<%= component_name %>.types';
import style from './<%= component_name %>.module.scss';

// Reminder: do not forget to add the following code to root index.ts
// export { <%= component_name %> } from './components/<%= folder %><%= component_name %>';

const <%= component_name %> = ({ className }: <%= component_name %>Props): ReactElement => (
  <div className={classNames(style.component, className)} />
);

export default <%= component_name %>;
