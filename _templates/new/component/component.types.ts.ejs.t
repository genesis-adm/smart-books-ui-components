---
to: <%= absPath %>/<%= component_name %>.types.tsx
---

export interface <%= component_name %>Props {
  className?: string;
}
