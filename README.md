## Available Scripts

### Before starting a project:

You need to install peer dependencies in the parent project:

- `react`
- `react-dom`

Install dependencies:
`yarn install`

In the project directory, you can run:
`yarn storybook`

Runs the app in the development mode.<br />
Open [http://localhost:6006](http://localhost:6006) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

`yarn test`

Launches the test runner in the interactive watch mode.<br />

`yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

`yarn new:component`

Creates a new component:<br />

- component.tsx <br />
- component.types.ts <br />
- component.stories.tsx <br />
- component.module.scss <br />
- index.ts<br />

`yarn chromatic`

Publishes your Storybook to Chromatic and kicks off tests if they're enabled.<br />

## Publish

### `Installing the CLI`

Verdaccio must be installed globally using either of the following methods: <br />
npm - `npm install -g verdaccio `<br />
yarn - `yarn global add verdaccio` <br />
pnpm - `pnpm install -g verdaccio` <br />

The client authentication is handled by the npm client itself. Once you log in to the application: <br />

`npm adduser --registry http://207.154.219.236:4873/`

### Publish a package:

- `npm publish`

### Publish a package with a tag (beta version etc.):

- `npm publish --tag #######`

where ####### is a tag name. Also, make sure that you have bumped a version in package.json and added a relevant tag to it.

## Usage of yarn link:

### What is yarn link

yarn link is a command that helps during the development of npm packages. It links a local npm package to an existing project that uses yarn package manager.</br>

What this does is that it removes the need to create a new example project to test your npm package in development.</br>

### How to use

To use the yarn link command, first you go the root of your local npm package and then type in the command</br>

- `yarn link`</br>

Now, You can go to your existing project that uses the yarn package manager and use the following command to start using your local npm package.</br>

- `yarn link ui-components`</br>

Make sure that the npm package is already being used by the project. The yarn link command creates a symlink in the node_modules folder of your project to the local copy of your npm package.</br>

To see the changes you made are working, you will have to run the command `yarn install --force` to recompile all packages.</br>

### How to stop:

You will have to first go to the root of your local ui-components and type in the command</br>

- `yarn unlink`</br>

Now you can go to your project and type</br>

- `yarn unlink ui-components`</br>

This will remove all symlinks and allow you to again use the published npm packages instead of the local ones.</br>

Again, you will have to run the `yarn install --force` command to recompile all packages.</br>
